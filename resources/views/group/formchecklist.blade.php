@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">{{ $title }}</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post">
                        @csrf
                        @foreach ($fields as $row)
      			              	{!! $row !!}
      			          	@endforeach
                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                @foreach ($buttons as $row)
                                    {!! $row !!}
                                @endforeach
                            </div>
                        </div>
                        @php ($Priv = count($privilage)) @endphp

                  			@if ($Priv > 0)
                              <div class="panel-body">

                      				<div class="content-group">
                      					  @php ($No = 1) @endphp
                      			  		@foreach ($privilage as $row)
                        			  			@if ($row["active"] == 0)
                          			  				@php ($Label = "Activate") @endphp
                          			  				@php ($Color = "#CCCCCC") @endphp
                        			  			@else
                          			  				@php ($Label = "Inactive") @endphp
                          				  			@if ($row["status"] == 0)
                          								     @php ($Color = "#FFE6E7") @endphp
                          				  			@else
                          								     @php ($Color = "#FFFFFF") @endphp
                          				  			@endif
                        				  		@endif

                          			  	  @if ($row["level"] == 1)
                            							<h6 class="text-semibold heading-divided"><input type="checkbox" name="check_menu[]" value="{{ $row["id"] }}" {{ $row["status"] == "1" ? "checked" : "" }} {{ $row["active"] == "0" ? "disabled" : "" }} />&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-folder6 position-left"></i> ({{ $row["level"] }}) {{ $row["name"] }} <small class="position-right"></small><span class="label {{ $row["active"] == "1" ? "label-primary" : "label-danger" }}" style="cursor: pointer;" onclick="window.location='{{ URL::to('group/'.strtolower($Label)).'/'.$row["group"].'/'.$row["id"] }}'">SET {{ $Label }}</span></h6>
                            							@if ($row["unit"] > 0)
                            								  <div class="no-border" style="margin-top:-10px;">
                            							@endif
                            							@php ($Unit = $row["unit"]) @endphp
                          						@else
                            							@if ($row["level"] == 2)
                              								<a href="javascript:void(0)" class="list-group-item no-border" style="cursor: default;">
                              									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="check_menu[]" value="{{ $row["id"] }}" {{ $row["status"] == "1" ? "checked" : "" }} {{ $row["active"] == "0" ? "disabled" : "" }} />&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-file-text2"></i> ({{ $row["level"] }}) {{ $row["name"] }} <span class="label {{ $row["active"] == "1" ? "label-primary" : "label-danger" }}" style="cursor: pointer;" onclick="window.location='{{ URL::to('group/'.strtolower($Label)).'/'.$row["group"].'/'.$row["id"] }}'">SET {{ $Label }}</span>
                              								</a>
                            							@else
                              								<a href="javascript:void(0)" class="list-group-item no-border" style="cursor: default;">
                              									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="check_menu[]" value="{{ $row["id"] }}" {{ $row["status"] == "1" ? "checked" : "" }} {{ $row["active"] == "0" ? "disabled" : "" }} />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-file-text2"></i> ({{ $row["level"] }}) {{ $row["name"] }} <span class="label {{ $row["active"] == "1" ? "label-primary" : "label-danger" }}"  style="cursor: pointer;" onclick="window.location='{{ URL::to('group/'.strtolower($Label)).'/'.$row["group"].'/'.$row["id"] }}'">SET {{ $Label }}</span>
                              								</a>
                            							@endif
                              						@if ($row["level"] == 2)
                              								@if ($Unit == $No)
                                									</div>
                                									@php ($No = 1) @endphp
                              								@else
                                									@php
                                									    $No = $No + 1
                                									@endphp
                              								@endif
                              					  @endif
                          						@endif
                      					 @endforeach
                      				   </div>
                      			</div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
