@extends('main')
@section('content')
<div id="content" class="content">
    <form id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" autocomplete="off">
    @csrf
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">{{ $title }}</h4>
                </div>
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="panel-body">
                    <!-- <div class="col-md-3 pull-right">
                        <input type="text" class="form-control" placeholder="Search ..."name="text_search" id="text_search" value="{{ $text_search }}">
                    </div> -->
                    <!-- <div class="col-md-2 pull-right">
                        <input type="date" class="form-control" placeholder="Tanggal Awal"name="tanggalawal" id="text_search" >
                    </div>
                        <input type="date" class="form-control" placeholder="Tanggal Awal"name="tanggalakhir" id="text_search" >
                    </div> -->
                    <p>

                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="btn-group btn-group-sm">
                                @foreach ($action as $row => $action_menu)
                                {!! getActionButton($action_menu->name, $action_menu->url, $action_menu->icon) !!}
                                @endforeach
                                <a href="" class="btn btn-sm btn-success"><i class="fa fa-refresh"></i></a>
                            </div>
                        </div>
                    </div>
<!--                     <div class="btn-group btn-group-justified">
                        
                    </div> -->
                        <!-- <a href="javascript:;" class="btn btn-sm btn-success" id="search-panel-botton"><i class="fa fa-search"></i></a> -->
                    </p>
                        <!-- <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" />
                        </div>
                         --><!-- <div class="form-group m-r-10">
                            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" />
                        </div> -->
                        <div class=row>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <label for="ex2">from</label>
                                <input class="form-control input-sm" id="tgl_awal_pengajuan" name="tgl_awal_pengajuan" type="date" value="{{$tgl_awal_pengajuan}}">
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <label for="ex1">to</label>
                                <input class="form-control input-sm" id="tgl_akhir_pengajuan" name="tgl_akhir_pengajuan" type="date" value="{{$tgl_akhir_pengajuan}}">
                            </div> 
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <label for="ex1">status</label>
                                <select class="form-control input-sm" id="text_status" name="text_status">
                                    @foreach($option as $key => $value)
                                    @if($key == $status_approval)
                                    <option value="{{$key}}" selected>
                                        {{$value}}
                                    </option >
                                    @else
                                    <option value="{{$key}}">
                                        {{$value}}
                                    </option>
                                    @endif    
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                <label for="ex1">nama karyawan</label>
                                <input class="form-control input-sm" id="text_search" name="text_search" type="text"  placeholder="Searching" value="{{$text_search}}" >
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-3">
                                <label for="ex1">&nbsp;</label>
                                <!-- <input class="form-control input-sm" id="text_search" name="text_search" type="hidden" placeholder="nama karyawan" value=""> -->
                                <input class="form-control btn-sm btn-primary" type="submit" value="cari">
                            </div>  
                        </div>
                    </p>
                    
                    <div style="margin-bottom: 15px;  overflow: scroll;" id="tbl">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                @foreach ($table_header as $header)
                                    <th width="{{ $header["width"] }}" style="text-align:{{ $header["align"] }}; border: 0px; font-weight: bold;">{{ $header["label"] }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($select) == 0)
                                <tr>
                                    <td colspan="{{ count($table_header) }}" style="text-align: center; background-color: #fff">No data found (0)</td>
                                </tr>
                            @else
                                @php ($No = 0) @endphp

                                @foreach ($select as $rs)
                                    <tr>
                                        @foreach ($table_header as $row)
                                            @php ($field = $row["name"]) @endphp

                                            <td style="width:{{ $row["width"] }}; text-align:{{ $row["item-align"] }};">
                                                @if ($row["item-format"] == "number")
                                                    {{ number_format($rs->$field, 0) }}
                                                @elseif ($row["item-format"] == "flag")
                                                    {!! getLabelFlag($rs->status_code) !!}
                                                @elseif ($row["item-format"] == "checkbox")
                                                    <input type="checkbox" id="checkbox_id_<?=$No?>" name="checkbox_id[]" class ="checkbox_id" value="{{ $rs->$field }}">
                                                @elseif ($row["item-format"] == "view")
                                                    @if($rs->$field != "" || $rs->$field != null)
                                                    <a href="{{ $rs->$field }}" target="_blank" class="btn btn-sm btn-info m-r-5">Lihat</a>
                                                    @endif
                                                @else
                                                    {{ $rs->$field }}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                    @php
                                        $No = $No + 1
                                    @endphp
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-15">
                            {!! $pagging !!}
                        </div>
                        <div class="col-sm-5">
                            <div class="dataTables_info">
                              @if (empty($query))
                                  No data found | (0) data
                              @else
                                  Found ({{$record}}) rows
                              @endif
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="text_control" name="text_control" value="hide">
    <form>
</div>
<script>
    $("#success-box").delay(1000).slideUp(500);
    $("#alert-box").delay(1000).slideUp(500);

    $("#search-panel-botton").click(function(){
        var control = $("#text_control").val();

        if(control == "hide") {
            $("#search-panel-body").show();
            $("#text_control").val("show");                
        } else {
            $("#search-panel-body").hide();
            $("#text_control").val("hide");                
        }
    });
</script>
@stop
