<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use Image;

class UserModel extends Model
{
    protected $table 	= "sys_users";

    public function getList($request=null, $offset=null, $limit=null) {
        $query 	= DB::table("vw_users")
                            ->select("id", "nik", "name", "group_name", "group_id", "avatar"
                                    , "user_status", "perpage", "status_code")
                            ->orderBy("id", "DESC");

        if(session()->has("SES_SEARCH_USER")) {
            $query->where("name", "LIKE", "%" . session()->get("SES_SEARCH_USER") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("vw_users")
                            ->select("id", "nik", "email", "name", "group_name", "group_id", "avatar"
                                    , "user_status", "perpage", "status_code")
                            ->where("id", $id)
                            ->orderBy("id", "DESC");

        $result = $query->get();

        return $result;
    }  

    public function getAksesuser($id) {
        $query  = DB::table("sys_users")
                            ->select( "group_id")
                            ->where("id", $id)
                            ->orderBy("id", "DESC");

        $result = $query->get();

        return $result;
    }  

    public function getGroup($id) {
        $query  = DB::table("m_jabatan")
                             ->select("group_id")
                             ->where("id_jabatan", $id);
        $result = $query->get();

        return $result;
    }        

    public function createData($request) {
        $qUser              = new UserModel;
        $group_id = $this->getGroup($request->id_jabatan)->first();
	if(is_null($group_id )){
        $group = 5;
        }else{
        $group = $group_id->group_id;
        }
        $p_karyawan = DB::table('p_karyawan')->where("id_karyawan", $request->id_karyawan)->first();
        $nik = '';
        $id_cabang=0;
        if($p_karyawan != null){
            $nik = $p_karyawan->nik;
            $id_cabang= $p_karyawan->id_cabang;
        }

        # ---------------
        $qUser->nik         = setString($nik);
        $qUser->email       = setString($request->email);
        $qUser->name        = setString($request->nama_karyawan);
        $qUser->password    = bcrypt("password");
        $qUser->group_id    = setString($group);
        $qUser->user_status = "1";
        $qUser->id_cabang   = setString($id_cabang);
        $qUser->avatar      = "unknown.png";
        $qUser->perpage     = env("APPS_PERPAGE");
        # ---------------
        $qUser->save();
        
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE USER (" . $qUser->id . ") " . strtoupper($request->email), Auth::user()->id, $request);
    }
    public function createData2($request) {
        $qUser              = new UserModel;
        $group_id = $this->getGroup($request->id_jabatan)->first();
        $p_karyawan = DB::table('p_karyawan')->where("nik", $request->nik)->first();
      
        $nik = $request->nik;
        $id_cabang=0;

        if($p_karyawan != null){
           // $nik = $p_karyawan->nik;
            $id_cabang= $p_karyawan->id_cabang;
        }
	

        # ---------------
        $qUser->nik         = setString($nik);
        $qUser->email       = setString($request->email);
        $qUser->name        = setString($request->name);
        $qUser->password    = bcrypt("password");
        $qUser->group_id    = setString($request->group_id);
        $qUser->user_status = "1";
        $qUser->id_cabang   = setString($id_cabang);
        $qUser->avatar      = "unknown.png";
        $qUser->perpage     = env("APPS_PERPAGE");
        # ---------------
        $qUser->save();
        
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE USER (" . $qUser->id . ") " . strtoupper($request->email), Auth::user()->id, $request);
    }	

    public function cekUsers($nik)
    {
     
        $query  = DB::table("sys_users")
                            ->select("nik")
                            ->where("nik", $nik);
                            
        $result = $query->get()->first();
        return $result;

    }
     public function cekEmail($email)
    {
     
        $query  = DB::table("sys_users")
                            ->select("email")
                            ->where("email", $email);
                            
        $result = $query->get()->first();
        return $result;

    }


    public function updateData($request) {
         //$group_id = $this->getGroup($request->id_jabatan)->first();
           // dd($request->id,$request->nik,$request->email,$request->nama_karyawan,$request->password);
          DB::table("sys_users")
                             ->where("id", $request->id)
                             ->update([  "nik"       => setString($request->nik),
                                        "email"       => setString($request->email),
                                        "password"    => bcrypt($request->password),
                                        "group_id"    => setString($request->group_id),
                                        "user_status" => "1",
                                        "avatar"      => "unknown.png",
                                        "perpage"     => env("APPS_PERPAGE")]);



            /*

                Update Tunjangan Jabatan di Tabel Karyawan                          

            */
       
              
        # ---------------
            
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE LEVEL(" . $request->id_level . ") " . strtoupper($request->nama_level), Auth::user()->id, $request);
    }

    public function updateDatakry($request) {
        $ceknik = $this->cekUsers($request->nik);
        $cekEmail= $this->cekEmail($request->email);
        $qUser              = new UserModel;
        $group_id = $this->getGroup($request->id_jabatan)->first();
       
       
        if(empty($ceknik))

        {
                //$simpan= $this->createData($request);

                 
                    # ---------------
                    $qUser->nik         = setString($request->nik);
                    $qUser->email       = setString($request->email);
                    $qUser->name        = setString($request->nama_karyawan);
                    $qUser->password    = bcrypt("password");
                    $qUser->group_id    = setString($group_id->group_id);
                    $qUser->user_status = "1";
                    $qUser->id_cabang   = setString($request->id_cabang);
                    $qUser->avatar      = "unknown.png";
                    $qUser->perpage     = env("APPS_PERPAGE");
                    # ---------------
                    $qUser->save();

        }
        else
        {
                       DB::table("sys_users")
                             ->where("nik", $request->nik)
                             ->update([  
                                        "email"       => setString($request->email),
                                        "group_id"    => setString($group_id->group_id),

                                      ]);
                /* ----------
                 Logs
                ----------------------- */
                    $qLog       = new LogModel;
                    # ---------------;
                    $qLog->createLog("UPDATE USER (" . $request->id . ") " . strtoupper($request->email), Auth::user()->id, $request);    
        }

       
        
    }

    public function removeData($request) {
        $qUser          = UserModel::find($request->id);
        # ---------------
        $qUser->delete();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE USER (" . $request->id . ") " . strtoupper($request->email), Auth::user()->id, $request);
    }

    public function updatePassword($request) {
        $qUser          = UserModel::find($request->id);
        # ---------------
        //if($request->password != env("APPS_PWDDEF")) {
            $qUser->password  = bcrypt($request->new_password);
        //}

        # ---------------
        $qUser->save();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CHANGE PASSWORD " . strtoupper($request->email), Auth::user()->id, $request);
    }

    public function updateProfile($request) {
        $qUser          = UserModel::find($request->id);

        if($request->file('foto') != null){
            $name = $qUser->name;
            $nik = str_replace(".", "", $qUser->nik);;
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = $nik.'-'.$name.'.' . $foto->getClientOriginalExtension();
            $foto->move('app/profile', $namaFile);
            //$filepath = public_path().'/app/profile/'.$namaFile;
            //Image::load($filepath)
            //    ->optimize()
            //    ->save($filepath);
            
            DB::table("sys_users")
                                ->where("id", $request->id)
                                ->update([ 
                                            "avatar"=>$namaFile,
                                        ]);
        }
        
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CHANGE FOTO " . strtoupper($request->email), Auth::user()->id, $request);
    }
}
