@extends('main')
@section('content')
<div id="content" class="content">
    <form id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" autocomplete="off">
    @csrf
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            
                   

            <div class="panel panel-inverse">
            <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="" method="post">
                        @csrf
                       
                        @foreach ($fields as $row)
                            {!! $row !!}
                        @endforeach
                        
                    </form>
                </div>
                <hr style="border-style: inset;border-width: 2px;">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                <div class="alert alert-success" id="success-box" style="{{ (Session::has("success_message")) ? "" : "display:none;" }}; border-radius: 0 !important;">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="success-message">{{ (Session::has("success_message")) ? Session::get("success_message") : "" }}</span>
                </div>
                <div class="panel-body">
                    <div class="col-md-3 pull-right" style="position: absolute; right: 0; margin-right: 10px;">
                        <input type="text" class="form-control" placeholder="Search ..."name="text_search" id="text_search" value="{{ $text_search }}">
                    </div>
                    <p>
                        @if ($hakakses == "1" or $hakakses == "4" )
                        <a href="{{ URL::to('/').$form_act_add }}" class="btn btn-sm btn-success m-r-5 btn-none-direct" title="http://localhost/mitrasdm/public/keluarga/add"><i class="fa fa-plus"></i>&nbsp;&nbsp; Add</a>
                       @endif
                        @if (isset($action))
                            @foreach ($action as $action_menu)
                                {!! getActionButton($action_menu->name, $action_menu->url, $action_menu->icon) !!}
                            @endforeach
                        @endif
                          @if (isset($form_act_add))
                             {!! getActionButton($label_add, $form_act_add, "direct|fa fa-plus") !!}
                          @endif
                         @if (isset($form_act_edit))
                             {!! getActionButton($label_edit, $form_act_edit, "single|fa fa-pencil") !!}
                          @endif
                          @if (isset($form_act_delete))
                             {!! getActionButton($label_delete, $form_act_delete, "single|fa fa-pencil") !!}
                          @endif
                        <a href="" class="btn btn-sm btn-success"><i class="fa fa-refresh"></i></a>
                        <!-- <a href="javascript:;" class="btn btn-sm btn-success" id="search-panel-botton"><i class="fa fa-search"></i></a> -->
                    </p>
                    <div style="margin-bottom: 15px; display: none;" id="search-panel-body">
                        <h4>Search</h4>
                        <div class="form-group m-r-10">
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" />
                        </div>
                        <div class="form-group m-r-10">
                            <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" />
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary m-r-5">Search</button>
                        <button type="submit" class="btn btn-sm btn-default">Clear Search</button>
                    </div>

                    <div style="margin-bottom: 15px;  overflow: scroll;" id="tbl">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                @foreach ($table_header as $header)
                                    <th width="{{ $header["width"] }}" style="text-align:{{ $header["align"] }}; border: 0px; font-weight: bold;">{{ $header["label"] }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($select) == 0)
                                <tr>
                                    <td colspan="{{ count($table_header) }}" style="text-align: center; background-color: #fff">No data found (0)</td>
                                </tr>
                            @else
                                @php ($No = 0) @endphp

                                @foreach ($select as $rs)
                                    <tr>
                                        @foreach ($table_header as $row)
                                            @php ($field = $row["name"]) @endphp

                                            <td style="width:{{ $row["width"] }}; text-align:{{ $row["item-align"] }};">
                                                @if ($row["item-format"] == "number")
                                                    {{ number_format($rs->$field, 0) }}
                                                @elseif ($row["item-format"] == "flag")
                                                    {!! getLabelFlag($rs->$field) !!}
                                                @elseif ($row["item-format"] == "checkbox")
                                                    <input type="checkbox" id="checkbox_id_<?=$No?>" name="checkbox_id[]" class ="checkbox_id" value="{{ $rs->$field }}">
                                                @else
                                                    {{ $rs->$field }}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                    @php
                                        $No = $No + 1
                                    @endphp
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-15">
                            {!! $pagging !!}
                        </div>
                        <div class="col-sm-5">
                            <div class="dataTables_info">
                              @if (empty($query))
                                  No data found | (0) data
                              @else
                                  Found ({{$record}}) rows
                              @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="text_control" name="text_control" value="hide">
    </form>
</div>
<script>
    $("#success-box").delay(1000).slideUp(500);
    $("#alert-box").delay(1000).slideUp(500);

    $("#search-panel-botton").click(function(){
        var control = $("#text_control").val();

        if(control == "hide") {
            $("#search-panel-body").show();
            $("#text_control").val("show");                
        } else {
            $("#search-panel-body").hide();
            $("#text_control").val("hide");                
        }
    });
</script>
@stop
