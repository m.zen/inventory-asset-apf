@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                <div class="alert alert-success" id="success-box" style="{{ (Session::has("success_message")) ? "" : "display:none;" }}; border-radius: 0 !important;">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="success-message">{{ (Session::has("success_message")) ? Session::get("success_message") : "" }}</span>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" enctype="multipart/form-data">
                        @csrf
                       
                        @foreach ($fields as $row)
  			              	{!! $row !!}
  			          	@endforeach
                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                @foreach ($buttons as $row)
                                    {!! $row !!}
                                @endforeach
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('app/js/form_validation.js') }}"></script>
<script>

    
    var id_asset  = $('select[name=id_asset]');
    var dari_cabang=  $('#dari_cabang');
    var nama_cabang=  $('#nama_cabang');
    var dari_departemen=  $('#ndari_departemen');
    var nama_departemen=  $('#nama_departemen');
    var dari_lokasi=  $('#dari_lokasi');
    var nama_lokasi=  $('#nama_lokasi');
    var ke_cabang=$('select[name=ke_cabang]');
    var ke_departemen=$('select[name=ke_departemen]');
    var id_lokasi=$('select[name=id_lokasi]');
   
    var urlSelect       = {!! json_encode($url_select) !!};

     
    // Fungsi ini dipanggil untuk Reload data di awal.
    init_asset(id_asset.val(), urlSelect);
// Fungsi ini dipanggil untuk get data pada saat select karyawan dipilih
   $(id_asset).on('change', function() {
      var value = this.value;
      init_asset(this.value, urlSelect);
     // alert("message successfully sent");
    });


    // Fungsi dibuat dengan parameter value yaitu id_karyawan
    function init_asset(value, route){
         var APP_URL = {!! json_encode(url('/')) !!} + route + value;

      $.get( APP_URL, function(data){
        var response = JSON.parse(data);
       console.log(response);

        var id_asset  = $('select[name=id_asset]');
        var dari_cabang=  $('#dari_cabang');
        var dari_departemen=  $('#dari_departemen');
        var dari_lokasi=  $('#dari_lokasi');
        var nama_cabang=  $('#nama_cabang');
        var nama_departemen=  $('#nama_departemen');
        var nama_lokasi=  $('#nama_lokasi');

        var ke_cabang=$('select[name=ke_cabang]');
        var ke_departemen=$('select[name=ke_departemen]');
        var id_lokasi=$('select[name=id_lokasi]');
        $(dari_cabang).val(response["id_cabang"]);
        $(nama_cabang).val(response["nama_cabang"]);
        $(dari_departemen).val(response["id_departemen"]);
        $(nama_departemen).val(response["nama_departemen"]);
        $(dari_lokasi).val(response["id_lokasi"]);
        $(nama_lokasi).val(response["nama_lokasi"]);
        $(ke_cabang).val(response.id_cabang).change();
        $(ke_departemen).val(response.id_departemen).change();
        $(id_lokasi).val(response.id_lokasi).change();

        // $(SelectCabang).val(response.id_cabang).change();
        // $(SelectDepartmen).val(response.id_departemen).change();
        // $(SelectJabatan).val(response.id_jabatan).change();
      });
    };




</script>
@stop
