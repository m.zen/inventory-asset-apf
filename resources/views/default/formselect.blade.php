@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                <div class="alert alert-success" id="success-box" style="{{ (Session::has("success_message")) ? "" : "display:none;" }}; border-radius: 0 !important;">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="success-message">{{ (Session::has("success_message")) ? Session::get("success_message") : "" }}</span>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" enctype="multipart/form-data">
                        @csrf
                       
                        @foreach ($fields as $row)
  			              	{!! $row !!}
  			          	@endforeach
                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                @foreach ($buttons as $row)
                                    {!! $row !!}
                                @endforeach
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('app/js/form_validation.js') }}">
</script>
<script src="{{ asset('app/js/jquery.mask.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
var APP_URL = {!! json_encode(url('/')) !!};
$('#id_cabang').on('change', function(e){
    e.preventDefault();
      
$.getJSON( APP_URL +"/dataapproval/departemenCabang/" + e.target.value, function(data) {
             //console.log(data);            
                var temp = [];
                //CONVERT INTO ARRAY
                $.each(data, function(key, value) {
                    temp.push({v:value, k: key});
                });
                //SORT THE ARRAY
                temp.sort(function(a,b){
                   if(a.v > b.v){ return 1}
                    if(a.v < b.v){ return -1}
                      return 0;
                });
                //APPEND INTO SELECT BOX
                $('#id_departemen').empty();
                // $('#id_departemen').hide();
                // $('#myform').find('#id_departemen').append('<option value="0" selected="selected">-Pilih-</option>');
                $('#id_approvaller').empty();
                $('#id_departemen').append('<option value="0" selected="selected">-Pilih-</option>');
                $('#id_departemen').append('<option value="all"">Semua</option>');
                $.each(temp, function(key, obj) {
                    // var opt = document.createElement('option');
                    // opt.text = obj[key];
                    // opt.value = key;
                    // $('#id_departemen').append(opt, null);
                    $('#id_departemen').append  ('<option value="' + obj.k +'" >' + obj.v + '</option>');
                });
                // $('#id_departemen').show();
                // $('.form-control selectpicker input-sm').hide();
                
                //APPEND INTO SELECT BOX
                // $('#id_departemen_approvaller').empty();
                // // $('#id_departemen').hide();
                // // $('#myform').find('#id_departemen').append('<option value="0" selected="selected">-Pilih-</option>');
                // $('#id_departemen_approvaller').empty();
                // $('#id_departemen_approvaller').append('<option value="0" selected="selected">-Pilih-</option>');
                // $.each(temp, function(key, obj) {
                //     // var opt = document.createElement('option');
                //     // opt.text = obj[key];
                //     // opt.value = key;
                //     // $('#id_departemen').append(opt, null);
                //     $('#id_departemen_approvaller').append  ('<option value="' + obj.k +'" >' + obj.v + '</option>');
                // });
            });  
        });

$('#id_departemen').on('change', function(e){
    e.preventDefault();
    var id_cabang = $("#myform").find("#id_cabang").val();
$.getJSON(APP_URL+"/dataapproval/jabatan/"+id_cabang+"/" + e.target.value, function(data) {
             //console.log(data);            
                var temp = [];
                //CONVERT INTO ARRAY
                $.each(data, function(key, value) {
                    temp.push({v:value, k: key});
                });
                //SORT THE ARRAY
                temp.sort(function(a,b){
                   if(a.v > b.v){ return 1}
                    if(a.v < b.v){ return -1}
                      return 0;
                });
                //APPEND INTO SELECT BOX
                $('#id_jabatan').empty();
                $('#id_jabatan').hide();
                $('#id_jabatan').append('<option value="0" selected="selected">-Pilih-</option>');
                $('#id_jabatan').append('<option value="all" >Semua</option>');
                $.each(temp, function(key, obj) {
                    // var opt = document.createElement('option');
                    // opt.text = obj[key];
                    // opt.value = key;
                    // $('#id_departemen').append(opt, null);
                    $('#id_jabatan').append  ('<option value="' + obj.k +'" >' + obj.v + '</option>');
                });
                $('#id_jabatan').show();
                // $('.form-control selectpicker input-sm').hide();
            });  
        });
    
    

    $('#id_cabang_approvaller').on('change', function(e){
    e.preventDefault();
      
var host = window.location.href
$.getJSON(APP_URL+"/dataapproval/departemenCabang/" + e.target.value, function(data) {
             //console.log(data);            
                var temp = [];
                //CONVERT INTO ARRAY
                $.each(data, function(key, value) {
                    temp.push({v:value, k: key});
                });
                //SORT THE ARRAY
                temp.sort(function(a,b){
                   if(a.v > b.v){ return 1}
                    if(a.v < b.v){ return -1}
                      return 0;
                });
                //APPEND INTO SELECT BOX
                $('#id_departemen_approvaller').empty();
                // $('#id_departemen').hide();
                // $('#myform').find('#id_departemen').append('<option value="0" selected="selected">-Pilih-</option>');
                $('#id_departemen_approvaller').empty();
                $('#id_departemen_approvaller').append('<option value="0" selected="selected">-Pilih-</option>');
                $.each(temp, function(key, obj) {
                    // var opt = document.createElement('option');
                    // opt.text = obj[key];
                    // opt.value = key;
                    // $('#id_departemen').append(opt, null);
                    $('#id_departemen_approvaller').append  ('<option value="' + obj.k +'" >' + obj.v + '</option>');
                });
                // $('#id_departemen').show();
                // $('.form-control selectpicker input-sm').hide();
                
                //APPEND INTO SELECT BOX
            });  
        });

    $('#id_departemen_approvaller').on('change', function(e){
    e.preventDefault();
    var id_cabang = $("#myform").find("#id_cabang_approvaller").val();
var host = window.location.href
$.getJSON(APP_URL+"/dataapproval/jabatan/"+id_cabang+"/" + e.target.value, function(data) {
             //console.log(data);            
                var temp = [];
                //CONVERT INTO ARRAY
                $.each(data, function(key, value) {
                    temp.push({v:value, k: key});
                });
                //SORT THE ARRAY
                temp.sort(function(a,b){
                   if(a.v > b.v){ return 1}
                    if(a.v < b.v){ return -1}
                      return 0;
                });
                //APPEND INTO SELECT BOX
                $('#id_approvaller').empty();
                $('#id_approvaller').hide();
                $('#id_approvaller').append('<option value="0" selected="selected">-Pilih-</option>');
                $.each(temp, function(key, obj) {
                    // var opt = document.createElement('option');
                    // opt.text = obj[key];
                    // opt.value = key;
                    // $('#id_departemen').append(opt, null);
                    $('#id_approvaller').append  ('<option value="' + obj.k +'" >' + obj.v + '</option>');
                });
                $('#id_approvaller').show();
                // $('.form-control selectpicker input-sm').hide();
            });  
        });
});
</script>
@stop
