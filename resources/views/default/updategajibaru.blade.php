@extends('main')
@section('content')
<div id="content" class="content">
    
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="panel-body">
                <div class="form-group">
                    <div class="col-md-12" style="margin-bottom:20px;">
                        @foreach ($fieldsHeader as $rowHeader)
                            {!! $rowHeader !!}
                        @endforeach
                    </div>
                </div>
                    
                <form id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" autocomplete="off">
                    @csrf
                    <div class="form-group">
                            
                          <div class="col-md-3 pull-right">
                            <input class="form-control input-sm" id="text_search" name="text_search" type="text" placeholder="search ..." value="{{$text_search}}">
                          </div>
                          {!! getActionButton("Edit", $form_act_edit, "single|fa fa-pencil") !!}
                        {!! getActionButton("PPh21", $form_act_pph, "direct|fa fa-plus") !!}
                        {!! getActionButton("Export", $form_act_export, "direct|fa fa-plus") !!}
                        </div>
                    </p>
                    
                    <div style="margin-bottom: 15px;  overflow: scroll;" id="tbl">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                @foreach ($table_header as $header)
                                    <th width="{{ $header["width"] }}" style="text-align:{{ $header["align"] }}; border: 0px; font-weight: bold;">{{ $header["label"] }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($select) == 0)
                                <tr>
                                    <td colspan="{{ count($table_header) }}" style="text-align: center; background-color: #fff">No data found (0)</td>
                                </tr>
                            @else
                                @php ($No = 0) @endphp

                                @foreach ($select as $rs)
                                    <tr>
                                        @foreach ($table_header as $row)
                                            @php ($field = $row["name"]) @endphp

                                            <td style="width:{{ $row["width"] }}; text-align:{{ $row["item-align"] }};">
                                                @if ($row["item-format"] == "number")
                                                    {{ number_format($rs->$field, 0) }}
                                                @elseif ($row["item-format"] == "flag")
                                                    {!! getLabelFlag($rs->status_code) !!}
                                                @elseif ($row["item-format"] == "checkbox")
                                                    <input type="checkbox" id="checkbox_id_<?=$No?>" name="checkbox_id[]" class ="checkbox_id" value="{{ $rs->$field }}">
                                                @else
                                                    {{ $rs->$field }}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                    @php
                                        $No = $No + 1
                                    @endphp
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-15">
                            {!! $pagging !!}
                        </div>
                        <div class="col-sm-5">
                            <div class="dataTables_info">
                              @if (empty($query))
                                  No data found | (0) data
                              @else
                                  Found ({{$record}}) rows
                              @endif
                            </div>
                        </div>
                    </div>
                    <form>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="text_control" name="text_control" value="hide">
    
</div>
<script>
    $("#success-box").delay(1000).slideUp(500);
    $("#alert-box").delay(1000).slideUp(500);

    $("#search-panel-botton").click(function(){
        var control = $("#text_control").val();

        if(control == "hide") {
            $("#search-panel-body").show();
            $("#text_control").val("show");                
        } else {
            $("#search-panel-body").hide();
            $("#text_control").val("hide");                
        }
    });
</script>
@stop
