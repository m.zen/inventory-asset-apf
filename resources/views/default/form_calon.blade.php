@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                <div class="alert alert-success" id="success-box" style="{{ (Session::has("success_message")) ? "" : "display:none;" }}; border-radius: 0 !important;">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="success-message">{{ (Session::has("success_message")) ? Session::get("success_message") : "" }}</span>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" enctype="multipart/form-data">
                        @csrf
                       
                        @foreach ($fields as $row)
  			              	{!! $row !!}
  			          	@endforeach
                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                @foreach ($buttons as $row)
                                    {!! $row !!}
                                @endforeach
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('app/js/form_validation.js') }}"></script>
<script src="{{ asset('app/js/jquery.mask.min.js') }}"></script>
<script>

    var SelectKaryawan  = $('select[name=id_karyawan_non_aktif]');
    var no_ktp           = $('#no_ktp');
    var tgl_berlaku_ktp = $('#tgl_berlaku_ktp');
    var no_kk           = $('#no_kk');
    var nama_karyawan   = $('#nama_karyawan');
    var id_departemen   = $('select[name=id_departemen]');
    var id_jabatan      = $('select[name=id_jabatan]');
    var id_cabang       = $('select[name=id_cabang]');
    var id_pendidikan   = $('select[name=id_pendidikan]');
    var status_nikah    = $('select[name=status_nikah]');
    var nonpwp          = $('#nonpwp');
    var id_ptkp         = $('select[name=id_ptkp]');
    var nobpjskes       = $('#nobpjskes');
    var nobjstk         = $('#nobjstk');
    var no_rekening     = $('#no_rekening');
    var asrekening      = $('#asrekening');
    var email           = $('#email');
    var tempat_lahir    = $('#tempat_lahir');
    var tgl_lahir       = $('#tgl_lahir');
    var jenis_kelamin   = $('select[name=jenis_kelamin]');
    var agama           = $('select[name=agama]');
    var golongan_darah  = $('select[name=golongan_darah]');
    var alamat          = $('#alamat');
    var no_telpon       = $('#no_telpon');
    var hp              = $('#hp');
    var SelectCabang    = $('select[name=id_cabangbaru]');
    var SelectDepartmen = $('select[name=id_departemenbaru]');
    var SelectJabatan   = $('select[name=id_jabatanbaru]');
    var urlSelect       = {!! json_encode($url_select) !!};

    // Fungsi ini dipanggil untuk Reload data di awal.
    init_karyawan(SelectKaryawan.val(), urlSelect);

    // Fungsi ini dipanggil untuk get data pada saat select karyawan dipilih
    $(SelectKaryawan).on('change', function() {
      var value = this.value;
      init_karyawan(this.value, urlSelect);
    });


    // Fungsi dibuat dengan parameter value yaitu id_karyawan
    function init_karyawan(value, route){
         var APP_URL = {!! json_encode(url('/')) !!} + route + value;

      $.get( APP_URL, function(data){
        var response = JSON.parse(data);
        var tgl_berlaku_ktp = response["tgl_berlaku_ktp"];
        var tgl_lahir = response["tgl_lahir"];
        // var no_hp = response["no_hp"].mask('9999-9999-9999');
        // console.log(response, tgl_berlaku_ktp == null);
        $(no_ktp).val(response["no_ktp"]);
        if(tgl_berlaku_ktp == null){
            $(tgl_berlaku_ktp).val(response["tgl_berlaku_ktp"]);
        }else{
            $(tgl_berlaku_ktp).val(formatDate(new Date(response["tgl_berlaku_ktp"]), 'dd/MM/yyyy'));
        }
        $(no_kk).val(response["no_kk"]);
        $(nama_karyawan).val(response["nama_karyawan"]);
        $(id_departemen).val(response.id_departemen).change();
        $(id_jabatan).val(response.id_jabatan).change();
        $(id_cabang).val(response.id_cabang).change();
        $(id_pendidikan).val(response.id_pendidikan).change();
        $(status_nikah).val(response.status_nikah).change();
        $(nonpwp).val(response["nonpwp"]);
        $(id_ptkp).val(response.id_ptkp).change();
        $(nobpjskes).val(response["nobpjskes"]);
        $(nobjstk).val(response["nobjstk"]);
        $(no_rekening).val(response["no_rekening"]);
        $(asrekening).val(response["anrekening"]);
        $(email).val(response["email"]);
        $(tempat_lahir).val(response["tempat_lahir"]);
        if(tgl_berlaku_ktp == null){
        $(tgl_lahir).val(response["tgl_lahir"]);
        }else{
        $(tgl_lahir).val(formatDate(new Date(response["tgl_lahir"]), 'dd/MM/yyyy'));
        }
        $(jenis_kelamin).val(response.jenis_kelamin).change();
        $(agama).val(response.agama).change();
        $(golongan_darah).val(response.golongan_darah).change();
        
        $(alamat).val(response["alamat"]);
        $(no_telpon).val(response["no_telpon"]);
        $(hp).val(response["hp"]);
        // $(SelectCabang).val(response.id_cabang).change();
        // $(SelectDepartmen).val(response.id_departemen).change();
        // $(SelectJabatan).val(response.id_jabatan).change();
      });
    };

    var monthNames = [
  "January", "February", "March", "April", "May", "June", "July",
  "August", "September", "October", "November", "December"
];
var dayOfWeekNames = [
  "Sunday", "Monday", "Tuesday",
  "Wednesday", "Thursday", "Friday", "Saturday"
];
function formatDate(date, patternStr){
    if (!patternStr) {
        patternStr = 'M/d/yyyy';
    }
    var day = date.getDate(),
        month = date.getMonth(),
        year = date.getFullYear(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        second = date.getSeconds(),
        miliseconds = date.getMilliseconds(),
        h = hour % 12,
        hh = twoDigitPad(h),
        HH = twoDigitPad(hour),
        mm = twoDigitPad(minute),
        ss = twoDigitPad(second),
        aaa = hour < 12 ? 'AM' : 'PM',
        EEEE = dayOfWeekNames[date.getDay()],
        EEE = EEEE.substr(0, 3),
        dd = twoDigitPad(day),
        M = month + 1,
        MM = twoDigitPad(M),
        MMMM = monthNames[month],
        MMM = MMMM.substr(0, 3),
        yyyy = year + "",
        yy = yyyy.substr(2, 2)
    ;
    // checks to see if month name will be used
    if (patternStr.indexOf('MMM') > -1) {
        patternStr = patternStr
          .replace('MMMM', MMMM)
          .replace('MMM', MMM);
    }
    else {
        patternStr = patternStr
          .replace('MM', MM)
          .replace('M', M);
    }
    return patternStr
      .replace('hh', hh).replace('h', h)
      .replace('HH', HH).replace('H', hour)
      .replace('mm', mm).replace('m', minute)
      .replace('ss', ss).replace('s', second)
      .replace('S', miliseconds)
      .replace('dd', dd).replace('d', day)
      
      .replace('EEEE', EEEE).replace('EEE', EEE)
      .replace('yyyy', yyyy)
      .replace('yy', yy)
      .replace('aaa', aaa)
    ;
}
function twoDigitPad(num) {
    return num < 10 ? "0" + num : num;
}
</script>
@stop
