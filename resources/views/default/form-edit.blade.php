@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li>Home</li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                @if ($tabs > 0)
                <div class="tab-overflow" style="background-color: #242A30">
                    <ul class="nav nav-tabs nav-tabs-inverse">
                        <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                        @foreach ($tabs as $row)
                        <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                        @endforeach
                        <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                    </ul>
                </div>
                @endif
                @else
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">{{ $title }}</h4>
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i>
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @foreach ($fields as $row)
                        {!! $row !!}
                        @endforeach
                        <div class="form-group button-container">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                @foreach ($buttons as $row)
                                {!! $row !!}
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group preloader-container">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                <img src="{{ asset('app/img/icon/preloader.gif') }}" alt="" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-body">
                    <div class="form-group button-container">
                        <div class="col-md-12" style="text-align: right;">
                            <button id="btnAddRow" type="button" class="btn btn-sm btn-primary">
                                Add Row
                            </button>
                        </div>
                    </div>
                </div>
                <!-- <div style="padding: 15px 0 0 15px; font-weight: bold;" id="response_nama_coa">&nbsp;</div> -->
                <form id="formTable">
                <div class="panel-body">
                    <div class="tableFixHead">
                        <table style="margin-bottom: -2px; width: 100%;" class="table display-data-table table-bordered" id="tblAddRow">
                            <thead>
                                <tr>
                                    <th class="detail_header" style="text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Nama Barang</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">No Asset</th>
                                    <th class="detail_header" style="width: 5%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Barang Baru/Bekas</th>
                                    <th class="detail_header" style="width: 5%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Cara Kepemilikan</th>
                                    <th class="detail_header" style="width: 5%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Cabang</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Departemen</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Lokasi/PIC</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Harga</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Serial No</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Photo Asset</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Keterangan</th>
                                    <th class="detail_header" style="text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assets as $i => $value)
                                <tr id="rec-1">
                                    <td style="margin: 0; padding: 0;">
                                        <select class="form-control select-pajak" id="id_barang" name="id_barang[]">
                                            @foreach($id_barang as $val)
                                            @php 
                                                $val        = (object)$val; 
                                                $isSeleted  = $val->id == $value->id_barang ? "selected" : "";
                                            @endphp
                                            
                                            <option value="{{$val->id}}" {{$isSeleted}}>{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <input type="hidden" id="idasset" name="idasset[]" title="" class="detail" style="background-color: #fafafa;" value="{{$value->id_asset}}">
                                        <input type="text" id="no_asset" name="no_asset[]" title="" class="detail" style="background-color: #fafafa;" value="{{$value->no_asset}}">
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <select class="form-control select-pajak" id="kondisi_pembelian" name="kondisi_pembelian[]">
                                            @foreach($kondisi_pembelian as $val)
                                            @php 
                                                $val = (object)$val; 
                                                $isSeleted  = $val->id == $value->kondisi_pembelian ? "selected" : ""; 
                                            @endphp
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <select class="form-control select-pajak" id="status_asset" name="status_asset[]">
                                            @foreach($status_asset as $val)
                                            @php 
                                                $val = (object)$val;  
                                                $isSeleted  = $val->id == $value->status_asset ? "selected" : "";
                                            @endphp
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <select class="form-control select-pajak" id="id_cabang" name="id_cabang[]">
                                            @foreach($id_cabang as $val)
                                            @php 
                                                $val = (object)$val;  
                                                $isSeleted  = $val->id == $value->id_cabang ? "selected" : "";
                                            @endphp
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <select class="form-control select-pajak" id="id_departement" name="id_departement[]">
                                            @foreach($id_departement as $val)
                                            @php 
                                                $val = (object)$val;  
                                                $isSeleted  = $val->id == $value->id_departemen ? "selected" : "";
                                            @endphp
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <select class="form-control select-pajak" id="id_lokasi" name="id_lokasi[]">
                                            @foreach($id_lokasi as $val)
                                            @php 
                                                $val = (object)$val;  
                                                $isSeleted  = $val->id == $value->id_lokasi ? "selected" : "";
                                            @endphp
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <input type="text" id="harga" name="harga[]" title="" class="detail" style="background-color: #fafafa;" value="{{$value->harga_perolehan}}">
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <input type="text" id="serial_no" name="serial_no[]" title="" class="detail" style="background-color: #fafafa;" value="{{$value->serial_no}}">
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <input type="file" id="foto" name="foto[]" title="" class="detail" style="background-color: #fafafa;" >
                                    </td>
                                    <td style="margin: 0; padding: 0;">
                                        <input type="text" id="keterangan" name="keterangan[]" title="" class="detail" style="background-color: #fafafa;" value="{{$value->keterangan}}">
                                    </td>
                                    <td style="margin: 0; padding: 0; text-align: center;">
                                        <a href="javascript:void(0);" id="btnDelRow"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="{{ asset('app/css/grid/sales-add.css') }}" rel="stylesheet" />
<!-- <script type="text/javascript" src="{{ asset('app/js/form_validation.js') }}"></script> -->
<script>
    $('#fotoDokumen').hide();

    $('#id_referensi').on('change', function() {
        var value = this.value;
        if(value  > 0){
            init_karyawan(value);
        }
    //   init_karyawan(this.value, urlSelect);
    });

    function init_karyawan(value){
         var APP_URL = {!! json_encode(url('/')) !!} + '/trans_masuk/getdocref/' + value;

        $.get( APP_URL, function(data){
            var response = JSON.parse(data);
            if(response){
                $('#linkFotoDokumen').attr("href", {!! json_encode(url('/')) !!} + '/app/photo_dokumen/' + response.gambar_referensi)
                $('#fotoDokumen').show();
            }else{
                $('#fotoDokumen').hide();
            }
        });
    }

    // Add row the table
    $('#btnAddRow').on('click', function() {
        // var lastRow = $('#tblAddRow tbody tr:last').html();
        // $('#tblAddRow tbody').append('<tr>' + lastRow + '</tr>');
        // $('#tblAddRow tbody tr:last input').val('');
	    var lenRow		= $('#tblAddRow tbody tr').length;

        var trc = $('#tblAddRow tbody tr:first').clone()
        trc.attr('id', 'rec-'+(lenRow+1));
        trc.find('input').val(''); // clear input
        trc.find('textarea').text(''); // clear textarea
        // $('#tblAddRow tbody tr:last').after(trc);
        trc.appendTo('#tblAddRow tbody');

        return true;
        // $('#id_karyawan').attr('name','id_karyawan[]');
        // console.log('<tr>' + lastRow + '</tr>');
        // var tableRef = document.getElementById('#tblAddRow');
        // var newRow   = tableRef.insertRow(-1);

        // var newCell  = newRow.insertCell(0);
        // var newElem = document.createElement( 'input' );
        // newElem.setAttribute("name", "no_asset[]");
        // newElem.setAttribute("type", "text");
        // newCell.appendChild(newElem);
    });

    $("#tblAddRow").on('click','#btnDelRow',function(){
	    var lenRow		= $('#tblAddRow tbody tr').length;
        
        if(lenRow > 1){
            $(this).parent().parent().remove();
        }
    });

</script>
<script>
    $(".preloader-container").hide();
    $("#myform").submit(function(){
        var flag = "F";
        var no = 1;

        $(".mandatory-input").each(function() {
            var obj = $(this).attr("name");

            if((trim($(this).val()) == "") || (trim($(this).val()) == "0") || (trim($(this).val()) == "__/__/____") || (trim($(this).val()) == "00/00/0000")) {
                $(this).css("border-color", "red");
                $(".style_form_input_" + obj).css("border-color", "red");
                $(".style_form_input_" + obj).css("color", "red");

                if(no == 1) {
                    $(this).focus();
                    no = 2;
                }

                flag = "T";
            } else {
                $(this).css("border-color", "#DDDDDD");
                $(".style_form_input_" + obj).css("border-color", "#DDDDDD");
                $(".style_form_input_" + obj).css("color", "#DDDDDD");
            }
        });
        
        var jumlahkolom = 9;
        var nomor       = 0;
        var noindex     = 0;
        var nameInput   = ["id_barang", "idasset", "no_asset", "kondisi_pembelian", "status_asset", "id_cabang", "id_departement", "id_lokasi", "harga", "serial_no", "foto", "keterangan"];
        $('#formTable :input').not("submit").clone().appendTo("#myform");
        $('#formTable :input').each(function (index) {
            if(nomor > 11){
                nomor =0;
            }
            if(nomor == 10){
                noindex++;
            }
            else if(nomor == 0 || nomor == 3 || nomor == 4 || nomor == 5|| nomor == 6 || nomor == 7){
                $("<input />").attr("type", "hidden").attr("name", nameInput[nomor]+"["+noindex+"]").attr("value", $(this).val()).appendTo("#myform");
            }
            
            nomor++;
        });

        if(flag == "T") {
            return false;
        }

        $(".button-container").hide();
        $(".preloader-container").show();
    });
</script>
@stop