@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Data Nasabah</h4>
                </div> 
                <div class="panel-body panel-form">
                    <div class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-md-2 table_detail_header">No Rekening</label>
                                <div class="col-md-4">
                                    {{ $no_rekening }}
                                </div>
                            <label class="col-md-2 table_detail_header">Tanggal Bayar</label>
                                <div class="col-md-3">
                                    {{ $tanggal_bayar }}
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 table_detail_header">Nama</label>
                                <div class="col-md-4">
                                    {{ $nama_lengkap }}
                                </div>
                            <label class="col-md-2 table_detail_header">Tanggal Real Bayar</label>
                                <div class="col-md-3">
                                    {{ $tanggal_real }}
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 table_detail_header">Nama Cabang</label>
                                <div class="col-md-4">
                                    {{ $nama_cabang }}
                                </div>
                            <label class="col-md-2 table_detail_header">Cara Pembayaran</label>
                                <div class="col-md-3">
                                    {{ $cara_pembayaran }}
                                </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 table_detail_header">Data Kendaraan</label>
                                <div class="col-md-4">
                                    {{ $kendaraan }}
                                </div>
                            <label class="col-md-2 table_detail_header">Angsuran/Bulan</label>
                                <div class="col-md-3">
                                    Rp {{ number_format($angsuran, 0) }}
                                </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Data Angsuran</h4>
                </div> 
                <div class="panel-body panel-form">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="tabel_content_header">Tanggal Cicil</th>
                                <th class="tabel_content_header">Periode</th>
                                <th class="tabel_content_header">Akumulasi</th>
                                <th class="tabel_content_header">Pokok</th>
                                <th class="tabel_content_header">Bunga</th>
                                <th class="tabel_content_header">Denda</th>
                                <th class="tabel_content_header">Total</th>
                                <th class="tabel_content_header">Hari</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php 
                                $akumulasi              = 0;
                                $totalpokok             = 0;
                                $totalbunga             = 0;
                                $totaldenda             = 0;
                                $grandtotal             = 0;
                            @endphp

                            @foreach ($DataAngsuran as $row)
                                @php
                                $kewajiban  = $row->kewajiban_pokok + $row->kewajiban_bunga;
                                $denda      = getNominalDenda($kewajiban, $row->realhari, $mildenda, $mindenda);
                                $total      = $row->kewajiban_pokok + $row->kewajiban_bunga + $denda;
                                $akumulasi  = $akumulasi + $kewajiban + $denda;

                                $totalpokok = $totalpokok + $row->kewajiban_pokok;
                                $totalbunga = $totalbunga + $row->kewajiban_bunga;
                                $totaldenda = $totaldenda + $denda;
                                $grandtotal = $grandtotal + $total;

                                @endphp
                                <tr>
                                    <td style="text-align: center;">{{ $row->tgl_cicil }}</td>
                                    <td style="text-align: center;">{{ $row->periode }}</td>
                                    <td style="text-align: right;">{{ number_format($akumulasi, 0) }}</td>
                                    <td style="text-align: right;">{{ number_format($row->kewajiban_pokok, 0) }}</td>
                                    <td style="text-align: right;">{{ number_format($row->kewajiban_bunga, 0) }}</td>
                                    <td style="text-align: right;">{{ number_format($denda, 0) }}</td>
                                    <td style="text-align: right;">{{ number_format($total, 0) }}</td>
                                    <td style="text-align: center;">{{ $row->realhari }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td style="text-align: center; font-weight: bold;" colspan="3">Total</td>
                                <td style="text-align: right; font-weight: bold;">{{ number_format($totalpokok, 0) }}</td>
                                <td style="text-align: right; font-weight: bold;">{{ number_format($totalbunga, 0) }}</td>
                                <td style="text-align: right; font-weight: bold;">{{ number_format($totaldenda, 0) }}</td>
                                <td style="text-align: right; font-weight: bold;">{{ number_format($grandtotal, 0) }}</td>
                                <td style="text-align: center;">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Data Penerimaan Angsuran</h4>
                </div>
                <div class="panel-body">
                    <form id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post">
                        @csrf
                        @foreach ($fields as $row)
                            {!! $row !!}
                        @endforeach
                        <fieldset>
                            @foreach ($fields_small as $row)
                                {!! $row !!}
                            @endforeach
                        </fieldset>
                        <button type="submit" class="btn btn-sm btn-primary" name="button_save" id="button_save" style="margin-left: 15px;">&nbsp;&nbsp;Save&nbsp;&nbsp;</button>
                        <button type="button" class="btn btn-sm btn-default" name="button_cancel" id="button_cancel" onclick="history.go(-1)">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .table_detail_header {
        margin: 15px 0 10px 0; text-align: left;
    }
    .tabel_content_header {
        text-align: center;
    }
    .table_detail_label {
        padding: 20px; 
    }
    .table_detail_input {
        padding-top: 20px; 
    }
</style>
<script>
$(".first-selected").focus();
$(".first-selected").select();

$("#myform").submit(function(){
    var flag = "F";
    var no = 1;

    $(".mandatory-input").each(function() {
        var obj = $(this).attr("name");

        if((trim($(this).val()) == "") || (trim($(this).val()) == "0") || (trim($(this).val()) == "__/__/____") || (trim($(this).val()) == "00/00/0000")) {
            $(this).css("border-color", "red");
            $(".style_form_input_" + obj).css("border-color", "red");
            $(".style_form_input_" + obj).css("color", "red");

            if(no == 1) {
                $(this).focus();
                no = 2;
            }

            flag = "T";
        } else {
            $(this).css("border-color", "#DDDDDD");
            $(".style_form_input_" + obj).css("border-color", "#DDDDDD");
            $(".style_form_input_" + obj).css("color", "#DDDDDD");
        }
    });

    if(flag == "T") {
        return false;
    }
});
</script>
@stop
