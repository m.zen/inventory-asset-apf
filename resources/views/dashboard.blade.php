@extends('main')
@section('content')
<div id="content" class="content">

@if ($hakAskses == 1 or $hakAskses == 4 )
           <div class="row">
                <!-- begin col-3 -->


                
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-green">
                        <div class="stats-icon"><i class="fa fa-car"></i></div>
                        <div class="stats-info">
                            <h4>TOTAL KARYAWAN</h4>
                            <p>{{ $totalKaryawan }} Orang</p>
                        </div>
                        <div class="stats-link">
                            <a href="{{ url('/karyawan/index')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end col-3 -->
                <!-- begin col-3 -->
                 <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-purple">
                        <div class="stats-icon"><i class="fa fa-gears"></i></div>
                        <div class="stats-info">
                            <h4>KONTRAK AKAN JATUH TEMPO</h4>
                            <p>{{ $totalakankontrak }} Orang</p>
                        </div>
                        <div class="stats-link">
                            <a href="#">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-red">
                        <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
                        <div class="stats-info">
                            <h4>KONTRAK TELAH JATUH TEMPO</h4>
                            <p>{{ $totalkontrak }} Orang</p>
                        </div>
                        <div class="stats-link">
                            <a href="{{ url('/karyawankontrak/jtk_index')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end col-3 -->
                <!-- begin col-3 -->
               
                 <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-purple">
                        <div class="stats-icon"><i class="fa fa-gears"></i></div>
                        <div class="stats-info">
                            <h4>DATA ULANG TAHUN KARYAWAN</h4>
                            <p>{{ $ultah }} Orang</p>
                        </div>
                        <div class="stats-link">
                            <a href="{{ url('/karyawan/ulangtahun_index')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end col-3 -->
            </div>


           
            <!--
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse" data-sortable-id="flot-chart-4">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Summary Data</h4>
                        </div>
                        <div class="panel-body block-grafik">
                            <div id="grafik-tahun" class="height-sm">
                                
                                 @if ($tabs > 0)
                                <div class="tab-overflow" style="background-color: #242A30">
                                    <ul class="nav nav-tabs nav-tabs-inverse">
                                        <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                        @foreach ($tabs as $row)
                                            <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                        @endforeach
                                        <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                                    </ul>
                                </div>
                           
                            </div>

                        </div>


                    </div>
                </div>
            </div>

            @endif

        </div>
      -->
@else
     <div class="row">
                <!-- begin col-3 -->


                
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-green">
                        <div class="stats-icon"><i class="fa fa-book"></i></div>
                        <div class="stats-info">
                            <h4>BIODATA</h4>
                            <br>
                            <br>
                        </div>
                        <div class="stats-link">
                            <a href="{{ url($idKry) }}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
               
                <!-- end col-3 -->
                <!-- begin col-3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-red">
                        <div class="stats-icon"><i class="fa fa-clock-o"></i></div>
                        <div class="stats-info">
                            <h4>DATA ABSENSI</h4>
                           <br>
                            <br>
                        </div>
                        <div class="stats-link">
                            <a href="{{ url('/absensi/index') }}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end col-3 -->
                <!-- begin col-3 -->
                <div class="col-md-4 col-sm-6">
                    <div class="widget widget-stats bg-purple">
                        <div class="stats-icon"><i class="fa fa-gears"></i></div>
                        <div class="stats-info">
                            <h4>PENGAJUAN TIME OFF</h4>
                             <br>
                            <br>
                        </div>

                        <div class="stats-link">
                            <a href="{{ url('/pengajuantm/index') }}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end col-3 -->
            </div>

@endif
</div>
@stop
