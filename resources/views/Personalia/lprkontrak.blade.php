<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>PKWT 2018</title>
  <!-- <style>
     @page { margin: 50px;  }
 
  </style> -->
  <style type="text/css">
        /*@page {
            margin-top: 50px;,
            margin-left: 50px;,
            margin-right: 50px;
            margin-bottom: : 100px;
        }*/
        body {
            /*margin-top: 50px;,*/
             margin-top: 100px;
            margin-left: 50px;
            margin-right: 50px;
            text-align: justify;
            font-family: Tahoma, Verdana, Segoe, sans-serif;
            font-size: 12px;
            /*margin-bottom: : 100px;*/
        }
        /** {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }*/
    </style>
    <style type="text/css">
    @page {
                margin: 75px 75px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 50px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: Gainsboro;
                text-align: left;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 50px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                border-top:1px solid gray;
            }

            .footer2 {
                position: ; 
                bottom: -50px; 
                left: 50px; 
                right: 20px;
                height: 0px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                text-align: right;
                /*border-bottom:1px solid gray;*/
                border-bottom-width: 50px solid gray;
            }
            .pagenum:before { content: counter(page); }
ol.demo {
  /*margin: 0;*/
  padding: 15;
}

#child2 {
  position: absolute;
  line-height: 12px;
  border-bottom: 1px solid red;
  bottom: 60px;
  right: -20px;
  margin-left: none;
  display: inline-block;
}

.page-break {
    page-break-after: always;
}

</style>
</head>
<body>
<header>
            <i><b>RAHASIA<b></i>
</header>

<footer>
                
            <span class="pagenum"></span> | Page <div id="child2">Paraf</div>
</footer>

<h3 align="center">KESEPAKATAN KERJA WAKTU TERTENTU</h3>

@foreach($pkwt as $field)
<p>Kesepakatan Kerja Waktu Tertentu ini (selanjutnya disebut ”Perjanjian”) dibuat dan 
ditandatangani pada tanggal {{date('d', strtotime($field->tgl_awal)) }} {{getMonthName(date('m', strtotime($field->tgl_awal))) }} {{date('Y', strtotime($field->tgl_awal)) }} oleh dan antara :


<table>
<tr><td>I. </td><td>Nama</td><td>:</td><td>PT. BUANA SEJAHTERA MULTIDANA</td></tr>
<tr><td>  </td><td>Alamat</td><td>:</td><td>Grand Slipi Tower Lantai 32, Jln. Letjen S. Parman Kav 22-24 Slipi Palmerah Jakarta Barat 11480</td></tr>
<tr><td>  </td><td>Diwakili</td><td>:</td><td>Yusuf Hamnoer</td></tr>
<tr><td>  </td><td>Jabatan</td><td>:</td><td>Manager HRGA</td></tr>
<tr><td>  </td><td colspan ="3">Selanjutnya dalam perjanjian ini disebut <b>PIHAK PERTAMA</b></td></tr>

<tr><td>II.</td><td>Nama</td><td>:</td><td>{{$field->nama_karyawan}}</td></tr>
<tr><td>  </td><td>Tempat/Tgl Lahir</td><td>:</td><td>{{$field->tempat_lahir}}, {{ date('d', strtotime($field->tgl_lahir)) }} {{getMonthName(date('m', strtotime($field->tgl_lahir))) }} {{ date('Y', strtotime($field->tgl_lahir)) }}</td></tr>
<tr><td>  </td><td>No. KTP</td><td>:</td><td>{{$field->no_ktp}}</td></tr>
<tr><td>  </td><td>Jenis Kelamin</td><td>:</td><td>{{$field->jenis_kelamin}}</td></tr>
<tr><td>  </td><td>Alamat</td><td>:</td><td>{{$field->alamat}}</td></tr>
<tr><td>  </td><td colspan ="3">Selanjutnya dalam perjanjian ini disebut <b>PIHAK KEDUA</b></td></tr>
</table>

<p><b>PIHAK PERTAMA</b> dan <b>PIHAK KEDUA</b> selanjutnya secara bersama-sama disebut “<b>PARA PIHAK</b>”.

<p> Para Pihak telah sepakat dan setuju untuk mengadakan Kesepakatan Kerja Waktu Tertentu
dan <b>PIHAK KEDUA</b> sepakat untuk memenuhi peraturan dan atau tata tertib kerja yang
tercantum dalam Perjanjian Kerja, ini sebagai berikut:

<p>
<h4 align="center">
    PASAL 1<br>
    POKOK KESEPAKATAN DAN JABATAN/TUGAS
</h4>

<p> PIHAK PERTAMA dengan ini menempatkan, dan PIHAK KEDUA dengan ini menerima 
penempatan PIHAK PERTAMA atas dirinya sebagai <b>STAFF ADMIN</b> pada PIHAK PERTAMA 
di <b>PT BUANA SEJAHTERA MULTIDANA – Tegal</b> atau di cabang lain dalam wilayah Republik Indonesia yang akan ditentukan oleh PIHAK PERTAMA dari waktu ke waktu.


<h4 align="center">
PASAL 2<br>
GAJI DAN TUNJANGAN
</h4>
<ol class="demo">
    <li><b>PIHAK PERTAMA</b> memberi gaji dan tunjangan kepada <b>PIHAK KEDUA</b> sesuai dengan Keputusan Direksi.</li>
    <li><b>PIHAK PERTAMA</b> akan membayar kepada <b>PIHAK KEDUA</b> yang terdiri dari komponen sebagai berikut :
        <!-- <ol>
            <li>Gaji Pokok</li>
            <li>Tunjangan Jabatan</li>
            <li>Tunjangan Transport</li>
            <li>Tunjangan Makan</li>
            <li>Tunjangan Kemahalan</li>
            <li>Tunjangan Jaga</li>
        </ol> -->
        <table style="font:  bold;">
            <tr><td>1)</td><td><b>Gaji Pokok</b></td><td colspan="2">: <b>Rp. {{$field->gaji_pokok}},-</b></td></tr>
            <tr><td>2)</td><td><b>Tunjangan Jabatan</b></td><td colspan="2">: <b>Rp. {{$field->tunj_jabatan}},-</b></td></tr>
            <tr><td>3)</td><td><b>Tunjangan Transport</b></td><td colspan="2">: <b>Rp. {{$field->tunj_transport}},-</b></td></tr>
            <tr><td>4)</td><td><b>Tunjangan Makan</b></td><td colspan="2">: <b>Rp. {{$field->tunj_makan}},-/hari/kehadiran</b></td></tr>
            <tr><td>5)</td><td><b>Tunjangan Kemahalan</b></td><td colspan="2">: <b>Rp. {{$field->tunj_kemahalan}},-</b></td></tr>
            <tr><td>6)</td><td><b>Tunjangan Jaga</b></td><td colspan="2">: <b>Rp. {{$field->tunj_jaga}},-</b></td></tr>
        </table>
    </li>
    <li>Gaji dan tunjangan tersebut akan dibayarkan <b>PIHAK PERTAMA</b> selambat-lambatnya 
        pada hari terakhir bulan berjalan dengan cara mentransfer ke rekening <b>PIHAK KEDUA</b>
        di bank yang ditunjuk oleh <b>PIHAK PERTAMA</b>. Jika tanggal terakhir jatuh di hari libur maka 
        gaji akan dibayarkan pada hari kerja terakhir sebelumnya.</li>
</ol>

<!-- <table>
    <tr><td>1)</td><td>Gaji Pokok</td><td>:</td><td>Rp. 1.550.000,-</td></tr>
    <tr><td>2)</td><td>Tunjangan Jabatan</td><td>:</td><td>Rp.  - </td></tr>
    <tr><td>3)</td><td>Tunjangan Transport</td><td>:</td><td>Rp.  - </td></tr>
    <tr><td>4)</td><td>Tunjangan Makan</td><td>:</td><td>Rp. 18.000,-/hari/kehadiran</td></tr>
    <tr><td>5)</td><td>Tunjangan Kemahalan</td><td>:</td><td>Rp.  -</td></tr>
    <tr><td>6)</td><td>Tunjangan Jaga</td><td>:</td><td>Rp.  -</td></tr>
</table>

<p> 3.  Gaji dan tunjangan tersebut akan dibayarkan <b>PIHAK PERTAMA</b> selambat-lambatnya 
pada hari terakhir bulan berjalan dengan cara mentransfer ke rekening <b>PIHAK KEDUA</b>
di bank yang ditunjuk oleh PIHAK PERTAMA. Jika tanggal terakhir jatuh di hari libur maka 
gaji akan dibayarkan pada hari kerja terakhir sebelumnya. 
 -->
<h4 align="center">
    PASAL 3<br>
    WAKTU KERJA
</h4>

<br><b>PIHAK KEDUA</b> akan mencurahkan seluruh kemampuan dan waktunya pada saat waktu kerja
untuk kepentingan <b>PIHAK PERTAMA</b> dan berusaha sebaik mungkin untuk meningkatkan 
kemampuannya tersebut demi kepentingan PIHAK PERTAMA. Baik pada waktu kerja 
maupun diluar waktu kerja <b>PIHAK PERTAMA</b>, <b>PIHAK KEDUA</b> tidak akan bekerja untuk 
kepentingan pihak lain dengan cara dan bentuk yang bagaimanapun juga kecuali 
bilamana mendapat ijin tertulis sebelumnya dari <b>PIHAK PERTAMA</b>. Kecuali ditentukan lain 
oleh <b>PIHAK PERTAMA</b>, jam kerja <b>PIHAK KEDUA</b> adalah sama dengan jam kerja <b>PIHAK KEDUA</b>
Tetap <b>PIHAK PERTAMA</b>, satu dan lain menunjuk ketentuan dalam peraturan <b>PIHAK PERTAMA</b> yang ada.


<h4 align="center">
    PASAL 4<br>
    TATA TERTIB DAN DISIPLIN
</h4>

<ol class="demo">
    <li>
        <b>PIHAK KEDUA</b> senantiasa akan mencurahkan segala kemampuan dan perhatiannya untuk kepentingan tugasnya sampai pada batas maksimal kecakapan dan keahlian yang dimilikinya dan melakukan segala daya upaya dengan inisiatif dan seefektif mungkin dengan selalu mengingat tata tertib dan peraturan serta kebijakan <b>PIHAK PERTAMA</b> yang berlaku dari waktu ke waktu. <br><br>
    </li>

    <li>
        <b>PIHAK KEDUA</b> senantiasa akan menjalankan tugasnya dengan hati-hati, jujur, tidak akan menerima/meminta pemberian material ataupun jasa dalam bentuk apapun juga dari pihak ketiga yang secara langsung maupun tidak langsung ada hubungan dengan sifat pekerjaan/tugasnya.  <br><br>
    </li>
    <li>
        Tanpa seijin <b>PIHAK PERTAMA</b>, <b>PIHAK KEDUA</b> tidak akan menyimpan atau memperlihatkan/memperdengarkan, memberitahukan kepada pihak ketiga atau membawa keluar dari gedung atau lingkungan usaha <b>PIHAK PERTAMA</b>; asli, rekaman, salinan dan/atau catatan-catatan lain semacam itu dan alat-alat, mesin serta perlengkapan kerja dalam arti kata seluas-luasnya yang menyangkut kepentingan <b>PIHAK PERTAMA</b>. <br><br>
    </li>
    <li>
        Pada akhir Perjanjian ini, semua barang-barang yang termaksud di atas yang dengan seijin <b>PIHAK PERTAMA</b> berada ditangan PIHAK KEDUA harus dikembalikan dengan segera oleh <b>PIHAK KEDUA</b> sendiri kepada <b>PIHAK PERTAMA</b> dalam keadaan baik seperti pada waktu pertama kali dipinjamkan kepada <b>PIHAK KEDUA</b>. Kelusuhan karena pemakaian yang wajar dapat diterima. Segala hasil kerja <b>PIHAK KEDUA</b> selama perjanjian ini berlaku, menjadi hak dan milik <b>PIHAK PERTAMA</b> tanpa dapat diganggu gugat lagi.  <br><br>
    </li>
    <li>
        <b>PIHAK KEDUA</b> senantiasa akan memegang teguh rahasia <b>PIHAK PERTAMA</b>. Rahasia <b>PIHAK PERTAMA</b>  yang harus dijaga oleh <b>PIHAK KEDUA</b> tersebut termasuk tetapi tidak terbatas pada cara kerja <b>PIHAK PERTAMA</b> dalam sektor manajemen, produksi, jasa, teknik (termasuk proses, formula, design, penemuan, sistim, hasil riset, keuangan dan transaksi-transaksi atau kontrak-kontrak). Kewajiban ini akan tetap berlaku terus meskipun Perjanjian ini telah berakhir/diakhiri. <br><br>
    </li>

    <li>
        <b>PIHAK KEDUA</b> wajib mematuhi dan menjalankan semua peraturan-peraturan yang berlaku dalam <b>PIHAK PERTAMA</b> baik yang sekarang telah ada maupun yang akan dikeluarkan sepanjang peraturan itu tidak bertentangan dengan Perjanjian ini. <br><br>
    </li>
</ol>


<h4 align="center">PASAL 5<br>JANGKA WAKTU KESEPAKATAN KERJA</h4>

<ol class="demo">
    <li>
        Perjanjian Kerja ini disusun dan dibuat untuk dilaksanakan selama <b>12 bulan</b>, terhitung dari tanggal <b>{{ date('d', strtotime($field->tgl_awal)) }} {{getMonthName(date('m', strtotime($field->tgl_awal))) }} {{ date('Y', strtotime($field->tgl_awal)) }}</b>  dan berakhir pada tanggal  s.d. <b>{{ date('d', strtotime($field->tgl_akhir)) }} {{getMonthName(date('m', strtotime($field->tgl_akhir))) }} {{ date('Y', strtotime($field->tgl_akhir)) }}</b>. Kecuali bila <b>PIHAK PERTAMA</b> karena sebab-sebab yang diatur dalam perjanjian ini mengakhiri perjanjian ini. <br><br>
    </li>
    <li>
        Bilamana <b>PIHAK PERTAMA</b> menganggap prestasi kerja dan dedikasi <b>PIHAK KEDUA</b> terhadap <b>PIHAK PERTAMA</b> memuaskan, <b>PIHAK PERTAMA</b> dapat memperpanjang perjanjian ini untuk jangka waktu dan syarat-syarat yang akan ditetapkan kemudian. Untuk maksud tersebut, <b>PIHAK PERTAMA</b> akan memberitahukannya secara lisan atau tertulis kepada <b>PIHAK KEDUA</b> selambat-lambatnya 30 (tigapuluh) hari sebelum jangka waktu perjanjian ini berakhir. Bila karena sebab dan alasan apapun <b>PIHAK PERTAMA</b> tidak memberitahukan sesuatu hal yang menyangkut perpanjangan perjanjian ini, sedangkan jangka waktu 30 (tigapuluh) hari sebelum berakhir perjanjian ini telah lampau, maka harus dianggap bahwa <b>PIHAK PERTAMA</b> tidak akan memperpanjang Perjanjian ini, dan karenanya Perjanjian ini berakhir sesuai dengan ketentuan <br><br>
    </li>
    <li><b>PIHAK PERTAMA</b> berhak mengakhiri Perjanjian ini sebelum jangka waktu Perjanjian ini berakhir karena alasan-alasan sebagai berikut :
        <ol type="a">
            <li>
                <b>PIHAK KEDUA</b> lalai, menolak untuk mematuhi salah satu tugas/kewajibannya yang diberikan oleh PIHAK PERTAMA atau atasannya yang berwenang.
            </li>
            <li>
                <b>PIHAK KEDUA</b> tidak mampu memenuhi tugas/kewajibannya yang diberikan oleh <b>PIHAK PERTAMA</b> atau atasannya yang berwenang dan tetap tidak mampu untuk memperbaiki prestasi kerjanya yang tidak memuaskan <b>PIHAK PERTAMA</b>.
            </li>
            <li>
                <b>PIHAK KEDUA</b> melakukan perbuatan-perbuatan yang menurut undang-undang yang berlaku merupakan perbuatan-perbuatan yang dapat dijadikan alasan kuat atau mendesak bagi <b>PIHAK PERTAMA</b> untuk memutuskan hubungan kerja.
            </li>
        </ol>
    </li>
    <li>
        Perjanjian ini berakhir dengan sendirinya dalam hal :
        <ol type="a">
            <li>
                Jangka waktu perjanjian ini sebagaimana diatur dalam pasal 5.1 perjanjian ini telah berakhir. 
            </li>
            <li>
                <b>PIHAK KEDUA</b> meninggal dunia.
            </li>
        </ol>
         <br><br>
    </li>
    <li>
        Dalam hal berakhirnya Perjanjian ini  adalah karena alasan-alasan tersebut dalam pasal 5.3. dan 5.4. di atas, maka <b>PIHAK KEDUA</b> tidak berhak untuk menuntut pada <b>PIHAK PERTAMA</b> dan  <b>PIHAK PERTAMA</b> tidak diwajibkan untuk membayar pada <b>PIHAK KEDUA</b> uang pesangon, uang jasa, kompensasi/ganti rugi dalam bentuk dan jumlah berapapun juga, kecuali gaji <b>PIHAK KEDUA</b> pada bulan yang bersangkutan sampai dengan tanggal berakhirnyanya hubungan kerja. <br><br>
    </li>
    <li>
        Untuk maksud  perjanjian ini maka kedua belah pihak sekarang dan untuk nantinya dengan ini melepaskan dan menyatakan sebagai tidak berlakunya ketentuan-ketentuan yang tercantum dalam pasal 1266 dan 1267 Kitab  Undang-Undang Hukum Perdata.  <br><br>
    </li>
</ol>

<p>
<h4 align="center">PASAL 6<br>PENYESUAIAN DAN PERUBAHAN PERJANJIAN</h4>

<ol class="demo">
    <li>
        Tanpa mengurangi ketentuan-ketentuan Perjanjian ini maka semua aturan kerja dan tata tertib <b>PIHAK PERTAMA</b> yang diberlakukan kepada <b>PIHAK KEDUA</b> Tetap <b>PIHAK PERTAMA</b> dari waktu ke waktu akan berlaku terhadap <b>PIHAK KEDUA</b>, meskipun secara tegas diakui oleh kedua belah pihak bahwa <b>PIHAK KEDUA</b> bukan merupakan <b>PIHAK KEDUA</b> Tetap <b>PIHAK PERTAMA</b>. <br><br>
    </li>
    <li>
        Untuk merubah atau meninjau kembali Perjanjian ini baik untuk sebagian maupun seluruhnya harus didasarkan pada kesepakatan kedua belah pihak yang dibuat secara tertulis, kecuali ditentukan lain dalam perjanjian ini atau disepakati kedua belah pihak secara lain. <br><br>
    </li>
    <li>
        Jika dipandang perlu oleh <b>PIHAK PERTAMA</b>, <b>PIHAK PERTAMA</b> berhak untuk menempatkan <b>PIHAK KEDUA</b> pada posisi atau jabatan lain selain yang tercantum pada Perjanjian ini sesuai dengan keperluan <b>PIHAK PERTAMA</b>. <br><br>
    </li>
    <li>
        Hal-hal yang tidak atau belum cukup diatur dalam Perjanjian ini akan ditetapkan berdasarkan kesepakatan kedua pihak yang dibuat secara tertulis. Kecuali ditentukan lain berdasarkan persetujuan kedua belah pihak, maka perjanjian ini merupakan satu-satunya dokumen yang memuat semua janji dan perikatan antara <b>PIHAK PERTAMA</b> dan <b>PIHAK KEDUA</b> dan karenanya semua janji dan perikatan baik yang dibuat secara lisan maupun tertulis oleh dan diantara kedua belah pihak sebelum tanggal Perjanjian ini menjadi tidak berlaku lagi. <br><br>
    </li>
</ol>


<h4 align="center">PASAL 7<br>DOMISILI</h4>

<p>Perjanjian ini, pelaksanaannya serta segala akibatnya akan diatur menurut Hukum Indonesia dan untuk tujuan tersebut kedua pihak sepakat untuk memilih domisili hukum yang tetap dan tidak berubah pada kantor kepaniteraan Pengadilan Negeri Setempat

<p>Demikian Perjanjian ini dibuat pada hari dan tanggal tersebut di atas, untuk dilaksanakan sebagaimana mestinya.

<!-- <p>PT. Buana Sejahtera Multidana,
<p>PIHAK PERTAMA
<div style="position:100px;">
PIHAK KEDUA
</div>

<b>Yusuf Hamnoer</b>   DEVI ARTI SANDI
Manager HRGA    -->                                                                                              
<br><br><br>
<table width="100%">
    <tr>
        <td align="left">PT. Buana Sejahtera Multidana,</td>
        <td align="right"></td>
    </tr>
    <tr>
        <td align="left">PIHAK PERTAMA</td>
        <td align="right">PIHAK KEDUA</td>
    </tr>
</table>
<br><br><br>
<table width="100%">
    <tr>
        <td align="left"><b><u>Yusuf Hamnoer<u></b></td>
        <td align="right"> <b>{{$field->nama_karyawan}}</b></td>
    </tr>
    <tr>
        <td align="left"><b>Manager HRGA</b></td>
        <td align="right"></td>
    </tr>
</table>
@endforeach
<!-- <div class="signature" align="right">
    London,&nbsp;8 January 2017<br>
    Chelsea Player<br><br><br><br>
    Eden Hazard<br>
</div> -->

</body>
</html>