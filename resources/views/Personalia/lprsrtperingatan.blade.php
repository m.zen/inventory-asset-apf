<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Surat Peringatan</title>
  <!-- <style>
     @page { margin: 50px;  }
 
  </style> -->
  <style type="text/css">
        /*@page {
            margin-top: 50px;,
            margin-left: 50px;,
            margin-right: 50px;
            margin-bottom: : 100px;
        }*/
        body {
            margin-top: 113px;
            margin-left: 80px;
            margin-right: 80px;
            text-align: justify;
            font-family: Tahoma, Verdana, Segoe, sans-serif;
            font-size: 12px;
            /*margin-bottom: : 100px;*/
        }
        table{
            margin: none;
           
            text-align: justify;
            vertical-align: text-top;
        }
        .table2 {
             border-collapse: collapse;
        }

        .th2 {
                border: 1px solid black;
            }
        /** {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }*/
    </style>
    
</head>
<body>
 @foreach($pkwt as $row )
<h3 align="center"><u>SURAT PERINGATAN 1</u><br>{{$row->no_surat}}</h3>

<table >
<tr ><td style="vertical-align: text-top;">Menimbang</td> <td style="vertical-align: text-top;">:</td><td colspan="2">Bahwa dalam rangka tertib Administratif Kepegawaian, penerapan Standar Operasional Prosedur dan penerapan Standar Kinerja terhadap seluruh karyawan PT. Buana Sejahtera Multidana <br><br></td></tr>
<tr><td style="vertical-align: text-top;">Mengingat</td> <td style="vertical-align: text-top;">:</td><td colspan="2">Tata Tertib Kepegawaian, Standar Operasional Prosedur dan Standar Kinerja yang berlaku di PT. Buana Sejahtera Multidana.</td></tr>

<tr><td colspan="3">
<h3 align="center">MEMUTUSKAN</h3>
</td>
</tr>
<tr><td style="vertical-align: text-top;">Pertama</td> <td style="vertical-align: text-top;"> :</td><td colspan="3">Menyatakan bahwa,<br><br></td></tr>
<tr><td> </td><td></td><td>Nama</td><td colspan="2"> : {{$row->nama_karyawan}}</td></tr>
<tr><td> </td><td></td><td>Jabatan</td><td colspan="2"> : {{$row->nama_jabatan}}</td></tr>
<tr><td> </td><td></td><td>NIK</td><td colspan="2"> : {{$row->nik}}</td></tr>
<tr><td> </td><td></td><td>Kantor</td><td colspan="2"> : {{$row->nama_cabang}}<br><br></td></tr>
<tr><td> </td><td></td><td colspan="3">Telah melanggar standar kinerja yang berlaku di PT. Buana Sejahtera Multidana yaitu : “Melakukan pelanggaran Pasal.25 Point.a ayat.1” :<br><br></td></tr>
<tr><td> </td><td></td><td colspan="3">“Tidak hadir bekerja 10 (sepuluh) hari tanpa keterangan dalam Periode 16 September – 15 Oktober 2018.”<br><br></td></tr>

<tr><td>Kedua</td> <td>:</td><td colspan="3">Memberikan PERINGATAN PERTAMA kepada yang bersangkutan.<br><br></td></tr>
<tr><td>Ketiga</td> <td>:</td><td colspan="3">Memberikan kesempatan kepada yang bersangkutan untuk melakukan perubahan-perubahan dalam pelaksanaan Standar Kinerja yang berlaku di perusahaan.<br><br></td></tr>
<tr><td>keempat</td> <td>:</td><td colspan="3">Masa berlaku dari Surat Peringatan ini adalah 3 bulan, terhitung dari  24/10/2018 s.d. 24/01/2019.<br><br></td></tr>
<tr><td>Ditetapkan di</td> <td>:</td><td colspan="3">Jakarta</td></tr>
<tr><td>Pada Tanggal</td> <td>:</td><td colspan="3">{{$row->tgl_berlaku}}<br><br></td></tr>

</table>

<table class="table2" width="100%" border=1 >
    <tr  align="center" >
        <th class="th2">Dikeluarkan</th>
        <th class="th2">Diberikan</th>
        <th class="th2">Telah Mengerti dan Menerima</th>
    </tr>
   <tr align="center">
        <td class="th2"></td>
        <td class="th2"></td>
        <td class="th2"><br><br><br><br><br><br></td>
    </tr>
    <tr align="center">
        <td class="th2">HRD Departemen</td>
        <td class="th2">Direktur Utama</td>
        <td class="th2">Karyawan</td>
    </tr>
</table>


@endforeach
<!-- Tembusan surat keputusan ini disampaikan kepada Yth :
<ol>
    <li>Direksi PT. Buana Sejahtera Multidana</li>
    <li>HRGA Dept.</li>
    <li>Collection Dept.</li>
    <li>-</li>
    <li>-</li>
</ol> -->

</body>
</html>