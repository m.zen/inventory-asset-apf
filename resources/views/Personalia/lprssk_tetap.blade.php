<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>SK Tetap 2018</title>
  <!-- <style>
     @page { margin: 50px;  }
 
  </style> -->
  <style type="text/css">
        /*@page {
            margin-top: 50px;,
            margin-left: 50px;,
            margin-right: 50px;
            margin-bottom: : 100px;
        }*/
        body {
            /*margin-top: 50px;,*/
            margin-left: 50px;,
            margin-right: 50px;
            text-align: justify;
            /*margin-bottom: : 100px;*/
        }
        /** {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }*/
    </style>
    <style type="text/css">
    @page {
                margin: 100px 50px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 50px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: Gainsboro;
                text-align: left;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 50px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                border-top:1px solid gray;
            }

            .footer2 {
                position: ; 
                bottom: -50px; 
                left: 50px; 
                right: 20px;
                height: 0px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                text-align: right;
                /*border-bottom:1px solid gray;*/
                border-bottom-width: 50px solid gray;
            }
            .pagenum:before { content: counter(page); }
ol.demo {
  /*margin: 0;*/
  padding: 15;
}

#child2 {
  position: absolute;
  line-height: 12px;
  border-bottom: 1px solid red;
  bottom: 60px;
  right: -20px;
  margin-left: none;
  display: inline-block;
}

.page-break {
    page-break-after: always;
}
table{
    margin: none;
    vertical-align: top;
}

</style>
</head>
<body>
@foreach($pkwt as $field )
<h3 align="center"><u>SURAT KEPUTUSAN PT. BUANA SEJAHTERA MULTIDANA</u><br>{{$field->no_surat}}</h3>

<table>
<tr><td >Menimbang</td> <td>:</td><td>Bahwa dalam rangka menunjang operasional PT. BUANA SEJAHTERA MULTIDANA</td><td></td></tr>
<tr><td>Mengingat</td> <td>:</td><td>Hasil Keputusan Direksi PT. BUANA SEJAHTERA MULTIDANA</td><td></td></tr>
</table>

 
<h3 align="center">MEMUTUSKAN</h3><br>Menetapkan
<table >

<tr><td>Pertama</td> <td> :</td><td colspan="3">Melakukan Pengangkatan Karyawan Tetap sesuai dengan ketentuan Undang-Undang No.13 Tahun 2003 Pasal 63 Ayat (2) kepada :</td></tr>
<tr><td> </td><td></td><td>Nama</td><td colspan="2"> : {{$field->nama_karyawan}}</td></tr>
<tr><td> </td><td></td><td>NIK</td><td colspan="2"> : {{$field->nik}}</td></tr>
<tr><td> </td><td></td><td>Alamat</td><td colspan="2"> : {{$field->alamat}}</td></tr>
<tr><td> </td><td></td><td>Tanggal Masuk</td><td colspan="2"> : {{$field->tgl_masuk}}</td></tr>

<tr><td> </td><td></td><td>Lokasi Kerja</td><td colspan="2"> : {{$field->nama_cabang}}</td></tr>
<tr><td> </td><td></td><td>Jabatan</td><td colspan="2"> : {{$field->nama_jabatan}}</td></tr>
<tr><td> </td><td></td><td>Upah Per Bulan</td><td colspan="2"> : Rp. {{setRupiah($field->gaji_pokok)}}</td></tr>

<tr><td>Kedua</td> <td>:</td><td colspan="3">Bertanggung jawab kepada DIREKTUR COLLECTION</td></tr>
<tr><td>Ketiga</td> <td>:</td><td colspan="3">Hal-hal yang belum diatur dalam Surat Keputusan ini akan ditetapkan sendiri.</td></tr>
<tr><td>keempat</td> <td>:</td><td colspan="3">Surat keputusan ini berlaku efektif pada tanggal 18 September 2018</td></tr>

</table>
<div class="signature">
    Ditetapkan di : Jakarta<br>
    Pada Tanggal : {{$field->tgl_awal}}<br><br><br><br>
    <u>Herry Mulyadi</u><br>
    Direktur Utama
</div>
Tembusan surat keputusan ini disampaikan kepada Yth :
<ol>
    <li>Direksi PT. Buana Sejahtera Multidana</li>
    <li>HRGA Dept.</li>
    <li>Collection Dept.</li>
    <li>-</li>
    <li>-</li>
</ol>
@endforeach
</body>
</html>