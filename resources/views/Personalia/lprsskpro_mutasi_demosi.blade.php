<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>SK Promosi Mutasi dan Demosi</title>
  <!-- <style>
     @page { margin: 50px;  }
 
  </style> -->
  <style type="text/css">
        /*@page {
            margin-top: 50px;,
            margin-left: 50px;,
            margin-right: 50px;
            margin-bottom: : 100px;
        }*/
        body {
            /*margin-top: 50px;,*/
            margin-left: 50px;,
            margin-right: 50px;
            text-align: justify;
            /*margin-bottom: : 100px;*/
        }
        /** {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }*/
    </style>
    <style type="text/css">
    @page {
                margin: 100px 100px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 50px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: Gainsboro;
                text-align: left;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 50px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                border-top:1px solid gray;
            }

            .footer2 {
                position: ; 
                bottom: -50px; 
                left: 50px; 
                right: 20px;
                height: 0px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                text-align: right;
                /*border-bottom:1px solid gray;*/
                border-bottom-width: 50px solid gray;
            }
            .pagenum:before { content: counter(page); }
ol.demo {
  /*margin: 0;*/
  padding: 15;
}

#child2 {
  position: absolute;
  line-height: 12px;
  border-bottom: 1px solid red;
  bottom: 60px;
  right: -20px;
  margin-left: none;
  display: inline-block;
}

.page-break {
    page-break-after: always;
}
table{
    margin: none;
    vertical-align: bottom;
}

</style>
</head>
<body>
@foreach($karir as $field)
<table >
<tr><td colspan="4" align="center"><h3 align="center"><u>SURAT KEPUTUSAN DIREKSI PT. ARTHA PRIMA FINANCE</u><br>No : {{$field->no_surat}}</h3>
</td></tr> 

<tr><td colspan="4" align="center">TENTANG</br></br></td></tr> 
<tr><td colspan="4" align="center"><B>{{$field->nama_karir}} Sdr. {{$field->nama_karyawan}}</B></td></tr> 
<tr><td colspan="4"align="center"><B>DIREKSI PT. ARTHA PRIMA FINANCE</br></br></B></td></tr> 
<tr><td style="vertical-align: text-top;width: 5px;">Menimbang</td> <td style=" width: : 1px;">:</td><td colspan=2>Bahwa dalam rangka  menunjang Operasional PT. ARTHA PRIMA FINANCE</td></tr>
<tr><td style="vertical-align: text-top;width: 5px;">Mengingat</br></br></td> <td>:</br></br></td><td  colspan=2>Hasil Keputusan Management PT. ARTHA PRIMA FINANCE</br></td></tr>
<tr><td colspan="4"align="center"><h3 align="center">MEMUTUSKAN</h3></td></tr> 
<tr><td>Menetapkan</td> <td>:</td><td></td><td></td></tr>
<tr><td>Pertama</td> <td>:</td><td>
@if ($field->nama_karir=="PROMOSI")
    Mengangkat
@elseif ($field->nama_karir=="MUTASI")
    Memutasikan
@else
    Menurunkan
@endif
</td><td></td></tr>
<tr><td> </td><td></td><td style="vertical-align: text-top;width: 2px;">Nama</td><td>: {{$field->nama_karyawan}}</td></tr>
<tr><td> </td><td></td><td>Departemen</td><td>: {{$field->nama_departemenawal}}</td></tr>
<tr><td> </td><td></td><td>Jabatan</td><td>: {{$field->nama_jabatanawal}}</td></tr>
<tr><td> </td><td></td><td>Kantor</br></br></td><td>: {{$field->nama_cabangawal}}</br></br></td></tr>

<tr><td> </td><td></td><td><b>Sebagai</b></br></br></td><td></td></tr>
<tr><td> </td><td></td><td>Jabatan</td><td>: {{$field->nama_jabatanbaru}}</td></tr>
<tr><td> </td><td></td><td>Kantor</td><td>: {{$field->nama_cabangbaru}}</td></tr>

<tr><td style="vertical-align: text-top;">Kedua</td> <td style="vertical-align: text-top;">:</td><td colspan="2" style="vertical-align: text-top;">Bertanggung jawab kepada {{$field->nama_jabatantj}}</td></tr>
<tr><td style="vertical-align: text-top;">Ketiga</td> <td style="vertical-align: text-top;">:</td><td colspan="2" style="vertical-align: text-top;">Hal-hal yang belum diatur dalam Surat Keputusan ini akan ditetapkan sendiri.</td></tr>
<tr><td>Keempat</td> <td style="vertical-align: text-top;">:</td><td colspan="2" style="vertical-align: text-top;">Surat Keputusan ini berlaku mulai tanggal {{$field->tgl_karir}} sampai dengan adanya keputusan lanjutan yang mencabut atau merevisi keputusan ini.</td></tr>

</table>


<br>
<div class="signature">
    Ditetapkan di   : Jakarta<br>
    Pada Tanggal: {{$field->tgl_karir}}<br><br><br>
    <u>IVAN YUNANTO</u><br>
    Direktur
</div>
<br>
<div class="signature">
    <b><i>Tembusan</i></b>  :<br>
    <ul>
        @if ($field->tembusan1<>"")
            <li>{{$field->tembusan1}}</li>
        @endif
         @if ($field->tembusan2<>"")
            <li>{{$field->tembusan2}}</li>
        @endif
         @if ($field->tembusan3<>"")
            <li>{{$field->tembusan3}}</li>
        @endif
         @if ($field->tembusan4<>"")
            <li>{{$field->tembusan4}}</li>
        @endif
         @if ($field->tembusan5<>"")
            <li>{{$field->tembusan5}}</li>
        @endif



    </ul>
    



</div>


@endforeach
</body>
</html>