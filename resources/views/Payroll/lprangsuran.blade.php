<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Data Angsuran</title>
  <!-- <style>
     @page { margin: 50px;  }
 
  </style> -->
  <style type="text/css">
        /*@page {
            margin-top: 50px;,
            margin-left: 50px;,
            margin-right: 50px;
            margin-bottom: : 100px;
        }*/
        body {
            /*margin-top: 50px;,*/
            margin-left: 50px;,
            margin-right: 50px;
            text-align: justify;
            /*margin-bottom: : 100px;*/
        }
        /** {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }*/
    </style>
    <style type="text/css">
    @page {
                margin: 75px 50px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 50px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: Gainsboro;
                text-align: left;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 50px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                border-top:1px solid gray;
            }

            .footer2 {
                position: ; 
                bottom: -50px; 
                left: 50px; 
                right: 20px;
                height: 0px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                text-align: right;
                /*border-bottom:1px solid gray;*/
                border-bottom-width: 50px solid gray;
            }
            .pagenum:before { content: counter(page); }
ol.demo {
  /*margin: 0;*/
  padding: 15;
}

#child2 {
  position: absolute;
  line-height: 12px;
  border-bottom: 1px solid red;
  bottom: 60px;
  right: -20px;
  margin-left: none;
  display: inline-block;
}

.page-break {
    page-break-after: always;
}
.table{
    margin: none;
    vertical-align: bottom;
    font-size:12px;

}

.table2, .th2{
  border-collapse: collapse;
  border: 1px solid black;
  font-size:12px;
}
.td2{
  border-right: 1px solid black;
  border-left: 1px solid black;
  border-collapse: collapse;
  padding-right: 10px;
  white-space: nowrap;
  font-size:12px;

}
</style>
</head>
<body>
 @foreach($pinjaman as $row )
<h3 align="center"><u>DAFTAR ANGSURAN PINJAMAN</u></h3>

</br>
</br>
</br>
<table >
<tr><td colspan="3"><u>DATA KARYAWAN</u></td></tr>
<tr><td>NIK</td><td> : </td><td>{{$row->nik}}</td></tr>
<tr><td>Nama</td><td> : </td><td>  {{$row->nama_karyawan}}</td></tr>
<tr><td>Kantor</td><td> : </td><td > {{$row->nama_cabang}}</td></tr>
<tr><td colspan="3"><br><br><u>DATA PINJAMAN</u> </td></tr>
<tr><td>Bulan Pinjam</td><td> : </td><td>{{$row->tgl_pinjaman}}</td></tr>
<tr><td>Jumlah Pinjaman</td><td> : </td><td> {{number_format($row->jumlah_pinjaman)}}</td></tr>
<tr><td>Bunga</td><td> : </td><td >  {{number_format($row->bunga_pinjaman,2)}}%</td></tr>
<tr><td>Tenor</td><td> : </td><td > {{$row->tenor}}</td></tr>
</table>
@endforeach


<h3>DAFTAR ANGSURAN</h3>
<table class="table2" width="100%" height="100%">
    <tr  align="center" >
        <th class="th2">Cicilan ke</th>
        <th class="th2">Bulan</th>
        <th class="th2">Angsuran</th>
        <th class="th2">Bayar Angsuran</th>
        <th class="th2">Saldo Angsuran</th>
        <th class="th2">Saldo Pokok</th>
        <th class="th2">Saldo Bunga</th>
        
    </tr>
     @foreach($angsuran as $row )
    <tr align="right">
       <td class="td2">{{$row->cicilanke}}.</td>
        <td class="td2", nowrap>{{$row->bulanangsur}} - {{$row->tahunangsur}} </td>
        <td class="td2">{{number_format($row->jml_angsuran)}}</td>
        <td class="td2">{{number_format($row->bayar_angsuran)}}</td>
        <td class="td2">{{number_format($row->saldo_angsuran)}}</td>
        <td class="td2">{{number_format($row->saldo_pokok)}}</td>
        <td class="td2">{{number_format($row->saldo_bunga)}}</td>
        
    </tr>
    @endforeach
</table>



<!-- Tembusan surat keputusan ini disampaikan kepada Yth :
<ol>
    <li>Direksi PT. Buana Sejahtera Multidana</li>
    <li>HRGA Dept.</li>
    <li>Collection Dept.</li>
    <li>-</li>
    <li>-</li>
</ol> -->

</body>
</html>