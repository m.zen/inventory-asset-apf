<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Data Angsuran</title>
  <!-- <style>
     @page { margin: 50px;  }
 
  </style> -->
  <style type="text/css">
        /*@page {
            margin-top: 50px;,
            margin-left: 50px;,
            margin-right: 50px;
            margin-bottom: : 100px;
        }*/
        body {
            /*margin-top: 50px;,*/
            margin-left: 50px;,
            margin-right: 50px;
            text-align: justify;
            /*margin-bottom: : 100px;*/
        }
        /** {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }*/
    </style>
    <style type="text/css">
    @page {
                margin: 75px 50px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 50px;
                right: 0px;
                height: 50px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: Gainsboro;
                text-align: left;
                line-height: 35px;
            }

            footer {
                position: fixed; 
                bottom: -60px; 
                left: 50px; 
                right: 0px;
                height: 50px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                border-top:1px solid gray;
            }

            .footer2 {
                position: ; 
                bottom: -50px; 
                left: 50px; 
                right: 20px;
                height: 0px; 

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: black;
                line-height: 35px;
                text-align: right;
                /*border-bottom:1px solid gray;*/
                border-bottom-width: 50px solid gray;
            }
            .pagenum:before { content: counter(page); }
ol.demo {
  /*margin: 0;*/
  padding: 15;
}

#child2 {
  position: absolute;
  line-height: 12px;
  border-bottom: 1px solid red;
  bottom: 60px;
  right: -20px;
  margin-left: none;
  display: inline-block;
}

.page-break {
    page-break-after: always;
}
.table{
    margin: none;
    vertical-align: bottom;
    font-size:12px;
   
}
.table2
{
   
}
.table2, .th2{
  border-collapse: collapse;
  border: 1px solid black;
  font-size:12px;
  text-align: center;

}
.td2{
  border-right: 1px solid black;
  border-left: 1px solid black;
  border-collapse: collapse;
  padding-right: 10px;
  white-space: nowrap;
  font-size:12px;
  vertical-align: top;

}
.td3{

  font-size:12px;
  text-align: right;

}




#theTable{
    background:#BDD2FF;
    font-size:12px;
    border:1px solid black;
    -moz-border-radius: 5px 5px 5px 5px; 
    -webkit-border-radius: 5px 5px 5px 5px; 
    border-radius: 5px 5px 5px 5px; 
    -moz-box-shadow:0px 0px 20px #aaa;
    -webkit-box-shadow:0px 0px 20px #aaa;
    box-shadow:0px 0px 20spx #aaa;
}
#theTable th,td {
    color: #000;
    padding-left     : 2px;
    padding-right    : 2px;
    padding-top      : 2px;
    padding-bottom   : 2px;
}
#theTable th {
    background-color:#5c9fe9;
    color:#FFF;
    text-align:center;
    font:normal 11px Verdana, Geneva, sans-serif;
    font-weight:bold;
    height:20px;
    padding-top:5px;
}
#theTable td{
    border:0px solid #5c9fe9;
    font:normal 11px Verdana, Geneva, sans-serif;
}
</style>
</head>
<body>

 @foreach($gaji as $row )


</br>
</br>
</br>
<table class="table">
<tr><th colspan="2"><u>PT BUANA MULTIDANA SEJAHTERA</u></th></tr>
<tr><td colspan="2"><u>SLIP GAJI ({{$row->bulan}} - {{$row->tahun}})<u></td></tr>
<tr><td colspan="2"><br></td></tr>
<tr><td>N I K</td><td>: {{$row->nik}}</td></tr>
<tr><td>Nama</td><td>: {{$row->nama_karyawan}}</td></tr>
<tr><td>Jabatan</td><td>: {{$row->nama_jabatan}}</td></tr>
<tr><td>Kantor</td><td>: {{$row->nama_cabang}}</td></tr>
</table>
<table class="table2" >
   <tr><th class="th2" >PENDAPATAN</th><th class="th2" >POTONGAN</th></tr>
   <tr><td class="td2">
   
        <table >
               <tr><td>Pendapatan</td><td> : </td><td class="td3">  {{setRupiah($row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_pph21+$row->uang_makan+$row->insentif+$row->koreksiplus)}}</td ></tr>
                
                 <tr><th></td><td> </td><th class="td3"> <hr></td></tr>
                <tr><th>Total Pendapatan</td><td></td><th class="td3">  {{setRupiah($row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_pph21+$row->uang_makan+$row->insentif+$row->koreksiplus-$row->koreksimin-$row->potongan_absen-$row->pph21)}}</td></tr>

        </table>


        </td>
        <td class="td2">
            <TABLE>
                                         @if ($row->simpanan_wajib > 0)
                                       <tr><td>Simpanan Wajib</td><td> : </td><td class="td3">  {{setRupiah($row->simpanan_wajib)}}</td>
                                        </tr>
                                        @endif
                                        
                                        @if ($row->pinjaman_koperasi > 0)
                                       <tr><td>Pinjaman</td><td> : </td><td class="td3">  {{setRupiah($row->pinjaman_koperasi)}}</td>
                                        </tr>
                                        @endif
                                       
                                        @if ($row->bpjs_kes_kry > 0)
                                       <tr><td>BPJS Kesehatan</td><td> : </td><td class="td3">  {{setRupiah($row->bpjs_kes_kry)}}</td>
                                        </tr>
                                        @endif 
                                        @if ($row->jht_kry > 0)
                                       <tr><td>BPJS TK</td><td> : </td><td class="td3">  {{setRupiah($row->jht_kry)}}</td>
                                        </tr>
                                        @endif
                                        @if ($row->jp_kry > 0)
                                       <tr><td>BPJS JP</td><td> : </td><td class="td3">  {{setRupiah($row->jp_kry)}}</td>
                                        </tr>
                                        @endif  


                                        
        
                                        <tr><th></td><td> </td><th class="td3"> <hr></td></tr>
                                         <tr><th>Total Potongan</td><td> : </td><th class="td3">{{setRupiah($row->simpanan_wajib+$row->bpjs_kes_kry+$row->jht_kry+$row->jp_kry)}}</th>
                                    </tr> 

                </TABLE>
        </td>

    </tr>
    <tr>
        <th class="th2" colspan="2">
            <TABLE>
           
               <tr>
                    <td>Pendapatan-Potongan</td><td> : </td><td class="td3">  {{setRupiah(($row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_pph21+$row->uang_makan+$row->insentif+$row->koreksiplus)-($row->koreksimin+$row->simpanan_wajib+$row->pinjaman_koperasi+$row->pph21+$row->bpjs_kes_kry+$row->jht_kry+$row->jp_kry))}} (THP)</td>
                </tr>
              </TABLE>
        
        </th>
    </tr>
</table>

<h6>Mengetahui</h6>
<br>
<br>
<h6> (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</h6>
@endforeach




<!-- Tembusan surat keputusan ini disampaikan kepada Yth :
<ol>
    <li>Direksi PT. Buana Sejahtera Multidana</li>
    <li>HRGA Dept.</li>
    <li>Collection Dept.</li>
    <li>-</li>
    <li>-</li>
</ol> -->

</body>
</html>