@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                </div>
                <div class="alert alert-success" id="success-box" style="{{ (Session::has("success_message")) ? "" : "display:none;" }}; border-radius: 0 !important;">
                    <i class="fa fa-times-circle fa-fw"></i> <span id="success-message">{{ (Session::has("success_message")) ? Session::get("success_message") : "" }}</span>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" enctype="multipart/form-data">
                        @csrf
                         @foreach ($fields as $row)
                            {!! $row !!}
                        @endforeach
                         <div class="col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                         <div class="col-md-12">
                                             <h3 style="text-align:center;">PENDAPATAN</h3> 
                                         </div>
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Gaji Pokok</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($gaji_pokok)}}</div>
                                             </div>
                                         </div>
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Tunjangan Jabatan</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-xs-5 text-right">{{setRupiah($tunj_jabatan)}}</div>
                                             </div>
                                         </div>
                                        @if ($tunj_transport > 0)
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Tunjangan Transport</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($tunj_transport)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($tunj_transport > 0)
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Tunjangan Jaga</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($tunj_jaga)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($tunj_transport > 0)
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Tunjangan Kemahalan</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($tunj_kemahalan)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($tunj_transport > 0)  
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Tunjangan Makan</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($tunj_makan)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($uang_makan> 0)
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Uang Makan</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($uang_makan)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($insentif > 0)
                                          <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">insentif</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($insentif)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($koreksiplus > 0) 
                                         <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Koreksi Plus</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($koreksiplus)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        @if ($tunj_pph21 > 0)
                                          <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Tunjanagan PPh21</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($tunj_pph21)}}</div>
                                             </div>
                                         </div>
                                        @endif
                                        <hr class="visible-sm visible-xs" style="margin-bottom : 0px; display: block;border-style: inset;  border-width: 1px;">
                                        <div class="col-md-12 m-b-10 visible-sm visible-xs">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5"><h5><b>Total Pendapatan</b></h5></div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2"><h5></h5></div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right"><h5><b>{{setRupiah($pendapatan)}}</b></h5></div>
                                             </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 style="text-align:center;">POTONGAN</h3  > 
                                        </div>
                                        @if ($koreksimin > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Koreksi Minus</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($koreksimin)}}</div>
                                             </div>
                                        </div>
                                        @endif
                                        @if ($simpanan_wajib > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Simpanan Wajib</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($simpanan_wajib)}}</div>
                                             </div>
                                        </div>
                                        @endif
                                        @if ($potongan_absen > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Potongan Absensi</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($potongan_absen)}}</div>
                                             </div>
                                        </div>
                                        @endif
                                        @if ($pinjaman_koperasi > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">Pinjaman</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($pinjaman_koperasi)}}</div>
                                             </div>
                                        </div>
                                        @endif
                                        @if ($bpjs_kes_kry > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">BPJS Kesehatan</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($bpjs_kes_kry)}}</div>
                                             </div>
                                        </div>
                                        @endif 
                                        @if ($jht_kry > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">BPJS TK</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($jht_kry)}}</div>
                                             </div>
                                        </div>
                                        @endif 
                                        
                                         @if ($jp_kry > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">BPJS JP</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($jp_kry)}}</div>
                                             </div>
                                        </div>
                                        @endif 

                                        @if ($pph21 > 0)
                                        <div class="col-md-12 m-b-10">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5">PPh21</div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2">:</div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right">{{setRupiah($pph21)}}</div>
                                             </div>
                                        </div>
                                        @endif 
                                        <hr class="visible-sm visible-xs" style="margin-bottom : 0px; display: block;border-style: inset;  border-width: 1px;">
                                        <div class="col-md-12 m-b-10 visible-sm visible-xs">
                                             <div class="row">
                                                 <div class="col-md-5 col-sm-5 col-xs-5"><h5><b>Total Potongan</b></h5></div>
                                                 <div class="col-md-2 col-sm-2 col-xs-2"><h5></h5></div>
                                                 <div class="col-md-5 col-sm-5 col-xs-5 text-right"><h5><b>{{setRupiah($potongan)}}</b></h5></div>
                                             </div>
                                         </div> 
                                    </div>
                                </div>
                            </div>
                         </div>
                         <div class="col-md-9">
                            <div class="row visible-lg visible-md">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12"><b><hr style="display: block;border-style: inset;  border-width: 1px;"><b></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                       <div class="col-md-12"><b><hr style="display: block;border-style: inset;  border-width: 1px;"><b></div>
                                    </div>
                                </div>
                            </div>  
                            <div class="row visible-lg visible-md">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-7"><h5><b>Total Pendapatan</b></h4></div>
                                        <div class="col-md-5 text-right"><h5><b>{{setRupiah($pendapatan)}}</b></h5></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-7"><h5><b>Total Potongan</b></h4></div>
                                        <div class="col-md-5 text-right"><h5><b>{{setRupiah($potongan)}}</b></h5></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-7 col-sm-7 col-xs-7"><h5><b>Total Gaji</b></h4></div>
                                        <div class="col-md-5 col-sm-5 col-xs-5 text-right"><h5><b>{{setRupiah($pendapatan-$potongan)}}</b></h5></div>
                                    </div>
                                </div>
                            </div>  
                         </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">&nbsp;</label>
                            <div class="col-md-9">
                                @foreach ($buttons as $row)
                                    {!! $row !!}
                                @endforeach
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('app/js/form_validation.js') }}"></script>
@stop
