@extends('main')
@section('content')
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">{{ $title }}</li>
    </ol>
    <h1 class="page-header">{{ $title }}</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                @if (isset($tabs))
                    @if ($tabs > 0)
                        <div class="tab-overflow" style="background-color: #242A30">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                @foreach ($tabs as $row)
                                    <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                @endforeach
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    @endif
                @else
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">{{ $title }}</h4>
                    </div> 
                @endif
                
                @if ($errors->any())
                <div class="alert alert-danger" id="alert-box">
                    @foreach ($errors->all() as $error)
                    <i class="fa fa-times-circle fa-fw"></i> 
                    <span id="alert-message"> {{ $error }}</span><br>
                    @endforeach
                </div>
                @endif

                <div class="panel-body">
                    <form class="form-horizontal" id="myform" name="myform">
                        @csrf
                        @foreach ($data_header as $row)
                            {!! $row !!}
                        @endforeach
                    </form>

                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                @foreach ($data_list as $header)
                                    <th width="{{ $header["width"] }}" style="text-align:{{ $header["align"] }}; border: 0px; font-weight: bold;">{{ $header["label"] }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($select) == 0)
                                <tr>
                                    <td colspan="{{ count($data_list) }}" style="text-align: center; background-color: #fff">No data found (0)</td>
                                </tr>
                            @else
                                @foreach ($select as $rs)
                                    <tr>
                                        @foreach ($data_list as $row)
                                            @php ($field = $row["name"]) @endphp
                                            <td style="width:{{ $row["width"] }}; text-align:{{ $row["item-align"] }};">
                                                @if ($row["item-format"] == "number")
                                                    {{ number_format($rs->$field, 0) }}
                                                @elseif ($row["item-format"] == "flag")
                                                    {!! getLabelFlag($rs->status_code) !!}
                                                @elseif ($row["item-format"] == "checkbox")
                                                    <input type="checkbox" id="checkbox_id_<?=$No?>" name="checkbox_id[]" class ="checkbox_id" value="{{ $rs->$field }}">
                                                @else
                                                    {{ $rs->$field }}
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('app/js/form_validation.js') }}"></script>
@stop
