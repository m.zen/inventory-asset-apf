@extends('main')
@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li>Home</li>
            <li class="active">{{ $title }}</li>
        </ol>
        <h1 class="page-header">{{ $title }}</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    @if (isset($tabs))
                        @if ($tabs > 0)
                            <div class="tab-overflow" style="background-color: #242A30">
                                <ul class="nav nav-tabs nav-tabs-inverse">
                                    <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                                    @foreach ($tabs as $row)
                                        <li class="{{ $row["active"] }}"><a href="{{ url('/') . $row['url'] }}">{{ $row["label"] }}</a></li>
                                    @endforeach
                                    <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                                </ul>
                            </div>
                        @endif
                    @else
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                            <h4 class="panel-title">{{ $title }}</h4>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger" id="alert-box">
                            @foreach ($errors->all() as $error)
                                <i class="fa fa-times-circle fa-fw"></i>
                                <span id="alert-message"> {{ $error }}</span><br>
                            @endforeach
                        </div>
                    @endif
                    <div class="alert alert-danger" id="alert-box" style="{{ (Session::has("error_message")) ? "" : "display:none;" }}">
                        <i class="fa fa-times-circle fa-fw"></i> <span id="alert-message">{{ (Session::has("error_message")) ? Session::get("error_message") : "" }}</span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="myform" name="myform" action="{{ URL::to('/').$form_act }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @foreach ($fields as $row)
                                {!! $row !!}
                            @endforeach
                            <div class="form-group button-container">
                                <label class="col-md-3 control-label">&nbsp;</label>
                                <div class="col-md-9">
                                    @foreach ($buttons as $row)
                                        {!! $row !!}
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group preloader-container">
                                <label class="col-md-3 control-label">&nbsp;</label>
                                <div class="col-md-9">
                                    <img src="{{ asset('app/img/icon/preloader.gif') }}" alt="" />
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <!-- <div style="padding: 15px 0 0 15px; font-weight: bold;" id="response_nama_coa">&nbsp;</div> -->
                    <div class="panel-body">
                        <div class="tableFixHead">
                            <table style="margin-bottom: -2px; width: 100%;" class="table display-data-table table-bordered">
                                <thead>
                                <tr>
                                    <th class="detail_header" style="width: 15%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Item</th>
                                    <th class="detail_header" style="text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Deskripsi</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Harga</th>
                                    <th class="detail_header" style="width: 5%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Qty</th>
                                    <th class="detail_header" style="width: 5%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Satuan</th>
                                    <th class="detail_header" style="width: 5%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Disc %</th>
                                    <th class="detail_header" style="width: 12%; text-align: center; background-color: #e2e7eb; border-top: 1px solid #e2e7eb;">Jumlah</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i=0; $i<=$default_row; $i++)
                                    <tr>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_produk_{{$i}}" name="text_produk[]" title="Row {{$i}}" class="detail text_produk" placeholder="Press enter or double click" style="text-align: center; background-color: #fafafa;" onKeypress="pressProduct({{$i}});" onKeydown="pressProduct({{$i}});" readonly="readonly"></td>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_deskripsi_{{$i}}" name="text_deskripsi[]" title="Row {{$i}}" class="detail text_deskripsi" onKeypress="pressEnter({{$i}});">
                                        </td>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_harga_{{$i}}" name="text_harga[]" title="Row {{$i}}" class="detail text_harga" onBlur="this.value=formatCurrency(this.value);" onKeypress="pressHarga({{$i}});" value="0" style="text-align: right;">
                                            <input type="hidden" id="text_harga_beli_{{$i}}" name="text_harga_beli[]" title="Row {{$i}}" class="detail text_harga_beli" onBlur="this.value=formatCurrency(this.value);">
                                            <input type="hidden" id="text_harga_pokok_penjualan_{{$i}}" name="text_harga_pokok_penjualan[]" title="Row {{$i}}" class="detail text_harga_pokok_penjualan" onBlur="this.value=formatCurrency(this.value);">
                                        </td>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_qty_{{$i}}" name="text_qty[]" title="Row {{$i}}" class="detail text_qty" onBlur="this.value=formatCurrency(this.value);" style="text-align: center;" value="0" onKeypress="pressQty({{$i}});">
                                        </td>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_satuan_{{$i}}" name="text_satuan[]" title="Row {{$i}}" class="detail text_satuan" style="text-align: center;" value="" readonly="readonly">
                                        </td>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_disc_{{$i}}" name="text_disc[]" title="Row {{$i}}" class="detail text_disc" style="text-align: center;" value="0" onKeypress="pressDisc({{$i}});" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"></td>
                                            <input type="hidden" id="text_amount_diskon_{{$i}}" name="text_amount_diskon[]" title="Row {{$i}}" class="detail text_amount_diskon" value="0">
                                        </td>
                                        <td style="margin: 0; padding: 0;">
                                            <input type="text" id="text_jumlah_{{$i}}" name="text_jumlah[]" title="Row {{$i}}" class="detail text_jumlah" onBlur="this.value=formatCurrency(this.value);" readonly="readonly" value="0" style="text-align: right; background-color: #FFFF99;">
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>
                        <table style="width: 100%; background-color: #fafafa;" class="table table-bordered">
                            <tr>
                                <td style="font-weight: bold; text-align: right; height: 10px;" colspan="6">Subtotal</td>
                                <td style="font-weight: bold; width: 13.2%; text-align: right; margin: 0; padding: 0; background-color: #fafafa;">
                                    <input type="text" id="subtotal" name="subtotal" onBlur="this.value=formatCurrency(this.value);" class="input-footer" value="0" readonly="readonly" style="background-color: #fafafa;">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; text-align: right;" colspan="6">
                                    <select id="no_coa_pph" name="no_coa_pph" style="width: 170px; text-align: right; align: right;">
                                        <option selected="selected" value="0">- Pilih PPH -</option>
                                        @foreach($select_pph as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td style="font-weight: bold; width: 13.2%; text-align: right; margin: 0; padding: 0; background-color: #fafafa;">
                                    <input type="text" id="total_pph" name="total_pph" class="input-footer" value="0" readonly="readonly" style="background-color: #fafafa;">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; text-align: right;" colspan="6">
                                    <select id="no_coa_ppn" name="no_coa_ppn" style="width: 170px; text-align: right; align: right;">
                                        <option selected="selected" value="0">- Pilih PPN -</option>
                                        @foreach($select_ppn as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td style="font-weight: bold; width: 13.2%; text-align: right; margin: 0; padding: 0; background-color: #fafafa;">
                                    <input type="text" id="total_ppn" name="total_ppn" class="input-footer" value="0" readonly="readonly" style="background-color: #fafafa;">
                                    <input type="hidden" id="total_ppn_inclusive" name="total_ppn_inclusive" class="input-footer" value="0" readonly="readonly" style="background-color: #fafafa;">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; text-align: right;" colspan="6">Total</td>
                                <td style="font-weight: bold; width: 13.2%; text-align: right; margin: 0; padding: 0; background-color: #fafafa;">
                                    <input type="text" id="grand_total" name="grand_total" onBlur="this.value=formatCurrency(this.value);" class="input-footer" value="0" readonly="readonly" style="background-color: #fafafa;">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; text-align: right;" colspan="6">Uang Muka</td>
                                <td style="font-weight: bold; width: 13.2%; text-align: right; margin: 0; padding: 0; background-color: #ffffff;">
                                    <input type="text" id="text_uang_muka" name="text_uang_muka" onBlur="this.value=formatCurrency(this.value);" class="input-footer" value="0">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; text-align: right;" colspan="6">Jumlah Tertagih</td>
                                <td style="font-weight: bold; width: 13.2%; text-align: right; margin: 0; padding: 0; background-color: #fafafa;">
                                    <input type="text" id="text_tertagih" name="text_tertagih" onBlur="this.value=formatCurrency(this.value);" class="input-footer" value="0">
                                </td>
                            </tr>
                            <!-- <tr>
                                <td style="font-weight: bold; text-align: right;" colspan="6">Pajak Include</td>
                                <td style="font-weight: bold; width: 12%; text-align: right; margin: 0; padding: 0; background-color: #fafafa;">
                                    <input type="text" id="total_pajak_inclusive" name="total_pajak_inclusive" onBlur="this.value=formatCurrency(this.value);" class="input-footer" value="0" readonly="readonly" style="background-color: #fafafa;">
                                </td>
                            </tr> -->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="snackbar"></div>
    <input type="hidden" id="text_url" name="text_url" value="{{ url('/') }}">
    <input type="hidden" id="text_default_row" name="text_default_row" value="{{ $default_row }}">
    <input type="hidden" id="text_jenis_ppn" name="text_jenis_ppn" value="">
    </form>
    <link href="{{ asset('app/css/grid/sales-add.css') }}" rel="stylesheet" />
    <script>
        $(".preloader-container").hide();
    </script>
    <script>
        $(".detail").keydown(function(e){
            var arra    = $(this).attr("title");
            var arrs    = arra.split(" ");
            var arr     = arrs[1];
            var elem    = document.activeElement.name;
            var obj     = elem.substring(0, (elem.length)-2);
            if(e.keyCode == 37) {
                // KIRI
                next_arr    = eval(arr) + 1;
                before_arr  = eval(arr) - 1;
                if(obj == "text_produk") {
                    if(arr > 0) {
                        $("#text_disc_" + before_arr).focus();
                        $("#text_disc_" + before_arr).select();
                    }
                }
                if(obj == "text_deskripsi") {
                    $("#text_produk_" + arr).focus();
                    $("#text_produk_" + arr).select();
                }
                if(obj == "text_harga") {
                    $("#text_deskripsi_" + arr).focus();
                    $("#text_deskripsi_" + arr).select();
                }
                if(obj == "text_qty") {
                    $("#text_harga_" + arr).focus();
                    $("#text_harga_" + arr).select();
                }
                if(obj == "text_disc") {
                    $("#text_qty_" + arr).focus();
                    $("#text_qty_" + arr).select();
                }
            }
            if(e.keyCode == 38) {
                // ATAS
                if(arr > 0) {
                    next_arr    = eval(arr) - 1;
                    obj_sel     = obj + "_" + next_arr;
                    $("#" + obj_sel).focus();
                    $("#" + obj_sel).select();
                }
            }
            if(e.keyCode == 39) {
                // KANAN
                next_arr    = eval(arr) + 1;
                if(obj == "text_produk") {
                    $("#text_deskripsi_" + arr).focus();
                    $("#text_deskripsi_" + arr).select();
                }
                if(obj == "text_deskripsi") {
                    $("#text_harga_" + arr).focus();
                    $("#text_harga_" + arr).select();
                }
                if(obj == "text_harga") {
                    $("#text_qty_" + arr).focus();
                    $("#text_qty_" + arr).select();
                }
                if(obj == "text_qty") {
                    $("#text_disc_" + arr).focus();
                    $("#text_disc_" + arr).select();
                }
                if(obj == "text_disc") {
                    $("#text_produk_" + next_arr).focus();
                    $("#text_produk_" + next_arr).select();
                }
            }
            if(e.keyCode == 40) {
                // BAWAH
                next_arr    = eval(arr) + 1;
                obj_sel     = obj + "_" + next_arr;
                $("#" + obj_sel).focus();
                $("#" + obj_sel).select();
            }
        });
        function pressEnter(i) {
            var rightclick;
            var e = window.event;
            if (e.which == 13) {
                $("#text_harga_" + i).focus();
                $("#text_harga_" + i).select();
                e.preventDefault();
                return false;
            }
        }
        function pressHarga(i) {
            var rightclick;
            var e = window.event;
            if (e.which == 13) {
                $("#text_qty_" + i).focus();
                $("#text_qty_" + i).select();
                e.preventDefault();
                return false;
            }
        }
        function pressQty(i) {
            var rightclick;
            var e = window.event;
            if (e.which == 13) {
                $("#text_disc_" + i).focus();
                $("#text_disc_" + i).select();
                e.preventDefault();
                return false;
            }
        }
        function pressDisc(i) {
            var rightclick;
            var e = window.event;
            var next_i = eval(i) + 1;
            if (e.which == 13) {
                $("#text_produk_" + next_i).focus();
                $("#text_produk_" + next_i).select();
                e.preventDefault();
                return false;
            }
        }
        function pressProduct(i) {
            var rightclick;
            var e   = window.event;
            var c   = $("#text_produk_" + i).val();
            if (e.which == 13) {
                // Press Enter
                var arr   = i;
                var url   = $("#text_url").val();
                var page  = url + "/popup/produk/" + i;
                showPopup(page);
                e.preventDefault();
                return false;
            }
            if (e.which == 8) {
                // Press Delete
                clearRow(i);
                e.preventDefault();
                return false;
            }
            if (e.which == 46) {
                // Press Delete
                clearRow(i);
                e.preventDefault();
                return false;
            }
            e.preventDefault();
            return false;
        }
        $(".text_produk").dblclick(function(){
            var arra    = $(this).attr("title");
            var arrs    = arra.split(" ");
            var arr     = arrs[1];
            var url   = $("#text_url").val();
            var page  = url + "/popup/barang/" + arr;
            showPopup(page);
            return false;
        });
        $(".text_qty").keyup(function(){
            var arr       = $(this).attr("title");
            var i         = arr.split(" ");
            var jumlah    = calcJumlah(i[1]);
            var subtotal  = calcSubtotal();
        });
        $(".text_disc").keyup(function(){
            var arr       = $(this).attr("title");
            var i         = arr.split(" ");
            var jumlah    = calcJumlah(i[1]);
            var subtotal  = calcSubtotal();
        });
        $(".text_harga").keyup(function(){
            var arr       = $(this).attr("title");
            var i         = arr.split(" ");
            var jumlah    = calcJumlah(i[1]);
            var subtotal  = calcSubtotal();
        });
        $("#text_discount").keyup(function() {
            calcSubtotal();
        });
        $("#text_uang_muka").keyup(function() {
            calcTertagih();
        });
        $("#no_coa_pph").change(function() {
            cekPPH();
        });
        $("#no_coa_ppn").change(function() {
            cekPPN();
        });
        function cekPPH() {
            var pajak             = $("#no_coa_pph").val();
            var subtotal          = $("#subtotal").val();
            var subtotal_r        = subtotal.replace(/\,/g,'');

            if(pajak != 0) {
                var split_pajak       = pajak.split("|");
                var pajak_id          = split_pajak[0];
                var pajak_name        = split_pajak[1];
                var pajak_value       = split_pajak[2];
                var pajak_type        = split_pajak[3];
                var pajak_excl_incl   = split_pajak[4];
                var hasil             = Math.round((eval(subtotal_r) * eval(pajak_value)) / 100);
            } else {
                var hasil             = 0;
            }

            $("#total_pph").val(formatNumber(hasil));

            calcGrandTotal();
        }
        function cekPPN() {
            var pajak             = $("#no_coa_ppn").val();
            var subtotal          = $("#subtotal").val();
            var subtotal_r        = subtotal.replace(/\,/g,'');

            if(pajak != 0) {
                var split_pajak       = pajak.split("|");
                var pajak_id          = split_pajak[0];
                var pajak_name        = split_pajak[1];
                var pajak_value       = split_pajak[2];
                var pajak_type        = split_pajak[3];
                var pajak_excl_incl   = split_pajak[4];

                if(pajak_excl_incl == "INCLUSIVE") {
                    var h             = ((eval(subtotal_r) / 1.1) * eval(pajak_value)) / 100;
                    var hasil         = Math.round(h);
                    $("#text_jenis_ppn").val("INCLUSIVE");
                    $("#total_ppn_inclusive").val(hasil);
                } else {
                    var hasil         = Math.round((eval(subtotal_r) * eval(pajak_value)) / 100);
                    $("#text_jenis_ppn").val("EXCLUSIVE");
                    $("#total_ppn_inclusive").val(0);
                }
            } else {
                var hasil             = 0;

                $("#text_jenis_ppn").val("");
                $("#total_ppn_inclusive").val(0);
            }

            $("#total_ppn").val(formatNumber(hasil));

            calcGrandTotal();
        }
        function clearRow(i) {
            $("#text_produk_" + i).val("");
            $("#text_deskripsi_" + i).val("");
            $("#text_harga_" + i).val(0);
            $("#text_harga_beli_" + i).val(0);
            $("#text_harga_pokok_penjualan_" + i).val(0);
            $("#text_qty_" + i).val(0);
            $("#text_disc_" + i).val(0);
            $("#text_amount_diskon_" + i).val(0);
            $("#text_jumlah_" + i).val(0);
            calcSubtotal();
        }
        function showPopup(url) {
            var str = "left=0,screenX=0,top=0,screenY=0";
            var ah = screen.availHeight - 30;
            var aw = screen.availWidth - 10;
            var sc = "yes";
            str += ",height=" + ah;
            str += ",innerHeight=" + ah;
            str += ",width=" + aw;
            str += ",innerWidth=" + aw;
            str += ",scrollbars=" + sc;
            str += ",resizable";
            myPopup(url, "Produk", 260, 370);
        }
        function myPopup(myURL, title, myWidth, myHeight) {
            var left = (screen.width - myWidth) / 2;
            var top = (screen.height - myHeight) / 4;
            var myWindow = window.open(myURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + myWidth + ', height=' + myHeight + ', top=' + top + ', left=' + left);
        }
        function calcJumlah(i) {
            var harga   = $("#text_harga_" + i).val();
            var qty     = $("#text_qty_" + i).val();
            var disc    = $("#text_disc_" + i).val();
            var harga_r = harga.replace(/\,/g,'');
            if(harga_r == "") {
                harga_r   = 0;
            }
            if(qty == "") {
                qty   = 0;
            }
            if(disc == "") {
                disc   = 0;
            }
            if(eval(disc) > 0) {
                var disc = ((eval(harga_r) * eval(qty)) * eval(disc)) / 100;
            }
            var jumlah  = eval(harga_r) * eval(qty);
            var result  = eval(jumlah) - eval(disc);
            var disc    = $("#text_amount_diskon_" + i).val(disc);



            $("#text_jumlah_" + i).val(formatNumber(result));
        }
        function calcSubtotal() {
            var row           = $("#text_default_row").val();
            var subtotal      = 0;
            var subtotal_disc = 0;
            var total_excl    = 0;
            var total_incl    = 0;
            var disc_rincian  = 0;
            var discount      = 0;
            var discount_r    = 0;
            if(discount_r == "") {
                discount_r    = 0;
            }
            // --------------------
            var ppn               = $("#total_ppn").val();
            var pph               = $("#total_pph").val();
            var ppn_r             = ppn.replace(/\,/g,'');
            var pph_r             = pph.replace(/\,/g,'');
            // --------------------
            for(i=0 ; i<row; i++) {
                var produk   = $("#text_produk_" + i).val();
                var jumlah   = $("#text_jumlah_" + i).val();
                var jumlah_r = jumlah.replace(/\,/g,'');
                var disc     = $("#text_amount_diskon_" + i).val();
                var disc_r   = disc.replace(/\,/g,'');
                var excl     = 0;
                var incl     = 0;
                if(produk != "") {
                    if(jumlah_r > 0) {
                        subtotal = eval(subtotal) + eval(jumlah_r);
                    }
                    if(excl != 0) {
                        total_excl    = eval(total_excl) + (eval(jumlah_r) * eval(excl) / 100);
                    }
                    if(incl != 0) {
                        total_incl    = eval(total_incl) + ((eval(jumlah_r) / 1.1) * eval(incl) / 100);
                        total_incl    = Math.round(total_incl);
                    }
                } else {
                    i = eval(row) + 1;
                }

                disc_rincian = eval(disc_rincian) + eval(disc_r);
            }

            var grand_total = eval(subtotal) + eval(total_excl) - eval(discount_r);
            $("#subtotal").val(formatNumber(subtotal));
            //$("#total_pajak_exclusive").val(formatNumber(total_excl));
            //$("#total_pajak_inclusive").val(formatNumber(total_incl));
            //$("#grand_total").val(formatNumber(grand_total));
            cekPPH();
            cekPPN();
            //$("#text_uang_muka").val(0);
            //$("#text_discount_rincian").val("-" + formatNumber(disc_rincian));
        }
        function calcGrandTotal() {
            var subtotal          = $("#subtotal").val();
            var ppn               = $("#total_ppn").val();
            var pph               = $("#total_pph").val();
            var ppn_inclusive     = $("#total_ppn_inclusive").val();
            // --------------------
            var subtotal_r        = subtotal.replace(/\,/g,'');
            var ppn_r             = ppn.replace(/\,/g,'');
            var pph_r             = pph.replace(/\,/g,'');
            var pph_exlclusive_r  = ppn_inclusive.replace(/\,/g,'');
            // --------------------
            var total             = eval(subtotal_r) - eval(pph_r) + eval(ppn_r) - eval(pph_exlclusive_r);
            // --------------------
            $("#grand_total").val(formatNumber(total));
            // --------------------
            calcTertagih()
        }
        function calcTertagih() {
            var grand_total       = $("#grand_total").val();
            var dp                = $("#text_uang_muka").val();
            // --------------------
            var grand_total_r     = grand_total.replace(/\,/g,'');
            var dp_r              = dp.replace(/\,/g,'');
            // --------------------
            var tertagih          = eval(grand_total_r) - eval(dp_r);
            // --------------------
            $("#text_tertagih").val(formatNumber(tertagih));
        }
        function formatNumber (num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        }
        function showSnackbar(msg) {
            document.getElementById("snackbar").innerHTML = msg;
            var x       = document.getElementById("snackbar");
            x.className = "show";
            // After 3 seconds, remove the show class from DIV
            setTimeout(function() {
                x.className = x.className.replace("show", "");
            }, 3000);
        }
        $("#myform").submit(function(){
            var tanggal_sales   = $("#tanggal_sales").val();
            var pelanggan       = $("#pelanggan_id").val();
            var tanggal_jt      = $("#tanggal_jatuh_tempo").val();
            var grand_total     = $("#grand_total").val();

            $(".button-container").hide();
            $(".preloader-container").show();

            if(tanggal_sales == "") {
                showSnackbar("Tanggal sales harus diisi");
                $(".button-container").show();
                $(".preloader-container").hide();
                return false;
            }
            if(tanggal_jt == "") {
                showSnackbar("Tanggal jatuh tempo diisi");
                $(".button-container").show();
                $(".preloader-container").hide();
                return false;
            }
            if(grand_total == "") {
                showSnackbar("Produk harus diisi");
                $(".button-container").show();
                $(".preloader-container").hide();
                return false;
            } else {
                if(grand_total <= 0) {
                    showSnackbar("Produk harus diisi");
                    $(".button-container").show();
                    $(".preloader-container").hide();
                    return false;
                }
            }

            var dp    = $("#text_uang_muka").val();
            var dp_r  = dp.replace(/\,/g,'');
            var bank  = $("#no_coa_bank").val();

            // if(dp_r > 0) {
            //     if(bank == 0) {
            //         showSnackbar("Akun bank harus diisi");

            //         $(".button-container").show();
            //         $(".preloader-container").hide();

            //         return false;
            //     }
            // }
        });
    </script>
    <style>
        .select-pajak {
            width: 100%;
            padding-left: 10px;
            padding-right: 20px;
            font-size: 11px;
            background-color:#fff;
            color:black;
            height: 30px;
            text-align: center;
            align-items: center;
            color:black;
            border-radius:0px;
            -webkit-border-radius:0;
            -webkit-appearance: none;
            -moz-appearance: none;
            border:0;
            outline:0;
            -moz-appearance: none;
            text-indent: 0.01px;
        }
        .select-pajak::-ms-expand {
            display: none;
        }
        .select-pajak:focus, .select-pajak:active {
            border:0;
            outline:0;
        }
        .input-footer {
            background-color: transparent;
            border: 0px solid;
            height: 35px;
            width: 100%;
            color: #333;
            text-align: right;
            margin: 0px;
            padding-right: 15px;
            font-family: Tahoma, Verdana, Arial, Helvetica, Sans-Serif;
            font-size: 11px;
        }
        /* The snackbar - position it at the bottom and in the middle of the screen */
        #snackbar {
            visibility: hidden; /* Hidden by default. Visible on click */
            min-width: 250px; /* Set a default minimum width */
            margin-left: -125px; /* Divide value of min-width by 2 */
            background-color: #eee; /* Black background color */
            color: #333; /* White text color */
            text-align: center; /* Centered text */
            border-radius: 2px; /* Rounded borders */
            padding: 16px; /* Padding */
            position: fixed; /* Sit on top of the screen */
            z-index: 1; /* Add a z-index if needed */
            left: 50%; /* Center the snackbar */
            bottom: 30px; /* 30px from the bottom */
        }
        /* Show the snackbar when clicking on a button (class added with JavaScript) */
        #snackbar.show {
            visibility: visible; /* Show the snackbar */
            /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
            However, delay the fade out process for 2.5 seconds */
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }
        /* Animations to fade the snackbar in and out */
        @-webkit-keyframes fadein {
            from {bottom: 0; opacity: 0;}
            to {bottom: 30px; opacity: 1;}
        }
        @keyframes fadein {
            from {bottom: 0; opacity: 0;}
            to {bottom: 30px; opacity: 1;}
        }
        @-webkit-keyframes fadeout {
            from {bottom: 30px; opacity: 1;}
            to {bottom: 0; opacity: 0;}
        }
        @keyframes fadeout {
            from {bottom: 30px; opacity: 1;}
            to {bottom: 0; opacity: 0;}
        }
    </style>
@stop
