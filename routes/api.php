<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

 Route::post('/login', 'Api\ApiController@login');
 Route::post('/absensi', 'Api\ApiabsensiController@Listabsensi');
 Route::post('/timeoff', 'Api\ApiTimeOffController@Listtimeoff');
 Route::get('/jenis_timeoff', 'Api\ApiTimeOffController@JenisTimeOff');
 Route::post('/simpan_timeoff', 'Api\ApiTimeOffController@Simpan_Timeoff');
  Route::post('/profilekry', 'Api\ApiProfileController@ProfileKaryawan');