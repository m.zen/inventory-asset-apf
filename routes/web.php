<?php
Auth::routes();

Route::group(['middleware' =>['auth']], function(){
    Route::get('/', 'DashboardController@index')->name('home');
    /* ----------
     Master
    -----------------------
		User
		----------------------- */
            Route::get('/user/index/{page?}', 'Master\UserController@index');
                Route::post('/user/index', 'Master\UserController@index');
            Route::get('/user/add', 'Master\UserController@add');
                Route::post('/user/save', 'Master\UserController@save');
            Route::get('/user/edit/{id}', 'Master\UserController@edit');
                Route::put('/user/update', 'Master\UserController@update');
            Route::get('/user/delete/{id}', 'Master\UserController@delete');
								Route::delete('/user/remove', 'Master\UserController@remove');
            Route::get('/user/changepassword', 'Master\UserController@changepassword');
                Route::post('/user/updatepassword', 'Master\UserController@updatepassword');

   /*-----------------------
       Group
        ----------------------- */
    Route::get('/group/index/{page?}', 'Master\GroupController@index');
               Route::post('/group/index', 'Master\GroupController@index');
           Route::get('/group/add', 'Master\GroupController@add');
               Route::post('/group/save', 'Master\GroupController@save');
           Route::get('/group/edit/{id}', 'Master\GroupController@edit');
               Route::put('/group/update', 'Master\GroupController@update');
           Route::get('/group/delete/{id}', 'Master\GroupController@delete');
                     Route::delete('/group/remove', 'Master\GroupController@remove');
           Route::get('/group/privilege/{id}', 'Master\GroupController@privilege');
                     Route::post('/group/setprivilege', 'Master\GroupController@setprivilege');

    

  
    /* ----------
      Cabang
    ----------------------- */
            Route::get('/cabang/index/{page?}', 'Master\CabangController@index');
                Route::post('/cabang/index', 'Master\CabangController@index');
            Route::get('/cabang/add', 'Master\CabangController@add');
                Route::post('/cabang/save', 'Master\CabangController@save');
            Route::get('/cabang/edit/{id}', 'Master\CabangController@edit');
                Route::put('/cabang/update', 'Master\CabangController@update');
            Route::get('/cabang/delete/{id}', 'Master\CabangController@delete');
                Route::delete('/cabang/remove', 'Master\CabangController@remove');

/* ----------
      departemen
   ----------------------- */

             Route::get('/departemen/index/{page?}', 'Master\DepartemenController@index');
                Route::post('/departemen/index', 'Master\DepartemenController@index');
            Route::get('/departemen/add', 'Master\DepartemenController@add');
                Route::post('/departemen/save', 'Master\DepartemenController@save');
            Route::get('/departemen/edit/{id}', 'Master\DepartemenController@edit');
                Route::put('/departemen/update', 'Master\DepartemenController@update');
            Route::get('/departemen/delete/{id}', 'Master\DepartemenController@delete');
                Route::delete('/departemen/remove', 'Master\DepartemenController@remove');
 /* ----------
      Merk
    ----------------------- */
    Route::get('/merk/index/{page?}', 'Master\MerkController@index');
    Route::post('/merk/index', 'Master\MerkController@index');
Route::get('/merk/add', 'Master\MerkController@add');
    Route::post('/merk/save', 'Master\MerkController@save');
Route::get('/merk/edit/{id}', 'Master\MerkController@edit');
    Route::put('/merk/update', 'Master\MerkController@update');
Route::get('/merk/delete/{id}', 'Master\MerkController@delete');
    Route::delete('/merk/remove', 'Master\MerkController@remove');




/*

    Lokasi
*/


Route::get('/lokasi/index/{page?}', 'Master\LokasiController@index');
    Route::post('/lokasi/index', 'Master\LokasiController@index');
Route::get('/lokasi/add', 'Master\LokasiController@add');
    Route::post('/lokasi/save', 'Master\LokasiController@save');
Route::get('/lokasi/edit/{id}', 'Master\LokasiController@edit');
    Route::put('/lokasi/update', 'Master\LokasiController@update');
Route::get('/lokasi/delete/{id}', 'Master\LokasiController@delete');
    Route::delete('/lokasi/remove', 'Master\LokasiController@remove');


/*
 group barang
*/
Route::get('/grupbarang/index/{page?}', 'Master\GrupbarangController@index');
    Route::post('/grupbarang/index', 'Master\GrupbarangController@index');
Route::get('/grupbarang/add', 'Master\GrupbarangController@add');
    Route::post('/grupbarang/save', 'Master\GrupbarangController@save');
Route::get('/grupbarang/edit/{id}', 'Master\GrupbarangController@edit');
    Route::put('/grupbarang/update', 'Master\GrupbarangController@update');
Route::get('/grupbarang/delete/{id}', 'Master\GrupbarangController@delete');
    Route::delete('/grupbarang/remove', 'Master\GrupbarangController@remove');

/*
   Kategori barang
*/
Route::get('/kategori/index/{page?}', 'Master\KategoriController@index');
    Route::post('/kategori/index', 'Master\KategoriController@index');
Route::get('/kategori/add', 'Master\KategoriController@add');
    Route::post('/kategori/save', 'Master\KategoriController@save');
Route::get('/kategori/edit/{id}', 'Master\KategoriController@edit');
    Route::put('/kategori/update', 'Master\KategoriController@update');
Route::get('/kategori/delete/{id}', 'Master\KategoriController@delete');
    Route::delete('/kategori/remove', 'Master\KategoriController@remove');

/*
    Master Barang
*/

Route::get('/barang/index/{page?}', 'Master\BarangController@index');
    Route::post('/barang/index', 'Master\BarangController@index');
Route::get('/barang/add', 'Master\BarangController@add');
    Route::post('/barang/save', 'Master\BarangController@save');
Route::get('/barang/edit/{id}', 'Master\BarangController@edit');
    Route::put('/barang/update', 'Master\BarangController@update');
Route::get('/barang/delete/{id}', 'Master\BarangController@delete');
    Route::delete('/barang/remove', 'Master\BarangController@remove');


/*
 vendor
*/
Route::get('/vendor/index/{page?}', 'Master\VendorController@index');
    Route::post('/vendor/index', 'Master\VendorController@index');
Route::get('/vendor/add', 'Master\VendorController@add');
    Route::post('/vendor/save', 'Master\VendorController@save');
Route::get('/vendor/edit/{id}', 'Master\VendorController@edit');
    Route::put('/vendor/update', 'Master\VendorController@update');
Route::get('/vendor/delete/{id}', 'Master\VendorController@delete');
    Route::delete('/vendor/remove', 'Master\VendorController@remove');

    Route::get('/referensi/index/{page?}', 'Master\ReferensiController@index');
    Route::post('/referensi/index', 'Master\ReferensiController@index');
Route::get('/referensi/add', 'Master\ReferensiController@add');
    Route::post('/referensi/save', 'Master\ReferensiController@save');
Route::get('/referensi/edit/{id}', 'Master\ReferensiController@edit');
    Route::put('/referensi/update', 'Master\ReferensiController@update');
Route::get('/referensi/delete/{id}', 'Master\ReferensiController@delete');
    Route::delete('/referensi/remove', 'Master\ReferensiController@remove');
    Route::get('/referensi/deletefile/{id}/{name}', 'Master\ReferensiController@deletefile');

/*
    Master Asset
*/

Route::get('/asset/index/{page?}', 'Master\AssetController@index');
    Route::post('/asset/index', 'Master\AssetController@index');
Route::get('/asset/add', 'Master\AssetController@add');
    Route::post('/asset/save', 'Master\AssetController@save');
Route::get('/asset/edit/{id}', 'Master\AssetController@edit');
    Route::put('/asset/update', 'Master\AssetController@update');
Route::get('/asset/delete/{id}', 'Master\AssetController@delete');
    Route::delete('/asset/remove', 'Master\AssetController@remove');


/*

*
    Asset Masuk
*/


Route::get('/trans_masuk/index/{page?}', 'Transaksi\TransaksimasukController@index');
    Route::post('/trans_masuk/index', 'Transaksi\TransaksimasukController@index');
Route::get('/trans_masuk/add', 'Transaksi\TransaksimasukController@add');
    Route::post('/trans_masuk/save', 'Transaksi\TransaksimasukController@save');
Route::get('/trans_masuk/edit/{id}', 'Transaksi\TransaksimasukController@edit');
    Route::put('/trans_masuk/update', 'Transaksi\TransaksimasukController@update');
Route::get('/trans_masuk/delete/{id}', 'Transaksi\TransaksimasukController@delete');
    Route::delete('/trans_masuk/remove', 'Transaksi\TransaksimasukController@remove');
Route::get('/trans_masuk/deletefile/{id}/{name}', 'Transaksi\TransaksimasukController@deletefile');
Route::get('/trans_masuk/approve/{id}', 'Transaksi\TransaksimasukController@approve');
Route::put('/trans_masuk/prosesapprove', 'Transaksi\TransaksimasukController@prosesapprove');
Route::get('/trans_masuk/getdocref/{id}', 'Transaksi\TransaksimasukController@getdocref');

Route::get('/trans_transfer/index/{page?}', 'Transaksi\TransaksitransferController@index');
    Route::post('/trans_transfer/index', 'Transaksi\TransaksitransferController@index');
Route::get('/trans_transfer/add', 'Transaksi\TransaksitransferController@add');
    Route::post('/trans_transfer/save', 'Transaksi\TransaksitransferController@save');
Route::get('/trans_transfer/edit/{id}', 'Transaksi\TransaksitransferController@edit');
    Route::put('/trans_transfer/update', 'Transaksi\TransaksitransferController@update');
Route::get('/trans_transfer/delete/{id}', 'Transaksi\TransaksitransferController@delete');
    Route::delete('/trans_transfer/remove', 'Transaksi\TransaksitransferController@remove');
Route::get('/trans_transfer/deletefile/{id}/{name}', 'Transaksi\TransaksitransferController@deletefile');
Route::get('/trans_transfer/get_profile/{id}', 'Transaksi\TransaksitransferController@get_profile');
Route::get('/trans_transfer/approve/{id}', 'Transaksi\TransaksitransferController@approve');
Route::put('/trans_transfer/prosesapprove', 'Transaksi\TransaksitransferController@prosesapprove');
/* ----------
     Laporan Masuk
   ----------------------- */
    Route::get('/laporan_masuk/index/{page?}', 'Laporan\LaporanMasukController@index');
    Route::post('/laporan_masuk/proses', 'Laporan\LaporanMasukController@proses');
    Route::get('/laporan_transfer/index/{page?}', 'Laporan\LaporanTransferController@index');
    Route::post('/laporan_transfer/proses', 'Laporan\LaporanTransferController@proses');
    Route::get('/laporan_disposal/index/{page?}', 'Laporan\LaporanDisposalController@index');
    Route::post('/laporan_disposal/proses', 'Laporan\LaporanDisposalController@proses');

Route::get('/trans_disposal/index/{page?}', 'Transaksi\TransaksihapusController@index');
    Route::post('/trans_disposal/index', 'Transaksi\TransaksihapusController@index');
Route::get('/trans_disposal/add', 'Transaksi\TransaksihapusController@add');
    Route::post('/trans_disposal/save', 'Transaksi\TransaksihapusController@save');
Route::get('/trans_disposal/edit/{id}', 'Transaksi\TransaksihapusController@edit');
    Route::put('/trans_disposal/update', 'Transaksi\TransaksihapusController@update');
Route::get('/trans_disposal/delete/{id}', 'Transaksi\TransaksihapusController@delete');
    Route::delete('/trans_disposal/remove', 'Transaksi\TransaksihapusController@remove');
Route::get('/trans_disposal/deletefile/{id}/{name}', 'Transaksi\TransaksihapusController@deletefile');

    /*
    Pinjaman
*/

Route::get('/pinjaman/index/{page?}', 'Payroll\PinjamanController@index');
                Route::post('/pinjaman/index', 'Payroll\PinjamanController@index');
            Route::get('/pinjaman/add', 'Payroll\PinjamanController@add');
                Route::post('/pinjaman/save', 'Payroll\PinjamanController@save');
            Route::get('/pinjaman/edit/{id}', 'Payroll\PinjamanController@edit');
                Route::put('/pinjaman/update', 'Payroll\PinjamanController@update');
            Route::get('/pinjaman/delete/{id}', 'Payroll\PinjamanController@delete');
                Route::delete('/pinjaman/remove', 'Payroll\PinjamanController@remove');
            Route::get('/pinjaman/print/{id}', 'Payroll\PinjamanController@cetak');
            Route::get('/pinjaman/pelunasan/{id}', 'Payroll\PinjamanController@pelunasan');
                Route::put('/pinjaman/updatepenlunasan', 'Payroll\PinjamanController@updatepenlunasan');


/*

     Closing Gaji

*/

     Route::get('/closing/index/{page?}', 'Payroll\ClosingController@index');
                Route::post('/closing/index', 'Master\ClosingController@index');
            Route::get('/closing/add', 'Payroll\ClosingController@add');
                Route::post('/closing/save', 'Payroll\ClosingController@save');
            Route::get('/closing/edit/{id}', 'Payroll\ClosingController@edit');
                Route::put('/closing/update', 'Payroll\ClosingController@update');
            Route::get('/closing/delete/{id}', 'Payroll\ClosingController@delete');
                Route::delete('/closing/remove', 'Payroll\ClosingController@remove');

/*

     Proses Gaji

*/

      Route::get('/prosesgaji/index/{page?}', 'Payroll\ProsesgajiController@index');
                Route::post('/prosesgaji/index', 'Master\ProsesgajiController@index');
            Route::get('/prosesgaji/add', 'Payroll\ProsesgajiController@add');
                Route::post('/prosesgaji/save', 'Payroll\ProsesgajiController@save');
            Route::get('/prosesgaji/edit/{id}', 'Payroll\ProsesgajiController@edit');
                Route::put('/prosesgaji/update', 'Payroll\ProsesgajiController@update');
            Route::get('/prosesgaji/delete/{id}', 'Payroll\ProsesgajiController@delete');
                Route::delete('/prosesgaji/remove', 'Payroll\ProsesgajiController@remove');


/*


  Proses THR

*/

      Route::get('/thr/index/{page?}', 'Payroll\HitungthrController@index');
                Route::post('/thr/index', 'Master\HitungthrController@index');
            Route::get('/thr/add', 'Payroll\HitungthrController@add');
                Route::post('/thr/save', 'Payroll\HitungthrController@save');
            Route::get('/thr/edit/{id}', 'Payroll\HitungthrController@edit');
                Route::put('/thr/update', 'Payroll\HitungthrController@update');
            Route::get('/thr/delete/{id}', 'Payroll\HitungthrController@delete');
                Route::delete('/thr/remove', 'Payroll\HitungthrController@remove');
            Route::get('/laporanthr/index', 'Payroll\HitungthrController@laporan_thr');
                Route::put('/laporanthr/cetak', 'Payroll\HitungthrController@cetak');

            


/*
/*

     Update Gaji

*/

           Route::get('/updategaji/index/{page?}', 'Payroll\UpdateGajiController@index');
                Route::post('/updategaji/index', 'Payroll\UpdateGajiController@index');
            Route::get('/updategaji/add', 'Payroll\UpdateGajiController@add');
                Route::post('/updategaji/save', 'Payroll\UpdateGajiController@save');
            Route::get('/updategaji/edit/{id}', 'Payroll\UpdateGajiController@edit');
                Route::put('/updategaji/update', 'Payroll\UpdateGajiController@update');
            Route::get('/updategaji/delete/{id}', 'Payroll\UpdateGajiController@delete');
                Route::delete('/updategaji/remove', 'Payroll\UpdateGajiController@remove');
      Route::get('/updategaji/pph', 'Payroll\UpdateGajiController@pph'); 
    Route::get('/updategaji/export', 'Payroll\UpdateGajiController@export');  


/*


 Paramete System

*/

 
Route::get('/profilesystem/index/{page?}', 'Master\ProfilesystemController@index');
                Route::post('/profilesystem/index', 'Master\ProfilesystemController@index');
            Route::get('/profilesystem/add', 'Master\ProfilesystemController@add');
                Route::post('/profilesystem/save', 'Master\ProfilesystemController@save');
            Route::get('/profilesystem/edit/{id}', 'Master\ProfilesystemController@edit');
                Route::put('/profilesystem/update', 'Master\ProfilesystemController@update');
            Route::get('/profilesystem/delete/{id}', 'Master\ProfilesystemController@delete');
                Route::delete('/profilesystem/remove', 'Master\ProfilesystemController@remove');


/*

*/

Route::get('/karyawan/index/{page?}', 'Personalia\KaryawanController@index');
                Route::post('/karyawan/index', 'Personalia\KaryawanController@index');
            Route::get('/karyawan/add', 'Personalia\KaryawanController@add');
                Route::post('/karyawan/save', 'Personalia\KaryawanController@save');
            Route::get('/karyawan/edit/{id}', 'Personalia\KaryawanController@edit');
                Route::put('/karyawan/update', 'Personalia\KaryawanController@update');
            Route::get('/karyawan/delete/{id}', 'Personalia\KaryawanController@delete');
                Route::delete('/karyawan/remove', 'Personalia\KaryawanController@remove');
            Route::get('/karyawan/cetakcv/{id}', 'Personalia\KaryawanController@cetakcv');
                Route::get('/karyawan/resign/{id}', 'Personalia\KaryawanController@resign');
            Route::put('/karyawan/updateresign', 'Personalia\KaryawanController@updateresign');
              Route::get('/karyawan/export', 'Personalia\KaryawanController@export');
              Route::get('/karyawan/batalkeluar/{id}', 'Personalia\KaryawanController@batalkeluar');
                Route::put('/karyawan/updatebatalkeluar', 'Personalia\KaryawanController@updatebatalkeluar');
                Route::get('/karyawan/gantinik/{id}', 'Personalia\KaryawanController@gantinik');
                Route::put('/karyawan/updatenik', 'Personalia\KaryawanController@updatenik');

Route::get('/karyawan/jaminan_keluar/{id}', 'Personalia\KaryawanController@jaminan_keluar');
                Route::put('/karyawan/update_jaminan', 'Personalia\KaryawanController@update_jaminan');

               Route::get('/karyawan/jaminan_index/{page?}', 'Personalia\KaryawanController@jaminan_index');
                Route::post('/karyawan/jaminan_index', 'Personalia\KaryawanController@jaminan_index');

Route::get('/karyawan/nonaktif_index/{page?}', 'Personalia\KaryawanController@nonaktif_index');
                Route::post('/karyawan/nonaktif_index', 'Personalia\KaryawanController@nonaktif_index');
 Route::get('/karyawan/nonaktif_export', 'Personalia\KaryawanController@nonaktif_export');
Route::get('/karyawan/ulangtahun_index/{page?}', 'Personalia\KaryawanController@ulangtahun_index');
                Route::post('/karyawan/ulangtahun_index', 'Personalia\KaryawanController@ulangtahun_index');

// Calon Karyawan

Route::get('/calonkaryawan/index/{page?}', 'Personalia\CalonKaryawanController@index');
                Route::post('/calonkaryawan/index/{idkry?}', 'Personalia\CalonKaryawanController@index');
            Route::get('/calonkaryawan/add', 'Personalia\CalonKaryawanController@add');
                Route::post('/calonkaryawan/save', 'Personalia\CalonKaryawanController@save');
            Route::get('/calonkaryawan/ajukan/{id}', 'Personalia\CalonKaryawanController@ajukan');
                Route::put('/calonkaryawan/prosesajukan', 'Personalia\CalonKaryawanController@prosesajukan');
            Route::get('/calonkaryawan/tolak/{id}', 'Personalia\CalonKaryawanController@tolak');
                Route::put('/calonkaryawan/prosestolak', 'Personalia\CalonKaryawanController@prosestolak');
            Route::get('/calonkaryawan/edit/{id}', 'Personalia\CalonKaryawanController@edit');
                Route::put('/calonkaryawan/update', 'Personalia\CalonKaryawanController@update');
            Route::get('/calonkaryawan/delete/{id}', 'Personalia\CalonKaryawanController@delete');
                Route::delete('/calonkaryawan/remove', 'Personalia\CalonKaryawanController@remove');
            Route::get('/calonkaryawan/approve/{id}', 'Personalia\CalonKaryawanController@approve');
                Route::put('/calonkaryawan/prosesapprove', 'Personalia\CalonKaryawanController@prosesapprove');
            Route::get('/calonkaryawan/view/{id}', 'Personalia\CalonKaryawanController@view');
            
            Route::get('/calonkaryawan/laporan', 'Personalia\CalonKaryawanController@laporan');
                Route::post('/calonkaryawan/exportproses', 'Personalia\CalonKaryawanController@exportproses');
            Route::get('/detailcalonkaryawan/index/{id}', 'Personalia\DetailCalonController@index');
Route::get('/calonkaryawan/get_profile/{id}', 'Personalia\calonkaryawanController@get_profile');
            Route::put('/calonkaryawan/cetak/{id}', 'Personalia\CalonKaryawanController@cetak');
            

Route::get('/karyawankeluar/index/{page?}', 'Personalia\KaryawankeluarController@index');
                Route::post('/karyawankeluar/index', 'Personalia\KaryawankeluarController@index');
            Route::get('/karyawankeluar/add', 'Personalia\KaryawankeluarController@add');
                Route::post('/karyawankeluar/save', 'Personalia\KaryawankeluarController@save');
            Route::get('/karyawankeluar/edit/{id}', 'Personalia\KaryawankeluarController@edit');
                Route::put('/karyawankeluar/update', 'Personalia\KaryawankeluarController@update');
            Route::get('/karyawankeluar/delete/{id}', 'Personalia\KaryawankeluarController@delete');
                Route::delete('/karyawankeluar/remove', 'Personalia\KaryawankeluarController@remove');
            Route::get('/karyawankeluar/cetakcv/{id}', 'Personalia\KaryawankeluarController@cetakcv');
            Route::get('/karyawankeluar/approve/{id}', 'Personalia\KaryawankeluarController@approve');
                Route::put('/karyawankeluar/updateapprove', 'Personalia\KaryawankeluarController@updateapprove');

Route::get('/karyawankontrak/index/{page?}', 'Personalia\KaryawankontrakController@index');
                Route::post('/karyawankontrak/index', 'Personalia\KaryawankontrakController@index');
            Route::get('/karyawankontrak/add', 'Personalia\KaryawankontrakController@add');
                Route::post('/karyawankontrak/save', 'Personalia\KaryawankontrakController@save');
            Route::get('/karyawankontrak/edit/{id}', 'Personalia\KaryawankontrakController@edit');
                Route::put('/karyawankontrak/update', 'Personalia\KaryawankontrakController@update');
            Route::get('/karyawankontrak/delete/{id}', 'Personalia\KaryawankontrakController@delete');
                Route::delete('/karyawankontrak/remove', 'Personalia\KaryawankontrakController@remove');
            Route::get('/karyawankontrak/cetak/{id}', 'Personalia\KaryawankontrakController@cetak');
            Route::get('/karyawankontrak/aktifasi/{id}', 'Personalia\KaryawankontrakController@aktivasi');
                Route::put('/karyawankontrak/updateaktivasi', 'Personalia\KaryawankontrakController@updateaktivasi');
            Route::get('/karyawankontrak/get_profile/{id}', 'Personalia\KaryawankontrakController@get_profile_karyawan');

                Route::get('/karyawankontrak/jtk_index/{page?}', 'Personalia\KaryawankontrakController@jtk_index');
                Route::post('/karyawankontrak/jtk_index', 'Personalia\KaryawankontrakController@jtk_index');


               
                 

                Route::get('/karyawankontrak/perpanjanan_kontrak/{id}', 'Personalia\KaryawankontrakController@perpanjanan_kontrak');

                 Route::get('/karyawankontrak/inq_index/{page?}', 'Personalia\KaryawankontrakController@inq_index');
                Route::post('/karyawankontrak/inq_index', 'Personalia\KaryawankontrakController@inq_index');
               





Route::get('/pkwtt/index/{page?}', 'Personalia\PkwttController@index');
                Route::post('/pkwtt/index', 'Personalia\PkwttController@index');
            Route::get('/pkwtt/add', 'Personalia\PkwttController@add');
                Route::post('/pkwtt/save', 'Personalia\PkwttController@save');
            Route::get('/pkwtt/edit/{id}', 'Personalia\PkwttController@edit');
                Route::put('/pkwtt/update', 'Personalia\PkwttController@update');
            Route::get('/pkwtt/delete/{id}', 'Personalia\PkwttController@delete');
                Route::delete('/pkwtt/remove', 'Personalia\PkwttController@remove');
            Route::get('/pkwtt/cetak/{id}', 'Personalia\PkwttController@cetak');
            Route::get('/pkwtt/aktifasi/{id}', 'Personalia\PkwttController@aktivasi');
                Route::put('/pkwtt/updateaktivasi', 'Personalia\PkwttController@updateaktivasi');


Route::get('/detail/index/{page?}', 'Personalia\DetailController@index');
                Route::post('/detail/index', 'Personalia\DetailController@index');
            

Route::get('/keluarga/index/{page?}', 'Personalia\KeluargaController@index');
                Route::post('/keluarga/index', 'Personalia\KeluargaController@index');
            Route::get('/keluarga/add/{idkry}', 'Personalia\KeluargaController@add');
                Route::post('/keluarga/save', 'Personalia\KeluargaController@save');
            Route::get('/keluarga/edit/{id}', 'Personalia\KeluargaController@edit');
                Route::put('/keluarga/update', 'Personalia\KeluargaController@update');
            Route::get('/keluarga/delete/{id}', 'Personalia\KeluargaController@delete');
                Route::delete('/keluarga/remove', 'Personalia\KeluargaController@remove');


Route::get('/kontak/index/{page?}', 'Personalia\KontakController@index');
                Route::post('/kontak/index', 'Personalia\KontakController@index');
            Route::get('/kontak/add/{idkry}', 'Personalia\KontakController@add');
                Route::post('/kontak/save', 'Personalia\KontakController@save');
            Route::get('/kontak/edit/{id}', 'Personalia\KontakController@edit');
                Route::put('/kontak/update', 'Personalia\KontakController@update');
            Route::get('/kontak/delete/{id}', 'Personalia\KontakController@delete');
                Route::delete('/kontak/remove', 'Personalia\KontakController@remove');


Route::get('/rpendidikan/index/{page?}', 'Personalia\RpendidikanController@index');
                Route::post('/rpendidikan/index', 'Personalia\RpendidikanController@index');
            Route::get('/rpendidikan/add/{idkry}', 'Personalia\RpendidikanController@add');
                Route::post('/rpendidikan/save', 'Personalia\RpendidikanController@save');
            Route::get('/rpendidikan/edit/{id}', 'Personalia\RpendidikanController@edit');
                Route::put('/rpendidikan/update', 'Personalia\RpendidikanController@update');
            Route::get('/rpendidikan/delete/{id}', 'Personalia\RpendidikanController@delete');
                Route::delete('/rpendidikan/remove', 'Personalia\RpendidikanController@remove');


Route::get('/pengalaman/index/{page?}', 'Personalia\PengalamanController@index');
                Route::post('/pengalaman/index', 'Personalia\PengalamanController@index');
            Route::get('/pengalaman/add/{idkry}', 'Personalia\PengalamanController@add');
                Route::post('/pengalaman/save', 'Personalia\PengalamanController@save');
            Route::get('/pengalaman/edit/{id}', 'Personalia\PengalamanController@edit');
                Route::put('/pengalaman/update', 'Personalia\PengalamanController@update');
            Route::get('/pengalaman/delete/{id}', 'Personalia\PengalamanController@delete');
                Route::delete('/pengalaman/remove', 'Personalia\PengalamanController@remove');


Route::get('/kontrakkerja/index/{page?}', 'Personalia\KontrakkerjaController@index');
                Route::post('/kontrakkerja/index', 'Personalia\KontrakkerjaController@index');
            Route::get('/kontrakkerja/add/{idkry}', 'Personalia\KontrakkerjaController@add');
                Route::post('/kontrakkerja/save', 'Personalia\KontrakkerjaController@save');
            Route::get('/kontrakkerja/edit/{id}', 'Personalia\KontrakkerjaController@edit');
                Route::put('/kontrakkerja/update', 'Personalia\KontrakkerjaController@update');
            Route::get('/kontrakkerja/delete/{id}', 'Personalia\KontrakkerjaController@delete');
                Route::delete('/kontrakkerja/remove', 'Personalia\KontrakkerjaController@remove');
            Route::get('/kontrakkerja/cetak/{id}', 'Personalia\KontrakkerjaController@cetak');
             Route::get('/kontrakkerja/aktif/{id}', 'Personalia\KontrakkerjaController@aktivasi');
                Route::put('/kontrakkerja/updateaktivasi', 'Personalia\KontrakkerjaController@updateaktivasi');

Route::get('/permanenkerja/index/{page?}', 'Personalia\PermanenkerjaController@index');
                Route::post('/permanenkerja/index', 'Personalia\PermanenkerjaController@index');
            Route::get('/permanenkerja/add/{idkry}', 'Personalia\PermanenkerjaController@add');
                Route::post('/permanenkerja/save', 'Personalia\PermanenkerjaController@save');
            Route::get('/permanenkerja/edit/{id}', 'Personalia\PermanenkerjaController@edit');
                Route::put('/permanenkerja/update', 'Personalia\PermanenkerjaController@update');
            Route::get('/permanenkerja/delete/{id}', 'Personalia\PermanenkerjaController@delete');
                Route::delete('/permanenkerja/remove', 'Personalia\PermanenkerjaController@remove');
            Route::get('/permanenkerja/cetak/{id}', 'Personalia\PermanenkerjaController@cetak');
             Route::get('/permanenkerja/aktif/{id}', 'Personalia\PermanenkerjaController@aktivasi');
                Route::put('/permanenkerja/updateaktivasi', 'Personalia\PermanenkerjaController@updateaktivasi');

Route::get('/suratperingatan/index/{page?}', 'Personalia\SuratperingatanController@index');
                Route::post('/suratperingatan/index', 'Personalia\SuratperingatanController@index');
            Route::get('/suratperingatan/add', 'Personalia\SuratperingatanController@add');
                Route::post('/suratperingatan/save', 'Personalia\SuratperingatanController@save');
            Route::get('/suratperingatan/edit/{id}', 'Personalia\SuratperingatanController@edit');
                Route::put('/suratperingatan/update', 'Personalia\SuratperingatanController@update');
            Route::get('/suratperingatan/delete/{id}', 'Personalia\SuratperingatanController@delete');
                Route::delete('/suratperingatan/remove', 'Personalia\SuratperingatanController@remove');
            Route::get('/suratperingatan/cetak/{id}', 'Personalia\SuratperingatanController@cetak');
             Route::get('/suratperingatan/approve/{id}', 'Personalia\SuratperingatanController@approve');
                Route::put('/suratperingatan/update_approve', 'Personalia\SuratperingatanController@update_approve');
                 Route::get('/suratperingatan/inqsp_index/{page?}', 'Personalia\SuratperingatanController@inqsp_index');
                Route::post('/suratperingatan/inqsp_index', 'Personalia\SuratperingatanController@inqsp_index');
                 Route::get('/suratperingatan/export', 'Personalia\SuratperingatanController@export');
                //Route::post('/suratperingatan/export', 'Personalia\SuratperingatanController@export');

Route::get('/karir/index/{page?}', 'Personalia\KarirController@index');
                Route::post('/karir/index', 'Personalia\KarirController@index');
            Route::get('/karir/add', 'Personalia\KarirController@add');
                Route::post('/karir/save', 'Personalia\KarirController@save');
            Route::get('/karir/edit/{id}', 'Personalia\KarirController@edit');
                Route::put('/karir/update', 'Personalia\KarirController@update');
            Route::get('/karir/delete/{id}', 'Personalia\KarirController@delete');
                Route::delete('/karir/remove', 'Personalia\KarirController@remove');
            Route::get('/karir/approve/{id}', 'Personalia\KarirController@approve');
                Route::put('/karir/update_approve', 'Personalia\KarirController@update_approve');
            Route::get('/karir/get_profile/{id}', 'Personalia\KarirController@get_profile_karyawan');
            Route::get('/karir/inqkarir_index/{page?}', 'Personalia\KarirController@inqkarir_index');
                Route::post('/karir/inqkarir_index', 'Personalia\karirController@inqkarir_index');
                 Route::get('/karir/export', 'Personalia\KarirController@export');
                   Route::get('/karir/cetak/{id}', 'Personalia\KarirController@Cetak');
                Route::get('/karir/batalapprove/{id}', 'Personalia\KarirController@batalapprove');
                Route::put('/karir/updatebatalapprove', 'Personalia\KarirController@updatebatalapprove');




Route::get('/penilaian/index/{page?}', 'Personalia\PenilaianController@index');
                Route::post('/penilaian/index', 'Personalia\PenilaianController@index');
            Route::get('/penilaian/add', 'Personalia\PenilaianController@add');
                Route::post('/penilaian/save', 'Personalia\PenilaianController@save');
            Route::get('/penilaian/edit/{id}', 'Personalia\PenilaianController@edit');
                Route::put('/penilaian/update', 'Personalia\PenilaianController@update');
            Route::get('/penilaian/delete/{id}', 'Personalia\PenilaianController@delete');
                Route::delete('/penilaian/remove', 'Personalia\PenilaianController@remove');


/* ----------
      tarif
   ----------------------- */  

Route::get('/tarif/index/{page?}', 'Payroll\TarifController@index');
                Route::post('/tarif/index', 'Payroll\TarifController@index');
            Route::get('/tarif/add', 'Payroll\TarifController@add');
                Route::post('/tarif/save', 'Payroll\TarifController@save');
            Route::get('/tarif/edit/{id}', 'Payroll\TarifController@edit');
                Route::put('/tarif/update', 'Payroll\TarifController@update');
            Route::get('/tarif/delete/{id}', 'Payroll\TarifController@delete');
                Route::delete('/tarif/remove', 'Payroll\TarifController@remove');

/* ----------
      ptkp
   ----------------------- */  
Route::get('/ptkp/index/{page?}', 'Payroll\PtkpController@index');
                Route::post('/ptkp/index', 'Payroll\PtkpController@index');
            Route::get('/ptkp/add', 'Payroll\PtkpController@add');
                Route::post('/ptkp/save', 'Payroll\PtkpController@save');
            Route::get('/ptkp/edit/{id}', 'Payroll\PtkpController@edit');
                Route::put('/ptkp/update', 'Payroll\PtkpController@update');
            Route::get('/ptkp/delete/{id}', 'Payroll\PtkpController@delete');
                Route::delete('/ptkp/remove', 'Payroll\PtkpController@remove');


Route::get('/inputgaji/index/{page?}', 'Payroll\InputgajiController@index');
                Route::post('/inputgaji/index', 'Payroll\InputgajiController@index');
            Route::get('/inputgaji/add', 'Payroll\InputgajiController@add');
                Route::post('/inputgaji/save', 'Payroll\InputgajiController@save');
            Route::get('/inputgaji/edit/{id}', 'Payroll\InputgajiController@edit');
                Route::put('/inputgaji/update', 'Payroll\InputgajiController@update');
            Route::get('/inputgaji/delete/{id}', 'Payroll\InputgajiController@delete');
                Route::delete('/inputgaji/remove', 'Payroll\InputgajiController@remove');
            Route::get('/inputgaji/export', 'Payroll\InputgajiController@export');

Route::get('/masterkarir/index/{page?}', 'Personalia\MasterKarirController@index');
                Route::post('/masterkarir/index', 'Personalia\MasterKarirController@index');
            Route::get('/masterkarir/add', 'Personalia\MasterKarirController@add');
                Route::post('/masterkarir/save', 'Personalia\MasterKarirController@save');
            Route::get('/masterkarir/edit/{id}', 'Personalia\MasterKarirController@edit');
                Route::put('/masterkarir/update', 'Personalia\MasterKarirController@update');
            Route::get('/masterkarir/delete/{id}', 'Personalia\MasterKarirController@delete');
                Route::delete('/masterkarir/remove', 'Personalia\MasterKarirController@remove');

Route::get('/grading/index/{page?}', 'Payroll\GradingController@index');
                Route::post('/grading/index', 'Payroll\GradingController@index');
            Route::get('/grading/add', 'Payroll\GradingController@add');
                Route::post('/grading/save', 'Payroll\GradingController@save');
            Route::get('/grading/edit/{id}', 'Payroll\GradingController@edit');
                Route::put('/grading/update', 'Payroll\GradingController@update');
            Route::get('/grading/delete/{id}', 'Payroll\GradingController@delete');
                Route::delete('/grading/remove', 'Payroll\GradingController@remove');

Route::get('/mastertraining/index/{page?}', 'Training\MastertrainingController@index');
                Route::post('/mastertraining/index', 'Training\MastertrainingController@index');
            Route::get('/mastertraining/add', 'Training\MastertrainingController@add');
                Route::post('/mastertraining/save', 'Training\MastertrainingController@save');
            Route::get('/mastertraining/edit/{id}', 'Training\MastertrainingController@edit');
                Route::put('/mastertraining/update', 'Training\MastertrainingController@update');
            Route::get('/mastertraining/delete/{id}', 'Training\MastertrainingController@delete');
                Route::delete('/mastertraining/remove', 'Training\MastertrainingController@remove');
Route::get('/providertraining/index/{page?}', 'Training\ProvidertrainingController@index');
                Route::post('/providertraining/index', 'Training\ProvidertrainingController@index');
            Route::get('/providertraining/add', 'Training\ProvidertrainingController@add');
                Route::post('/providertraining/save', 'Training\ProvidertrainingController@save');
            Route::get('/providertraining/edit/{id}', 'Training\ProvidertrainingController@edit');
                Route::put('/providertraining/update', 'Training\ProvidertrainingController@update');
            Route::get('/providertraining/delete/{id}', 'Training\ProvidertrainingController@delete');
                Route::delete('/providertraining/remove', 'Training\ProvidertrainingController@remove');

});


      Route::get('/grupabsen/index/{page?}', 'Absensi\GrupabsenController@index');
                Route::post('/grupabsen/index', 'Absensi\GrupabsenController@index');
            Route::get('/grupabsen/add', 'Absensi\GrupabsenController@add');
                Route::post('/grupabsen/save', 'Absensi\GrupabsenController@save');
            Route::get('/grupabsen/edit/{id}', 'Absensi\GrupabsenController@edit');
                Route::put('/grupabsen/update', 'Absensi\GrupabsenController@update');
            Route::get('/grupabsen/delete/{id}', 'Absensi\GrupabsenController@delete');
                Route::delete('/grupabsen/remove', 'Absensi\GrupabsenController@remove');

Route::get('/dataapproval/index/{page?}', 'Absensi\DataapprovalController@index');
                Route::post('/dataapproval/index', 'Absensi\DataapprovalController@index');
            Route::get('/dataapproval/add', 'Absensi\DataapprovalController@add');
                Route::post('/dataapproval/save', 'Absensi\DataapprovalController@save');
            Route::get('/dataapproval/edit/{id}', 'Absensi\DataapprovalController@edit');
                Route::put('/dataapproval/update', 'Absensi\DataapprovalController@update');
            Route::get('/dataapproval/delete/{id}', 'Absensi\DataapprovalController@delete');
                Route::delete('/dataapproval/remove', 'Absensi\DataapprovalController@remove');
    
      Route::get('/daftarabsen/index/{page?}', 'Absensi\DaftarabsenController@index');
                Route::post('/daftarabsen/index', 'Absensi\DaftarabsenController@index');
            Route::get('/daftarabsen/add', 'Absensi\DaftarabsenController@add');
                Route::post('/daftarabsen/save', 'Absensi\DaftarabsenController@save');
            Route::get('/daftarabsen/edit/{id}', 'Absensi\DaftarabsenController@edit');
                Route::put('/daftarabsen/update', 'Absensi\DaftarabsenController@update');
            Route::get('/daftarabsen/delete/{id}', 'Absensi\DaftarabsenController@delete');
                Route::delete('/daftarabsen/remove', 'Absensi\DaftarabsenController@remove');

Route::get('/absensi/index/{page?}', 'Absensi\DataabsensiController@index');
                Route::post('/absensi/index', 'Absensi\DataabsensiController@index');
            Route::get('/absensi/add', 'Absensi\DataabsensiController@add');
                Route::post('/absensi/save', 'Absensi\DataabsensiController@save');
            Route::get('/absensi/edit/{id}', 'Absensi\DataabsensiController@edit');
                Route::put('/absensi/update', 'Absensi\DataabsensiController@update');
            Route::get('/absensi/delete/{id}', 'Absensi\DataabsensiController@delete');
                Route::delete('/absensi/remove', 'Absensi\DataabsensiController@remove');
            Route::get('/absensi/import', 'Absensi\DataabsensiController@import');
                Route::post('/absensi/import/save', 'Absensi\DataabsensiController@importsave');
            Route::get('/absensi/laporan', 'Absensi\DataabsensiController@laporan');
                Route::post('/absensi/laporan/export', 'Absensi\DataabsensiController@laporann');
                
  Route::get('/pengajuantm/index/{page?}', 'Absensi\PengajuantmController@index');
                Route::post('/pengajuantm/index', 'Absensi\PengajuantmController@index');
            Route::get('/pengajuantm/add', 'Absensi\PengajuantmController@add');
                Route::post('/pengajuantm/save', 'Absensi\PengajuantmController@save');
            Route::get('/pengajuantm/edit/{id}', 'Absensi\PengajuantmController@edit');
                Route::put('/pengajuantm/update', 'Absensi\PengajuantmController@update');
            Route::get('/pengajuantm/delete/{id}', 'Absensi\PengajuantmController@delete');
                Route::delete('/pengajuantm/remove', 'Absensi\PengajuantmController@remove');
            Route::get('/pengajuantm/deletefile/{id}/{name}', 'Absensi\pengajuantmController@deletefile');

  // Laporan Personal
Route::get('/lprkaryawanbaru/index', 'Personalia\LaporanpaController@Karyawanbaru_index');
         Route::post('/lprkaryawanbaru/cetak', 'Personalia\LaporanpaController@Karyawanbaru_cetak');

Route::get('//lprresume/index', 'Personalia\LaporanpaController@laporansumm_index');
         Route::post('//lprresume/cetakresume', 'Personalia\LaporanpaController@resume_cetak');


Route::get('/lprkaryawankeluar/index', 'Personalia\LaporanpaController@Karyawankeluar_index');
         Route::post('/lprkaryawankeluar/cetak', 'Personalia\LaporanpaController@Karyawankeluar_cetak');

Route::get('/lprkaryawanturnover/index', 'Personalia\LaporanpaController@Karyawanturnover_index');
         Route::post('/lprkaryawanturnover/cetak', 'Personalia\LaporanpaController@Karyawanturnover_cetak');

Route::get('/lprkontrak/index', 'Personalia\LaporanpaController@jatuhtempo_index');
         Route::post('/lprkontrak/cetak', 'Personalia\LaporanpaController@jatuhtempo_cetak');

Route::get('/lprkaryawanaktif/index', 'Personalia\LaporanpaController@Karyawanaktif_index');
         Route::post('/lprkaryawanaktif/cetak', 'Personalia\LaporanpaController@Karyawanaktif_cetak');




// Laporan Data Gaji

Route::get('/lpgaji/index', 'Payroll\LaporanGajiController@index');
         Route::post('/lpgaji/cetak', 'Payroll\LaporanGajiController@cetak');

Route::get('/lpslip/index', 'Payroll\SlipController@index');
         Route::post('/lpslip/show', 'Payroll\SlipController@show');
         Route::post('/lpslip/cetak', 'Payroll\SlipController@cetak');


Route::get('/lpkop/index', 'Payroll\LaporankoperasiController@index');
         Route::post('/lpkop/cetak', 'Payroll\LaporankoperasiController@show');


Route::get('/dataapproval/departemenCabang/{id}', 'Absensi\DataapprovalController@departemenCabang');
            Route::get('/dataapproval/jabatan/{id_cabang}/{id_departemen}', 'Absensi\DataapprovalController@jabatan');
            Route::get('/dataapproval/approveler/{id_cabang}/{id_departemen}/{id}', 'Absensi\DataapprovalController@approveler');





//index
 Route::get('/daftarapproval/index/{page?}', 'Absensi\DaftarapprovalController@index');
               Route::post('/daftarapproval/index/', 'Absensi\DaftarapprovalController@index');
            Route::get('/daftarapproval/approve/{id}', 'Absensi\DaftarapprovalController@approve');
               Route::put('/daftarapproval/approve/update', 'Absensi\DaftarapprovalController@approveupdate');
                

Route::get('/karyawan/nonaktif_view/{id}', 'Personalia\KaryawanController@nonaktif_cetak');
Route::get('/karyawan/view_percobaan/{id}', 'Personalia\KaryawanController@view_percobaan');
Route::get('/karyawan/printview_percobaan/{id}', 'Personalia\KaryawanController@printview_percobaan');
Route::put('/karyawan/cetak_percobaan', 'Personalia\KaryawanController@cetak_percobaan');



Route::get('/daftarsertifikasi/index/{page?}', 'Sertifikasi\MastersertifikasiController@index');
                Route::post('/daftarsertifikasi/index', 'Sertifikasi\MastersertifikasiController@index');
            Route::get('/daftarsertifikasi/add', 'Sertifikasi\MastersertifikasiController@add');
                Route::post('/daftarsertifikasi/save', 'Sertifikasi\MastersertifikasiController@save');
            Route::get('/daftarsertifikasi/edit/{id}', 'Sertifikasi\MastersertifikasiController@edit');
                Route::put('/daftarsertifikasi/update', 'Sertifikasi\MastersertifikasiController@update');
            Route::get('/daftarsertifikasi/delete/{id}', 'Sertifikasi\MastersertifikasiController@delete');
                Route::delete('/daftarsertifikasi/remove', 'Sertifikasi\MastersertifikasiController@remove');

                Route::get('/vendorsertifikasi/index/{page?}', 'Sertifikasi\VendorsertifikasiController@index');
                Route::post('/vendorsertifikasi/index', 'Sertifikasi\VendorsertifikasiController@index');
            Route::get('/vendorsertifikasi/add', 'Sertifikasi\VendorsertifikasiController@add');
                Route::post('/vendorsertifikasi/save', 'Sertifikasi\VendorsertifikasiController@save');
            Route::get('/vendorsertifikasi/edit/{id}', 'Sertifikasi\VendorsertifikasiController@edit');
                Route::put('/vendorsertifikasi/update', 'Sertifikasi\VendorsertifikasiController@update');
            Route::get('/vendorsertifikasi/delete/{id}', 'Sertifikasi\VendorsertifikasiController@delete');
                Route::delete('/vendorsertifikasi/remove', 'Sertifikasi\VendorsertifikasiController@remove');

                Route::get('/jadwalabsen/index/{page?}', 'Absensi\JadwalabsenController@index');
Route::post('/jadwalabsen/index/', 'Absensi\JadwalabsenController@index');
Route::get('/jadwalabsen/add', 'Absensi\JadwalabsenController@add');
    Route::post('/jadwalabsen/save', 'Absensi\JadwalabsenController@save');
Route::get('/jadwalabsen/edit/{id}', 'Absensi\JadwalabsenController@edit');
    Route::put('/jadwalabsen/update', 'Absensi\JadwalabsenController@update');
Route::get('/jadwalabsen/delete/{id}', 'Absensi\JadwalabsenController@delete');
    Route::delete('/jadwalabsen/remove', 'Absensi\JadwalabsenController@remove');

Route::put('/jadwalabsen/proses', 'Absensi\JadwalabsenController@proses');
Route::get('/jadwalabsen/departemenCabang/{id}', 'Absensi\JadwalabsenController@departemenCabang');