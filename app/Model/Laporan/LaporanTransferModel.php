<?php

namespace App\Model\Laporan;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;

class LaporanTransferModel extends Model
{
    protected $table = "tr_transfer_asset";
    public $timestamps = false;

    public function getList($request = null, $offset = null, $limit = null)
    {
        $query = DB::table("m_asset_header as a")
            // ->select("a.*", "b.nama_barang", "b.keterangan","a.status_approve")
            // ->leftjoin("m_barang as b", "b.id_barang", "=", "a.id_barang")
            ->orderBy("a.id", "asc");

        if (session()->has("SES_SEARCH_ASSET")) {
            $query->where("nama_barang", "LIKE", "%" . session()->get("SES_SEARCH_ASSET") . "%");
        }

        if ($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getDataLaporan($request){
        $query = DB::table("$this->table")
                ->select("$this->table.*", "m_asset.no_asset", "m_barang.nama_barang", "m_departemen.nama_departemen AS dari_nama_departemen", "m_cabang.nama_cabang AS dari_nama_cabang"
                        , "m_lokasi.nama_lokasi AS dari_nama_lokasi", "b.nama_departemen AS ke_nama_departemen", "a.nama_cabang AS ke_nama_cabang", "c.nama_lokasi AS ke_nama_lokasi")
                ->join("m_asset", "$this->table.id_asset", "m_asset.id_asset")
                ->leftJoin("m_barang", "m_asset.id_barang", "m_barang.id_barang")
                ->leftJoin("m_cabang", "$this->table.dari_cabang", "m_cabang.id_cabang")
                ->leftJoin("m_departemen", "$this->table.dari_departemen", "m_departemen.id_departemen")
                ->leftJoin("m_lokasi", "$this->table.dari_lokasi", "m_lokasi.id_lokasi")
                ->leftJoin("m_cabang as a", "$this->table.ke_cabang", "a.id_cabang")
                ->leftJoin("m_departemen as b", "$this->table.ke_departemen", "b.id_departemen")
                ->leftJoin("m_lokasi as c", "$this->table.ke_lokasi", "c.id_lokasi")
                ->where("$this->table.tgl_transfer", ">=", setYMD($request->tgl_transfer_awal, "/"))
                ->where("$this->table.tgl_transfer", "<=", setYMD($request->tgl_transfer_akhir, "/"))
                ->get();

        return $query;
    }
}
