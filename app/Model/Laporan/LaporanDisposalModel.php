<?php

namespace App\Model\Laporan;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;

class LaporanDisposalModel extends Model
{
    protected $table = "tr_disposal_asset";
    public $timestamps = false;

    public function getList($request = null, $offset = null, $limit = null)
    {
        $query = DB::table("m_asset_header as a")
            // ->select("a.*", "b.nama_barang", "b.keterangan","a.status_approve")
            // ->leftjoin("m_barang as b", "b.id_barang", "=", "a.id_barang")
            ->orderBy("a.id", "asc");

        if (session()->has("SES_SEARCH_ASSET")) {
            $query->where("nama_barang", "LIKE", "%" . session()->get("SES_SEARCH_ASSET") . "%");
        }

        if ($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getDataLaporan($request){
        $query = DB::table("$this->table")
                ->select("$this->table.*", "m_asset.no_asset", "m_barang.nama_barang", "m_departemen.nama_departemen AS nama_departemen", "m_cabang.nama_cabang AS nama_cabang"
                        , "m_lokasi.nama_lokasi AS nama_lokasi", "m_disposal.nama_disposal")
                ->join("m_asset", "$this->table.id_asset", "m_asset.id_asset")
                ->leftJoin("m_barang", "m_asset.id_barang", "m_barang.id_barang")
                ->leftJoin("m_disposal", "$this->table.id_disposal", "m_disposal.id_disposal")
                ->leftJoin("m_cabang", "m_asset.id_cabang", "m_cabang.id_cabang")
                ->leftJoin("m_departemen", "m_asset.id_departemen", "m_departemen.id_departemen")
                ->leftJoin("m_lokasi", "m_asset.id_lokasi", "m_lokasi.id_lokasi")
                ->where("$this->table.tgl_hapus", ">=", setYMD($request->tgl_disposal_awal, "/"))
                ->where("$this->table.tgl_hapus", "<=", setYMD($request->tgl_disposal_akhir, "/"))
                ->get();

        return $query;
    }
}
