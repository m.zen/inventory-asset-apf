<?php

namespace App\Model\Laporan;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;

class LaporanMasukModel extends Model
{
    protected $table = "m_asset";
    public $timestamps = false;

    public function getList($request = null, $offset = null, $limit = null)
    {
        $query = DB::table("m_asset_header as a")
            // ->select("a.*", "b.nama_barang", "b.keterangan","a.status_approve")
            // ->leftjoin("m_barang as b", "b.id_barang", "=", "a.id_barang")
            ->orderBy("a.id", "asc");

        if (session()->has("SES_SEARCH_ASSET")) {
            $query->where("nama_barang", "LIKE", "%" . session()->get("SES_SEARCH_ASSET") . "%");
        }

        if ($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getDataLaporan($request){
        $query = DB::table("m_asset_header")
                ->select("m_asset_header.no_transaksi", "$this->table.*", "m_barang.nama_barang", "m_departemen.nama_departemen", "m_cabang.nama_cabang", "m_lokasi.nama_lokasi")
                ->join("$this->table", "m_asset_header.id", "$this->table.id_asset_header")
                ->leftJoin("m_barang", "$this->table.id_barang", "m_barang.id_barang")
                ->leftJoin("m_departemen", "$this->table.id_departemen", "m_departemen.id_departemen")
                ->leftJoin("m_cabang", "$this->table.id_cabang", "m_cabang.id_cabang")
                ->leftJoin("m_lokasi", "$this->table.id_lokasi", "m_lokasi.id_lokasi")
                ->where("m_asset_header.tanggal_perolehan", ">=", setYMD($request->tgl_perolehan_awal, "/"))
                ->where("m_asset_header.tanggal_perolehan", "<=", setYMD($request->tgl_perolehan_akhir, "/"))
                ->get();

        return $query;
    }

    public function getProfile($id)
    {
        $query = DB::table("m_asset as a")
        ->select("a.*", "b.nama_cabang", "c.nama_departemen","d.nama_lokasi")
        ->leftjoin("m_cabang as b", "b.id_cabang", "=", "a.id_cabang")
        ->leftjoin("m_departemen as c", "c.id_departemen", "=", "a.id_departemen")
        ->leftjoin("m_lokasi as d","d.id_lokasi","=","a.id_lokasi")
      // ->leftjoin("m_departemen as e", "e.id_departemen", "=", "a.id_departemen")
            //->leftjoin("m_referensi as f", "f.id_referensi", "=", "a.id_referensi")
            ->where("a.id_asset", $id)
            ->orderBy("a.id_asset", "DESC");

        $result = $query->get();

        return $result;
    }

    public function getAssetHeader($id){
        $query = DB::table("m_asset_header")
                ->where("m_asset_header.id", $id)
                ->orderBy("m_asset_header.id", "DESC")
                ;
        $result = $query->get();

        return $result;
    }

    public function getAssets($id){
        $query = DB::table("m_asset")
                ->where("m_asset.id_asset_header", $id)
                ->orderBy("m_asset.id_asset", "ASC")
                ;
        $result = $query->get();

        return $result;
    }

    public function createData($request){
        # header asset
        $id =   DB::table("m_asset_header")
                ->insertGetId([
                    "no_transaksi"      => $request->no_transaksi,
                    "tanggal_perolehan" => setYMD($request->tgl_perolehan, "/"),
                    "no_ref"            => $request->id_referensi,
                    "status"            => "Proses",
                ]);
        
        foreach ($request->id_barang as $i => $value) {
            # code...
            $namaFile = NULL;
            if ($request->foto[$i] != null) {
                $foto       = $request->foto[$i];
                $datenow    = date('Y-m-d');
                $namaFile   = 'Aset-' . $request->id_barang[$i] . ' ' . $datenow . '.' . $foto->getClientOriginalExtension();
                $foto->move('app/photo_asset', $namaFile);
                $filepath = public_path() . '/app/photo_asset/' . $namaFile;
                Image::load($filepath)->optimize()->save($filepath);
            }
            DB::table("m_asset")
                ->insertGetId([
                    "id_asset_header"   => $id,
                    "id_barang"         => $value,
                    "no_asset"          => $request->no_asset[$i],
                    "kondisi_pembelian" => $request->kondisi_pembelian[$i],
                    "status_asset"      => $request->status_asset[$i],
                    "id_cabang"         => $request->id_cabang[$i] > 0 ? $request->id_cabang[$i] : NULL,
                    "id_departemen"     => $request->id_departement[$i] > 0 ? $request->id_departement[$i] : NULL,
                    "id_lokasi"         => $request->id_lokasi[$i] > 0 ? $request->id_lokasi[$i] : NULL,
                    "harga_perolehan"   => $request->harga[$i],
                    "serial_no"         => $request->serial_no[$i],
                    "gambar_asset"      => $namaFile,
                    "keterangan"        => $request->keterangan[$i],
                    "user_id"           => Auth::user()->id,
                    "create_at"         => date('Y-m-d H:i:s'),
                    "update_at"         => date('Y-m-d H:i:s')
                ]);
        }
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE ASSET (" . $id . ") " . strtoupper($request->no_transaksi), Auth::user()->id, $request);
    }

    public function createDataV1($request)
    {
        $qasset = new TransaksimasukModel;
        # ---------------
       // $qasset->kode       = setString($request->kode);
        $qasset->id_barang = setString($request->id_barang);
        $qasset->tgl_perolehan = setYMD($request->tgl_perolehan, "/");
        $qasset->harga_perolehan = setNoComma($request->harga_perolehan);
        $qasset->status_asset = $request->status_asset;
        $qasset->kondisi_pembelian = $request->kondisi_pembelian;
        $qasset->serial_no = $request->serial_no;
        $qasset->no_asset = setString($request->no_asset);
        $qasset->id_referensi = $request->id_referensi;
        //$qasset->no_referensi = setString($request->no_referensi);
        $qasset->id_lokasi = setString($request->id_lokasi);
        $qasset->id_cabang = setString($request->id_cabang);
        $qasset->id_departemen= setString($request->id_departement);
        //$qasset->gambar_asset = setString($request->gambar_asset);
        if ($request->foto != null) {
            // Simpan file gabar folder server
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'Aset-' . $request->id_barang . ' ' . $datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/photo_asset', $namaFile);
            // $compress_images = $this->compressImage('app/foto_timeoff/'.$name, $foto);
            //             $factory = new \ImageOptimizer\OptimizerFactory();
            // $optimizer = $factory->get();

            //5. Kompres file gambar

            $filepath = public_path() . '/app/photo_asset/' . $namaFile;

            Image::load($filepath)->optimize()->save($filepath);
            // $optimizerChain = OptimizerChainFactory::create();

            // $optimizerChain->optimize(public_path('app/foto_timeoff/'.$namaFile));
            // $optimizer->optimize($filepath);
            // dd($optimizer);
            //dd($namaFile);
            $qasset->gambar_asset = $namaFile;
            $qasset->aktif = '0';
        }

        $qasset->keterangan = setString($request->keterangan);;
        $qasset->status_approve = "Proses";
        $qasset->user_id = setString(Auth::user()->id);
        $qasset->create_at = setString(date('Y-m-d H:i:s'));
        $qasset->update_at = setString(date('Y-m-d H:i:s'));
        # ---------------
        $qasset->save();
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE CABANG (" . $qasset->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function updateData($request){
        dd($request->all());
    }

    public function updateDataV1($request)
    {
        $namaFile=$request->nama_gambar;

        if ($request->foto != null) {
            // Simpan file gabar folder server
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'Aset-' . $request->id_barang . ' ' . $datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/photo_asset', $namaFile);
            $filepath = public_path() . '/app/photo_asset/' . $namaFile;
            Image::load($filepath)
                ->optimize()
                ->save($filepath);
            // $optimizerChain = OptimizerChainFactory::create();

            // $optimizerChain->optimize(public_path('app/foto_timeoff/'.$namaFile));
            // $optimizer->optimize($filepath);
            // dd($optimizer);
            //dd($namaFile);
           
        }
       
        DB::table("m_asset")
            ->where("id_asset", $request->id_asset)
            ->update([
                "id_barang" => setString($request->id_barang),
                "tgl_perolehan" => setYMD($request->tgl_perolehan, "/"),
                "harga_perolehan" => setNoComma($request->harga_perolehan),
                "status_asset" => $request->status_asset,
                "kondisi_pembelian" => $request->kondisi_pembelian,
                "serial_no" => $request->serial_no,
                "no_asset" => setString($request->no_asset),
                "id_referensi" => $request->id_referensi,
                //"no_referensi" =>setString($request->no_referensi),
                "id_lokasi" => setString($request->id_lokasi),
                "id_cabang" => setString($request->id_cabang),
                "id_departemen" => setString($request->id_departement),
                "keterangan" => setString($request->keterangan),
                
                "gambar_asset"=> $namaFile,


                "user_id" => setString(Auth::user()->id),
                "update_at" => setString(date('Y-m-d H:i:s'))
            ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("UPDATE CABANG(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }
    public function ApproveData($request)
    {
        
       
        DB::table("m_asset")
            ->where("id_asset", $request->id_asset)
            ->update([
                "status_approve" => setString($request->status_approve),
                 "aktif" =>'1',
                "user_id" => setString(Auth::user()->id),
                "update_at" => setString(date('Y-m-d H:i:s'))
            ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("UPDATE CABANG(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function removeData($request)
    {
        DB::table("m_asset")
            ->where("id_asset", $request->id_asset)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("DELETE CABANG (" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }
}
