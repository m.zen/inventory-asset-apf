<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class MenuModel extends Model
{
    protected $table = "sys_menus";

    public function getMenu($group=null, $level=null, $parent=null) {
    		$query 	= DB::table("sys_menus")
      							->join("sys_group_menus", "sys_menus.id", "=", "sys_group_menus.menu_id")
      							->where("status", 1)
                                  ->select("sys_menus.id", "sys_menus.name", "sys_menus.icon", "sys_menus.url", "sys_menus.parent", "sys_menus.order")
                                  ->orderBy("sys_menus.order", "ASC");

        if($group != null) {
        	$query->where("sys_group_menus.group_id", $group);
        }

        if($level != null) {
        	$query->where("sys_menus.level", $level);
        }

        if($parent != null) {
        	$query->where("sys_menus.parent", $parent);
        }

        $result = $query->get();

        return $result;
    }

    public function getActionMenu($group, $parent) {
        $query 	= DB::table("sys_menus")
        		            ->join("sys_group_menus", "sys_menus.id", "=", "sys_group_menus.menu_id")
        		            ->select("sys_menus.id", "sys_menus.name", "sys_menus.url", "sys_menus.icon")
        		            ->where("sys_group_menus.group_id", "=", $group)
        		            ->where("sys_menus.level", "=", 3)
        		            ->where("sys_menus.status", "=", 1)
        		            ->where("sys_menus.parent", "=", $parent)
                            ->where("sys_menus.position", "=", "sidebar")
                            ->orderBy("sys_menus.order", "ASC")
        		            ->get();

    	   return $query;
    }

    public function getParentMenu($url) {
        $query  = DB::table("vw_structure_menu")
                            ->select("id", "url", "name", "level", "parent_id", "parent_name", "parent_level")
                            ->where("url", "=", $url)
                            ->get();

        return $query;
    }
}
