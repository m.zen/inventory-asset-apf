<?php

namespace App\Model\Transaksi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;
class TransaktransferModel extends Model
{
    protected $table = "tr_transfer_asset";
    public $timestamps = false;

    public function getList($request = null, $offset = null, $limit = null)
    {
        $query = DB::table("tr_transfer_asset as a")
            ->select("a.*", "b.nama_cabang as daricabang", "c.nama_cabang as kecabang","e.nama_barang")
            ->leftjoin("m_cabang as b", "b.id_cabang", "=", "a.dari_cabang")
            ->leftjoin("m_cabang as c", "c.id_cabang", "=", "a.ke_cabang")
            ->leftjoin("m_asset as d", "d.id_asset", "=", "a.id_asset")
            ->leftjoin("m_barang as e","e.id_barang","=","d.id_barang")
            ->leftjoin("m_lokasi as f","f.id_lokasi","=","a.ke_lokasi")
            ->orderBy("a.id_asset", "asc");

        if (session()->has("SES_SEARCH_ASSET_TRANSFER")) {
            $query->where("e.nama_asset", "LIKE", "%" . session()->get("SES_SEARCH_ASSET_TRANSFER") . "%");
        }

        if ($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id)
    {
        $query = DB::table("tr_transfer_asset as a")
            ->select("a.*", "b.nama_cabang", "c.nama_departemen","d.nama_lokasi","e.nama_cabang as cabang_tujuan", "f.nama_departemen as departemen_tujuan ","g.nama_lokasi as lokasi_tujuan ")
            ->leftjoin("m_cabang as b", "b.id_cabang", "=", "a.dari_cabang")
            ->leftjoin("m_departemen as c", "c.id_departemen", "=", "a.dari_departemen")
            ->leftjoin("m_lokasi as d","d.id_lokasi","=","a.dari_lokasi")
            ->leftjoin("m_cabang as e", "e.id_cabang", "=", "a.ke_cabang")
            ->leftjoin("m_departemen as f", "f.id_departemen", "=", "a.ke_departemen")
            ->leftjoin("m_lokasi as g","g.id_lokasi","=","a.ke_lokasi")

            ->where("a.id_transfer_asset", $id)
            ->orderBy("a.id_transfer_asset", "DESC");

        $result = $query->get();

        return $result;
    }

    public function createData($request)
    {
        $qTransfer                      = new TransaktransferModel;
        # ---------------
        $qTransfer->id_asset            = setString($request->id_asset);
        $qTransfer->tgl_transfer        = setYMD($request->tgl_transfer, "/");
        $qTransfer->dari_cabang         = $request->dari_cabang;
        $qTransfer->dari_departemen     = $request->dari_departemen;
        $qTransfer->dari_lokasi         = $request->dari_lokasi;
        $qTransfer->ke_cabang           = $request->ke_cabang;
        $qTransfer->ke_departemen       = $request->ke_departemen;
        $qTransfer->ke_lokasi           = $request->ke_lokasi;
        $qTransfer->keterangan          = setString($request->keterangan);
        $qTransfer->user_id             = setString(Auth::user()->id);
        $qTransfer->create_at           = setString(date('Y-m-d H:i:s'));
        $qTransfer->update_at           = setString(date('Y-m-d H:i:s'));
        # ---------------
        $qTransfer->save();
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE TRANSFER ASSET (" . $qTransfer->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function updateData($request)
    {
        DB::table("tr_transfer_asset")
            ->where("id_transfer_asset", $request->id_transfer_asset)
            ->update([
                "tgl_transfer"      => setYMD($request->tgl_transfer, "/"),
                "id_asset"          => $request->id_asset,
                "dari_cabang"       => $request->dari_cabang,
                "dari_departemen"   => $request->dari_departemen,
                "dari_lokasi"       => $request->dari_lokasi,
                "ke_cabang"         => $request->ke_cabang,
                "ke_departemen"     => $request->ke_departemen,
                "ke_lokasi"         => $request->ke_lokasi,
                "keterangan"        => setString($request->keterangan),
                "user_id"           => setString(Auth::user()->id),
                "update_at"         => setString(date('Y-m-d H:i:s'))
            ]);
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("UPDATE TRANSFER ASSET(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function removeData($request)
    {
        DB::table("tr_transfer_asset")
            ->where("id_transfer_asset", $request->id_transfer_asset)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("DELETE CABANG (" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }
}
