<?php

namespace App\Model\Transaksi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;

class TransakhapusModel extends Model
{
    protected $table = "tr_disposal_asset";
    public $timestamps = false;

    public function getList($request = null, $offset = null, $limit = null)
    {
        $query = DB::table("tr_disposal_asset as a")
            ->select("a.*", "b.no_asset", "c.nama_barang")
            ->leftjoin("m_asset as b", "b.id_asset", "=", "a.id_asset")
            ->leftjoin("m_barang as c", "c.id_barang", "=", "b.id_barang")
            ->orderBy("a.id_disposal_asset", "asc");

        if (session()->has("SES_SEARCH_ASSET_DISPOSAL")) {
            $query->where("c.nama_barang", "LIKE", "%" . session()->get("SES_SEARCH_ASSET_DISPOSAL") . "%");
        }

        if ($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id)
    {
        $query = DB::table("tr_disposal_asset as a")
            ->select("a.*")
            ->where("a.id_disposal_asset", $id)
            ->orderBy("a.id_disposal_asset", "DESC");

        $result = $query->get();

        return $result;
    }

    public function createData($request)
    {
        $qDisposal = new TransakhapusModel;
        # ---------------
       //$qDisposal->kode       = setString($request->kode);
        $qDisposal->id_asset = setString($request->id_asset);
        $qDisposal->tgl_hapus = setYMD($request->tgl_hapus, "/");
        $qDisposal->id_disposal = $request->id_disposal;
        $qDisposal->keterangan = setString($request->keterangan);
        $qDisposal->status_approval = "Proses";

        $qDisposal->user_id = setString(Auth::user()->id);
        $qDisposal->create_at = setString(date('Y-m-d H:i:s'));
        $qDisposal->update_at = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qDisposal->save();
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE CABANG (" . $qDisposal->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);

    }

    public function updateData($request)
    {
        DB::table("tr_disposal_asset")
            ->where("id_disposal_asset", $request->id_disposal_asset)
            ->update([
                "id_asset" => setString($request->id_asset),
                "tgl_hapus" => setYMD($request->tgl_hapus, "/"),
                "id_disposal" => $request->id_disposal,
                "keterangan" => setString($request->keterangan),
                "user_id" => setString(Auth::user()->id),
                "update_at" => setString(date('Y-m-d H:i:s'))
            ]);
      
       
# ---------------

/* ----------
Logs
----------------------- */
        $qLog = new LogModel;
# ---------------;
        $qLog->createLog("UPDATE CABANG(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);

    }


    public function removeData($request)
    {
        DB::table("m_asset")
            ->where("id_asset", $request->id_asset)
            ->update([
                "nama_barang" => $request->nama_barang,


                "user_id" => setString(Auth::user()->id),
                "update_at" => setString(date('Y-m-d H:i:s'))
            ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("UPDATE CABANG(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);

        {
            DB::table("tr_disposal_asset")
                ->where("id_asset", $request->id_asset)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
        }
    }
}
