<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class LogModel extends Model
{
    protected $table = "sys_logs";

    public function createLog($desc=null, $user=null, $request=null) {
        $qLog     		      = new LogModel;
        # ---------------
        $qLog->keterangan     = setString($desc);
        $qLog->user_id 		  = $user;
        # ---------------
        $qLog->save();
    }
}
