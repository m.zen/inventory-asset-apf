<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use DateTime;
class ApiTimeOffModel extends Model
{
    protected $table    = "tm_timeoff";
    public $timestamps= false ;
   
    function getTimeOff ($nik,$tgl_awal,$tgl_akhir)
    {
       
         $query  = DB::table("tm_timeoff as a")
                            ->select("a.*","b.nik",'c.nama_jenisabsen')
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("tm_jenisabsen as c","c.id_jenisabsen","=","a.id_status_karyawan")
                            ->where("b.nik", $nik)
                            ->whereBetween('a.tgl_awal',array($tgl_awal,$tgl_akhir));
        $result = $query->get();
         return $result;
    }
    function getJenisTimeoff()
    {
    	
    	$query  = DB::table("tm_jenisabsen")
                            ->select("inisial","nama_jenisabsen");
                           
        $result = $query->get();
         return $result;
    }

    function createData($request)
    {
    	
      
		$nik 		= $request->nik;
        $name 		= $request->name;
        $userid 	= $request->id;

        // 1. Cek Group Jam Masuk
        $qgrupabsen = DB::table("p_karyawan")
        //             // ->join("tm_dataabsensi","p_karyawan.id_karyawan","tm_dataabsensi.id_karyawan")
        ->join("m_departemen","p_karyawan.id_departemen","m_departemen.id_departemen")
        ->join("tm_grupabsen","m_departemen.id_grupabsen","tm_grupabsen.id_grupabsen")
        // ->select('id_karyawan','jam_masuk','jam_masuk2','jam_keluar','jam_keluar2','nik')
        ->where("nik", $nik)
        ->first();
        // dd($qgrupabsen);

        //2. Ambil keterangan Deskripsi dari jenis Absen
        $qjenisabsen = DB::table("tm_jenisabsen")->where("id_jenisabsen", $request->id_status_karyawan)->first();
        
        $inisial = $qjenisabsen->inisial;
        $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
        // $tglygtelahdiambil= [];
        // foreach ($rangedate as $keyrange => $valuedate) {
        //     # code...

        // $cekdataabsensi = DB::table("tm_dataabsensi")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan],['tgl_absensi',"=",$valuedate]])->select('tgl_absensi')->first();
        //     if($cekdataabsensi != null){
        //     $tglygtelahdiambil[] = ' '.$valuedate;    
        //     }
        // }

        // if(!empty($tglygtelahdiambil)){
        //     $response = ['Tanggal pengajuan sudah pernah diambil'.implode(',',$tglygtelahdiambil)];
        //     return array("status"=>'failed',"response"=>$response);
        // }else{

        //3. Cek Pengajuan user (Pernah/baru)
        $cektimeoff = DB::table("tm_timeoff")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan]])->select('tm_timeoff.tgl_awal','tm_timeoff.tgl_akhir')->get();
        $hitungtglsama = 0;
            if(count($cektimeoff) > 0){
                $i =0;
                foreach ($cektimeoff as $key => $value) {
                    # code...
                    $rangelastdate = $this->getDatesFromRange2($value->tgl_awal,$value->tgl_akhir);
                    foreach ($rangelastdate as $key2 => $variable) {
                        # code...
                    $tglygtelahdiajukan[$i] = $variable; 
                    $i++;
                    }
                }

                foreach ($rangedate as $key3 => $value3) {
                    # code...
                    if(in_array($value3, $tglygtelahdiajukan)){
                        $hitungtglsama = $hitungtglsama+1;
                        $tglsama[] = $value3;
                    }
                }

            } 

        if($hitungtglsama > 0){
            $ket[] = 'Tanggal pengajuan sudah pernah diambil dan klik edit untuk perubahan';
            return array("status"=>'failed', "response"=>array_merge($ket,$tglsama));
        }else{

            $pengajuan =  DB::table('p_karyawan')
                        ->join('tm_dataapproval','p_karyawan.id_jabatan','tm_dataapproval.id_jabatan')
                        ->select('p_karyawan.id_karyawan','tm_dataapproval.id_cabang_approval', 'tm_dataapproval.id_departemen_approval','tm_dataapproval.id_approvaller','tm_dataapproval.approvallevel','tm_dataapproval.id_jabatan',
                        'p_karyawan.id_cabang','p_karyawan.id_departemen','p_karyawan.id_jabatan')
                        ->where([['nik',$nik],['tm_dataapproval.id_cabang', $qgrupabsen->id_cabang],['tm_dataapproval.id_departemen', $qgrupabsen->id_departemen]])
                        ->get();
            // $jumlahapprovalygterdaftar = 0;
            
            // 4. Cek Approverer (Ada/tidak ada)
            $totalygaktif =0;
            foreach ($pengajuan as $key_pengajuan => $value_pengajuan) {
                # code...
                if($value_pengajuan->id_approvaller == 0 || $value_pengajuan->id_approvaller == '' || $value_pengajuan->id_approvaller == null){
                    $jabatan = DB::table('m_jabatan')->select('nama_jabatan')->where('id_jabatan', $value_pengajuan->id_jabatan)->first();
                    $nama_jabatan = $jabatan->nama_jabatan;
                    // $jumlahapprovalygterdaftar = $jumlahapprovalygterdaftar+1;
                    $pesan = ['Jabatan '.strtolower($nama_jabatan).' tidak bisa melakukan pengajuan time off karena tidak memiliki approvaller'];
                    return array("status"=>'failed', "response"=>$pesan);
                }else{
                    $qp_karyawan = DB::table('p_karyawan')->where([['id_cabang', (int)$value_pengajuan->id_cabang_approval],['id_departemen', $value_pengajuan->id_departemen_approval],['id_jabatan', $value_pengajuan->id_approvaller],["aktif",1]])->first();
                    // $jabatan = DB::table('m_jabatan')->select('nama_jabatan')->where('id_jabatan', $value_pengajuan->id_jabatan)->first();
                    // $nama_jabatan = $jabatan->nama_jabatan;
                    if($qp_karyawan == null){
                        $totalygaktif = $totalygaktif+0;                     
                    }else{
                        $totalygaktif = $totalygaktif+1;
                    }
                }
            }
            
            if($totalygaktif == 0){
                $pesan = ['Status dari Approvaller tidak aktif'];
                        return array("status"=>'failed', "response"=>$pesan); 
            }



            if(count($pengajuan)>0){

            $qdataabsensi              = new PengajuantmModel;
            # ---------------
            $grAbasen           = $qdataabsensi->getIdKaryawan(Auth::user()->nik);
            /**/
            $qdataabsensi->id_karyawan          = setString($grAbasen->id_karyawan);
            $qdataabsensi->id_status_karyawan   = setString($request->id_status_karyawan);
            $qdataabsensi->komentar             = setString($request->komentar);
            $qdataabsensi->tgl_awal             = setYMD($request->tgl_awal,"/");
            $qdataabsensi->tgl_akhir            = setYMD($request->tgl_akhir,"/");
            // $qdataabsensi->id_delegasi           = setString($request->id_delegasi);
            $qdataabsensi->user_id              = setString(Auth::user()->id);
            $qdataabsensi->create_at            = setString(date('Y-m-d H:i:s'));
            $qdataabsensi->update_at            = setString(date('Y-m-d H:i:s'));

            if($request->foto != null){
            // Simpan file gabar folder server
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'profile '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/foto_timeoff', $namaFile);
            // $compress_images = $this->compressImage('app/foto_timeoff/'.$name, $foto);
            //             $factory = new \ImageOptimizer\OptimizerFactory();
            // $optimizer = $factory->get();

            //5. Kompres file gambar
            $filepath = public_path().'/app/foto_timeoff/'.$namaFile;
            Image::load($filepath)
               ->optimize()
               ->save($filepath);
            // $optimizerChain = OptimizerChainFactory::create();

            // $optimizerChain->optimize(public_path('app/foto_timeoff/'.$namaFile));
            // $optimizer->optimize($filepath);
            // dd($optimizer);
            $qdataabsensi->foto                 = $namaFile;
            }
			if($request->foto2 != null){
                $foto = $request->file('foto2');
                $datenow = date('Y-m-d');
                $namaFile = 'profile '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
                $foto->move('app/foto_timeoff', $namaFile);
                $filepath = public_path().'/app/foto_timeoff/'.$namaFile;
                Image::load($filepath)
                   ->optimize()
                   ->save($filepath);
                $qdataabsensi->foto                 = $namaFile;
            }

            # ---------------
            $qdataabsensi->save();

            //6. Cek hari libur
            $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
            foreach ($hari_libur as $key => $value) {
                # code...
                $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
            }

            foreach ($rangedate as $key => $value) {
            # code...
            $hari = new DateTime($value);
            $no_hari = $hari->format('w');
            $ceklibur = in_array($value, $tgl_libur);
            if($no_hari != 0 && $ceklibur == false){
            if($no_hari == 6){
                $scheduli_in = $qgrupabsen->jam_masuk2;
                $scheduli_out = $qgrupabsen->jam_keluar2;
            }else{
                $scheduli_in = $qgrupabsen->jam_masuk;
                $scheduli_out = $qgrupabsen->jam_keluar;
            }
            
            // 7. 
            $cek = DB::table("tm_dataabsensi")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan],['tgl_absensi',"=",$value]])->first();
            if($cek == null){
                if($request->id_status_karyawan == 1){
                    $ket_masuk             = setString($request->komentar);
                    $datapengajuan = [
                            'id_karyawan'=> $qgrupabsen->id_karyawan,
                            'nik'=> $nik,
                            'tgl_absensi'=> $value,
                            'schedule_in' => $scheduli_in,
                            'schedule_out' => $scheduli_out,
                            'ket_masuk' => $ket_masuk,
                            'status_absensi' => 'Waiting',
                            'status_potong_makan' => 1,
                            'status_potong_gaji' => 0,
                            'keterangan' => $ket_masuk,
                            'user_id' => $userid,
                        ];
                }else if($request->id_status_karyawan == 7){
                    $ket_keluar             = setString($request->komentar);
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'ket_keluar' => $ket_keluar,
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'keterangan' => $ket_keluar,
                        'user_id' => $userid,
                    ];
                }else{
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'keterangan' => setString($request->komentar),
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'user_id' => $userid,
                    ];
                }
                DB::table("tm_dataabsensi")->insert($datapengajuan);
            }else{
                if($request->id_status_karyawan == 1){
                    $ket_masuk             = setString($request->komentar);
                    $datapengajuan = [
                            'id_karyawan'=> $qgrupabsen->id_karyawan,
                            'nik'=> $nik,
                            'tgl_absensi'=> $value,
                            'schedule_in' => $scheduli_in,
                            'schedule_out' => $scheduli_out,
                            'ket_masuk' => $ket_masuk,
                            'status_absensi' => 'Waiting',
                            'status_potong_makan' => 1,
                            'status_potong_gaji' => 0,
                            'keterangan' => $ket_masuk,
                            'user_id' => $userid,
                        ];
                }else if($request->id_status_karyawan == 7){
                    $ket_keluar             = setString($request->komentar);
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'ket_keluar' => $ket_keluar,
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'keterangan' => $ket_keluar,
                        'user_id' => $userid,
                    ];
                }else{
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'keterangan' => setString($request->komentar),
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'user_id' => $userid,
                    ];
                }
                // $cek->update($datapengajuan);
                DB::table("tm_dataabsensi")->where([
                                            ['id_karyawan',"=", $qgrupabsen->id_karyawan],
                                            ['tgl_absensi',"=",$value],
                                            ])->update($datapengajuan);
            }
            }
            }
            
            
            $newdata = DB::table("tm_timeoff")->where([['id_karyawan',"=",$grAbasen->id_karyawan],['tgl_awal',"=",setYMD($request->tgl_awal,"/")],['tgl_akhir',"=",setYMD($request->tgl_akhir,"/")]])->select('*')->first();
            foreach ($pengajuan as $keypengajuan => $valuepengajuan) {
                # code...
                // $cekpengajuanabsen = DB::table('tm_pengajuanabsen')->where([['id_tm',$newdata->id_tm],['order', '=',1] ,['statusapproval', '!=',0]])->first();
                // if($cekpengajuanabsen == null){
                // $datapengajuanabsen = [
                //         'id_tm'=> $newdata->id_tm,
                //         'id_karyawan'=> $valuepengajuan->id_approvaller,
                //         'order'=> $valuepengajuan->approvallevel,
                //         'statusapproval'=> 0,
                //         'create_at' => setString(date('Y-m-d H:i:s')),
                //         'update_at' => setString(date('Y-m-d H:i:s')),
                //       ];
                // DB::table("tm_pengajuanabsen")->insert($datapengajuanabsen);
                // }
                $qp_karyawan = DB::table('p_karyawan')->where([['id_cabang', (int)$valuepengajuan->id_cabang_approval],['id_departemen', $valuepengajuan->id_departemen_approval],['id_jabatan', $valuepengajuan->id_approvaller],["aktif",1]])->first();
                // dd($valuepengajuan->id_approvaller, $qp_karyawan, $pengajuan);
                // dd($qp_karyawan->id_karyawan);
                    # code...
                if($qp_karyawan != null){
                    $datapengajuanabsen = [
                        'id_tm'=> $newdata->id_tm,
                        'id_karyawan'=> $qp_karyawan->id_karyawan,
                        'order'=> $valuepengajuan->approvallevel,
                        'create_at' => setString(date('Y-m-d H:i:s')),
                        'update_at' => setString(date('Y-m-d H:i:s')),
                      ]; 
                DB::table("tm_pengajuanabsen")->insert($datapengajuanabsen);
                if($qp_karyawan->email != null || $qp_karyawan->email != ''){
                    $title = '(Approvaller) Pengajuan Time Off dari '.$grAbasen->nama_karyawan;
                    $pesan = 'Pesan Terkirim';
                    
                     $data = array(
                       'tgl' => date('d-m-Y',strtotime($newdata->tgl_awal)).' s/d '.date('d-m-Y', strtotime($newdata->tgl_akhir)),
                       'nama_karyawan' => $grAbasen->nama_karyawan,
                       'link' => 'https://hrbs.mitrafin.co.id/daftarapproval/index',
                       'email' => $qp_karyawan->email,
                       'nama_pengirim' => $qp_karyawan->nama_karyawan,
                     );

                     $this->sendEmailApprove($data,$pesan,$title);
                    }
					$title = '(Notifikasi) Pengajuan Time Off dari '.$grAbasen->nama_karyawan;
                    $pesan = 'Pesan Terkirim';
					$data = array(
                       'tgl' => date('d-m-Y',strtotime($newdata->tgl_awal)).' s/d '.date('d-m-Y', strtotime($newdata->tgl_akhir)),
                       'nama_karyawan' => $grAbasen->nama_karyawan,
                       'link' => 'https://hrbs.mitrafin.co.id/daftarapproval/index',
					   'email' => 's.winardi@buana-sejahtera.com',
                       'nama_pengirim' => 'SAMUEL WINARDI',
                     );

                     $this->sendEmailApprove($data,$pesan,$title);
                }
            }
            // SELECT a.id_karyawan, a.id_jabatan, b.id_approvaller,b.approvallevel FROM p_karyawan AS a LEFT JOIN tm_dataapproval AS b  ON (b.id_jabatan = a.id_jabatan) WHERE a.nik="2000.0816.0188"

            // SELECT id_karyawan,nama_karyawan FROM p_karyawan WHERE id_jabatan=14

            return array("status"=>'success');
            }else{
            $pesan = ['Approval Tidak Terdaftar'];
            return array("status"=>'failed', "response"=>$pesan);

            }
        }
    // }
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE DATAABSENSI (" . $qdataabsensi->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
    }

 
}
