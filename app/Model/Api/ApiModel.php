<?php

namespace App\Model\Api;


// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class ApiModel extends Authenticatable
{

    use HasApiTokens, Notifiable;
    protected $table 	= "sys_users";
    public function getUser($nik) {
        $query  = DB::table("sys_users")
                            ->select( "*")
                            ->where("nik", $nik);

        $result = $query->get();

        return $result;
    }
    public function getPeriodeAbsen() {
        $query  = DB::table("m_profilesystem")
                            ->select("id_profilesystem","tgl_absenawal","tgl_absenakhir");
                            

        $result = $query->get();

        return $result;
    }       
}
