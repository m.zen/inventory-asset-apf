<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class ApiProfileModel extends Model
{
     public function getProfilekry($nik) {
        $query  =DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.nik", $nik)
                            ;

    
        $result = $query->get();

        return $result;
    }   
}
