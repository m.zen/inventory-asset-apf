<?php

namespace App\Model\Api;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use DateTime;
class ApiAbsensiModel extends Model
{
    protected $table    = "tm_dataabsensi";
    public $timestamps= false ;
   
    function getApiabsensi ($nik,$tgl_awal,$tgl_akhir)
    {
       
         $query  = DB::table("tm_dataabsensi")
                            ->select("*")
                            ->where("nik", $nik)
                            ->whereBetween('tgl_absensi',array($tgl_awal,$tgl_akhir));
        $result = $query->get();
         return $result;
    }
    function getApisum_absensi ($nik,$tgl_awal,$tgl_akhir)
    {
       
         $query  = DB::table("tm_dataabsensi")
                            ->select("status_absensi",db::Raw("COUNT(status_absensi) as Jumlah"))
                            ->where("nik", $nik)
                            ->whereBetween('tgl_absensi',array($tgl_awal,$tgl_akhir))
                            ->groupBy('status_absensi');
        $result = $query->get();
         return $result;
    }
    
 
}
