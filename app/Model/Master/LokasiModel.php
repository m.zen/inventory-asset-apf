<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class LokasiModel extends Model
{
    protected $table    = "m_lokasi";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_lokasi as a")
                            ->select("a.id_lokasi" ,"a.nama_lokasi")
			  
                            ->orderBy("a.id_lokasi", "asc");

        if(session()->has("SES_SEARCH_LOKASI")) {
            $query->where("nama_lokasi", "LIKE", "%" . session()->get("SES_SEARCH_LOKASI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_lokasi as a")
                            ->select("a.id_lokasi", "a.nama_lokasi")
			   
                            ->where("a.id_lokasi", $id)
                            ->orderBy("a.nama_lokasi", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qlokasi              = new LokasiModel;
        # ---------------
       // $qlokasi->kode       = setString($request->kode);
        $qlokasi->nama_lokasi      = setString($request->nama_lokasi);
          $qlokasi->user_id          = setString(Auth::user()->id);
        $qlokasi->create_at        = setString(date('Y-m-d H:i:s'));
        $qlokasi->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qlokasi->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qlokasi->id_lokasi . ") " . strtoupper($request->nama_lokasi), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_lokasi")
                             ->where("id_lokasi", $request->id_lokasi)
                            ->update([ 
                                        "nama_lokasi"=>$request->nama_lokasi,
                                       
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_lokasi . ") " . strtoupper($request->nama_lokasi), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_lokasi")
                             ->where("id_lokasi", $request->id_lokasi)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_lokasi . ") " . strtoupper($request->nama_lokasi), Auth::user()->id, $request);
    }
}

