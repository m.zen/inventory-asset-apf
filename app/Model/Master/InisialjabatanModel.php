<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class InisialjabatanModel extends Model
{
     protected $table    = "m_inisialjabatan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_inisialjabatan as a")
                            ->select("a.id_inisialjabatan", "a.nama_inisialjabatan","a.kategoricabang","b.nama_departemen","c.nama_jabatan")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as c","c.id_jabatan","=","a.id_jabatan")
                            
                            ->orderBy("id_inisialjabatan", "DESC");

        if(session()->has("SES_SEARCH_INISIALJABATAN")) {
            $query->where("nama_inisialjabatan", "LIKE", "%" . session()->get("SES_SEARCH_INISIALJABATAN") . "%")
                  ->orwhere("c.nama_jabatan", "LIKE", "%" . session()->get("SES_SEARCH_INISIALJABATAN") . "%");  
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_inisialjabatan as a")
                            ->select("a.id_inisialjabatan", "a.nama_inisialjabatan","a.kategoricabang","b.id_departemen","c.id_jabatan")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as c","c.id_jabatan","=","a.id_jabatan")
                            
                            ->where("a.id_inisialjabatan", $id)
                            ->orderBy("a.nama_inisialjabatan", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qinisialjabatan              = new InisialjabatanModel;
        # ---------------
      
        $qinisialjabatan->nama_inisialjabatan      = setString($request->nama_inisialjabatan);
        $qinisialjabatan->kategoricabang           = setString($request->kategoricabang);
        $qinisialjabatan->id_departemen            = setString($request->id_departemen);
        $qinisialjabatan->id_jabatan               = setString($request->id_jabatan);
        $qinisialjabatan->user_id                  = setString(Auth::user()->id);
        $qinisialjabatan->create_at                = setString(date('Y-m-d H:i:s'));
        $qinisialjabatan->update_at                = setString(date('Y-m-d H:i:s'));
        # ---------------
        $qinisialjabatan->save();


        //Update Karyawan
        if($request->id_departemen==1000)
        {
                DB::table("p_karyawan as a")
				 ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")

                             ->where("b.kategori", $request->kategoricabang)
                             ->where("a.id_jabatan", $request->id_jabatan)
                             ->where("a.aktif", 1)
                            ->update([  "a.nama_inisialjabatan"=>$request->nama_inisialjabatan,
                                        "a.user_id"=>setString(Auth::user()->id),
                                         "a.update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        }
       else
       {
       			   DB::table("p_karyawan as a")
				 ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")

                             ->where("b.kategori", $request->kategoricabang)
                              ->where("a.id_departemen", $request->id_departemen)
                             ->where("a.id_jabatan", $request->id_jabatan)
                              ->where("a.aktif", 1)
                            ->update([  "a.nama_inisialjabatan"=>$request->nama_inisialjabatan,
                                        "a.user_id"=>setString(Auth::user()->id),
                                         "a.update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);

       }






        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE INISIALJABATAN (" . $qinisialjabatan->id_inisialjabatan . ") " . strtoupper($request->nama_inisialjabatan), Auth::user()->id, $request);


         


    }

    public function updateData($request) {
         DB::table("m_inisialjabatan")
                             ->where("id_inisialjabatan", $request->id_inisialjabatan)
                            ->update([  "nama_inisialjabatan"=>$request->nama_inisialjabatan,
                                        "kategoricabang" => $request->kategoricabang,
                                        
                                        "id_jabatan"=> $request->id_jabatan,
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
               if($request->id_departemen==1000)
        {
                DB::table("p_karyawan as a")
				 ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")

                             ->where("b.kategori", $request->kategoricabang)
                             ->where("a.id_jabatan", $request->id_jabatan)
                            ->where("a.aktif", 1)
                              
                            
                            ->update([  "a.nama_inisialjabatan"=>$request->nama_inisialjabatan,
                                        "a.user_id"=>setString(Auth::user()->id),
                                         "a.update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);

        }
       else
       {
       			   DB::table("p_karyawan as a")
				 ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")

                             ->where("b.kategori", $request->kategoricabang)
                             ->where("a.id_departemen", $request->id_departemen)
                             ->where("a.id_jabatan", $request->id_jabatan)
                              ->where("a.aktif", 1)
                            ->update([  "a.nama_inisialjabatan"=>$request->nama_inisialjabatan,
                                        "a.user_id"=>setString(Auth::user()->id),
                                         "a.update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);

       }


           



                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE INISIALJABATAN(" . $request->id_inisialjabatan . ") " . strtoupper($request->nama_inisialjabatan), Auth::user()->id, $request);
    }


     public function refreshData()
     {
        // $query  = DB::table("id_inisialjabatan")
        //                     ->select("*")
        //                     ->orderBy("b.id_level","ASC");
                            
                            
        //              $result=$query->get();
                    
        //              $kolom = "B";
        //              foreach($result as $row) 



     }

    public function removeData($request) {
         DB::table("m_inisialjabatan")
                             ->where("id_inisialjabatan", $request->id_inisialjabatan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE INISIALJABATAN (" . $request->id_inisialjabatan . ") " . strtoupper($request->nama_inisialjabatan), Auth::user()->id, $request);
    }
}
