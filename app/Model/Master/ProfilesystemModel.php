<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class ProfilesystemModel extends Model
{
   protected $table    = "m_profilesystem";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_profilesystem")
                            ->select("*")
                            ->orderBy("id_profilesystem", "DESC");

        if(session()->has("SES_SEARCH_CABANG")) {
            $query->where("nama_profilesystem", "LIKE", "%" . session()->get("SES_SEARCH_CABANG") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_profilesystem")
                            ->select("*")
                            ->where("id_profilesystem", $id)
                            ->orderBy("nama_perusahaan", "DESC");

        $result = $query->get();

        return $result;
    }    
/*
    public function createData($request) {
        $qprofilesystem              = new ProfilesystemModel;
        # ---------------
       // $qprofilesystem->kode       = setString($request->kode);
        $qprofilesystem->kode_profilesystem      = setString($request->kode_profilesystem);
        $qprofilesystem->nama_profilesystem      = setString($request->nama_profilesystem);
        $qprofilesystem->user_id          = setString(Auth::user()->id);
        $qprofilesystem->create_at        = setString(date('Y-m-d H:i:s'));
        $qprofilesystem->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qprofilesystem->save();
        /* ----------
         Logs
        
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qprofilesystem->id_profilesystem . ") " . strtoupper($request->nama_profilesystem), Auth::user()->id, $request);
    }
*/
    public function updateData($request) {
         DB::table("m_profilesystem")
                             ->where("id_profilesystem", $request->id_profilesystem)
                            ->update([ "nama_perusahaan"=>$request->nama_perusahaan,
                            			"alamat_perusahaan"=>$request->alamat_perusahaan,
                            			"bulanpayroll"=>$request->bulanpayroll,
                            			"tahunpayroll"=>$request->tahunpayroll,
                            			"tgl_absenawal"=>setYMD($request->tgl_absenawal,"/"),
                            			"tgl_absenakhir"=>setYMD($request->tgl_absenakhir,"/"),
                                        "tgl_payrollawal"=>setYMD($request->tgl_payrollawal,"/"),
                                        "tgl_payrollakhir"=>setYMD($request->tgl_payrollakhir,"/"),
                                        "max_rate_bpjstk"=> setNoComma($request->max_rate_bpjstk),
                                        "max_rate_bpjskes"=>setNoComma($request->max_rate_bpjskes),
                            			"dibuatoleh"=>$request->dibuatoleh,
                            			"diperiksaoleh"=>$request->diperiksaoleh,
                            			"disetujuioleh"=>$request->disetujuioleh  ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_profilesystem . ") " . strtoupper($request->nama_profilesystem), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_profilesystem")
                             ->where("id_profilesystem", $request->id_profilesystem)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_profilesystem . ") " . strtoupper($request->nama_profilesystem), Auth::user()->id, $request);
    }
}
