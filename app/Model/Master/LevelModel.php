<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class LevelModel extends Model
{
   protected $table    = "m_level";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_level")
                            ->select("id_level","nama_level","tunj_jabatan","uang_makan")
                            ->orderBy("id_level", "DESC");

        if(session()->has("SES_SEARCH_LEVEL")) {
            $query->where("nama_level", "LIKE", "%" . session()->get("SES_SEARCH_LEVEL") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_level")
                            ->select("id_level", "nama_level","tunj_jabatan","uang_makan")
                            ->where("id_level", $id)
                            ->orderBy("nama_level", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qlevel              = new LevelModel;
        # ---------------
     
        $qlevel->nama_level      = setString($request->nama_level);
        //$qlevel->tunj_jabatan    = setNoComma($request->tunj_jabatan);
        //$qlevel->uang_makan      = setNoComma($request->uang_makan);
        $qlevel->user_id         = setString(Auth::user()->id);
        $qlevel->create_at       = setString(date('Y-m-d H:i:s'));
        $qlevel->update_at       = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qlevel->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE LEVEL (" . $qlevel->id_level . ") " . strtoupper($request->nama_level), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_level")
                             ->where("id_level", $request->id_level)
                            ->update([ "nama_level"=>$request->nama_level,
                                      //  "tunj_jabatan"=> setNoComma($request->tunj_jabatan),
                                       // "uag_makan"=> setNoComma($request->uang_makan),
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);



            /*

                Update Tunjangan Jabatan di Tabel Karyawan                          

            */
       
              
        # ---------------
            
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE LEVEL(" . $request->id_level . ") " . strtoupper($request->nama_level), Auth::user()->id, $request);
    }

    public function removeData($request) {
        DB::table("m_level")
                             ->where("id_level", $request->id_level)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE LEVEL (" . $request->id_level . ") " . strtoupper($request->nama_level), Auth::user()->id, $request);
    }
}
