<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;
use Spatie\ImageOptimizer\Optimizers\Pngquant;
use Spatie\Image\Image;

class ReferensiModel extends Model
{
    protected $table = "m_referensi";
    public $timestamps = false;

    public function getList($request = null, $offset = null, $limit = null)
    {
        $query = DB::table("m_referensi")
            ->select("*")
            ->orderBy("id_referensi", "asc");

        if (session()->has("SES_SEARCH_REFERENSI")) {
            $query->where("no_referensi", "LIKE", "%" . session()->get("SES_SEARCH_REFERENSI") . "%");
        }

        if ($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id)
    {
        $query = DB::table("m_referensi")
            ->select("*")
            ->where("id_referensi", $id)
            ->orderBy("id_referensi", "DESC");

        $result = $query->get();

        return $result;
    }

    public function createData($request)
    {
        $referensi = new ReferensiModel;
        $referensi->jenis_referensi = $request->jenis_referensi;
        $referensi->no_referensi = $request->no_referensi;
      
      if ($request->foto != null) {
            // Simpan file gabar folder server
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'Dokumen-' . $request->no_referensi . ' ' . $datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/photo_dokumen', $namaFile);
            // $compress_images = $this->compressImage('app/foto_timeoff/'.$name, $foto);
            //             $factory = new \ImageOptimizer\OptimizerFactory();
            // $optimizer = $factory->get();

            //5. Kompres file gambar

            $filepath = public_path() . '/app/photo_dokumen/' . $namaFile;

            Image::load($filepath)
                ->optimize()
                ->save($filepath);
            // $optimizerChain = OptimizerChainFactory::create();

            // $optimizerChain->optimize(public_path('app/foto_timeoff/'.$namaFile));
            // $optimizer->optimize($filepath);
            // dd($optimizer);
            //dd($namaFile);
            $referensi->gambar_referensi = $namaFile;
        }


        $referensi->user_id = setString(Auth::user()->id);
        $referensi->create_at = setString(date('Y-m-d H:i:s'));
        $referensi->update_at = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $referensi->save();
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE CABANG (" . $referensi->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function updateData($request)
    {
        $namaFile = $request->nama_gambar;

        if ($request->foto != null) {
            // Simpan file gabar folder server
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'Aset-' . $request->id_barang . ' ' . $datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/photo_dokumen', $namaFile);
            $filepath = public_path() . '/app/photo_dokumen/' . $namaFile;
            Image::load($filepath)
                ->optimize()
                ->save($filepath);
            // $optimizerChain = OptimizerChainFactory::create();

            // $optimizerChain->optimize(public_path('app/foto_timeoff/'.$namaFile));
            // $optimizer->optimize($filepath);
            // dd($optimizer);
            //dd($namaFile);

        }

        DB::table("m_referensi")
            ->where("id_referensi", $request->id_referensi)
            ->update([
                "jenis_referensi" => $request->jenis_referensi,
                "no_referensi" => $request->no_referensi,
               "gambar_referensi" => $namaFile,
               "user_id" => setString(Auth::user()->id),
                "update_at" => setString(date('Y-m-d H:i:s'))
            ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("UPDATE CABANG(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function removeData($request)
    {
        DB::table("m_referensi")
            ->where("id_referensi", $request->id_asset)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
        $qLog = new LogModel;
            # ---------------;
        $qLog->createLog("DELETE CABANG (" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }
}
