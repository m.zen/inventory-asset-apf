<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;

class MasterModel extends Model
{
    public function getSelectGroup() {
        $query = DB::select("SELECT id, name FROM sys_groups ORDER BY id");

        return $query;
    }
    public function getSelectWilayahCabang() {
        $query = DB::select("SELECT id_wilayah as id, nama_wilayah as name FROM m_wilayah ORDER BY id_wilayah");

        return $query;
    }

     public function getSelectApprovaller() {
        $query = DB::select("SELECT id_jabatan as id, nama_jabatan as name FROM m_jabatan where id_level >1 ORDER BY id");

        return $query;
    }


    public function getSelectMerk() {
        $query = DB::select("SELECT id_merk as id, nama_merk as name FROM m_merk ORDER BY id_merk");

        return $query;
    }

    

    public function getSelectKodePos() {
        $query = DB::select("SELECT kode AS id, CONCAT(kelurahan,'/',kecamatan,'/',kabupaten,'/',provinsi) as name FROM tabel_data_kodepos ORDER BY id");

        return $query;
    }

    public function getSelectGrupbarang() {
        $query = DB::select("SELECT id_grupbarang AS id, nama_grupbarang as name FROM m_grupbarang  ORDER BY id_grupbarang");

        return $query;
    }

    public function getSelectKategori() {
        $query = DB::select("SELECT id_kategori AS id, nama_kategori as name FROM m_kategori  ORDER BY id_kategori");

        return $query;
    }

 public function getSelectCabang() {
        $query = DB::select("SELECT id_cabang AS id, CONCAT(nama_cabang, ' / ', kode_cabang) as name FROM m_cabang where id_cabang != '' ORDER BY id_cabang");

        return $query;
    }

    public function getSelectBarang() {
        $query = DB::select("SELECT id_barang AS id, nama_barang as name FROM m_barang  ORDER BY id_barang");

        return $query;
    }
    public function getSelectAsset() {
        $query = DB::select("SELECT a.id_asset as id,  CONCAT(a.no_asset, ' / ', b.nama_barang) as name FROM m_asset AS a LEFT JOIN m_barang AS b ON b.id_barang= a.id_barang ORDER BY a.id_asset");

        return $query;
    }
    public function getSelectAssetDetail() {
        $query = DB::select("SELECT a.id_asset as id,  CONCAT(IFNULL(a.no_asset, ''), ' / ', b.nama_barang) as name FROM m_asset AS a LEFT JOIN m_barang AS b ON b.id_barang= a.id_barang ORDER BY a.id_asset");

        return $query;
    }
    public function getSelectLokasi() {
        $query = DB::select("SELECT id_lokasi AS id, nama_lokasi as name FROM m_lokasi  ORDER BY id_lokasi");

        return $query;
    }

public function getSelectPtkp() {
        $query = DB::select("SELECT id_ptkp AS id, status as name FROM p_ptkp where id_ptkp != '' ORDER BY id_ptkp");

        return $query;
    }




public function getSelectDepartemen() {
        $query = DB::select("SELECT id_departemen AS id, nama_departemen as name FROM m_departemen where id_departemen != '' ORDER BY id_departemen");

        return $query;
    }

  public function getSelectDivisi() {
        $query = DB::select("SELECT id_divisi AS id, nama_divisi as name FROM m_divisi where id_divisi != '' ORDER BY id_divisi");

        return $query;
    }

public function getSelectLevel() {
        $query = DB::select("SELECT id_level AS id, nama_level as name FROM m_level where id_level != '' ORDER BY id_level");

        return $query;
    }


    public function getSelectPekerjaan() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM tabel_pekerjaan where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectJabatan() {
           $query = DB::select("SELECT id_jabatan AS id, nama_jabatan as name FROM m_jabatan where id_jabatan != '' ORDER BY id_jabatan");
        return $query;
    }
    public function getSelectGrading() {
           $query = DB::select("SELECT id_grading AS id, CONCAT(nama_grading,' ( ',batasbawah,  ' - ',  batasatas,')')  as name FROM p_grading where id_grading != '' ORDER BY id_grading");
        return $query;
    }



    public function getSelectKaryawan() {
        $query = DB::select("SELECT id_karyawan AS id,CONCAT(nama_karyawan, ' / ', nik) as name FROM p_karyawan where aktif = 1 ORDER BY id_karyawan");

        return $query;
    }
     public function getSelectKaryawankontrak() {
        $query = DB::select("SELECT id_karyawan AS id,CONCAT(nama_karyawan, ' / ', nik) as name FROM p_karyawan where status_kerja !='Tetap' and aktif=1 ORDER BY id_karyawan");

        return $query;
    }

 

 public function getSelectKaryawankontrak1() {
        $query = DB::select("SELECT id_karyawan AS id,CONCAT(nama_karyawan, ' / ', nik) as name FROM p_karyawan where status_kerja !='Tetap' and aktif=1  and (keterangan_kontrak LIKE '%KONTRAK%' or keterangan_kontrak= 'PERCOBAAN') ORDER BY id_karyawan");

        return $query;
    }
    public function getSelectKaryawankontrak2() {
        $query = DB::select("SELECT id_karyawan AS id,CONCAT(nama_karyawan, ' / ', nik) as name FROM p_karyawan where status_kerja !='Tetap' and aktif=1 and keterangan_kontrak LIKE '%KONTRAK%' ORDER BY id_karyawan");

        return $query;
    }
    public function getSelectKaryawanNonAktif() {
    $query = DB::select("SELECT id_karyawan AS id,CONCAT(nama_karyawan, ' / ', nik) as name FROM p_karyawan where aktif = 0 ORDER BY id_karyawan");

    return $query;
}

public function getSelectKopKaryawan() {
         $query = DB::select("SELECT a.id_karyawan as id,a.nama_karyawan as name FROM p_karyawan as a LEFT JOIN 
                            p_koperasi as b ON ( a.id_karyawan= b.id_karyawan) WHERE b.id_karyawan IS NULL");

        // $query = DB::select("SELECT a.id_karyawan as id,a.nama_karyawan as name FROM p_karyawan as a 
        //                         where a.id_karyawan not in (select id_karyawan from p_koperasi as b where b.id_karyawan = a.id_karyawan)");

        // return $query;
   // $query = DB::select("SELECT a.id_karyawan AS id, a.nama_karyawan as name FROM p_karyawan as a LEFT JOIN 
     //                        p_koperasi as b ON ( a.id_karyawan= b.id_karyawan) WHERE b.id_karyawan IS NULL ORDER BY a.id_karyawan");

        return $query;
    }

    public function getSelectDisposal() {
        $query = DB::select("SELECT id_disposal AS id, nama_disposal as name FROM m_disposal order by nama_disposal");
      
                           
        return $query;
    }
    public function getSelectAngota() {
        $query = DB::select("SELECT tr_disposal_asset AS id, nama_karyawan as name FROM p_koperasi left join 
                              p_karyawan on p_karyawan.id_karyawan = p_koperasi.id_karyawan");
      
                           
        return $query;
    }

    public function getSelectBank() {
        $query = DB::select("SELECT id_bank AS id, nama_bank as name FROM p_bank where id_bank != '' ORDER BY id_bank");

        return $query;
    }

    public function getSelectGrupabsen() {
        $query = DB::select("SELECT id_grupabsen AS id, nama_grupabsen as name FROM tm_grupabsen where id_grupabsen != '' ORDER BY id_grupabsen");

        return $query;
    }


    public function getSelectDealer() {
        $query = DB::select("SELECT kode AS id, nama_dealer as name FROM tabel_dealer where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectKodeGroup1() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM tabel_group1 where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectKodeGroup2() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM tabel_group2 where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectKodeGroup3() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM tabel_group3 where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectWilayah() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM tabel_wilayah where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectSidDati() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM sid_dati2 where kode != '' ORDER BY kode");

        return $query;
    }

    public function getSelectSidStatusGelar() {
        $query = DB::select("SELECT kode AS id, keterangan as name FROM sid_status_gelar where kode != '' ORDER BY kode");

        return $query;
    }

    
   

     


    public function getKodePos($id) {
        $query  = DB::table("tabel_data_kodepos")
                            ->select("CONCAT(kelurahan,'/',kecamatan,'/',kabupaten,'/',provinsi) as keterangan")
                            ->where("kode","=",$id);
                            
        return $query;
    }

   
    public function getJabatan($id) {
        $query  = DB::table("m_jabatan")
                            ->select("nama_jabatan")
                            ->where("id_jabatan","=",$id);

        return $query;
    }

 
    public function getCabang($id) {
        $query  = DB::table("m_cabang")
                            ->select("nama_cabang")
                            ->where("id_cabang","=",$id);

        return $query;
    }

     public function getDepartemen($id) {
        $query  = DB::table("m_departemen")
                            ->select("nama_departemen")
                            ->where("id_departemen","=",$id);

        return $query;
    }

    public function getProduk($id) {
        $query  = DB::table("tabel_produk")
                            ->select("keterangan")
                            ->where("kode","=",$id);

        return $query;
    }

    public function getKaryawan($id) {
        $query  = DB::table("p_karyawan")
                            ->select("*")
                            ->where("id_karyawan","=",$id);

        return $query;
    }

    public function getDealer($id) {
        $query  = DB::table("tabel_dealer")
                            ->select("nama_dealer")
                            ->where("kode","=",$id);

        return $query;
    }

    public function getKodeGroup1($id) {
        $query  = DB::table("tabel_group1")
                            ->select("keterangan")
                            ->where("kode","=",$id);

        return $query;
    }

    public function getKodeGroup2($id) {
        $query  = DB::table("tabel_group2")
                            ->select("keterangan")
                            ->where("kode","=",$id);

        return $query;
    }

    public function getKodeGroup3($id) {
        $query  = DB::table("tabel_group3")
                            ->select("keterangan")
                            ->where("kode","=",$id);

        return $query;
    }

    public function getWilayah($id) {
        $query  = DB::table("tabel_wilayah")
                            ->select("keterangan")
                            ->where("kode","=",$id);

        return $query;
    }

    public function getSidDati($id) {
        $query  = DB::table("sid_dati2")
                            ->select("keterangan")
                            ->where("kode","=",$id);
                            
        return $query;
    }

    public function getSidStatusGelar($id) {
        $query  = DB::table("sid_status_gelar")
                            ->select("keterangan")
                            ->where("kode","=",$id);

        return $query;
    }

     public function getSimpananwajib($id,$kateori) {
        $query  = DB::table("p_kategorianggota")
                            ->select("simpanan_wajib")
                            ->where("id_level","=",$id)
                            ->where("kategori","=",$kateori)->get()->first();

        return $query->simpanan_wajib;
    }
     public function getTunjjabatan($id) {
        $query  = DB::table("m_level")
                            ->select("tunj_jabatan")
                            ->where("id_level","=",$id);

        return $query;
    }
public function getJenisPengajuan() {
        $query = DB::select("SELECT id_jenisabsen AS id, nama_jenisabsen as name FROM tm_jenisabsen where id_jenisabsen != '' ORDER BY id_jenisabsen");

        return $query;
    }

    public function getNamaAnggotaDepartemen($id) {
        $query = DB::select("SELECT id_karyawan , id_departemen, nama_karyawan as name FROM p_karyawan where nik = '$id'");
        $id_departemen = $query[0]->id_departemen;
        $id_karyawan = $query[0]->id_karyawan;
        $query2 = DB::select("SELECT id_karyawan AS id, id_departemen, nama_karyawan as name FROM p_karyawan where id_departemen = $id_departemen && id_karyawan !=$id_karyawan  ORDER BY id_karyawan");
        return $query2;
    }
    
    
public function getJenisCabang() {
        $query = DB::select("SELECT id_cabang AS id, nama_cabang as name FROM m_cabang where id_cabang != '' ORDER BY id_cabang");
 
        return $query;
    }

public function getAlasankeluar() {
        $query = DB::select("SELECT id_alasankeluar as id, nama_alasankeluar as name FROM pa_alasankeluar where id_alasankeluar != '' ORDER BY id_alasankeluar");
 
        return $query;
    }
 
 public function getSelectJaminan() {
        $query = DB::select("SELECT id_jaminan AS id, nama_jaminan as name FROM pa_jaminan where id_jaminan != '' ORDER BY id_jaminan");

        return $query;
    }


// public function getJenisCabang() {
//         $query = DB::select("SELECT id_cabang AS id, nama_cabang as name FROM m_cabang where id_cabang != '' ORDER BY id_cabang");
 
//         return $query;
//     }

public function getJenisDepartemenPusat() {
    $query = DB::select("SELECT id_departemen AS id, nama_departemen as name FROM m_departemen where id_departemen IS NOT NULL  ORDER BY id_departemen");
   
    return $query;
}

public function getSelectDepartemenById($id_cabang) {
    // $query = DB::select("SELECT id_departemen AS id, nama_departemen as name FROM m_departemen where id_departemen != '' ORDER BY id_departemen");
    $query = DB::select("SELECT m_departemen.id_departemen as id, m_departemen.nama_departemen as name FROM p_karyawan left join 
    m_departemen on m_departemen.id_departemen = p_karyawan.id_departemen where m_departemen.id_departemen is not null and p_karyawan.id_cabang=$id_cabang and p_karyawan.aktif = 1");
    return $query;
}

public function getSelectJabatanById($id_cabang, $id) {
    $query = DB::select("SELECT m_jabatan.id_jabatan as id, CONCAT(m_jabatan.nama_jabatan) name  FROM p_karyawan left join 
    m_jabatan on m_jabatan.id_jabatan = p_karyawan.id_jabatan  where p_karyawan.id_cabang =$id_cabang and p_karyawan.id_departemen = $id and p_karyawan.aktif = 1");
    

    return $query;
}
public function getSelectJabatanByAll($id_cabang) {
    $query = DB::select("SELECT m_jabatan.id_jabatan as id, CONCAT(m_jabatan.nama_jabatan) name  FROM p_karyawan left join 
    m_jabatan on m_jabatan.id_jabatan = p_karyawan.id_jabatan  where p_karyawan.id_cabang =$id_cabang and p_karyawan.aktif = 1");
    

    return $query;
}

public function getSelectJabatanByIdCabangAndAllJabatan($id_cabang, $id) {
    $query = DB::select("SELECT m_jabatan.id_jabatan as id, CONCAT(m_jabatan.nama_jabatan) name  FROM p_karyawan left join 
    m_jabatan on m_jabatan.id_jabatan = p_karyawan.id_jabatan  where p_karyawan.id_cabang =$id_cabang and p_karyawan.id_departemen = $id and p_karyawan.aktif = 1");
    

    return $query;
}

public function getSelectJabatanByIdCabangAndAllDepartemen($id_cabang, $id) {
    $query = DB::select("SELECT m_jabatan.id_jabatan as id, CONCAT(m_jabatan.nama_jabatan) name  FROM p_karyawan left join 
    m_jabatan on m_jabatan.id_jabatan = p_karyawan.id_jabatan  where p_karyawan.id_cabang =$id_cabang and p_karyawan.id_jabatan = $id and p_karyawan.aktif = 1");
    

    return $query;
}

public function getSelectJabatanByIdCabangAndByIdDepartemenByIdJabatan($id_cabang, $id_departemen, $id_jabatan) {
    $query = DB::select("SELECT  m_cabang.*,m_departemen.*, m_jabatan.*  FROM p_karyawan 
    left join m_cabang on m_cabang.id_cabang = p_karyawan.id_cabang 
    left join m_jabatan on m_jabatan.id_jabatan = p_karyawan.id_jabatan 
    left join m_departemen on m_departemen.id_departemen = p_karyawan.id_departemen
    where p_karyawan.id_cabang =$id_cabang and p_karyawan.id_departemen = $id_departemen and p_karyawan.id_jabatan = $id_jabatan and p_karyawan.aktif = 1");
    

    return $query;
}
public function getSelectReferensi() {
    $query = DB::select("SELECT id_referensi AS id, no_referensi as name FROM m_referensi  ORDER BY id_referensi");

    return $query;
}

// public function getSelectKaryawanNonAktif() {
//     $query = DB::select("SELECT id_karyawan AS id,CONCAT(nama_karyawan, ' / ', nik) as name FROM p_karyawan where aktif = 0 ORDER BY id_karyawan");

//     return $query;
// }

}