<?php

namespace App\Model\master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class JabatanModel extends Model
{
    protected $table    = "m_jabatan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_jabatan as a")
                            ->select("a.id_jabatan", "a.nama_jabatan","b.nama_level","a.tunj_transport","a.group_id","c.name")
                            ->leftjoin("m_level as b","b.id_level","=","a.id_level")
                            ->leftjoin("sys_groups as c","c.id","=","a.group_id")
                            
                            ->orderBy("a.id_jabatan", "DESC");




        if(session()->has("SES_SEARCH_MJABATAN")) {
            $query->where("nama_jabatan", "LIKE", "%" . session()->get("SES_SEARCH_MJABATAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_jabatan as a")
                             ->select("a.*","b.nama_level")
                            ->leftjoin("m_level as b","b.id_level","=","a.id_jabatan")
                            ->where("a.id_jabatan", $id)
                            ->orderBy("a.nama_jabatan", "DESC");

    
        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qjabatan              = new jabatanModel;
        # ---------------
       // $qjabatan->kode       = setString($request->kode);
        $qjabatan->nama_jabatan       = setString($request->nama_jabatan);
        $qjabatan->id_level           = setString($request->id_level);
        $qjabatan->tunj_transport       = setNoComma($request->tunj_transport);
        $qjabatan->group_id           = setString($request->group_id);
        $qjabatan->user_id            = setString(Auth::user()->id);
        $qjabatan->create_at          = setString(date('Y-m-d H:i:s'));
        $qjabatan->update_at          = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qjabatan->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE Jabatan (" . $qjabatan->id_jabatan . ") " . strtoupper($request->nama_jabatan), Auth::user()->id, $request);
    }

    public function updateData($request) {


         DB::table("m_jabatan",$request->id_jabatan)
                             ->where("id_jabatan", $request->id_jabatan)
                            ->update([ "nama_jabatan"        =>$request->nama_jabatan,
                                       "id_level"            =>$request->id_level,
                                       "group_id"            =>$request->group_id,
                                       "tunj_transport"      => setNoComma($request->tunj_transport),
                            	       "user_id"             =>setString(Auth::user()->id),
                            	       "update_at"           =>setString(date('Y-m-d H:i:s'))	
                            	      ]);
  
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE Jabatan(" . $request->id_jabatan . ") " . strtoupper($request->nama_jabatan), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_jabatan")
                             ->where("id_jabatan", $request->id_jabatan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE Jabatan (" . $request->id_jabatan . ") " . strtoupper($request->nama_jabatan), Auth::user()->id, $request);
    }
}
