<?php
namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class AssetModel extends Model
{
    protected $table    = "m_asset";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_asset as a")
                            ->select("a.*","b.nama_barang","b.keterangan" )
                            ->leftjoin("m_barang as b","b.id_barang","=","a.id_barang")
                            ->orderBy("a.id_asset", "asc");

        if(session()->has("SES_SEARCH_ASSET")) {
            $query->where("nama_barang", "LIKE", "%" . session()->get("SES_SEARCH_ASSET") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_asset as a")
                            ->select("a.*", "b.keterangan")
                            ->leftjoin("m_barang as b","b.id_barang","=","a.id_barang")
                            ->where("a.id_asset", $id)
                            ->orderBy("a.id_asset", "DESC");

        $result = $query->get();

        return $result;
    }    
 
    public function createData($request) {
        $qasset              = new AssetModel;
        # ---------------
       // $qasset->kode       = setString($request->kode);
    
       $qasset->id_barang       = setString($request->id_barang);
 
        $qasset->no_asset        = setString($request->no_asset);
        $qasset->tgl_perolehan   = setYMD($request->tgl_perolehan,"/");
        $qasset->harga_perolehan = setNoComma($request->harga_perolehan);
        $qasset->status_asset    = $request->status_asset;
        $qasset->id_lokasi       = setString($request->id_lokasi);
        $qasset->id_cabang       = setString($request->id_cabang);
        $qasset->keterangan      = setString($request->keterangan);
       $qasset->user_id         = setString(Auth::user()->id);
        $qasset->create_at       = setString(date('Y-m-d H:i:s'));
        $qasset->update_at       = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qasset->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qasset->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_asset")
                             ->where("id_asset", $request->id_asset)
                            ->update([ 
                                        "nama_barang"=>$request->nama_barang,
                                       
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_asset")
                             ->where("id_asset", $request->id_asset)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_asset . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }
}
