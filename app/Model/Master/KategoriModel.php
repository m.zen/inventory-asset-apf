<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class KategoriModel extends Model
{
    protected $table    = "m_kategori";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_kategori as a")
                            ->select("a.id_kategori" ,"a.nama_kategori")
			  
                            ->orderBy("a.id_kategori", "asc");

        if(session()->has("SES_SEARCH_KATEGORI")) {
            $query->where("nama_kategori", "LIKE", "%" . session()->get("SES_SEARCH_KATEGORI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_kategori as a")
                            ->select("a.id_kategori", "a.nama_kategori")
			   
                            ->where("a.id_kategori", $id)
                            ->orderBy("a.nama_kategori", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qkategori              = new KategoriModel;
        # ---------------
       // $qkategori->kode       = setString($request->kode);
        $qkategori->nama_kategori      = setString($request->nama_kategori);
          $qkategori->user_id          = setString(Auth::user()->id);
        $qkategori->create_at        = setString(date('Y-m-d H:i:s'));
        $qkategori->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qkategori->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qkategori->id_kategori . ") " . strtoupper($request->nama_kategori), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_kategori")
                             ->where("id_kategori", $request->id_kategori)
                            ->update([ 
                                        "nama_kategori"=>$request->nama_kategori,
                                       
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_kategori . ") " . strtoupper($request->nama_kategori), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_kategori")
                             ->where("id_kategori", $request->id_kategori)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_kategori . ") " . strtoupper($request->nama_kategori), Auth::user()->id, $request);
    }
}
