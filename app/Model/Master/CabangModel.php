<?php

namespace App\Model\master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class CabangModel extends Model
{
    protected $table    = "m_cabang";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_cabang as a")
                            ->select("a.id_cabang","a.kode_cabang" ,"a.nama_cabang")
			  
                            ->orderBy("kode_cabang", "asc");

        if(session()->has("SES_SEARCH_CABANG")) {
            $query->where("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_CABANG") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_cabang as a")
                            ->select("a.id_cabang","a.kode_cabang", "a.nama_cabang")
			   
                            ->where("a.id_cabang", $id)
                            ->orderBy("a.nama_cabang", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qcabang              = new CabangModel;
        # ---------------
       // $qcabang->kode       = setString($request->kode);
        $qcabang->kode_cabang      = setString($request->kode_cabang);
        $qcabang->nama_cabang      = setString($request->nama_cabang);
          $qcabang->user_id          = setString(Auth::user()->id);
        $qcabang->create_at        = setString(date('Y-m-d H:i:s'));
        $qcabang->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qcabang->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qcabang->id_cabang . ") " . strtoupper($request->nama_cabang), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_cabang")
                             ->where("id_cabang", $request->id_cabang)
                            ->update([  "kode_cabang"=>$request->kode_cabang,
                                        "nama_cabang"=>$request->nama_cabang,
                                       
					"id_grupabsen"=> $request->id_grupabsen,	
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_cabang . ") " . strtoupper($request->nama_cabang), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_cabang")
                             ->where("id_cabang", $request->id_cabang)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_cabang . ") " . strtoupper($request->nama_cabang), Auth::user()->id, $request);
    }
}
