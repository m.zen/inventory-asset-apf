<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class BarangModel extends Model
{
    protected $table    = "m_barang";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_barang as a")
                            ->select("a.*" ,"a.nama_barang","b.nama_grupbarang","c.nama_merk","d.nama_kategori")
                            ->leftjoin("m_grupbarang as b","b.id_grupbarang","=","a.id_grupbarang")
                            ->leftjoin("m_merk as c","c.id_merk","=","a.id_merk")
                            ->leftjoin("m_kategori as d","d.id_kategori","=","a.id_kategori")
			  
                            ->orderBy("a.id_barang", "asc");

        if(session()->has("SES_SEARCH_BARANG")) {
            $query->where("nama_barang", "LIKE", "%" . session()->get("SES_SEARCH_BARANG") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_barang as a")
                        ->select("a.kode_barang","a.nama_barang","a.keterangan","b.id_grupbarang","c.id_merk","d.id_kategori")
                        ->leftjoin("m_grupbarang as b","b.id_grupbarang","=","a.id_grupbarang")
                        ->leftjoin("m_merk as c","c.id_merk","=","a.id_merk")
                        ->leftjoin("m_kategori as d","d.id_kategori","=","a.id_kategori")
                        ->where("a.id_barang", $id)
                        ->orderBy("a.nama_barang", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qbarang              = new BarangModel;
        # ---------------
       // $qbarang->kode       = setString($request->kode);
        $qbarang->kode_barang      = setString($request->kode_barang);
        $qbarang->nama_barang      = setString($request->nama_barang);
        $qbarang->id_grupbarang    = setString($request->id_grupbarang);
        $qbarang->id_merk          = setString($request->id_merk);
        $qbarang->id_kategori      = setString($request->id_kategori);
        $qbarang->keterangan       = setString($request->katerangan);
        $qbarang->user_id          = setString(Auth::user()->id);
        $qbarang->create_at        = setString(date('Y-m-d H:i:s'));
        $qbarang->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qbarang->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qbarang->id_barang . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_barang")
                             ->where("id_barang", $request->id_barang)
                            ->update([  "nama_barang"=>$request->nama_barang,
                                        "kode_barang"=>$request->kode_barang,
                                        "id_merk"=>$request->id_merk,
                                        "id_grupbarang"=>$request->id_grupbarang,
                                        "id_merk"=>$request->id_kategori,
                                        "keterangan"=>$request->keterangan,
                                       
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_barang . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_barang")
                             ->where("id_barang", $request->id_barang)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_barang . ") " . strtoupper($request->nama_barang), Auth::user()->id, $request);
    }
}
