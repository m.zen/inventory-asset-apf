<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class GrupbarangModel extends Model
{
    protected $table    = "m_grupbarang";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_grupbarang as a")
                            ->select("a.id_grupbarang" ,"a.nama_grupbarang")
			  
                            ->orderBy("a.id_grupbarang", "asc");

        if(session()->has("SES_SEARCH_GRUPBARANG")) {
            $query->where("nama_grupbarang", "LIKE", "%" . session()->get("SES_SEARCH_GRUPBARANG") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_grupbarang as a")
                            ->select("a.id_grupbarang", "a.nama_grupbarang")
			   
                            ->where("a.id_grupbarang", $id)
                            ->orderBy("a.nama_grupbarang", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qgrupbarang              = new GrupbarangModel;
        # ---------------
       // $qgrupbarang->kode       = setString($request->kode);
        $qgrupbarang->nama_grupbarang      = setString($request->nama_grupbarang);
          $qgrupbarang->user_id          = setString(Auth::user()->id);
        $qgrupbarang->create_at        = setString(date('Y-m-d H:i:s'));
        $qgrupbarang->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qgrupbarang->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qgrupbarang->id_grupbarang . ") " . strtoupper($request->nama_grupbarang), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_grupbarang")
                             ->where("id_grupbarang", $request->id_grupbarang)
                            ->update([ 
                                        "nama_grupbarang"=>$request->nama_grupbarang,
                                       
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_grupbarang . ") " . strtoupper($request->nama_grupbarang), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_grupbarang")
                             ->where("id_grupbarang", $request->id_grupbarang)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_grupbarang . ") " . strtoupper($request->nama_grupbarang), Auth::user()->id, $request);
    }
}
