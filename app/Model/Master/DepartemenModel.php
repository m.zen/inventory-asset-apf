<?php

namespace App\Model\master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class DepartemenModel extends Model
{
    protected $table    = "m_departemen";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_departemen")
                            ->select("*")
                              ->orderBy("id_departemen", "DESC");




        if(session()->has("SES_SEARCH_DEPARTEMEN")) {
            $query->where("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_DEPARTEMEN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_departemen")
                             ->select("*")
                             ->where("id_departemen", $id)
                            ->orderBy("nama_departemen", "DESC");

    
        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qdepartemen              = new DepartemenModel;
        # ---------------
       // $qdepartemen->kode       = setString($request->kode);
        $qdepartemen->nama_departemen      = setString($request->nama_departemen);
        $qdepartemen->id_divisi            = setString($request->id_divisi);
        $qdepartemen->id_grupabsen         = setString($request->id_grupabsen);
        $qdepartemen->user_id              = setString(Auth::user()->id);
        $qdepartemen->create_at            = setString(date('Y-m-d H:i:s'));
        $qdepartemen->update_at            = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qdepartemen->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE DEPARTEMEN (" . $qdepartemen->id_departemen . ") " . strtoupper($request->nama_departemen), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_departemen")
                             ->where("id_departemen", $request->id_departemen)
                            ->update([ "nama_departemen"=>$request->nama_departemen,
                                       "id_divisi"=>$request->id_divisi,
                                       "id_grupabsen"=>$request->id_grupabsen,
                                       
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE DEPARTEMEN(" . $request->id_departemen . ") " . strtoupper($request->nama_departemen), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_departemen")
                             ->where("id_departemen", $request->id_departemen)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE DEPARTEMEN (" . $request->id_departemen . ") " . strtoupper($request->nama_departemen), Auth::user()->id, $request);
    }
}
