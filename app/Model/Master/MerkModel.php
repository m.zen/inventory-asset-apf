<?php



namespace App\Model\master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class MerkModel extends Model
{
    protected $table    = "m_merk";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_merk as a")
                            ->select("a.id_merk" ,"a.nama_merk")
			  
                            ->orderBy("a.id_merk", "asc");

        if(session()->has("SES_SEARCH_MERK")) {
            $query->where("nama_merk", "LIKE", "%" . session()->get("SES_SEARCH_MERK") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_merk as a")
                            ->select("a.id_merk", "a.nama_merk")
			   
                            ->where("a.id_merk", $id)
                            ->orderBy("a.nama_merk", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qmerk              = new MerkModel;
        # ---------------
       // $qmerk->kode       = setString($request->kode);
        $qmerk->nama_merk      = setString($request->nama_merk);
          $qmerk->user_id          = setString(Auth::user()->id);
        $qmerk->create_at        = setString(date('Y-m-d H:i:s'));
        $qmerk->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qmerk->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qmerk->id_merk . ") " . strtoupper($request->nama_merk), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_merk")
                             ->where("id_merk", $request->id_merk)
                            ->update([ 
                                        "nama_merk"=>$request->nama_merk,
                                       
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_merk . ") " . strtoupper($request->nama_merk), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_merk")
                             ->where("id_merk", $request->id_merk)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_merk . ") " . strtoupper($request->nama_merk), Auth::user()->id, $request);
    }
}

?>