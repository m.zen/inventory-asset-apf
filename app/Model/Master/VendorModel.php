<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class VendorModel extends Model
{
    protected $table    = "m_vendor";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_vendor as a")
                            ->select("a.*" )
			  
                            ->orderBy("a.id_vendor", "asc");

        if(session()->has("SES_SEARCH_VENDOR")) {
            $query->where("nama_vendor", "LIKE", "%" . session()->get("SES_SEARCH_VENDOR") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("m_vendor as a")
                            ->select("a.*")
			   
                            ->where("a.id_vendor", $id)
                            ->orderBy("a.nama_vendor", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qvendor              = new VendorModel;
        # ---------------
       // $qvendor->kode       = setString($request->kode);
        $qvendor->nama_vendor      = setString($request->nama_vendor);
        $qvendor->alamat      = setString($request->alamat);
        $qvendor->telpon      = setString($request->telpon);
        $qvendor->email      = setString($request->email);
        
          $qvendor->user_id          = setString(Auth::user()->id);
        $qvendor->create_at        = setString(date('Y-m-d H:i:s'));
        $qvendor->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qvendor->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE CABANG (" . $qvendor->id_vendor . ") " . strtoupper($request->nama_vendor), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_vendor")
                             ->where("id_vendor", $request->id_vendor)
                            ->update([ 
                                        "nama_vendor"=>$request->nama_vendor,
                                        "alamat"=>$request->nama_vendor,
                                        "telpon"=>$request->telpon,
                                        "email"=>$request->email,
					
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE CABANG(" . $request->id_vendor . ") " . strtoupper($request->nama_vendor), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_vendor")
                             ->where("id_vendor", $request->id_vendor)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE CABANG (" . $request->id_vendor . ") " . strtoupper($request->nama_vendor), Auth::user()->id, $request);
    }
}
