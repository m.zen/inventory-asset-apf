<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class HariliburModel extends Model
{
    protected $table    = "m_harilibur";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("m_harilibur as a")
                            ->select("*")
                            ->orderBy("tgl_libur", "DESC");




        if(session()->has("SES_SEARCH_HARILIBUR")) {
            $query->where("nama_harilibur", "LIKE", "%" . session()->get("SES_SEARCH_HARILIBUR") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
    
    public function getProfile($id) {
        $query  = DB::table("m_harilibur as a")
                             ->select("*")
                            ->where("a.id_harilibur", $id)
                            ->orderBy("a.nama_harilibur", "DESC");

    
        $result = $query->get();

        return $result;
    }    

public function getNamahari($namahari)
{   $nh="Minggu";
    if ($namahari=="Mon")
    {
        $nh="Senin";
    }
    else if ($namahari=="Tue")
    {
        $nh="Selasa";
    }
    else if ($namahari=="Wed")
    {
        $nh="Rabu";
    }
    else if ($namahari=="Thu")
    {
        $nh="Kamis";
    }
    else if ($namahari=="Fri")
    {
        $nh="Jumat";
    }
    else if ($namahari=="Sat")
    {
        $nh="Sabtu";
    }
    else if ($namahari=="")
    {
        $nh="Minggu";
    }
    return $nh;
    
}

public function CekTanggal($tglLibur) 
{


        $query  = DB::table("m_harilibur")
                            ->select("tgl_libur")
                            ->where("tgl_libur", setYMD($tglLibur,"/"));
                           
        $result = $query->count();
        
        return $result;
   }



    public function createData($request) {





        $qharilibur              = new hariliburModel;
        $day = date('D', strtotime(setYMD($request->tgl_libur, "/")));
        $hari = $this->getNamahari($day);
         # ---------------
       // $qharilibur->kode       = setString($request->kode);
        $qharilibur->nama_harilibur       = setString($request->nama_harilibur);
        $qharilibur->hari                 = $hari;
        $qharilibur->tgl_libur            = setYMD($request->tgl_libur, "/");
        $qharilibur->user_id              = setString(Auth::user()->id);
        $qharilibur->create_at            = setString(date('Y-m-d H:i:s'));
        $qharilibur->update_at            = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qharilibur->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE Jabatan (" . $qharilibur->id_harilibur . ") " . strtoupper($request->nama_harilibur), Auth::user()->id, $request);
    }
     public function hariminggu_proses($request) {
        /* ----------
         Model
        ----------------------- */
        //$qLaporan              = new LaporanModel;

        //$day = date('D', strtotime(setYMD($request->tgl_libur, "/")));
        $tanggal               = setYMD($request->tgl_awal, "/");
        $tglakhir              = setYMD($request->tgl_akhir, "/");
 
        
        while (strtotime($tanggal) <= strtotime($tglakhir)) 
        {
                $day = date('D', strtotime($tanggal));
                if ($day=="Sun")
                {
                    $qharilibur              = new hariliburModel;
                    # ---------------
                   // $qharilibur->kode       = setString($request->kode);
                    $qharilibur->nama_harilibur       = setString($request->nama_harilibur);
                    $qharilibur->hari                 = "Minggu";
                    $qharilibur->tgl_libur            = $tanggal;
                    $qharilibur->user_id              = setString(Auth::user()->id);
                    $qharilibur->create_at            = setString(date('Y-m-d H:i:s'));
                    $qharilibur->update_at            = setString(date('Y-m-d H:i:s'));
                    
                    
                    
                    # ---------------
                    $qharilibur->save(); 
                } 
               $tanggal = date ("Y-m-d", strtotime("+1 day", strtotime($tanggal)));    
                  
        }
           
    }
    public function updateData($request) {
         DB::table("m_harilibur")
                             ->where("id_harilibur", $request->id_harilibur)
                            ->update([ "nama_harilibur"=>$request->nama_harilibur,"id_level"=>$request->id_level,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE Jabatan(" . $request->id_harilibur . ") " . strtoupper($request->nama_harilibur), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("m_harilibur")
                             ->where("id_harilibur", $request->id_harilibur)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE Jabatan (" . $request->id_harilibur . ") " . strtoupper($request->nama_harilibur), Auth::user()->id, $request);
    }
}
