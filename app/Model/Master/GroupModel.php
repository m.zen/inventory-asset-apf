<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class GroupModel extends Model
{
    protected $table 	= "sys_groups";

    public function getList($request=null, $offset=null, $limit=null) {
        $query 	= DB::table("sys_groups")
                            ->select("id", "name")
                            ->orderBy("id", "DESC");

        if(session()->has("SES_SEARCH_GROUP")) {
            $query->where("name", "LIKE", "%" . session()->get("SES_SEARCH_GROUP") . "%");
        }
        
        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function createData($request) {
        $qGroup             = new GroupModel;
        # ---------------
        $qGroup->name       = setString($request->name);
        # ---------------
        $qGroup->save();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE GROUP " . strtoupper($request->name), Auth::user()->id, $request);
    }

    public function updateData($request) {
        $qGroup             = GroupModel::find($request->id);
        # ---------------
        $qGroup->name       = setString($request->name);
        # ---------------
        $qGroup->save();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE GROUP (" . $request->id . ") " . strtoupper($request->input("name")), Auth::user()->id, $request);
    }

    public function removeData($request) {
        $qGroup             = GroupModel::find($request->id);
        # ---------------
        $qGroup->delete();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE USER GROUP (" . $request->input("id") . ")", Auth::user()->id, $request);
    }
}
