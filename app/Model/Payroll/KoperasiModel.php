<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\Master\MasterModel;
use App\Model\LogModel;
class KoperasiModel extends Model
{
   protected $table    = "p_koperasi";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_koperasi as a")
                            ->select("a.*","b.nama_karyawan","c.nama_cabang","d.nama_jabatan","d.id_level")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","b.id_jabatan")
                            ->orderBy("id_koperasi", "DESC");

        if(session()->has("SES_SEARCH_KOPERASI")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KOPERASI") . "%")
             ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KOPERASI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();
           
        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("p_koperasi as a")
                            ->select("a.*","b.nama_karyawan","c.nama_cabang","d.nama_jabatan","d.id_level")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","b.id_jabatan")
                            ->where("id_koperasi", $id)
                            ->orderBy("id_koperasi", "DESC");
        $result = $query->get();
        return $result;
    }    
 public function getKry($id) {
        $query  = DB::table("p_karyawan as a")
                            ->select("b.id_level","c.kategori")
                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","a.id_cabang")
                            ->where("id_karyawan", $id);
        $result = $query->get();
        return $result;
    }   
    public function createData($request) {
        $qkoperasi              = new KoperasiModel;
        $idlevel                = $qkoperasi->getKry($request->id_karyawan)->first(); 
        $level                  = new MasterModel;
       // $Simpanawajib           = $level->getSimpananwajib($idlevel->id_level,$idlevel->kategori);
        
       
        # ---------------
       // $qkoperasi->kode       = setString($request->kode);
        $qkoperasi->id_karyawan     = setString($request->id_karyawan);
        $qkoperasi->tgl_daftar	    = setYMD($request->tgl_daftar,"/");
        //$qkoperasi->simpanan_wajib  = $Simpanawajib ->simpanan_wajib;
        //$qkoperasi->simpanan_wajib  = $Simpanawajib ;
        $qkoperasi->simpanan_wajib  = setNoComma($request->simpanan_wajib);
        $qkoperasi->aktif    		= setString($request->aktif);
        $qkoperasi->user_id         = setString(Auth::user()->id);
        $qkoperasi->create_at       = setString(date('Y-m-d H:i:s'));
        $qkoperasi->update_at       = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qkoperasi->save();
        /* ----------
         Logs
        */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE KOPERASI (" . $qkoperasi->id_koperasi . ") " . strtoupper($request->nama_koperasi), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("p_koperasi")
                             ->where("id_koperasi", $request->id_koperasi)
                            ->update([ "id_karyawan"=>$request->id_karyawan,
                            			"tgl_daftar"=> setYMD($request->tgl_daftar,"/"),
                            			"simpanan_wajib"=>setNoComma($request->simpanan_wajib),
                            			
                            			"aktif"=>setString($request->aktif),
                            			"user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))  ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KOPERASI(" . $request->id_koperasi . ") " . strtoupper($request->nama_koperasi), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_koperasi")
                             ->where("id_koperasi", $request->id_koperasi)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE KOPERASI (" . $request->id_koperasi . ") " . strtoupper($request->nama_koperasi), Auth::user()->id, $request);
    }  
}
