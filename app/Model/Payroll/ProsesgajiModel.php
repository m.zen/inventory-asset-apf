<?php

namespace App\Model\Payroll;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class ProsesgajiModel extends Model
{
    protected $table    = "m_profilesystem";
    public $timestamps= false ;

    

    public function getProfile() {
        $query  = DB::table("m_profilesystem")
                            ->select("bulanpayroll","tahunpayroll","tgl_payrollawal","tgl_payrollakhir","tgl_absenawal","tgl_absenakhir");
                           
        $result = $query->get();

        return $result;
    }    
    
	/* Function Hari Libur */
    function jmllibur($tglawal,$tglakhir)
    {
    	

        	$exists = DB::table('m_harilibur')
        	          ->whereBetween('tgl_libur', [$tglawal, $tglakhir])
                      ->count();
    	$jml=0;
    	if( $exists>0)
			{
				$jml=$exists;
			}
		return $jml;

    }

    /*Function Gaji Prorate*/
   
   function gajiprorata($tgl_masuk,$tgl_keluar,$gaji_pokok)
   {
   		

   	    $tglPayroll            = $this->getProfile()->first();
		    $tgl_payrollawal       = $tglPayroll->tgl_payrollawal;
		    $tgl_payrollakhir      = $tglPayroll->tgl_payrollakhir;	
   		
   		
   		//hitung total hari kerja dalam sebulan
   		// $jmlharilibur      = $this->jmllibur($tgl_payrollawal,$tgl_payrollakhir);
   	  $interval          = date_diff(date_create($tgl_payrollawal),date_create($tgl_payrollakhir));
   		$totalhari         = $interval->format("%a")+1 ;

   		// hitung hari masuk karyawan

   		$jmlharilibur      = $this->jmllibur($tgl_masuk,$tgl_payrollakhir);
   		//dd($jmlharilibur,$tgl_masuk,$tgl_payrollakhir);
		  $interval          = date_diff(date_create($tgl_masuk),date_create($tgl_payrollakhir));

   	//	$totalharimasuk    = $interval->format("%d") - $jmlharilibur + 1;
   	   $totalharimasuk    = $interval->format("%d")  + 1;
    	
   		// Jika Tgl Keluar Empty
   		if( empty($tgl_keluar) )
   		{
   			
   				 // $gaji["gp"]   = ($totalharimasuk/$totalhari)*$gaji_pokok;
   				 // $gaji["tj"]   = ($totalharimasuk/$totalhari)*$tunj_jabatan;
   				 // $gaji["tt"]   = ($totalharimasuk/$totalhari)*$tunj_transport;
   				 // $gaji["tk"]   = ($totalharimasuk/$totalhari)*$tunj_kemahalan;
   				 // $gaji["th"]   = $totalhari;
   				 // $gaji["thm"]   = $totalharimasuk;


           $gaji["gp"]   = ($totalharimasuk/30)*$gaji_pokok;
           // $gaji["tj"]   = ($totalharimasuk/30)*$tunj_jabatan;
           // $gaji["tt"]   = ($totalharimasuk/30)*$tunj_transport;
           // $gaji["tk"]   = ($totalharimasuk/30)*$tunj_kemahalan;
           // $gaji["th"]   = $totalhari;
           // $gaji["thm"]   = $totalharimasuk;

   		}
   		else
   		{
   			if ($tgl_masuk < $tgl_payrollawal)
   			{
   				$jmlharilibur    = $this->jmllibur($tgl_payrollawal,$tgl_keluar);

   				$interval          = date_diff(date_create($tgl_payrollawal),date_create($tgl_keluar));

   				//$totalharimasuk  = $interval->format("%d")  + 1 - $jmlharilibur;
          $totalharimasuk  = $interval->format("%d")+1 ;


   				
   			}
   			elseif ($tgl_masuk >$tgl_payrollawal)
   			{

   				$jmlharilibur    = $this->jmllibur($tgl_masuk,$tgl_keluar);
   				$interval          = date_diff(date_create($tgl_masuk),date_create($tgl_keluar));
   				//$totalharimasuk  = $interval->format("%d")  + 1 - $jmlharilibur;
         
         $totalharimasuk  = $interval->format("%d")+1 ;
           
   			}

//   			dd($totalharimasuk,$totalhari,$gaji_pokok);
				  $gaji["gp"]   = ($totalharimasuk/30)*$gaji_pokok;
   				// $gaji["tj"]   = ($totalharimasuk/30)*$tunj_jabatan;
   				// $gaji["tt"]   = ($totalharimasuk/30)*$tunj_transport; 
   				// $gaji["tk"]   = ($totalharimasuk/30)*$tunj_kemahalan;
     			$gaji["th"]   = $totalhari;
   				$gaji["thm"]   = $totalharimasuk;

   		}
     //dd($id_karyawan,$gaji,$tgl_masuk,$tgl_payrollawal,$tgl_payrollakhir,$tgl_keluar,$gaji_pokok,$totalharimasuk,$totalhari);
       

   		
   		return $gaji;


   }


   /*Function cari PTKP      */
    function Ptkp($id_ptkp)
    {
      $query  = DB::table("p_ptkp")
                ->select("ptkp")
                ->where("id_ptkp",$id_ptkp)->get()->first();
                           
       if(empty($query))
       {
       		$result=0;
       }
       else
       {
       	$result = $query->ptkp;	
       }	
       

        return $result;

    } 

    /* Fuction Cara Hitung PPH21  */
    function Hitungpph21($pkp,$mk)
    {
    	 $qData  = DB::table("p_tarif")
                ->select("batasatas","batasbawah","tarif")
                ->where("batasbawah","<=",$pkp)->get();
        $pajak = 0;
        $jj[0]=0;
        $pkpdim=$pkp;
        $pkpa[0]=$pkpdim;
        $u=0;
       
        if($pkp<60000000)
        {
        	$pajak=$pkp*5/100;
        }
        elseif($pkp>60000000 and $pkp<=250000000)
        {
        	$pajak=(50000000*5/100) + (($pkp-50000000)*15/100);

        }
        elseif($pkp>250000000 and $pkp<=500000000)
        {
        	$pajak=(50000000*5/100) +(200000000*15/100) + (($pkp-250000000)*25/100);

        }
        elseif($pkp>500000000)
        {
        	$pajak=(50000000*5/100) +(200000000*15/100) + (250000000*25/100)+(($pkp-500000000)*30/100);
        }

/*
        foreach($qData as $row) 
        {
        	    
        		if($pkpdim >= $row->batasbawah and $pkpdim < $row->batasatas  )
        		{

        				$jml=$pkpdim*($row->tarif)/100;
        		}
        		elseif ($pkpdim > $row->batasatas)
        	    {
        	    	$jml= ($row->batasatas)* ($row->tarif)/100;
        	    	$pkpdim=$pkpdim - $row->batasatas;
        	    	
        	    	
        	    }
        	    $u=$u+1;
        	    $pkpa[$u]=$pkpdim;
        		$pajak = $pajak + $jml; 
        		$jj[$u]=$jml; 
        }

       */
        $pajak=$pajak/$mk;
        
        
        return $pajak;
    }

    public function getAbsen($nik,$tgl_awal,$tgl_akhir)
    {
    	 
    	//$query = DB::select("SELECT nik,SUM(status_potong_makan) spm,SUM(status_potong_gaji)  spg , COUNT(menit_terlambat)  ml FROM tm_dataabsensi  where (tgl_absensi BETWEEN ? AND ? ) AND (nik= ?) group by nik", [$tgl_awal,$tgl_akhir,$nik])->get();
 
    $querys = DB::table('tm_dataabsensi')
      ->select(DB::raw('
      count(nik) as jmk,  
        SUM(status_potong_gaji)  as spg,
        sum(uang_makan) as  um '))
              ->whereraw("nik='$nik' AND (status_absensi LIKE 'H%' OR status_absensi ='DL' OR status_absensi ='I' OR status_absensi ='S' OR status_absensi ='IK' or status_absensi ='IT' OR status_absensi ='CT' or status_absensi ='IDC' or status_absensi ='KR')")
              ->whereBetween('tgl_absensi', [$tgl_awal,$tgl_akhir])
                    ->groupBy('nik')
                    ->get()->first();

       // if($nik="8000.0316.0001")
       // {
       //   dd($querys,$tgl_awal,$tgl_akhir);
       // }

        return $querys ;
       
        
    }
    public function getAbsenumr($nik,$tgl_awal,$tgl_akhir)
    {
       
      //$query = DB::select("SELECT nik,SUM(status_potong_makan) spm,SUM(status_potong_gaji)  spg , COUNT(menit_terlambat)  ml FROM tm_dataabsensi  where (tgl_absensi BETWEEN ? AND ? ) AND (nik= ?) group by nik", [$tgl_awal,$tgl_akhir,$nik])->get();
      
      $querys = DB::table('tm_dataabsensi')
      ->select(DB::raw('
      count(nik) as jmk,  
        SUM(status_potong_gaji)  as spg,
        sum(uang_makan) as  um '))
              ->whereraw("nik='$nik' AND (status_absensi LIKE 'H%' OR status_absensi ='DL' OR status_absensi ='I' OR status_absensi ='S' OR status_absensi ='IK' OR status_absensi ='CT' or status_absensi ='IDC' or status_absensi ='A'or status_absensi ='IT' )")
              ->whereBetween('tgl_absensi', [$tgl_awal,$tgl_akhir])
                    ->groupBy('nik')
                    ->get()->first();
        return $querys ;
       
        
    }

    
    
    
    public function getUangmakan($id_jabatan)
    {
     
    	$query  = DB::table("m_level as a")
                            ->select("a.uang_makan","a.tunj_jabatan","b.tunj_transport")
                            ->leftjoin("m_jabatan as b","b.id_level","=","a.id_level")
                            ->where("b.id_jabatan", $id_jabatan);
                            
        $result = $query->get()->first();
        return $result;

    }

    public function getKoperasi($id_karyawan)
    {

    		$query  = DB::table("p_koperasi")
                            ->select("simpanan_wajib","simpanan_pokok","tgl_daftar")
                            ->where("id_karyawan", $id_karyawan);
                            
        $result = $query->get()->first();
        return $result;

    }
    public function getPinjaman($id_karyawan)
    {
    	$querys = DB::table('p_pinjaman as a')
    	->select(DB::raw('
        SUM(a.angsuran) as angsuran' ))
        			->leftjoin("p_koperasi as b","b.id_koperasi","=","a.id_koperasi")
                    ->where("b.id_karyawan",$id_karyawan)
                    ->where("a.status_pinjaman","T")
                    
                    ->groupBy('b.id_karyawan')
                    ->get()->first();

        return $querys ;

    }

     public function cekUmr($id_cabang)
    {
      $querys = DB::table('m_cabang')
      ->select("umr")
                    ->where("id_cabang",$id_cabang)
                    ->get();
        return $querys ;

    }



    public function updateData($request) 
    {
  
    $tglPayroll            = $this->getProfile()->first();
  	$tgl_payrollawal       = $tglPayroll->tgl_payrollawal;
  	$tgl_payrollakhir      = $tglPayroll->tgl_payrollakhir;	

     $qData  = DB::select("SELECT  * FROM  p_karyawan where aktif=1 order by id_karyawan
                            ");

 

        foreach($qData as $row) 
        {

        /* -----------------------------------
         Proses Gaji Prorate
         -------------------------------------*/
                 $ptkp =$this->ptkp($row->id_ptkp);

             //    $uangmakanharian = $this->getUangmakan($row->id_jabatan);
                 
             $uangmakanharian = $row->tunj_makan;;    
		         $gaji=$row->gaji_pokok;

		         $gaji_pokok      = $row->gaji_pokok;
		         $tunj_jabatan    = $row->tunj_jabatan;
		         $tunj_transport  = $row->tunj_transport;
		         $tunj_kemahalan  = $row->tunj_kemahalan;
		         $tgl_masuk       = $row->tgl_masuk;
		         $tgl_keluar      = $row->tgl_keluar;

		         $th    =0;
		         	$thm    = 0;
               
		        
		    	/*Gaji         */

		    	
		    	if (($row->tgl_masuk >= $tgl_payrollawal && $row->tgl_masuk <= $tgl_payrollakhir) || ($row->tgl_keluar >= $tgl_payrollawal && $row->tgl_keluar <= $tgl_payrollakhir)  )
		    	{

		    		$gp=$this->gajiprorata($tgl_masuk,$tgl_keluar,$gaji_pokok);
		    		
		    		//dd($row->nama_karyawan);
		    	  	$gaji_pokok      = $gp["gp"];
		        	// $tunj_jabatan    = $gp["tj"];
		         // 	$tunj_transport  = $gp["tt"];
		         // 	$th              = $gp["th"];
		         //	$thm             = $gp["thm"];
		        // 	$tunj_kemahalan  = $gp["tk"];

		         	
		
		    	}


		        //  if ($row->tgl_masuk > $request->tgl_payrollawal or ($row->tgl_keluar>$request->tgl_payrollawal and $row->tgl_keluar < $request->tgl_payrollakhir) )
		   		   // {

		   		   // 		$gaji = $this->gajiprorata($row->tgl_masuk,$request->tgl_payrollawal,$request->tgl_payrollakhir,$row->tgl_keluar,$row->gaji_pokok);
		   			   
		   		   		
		   		   // }
        /*---------------------------------------------

            Proses perhitungan BPJS
         -------------------------------------------------*/


               $rate_bpjstk   = $row->rate_bpjstk;
               $rate_bpjskes  = $row->rate_bpjskes;
               $jht_kry       = ($rate_bpjstk * 2) /100;
               $jht_prs       = ($rate_bpjstk * 3.7) /100;
               
               $jp_kry        = ($rate_bpjstk * 1) /100;
               if($jp_kry > 85124)
               {

               		$jp_kry = 85124;
               }


               $jp_prs        = ($rate_bpjstk * 2) /100;
              
               if($jp_prs > 170249)
               {

               		$jp_prs=170249;
               		
               		
               }

               // Versi BSM
               $jkk           = ($rate_bpjstk * 0.24) /100;
               $jkm           = ($rate_bpjstk * 0.30) /100;
               // $bpjs_kes_kry  = ($rate_bpjskes * 1)/100;
               // $bpjs_kes_prs  = ($rate_bpjskes * 4)/100;
             
               $bpjs_kes_kry  = 0;
               $bpjs_kes_prs  = ($rate_bpjskes * 5)/100;
 
               /*----------------------------------------------------------- 
                                              Koperasi
               -------------------------------------------------------------*/


		    	   $koperasi                    = $this->getKoperasi($row->id_karyawan);
		       	if(!empty($koperasi))
		       	{
		  			  	
		       			
		  			  
		  			  		$simpanan_wajib     = $koperasi->simpanan_wajib;
		  			  
		       		
		       	}
		       	else
		       	{
		       		  	$simpanan_wajib     = 0;
		       		 
		       	}


		       	/*-------------------------------------------------------------------
                                      Pinjaman
             -------------------------------------------------------------------*/
		    	  
            $pinjaman                   = $this->getPinjaman($row->id_karyawan);
		       	if(!empty($pinjaman))
		       	{
		       		$pot_koperasi           = $pinjaman->angsuran;
	
		       	}
		       	else
		       	{
		       		$pot_koperasi       = 0;
		       	}
		       	
		    	
		       	/*----------------------------------------------------------------
                                            Absesni            
              ------------------------------------------------------------------*/
            

		    	 $tglAbsen           = $this->getProfile()->first();
           if ($row->tgl_masuk > $tglAbsen->tgl_absenawal)
           {
              $tglAbsenawal       = $row->tgl_masuk;
           }
           else
           {
              $tglAbsenawal       = $tglAbsen->tgl_absenawal;
           }

           if ($row->tgl_keluar == null )
           {
             $tglAbsenakhir      = $tglAbsen->tgl_absenakhir;
           }
           else
           {
             $tglAbsenakhir      =$row->tgl_keluar;
             if($tglAbsen->tgl_absenakhir < $row->tgl_keluar)
             {
                 $tglAbsenakhir      = $tglAbsen->tgl_absenakhir;
             }

           }
		    	
		    	
		    	// $Absensi            = $this->getAbsen($row->nik,$tglAbsenawal,$tglAbsenakhir);
		    	// $uang_makan         = 0;
       //    $jml_hadir=0;
       //    $umh=0;
       //    $gajifix=0;
		    	// if(!empty($Absensi))
		    	// {
		    	// 	 	$jmk                = $Absensi->jmk- $Absensi->spg; //Jumlah Masuk Kerja
		    	// 	 	$spg                = $Absensi->spg; //Jumlah Potongan Gaji
		    	// 		$uang_makan         = $Absensi->um;  // Jumlah Uang Makan

       //        $uangmakanharian = $this->getUangmakan($row->id_jabatan);

       //        if($uangmakanharian==null)
       //        {

       //          //dd($row->nama_karyawan);
       //          $umh=0;
       //        }
       //        else
       //        {
       //           $umh =  $uangmakanharian->uang_makan;
       //        }
            
             

          
       //        $gajifix    = $row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_jaga+($umh*26); // Gapok + Tunj Tetap dan uang makan 26 hari
       //        $Umr = $this->cekUmr($row->id_cabang)->first();

              
       //        if ($Umr->umr >  $gajifix)
       //        {
       //              $uang_makan      = $umh * $Absensi->jmk; //Tidak Potongan 
              
       //        }
             
       //         $potongan_absen = ($row->gaji_pokok/30)*$spg;

		    		
		    	// }
		    	// else
			    // {
			    // 		$spg       = 0;
			    // 		$uang_makan=0;

			    // }






          //$uangmakanharian = $this->getUangmakan($row->id_jabatan);
          $umh             = 0;
          $potongan_absen  = 0;
          $uang_makan      = 0; 
          $spg             = 0;
          $jml_hadir       = 0;

          // if($uangmakanharian==null)
          //   {

          //       //dd($row->nama_karyawan);
          //       $umh=0;
          //   }
          // else
          //   {
          //        $umh =  $uangmakanharian->uang_makan;
          //   }

           // $gajifix    = $row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_jaga+($umh*26); 
           // $Umr = $this->cekUmr($row->id_cabang)->first();
           // if ($Umr->umr >  $gajifix) // Jika dibawah ump

           //   {

           //         $Absensiumr            = $this->getAbsenumr($row->nik,$tglAbsenawal,$tglAbsenakhir);
           //         if(!empty($Absensiumr))
           //         {
           //            $jmk                = $Absensiumr->jmk; //Jumlah Masuk Kerja
           //            $uang_makan         = $umh * $Absensiumr->jmk;
           //            $jml_hadir          = $Absensiumr->jmk;

                     

           //         }
           //         else
           //          {
           //            $uang_makan=0;

           //          }

           //           $potongan_absen = 0;


                              
           //  } 
           //  else
           //  {

           //         $Absensi            = $this->getAbsen($row->nik,$tglAbsenawal,$tglAbsenakhir);
           //         if(!empty($Absensi))
           //         {
           //            $jmk                = $Absensi->jmk- $Absensi->spg; //Jumlah Masuk Kerja
           //            $spg                = $Absensi->spg; //Jumlah Potongan Gaji
           //            $uang_makan         = $Absensi->um;  // Jumlah Uang Makan
           //            $jml_hadir         = $jmk ;
                    
           //         }
           //         else
           //          {
           //            $uang_makan=0;

           //          }

           //           $potongan_absen = ($row->gaji_pokok/30)*$spg;


           //  } 




              $Absensi            = $this->getAbsen($row->nik,$tglAbsenawal,$tglAbsenakhir);
                   if(!empty($Absensi))
                   {
                      $jmk                = $Absensi->jmk- $Absensi->spg; //Jumlah Masuk Kerja
                      $spg                = $Absensi->spg; //Jumlah Potongan Gaji
                      $uang_makan         = $Absensi->um;  // Jumlah Uang Makan
                      if (!empty($row->tunj_makan))
                        {
                         $jml_hadir         =  $Absensi->um/$row->tunj_makan ;
                        }
                      else
                      {
                          $jml_hadir=0;
                      }
                    
                   }
                   else
                    {
                      $uang_makan=0;

                    }

                     $potongan_absen = ($row->gaji_pokok/30)*$spg;


		    
           // if ($row->nik =="2015.0319.0019")
           //    {
           //      dd($Umr->umr,$gajifix,$uang_makan,$jml_hadir,$row->nik,$tglAbsenawal,$tglAbsenakhir);
           //    }

		    	

		    	// if($row->nik =="2016.0916.0079")
       //    {

       //       dd( $row->tgl_masuk,$row->tgl_keluar,$tgl_payrollawal, $tgl_payrollakhir,$gaji_pokok );

       //    }


		    	 
		    /*--------------------------------------------------------------------*/
	        	$exists = DB::table('p_penggajian')
	        	          ->where('id_karyawan', $row->id_karyawan)
	        	          ->where('bulan', $request->bulanpayroll)
	        	          ->where('tahun', $request->tahunpayroll)
	        	          ->count();
                      if($row->tunj_makan==0)
                            {
                               $uang_makan=0;
                               $jml_hadir=0;
                            }
                            else
                            {
                              $jml_hadir=$uang_makan/$row->tunj_makan;        
                            }
                      
	        	
				if( $exists>0)
				{

                    if($row->tunj_makan==0)
                            {
                               $uang_makan=0;
                            }

                            
					 DB::table("p_penggajian")
                             ->where("id_karyawan",$row->id_karyawan)
                             ->where('bulan', $request->bulanpayroll)
                             ->where('tahun', $request->tahunpayroll)
		                      ->update(["nik"=>$row->nik,
		                        "nama_karyawan"=>$row->nama_karyawan,
		                        "id_cabang"=>$row->id_cabang,
		                        "tgl_masuk"=>$row->tgl_masuk,
		                        "tgl_keluar"=>$row->tgl_keluar,
		                        "id_departemen"=>$row->id_departemen,
		                        "id_jabatan"=>$row->id_jabatan,
		                        "status_kerja"=>$row->status_kerja,
		                        "id_ptkp"=>$row->id_ptkp,
		                        "noabsen"=>$row->noabsen,
		                        "nonpwp"=>$row->nonpwp,
            								 "nobpjskes"=>$row->nobpjskes,
            								 "nobjstk"=>$row->nobjstk,
            								 "id_grupabsen"=>$row->id_grupabsen,
		                        "id_bank"=>$row->id_bank,
		                        "no_rekening"=>$row->no_rekening,
		                        "anrekening"=>$row->anrekening,
					//"penempatan"=>$row->penempatan,
		                        "id_grading"=>$row->id_grading,
		                        "gaji_pokokfull"=>$row->gaji_pokok,
		                        "gaji_pokok"=>$gaji_pokok,
		                        "rate_bpjstk"=>$row->rate_bpjstk,
                                "rate_bpjskes"=>$row->rate_bpjskes,
                                "tunj_jabatan"=>$tunj_jabatan,
		                        "tunj_transport"=>$tunj_transport,
		                        //"tunj_khusus"=>$row->tunj_khusus,
		                        "tunj_kemahalan"=>$row->tunj_kemahalan,
								            "tunj_jaga"=>$row->tunj_jaga,
		                        //"tunj_khusus"=>$row->tunj_khusus,
                            "tunj_makan"=>$row->tunj_makan,
		                        "tunj_kemahalan"=>$tunj_kemahalan,
		                        "jenispajak"=>$row->jenispajak,
                           
                                "uang_makan"=>$uang_makan,
                            
		                        "uang_makan"=>$uang_makan,
		                        "potongan_absen"=>$potongan_absen,
		                        "simpanan_wajib"=>$simpanan_wajib,
		                        "pinjaman_koperasi"=>$pot_koperasi,
		                        "potongan_absen"=>$potongan_absen,
		                        "bpjs_kes_kry"=>$bpjs_kes_kry,
                                "bpjs_kes_prs"=>$bpjs_kes_prs,
                                "jht_kry"=> $jht_kry,
                                "jht_prs"=>$jht_prs,
                                "jp_kry"=>0,
                                "jp_prs"=>0,
                                "jkk"=>$jkk,
                                "jkm"=>$jkm,
                                 "th"=>0,
								              "thm"=>0,
                                "makanperhari"=>$umh,
                                "jml_hadir"=>$jml_hadir,
                                 "tunj_pph21"=>0,

                                "pph21"=>0,


		                       ]);
							

		          
		    }
		    else
		    {


		                     DB::table("p_penggajian")
		                    ->insert(["id_karyawan"=>$row->id_karyawan,
		                        "nik"=>$row->nik,
		                        "bulan"=> $request->bulanpayroll,
		                        "tahun"=> $request->tahunpayroll,
		                        "nama_karyawan"=>$row->nama_karyawan,
		                        "tgl_masuk"=>$row->tgl_masuk,
		                        "tgl_keluar"=>$row->tgl_keluar,
		                        "id_cabang"=>$row->id_cabang,
		                        "id_departemen"=>$row->id_departemen,
		                        "id_jabatan"=>$row->id_jabatan,
		                        "status_kerja"=>$row->status_kerja,
		                        "jenispajak"=>$row->jenispajak,
		                        "id_ptkp"=>$row->id_ptkp,
		                        "noabsen"=>$row->noabsen,
		                        "nonpwp"=>$row->nonpwp,
								 "nobpjskes"=>$row->nobpjskes,
								 "nobjstk"=>$row->nobjstk,
								 "id_grupabsen"=>$row->id_grupabsen,
					//"penempatan"=>$row->penempatan,
		                        "id_bank"=>$row->id_bank,
		                        "no_rekening"=>$row->no_rekening,
		                        "anrekening"=>$row->anrekening,
		                        "id_grading"=>$row->id_grading,
		                        "gaji_pokokfull"=>$row->gaji_pokok,
		                        "gaji_pokok"=>$gaji_pokok,
		                        "rate_bpjstk"=>$row->rate_bpjstk,
                                "rate_bpjskes"=>$row->rate_bpjskes,
                                "tunj_jabatan"=>$tunj_jabatan,
		                        "tunj_transport"=>$tunj_transport,
		                        "tunj_khusus"=>$row->tunj_khusus,
		                        "tunj_kemahalan"=>$row->tunj_kemahalan,
								"tunj_jaga"=>$row->tunj_jaga,
		                        "tunj_khusus"=>0,
		                        "tunj_kemahalan"=>$tunj_kemahalan,
		                        "tunj_makan"=>$row->tunj_makan,
		                        
                                "uang_makan"=>$uang_makan,
                           
		                       
		                        "insentif"=>0,
		                        "koreksiplus"=>0,
		                        "koreksimin"=>0,
		                        "potongan_absen"=>$potongan_absen,
		                        "koreksiplus"=>0,
		                        "koreksimin"=>0,
                            "pesangon"=>0,
                            "bonus"=>0,
                            
		                        "simpanan_wajib"=>$simpanan_wajib,
		                        "potongan_absen"=>$potongan_absen,
		                        "pinjaman_koperasi"=>$pot_koperasi,
		                        "tunj_pph21"=>0,
		                        "bpjs_kes_kry"=>$bpjs_kes_kry,
                                "bpjs_kes_prs"=>$bpjs_kes_prs,
                                "jht_kry"=> $jht_kry,
                                "jht_prs"=>$jht_prs,
                                "jp_kry"=>0,
                                "jp_prs"=>0,
                                "jkk"=>$jkk,
                                "jkm"=>$jkm,
                                "th"=>0,
								                "thm"=>0,
                                "makanperhari"=>$umh,
                                "jml_hadir"=>$jml_hadir,
                                "pph21"=>0
		                      
		            ]);
					
		    }

        }






                            
       }
       
}
