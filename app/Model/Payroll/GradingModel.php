<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class GradingModel extends Model
{
    protected $table    = "p_grading";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_grading")
                            ->select("*")
                            ->orderBy("id_grading", "asc");

        if(session()->has("SES_SEARCH_GRADING")) {
            $query->where("nama_grading", "LIKE", "%" . session()->get("SES_SEARCH_GRADING") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("p_grading")
                            ->select("*")
                            ->where("id_grading", $id)
                            ->orderBy("id_grading", "asc");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qgrading              = new GradingModel;
        # ---------------
       // $qgrading->kode       = setString($request->kode);
        $qgrading->kode_grading   = setString($request->kode_grading);
        $qgrading->nama_grading   = setString($request->nama_grading);
        $qgrading->batasbawah     = setNoComma($request->batasbawah);
        $qgrading->batasatas      = setNoComma($request->batasatas);
        $qgrading->user_id        = setString(Auth::user()->id);
        $qgrading->create_at      = setString(date('Y-m-d H:i:s'));
        $qgrading->update_at       = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qgrading->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE GRADING (" . $qgrading->id_grading . ") " . strtoupper($request->grading), Auth::user()->id, $request);
    }

    public function updateData($request) 
    {

         DB::table("p_grading")
                             ->where("id_grading", $request->id_grading)
                            ->update([ 	"kode_grading"=>$request->kode_grading,
                            			"nama_grading"=>$request->nama_grading,
                            			"batasbawah"=>setNoComma($request->batasbawah),
                            			"batasatas"=>setNoComma($request->batasatas),
                            			"user_id"=>setString(Auth::user()->id),
                            	        "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE GRADING(" . $request->id_grading . ") " . strtoupper($request->status), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_grading")
                             ->where("id_grading", $request->id_grading)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE GRADING (" . $request->id_grading . ") " . strtoupper($request->status), Auth::user()->id, $request);
    }
}
