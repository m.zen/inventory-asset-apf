<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class InputgajiModel extends Model
{
    protected $table    = "p_karyawan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $aktif=1;
        $query  = DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik", "a.nama_karyawan","a.gaji_pokok","a.rate_bpjstk","a.rate_bpjskes","a.id_ptkp",
                           "a.tunj_jabatan","e.tunj_jabatan","d.tunj_transport","a.tunj_jaga","a.tunj_kemahalan",
                           "a.tunj_makan","a.nonpwp","a.id_ptkp","a.nobpjskes","a.nobjstk","a.id_bank","a.no_rekening","a.anrekening","a.id_grading",
                           "b.nama_cabang","c.nama_departemen","d.nama_jabatan","a.plafon")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_level as e","e.id_level","=","d.id_level")
                            
                            ->where("a.aktif",$aktif)
                            ->orderBy("a.id_karyawan", "DESC");




        if(session()->has("SES_SEARCH_INPUTGAJI")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_INPUTGAJI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  =DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik", "a.nama_karyawan","a.gaji_pokok","a.rate_bpjstk","a.rate_bpjskes","a.id_ptkp",
                           "a.tunj_jabatan","a.tunj_jabatan","a.tunj_transport","a.tunj_jaga","a.tunj_kemahalan",
                            "a.tunj_makan","a.nonpwp","a.id_ptkp","a.nobpjskes","a.nobjstk","a.id_bank","a.no_rekening","a.anrekening","a.noabsen","a.id_grading",
                            "b.nama_cabang","c.nama_departemen","d.nama_jabatan","d.id_level","a.plafon")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.id_karyawan", $id)
                            ->orderBy("a.nama_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }  

 public function getTnjjabatan($id) {
        $query  =DB::table("m_level")
                            ->select("tunj_jabatan")
                            ->where("id_level", $id);
   
        $result = $query->get();

        return $result;
    }    

    public function updateData($request) {
         $jht_kry        =   setNoComma($request->rate_bpjstk)*2/100;
        if (setNoComma($request->rate_bpjstk) > 8094000)
        {
            $jp_kry     = 8094000 * 1/100;
            $jp_prs     = 8094000 * 2/100;
        }
        else
        {

               $jp_kry  = setNoComma($request->rate_bpjstk) * 1/100; 
               $jp_prs  = setNoComma($request->rate_bpjstk) * 2/100; 
        }

            $jkk           = setNoComma($request->rate_bpjstk) * 0.24/100;
            $jkm           = setNoComma($request->rate_bpjstk) * 0.3/100;
            $jht_prs       = setNoComma($request->rate_bpjstk) * 3.7/100;
        if (setNoComma($request->rate_bpjskes) > 8000000)
        {
           $bpjs_kes_kry     = 8094000 * 1/100;
           $bpjs_kes_prs     = 8094000 * 4/100;
       }
        else
        {

               $bpjs_kes_kry  = setNoComma($request->rate_bpjskes) * 1/100; 
               $bpjs_kes_prs  = setNoComma($request->rate_bpjskes) * 4/100; 
        }

        

         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "gaji_pokok"=>setNoComma($request->gaji_pokok),
                                "rate_bpjstk"=>setNoComma($request->rate_bpjstk),
                                "rate_bpjskes"=>setNoComma($request->rate_bpjskes),
                                "tunj_jabatan"=>setNoComma($request->tunj_jabatan),
                                "tunj_transport"=>setNoComma($request->tunj_transport),
								"tunj_jaga"=>setNoComma($request->tunj_jaga),
                                "tunj_kemahalan"=>setNoComma($request->tunj_kemahalan),
                                "tunj_makan"=>setNoComma($request->tunj_makan),
                                "bpjs_kes_kry"=>$bpjs_kes_kry,
                                "bpjs_kes_prs"=>$bpjs_kes_prs,
                                "jht_kry"=> $jht_kry,
                                "jht_prs"=>$jht_prs,
                                "jp_kry"=>$jp_kry,
                                "jp_prs"=>$jp_prs,
                                "jkk"=>$jkk,
                                "jkm"=>$jkm,
                                "plafon"=>setNoComma($request->plafon),
                                "id_ptkp"=>$request->id_ptkp,
                                "jenispajak"=>2, 
                                "nonpwp"=>$request->nonpwp,
                                "nobpjskes"=>$request->nobpjskes,
                                "nobjstk"=>$request->nobjstk,
                                "id_bank"=>$request->id_bank,
                                "no_rekening"=>$request->no_rekening,
                                "anrekening"=>$request->anrekening,
                                "noabsen"=>$request->noabsen,
                                "id_grading"=>$request->id_grading,
                                "user_id"=>setString(Auth::user()->id),
                            	"update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);


                          
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE INPUTGAJI(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_inputgaji")
                             ->where("id_inputgaji", $request->id_inputgaji)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE INPUTGAJI (" . $request->id_inputgaji . ") " . strtoupper($request->nama_inputgaji), Auth::user()->id, $request);
    }
}
