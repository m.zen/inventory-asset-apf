<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class SlipModel extends Model
{
      public function getGaji($nik,$bulan,$tahun) {
      	//dd($nik,$bulan,$tahun);
        $query  =DB::table("p_penggajian as a")
                            ->select("a.*", "b.nama_cabang","c.nama_departemen","d.nama_jabatan","d.id_level")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.nik", $nik)
                            ->where("a.bulan", $bulan)
                            ->where("a.tahun", $tahun)
                           ->orderBy("a.nama_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }  
}
