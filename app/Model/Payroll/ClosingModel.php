<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class ClosingModel extends Model
{
   protected $table    = "m_profilesystem";
    public $timestamps= false ;

    

    public function getProfile() {
        $query  = DB::table("m_profilesystem")
                            ->select("*");
                           
        $result = $query->get();

        return $result;
    }    

    

    public function updateData($request) 
    {
        
        //ubah profile system
        //dd($request->tgl_absenawal,$request->tgl_absenakhir,$request->tgl_payrollawal,$request->tgl_payrollakhir);
       // $tglabsenawal     = setYMD($request->tgl_absenawal,"/");
       // $tglabsenakhir    = setYMD($request->tgl_absenakhir,"/");
       // $tglpayrollawal   = setYMD($request->tgl_payrollawal,"/");
       // $tglpayrollakhir  = setYMD($request->tgl_payrollakhir,"/");
        $tglpayrollawal   = $request->tgl_payrollawal;
       $tglpayrollakhir  = $request->tgl_payrollakhir;
      



        $tgl_payrollawal  = date("Y-m-d",strtotime("+1 months",strtotime( $tglpayrollawal )));
        $tgl_payrollakhir = date("Y-m-d",strtotime("+1 months",strtotime($tglpayrollakhir))); 
        $bulan            = $request->bulanpayroll;
        $tahun            = $request->tahunpayroll;
        $bulan2           = $request->bulanpayroll2;
        $tahun2           = $request->tahunpayroll2;

       // Backup Data  

           DB::table("p_penggajianhistori")
                                 ->where("bulan",$bulan)
                                 ->where("tahun", $tahun)
                                 ->delete();

           $statement = "INSERT INTO p_penggajianhistori SELECT * FROM p_penggajian where bulan=".$bulan." and tahun=".$tahun;

          DB::statement($statement);


         // Update Pinjaman 1
           DB::statement("UPDATE p_pinjaman  SET status_pinjaman='F' where angsuranke = tenor");
           $qStatement = "UPDATE p_pinjaman  SET bulan=".$bulan2.",tahun=".$tahun2.",angsuranke=angsuranke+1,                          saldo_angsuran=saldo_angsuran-angsuran,saldo_bunga=saldo_bunga-(total_bunga/tenor),saldo_pokok = saldo_pokok-(jumlah_pinjaman/tenor) where angsuranke < tenor and status_pinjaman='T' and bulan=".$bulan." and tahun=".$tahun;

           
            DB::statement($qStatement);


           
              $qUpdatekry = "UPDATE p_karyawan   SET aktif=0  where tgl_keluar between '" .$tglpayrollawal ."' and '" .$tglpayrollakhir ."'";
            
             
             DB::statement($qUpdatekry);



           DB::table("m_profilesystem")
                                ->update([  "bulanpayroll"=>$request->bulanpayroll2,
                                            "tahunpayroll"=>$request->tahunpayroll2,    
                                              "tgl_payrollawal"=>$tgl_payrollawal,
                                            "tgl_payrollakhir"=>$tgl_payrollakhir,
						"user_id"=>setString(Auth::user()->id),
                                		"update_at"=>setString(date('Y-m-d H:i:s'))    
                                          ]);
        /*
        DB::beginTransaction();

        try {
                
             DB::table("m_profilesystem")
                                ->update([  "bulanpayroll"=>$request->bulanpayroll2,
                                            "tahunpayroll"=>$request->tahunpayroll2,    
                                            "tgl_absenawal"=>$tgl_absenawal,
                                            "tgl_absenakhir"=>$tgl_absenakhir,
                                            "tgl_payrollawal"=>$tgl_payrollawal,
                                            "tgl_payrollakhir"=>$tgl_payrollakhir
                                          ]);


           //Backup Data  

           DB::table("p_penggajianhistori")
                                 ->where("bulan", $request->$request->bulanpayroll)
                                 ->where("tahun", $request->$request->tahunpayroll)
                                 ->delete();

           $statement = "INSERT INTO p_penggajianhistori SELECT * FROM p_penggajian where bulan=".$bulan." and tahun=".$tahun;

          DB::statement($statement);


          //Update Pinjaman
           DB::statement("UPDATE p_pinjaman  SET status_pinjaman='F' where angsuranke = tenor");

           $qStatement = "UPDATE p_pinjaman  SET bulan=".$bulan.",tahun=".$tahun.",angsuranke=angsuranke+1,                          saldo_angsuran=saldo_angsuran-angsuran,saldo_bunga=saldo_bunga-(total_bunga/tenor),saldo_pokok = saldo_pokok-(jumlah_pinjaman/tenor) where angsuranke < tenor and status_pinjaman='T'";
             DB::statement($qStatemen);

            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }


*/
                            
       }

     


   
}
