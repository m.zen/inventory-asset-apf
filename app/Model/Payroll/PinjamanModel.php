<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class PinjamanModel extends Model
{
    protected $table    = "p_pinjaman";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_pinjaman as a")
                            ->select("a.*",DB::raw('CONCAT(a.jumlah_pinjaman + a.total_bunga) AS  total'),"c.nik","c.nama_karyawan","d.nama_cabang")
                            ->leftjoin("p_koperasi as b","b.id_koperasi","=","a.id_koperasi")
                            ->leftjoin("p_karyawan as c","b.id_karyawan","=","c.id_karyawan")
                            ->leftjoin("m_cabang as d","d.id_cabang","=","c.id_cabang")
                            ->where("status_pinjaman","T")
                            ->orderBy("id_pinjaman", "DESC");

        if(session()->has("SES_SEARCH_PINJAMAN")) {
            $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PINJAMAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
     public function getProfile($id) {
        $query  = DB::table("p_pinjaman as a")
                            ->select("a.*","b.id_koperasi","c.id_karyawan","c.nik","c.nama_karyawan","d.nama_cabang")
                            ->leftjoin("p_koperasi as b","b.id_koperasi","=","a.id_koperasi")
                            ->leftjoin("p_karyawan as c","b.id_karyawan","=","c.id_karyawan")
                            ->leftjoin("m_cabang as d","d.id_cabang","=","c.id_cabang")
                            
                            ->where("id_pinjaman", $id)
                            ->orderBy("id_pinjaman", "ASC");
        $result = $query->get();

        return $result;
    }

    public function getSaldo($id) {
        $query  = DB::table("p_pinjaman as a")
                            ->select("a.*","c.nik","c.nama_karyawan","d.nama_cabang")
                            ->leftjoin("p_koperasi as b","b.id_koperasi","=","a.id_koperasi")
                            ->leftjoin("p_karyawan as c","b.id_karyawan","=","c.id_karyawan")
                            ->leftjoin("m_cabang as d","d.id_cabang","=","c.id_cabang")
                            
                            ->where("id_pinjaman", $id)
                            ->orderBy("id_pinjaman", "ASC");
        $result = $query->get();

        return $result;
    }


    public function getAngsuranpinjaman($id) {
        $query  = DB::table("p_angsuranpinjaman")
                            ->select("*")
                            ->where("id_pinjaman", $id)
                            ->orderBy("cicilanke", "ASC");
        $result = $query->get();
        return $result;
    }        

    public function createData($request) {


    	/*----------------------------------------------------------
                    Baca Parameter
        ------------------------------------------------------------*/

        $jumlah_pinjaman        = setNoComma($request->jumlah_pinjaman);
        $bunga                  = $request->bunga_pinjaman;
        $jumlahbunga            = ($jumlah_pinjaman*$bunga/100);

        $tenor                  = $request->tenor;
         if ($tenor<12)
        {

           $jumlahbunga            = ($jumlah_pinjaman*$bunga/100)*$tenor/12;
        }
        $bulan                  = $request->bulanpayroll;
        $tahun                  = $request->tahunpayroll;
        $bulanakhir             = $bulan+$tenor-1;
        $tahunakhir             = $tahun;
        $varbulanakhir          = $bulan;
        /*-----------------------------------------------------------------
                   Cari Tahun
        -----------------------------------------------------------------*/   
        if ($bulanakhir > 12)
        {
            for($i=$bulan; $i<=($bulan+$tenor-1); $i++) 
            {
                   $varbulanakhir=$varbulanakhir+1;
                   if ($varbulanakhir > 12)
                   {
                    $varbulanakhir=1;
                    $tahunakhir=$tahunakhir+1;
                   } 
             
            }
            $bulanakhir=$varbulanakhir;
        }
         

        $pokokbulanan           = round($jumlah_pinjaman/$tenor);
        $bungabulanan           = round($jumlahbunga/$tenor);
        $angsuran               = round(( $jumlah_pinjaman+  $jumlahbunga )/$tenor);
        # ---------------
       
       /*------------------------------------------------------------------------
                            Insert Data Baru
       --------------------------------------------------------------------------*/

        $id_pinjaman     = DB::table("p_pinjaman")->insertGetId(
                ["id_koperasi"     => setString($request->id_koperasi)
                ,"tgl_pinjaman"    => setYMD($request->tgl_pinjaman,"/")
                ,"jumlah_pinjaman" => setNoComma($request->jumlah_pinjaman)
                ,"bunga_pinjaman"  => setString($request->bunga_pinjaman)
                ,"total_bunga"     => $jumlahbunga
                ,"angsuranke"      => 1
                ,"saldo_angsuran"  => ( $jumlah_pinjaman+  $jumlahbunga )
                ,"saldo_pokok"     => ( $jumlah_pinjaman )
                ,"saldo_bunga"     => ( $jumlahbunga )
                ,"bulanawal"       => $bulan
                ,"tahunawal"       => $tahun
                ,"bulanakhir"       => $bulanakhir
                ,"tahunakhir"       => $tahunakhir
                ,"bulan"       => $bulan
                ,"tahun"       => $tahun
                 ,"tenor"           => setNoComma($request->tenor)
                ,"angsuran"        => $angsuran
                ,"status_pinjaman" => "T"
                ,"user_id"         => setString(Auth::user()->id)
                ,"create_at"       => setString(date('Y-m-d H:i:s'))
                ,"update_at"       => setString(date('Y-m-d H:i:s'))

                ]);

      /*
        $cicilanke          =0;
        $bulanarr           =$bulan;
        $tahunarr           =$tahun;
        $saldo_angsuran     =$angsuran*$tenor; 
        $saldo_pokok        =$pokokbulanan*$tenor; 
        $saldo_bunga        =$bungabulanan*$tenor;
        for($i=1; $i<=$tenor; $i++) 
        {
           

           $cicilanke        = $i; 
           $jml_angsuran     = $angsuran;
           $bayar_angsuran   = 0;
           $saldo_angsuran   = $saldo_angsuran - $angsuran; 
           $saldo_pokok      = $saldo_pokok - $pokokbulanan; 
           $saldo_bunga      = $saldo_bunga - $bungabulanan;
           $bulanangsur      = $bulanarr;
           $tahunangsur      = $tahunarr;
            

              DB::statement("INSERT INTO p_angsuranpinjaman (
                            id_pinjaman,
                            cicilanke,
                            jml_angsuran,
                            bayar_angsuran,
                            saldo_angsuran,
                            saldo_pokok,
                            saldo_bunga,
                            bulanangsur,
                            tahunangsur,
                            status
                        )
                        values
                        (
                            $id_pinjaman,
                            $cicilanke,
                            $jml_angsuran,
                            $bayar_angsuran,
                            $saldo_angsuran,
                            $saldo_pokok,
                            $saldo_bunga,
                            $bulanangsur,
                            $tahunangsur,
                            1

                          )"
                        );
           $bulanarr=$bulanarr+1;
           if ( $bulanarr >12)
           {

                $bulanarr = 1;
                $tahunarr = $tahunarr+1;
        //        dd($bulanarr,$tahunarr,$tenor);
           }

        }

        



        */
            $qLog       = new LogModel;
            # ---------------;
           $qLog->createLog("CREATE PINJAMAN (" . $id_pinjaman . ") " . strtoupper($request->nama_pinjaman), Auth::user()->id, $request);
    }

public function updateData_lunas($request)
{

     DB::table("p_pinjaman")
                             ->where("id_pinjaman", $request->id_pinjaman)
                            ->update([ "status_pinjaman" => "F",



                                        "user_id"          => setString(Auth::user()->id),
                                        "create_at"        => setString(date('Y-m-d H:i:s')),
                                        "update_at"        => setString(date('Y-m-d H:i:s'))]);

}

    public function updateData($request) {

        $qpinjaman              = new PinjamanModel;
        $jumlah_pinjaman        = setNoComma($request->jumlah_pinjaman);
        $bunga                  = $request->bunga_pinjaman;
        $jumlahbunga            = ($jumlah_pinjaman*$bunga/100); 
        $tenor                  = $request->tenor; 
        $angsuran               = ( $jumlah_pinjaman+  $jumlahbunga )/$tenor;
/*
         DB::table("p_pinjaman")
                             ->where("id_pinjaman", $request->id_pinjaman)
                            ->update([ "id_koperasi"       => setString($request->id_koperasi),
                                        "tgl_pinjaman"     => setYMD($request->tgl_pinjaman,"/"),
                                        "jumlah_pinjaman"  => setNoComma($request->jumlah_pinjaman),
                                        "bunga_pinjaman"   => setString($request->bunga_pinjaman),
                                        "total_bunga"      => $jumlahbunga,
                                        "saldo_angsuran"  => ( $jumlah_pinjaman+  $jumlahbunga ),
                                        "saldo_pokok"     => ( $jumlah_pinjaman ),
                                        "saldo_bunga"     => ( $jumlahbunga ),
                                        "bulanawal"       => $bulan,
                                        "tahunawal"       => $tahun,
                                        "tenor"           => setNoComma($request->tenor),
                                        "angsuran"        => $angsuran,
                                        "status_pinjaman" => "T",



                                        "user_id"          => setString(Auth::user()->id),
                                        "create_at"        => setString(date('Y-m-d H:i:s')),
                                        "update_at"        => setString(date('Y-m-d H:i:s'))]);

               
     

         $qData  = DB::select("SELECT  *   FROM  p_pinjaman ");
 

        foreach($qData as $row) 
        {


                $cicilanke          =0;
                $bulanarr           =$row->bulanawal;
                $tahunarr           =$row->tahunawal;
                $tenor              =$row->tenor;
                $angsuran           =$row->angsuran; 
                $saldo_angsuran     =$angsuran*$tenor; 
                $saldo_pokok        =$angsuran*$tenor; 
                $saldo_bunga        =0;
                $pokokbulanan       = $angsuran;
                $angsuranke         =$row->angsuranke-1;
                $id_pinjaman        =$row->id_pinjaman;
                for($i=1; $i<=$tenor; $i++) 
                {
                   

                   $cicilanke        = $i; 
                   $jml_angsuran     = $angsuran;
                   if ($angsuranke >0)
                   {
                         $bayar_angsuran=$angsuran;
                   }
                   else
                   {
                         $bayar_angsuran   = 0;
                    
                   }
                   $saldo_angsuran   = $saldo_angsuran - $angsuran; 
                   $saldo_pokok      = $saldo_pokok - $pokokbulanan; 
                   $saldo_bunga      = 0;
                   $bulanangsur      = $bulanarr;
                   $tahunangsur      = $tahunarr;
                    

                      DB::statement("INSERT INTO p_angsuranpinjaman (
                                    id_pinjaman,
                                    cicilanke,
                                    jml_angsuran,
                                    bayar_angsuran,
                                    saldo_angsuran,
                                    saldo_pokok,
                                    saldo_bunga,
                                    bulanangsur,
                                    tahunangsur,
                                    status
                                )
                                values
                                (
                                    $id_pinjaman,
                                    $cicilanke,
                                    $jml_angsuran,
                                    $bayar_angsuran,
                                    $saldo_angsuran,
                                    $saldo_pokok,
                                    $saldo_bunga,
                                    $bulanangsur,
                                    $tahunangsur,
                                    1

                                  )"
                                );
                   $bulanarr=$bulanarr+1;
                   $angsuranke=$angsuranke-1;
                   if ( $bulanarr >12)
                   {

                        $bulanarr = 1;
                        $tahunarr = $tahunarr+1;
                //        dd($bulanarr,$tahunarr,$tenor);
                   }

                }
             }   
                           
                      
            $qLog       = new LogModel;
            $qLog->createLog("UPDATE PINJAMAN(" . $request->id_pinjaman . ") " . strtoupper($request->nama_pinjaman), Auth::user()->id, $request);
            
    }

    public function removeData($request) {
         DB::table("p_pinjaman")
                             ->where("id_pinjaman", $request->id_pinjaman)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE PINJAMAN (" . $request->id_pinjaman . ") " . strtoupper($request->nama_pinjaman), Auth::user()->id, $request);
    }  
}
