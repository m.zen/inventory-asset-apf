<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class TarifModel extends Model
{
    protected $table    = "p_tarif";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_tarif")
                            ->select("id_tarif", "batasbawah","batasatas","tarif")
                            ->orderBy("batasbawah", "asc");

        if(session()->has("SES_SEARCH_TARIF")) {
            $query->where("batasbawah", "LIKE", "%" . session()->get("SES_SEARCH_TARIF") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("p_tarif")
                            ->select("id_tarif","batasbawah", "batasatas","tarif")
                            ->where("id_tarif", $id)
                            ->orderBy("batasbawah", "asc");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qtarif              = new TarifModel;
        # ---------------
       // $qtarif->kode       = setString($request->kode);
        $qtarif->batasbawah       =setNoComma($request->batasbawah);
        $qtarif->batasatas        =setNoComma($request->batasatas);
        $qtarif->tarif         	  = setNoComma($request->tarif);
        $qtarif->user_id          = setString(Auth::user()->id);
        $qtarif->create_at        = setString(date('Y-m-d H:i:s'));
        $qtarif->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qtarif->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE TARIF (" . $qtarif->id_tarif . ") " . strtoupper($request->batasbawah), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("p_tarif")
                             ->where("id_tarif", $request->id_tarif)
                            ->update([ "batasbawah"=>setNoComma($request->batasbawah),
                            			"batasatas"=>setNoComma($request->batasatas),
                            			"tarif"=>setNoComma($request->tarif),
                            		   	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE TARIF(" . $request->id_tarif . ") " . strtoupper($request->nama_tarif), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_tarif")
                             ->where("id_tarif", $request->id_tarif)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE TARIF (" . $request->id_tarif . ") " . strtoupper($request->nama_tarif), Auth::user()->id, $request);
    }
}
