<?php

namespace App\Model\Payroll;
use DB;
use Auth;
use App\Model\LogModel;
use Illuminate\Database\Eloquent\Model;

class HitungthrModel extends Model
{
    protected $table    = "m_profilesystem";
    public $timestamps= false ;

    

    public function getProfile() {
        $query  = DB::table("m_profilesystem")
                            ->select("tahunthr","bulanthr","tgl_thr");
                           
        $result = $query->get();

        return $result;
    }
    

   /*Function cari PTKP      */
    function Ptkp($id_ptkp)
    {
      $query  = DB::table("p_ptkp")
                ->select("ptkp")
                ->where("id_ptkp",$id_ptkp)->get()->first();
                           
       if(empty($query))
       {
            $result=0;
       }
       else
       {
        $result = $query->ptkp; 
       }    
       

        return $result;

    } 

    /* Fuction Cara Hitung PPH21  */
    function Hitungpph21($pkp,$mk)
    {
         
        $pajak = 0;
        
        //=IF(C19<=50000000,C19*5%,IF(AND(C19>50000000,C19<=250000000),(50000000*5%)+(C19-50000000)*15%,IF(AND(C19>250000000,C19<=500000000),(50000000*5%)+(C19-250000000*15%)+(C19-200000000*25%),(50000000*5%)+(C19-250000000*15%)+(C19-200000000*25%)+(500000000*30%))))        
        if($pkp<50000000)
        {
            $pajak=$pkp*5/100;
        }
        elseif($pkp>50000000 and $pkp<=250000000)
        {
            $pajak=(50000000*5/100) + (($pkp-50000000)*15/100);

        }
        elseif($pkp>250000000 and $pkp<=500000000)
        {
            $pajak=(50000000*5/100) +(200000000*15/100) + (($pkp-250000000)*25/100);

        }
        elseif($pkp>500000000)
        {
            $pajak=(50000000*5/100) +(200000000*15/100) + (250000000*25/100)+(($pkp-500000000)*30/100);
        }


        $pajak=$pajak/$mk;
        
        
        return $pajak;
    }

   
    
   public function updatepphnonthr($gaji_pokok,$id_ptkp,$tgl_masuk,$jenispajak) 
    {

             
            $thr            = new  HitungthrModel(); 
            $thr            = $this->getProfile()->first();
            $tahunthr       = $thr->tahunthr;
            $pph            = 0;

             $ptkp = $this->ptkp($id_ptkp);

              $tahunmasuk = date('Y', strtotime($tgl_masuk));
              $bulanmasuk = date('m', strtotime($tgl_masuk));
              $jenispajak = $jenispajak;
              $tnj_pph21  = 0; 
            
              $masakerja  =12;
              if ($tahunmasuk == $tahunthr) 
              {

                $masakerja    = 13 - $bulanmasuk;
              } 

               $pphpendapatan  = $gaji_pokok;
             
              $bijab          =  ($pphpendapatan * 5)/100;
              //Biaya jabayan =IF(C9*5%>500000,500000,C9*5%)
              if ($bijab > 500000)
              {
                    $bijab = 500000;

              }  
  //$pphpotongan         = ($bijab+$bpjs_kes_kry+$jht_kry+$jp_kry);
              $pphpotongan               = ($bijab);

              $pphpendapatantahun        = $pphpendapatan * $masakerja;
              $pphpendapatantahunawal    = $pphpendapatan * $masakerja;
              $pphpotongantahun          = $pphpotongan * $masakerja;
              $ptkptahun                 = $this->Ptkp($id_ptkp);
              $pkp                       = $pphpendapatantahun - $pphpotongantahun - $ptkptahun;
             
              if($pkp>0)
              {

                $pph=$this->Hitungpph21($pkp,$masakerja);
                $pphawal = $pph;
                $cekpph[0]=$pph;
                $cekpe[0]=$pphpendapatantahun;
                $cekpo[0]=$pphpotongantahun;
                $cekptkp[0]=$ptkptahun; 
                $cekpkp[0]=$pkp;


                if ($jenispajak==2)
                    {
                        $it =0;
                        Iterasi:
                        {

                              //$pphpendapatan  = ($tnj_pph21+$jht_prs+$jp_prs+ $jkk+$jkm +$bpjs_kes_prs+ $gaji +$row->tunj_jabatan+$row->tunj_transport+$row->tunj_khusus+$row->tunj_jaga + $row->tunj_kemahalan);
                              $pphpendapatan  = ($tnj_pph21+$gaji_pokok);
                                        
                              $bijab          =  ($pphpendapatan * 5)/100;
                              //Biaya jabayan =IF(C9*5%>500000,500000,C9*5%)
                              if ($bijab > 500000)
                              {
                                    $bijab = 500000;

                              }  

                             // $pphpotongan         = ($bijab+$bpjs_kes_kry+$jht_kry+$jp_kry);
                              $pphpotongan         = ($bijab);
                              $pphpendapatantahun  = $pphpendapatan * $masakerja;
                              $pphpotongantahun    = $pphpotongan * $masakerja; 
                              $pkp                 = $pphpendapatantahun - $pphpotongantahun - $ptkptahun;
                              $pph                 = 0;
                              $pph=$this->Hitungpph21($pkp,$masakerja); 
                              $it=$it+1;
                              $cekpph[$it]=$pph;
                              $cekpe[$it]=$pphpendapatantahun;
                              $cekpo[$it]=$pphpotongantahun;
                              $cekptkp[$it]=$ptkptahun;
                              $cekpkp[$it]=$pkp; 
                             
                        } 
                         if (($tnj_pph21 < $pph))
                        {

                                $tnj_pph21 =$pph;
                                goto Iterasi;
                        }
                    }
                      
                     
            
            }
            
         return $pph;
        
   }

    public function updatepphthr($gaji_pokok,$nilaithr,$id_ptkp,$tgl_masuk,$jenispajak) 
    {

            
            $thr            = new  HitungthrModel(); 
            $thr            = $this->getProfile()->first();
            $tahunthr       = $thr->tahunthr;
            $pph            = 0;
             $ptkp = $this->ptkp($id_ptkp);

              $tahunmasuk = date('Y', strtotime($tgl_masuk));
              $bulanmasuk = date('m', strtotime($tgl_masuk));
              $jenispajak = $jenispajak;
              $tnj_pph21  = 0; 
            
              $masakerja  =12;
              if ($tahunmasuk == $tahunthr) 
              {

                $masakerja    = 13 - $bulanmasuk;
              } 

               $pphpendapatan  = $gaji_pokok;
             
              $bijab          =  ($pphpendapatan * 5)/100;
              //Biaya jabayan =IF(C9*5%>500000,500000,C9*5%)
              if ($bijab > 500000)
              {
                    $bijab = 500000;

              }  
  //$pphpotongan         = ($bijab+$bpjs_kes_kry+$jht_kry+$jp_kry);
              $pphpotongan               = ($bijab);

              $pphpendapatantahun        = $pphpendapatan * $masakerja;
              $pphpendapatantahunawal    = $pphpendapatan * $masakerja;
              $pphpotongantahun          = $pphpotongan * $masakerja;
              $ptkptahun                 = $this->Ptkp($id_ptkp);
              $pkp                       = $pphpendapatantahun - $pphpotongantahun - $ptkptahun;
             


              if($pkp>0)
              {

                $pph=$this->Hitungpph21($pkp,$masakerja);

                $pphawal = $pph;
                $cekpph[0]=$pph;
                $cekpe[0]=$pphpendapatantahun;
                $cekpo[0]=$pphpotongantahun;
                $cekptkp[0]=$ptkptahun; 
                $cekpkp[0]=$pkp;


                if ($jenispajak==2)
                    {
                        $it =0;
                        Iterasi:
                        {

                              //$pphpendapatan  = ($tnj_pph21+$jht_prs+$jp_prs+ $jkk+$jkm +$bpjs_kes_prs+ $gaji +$row->tunj_jabatan+$row->tunj_transport+$row->tunj_khusus+$row->tunj_jaga + $row->tunj_kemahalan);
                              $pphpendapatan  = ($tnj_pph21+$gaji_pokok);
                                        
                              $bijab          =  ($pphpendapatan * 5)/100;
                              //Biaya jabayan =IF(C9*5%>500000,500000,C9*5%)
                              if ($bijab > 500000)
                              {
                                    $bijab = 500000;

                              }  

                             // $pphpotongan         = ($bijab+$bpjs_kes_kry+$jht_kry+$jp_kry);
                              $pphpotongan         = ($bijab);
                              $pphpendapatantahun  = $pphpendapatan * $masakerja+$nilaithr;
                              $pphpotongantahun    = $pphpotongan * $masakerja; 
                              $pkp                 = $pphpendapatantahun - $pphpotongantahun - $ptkptahun;
                              $pph                 = 0;
                              $pph=$this->Hitungpph21($pkp,$masakerja); 
                              $it=$it+1;
                              $cekpph[$it]=$pph;
                              $cekpe[$it]=$pphpendapatantahun;
                              $cekpo[$it]=$pphpotongantahun;
                              $cekptkp[$it]=$ptkptahun;
                              $cekpkp[$it]=$pkp; 
                              
                             

                        } 
                         if (($tnj_pph21 < $pph))
                        {

                                $tnj_pph21 =$pph;
                               
                                goto Iterasi;
                        }

                    }
             
            
            }
              
            return $pph;

        
   }


    public function updateData($request) 
    {
  
    
        $tgl_thr      = setYMD($request->tgl_thr,"/"); 
        $tglthr       = date_create($tgl_thr);
        $bulan        = $request->bulanthr;
        $tahun        = $request->tahunthr;


        DB::table("p_thr")
                             ->where("bulan", $bulan)
                             ->where("tahun", $tahun)
                             ->delete();

  
             







        $qData  = DB::select("SELECT  * FROM  p_karyawan where aktif=1 and ISNULL(tgl_keluar) order by id_karyawan
                            ");

 

        foreach($qData as $row) 
        {

        /* -----------------------------------
         Proses Gaji Prorate
         -------------------------------------*/
                 $ptkp           = $this->ptkp($row->id_ptkp);
                 $gaji           = $row->gaji_pokok;
                 $tgl_masuk      = $row->tgl_masuk;
                 $tglmasuk       = date_create($tgl_masuk);
                 $masakerjatahun =0;
                 $masakerjabulan =0;
                 $masakerjahari  =0; 
                 //dd($tglmasuk,$tglthr);
                 $jumlahhari     = date_diff($tglmasuk,$tglthr)->format("%a");
                 $sisahari       = $jumlahhari;
                 //$masatahun      = round($jumlahhari/365,PHP_ROUND_HALF_DOWN);
                 //$masabulan      = round($jumlahhari-($masatahun*12))
                  $tahunmasuk = date('Y', strtotime($tgl_masuk));
                  $bulanmasuk = date('m', strtotime($tgl_masuk));
                         
                   $masakerja  =12;
                  if ($tahunmasuk == $tahun) 
                  {

                    $masakerja    = 13 - $bulanmasuk;
                  } 
               
                 
                if ($jumlahhari > 30)
                {

                        //Hitung tahun

                        if ($jumlahhari >=365)
                        {
                           $masakerjatahun=$jumlahhari/360;
                           $sisahari = $jumlahhari - (floor($masakerjatahun)*365) ;

                        }

                        //Hitung Bulan

                        if ($sisahari >= 30)
                        {
                              $masakerjabulan= $sisahari/30;
                              $sisahari = $sisahari - (floor($masakerjabulan)*30) ;
                        }

                        // Hitung Hari
                        if ($sisahari >= 1)
                        {
                              $masakerjahari= $sisahari;
                             
                        }


                        $thr =  $gaji;
                        if ($jumlahhari < 365)
                        {
                            $thr =  $gaji * $jumlahhari / 365;
                         } 
                        $exists = DB::table('p_thr')
                          ->where('id_karyawan', $row->id_karyawan)
                          ->where('bulan', $bulan)
                          ->where('tahun', $tahun)
                          ->count();
                          //PPH21 THR
                                $pph21thr=$this->updatepphthr($row->gaji_pokok,$thr,$row->id_ptkp,$row->tgl_masuk,$row->jenispajak);
                                $pph21nonthr=$this->updatepphnonthr($row->gaji_pokok,$row->id_ptkp,$row->tgl_masuk,$row->jenispajak);
                                $totalpphthr =  ($pph21thr -  $pph21nonthr)*$masakerja;

                                // if($row->nik=="2000.0918.0050")
                                // {
                                //         dd($pph21thr ,  $pph21nonthr,$thr,$gaji,$masakerja);

                                // }
                
                            if( $exists>0)
                            {


                                          DB::table("p_thr")
                                         ->where("id_karyawan",$row->id_karyawan)
                                          ->update(["nik"=>$row->nik,
                                            "tahun"=>$tahun,
                                            "bulan"=>$bulan,
                                            "tgl_thr"=>$tgl_thr,
                                            "nama_karyawan"=>$row->nama_karyawan,
                                            "id_cabang"=>$row->id_cabang,
                                            "tgl_masuk"=>$row->tgl_masuk,
                                            "id_departemen"=>$row->id_departemen,
                                            "id_jabatan"=>$row->id_jabatan,
                                            "status_kerja"=>$row->status_kerja,
                                            "id_ptkp"=>$row->id_ptkp,
                                            "id_bank"=>$row->id_bank,
                                            "no_rekening"=>$row->no_rekening,
                                            "anrekening"=>$row->anrekening,
                                            "jenispajak"=>$row->jenispajak,
                                            "gaji_pokok"=> $gaji,
                                            "thr"=>$thr,
                                            "masakerjahari"=>$masakerjahari,
                                             "masakerjabulan"=>$masakerjabulan,
                                              "masakerjatahun"=>$masakerjatahun,
                                            "tunj_pph21"=>$totalpphthr,
                                            "pph21"=>$totalpphthr
                                           ]);
                                        

                              
                        }
                        else
                        {


                                         DB::table("p_thr")
                                        ->insert(["id_karyawan"=>$row->id_karyawan,
                                            "nik"=>$row->nik,
                                            "tahun"=>$tahun,
                                            "bulan"=>$bulan,
                                            "tgl_thr"=>$tgl_thr,
                                            "nama_karyawan"=>$row->nama_karyawan,
                                            "tgl_masuk"=>$row->tgl_masuk,
                                            "id_cabang"=>$row->id_cabang,
                                            "id_departemen"=>$row->id_departemen,
                                            "id_jabatan"=>$row->id_jabatan,
                                            "status_kerja"=>$row->status_kerja,
                                            "jenispajak"=>$row->jenispajak,
                                            "id_ptkp"=>$row->id_ptkp,
                                            "id_bank"=>$row->id_bank,
                                            "no_rekening"=>$row->no_rekening,
                                            "anrekening"=>$row->anrekening,
                                            "gaji_pokok"=> $gaji,
                                             "masakerjahari"=>$masakerjahari,
                                             "masakerjabulan"=>$masakerjabulan,
                                              "masakerjatahun"=>$masakerjatahun,
                                            "thr"=>$thr,
                                            "tunj_pph21"=>$totalpphthr,
                                            
                                            "pph21"=>$totalpphthr
                                          
                                ]);
                                
                            }
               }
             
            }

            /* ------------------------------------------------------------------
                    UPDATE PROFILE TABLE

            -----------------------------------------------------------------------*/
            

            DB::table("m_profilesystem")
                                          ->update(["tahunthr"=>$tahun,
                                            "bulanthr"=>$bulan,
                                            "tgl_thr"=>$tgl_thr
                                           
                                           ]);

      }




}
         
