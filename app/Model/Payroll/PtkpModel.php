<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class PtkpModel extends Model
{
    protected $table    = "p_ptkp";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_ptkp")
                            ->select("id_ptkp", "status","ptkp")
                            ->orderBy("status", "asc");

        if(session()->has("SES_SEARCH_PTKP")) {
            $query->where("status", "LIKE", "%" . session()->get("SES_SEARCH_PTKP") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("p_ptkp")
                            ->select("id_ptkp","status","ptkp")
                            ->where("id_ptkp", $id)
                            ->orderBy("status", "asc");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qptkp              = new PtkpModel;
        # ---------------
       // $qptkp->kode       = setString($request->kode);
        $qptkp->status       = setString($request->status);
        $qptkp->ptkp           = setNoComma($request->ptkp);
        $qptkp->user_id        = setString(Auth::user()->id);
        $qptkp->create_at        = setString(date('Y-m-d H:i:s'));
        $qptkp->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qptkp->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE PTKP (" . $qptkp->id_ptkp . ") " . strtoupper($request->ptkp), Auth::user()->id, $request);
    }

    public function updateData($request) 
    {

         DB::table("p_ptkp")
                             ->where("id_ptkp", $request->id_ptkp)
                            ->update([ 	"ptkp"=>$request->ptkp,
                            		   	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE PTKP(" . $request->id_ptkp . ") " . strtoupper($request->status), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_ptkp")
                             ->where("id_ptkp", $request->id_ptkp)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE PTKP (" . $request->id_ptkp . ") " . strtoupper($request->status), Auth::user()->id, $request);
    }
}
