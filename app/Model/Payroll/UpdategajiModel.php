<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Payroll\ProsesgajiModel;
use App\Model\Personalia\KaryawanModel;
use Excel;

class UpdategajiModel extends Model
{
   protected $table    = "p_penggajian";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $gaji   = new  ProsesgajiModel(); 
		 $tglPayroll  = $gaji->getProfile()->first();
		 $bulan       = $tglPayroll->bulanpayroll;
         $tahun       = $tglPayroll->tahunpayroll; 
      
		$query  = DB::table("p_penggajian as a")
                           ->select("a.id_penggajian","a.nik","a.nama_karyawan","a.tunj_khusus","a.koreksiplus","a.koreksimin","a.insentif","a.bonus","a.pesangon","b.nama_cabang","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
							->where("a.bulan",$bulan)
							->where("a.tahun",$tahun)
							
                            
                            ->orderBy("b.id_cabang","a.nama_karyawan", "asc");



        // dd(session()->has("SES_SEARCH_UPDATEGAJI"));
        if(session()->has("SES_SEARCH_UPDATEGAJI")) {
                 $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_UPDATEGAJI") . "%")
                       ->orwhere("b.nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_UPDATEGAJI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
     public function getStatuspajak($id_ptkp)
    {
      $query  = DB::table("p_ptkp")
                ->select("status")
                ->where("id_ptkp",$id_ptkp)->get()->first();
                           
       if(empty($query))
       {
            $result=0;
       }
       else
       {
        $result = $query->status; 
       }    
       

        return $result;

    } 

    public function getProfile($id) {
        $query  = DB::table("p_penggajian")
                            ->select("id_penggajian","nik","nama_karyawan","koreksiplus","pesangon","bonus","koreksimin","insentif","tunj_khusus")
                            ->where("id_penggajian", $id)
                           ->orderBy("id_penggajian", "asc");

        $result = $query->get();

        return $result;
    }    
   public function getPeriode() {
    $query = DB::table("m_profilesystem")
    ->select("bulanpayroll","tahunpayroll","bulanslip","tahunslip","tgl_absenawal","tgl_absenakhir","tgl_payrollawal","tgl_payrollakhir");
     
    $result = $query->get();
     
    return $result;
    }    
 
   
    public function updateData($request) {
         DB::table("p_penggajian")
                             ->where("id_penggajian", $request->id)

                            ->update([  "koreksiplus"=>setNoComma($request->koreksiplus),
                                        "koreksimin"=>setNoComma($request->koreksimin),
                                        "insentif"=>setNoComma($request->insentif),
                                        "tunj_khusus"=>setNoComma($request->tunj_khusus),
                                        "bonus"=>setNoComma($request->bonus),
                                        "pesangon"=>setNoComma($request->pesangon),

                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE penggajian(" . $request->id_penggajian . ") " . strtoupper($request->nama_penggajian), Auth::user()->id, $request);
    }


public function updatepph($request) 
    {

      $gaji                  = new  ProsesgajiModel(); 
      

      $tglPayroll            = $gaji->getProfile()->first();
      $tgl_payrollawal       = $tglPayroll->tgl_payrollawal;
      $tgl_payrollakhir      = $tglPayroll->tgl_payrollakhir; 
      $bulanpayroll          = $tglPayroll->bulanpayroll;
      $tahunpayroll          = $tglPayroll->tahunpayroll;
    $qData  = DB::select("SELECT * FROM  p_penggajian where bulan=".$bulanpayroll ." and tahun=".$tahunpayroll ." order by id_karyawan ");
   //dd("SELECT * FROM  p_penggajian where bulan=".$bulanpayroll ." and tahun=".$tahunpayroll ." order by id_karyawan ");

         

        foreach($qData as $row) 
        {
              DB::table("p_penggajian")
                             ->where("id_penggajian",$row->id_penggajian)
                              ->update(["tunj_pph21"=>0,
                                "pph21"=>0]);

             $ptkp = $gaji->ptkp($row->id_ptkp);

              $tahunmasuk = date('Y', strtotime($row->tgl_masuk));
              $bulanmasuk = date('m', strtotime($row->tgl_masuk));
              $jenispajak = $row->jenispajak;
              $tnj_pph21  = 0; 
             // dd($tahunmasuk);
              /*--------------------------------------------------
                                Cek Masa Kerja
              -----------------------------------------------------*/
              $masakerja  =12;
              if ($tahunmasuk == $tahunpayroll) 
              {

                $masakerja    = 13 - $bulanmasuk;
              } 

              //$pphpendapatan  = ( $jht_prs+$jp_prs+ $jkk+$jkm +$bpjs_kes_prs+ $gaji +$row->tunj_jabatan+$row->tunj_transport+$row->tunj_khusus+$row->tunj_jaga + $row->tunj_kemahalan);
              //$pphpendapatan  = ($row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_makan+$row->uang_makan+$row->insentif+$row->koreksiplus +$row->jht_prs+$row->jp_prs);
              /*---------------------------------------------------------
                               Hitung Pendapatan
              -------------------------------------------------------------*/
              $pphpendapatan  = ($row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->uang_makan+$row->koreksiplus +$row->jkk+$row->jkm+$row->bpjs_kes_prs +$row->tunj_khusus+$row->insentif+$row->bonus+$row->pesangon);
             
             /*-----------------------------------------------------------------
                                          Hitung Bijab
              ------------------------------------------------------------------*/
              $bijab          =  ($pphpendapatan * 5)/100;
              //Biaya jabayan =IF(C9*5%>500000,500000,C9*5%)
              if ($bijab > 500000)
              {
                    $bijab = 500000;

              }  

  //$pphpotongan         = ($bijab+$bpjs_kes_kry+$jht_kry+$jp_kry);
              $pphpotongan               = ($bijab+$row->jht_kry+$row->jp_kry);

              $pphpendapatantahun        = ($pphpendapatan * $masakerja);
              $pphpendapatantahunawal    = $pphpendapatan * $masakerja;
              $pphpotongantahun          = $pphpotongan * $masakerja;
              $ptkptahun                 = $gaji->Ptkp($row->id_ptkp); // Cari PTKP Tahun

              $pkp                       = $pphpendapatantahun - $pphpotongantahun - $ptkptahun;
               // if($row->nik=="2020.0619.0024")
               //         {

               //          dd( $pphpendapatan, $pphpendapatantahun,$pphpotongantahun,$ptkptahun,$pkp);
               //         }
              $pph                       = 0;
             
              // if($row->nik=="2000.1215.0005")
              // {

              //   dd("Pendapantan : " . $pphpendapatan,"Pendapantan Tahun: " .$pphpendapatantahun,"Bijab : " .$bijab,"PTKP " .$ptkptahun,"PKP " . $pkp ,"PTKP : " .$ptkptahun,$pphpendapatantahun, $pphpotongantahun , $ptkptahun );
              // }
             /*----------------------------------------------------------------------
                                         Hitung PKP
              ----------------------------------------------------------------------*/  
              if($pkp>0)
              {

                $pph=$gaji->Hitungpph21($pkp,$masakerja);
                $pphawal = $pph;
                $cekpph[0]=$pph;
                $cekpe[0]=$pphpendapatantahun;
                $cekpo[0]=$pphpotongantahun;
                $cekptkp[0]=$ptkptahun; 
                $cekpkp[0]=$pkp;


                if ($jenispajak==2)
                    {
                        $it =0;
                        Iterasi:
                        {

                              //$pphpendapatan  = ($tnj_pph21+$jht_prs+$jp_prs+ $jkk+$jkm +$bpjs_kes_prs+ $gaji +$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga + $row->tunj_kemahalan);
                              $pphpendapatan  = ($tnj_pph21+$row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_makan+$row->uang_makan+$row->insentif+$row->koreksiplus+$row->tunj_khusus+$row->jkk+$row->jkm+$row->bpjs_kes_prs+$row->bonus+$row->pesangon );
                                        
                              $bijab          =  ($pphpendapatan * 5)/100;
                              //Biaya jabayan =IF(C9*5%>500000,500000,C9*5%)
                              if ($bijab > 500000)
                              {
                                    $bijab = 500000;

                              }  

                             // $pphpotongan         = ($bijab+$bpjs_kes_kry+$jht_kry+$jp_kry);
                              $pphpotongan         = ($bijab+$row->jht_kry+$row->jp_kry);
                              $pphpendapatantahun  = $pphpendapatan * $masakerja;
                              $pphpotongantahun    = $pphpotongan * $masakerja; 
                              $pkp                 = $pphpendapatantahun - $pphpotongantahun - $ptkptahun;
                              $pph                 = 0;
                              $pph=$gaji->Hitungpph21($pkp,$masakerja); 
                              $it=$it+1;
                              $cekpph[$it]=$pph;
                              $cekpe[$it]=$pphpendapatantahun;
                              $cekpo[$it]=$pphpotongantahun;
                              $cekptkp[$it]=$ptkptahun;
                              $cekpkp[$it]=$pkp; 
                             

                        } 
                         if (($tnj_pph21 < $pph))
                        {

                                $tnj_pph21 =$pph;
                                goto Iterasi;
                        }
                    }
                   

                        DB::table("p_penggajian")
                             ->where("id_penggajian",$row->id_penggajian)
                              ->update(["tunj_pph21"=>$pph,
                                "pph21"=>$pph]);


                                 
                      


                        
                   }
             
            
            }

        
   }





    public function removeData($request) {
         DB::table("pa_penggajian")
            ->where("id_penggajian", $request->id_penggajian)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE penggajian (" . $request->id_penggajian . ") " . strtoupper($request->nama_penggajian), Auth::user()->id, $request);
    }


    public function importcsv($request){
        config(['excel.import.startRow' => 3 ]);
        $rows = Excel::load($request->file('file_excel'))->get();
        $parameter_system = $this->getPeriode()->first();
        $bulan_payroll = $parameter_system->bulanpayroll;
        foreach ($rows as $key => $value) {
            DB::table("p_penggajian")
                             ->where([["nik" ,$value['n_i_k']],["bulan",$bulan_payroll]])
                              ->update([
                                    "koreksiplus"=> $value['koreksiplus'],
                                    "koreksimin"=> $value['koreksimin'],
                                    "insentif"=> $value['insentif'],
                               ]);
        }
    
    /* ----------
         Logs
        ----------------------- */
        $qLog       = new LogModel;
        # ---------------;
        $qLog->createLog("update penggajian dari import excel", Auth::user()->id, $request);
    }


}
