<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class GroupMenuModel extends Model
{
    protected $table = "sys_group_menus";

    public function getAllMenus($group) {
        $result     = array();
        # --------------
        $query  = DB::table("sys_menus")
                            ->where("level", 1)
                            ->select("id", "name", "status")
                            ->orderBy("sys_menus.order", "ASC")->get();

        if(!empty($query)) {
            foreach($query AS $rs) {
                $exists         = DB::table("sys_group_menus")
                                            ->where("group_id", $group)
                                            ->where("menu_id", $rs->id)->get();

                $flag           = (count($exists) == 0) ? 0 : 1;
                $jump           = "F";
                # --------------
                $sub_query      = DB::table("sys_menus")
                                            ->where("level", 2)
                                            ->where("parent", $rs->id)
                                            ->select("id", "name", "status")
                                            ->orderBy("sys_menus.order", "ASC")->get();

                $unit_query     = count($sub_query);
                # --------------
                if($unit_query == 0) {
                    $sub_query      = DB::table("sys_menus")
                                                ->where("level", 3)
                                                ->where("parent", $rs->id)
                                                ->select("id", "name", "status")
                                                ->orderBy("sys_menus.order", "ASC")->get();

                    $unit_query     = count($sub_query);
                    # --------------
                    $jump           = "T";
                }
                # --------------
                array_push($result, array("id"=>$rs->id, "name"=>$rs->name, "level"=>1, "unit"=>$unit_query, "status"=>$flag, "active"=>$rs->status, "group"=>$group));
                # --------------
                if(!empty($sub_query)) {
                    foreach($sub_query AS $rs_sub) {
                        $exists         = DB::table("sys_group_menus")
                                                    ->where("group_id", $group)
                                                    ->where("menu_id", $rs_sub->id)->get();

                        $flag           = (count($exists) == 0) ? 0 : 1;
                        # --------------
                        $sub_sub_query  = DB::table("sys_menus")
                                                    ->where("level", 3)
                                                    ->where("parent", $rs_sub->id)
                                                    ->whereIn("position", ["sidebar", "list"])
                                                    ->select("id", "name", "status")
                                                    ->orderBy("sys_menus.order", "ASC")->get();

                        $unit_sub_query = count($sub_sub_query);
                        # --------------
                        if($jump == "F") {
                            array_push($result, array("id"=>$rs_sub->id, "name"=>$rs_sub->name, "level"=>2, "unit"=>$unit_sub_query, "status"=>$flag, "active"=>$rs_sub->status, "group"=>$group));
                        } else {
                            array_push($result, array("id"=>$rs_sub->id, "name"=>$rs_sub->name, "level"=>3, "unit"=>$unit_sub_query, "status"=>$flag, "active"=>$rs_sub->status, "group"=>$group));
                        }
                        # --------------
                        if(!empty($sub_sub_query)) {
                            foreach($sub_sub_query AS $rs_sub_sub) {
                                $exists         = DB::table("sys_group_menus")
                                                            ->where("group_id", $group)
                                                            ->where("menu_id", $rs_sub_sub->id)->get();

                                $flag           = (count($exists) == 0) ? 0 : 1;
                                # --------------
                                array_push($result, array("id"=>$rs_sub_sub->id, "name"=>$rs_sub_sub->name, "level"=>3, "unit"=>0, "status"=>$flag, "active"=>$rs_sub_sub->status, "group"=>$group));
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function createData($request, $arr=null) {
        $qGroup             = new GroupMenuModel;
        # ---------------
        $qGroup->menu_id    = $request->check_menu[$arr];
        $qGroup->group_id   = $request->id;
        $qGroup->timestamps = false;
        # ---------------
        $qGroup->save();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE PRIVILEGE, GROUP (" . strtoupper($request->id) . "), ID (" . $qGroup->menu_id . ")", Auth::user()->id, $request);
    }

    public function removeDataByGroup($request) {
        DB::table("sys_group_menus")->where("group_id", $request->id)->delete();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE PRIVILEGE, GROUP (" . strtoupper($request->id) . ")", Auth::user()->id, $request);
    }
}
