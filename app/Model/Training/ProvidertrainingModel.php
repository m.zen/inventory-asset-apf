<?php

namespace App\Model\Training;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class ProvidertrainingModel extends Model
{
    protected $table    = "t_providertraining";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("t_providertraining")
                            ->select("*")
                            ->orderBy("id_providertraining", "DESC");

        if(session()->has("SES_SEARCH_PROVIDERTRAINING")) {
            $query->where("nama_providertraining", "LIKE", "%" . session()->get("SES_SEARCH_PROVIDERTRAINING") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("t_providertraining")
                            ->select("*")
                            ->where("id_providertraining", $id)
                            ->orderBy("nama_providertraining", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qprovidertraining              = new ProvidertrainingModel;
        # ---------------
       // $qprovidertraining->kode       = setString($request->kode);
        //$qprovidertraining->kategori_providertraining  = setString($request->kategori_providertraining);
        $qprovidertraining->nama_providertraining      = setString($request->nama_providertraining);
        $qprovidertraining->alamat_providertraining    = setString($request->alamat_providertraining);
        $qprovidertraining->telp_providertraining      = setString($request->telp_providertraining);
        $qprovidertraining->fax_providertraining       = setString($request->fax_providertraining);
        $qprovidertraining->email_providertraining     = setString($request->email_providertraining);
       
        $qprovidertraining->user_id                  = setString(Auth::user()->id);
        $qprovidertraining->create_at                = setString(date('Y-m-d H:i:s'));
        $qprovidertraining->update_at                = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qprovidertraining->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE PROVIDERTRAINING (" . $qprovidertraining->id_providertraining . ") " . strtoupper($request->nama_providertraining), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("t_providertraining")
                             ->where("id_providertraining", $request->id_providertraining)
                            ->update([ "nama_providertraining"=>$request->nama_providertraining,
                            			"alamat_providertraining"=>$request->alamat_providertraining,
                            			"telp_providertraining"=>$request->telp_providertraining,
                            			"fax_providertraining"=>$request->fax_providertraining,
                            			"email_providertraining"=>$request->email_providertraining,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE PROVIDERTRAINING(" . $request->id_providertraining . ") " . strtoupper($request->nama_providertraining), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("t_providertraining")
                             ->where("id_providertraining", $request->id_providertraining)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE PROVIDERTRAINING (" . $request->id_providertraining . ") " . strtoupper($request->nama_providertraining), Auth::user()->id, $request);
    }
}
