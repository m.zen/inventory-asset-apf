<?php

namespace App\Model\Training;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class MastertrainingModel extends Model
{
    protected $table    = "t_mastertraining";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("t_mastertraining")
                            ->select("id_mastertraining", "nama_mastertraining","kategori_mastertraining")
                            ->orderBy("id_mastertraining", "DESC");

        if(session()->has("SES_SEARCH_MASTERTRAINING")) {
            $query->where("nama_mastertraining", "LIKE", "%" . session()->get("SES_SEARCH_MASTERTRAINING") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("t_mastertraining")
                            ->select("id_mastertraining", "nama_mastertraining","kategori_mastertraining")
                            ->where("id_mastertraining", $id)
                            ->orderBy("nama_mastertraining", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qmastertraining              = new MastertrainingModel;
        # ---------------
       // $qmastertraining->kode       = setString($request->kode);
        //$qmastertraining->kategori_mastertraining  = setString($request->kategori_mastertraining);
        $qmastertraining->nama_mastertraining      = setString($request->nama_mastertraining);
        $qmastertraining->user_id                  = setString(Auth::user()->id);
        $qmastertraining->create_at                = setString(date('Y-m-d H:i:s'));
        $qmastertraining->update_at                = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qmastertraining->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE MASTERTRAINING (" . $qmastertraining->id_mastertraining . ") " . strtoupper($request->nama_mastertraining), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("t_mastertraining")
                             ->where("id_mastertraining", $request->id_mastertraining)
                            ->update([ "nama_mastertraining"=>$request->nama_mastertraining,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE MASTERTRAINING(" . $request->id_mastertraining . ") " . strtoupper($request->nama_mastertraining), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("t_mastertraining")
                             ->where("id_mastertraining", $request->id_mastertraining)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE MASTERTRAINING (" . $request->id_mastertraining . ") " . strtoupper($request->nama_mastertraining), Auth::user()->id, $request);
    }
}
