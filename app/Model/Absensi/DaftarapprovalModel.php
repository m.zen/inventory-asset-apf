<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class DaftarapprovalModel extends Model
{
    protected $table    = "tm_dataabsensi";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $group_id = Auth::user()->group_id;
        // $result = [];        
        $data  = [];
        $lastresult = [];
        // $group_id = 4;
        if($group_id != 1){
        $p_karyawan = $this->getDataApprovalLevel(Auth::user()->nik);
	
	    $wherecondition =[
                            ['d.id_cabang_approval',$p_karyawan->id_cabang],
                            ['d.id_departemen_approval',$p_karyawan->id_departemen],
                            ['d.id_approvaller',$p_karyawan->id_jabatan],  
                            ['d.id_approvaller',$p_karyawan->id_jabatan],  
            ];

        $query=  DB::table('tm_pengajuanabsen as a')
        ->select(
        'd.approvallevel','b.tgl_awal','b.tgl_akhir','a.statusapproval','a.id_pengajuanabsen','c.nik','e.nama_jenisabsen',
           'c.id_cabang as id_c','c.id_departemen as id_d','c.id_jabatan as id_j','c.nama_karyawan','d.id_cabang','d.id_departemen','d.id_jabatan','d.id_cabang_approval','d.id_departemen_approval','d.id_approvaller'
           ,'b.id_karyawan','b.id_tm','d.approvallevel'
           ,'i.nama_cabang','j.nama_departemen','k.nama_jabatan'
           )
        ->leftjoin('tm_timeoff as b','b.id_tm','a.id_tm')
        ->leftjoin('p_karyawan as c','c.id_karyawan','b.id_karyawan')
        ->leftjoin('tm_dataapproval as d','d.id_jabatan','c.id_jabatan')
        ->leftjoin("tm_jenisabsen as e","e.id_jenisabsen","=","b.id_status_karyawan")
        ->leftjoin("m_cabang as f","f.id_cabang","=","c.id_cabang")
        ->leftjoin("m_departemen as g","g.id_departemen","=","c.id_departemen")
        ->leftjoin("m_jabatan as h","h.id_jabatan","=","c.id_jabatan")
        ->leftjoin("m_cabang as i","i.id_cabang","=","d.id_cabang_approval")
        ->leftjoin("m_departemen as j","j.id_departemen","=","d.id_departemen_approval")
        ->leftjoin("m_jabatan as k","k.id_jabatan","=","d.id_approvaller")
        ->where($wherecondition)
        // ->where("a.statusapproval",  null)
        // ->where('d.id_departemen_approval',$p_karyawan->id_departemen)
        // ->where('d.id_approvaller',$p_karyawan->id_jabatan)
        //->where('a.order',$p_karyawan->approvallevel)
        ->orderBy("c.nama_karyawan", "ASC")
        ->orderBy("b.tgl_awal", "DESC")
        ->orderBy("a.id_pengajuanabsen", "ASC");
        
        // if(session()->has("SES_SEARCH_DATAABSENSI")) {
        //     $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%");
        // }

        if(session()->has("SES_SEARCH_PENGAJUAN_FROMDATE") && session()->has("SES_SEARCH_PENGAJUAN_TODATE") && session()->has("SES_SEARCH_STATUS") && session()->has("SES_SEARCH_PENGAJUAN")) {
            if(session()->get("SES_SEARCH_STATUS") == '3'){
                $query->where('c.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%")
                ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
            }else if(session()->get("SES_SEARCH_STATUS") == '2'){
                $query->where('c.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%")
                ->where('a.statusapproval', null)
                ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
            }else{
                $query->where('c.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%")
                ->where('a.statusapproval', "LIKE", "%" . session()->get("SES_SEARCH_STATUS") . "%")
                ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
            }
            
        }
        else if(session()->has("SES_SEARCH_PENGAJUAN_FROMDATE") && session()->has("SES_SEARCH_PENGAJUAN_TODATE") && session()->has("SES_SEARCH_STATUS")){
            if(session()->get("SES_SEARCH_STATUS") == '3'){
                $query
                ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
            }else{  
                if(session()->get("SES_SEARCH_STATUS") == '2'){
                    $query->where("a.statusapproval", null)
                   ->whereBetween("b.tgl_awal", array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                }else{
                    $query->where("a.statusapproval", session()->get("SES_SEARCH_STATUS"))
                    ->whereBetween("b.tgl_awal", array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                }
                
            }
        }else 
        if(session()->has("SES_SEARCH_PENGAJUAN")) {
            $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%");
        }
        
        $result = $query->get();
        }else{

            $wherecondition = [
                []
            ];

            $query  = DB::table("tm_pengajuanabsen as a")
                    ->select(
                    'd.approvallevel','b.tgl_awal','b.tgl_akhir','a.statusapproval','a.id_pengajuanabsen','c.nik','e.nama_jenisabsen',
                        'c.id_cabang as id_c','c.id_departemen as id_d','c.id_jabatan as id_j','c.nama_karyawan','d.id_cabang','d.id_departemen','d.id_jabatan','d.id_cabang_approval','d.id_departemen_approval','d.id_approvaller'
                        ,'b.id_karyawan','b.id_tm','d.approvallevel'
                        ,'i.nama_cabang','j.nama_departemen','k.nama_jabatan'
                        )
                    ->leftjoin('tm_timeoff as b','b.id_tm','a.id_tm')
                    ->leftjoin('p_karyawan as c','c.id_karyawan','b.id_karyawan')
                    ->leftjoin('tm_dataapproval as d','d.id_jabatan','c.id_jabatan')
                    ->leftjoin("tm_jenisabsen as e","e.id_jenisabsen","=","b.id_status_karyawan")
                    ->leftjoin("m_cabang as f","f.id_cabang","=","c.id_cabang")
                    ->leftjoin("m_departemen as g","g.id_departemen","=","c.id_departemen")
                    ->leftjoin("m_jabatan as h","h.id_jabatan","=","c.id_jabatan")
                    ->leftjoin("m_cabang as i","i.id_cabang","=","d.id_cabang_approval")
                    ->leftjoin("m_departemen as j","j.id_departemen","=","d.id_departemen_approval")
                    ->leftjoin("m_jabatan as k","k.id_jabatan","=","d.id_approvaller")
                        // ->where("b.status",null)
                        // ->where($wherecondition)
                        ->orderBy("c.nama_karyawan", "ASC")
                        ->orderBy("b.tgl_awal", "DESC")
                        ;
            if(session()->has("SES_SEARCH_PENGAJUAN_FROMDATE") && session()->has("SES_SEARCH_PENGAJUAN_TODATE") && session()->has("SES_SEARCH_STATUS") && session()->has("SES_SEARCH_PENGAJUAN")) {
                if(session()->get("SES_SEARCH_STATUS") == '3'){
                    $query->where('c.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%")
                    ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                }else if(session()->get("SES_SEARCH_STATUS") == '2'){
                    $query->where('c.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%")
                    ->where('a.statusapproval', null)
                    ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                }else{
                    $query->where('c.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%")
                    ->where('a.statusapproval', "LIKE", "%" . session()->get("SES_SEARCH_STATUS") . "%")
                    ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                }
                
            }
            else if(session()->has("SES_SEARCH_PENGAJUAN_FROMDATE") && session()->has("SES_SEARCH_PENGAJUAN_TODATE") && session()->has("SES_SEARCH_STATUS")){
                if(session()->get("SES_SEARCH_STATUS") == '3'){
                    $query
                    ->whereBetween('b.tgl_awal', array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                }else{  
                    if(session()->get("SES_SEARCH_STATUS") == '2'){
                        $query->where("a.statusapproval", null)
                        ->whereBetween("b.tgl_awal", array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                    }else{
                        $query->where("a.statusapproval", session()->get("SES_SEARCH_STATUS"))
                        ->whereBetween("b.tgl_awal", array(session()->get("SES_SEARCH_PENGAJUAN_FROMDATE"), session()->get("SES_SEARCH_PENGAJUAN_TODATE")));
                    }
                    
                }
            }else 
            if(session()->has("SES_SEARCH_PENGAJUAN")) {
                $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PENGAJUAN") . "%");
            }

            $result = $query->get();
        }
        
        if(!empty($result)){
            foreach($result as $key3 => $valuefilter){
                $c= DB::table('tm_dataapproval')
                    ->where([
                        ['id_cabang', $valuefilter->id_c],
                        ['id_departemen', $valuefilter->id_d],
                        ['id_jabatan', $valuefilter->id_j],
                        ['id_cabang_approval', $valuefilter->id_cabang_approval],
                        ['id_departemen_approval', $valuefilter->id_departemen_approval],
                        ['id_approvaller', $valuefilter->id_approvaller],
                        ])
                    ->first();
                if($c != null){
                    $data[$valuefilter->id_tm] = $valuefilter; 
                }
            }
            $lastresult = array_slice($data, $offset, $limit);
        }
        return $lastresult;
    }

    public function getList4($request=null, $offset=null, $limit=null) {
        $group_id = Auth::user()->group_id;
        if($group_id != 1){
        $p_karyawan = $this->getDataApprovalLevel(Auth::user()->nik);
	
	    $wherecondition =[
                            ['d.id_cabang_approval',$p_karyawan->id_cabang],
                            ['d.id_departemen_approval',$p_karyawan->id_departemen],
                            ['d.id_approvaller',$p_karyawan->id_jabatan],  
                            ['d.id_approvaller',$p_karyawan->id_jabatan],  
            ];

        $query=  DB::table('tm_pengajuanabsen as a')
        ->select(
        'd.approvallevel','b.tgl_awal','b.tgl_akhir','a.statusapproval','a.id_pengajuanabsen','c.nik','e.nama_jenisabsen',
           'c.id_cabang as id_c','c.id_departemen as id_d','c.id_jabatan as id_j','c.nama_karyawan','d.id_cabang','d.id_departemen','d.id_jabatan','d.id_cabang_approval','d.id_departemen_approval','d.id_approvaller'
           ,'b.id_karyawan','b.id_tm','d.approvallevel'
           ,'i.nama_cabang','j.nama_departemen','k.nama_jabatan'
           )
        ->leftjoin('tm_timeoff as b','b.id_tm','a.id_tm')
        ->leftjoin('p_karyawan as c','c.id_karyawan','b.id_karyawan')
        ->leftjoin('tm_dataapproval as d','d.id_jabatan','c.id_jabatan')
        ->leftjoin("tm_jenisabsen as e","e.id_jenisabsen","=","b.id_status_karyawan")
        ->leftjoin("m_cabang as f","f.id_cabang","=","c.id_cabang")
        ->leftjoin("m_departemen as g","g.id_departemen","=","c.id_departemen")
        ->leftjoin("m_jabatan as h","h.id_jabatan","=","c.id_jabatan")
        ->leftjoin("m_cabang as i","i.id_cabang","=","d.id_cabang_approval")
        ->leftjoin("m_departemen as j","j.id_departemen","=","d.id_departemen_approval")
        ->leftjoin("m_jabatan as k","k.id_jabatan","=","d.id_approvaller")
        ->where($wherecondition)
        // ->where("a.statusapproval",  null)
        // ->where('d.id_departemen_approval',$p_karyawan->id_departemen)
        // ->where('d.id_approvaller',$p_karyawan->id_jabatan)
        //->where('a.order',$p_karyawan->approvallevel)
        ->orderBy("c.nama_karyawan", "ASC")
        ->orderBy("b.tgl_awal", "DESC")
        ->orderBy("a.id_pengajuanabsen", "ASC");
        
        if(session()->has("SES_SEARCH_DATAABSENSI")) {
            $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%");
        }
        // $result = [];        
        $data  = [];
        $lastresult = [];
        $result = $query->get();
        }else{

            $wherecondition = [
                []
            ];

            $query  = DB::table("tm_pengajuanabsen as a")
                    ->select(
                    'd.approvallevel','b.tgl_awal','b.tgl_akhir','a.statusapproval','a.id_pengajuanabsen','c.nik','e.nama_jenisabsen',
                        'c.id_cabang as id_c','c.id_departemen as id_d','c.id_jabatan as id_j','c.nama_karyawan','d.id_cabang','d.id_departemen','d.id_jabatan','d.id_cabang_approval','d.id_departemen_approval','d.id_approvaller'
                        ,'b.id_karyawan','b.id_tm','d.approvallevel'
                        ,'i.nama_cabang','j.nama_departemen','k.nama_jabatan'
                        )
                    ->leftjoin('tm_timeoff as b','b.id_tm','a.id_tm')
                    ->leftjoin('p_karyawan as c','c.id_karyawan','b.id_karyawan')
                    ->leftjoin('tm_dataapproval as d','d.id_jabatan','c.id_jabatan')
                    ->leftjoin("tm_jenisabsen as e","e.id_jenisabsen","=","b.id_status_karyawan")
                    ->leftjoin("m_cabang as f","f.id_cabang","=","c.id_cabang")
                    ->leftjoin("m_departemen as g","g.id_departemen","=","c.id_departemen")
                    ->leftjoin("m_jabatan as h","h.id_jabatan","=","c.id_jabatan")
                    ->leftjoin("m_cabang as i","i.id_cabang","=","d.id_cabang_approval")
                    ->leftjoin("m_departemen as j","j.id_departemen","=","d.id_departemen_approval")
                    ->leftjoin("m_jabatan as k","k.id_jabatan","=","d.id_approvaller")
                        // ->where("b.status",null)
                        // ->where($wherecondition)
                        ->orderBy("c.nama_karyawan", "ASC")
                        ->orderBy("b.tgl_awal", "DESC")
                        ;
            if(session()->has("SES_SEARCH_DATAABSENSI")) {
            $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%");
            }

            $result = $query->get();
        }
        // dd($result);
        // foreach($result as $key3 => $value3){
        //     $rtt[$value3->id_tm] = $value3;
        // }
	
        if(!empty($result)){
            foreach($result as $key3 => $valuefilter){
                $c= DB::table('tm_dataapproval')
                    ->where([
                        ['id_cabang', $valuefilter->id_c],
                        ['id_departemen', $valuefilter->id_d],
                        ['id_jabatan', $valuefilter->id_j],
                        ['id_cabang_approval', $valuefilter->id_cabang_approval],
                        ['id_departemen_approval', $valuefilter->id_departemen_approval],
                        ['id_approvaller', $valuefilter->id_approvaller],
                        ])
                    ->first();
                if($c != null){
                    $data[$valuefilter->id_tm] = $valuefilter; 
                }
            }
            
            $lastresult = array_slice($data, $offset, $limit);
        }
            return $lastresult;
    }

    public function getListPending($request=null, $offset=null, $limit=null) {
        $p_karyawan = $this->getDataApprovalLevel(Auth::user()->nik);
	
	$wherecondition =[
                            ['d.id_cabang_approval',$p_karyawan->id_cabang],
                            ['d.id_departemen_approval',$p_karyawan->id_departemen],
                            ['d.id_approvaller',$p_karyawan->id_jabatan],  
                            ['d.id_approvaller',$p_karyawan->id_jabatan],  
            ];

        $query=  DB::table('tm_pengajuanabsen as a')
       ->select(
        'd.approvallevel','b.tgl_awal','b.tgl_akhir','a.statusapproval','a.id_pengajuanabsen','c.nik','e.nama_jenisabsen',
           'c.id_cabang as id_c','c.id_departemen as id_d','c.id_jabatan as id_j','c.nama_karyawan','d.id_cabang','d.id_departemen','d.id_jabatan','d.id_cabang_approval','d.id_departemen_approval','d.id_approvaller'
           ,'b.id_karyawan','b.id_tm','d.approvallevel'
           ,'i.nama_cabang','j.nama_departemen','k.nama_jabatan'
           )
        ->leftjoin('tm_timeoff as b','b.id_tm','a.id_tm')
        ->leftjoin('p_karyawan as c','c.id_karyawan','b.id_karyawan')
        ->leftjoin('tm_dataapproval as d','d.id_jabatan','c.id_jabatan')
        ->leftjoin("tm_jenisabsen as e","e.id_jenisabsen","=","b.id_status_karyawan")
        ->leftjoin("m_cabang as f","f.id_cabang","=","c.id_cabang")
        ->leftjoin("m_departemen as g","g.id_departemen","=","c.id_departemen")
        ->leftjoin("m_jabatan as h","h.id_jabatan","=","c.id_jabatan")
        ->leftjoin("m_cabang as i","i.id_cabang","=","d.id_cabang_approval")
        ->leftjoin("m_departemen as j","j.id_departemen","=","d.id_departemen_approval")
        ->leftjoin("m_jabatan as k","k.id_jabatan","=","d.id_approvaller")
        ->where($wherecondition)
        ->where("a.statusapproval",  null)
        // ->where('d.id_departemen_approval',$p_karyawan->id_departemen)
        // ->where('d.id_approvaller',$p_karyawan->id_jabatan)
        //->where('a.order',$p_karyawan->approvallevel)
        ->orderBy("c.nama_karyawan", "ASC")
        ->orderBy("b.tgl_awal", "DESC")
        ->orderBy("a.id_pengajuanabsen", "ASC");
        
        if(session()->has("SES_SEARCH_DATAABSENSI")) {
            $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%");
        }
        $result = [];        
        $data  = [];
        $lastresult = [];
        $result = $query->get();
        // foreach($result as $key3 => $value3){
        //     $rtt[$value3->id_tm] = $value3;
        // }
	
        if(!empty($result)){
            
            foreach($result as $key3 => $valuefilter){
                $c= DB::table('tm_dataapproval')
                    ->where([
                        ['id_cabang', $valuefilter->id_c],
                        ['id_departemen', $valuefilter->id_d],
                        ['id_jabatan', $valuefilter->id_j],
                        ['id_cabang_approval', $valuefilter->id_cabang_approval],
                        ['id_departemen_approval', $valuefilter->id_departemen_approval],
                        ['id_approvaller', $valuefilter->id_approvaller],
                        ])
                    ->first();
                if($c != null){
                    $data[$valuefilter->id_tm] = $valuefilter; 
                }
            }
            $lastresult = array_slice($data, $offset, $limit);
        }
            return $lastresult;
    }

    public function getDataApprovalLevel($nik){
        return DB::table("p_karyawan as a")
        //->select('b.approvallevel','b.id_approvaller','b.approvallevel', 'a.id_cabang', 'a.id_departemen', 'a.id_jabatan','a.id_karyawan')
        //->leftjoin('tm_dataapproval as b','b.id_approvaller', 'a.id_jabatan')
        ->where([['a.nik', $nik],['a.aktif', 1]])->first();
    }
	
    public function getProfileApproval($id_tm, $approval_level) {
        $query  = DB::table("tm_pengajuanabsen as a")
                            ->select("a.*","c.nik","c.nama_karyawan","d.*","b.*", "c.sisacuti")
                            ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
                            ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
                            ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
                            ->where([["a.id_tm", $id_tm],["a.order", $approval_level]]);
                            // ->orderBy("nama_karyawan", "DESC");
        $result = $query->get();
        return $result;
    }
    
    public function getList2($request=null, $offset=null, $limit=null) {
    	$p_karyawan = DB::table("p_karyawan as a")->where('nik', Auth::user()->nik)->first();
        $query  = DB::table("tm_pengajuanabsen as a")
                            ->select("a.*","b.id_status_karyawan","b.id_karyawan","b.tgl_awal","b.tgl_akhir","c.nik","c.nama_karyawan","d.nama_jenisabsen")
                            ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
                            ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
                            ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","c.id_jabatan")
                            ->where("a.id_karyawan", $p_karyawan->id_karyawan)
                            ->orderBy("b.tgl_awal", "DESC");;
        if(session()->has("SES_SEARCH_DATAABSENSI")) {
            $query->where("c.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();
      //   if(count($result)>0){
		    // foreach ($result as $key => $value) {
	     //    	# code...
	     //    	if($value->id_pengajuanabsen != null || $value->id_pengajuanabsen != ''){
	     //    		$resultt[$key] = $value;
	     //    	}
	        	
	     //    }
	     //    // dd($resultt);
	     //    return $resultt;
      //   }else{
	        return $result;
        // }
    }

    public function getProfile($id) {
        $query  = DB::table("tm_pengajuanabsen as a")
                            ->select("a.*","c.nik","c.nama_karyawan","d.*","b.*","c.sisacuti")
                            ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
                            ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
                            ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
                            ->where("id_pengajuanabsen", $id);
                            // ->orderBy("nama_karyawan", "DESC");

        $result = $query->get();
        return $result;
    }

    public function getProfileTm($id_tm) {
        $query  = DB::table("tm_pengajuanabsen as a")
                            ->select("a.*","c.nik","c.nama_karyawan","d.*","b.*")
                            ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
                            ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
                            ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
                            ->where([["a.id_tm", $id_tm],["a.order", 1]]);

        $result = $query->get();
        return $result;
    }

    public function getData($nik) {
        $query  = DB::table("tm_pengajuanabsen as a")
                            ->select("a.*","c.nik","c.nama_karyawan","d.*","b.*")
                            ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
                            ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
                            ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
                            ->where("c.nik", $nik);

        $result = $query->get();
        return $result;
    }

    public function updateData($request) {
            if($request->statusapproval == 'APPROVE'){
    		$status = 1;
    		$ket = 'Approve';
    	}else if($request->statusapproval == 'RETURN'){
    		$status = 2;
    		$ket = 'Return';
    	}else{
    		$status = '0';
    		$ket = 'Reject';
    	}
	
    	DB::table("tm_pengajuanabsen")
                             ->where("id_pengajuanabsen", $request->id_pengajuanabsen)
                            ->update([ "statusapproval"=>$status,
                            	    	"keterangan"=>$request->komentar,
                            	         "tgl_approved"=>setString(date('Y-m-d H:i:s')),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
        $qjenisabsen =	DB::table("tm_jenisabsen")->select('inisial','nama_jenisabsen')->where('nama_jenisabsen',$request->id_status_karyawan)->first();
    	$cekpengajuan = DB::table('tm_pengajuanabsen')->select('id_tm')->where('id_tm',$request->id_tm)->get();
    	$jenisabsen = $qjenisabsen->inisial;
    	   $inisialabsen = array('DL','S','I','CT','IDC','IK','ST','H','IT','KR');
    	   $potong_gaji = array(0,0,0,0,1,0,0,0,0,0);
    	   $potong_makan = array(0,0,0,0,1,0,0,0,0,0);

        	for ($i=0; $i < count($inisialabsen) ; $i++) { 
        		# code...
        		if($jenisabsen == $inisialabsen[$i]){
        			$status_potong_makan = $potong_makan[$i];
        			$status_potong_gaji = $potong_gaji[$i];
        		}
            }
        
        if($request->order == count($cekpengajuan)){
            if($status == 1){
            $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
        		foreach ($rangedate as $key => $value) {
                    # code...
                    $cek_tm_dataabsensi = DB::table("tm_dataabsensi")
                                 ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])->first();
                    if($cek_tm_dataabsensi != null)
                    {
        			DB::table("tm_dataabsensi")
                                 ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])
                                ->update([ 
                                			"status_absensi"=>$qjenisabsen->inisial,
    										"status_potong_makan"=>$status_potong_makan,
    										"status_potong_gaji"=>$status_potong_gaji,
                                        ]);
                        // if($qjenisabsen->inisial == 'CT'){
                        //     dd();
                        // $p_karyawan = DB::table('p_karyawan')->where('id_karyawan', $request->id_karyawan)->first();
                        // $tgl_awalcuti =date("Y-m-d H:i:s", strtotime("+1 years", strtotime(setYMD($request->tgl_masuk, "/"))));
                        // $tgl_akhircuti =date("Y-m-d H:i:s", strtotime("+2 years", strtotime(setYMD($request->tgl_masuk, "/"))));
                        // DB::table('p_karyawan')->increment('sisacuti', 1, ['id_karyawan' => ]);
                        // }
                        
                    }
        		}
            }else if($status == 2){
                $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
        		foreach ($rangedate as $key => $value) {
                    # code...
                    $cek_tm_dataabsensi = DB::table("tm_dataabsensi")
                                 ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])->first();
                    if($cek_tm_dataabsensi != null)
                    {
        			DB::table("tm_dataabsensi")
                                 ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])
                                ->update([ 
                                			"status_absensi"=>"Waiting",
    										"status_potong_makan"=>1,
    										"status_potong_gaji"=>0,
                                        ]);
                    }
        		}     
            }else{
                $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
        		foreach ($rangedate as $key => $value) {
                    # code...
                    $cek_tm_dataabsensi = DB::table("tm_dataabsensi")
                                 ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])->first();
                    if($cek_tm_dataabsensi != null)
                    {
        			DB::table("tm_dataabsensi")
                                 ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])
                                ->update([ 
                                			"status_absensi"=>"A",
    										"status_potong_makan"=>1,
    										"status_potong_gaji"=>0,
                                        ]);
                    }
        		}     
            }
    		DB::table("tm_timeoff")
                             ->where([["id_tm", $request->id_tm]])
                            ->update([ 
					"status"=>$ket,
					"komentarapp"=>$request->komentarapp,
                            	      ]);
    	}else{
    		$rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
    		foreach ($rangedate as $key => $value) {
    			# code...
    			DB::table("tm_dataabsensi")
                             ->where([["id_karyawan", $request->id_karyawan],["tgl_absensi", $value]])
                            ->update([
                            			"status_absensi"=>'Waiting',
                            	    	"status_potong_makan"=>1,
										"status_potong_gaji"=>0,
									]);
    		}
    		
	    	if(count($cekpengajuan)>1){
	    		$statustimeoff = 'Pending';
	    	}else{
	    		$statustimeoff = $ket;
	    	}
	    		DB::table("tm_timeoff")
                             ->where([["id_tm", $request->id_tm]])
                            ->update([ 
					"status"=>$statustimeoff,
					"komentarapp"=>$request->komentarapp,
                            	      ]);
    	}
         
        return array("status"=>'success');
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE DATAABSENSI(" . $request->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
    }

    public function getDatesFromRange2($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
	}

   public function getdatapending2(){
        // dd(
        $profilesystem = DB::table('m_profilesystem')->first();
        return 
        DB::table("tm_pengajuanabsen as a")
        // ->select("a.*","b.id_status_karyawan","b.id_karyawan","b.tgl_awal","b.tgl_akhir","c.nik","c.id_cabang","c.id_departemen","c.id_jabatan",
        // "f.nama_cabang","g.nama_departemen","c.nama_karyawan","d.nama_jenisabsen","d.inisial",
        // "a.id_karyawan as id_karyawan_approval","b.komentar"
        // ,"i.id_approvaller"
        // )
        ->select(
            // "a.id_pengajuanabsen",
            "a.id_tm",
            "a.order",
            "a.statusapproval",
            "b.tgl_awal",
            "b.tgl_akhir",
            "b.id_karyawan",
            "c.nik",
            "c.nama_karyawan",
            "c.id_cabang",
            "c.id_departemen",
            "c.id_jabatan",
            "d.nama_jenisabsen"
            // "i.id_approvaller"
            )
        ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
        ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
        ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
        ->leftjoin("tm_dataapproval as i","i.id_jabatan","=","c.id_jabatan")
        ->where("b.status",null)
        ->where("b.id_karyawan","!=",null)
        ->where("b.tgl_awal", ">=", $profilesystem->tgl_absenawal)
        ->groupBy(
            // "a.id_pengajuanabsen",
            "a.id_tm",
            "a.order",
            "a.statusapproval",
            "b.tgl_awal",
            "b.tgl_akhir",
            "b.id_karyawan",
            "c.nik",
            "c.nama_karyawan",
            "c.id_cabang",
            "c.id_departemen",
            "c.id_jabatan",
            "d.nama_jenisabsen"
            // "i.id_approvaller"
            )
        ->orderBy("c.nama_karyawan", "ASC")
        ->orderBy("b.tgl_awal", "DESC")
        ->get()
        ;
    }
}
