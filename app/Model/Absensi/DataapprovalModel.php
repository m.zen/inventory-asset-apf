<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\MasterModel;

class DataapprovalModel extends Model
{
    protected $table    = "tm_dataapproval";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("tm_dataapproval as a")
                            ->select("a.id_dataapproval","b.nama_jabatan","c.nama_jabatan as approved","a.approvallevel", "d.nama_cabang","e.nama_cabang as nama_cabangb", "f.nama_departemen","g.nama_departemen as nama_departemenb","d.id_cabang")
                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_cabang as d","d.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_cabang as e","e.id_cabang","=","a.id_cabang_approval")
                            ->leftjoin("m_departemen as f","f.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_departemen as g","g.id_departemen","=","a.id_departemen_approval")
                            ->leftjoin("m_jabatan as c","c.id_jabatan","=","a.id_approvaller")
                            ->orderBy("id_cabang", "DESC")
                            ->orderBy("id_dataapproval", "DESC");

        if(session()->has("SES_SEARCH_CABANG_USER")) {
            if(session()->get("SES_SEARCH_CABANG_USER") != '-'){
                $query->where("d.id_cabang", session()->get("SES_SEARCH_CABANG_USER"));
            }
            
        }
        if(session()->has("SES_SEARCH_DEPARTEMEN_USER")) {
          
            if(session()->get("SES_SEARCH_DEPARTEMEN_USER") != '-'){
                $query->where("f.id_departemen", session()->get("SES_SEARCH_DEPARTEMEN_USER"));
            }
            
        }
        if(session()->has("SES_SEARCH_JABATAN_USER")) {
            if(session()->get("SES_SEARCH_JABATAN_USER") != '-'){
                $query->where("b.id_jabatan", session()->get("SES_SEARCH_JABATAN_USER"));
            }
            
        }

        if(session()->has("SES_SEARCH_DATAAPPROVAL")) {
		    $query->where("b.nama_jabatan", "LIKE", "%" . session()->get("SES_SEARCH_DATAAPPROVAL") . "%")
            ->orwhere("d.nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_DATAAPPROVAL") . "%")
            ->orwhere("f.nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_DATAAPPROVAL") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();
        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("tm_dataapproval")
                            ->select("id_cabang","id_cabang_approval","id_departemen","id_departemen_approval","id_dataapproval", "id_jabatan","id_approvaller","approvallevel")
                            ->where("id_dataapproval", $id)
                            ->orderBy("id_dataapproval", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qMaster               = new MasterModel;
        # ---------------
       // $qdataapproval->kode       = setString($request->kode);
    //    dd($request->id_departemen == "all" && $request->id_jabatan == "all");
        if($request->id_departemen == "all" && $request->id_jabatan == "all"){
            $qDepartemen                 = $qMaster->getSelectDepartemenById($request->id_cabang);
            //hilangkan duplicate data departemen
            foreach($qDepartemen as $key => $value){
                $qDepartemenn[$value->id] = $value;
            }
            $qDepartemen = $qDepartemenn;
            $i = 0;
            foreach($qDepartemen as $key2 => $value2){
                $qJabatan                 = $qMaster->getSelectJabatanById($request->id_cabang, $value2->id);
                //hilangkan duplicate data jabatan
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $value2->id,
                                                'id_jabatan' => $value4->id
                                                ]
                                                ;
                }
            }
            $qJabatan = $collection;
            $kode = 1;

       }else if($request->id_departemen == "all" && $request->id_jabatan != "all"){
        // $qJabatan                 = $qMaster->getSelectJabatanByIdCabangAndAllJabatan($request->id_cabang,$request->id_jabatan);
        $qDepartemen                 = $qMaster->getSelectDepartemenById($request->id_cabang);
            //hilangkan duplicate data departemen
            foreach($qDepartemen as $key => $value){
                $qDepartemenn[$value->id] = $value;
            }
            $qDepartemen = $qDepartemenn;
            $i = 0;
            foreach($qDepartemen as $key2 => $value2){
                $qJabatan                 = $qMaster->getSelectJabatanById($request->id_cabang, $value2->id);
                //hilangkan duplicate data jabatan
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    if($request->id_jabatan == $value4->id){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $value2->id,
                                                'id_jabatan' => $value4->id
                                                ]
                                                ;
                    }
                }
            }
            $qJabatan = $collection;
            $kode = 2;
       }else if($request->id_departemen != "all" && $request->id_jabatan == "all"){
                $qJabatan                 = $qMaster->getSelectJabatanById($request->id_cabang, $request->id_departemen);
                //hilangkan duplicate data jabatan
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $request->id_departemen,
                                                'id_jabatan' => $value4->id
                                                ]
                                                ;
                }
            $qJabatan = $collection;
            $kode = 3;
       }else{
        $qJabatan                 = $qMaster->getSelectJabatanByIdCabangAndByIdDepartemenByIdJabatan($request->id_cabang,$request->id_departemen,$request->id_jabatan);
            //hilangkan duplicate data jabatan
            foreach($qJabatan as $key3 => $value3){
                $qJabatann[$value3->id_jabatan] = $value3;
            }
            $qJabatan = $qJabatann;
            $qJabatann = null;
            foreach($qJabatan as $key4 => $value4){
                $collection[]            = (object)
                                            [
                                            'id_cabang' =>$request->id_cabang,
                                            'id_departemen' => $request->id_departemen,
                                            'id_jabatan' => $value4->id_jabatan
                                            ]
                                            ;
            }
            $qJabatan = $collection;
            $kode = 4;
        }
        
        // // hilangkan duplicate data jabatan
        // foreach($qJabatan as $key3 => $value3){
        //     $qJabatann[$value3->id_jabatan] = $value3;
        // }
        // $qJabatan = $qJabatann;
       foreach($qJabatan as $index => $value){
        $qdataapproval              = new DataapprovalModel;
       $qdataapproval->id_cabang       = $value->id_cabang;
       $qdataapproval->id_departemen   = $value->id_departemen;
       $qdataapproval->id_jabatan       = $value->id_jabatan;
       $qdataapproval->id_cabang_approval       = $request->id_cabang_approvaller;
       $qdataapproval->id_departemen_approval   = $request->id_departemen_approvaller;
       // $qdataapproval->id_jabatan_approval      = setString($request->id_jabatan_approvaller);
       $qdataapproval->id_approvaller   = $request->id_approvaller;
       $qdataapproval->approvallevel    = $request->approvallevel;
        # ---------------
        $qdataapproval->save();
       }
        
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE DATAPPROVAL (" . $qdataapproval->id_dataapproval . ") " . strtoupper($request->nama_dataapproval), Auth::user()->id, $request);
    }

    public function updateData($request) {
        $qMaster               = new MasterModel;

        if($request->id_departemen == "all" && $request->id_jabatan == "all"){
            $qDepartemen                 = $qMaster->getSelectDepartemenById($request->id_cabang);
            //hilangkan duplicate data departemen
            foreach($qDepartemen as $key => $value){
                $qDepartemenn[$value->id] = $value;
            }
            $qDepartemen = $qDepartemenn;
            $i = 0;
            foreach($qDepartemen as $key2 => $value2){
                $qJabatan                 = $qMaster->getSelectJabatanById($request->id_cabang, $value2->id);
                //hilangkan duplicate data jabatan
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $value2->id,
                                                'id_jabatan' => $value4->id
                                                ]
                                                ;
                }
            }
            $qJabatan = $collection;
            $kode = 1;

       }else if($request->id_departemen == "all" && $request->id_jabatan != "all"){
        // $qJabatan                 = $qMaster->getSelectJabatanByIdCabangAndAllJabatan($request->id_cabang,$request->id_jabatan);
        $qDepartemen                 = $qMaster->getSelectDepartemenById($request->id_cabang);
            //hilangkan duplicate data departemen
            foreach($qDepartemen as $key => $value){
                $qDepartemenn[$value->id] = $value;
            }
            $qDepartemen = $qDepartemenn;
            $i = 0;
            foreach($qDepartemen as $key2 => $value2){
                $qJabatan                 = $qMaster->getSelectJabatanById($request->id_cabang, $value2->id);
                //hilangkan duplicate data jabatan
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    if($request->id_jabatan == $value4->id){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $value2->id,
                                                'id_jabatan' => $value4->id
                                                ]
                                                ;
                    }
                }
            }
            $qJabatan = $collection;
            $kode = 2;
       }else if($request->id_departemen != "all" && $request->id_jabatan == "all"){
                $qJabatan                 = $qMaster->getSelectJabatanById($request->id_cabang, $request->id_departemen);
                //hilangkan duplicate data jabatan
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $request->id_departemen,
                                                'id_jabatan' => $value4->id
                                                ]
                                                ;
                }
            $qJabatan = $collection;
            $kode = 3;
       }else{
        $qJabatan                 = $qMaster->getSelectJabatanByIdCabangAndByIdDepartemenByIdJabatan($request->id_cabang,$request->id_departemen,$request->id_jabatan);
             if(!empty($qJabatan)){
                foreach($qJabatan as $key3 => $value3){
                    $qJabatann[$value3->id_jabatan] = $value3;
                }
                $qJabatan = $qJabatann;
                $qJabatann = null;
                foreach($qJabatan as $key4 => $value4){
                    $collection[]            = (object)
                                                [
                                                'id_cabang' =>$request->id_cabang,
                                                'id_departemen' => $request->id_departemen,
                                                'id_jabatan' => $value4->id_jabatan
                                                ]
                                                ;
                }
                $qJabatan = $collection;
                $kode = 4;
            }else{
                $cabang = DB::table('m_cabang')->where('id_cabang', $request->id_cabang)->first();
                $departemen = DB::table('m_departemen')->where('id_departemen', $request->id_cabang)->first();
                $jabatan = DB::table('m_jabatan')->where('id_jabatan', $request->id_jabatan)->first();
                return array("status"=>'failed', "response"=> setString('Karyawan '.$cabang->nama_cabang.' departemen '.$departemen->nama_departemen .' dan jabatan '.$jabatan->nama_jabatan.' tidak ada'));
            }
        }
        // hilangkan duplicate data jabatan
        // foreach($qJabatan as $key3 => $value3){
        //     $qJabatann[$value3->id_jabatan] = $value3;
        // }
        //$qJabatan = $qJabatann;
       foreach($qJabatan as $index => $value){
         DB::table("tm_dataapproval")
                             ->where("id_dataapproval", $request->id_dataapproval)
                            ->update([
                            	    	"id_cabang_approval"=>$request->id_cabang_approvaller,
                            	    	"id_departemen_approval"=>$request->id_departemen_approvaller,
                            	    	"id_approvaller"=>$request->id_approvaller,
                            	    	"approvallevel"=>$request->approvallevel,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	        "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
        }
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE DATAPPROVAL(" . $request->id_dataapproval . ") " . strtoupper($request->nama_jabatan), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("tm_dataapproval")
                             ->where("id_dataapproval", $request->id_dataapproval)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE DATAPPROVAL (" . $request->id_dataapproval . ") " . strtoupper($request->id_dataapproval), Auth::user()->id, $request);
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  