<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class GrupabsenModel extends Model
{
     protected $table    = "tm_grupabsen";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("tm_grupabsen")
                            ->select("id_grupabsen","nama_grupabsen","jam_masuk","jam_keluar","jam_masuk2","jam_keluar2","jam_masuk3","jam_keluar3")
                           
                            ->orderBy("id_grupabsen", "DESC");

        if(session()->has("SES_SEARCH_GRUPABSEN")) {
            $query->where("nama_grupabsen", "LIKE", "%" . session()->get("SES_SEARCH_GRUPABSEN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("tm_grupabsen")
                            ->select("id_grupabsen", "nama_grupabsen","jam_masuk","jam_keluar","jam_masuk2","jam_keluar2","jam_masuk3","jam_keluar3")
                            ->where("id_grupabsen", $id)
                            ->orderBy("nama_grupabsen", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qgrupabsen              = new GrupabsenModel;
        # ---------------
       // $qgrupabsen->kode       = setString($request->kode);
        $qgrupabsen->nama_grupabsen   = setString($request->nama_grupabsen);
        $qgrupabsen->jam_masuk        = setString($request->jam_masuk);
        $qgrupabsen->jam_keluar       = setString($request->jam_keluar);
        $qgrupabsen->jam_masuk2        = setString($request->jam_masuk2);
        $qgrupabsen->jam_keluar2       = setString($request->jam_keluar2);
	$qgrupabsen->jam_masuk3        = setString($request->jam_masuk3);
        $qgrupabsen->jam_keluar3       = setString($request->jam_keluar3);

        $qgrupabsen->user_id          = setString(Auth::user()->id);
        $qgrupabsen->create_at        = setString(date('Y-m-d H:i:s'));
        $qgrupabsen->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qgrupabsen->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE GRUPABSEN (" . $qgrupabsen->id_grupabsen . ") " . strtoupper($request->nama_grupabsen), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("tm_grupabsen")
                             ->where("id_grupabsen", $request->id_grupabsen)
                            ->update([ "nama_grupabsen"=>$request->nama_grupabsen,
                            	    	"jam_masuk"=>$request->jam_masuk,
                            	    	"jam_keluar"=>$request->jam_keluar,
                            	    	"jam_masuk2"=>$request->jam_masuk2,
                                        "jam_keluar2"=>$request->jam_keluar2,
					"jam_masuk3"=>$request->jam_masuk3,
                                        "jam_keluar3"=>$request->jam_keluar3,
                                        "user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE GRUPABSEN(" . $request->id_grupabsen . ") " . strtoupper($request->nama_grupabsen), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("tm_grupabsen")
                             ->where("id_grupabsen", $request->id_grupabsen)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE GRUPABSEN (" . $request->id_grupabsen . ") " . strtoupper($request->nama_grupabsen), Auth::user()->id, $request);
    }
}
