<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use DateTime;
use App\Model\Absensi\DataabsensiModel;

class TemporaryAbsenModel extends Model
{
  protected $table    = "tm_temporary_txt";
  public $timestamps= false ;

  public function createData($dataarray) {
     $create_data = [];
      $updatedata = [];
      $rt = [];
      $cekdata = [];
      $status_terlambat = 0;
      foreach ($dataarray as $key => $value) {
          # code...
      $status_terlambat = 0;
      $value_array = explode(",", $value);
      
      $whereCondition = 
      [
        ['noabsen', $value_array[1]],
        ['tgl_absensi', $value_array[2]],
      ];

      $umr = 0;
      $gaji_pokok = 0;
      $qgrupabsen = DB::table("p_karyawan")->where([["noabsen", $value_array[1]],["p_karyawan.aktif",1]])
        //             // ->join("tm_dataabsensi","p_karyawan.id_karyawan","tm_dataabsensi.id_karyawan")
        ->join("m_departemen","p_karyawan.id_departemen","m_departemen.id_departemen")
        ->join("tm_grupabsen","m_departemen.id_grupabsen","tm_grupabsen.id_grupabsen")
        ->join("m_cabang","m_cabang.id_cabang","p_karyawan.id_cabang")
        ->select('id_karyawan','jam_masuk','jam_masuk2','jam_masuk3','jam_keluar','jam_keluar2','jam_keluar3', 'nik','gaji_pokok')
        ->first();

        $cek = null;
        if($qgrupabsen != null){
          $whereCondition = 
          [
            ['id_karyawan', $qgrupabsen->id_karyawan],
            ['tgl_absensi', $value_array[2]],
          ];
          $cek = DB::table("tm_dataabsensi")->where($whereCondition)->first();
        }
        
         if($cek != null && $qgrupabsen != null){
           if($value_array[0] == "I"){
             $menitawal = $cek->jam_masuk;
             if($menitawal == null){
             $menitawal = '00:00:00';
             }
             $menitkeluar = $cek->jam_keluar;
             if($menitkeluar == null){
             $menitkeluar = '00:00:00';
             }
             $menitakhir = $value_array[4];
             $laststatus = $cek->status_absensi;
             
             $jadwal = DB::table('tm_jadwal')
             ->leftJoin('tm_grupabsen','tm_grupabsen.id_grupabsen', 'tm_jadwal.id_grupabsen')
             ->where([ ['nik', $qgrupabsen->nik], ['tanggal',$value_array[2]] ])->first();

              if(!is_null($jadwal)){
                if($value_array[3] == 6){
                  $schedule_in = $jadwal->jam_masuk2;
                  $schedule_out = $jadwal->jam_keluar2;
                  $filter_data['schedule_in']= $schedule_in;
                  $filter_data['schedule_out']= $schedule_out;
                }else if($value_array[3] == 0){
                  $schedule_in = $jadwal->jam_masuk3;
                  $schedule_out = $jadwal->jam_keluar3;
                  $filter_data['schedule_in']= $schedule_in;
                  $filter_data['schedule_out']= $schedule_out;
                }else{
                  $schedule_in = $jadwal->jam_masuk;
                  $schedule_out = $jadwal->jam_keluar;
                  $filter_data['schedule_in']= $schedule_in;
                  $filter_data['schedule_out']= $schedule_out;
                }
              }else{
                if($value_array[3] == 6){
                    $schedule_in = $qgrupabsen->jam_masuk2;
                    $schedule_out = $qgrupabsen->jam_keluar2;
                    $filter_data['schedule_in']= $schedule_in;
                    $filter_data['schedule_out']= $schedule_out;
                }else if($value_array[3] == 0){
                  $schedule_in = $qgrupabsen->jam_masuk3;
                  $schedule_out = $qgrupabsen->jam_keluar3;
                  $filter_data['schedule_in']= $schedule_in;
                  $filter_data['schedule_out']= $schedule_out;
                }else{
                    $schedule_in = $qgrupabsen->jam_masuk;
                    $schedule_out = $qgrupabsen->jam_keluar;
                    $filter_data['schedule_in']= $schedule_in;
                    $filter_data['schedule_out']= $schedule_out;
                }
              }
               // dd($menitawal = $cek->schedule_in, $menitakhir);
             list($th,$tm,$ts) = explode(":",$menitawal);
               $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
               $total_menit_masuk = $dtAwal-$ts;
   
               list($t2h,$t2m,$t2s) = explode(":",$menitakhir);
               $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
               $total_menit_datang = $dtAwal2-$t2s;
   
               // dd($total_menit_masuk, $total_menit_datang);
               if(($menitawal == null || $menitawal == '00:00:00')  && $menitakhir != '00:00:00'){
                 list($t4h,$t4m,$t4s) = explode(":",$schedule_in);
                 $dtAwal4 = mktime($t4h,$t4m,$t4s,'1','1','1'); 
                 $total_menit_datang4 = $dtAwal4-$t4s;
   
                   if($total_menit_datang4 >= $total_menit_datang){
                       $filter_data['menit_terlambat'] = 0;//0
                       // $filter_data['status_absensi'] = "H/NCO";
                       if($menitkeluar != '00:00:00'){
                         list($th,$tm,$ts) = explode(":",$filter_data['schedule_out']);
                         $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
                         $total_jam_keluar = floor($dtAwal/3600);
   
                         list($t2h,$t2m,$t2s) = explode(":",$menitkeluar);
                         $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
                         $total_jam_pulang = floor($dtAwal2/3600);
   
                           if($total_jam_pulang <= $total_jam_keluar){
                             // $selisih  = date_diff(date_create($pilihan_waktu), date_create($schedule_out));
                             // $nilai_jam = $selisih->h;
                             // $nilai_menit = $selisih->i;
                             // $total_menit_telat = $nilai_jam*60+$nilai_menit;
                             // $menit_terlambat = $total_menit_telat;
                             // $status_absensi = "H/EO";
                             if($laststatus == 'DL'){
                                 $filter_data['status_absensi'] = 'H/EO/'.$laststatus;
                                 $filter_data['status_potong_gaji']= 0;
                                 $filter_data['status_potong_makan']= 0;
                             }else{
                                 $filter_data['status_absensi'] = 'H';
                                 $filter_data['status_potong_gaji']= 0;
                                 $filter_data['status_potong_makan']= 0;
                             }
                           // $filter_data['status_absensi']= 'H/EO';
                           }else{
                             // $menit_terlambat = 0;
                             // $status_absensi = "A";
                             $filter_data['status_absensi']= 'H';
                             $filter_data['status_potong_gaji']= 0;
                             $filter_data['status_potong_makan']= 0;
                           }
                       }else{
                         $filter_data['status_absensi']= 'H/NCO';
                         $filter_data['status_potong_gaji']= 0;
                         $filter_data['status_potong_makan']= 1;
                       }
   
                         $filter_data['jam_masuk'] = $menitakhir;
                   }else{
                     $selisih  = date_diff(date_create($menitakhir), date_create($schedule_in));
                     $nilai_jam = $selisih->h;
                     $nilai_menit = $selisih->i;
                     $total_menit_telat = $nilai_jam*60+$nilai_menit;
                     $menit_terlambat = $total_menit_telat;
                     $filter_data['menit_terlambat'] = $menit_terlambat;
                     $filter_data['status_absensi'] = "H/LI/NCO";
                     $filter_data['status_potong_gaji']= 0;
                     $filter_data['status_potong_makan']= 1;
                     
                     if($menitkeluar != '00:00:00'){
                       list($th,$tm,$ts) = explode(":",$filter_data['schedule_out']);
                       $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
                       $total_jam_keluar = floor($dtAwal/3600);
   
                       list($t2h,$t2m,$t2s) = explode(":",$menitkeluar);
                       $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
                       $total_jam_pulang = floor($dtAwal2/3600);
   
                       if($total_jam_pulang <= $total_jam_keluar){
                         // $selisih  = date_diff(date_create($pilihan_waktu), date_create($schedule_out));
                         // $nilai_jam = $selisih->h;
                         // $nilai_menit = $selisih->i;
                         // $total_menit_telat = $nilai_jam*60+$nilai_menit;
                         // $menit_terlambat = $total_menit_telat;
                         // $status_absensi = "H/EO";
                         $filter_data['status_absensi']= 'H/LI';
                         $filter_data['status_potong_gaji']= 0;
                         $filter_data['status_potong_makan']= 0;
                         $filter_data['status_terlambat']= 1;
                       }else{
                         // $menit_terlambat = 0;
                           // $status_absensi = "A";
                         $filter_data['status_absensi']= 'H/LI';
                         $filter_data['status_potong_gaji']= 0;
                         $filter_data['status_potong_makan']= 0;
                         $filter_data['status_terlambat']= 1;
                       }
                     }else{
                       $filter_data['status_absensi']= 'H/LI/NCO';
                       $filter_data['status_potong_gaji']= 0;
                       $filter_data['status_potong_makan']= 1;
                     }
                       $filter_data['jam_masuk'] = $menitakhir;
               }
   
                 if($laststatus == 'DL'){
                 $filter_data['status_absensi'] = "DL";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'CT'){
                 $filter_data['status_absensi'] = "CT";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 1;
                 }else if($laststatus == 'I'){
                 $filter_data['status_absensi'] = "I";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 1;
                 }else if($laststatus == 'IK'){
                 $filter_data['status_absensi'] = "IK";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'S'){
                 $filter_data['status_absensi'] = "S";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'IDC'){
                  $filter_data['status_absensi'] = "IDC";
                  // $filter_data['status_potong_gaji']= 1;
                    // $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'KR'){
                  $filter_data['status_absensi'] = "KR";
                  // $filter_data['status_potong_gaji']= 1;
                    // $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'Waiting'){
                  $filter_data['status_absensi'] = "Waiting";
                  // $filter_data['status_potong_gaji']= 0;
                    // $filter_data['status_potong_makan']= 1;
              }
                //  else{
                //  $filter_data['status_absensi'] = $filter_data['status_absensi'];
                //    $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                //  }
           }else if(($menitawal == null || $menitawal == '00:00:00') && ($menitakhir == null || $menitakhir == '00:00:00')){
               $filter_data['menit_terlambat'] = 0;
               if($laststatus == 'DL'){
                $filter_data['status_absensi'] = "DL";
               //  $filter_data['status_potong_gaji']= 0;
               //    $filter_data['status_potong_makan']= 0;
                }else if($laststatus == 'CT'){
                $filter_data['status_absensi'] = "CT";
               //  $filter_data['status_potong_gaji']= 0;
               //    $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'I'){
                $filter_data['status_absensi'] = "I";
               //  $filter_data['status_potong_gaji']= 0;
               //    $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'IK'){
                $filter_data['status_absensi'] = "IK";
               //  $filter_data['status_potong_gaji']= 0;
               //    $filter_data['status_potong_makan']= 0;
                }else if($laststatus == 'S'){
                $filter_data['status_absensi'] = "S";
               //  $filter_data['status_potong_gaji']= 0;
               //    $filter_data['status_potong_makan']= 0;
                }else if($laststatus == 'IDC'){
                 $filter_data['status_absensi'] = "IDC";
                 // $filter_data['status_potong_gaji']= 1;
                   // $filter_data['status_potong_makan']= 1;
               }else if($laststatus == 'KR'){
                 $filter_data['status_absensi'] = "KR";
                 // $filter_data['status_potong_gaji']= 1;
                   // $filter_data['status_potong_makan']= 1;
               }else if($laststatus == 'Waiting'){
                 $filter_data['status_absensi'] = "Waiting";
                 // $filter_data['status_potong_gaji']= 0;
                   // $filter_data['status_potong_makan']= 1;
             }
           }else{
             // $menit_telat = $cek->jam_keluar;
   
               if($total_menit_masuk <= $total_menit_datang){
                 $waktu_tercepat = $menitawal;
                 $filter_data['menit_terlambat'] = $cek->menit_terlambat;
               $filter_data['jam_masuk'] = $waktu_tercepat;
             }else{
                 $waktu_tercepat = $menitakhir;
                 $selisih  = date_diff(date_create($schedule_in), date_create($waktu_tercepat));
                 $nilai_jam = $selisih->h;
                 $nilai_menit = $selisih->i;
                 $total_menit_telat = $nilai_jam*60+$nilai_menit;
                 $menit_terlambat = $total_menit_telat;
                 $filter_data['menit_terlambat'] = $total_menit_telat;
   
                 list($t3h,$t3m,$t3s) = explode(":",$waktu_tercepat);
                 $dtAwal3 = mktime($t3h,$t3m,$t3s,'1','1','1'); 
                 $total_menit_datang3 = $dtAwal3-$t3s;
                 list($t4h,$t4m,$t4s) = explode(":",$schedule_in);
                 $dtAwal4 = mktime($t4h,$t4m,$t4s,'1','1','1'); 
                 $total_menit_datang4 = $dtAwal4-$t4s;
   
                 if($total_menit_datang4 >= $total_menit_datang3){
                   $filter_data['menit_terlambat'] = 0;//0
                   if($menitkeluar == '00:00:00'){
                   $status_absensi = "H/NCO";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 0;
                   }else{
                   $status_absensi = "H";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 0;
                   }
                 }else{
                   $selisih  = date_diff(date_create($waktu_tercepat), date_create($schedule_in));
                   $nilai_jam = $selisih->h;
                   $nilai_menit = $selisih->i;
                   $total_menit_telat = $nilai_jam*60+$nilai_menit;
                   $menit_terlambat = $total_menit_telat;
                   $filter_data['menit_terlambat'] = $menit_terlambat;
                   $status_absensi = "H/LI/NCO";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 1;
                 }
   
   
               $filter_data['jam_masuk'] = $waktu_tercepat;
   
               $filter_data['status_absensi'] = $status_absensi;
               $filter_data['nik'] = $qgrupabsen->nik;
   
               } 
   
           }
           
           if($laststatus == 'DL'){
                 $filter_data['status_absensi'] = "DL";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'CT'){
                 $filter_data['status_absensi'] = "CT";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 1;
                 }else if($laststatus == 'I'){
                 $filter_data['status_absensi'] = "I";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 1;
                 }else if($laststatus == 'IK'){
                 $filter_data['status_absensi'] = "IK";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'S'){
                 $filter_data['status_absensi'] = "S";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'IDC'){
                  $filter_data['status_absensi'] = "IDC";
                  // $filter_data['status_potong_gaji']= 1;
                    // $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'KR'){
                  $filter_data['status_absensi'] = "KR";
                  // $filter_data['status_potong_gaji']= 1;
                    // $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'Waiting'){
                  $filter_data['status_absensi'] = "Waiting";
                  // $filter_data['status_potong_gaji']= 0;
                    // $filter_data['status_potong_makan']= 1;
              }
   
           DB::table("tm_dataabsensi")->where("id_dataabsensi", $cek->id_dataabsensi)->update($filter_data); 
         $filter_data = null;
       }else{
           $jam_keluar = $cek->jam_keluar;
           $laststatus = $cek->status_absensi;
   
           if($jam_keluar == null){
             $jam_keluar ='00:00:00';
           }
           $jam_masuk = $cek->jam_masuk;
           if($jam_masuk == null){
             $jam_masuk ='00:00:00';
           }
           $menitakhir = $value_array[4];
   
           if($value_array[3] == 6){
                 $schedule_in = $qgrupabsen->jam_masuk2;
                 $schedule_out = $qgrupabsen->jam_keluar2;
                 $filter_data['schedule_in']= $schedule_in;
                 $filter_data['schedule_out']= $schedule_out;
               }else{
                 $schedule_in = $qgrupabsen->jam_masuk;
                 $schedule_out = $qgrupabsen->jam_keluar;
                 $filter_data['schedule_in']= $schedule_in;
                 $filter_data['schedule_out']= $schedule_out;
             }
   
             
           if($jam_masuk =='00:00:00'){
             $filter_data['jam_masuk'] = NULL;
             // $filter_data['status_absensi'] = 'A';
             $filter_data['menit_terlambat'] = $cek->menit_terlambat;
             $filter_data['jam_keluar'] = $jam_keluar;
   
             if($menitakhir == '00:00:00'){
             list($th,$tm,$ts) = explode(":",$menitakhir);
             $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
               $total_waktu_pulang = $dtAwal-$ts;
             list($t2h,$t2m,$t2s) = explode(":",$filter_data['schedule_out']);
             $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
             $total_waktu_keluar = $dtAwal2-$t2s;
   
               if($total_waktu_pulang >= $total_waktu_keluar){
               $filter_data['jam_keluar'] = $menitakhir;
                 $filter_data['status_absensi'] = "H/NCI";
                  $filter_data['status_potong_gaji']= 0;
                 $filter_data['status_potong_makan']= 1;
               }else{
                  $filter_data['jam_keluar'] = $menitakhir;
                  $filter_data['status_absensi'] = "H/NCI/EO";
                  $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 1;
               }
             }else{
              list($th,$tm,$ts) = explode(":",$menitakhir);
              $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
                $total_waktu_pulang = $dtAwal-$ts;
              list($t2h,$t2m,$t2s) = explode(":",$filter_data['schedule_out']);
              $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
              $total_waktu_keluar = $dtAwal2-$t2s;
    
                if($total_waktu_pulang >= $total_waktu_keluar){
                $filter_data['jam_keluar'] = $menitakhir;
                  $filter_data['status_absensi'] = "H/NCI";
                   $filter_data['status_potong_gaji']= 0;
                  $filter_data['status_potong_makan']= 1;
                }else{
                   $filter_data['jam_keluar'] = $menitakhir;
                   $filter_data['status_absensi'] = "H/NCI/EO";
                   $filter_data['status_potong_gaji']= 0;
                    $filter_data['status_potong_makan']= 1;
                }
              }
   
            }else{   
             $filter_data['jam_masuk'] = $jam_masuk;
             $filter_data['menit_terlambat'] = $cek->menit_terlambat;
             if($jam_keluar =='00:00:00'){
               $filter_data['jam_keluar'] = $menitakhir;
               list($th,$tm,$ts) = explode(":",$filter_data['jam_keluar']);
                 $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
                 $total_waktu_pulang = $total_waktu_pulang = $dtAwal-$ts;
   
                 list($t2h,$t2m,$t2s) = explode(":",$schedule_out);
                 $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
                 $total_waktu_keluar = $dtAwal2-$t2s;
   
                 if($total_waktu_pulang >= $total_waktu_keluar){
                 $filter_data['jam_keluar'] = $filter_data['jam_keluar'];
                 if($filter_data['menit_terlambat'] >0){
                   $filter_data['status_absensi'] = "H/LI";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 0;
                   $filter_data['status_terlambat']= 1;
                 }else{
                   $filter_data['status_absensi'] = "H";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 0;
                   }
                 }else{
                 $filter_data['jam_keluar'] = $filter_data['jam_keluar'];
                 if($filter_data['menit_terlambat'] >0){
                   $filter_data['status_absensi'] = "H/LI/EO";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 0;
                   $filter_data['status_terlambat']= 1;
                   }else{
                   $filter_data['status_absensi'] = "H/EO";
                   $filter_data['status_potong_gaji']= 0;
                   $filter_data['status_potong_makan']= 0;
                   }
                 }
             }else{
               list($th,$tm,$ts) = explode(":",$menitakhir);
                 $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
                 $total_waktu_pulang = $dtAwal-$ts;
                 list($t2h,$t2m,$t2s) = explode(":",$jam_keluar);
                 $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
                 $total_waktu_keluar = $dtAwal2-$t2s;
   
                 if($total_waktu_pulang >= $total_waktu_keluar){
                 $filter_data['jam_keluar'] = $menitakhir;
                   // $filter_data['status_absensi'] = "IJJ";
                 }else{
                 $filter_data['jam_keluar'] = $jam_keluar;
                   // $filter_data['status_absensi'] = "HADIR";
                 }
                 list($th,$tm,$ts) = explode(":",$filter_data['jam_keluar']);
                 $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
                 $total_waktu_pulangg = $dtAwal-$ts;
                 list($t2h,$t2m,$t2s) = explode(":",$schedule_out);
                 $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
                 $total_waktu_keluart = $dtAwal2-$t2s;
   
                 if($total_waktu_pulangg >= $total_waktu_keluart){
                 $filter_data['jam_keluar'] = $filter_data['jam_keluar'];
                   // $filter_data['status_absensi'] = 'H';
                   $status_absensi = 'H';
                   if($cek->menit_terlambat > 0){
                     $filter_data['status_absensi'] = 'H/LI';
                     $filter_data['status_potong_gaji']= 0;
                     $filter_data['status_potong_makan']= 0;
                     $filter_data['status_terlambat']= 1;
                   }else{
                     $filter_data['status_absensi'] = 'H';
                     $filter_data['status_potong_gaji']= 0;
                     $filter_data['status_potong_makan']= 0;
                   }
                 }else{
                 $filter_data['jam_keluar'] = $filter_data['jam_keluar'];
                   // $filter_data['status_absensi'] = 'I';
                   $status_absensi = 'H/EO';
                   if($cek->menit_terlambat > 0){
                     $filter_data['status_absensi'] = 'H/LI/EO';
                     $filter_data['status_potong_gaji']= 0;
                     $filter_data['status_potong_makan']= 0;
                     $filter_data['status_terlambat']= 1;
                   }else{
                     $filter_data['status_absensi'] = 'H/EO';
                     $filter_data['status_potong_gaji']= 0;
                     $filter_data['status_potong_makan']= 0;
               
                   }
                 }
                 
               //  if($cek->menit_terlambat > 0){
               //  $filter_data['status_absensi'] = 'H/LI';
               // }else{
               //  $filter_data['status_absensi'] = 'H';
               //  }
               $filter_data['nik'] = $qgrupabsen->nik;
             }
           }
   
           if($laststatus == 'DL'){
                 $filter_data['status_absensi'] = "DL";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'CT'){
                 $filter_data['status_absensi'] = "CT";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 1;
                 }else if($laststatus == 'I'){
                 $filter_data['status_absensi'] = "I";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 1;
                 }else if($laststatus == 'IK'){
                 $filter_data['status_absensi'] = "IK";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'S'){
                 $filter_data['status_absensi'] = "S";
                //  $filter_data['status_potong_gaji']= 0;
                //    $filter_data['status_potong_makan']= 0;
                 }else if($laststatus == 'IDC'){
                  $filter_data['status_absensi'] = "IDC";
                  // $filter_data['status_potong_gaji']= 1;
                    // $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'KR'){
                  $filter_data['status_absensi'] = "KR";
                  // $filter_data['status_potong_gaji']= 1;
                    // $filter_data['status_potong_makan']= 1;
                }else if($laststatus == 'Waiting'){
                  $filter_data['status_absensi'] = "Waiting";
                  // $filter_data['status_potong_gaji']= 0;
                    // $filter_data['status_potong_makan']= 1;
              }
   
               DB::table("tm_dataabsensi")->where("id_dataabsensi", $cek->id_dataabsensi)->update($filter_data);
         $filter_data = null;
           }
         $updatedata[] = $qgrupabsen->id_karyawan;
         }else if($cek == null && $qgrupabsen != null){
           $qdata= new DataabsensiModel;
           # code...
           $qdata['id_karyawan']= $qgrupabsen->id_karyawan;
           $create_data = $qdata['id_karyawan'];
           $pilihan_waktu = $value_array[4];
           if($value_array[0]=="I"){
               if($value_array[3] == 6){
                 $schedule_in = $qgrupabsen->jam_masuk2;
                 $schedule_out = $qgrupabsen->jam_keluar2;
                 $qdata['schedule_in']= $schedule_in;
                 $qdata['schedule_out']= $schedule_out;
               }else{
                 $schedule_in = $qgrupabsen->jam_masuk;
                 $schedule_out = $qgrupabsen->jam_keluar;
                 $qdata['schedule_in']= $schedule_in;
                 $qdata['schedule_out']= $schedule_out;
             }
   
               list($th,$tm,$ts) = explode(":",$schedule_in);
               $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
               $total_menit_masuk = $dtAwal-$ts;
   
               list($t2h,$t2m,$t2s) = explode(":",$pilihan_waktu);
               $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
               $total_menit_datang = $dtAwal2-$t2s;
   
               if($total_menit_masuk >= $total_menit_datang){
                 $menit_terlambat = 0;
                 // $status_absensi = "H";
               }
               else{
                 $selisih  = date_diff(date_create($pilihan_waktu), date_create($schedule_in));
                 $nilai_jam = $selisih->h;
                 $nilai_menit = $selisih->i;
                 $total_menit_telat = $nilai_jam*60+$nilai_menit;
                 $menit_terlambat = $total_menit_telat;
                 // $status_absensi = "I";
               }
   
               $qdata['status_absensi']= 'A';
               $qdata['status_potong_gaji']= 0;
               $qdata['status_potong_makan']= 1;
              //  $qdata['jam_masuk']= $pilihan_waktu;
              //  $qdata['jam_keluar']= '00:00:00';
               $qdata->nik      = $qgrupabsen->nik;
             $qdata->tgl_absensi      = $value_array[2];
             $qdata->menit_terlambat   = setString($menit_terlambat);
             $qdata->user_id   = setString(Auth::user()->id);
             $qdata->create_at   = setString(date('Y-m-d H:i:s'));
             $qdata->update_at   = setString(date('Y-m-d H:i:s'));
             $qdata->save();
             $qdata = null;
           }else{
               if($value_array[3] == 6){
                 $schedule_in = $qgrupabsen->jam_masuk2;
                 $schedule_out = $qgrupabsen->jam_keluar2;
                 $qdata['schedule_in']= $schedule_in;
                 $qdata['schedule_out']= $schedule_out;
               }else{
                 $schedule_in = $qgrupabsen->jam_masuk;
                 $schedule_out = $qgrupabsen->jam_keluar;
                 $qdata['schedule_in']= $schedule_in;
                 $qdata['schedule_out']= $schedule_out;
             }
   
               list($th,$tm,$ts) = explode(":",$schedule_out);
               $dtAwal = mktime($th,$tm,$ts,'1','1','1'); 
               $total_jam_keluar = floor($dtAwal/3600);
   
               list($t2h,$t2m,$t2s) = explode(":",$pilihan_waktu);
               $dtAwal2 = mktime($t2h,$t2m,$t2s,'1','1','1'); 
               $total_jam_pulang = floor($dtAwal2/3600);
   
               if($total_jam_pulang <= $total_jam_keluar){
                 $selisih  = date_diff(date_create($pilihan_waktu), date_create($schedule_out));
                 $nilai_jam = $selisih->h;
                 $nilai_menit = $selisih->i;
                 $total_menit_telat = $nilai_jam*60+$nilai_menit;
                 $menit_terlambat = $total_menit_telat;
                 // $status_absensi = "H/EO";
               $qdata['status_absensi']= 'H/NCI/EO';
               $qdata['status_potong_gaji']= 0;
               $qdata['status_potong_makan']= 1;
               }else{
                 $menit_terlambat = 0;
                 // $status_absensi = "A";
               $qdata['status_absensi']= 'H/NCI';
               $qdata['status_potong_gaji']= 0;
               $qdata['status_potong_makan']= 1;
               }
   
              //  $qdata['jam_keluar']= $pilihan_waktu;
              //  $qdata['jam_masuk']= '00:00:00';
               $qdata->nik      = $qgrupabsen->nik;
             $qdata->tgl_absensi      = $value_array[2];
             $qdata->menit_terlambat   = setString($menit_terlambat);
             $qdata->user_id   = setString(Auth::user()->id);
             $qdata->create_at   = setString(date('Y-m-d H:i:s'));
             $qdata->update_at   = setString(date('Y-m-d H:i:s'));
             $qdata->save();
             $qdata = null;
   
           }
           $rt[] = $qgrupabsen->id_karyawan; 
         }else if($cek == null && $qgrupabsen == null){
             $cekdata[] = $value_array[1].' '.$value_array[5];
             $status = "true";
           }
         }
   
       // if(array_unique($create_data) >0 &&)
       if(count(array_unique($cekdata))>0){
         $ket[] = 'cek kembali nik dibawah pada data karyawan, jika sudah sesuai lakukan import ulang';
         return array("status"=>'failed', "response"=>array_merge($ket,$cekdata));
       }else{
         return array("status"=>'true', "response"=>'Upload Data Berhasil');
       }
       // return array("status"=>$status, "response"=>implode(',', $response));
           # ---------------
   
           # ---------------
           /* ----------
            Logs
            ----------------------- */  
            // $qLog       = new LogModel;
            //    # ---------------;
            // $qLog->createLog("CREATE DATAABSENSI (" . $qdataabsensi->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
          }

       public function updateData($request) {
         DB::table("tm_dataabsensi")
         ->where("id_dataabsensi", $request->id_dataabsensi)
         ->update([ "nama_dataabsensi"=>$request->nama_dataabsensi,
          "jam_masuk"=>$request->jam_masuk,
          "jam_keluar"=>$request->jam_keluar,
          "user_id"=>setString(Auth::user()->id),
          "update_at"=>setString(date('Y-m-d H:i:s')) 
        ]);


        # ---------------

        /* ----------
         Logs
         ----------------------- */
         $qLog       = new LogModel;
            # ---------------;
         $qLog->createLog("UPDATE DATAABSENSI(" . $request->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
       }

       public function removeData($request) {
         DB::table("tm_dataabsensi")
         ->where("id_dataabsensi", $request->id_dataabsensi)->delete();



        /* ----------
         Logs
         ----------------------- */
         $qLog       = new LogModel;
            # ---------------;
         $qLog->createLog("DELETE DATAABSENSI (" . $request->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
       }

  public function importcsv($dataarray) {
    $updatedata = [];
    $create_data = [];
    $id_dataabsensi = [];
      foreach ($dataarray as $key => $value) {
          # code...
          $whereCondition = 
          [
            ['nik', $value['employee_id']],
            ['tgl_absensi', $value['date']],
          ];
          $cek = DB::table("tm_dataabsensi")->where($whereCondition)->first();
          if($cek != null){
            // if($cek->status_absensi != $value['attendance_code']){
              $inisial =['DL','CT','I','S','A','H','H/NCI/EO','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC','IK'];
              // $val_stat_ma = [0,1,2,1,1,0,1,1,0,1,1,1,1,0,1,1];
              $val_stat_makan = [0,1,1,1,1,0,1,1,0,0,0,1,1,0,1,1];
              $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0];
              $menit_terlambat = 0;
              $status_potong_gaji = 0;
              $status_potong_makan = 0;
              $status_terlambat = 0;
              
              // if($value['check_in'] != null && $value['check_out'] != null && $value['attendance_code'] != 'DL'){
              //   if(($value['check_in'] > $value['schedule_in']) && ($value['check_out'] < $value['schedule_out'])) {
              //     $status_absensi = 'H/LI/EO';
              //     $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
              //     $nilai_jam = $selisih->h;
              //     $nilai_menit = $selisih->i;
              //     $total_menit_telat = $nilai_jam*60+$nilai_menit;
              //     $menit_terlambat = $total_menit_telat;
              //     $status_terlambat = 1;
              //   }else if(($value['check_in'] > $value['schedule_in']) && ($value['check_out'] >= $value['schedule_out'])) {
              //     $status_absensi = 'H/LI';
              //     $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
              //     $nilai_jam = $selisih->h;
              //     $nilai_menit = $selisih->i;
              //     $total_menit_telat = $nilai_jam*60+$nilai_menit;
              //     $menit_terlambat = $total_menit_telat;
              //     $status_terlambat = 1;
              //   }else if(($value['check_in'] <= $value['schedule_in']) && ($value['check_out'] < $value['schedule_out'])) {
              //     $status_absensi = 'H/EO';
              //   }else {
              //     $status_absensi = 'H';
              //   }
              // }else if($value['check_in'] == null && $value['check_out'] != null && $value['attendance_code'] != 'DL'){
              //   if($value['check_out'] < $value['schedule_out']) {
              //     $status_absensi = 'H/NCI/EO';
              //   }else{
              //     $status_absensi = 'H/NCI';
              //   }
              // }else if($value['check_in'] != null && $value['check_out'] == null && $value['attendance_code'] != 'DL'){
              //   if($value['check_in'] > $value['schedule_in']) {
              //     $status_absensi = 'H/LI/NCO';
              //     $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
              //     $nilai_jam = $selisih->h;
              //     $nilai_menit = $selisih->i;
              //     $total_menit_telat = $nilai_jam*60+$nilai_menit;
              //     $menit_terlambat = $total_menit_telat;
              //   }else{
              //     $status_absensi = 'H/NCO';
              //   }
              // }else{
              //     $status_absensi = $value['attendance_code'];
              // }

              // foreach ($inisial as $keyinisial => $valueinisial) {
              //   # code...
              //   if($status_absensi == $valueinisial){
              //     $status_potong_gaji = $val_stat_gaji[$keyinisial];
              //     $status_potong_makan = $val_stat_makan[$keyinisial];
              //   }
              // }

                if(($value['check_in'] == '00:00:00' || $value['check_in'] == '00:00') && ($value['check_out'] == '00:00:00' || $value['check_out'] == '00:00')){
                    $status_absensi = $value['attendance_code'];
                }else if($value['check_in'] != null && $value['check_out'] == null){
                // $status_absensi = 'H/NCI';
                    if($value['check_in'] > $value['schedule_in'] ){
                        $status_absensi = 'H/LI/NCO';
                        $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if($value['check_in'] <= $value['schedule_in'] ){
                        $status_absensi = 'H/NCO';
                    }
                }else if($value['check_in'] == null && $value['check_out'] != null){
                    // $status_absensi = 'H/NCI';
                    if($value['check_out'] < $value['schedule_out'] ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($value['check_out'] > $value['schedule_out'] ){
                        $status_absensi = 'H/NCI';
                    }
                }else if($value['check_in'] != null && $value['check_out'] != null){
                    if(($value['check_in'] != '00:00:00' || $value['check_in'] != '00:00') && ($value['check_out'] == '00:00:00' || $value['check_out'] == '00:00')){
                        if($value['check_in'] < $value['schedule_in'] ){
                            $status_absensi = 'H/NCO';
                            $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }else if($value['check_in'] > $value['schedule_in'] ){
                            $status_absensi = 'H/LI/NCO';
                        }

                    }else if(($value['check_in'] == '00:00:00' || $value['check_in'] == '00:00') && ($value['check_out'] != '00:00:00' || $value['check_out'] != '00:00')){
                        if($value['check_out'] < $value['schedule_out'] ){
                            $status_absensi = 'H/NCI/EO';
                        }else if($value['check_out'] >= $value['schedule_out'] ){
                            $status_absensi = 'H/NCI';
                        }
                    }else{
                        // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                        if($value['check_in'] <= $value['schedule_in'] && $value['check_out'] < $value['schedule_out']){
                            $status_absensi = 'H/EO';
                            $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }elseif($value['check_in'] <= $value['schedule_in'] && $value['check_out'] >= $value['schedule_out']){
                            $status_absensi = 'H';
                            $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }elseif($value['check_in'] > $value['schedule_in'] && $value['check_out'] < $value['schedule_out']){
                            $status_absensi = 'H/LI/EO';
                        }elseif($value['check_in'] > $value['schedule_in'] && $value['check_out'] >= $value['schedule_out']){
                            $status_absensi = 'H/LI';
                        }
                    }
                }else{
                    $status_absensi = $value['attendance_code'];
                }

                foreach ($inisial as $keyinisial => $valueinisial) {
                # code...
                    if($status_absensi == $valueinisial){
                        $status_potong_gaji = $val_stat_gaji[$keyinisial];
                        $status_potong_makan = $val_stat_makan[$keyinisial];
                    }
                }

            $updatedata[] = [
                      "status_absensi"=>$status_absensi,
                      "status_potong_makan"=>$status_potong_makan,
                      "status_potong_gaji"=>$status_potong_gaji,
                      "jam_masuk"=>$value['check_in'],
                      "jam_keluar"=>$value['check_out'],
                      "schedule_in"=>$value['schedule_in'],
                      "schedule_out"=>$value['schedule_out'],
                      "menit_terlambat"=>$menit_terlambat,
                      "status_terlambat"=>$status_terlambat,
                    ];
            $id_dataabsensi[] = $cek->id_dataabsensi;        
            // DB::table("tm_dataabsensi")
            // ->where("id_dataabsensi", $cek->id_dataabsensi)
            // ->update([
            //           "status_absensi"=>$value['attendance_code'],
            //           "status_potong_makan"=>$status_potong_makan,
            //           "status_potong_gaji"=>$status_potong_gaji,
            //           "jam_masuk"=>$value['check_in'],
            //           "jam_keluar"=>$value['check_out'],
            //         ]);
            // }
          }else{
              $inisial =['DL','CT','I','S','A','H','H/NCI/EO','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC','IK'];
              $val_stat_makan = [0,1,1,1,1,0,1,1,0,0,0,1,1,0,1,1];
              $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0];
              $menit_terlambat = 0;
              $status_potong_gaji = 0;
              $status_potong_makan = 0;
              $status_terlambat = 0;
              // DB::table("tm_dataabsensi as a")
              //               ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang")
              //               ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
              //               ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
              //               ->where($WhereCondition)
              //               ->orderBy("id_dataabsensi", "DESC")
              //               ->orderBy("tgl_absensi", "DESC")

              $p_karyawan = DB::table('p_karyawan as a')
                          ->select('a.id_karyawan','a.nik','a.id_jabatan','b.nama_jabatan','b.id_jabatan','b.id_level')
                          ->leftjoin('m_jabatan as b','b.id_jabatan','=','a.id_jabatan')
                          ->where('nik',$value['employee_id'])
                          ->first();

              $id = '0';
              if($p_karyawan != null){
              $id = $p_karyawan->id_karyawan;
              }


              if($value['check_in'] != null && $value['check_out'] != null && $value['attendance_code'] != 'DL'){
                if(($value['check_in'] > $value['schedule_in']) && ($value['check_out'] < $value['schedule_out'])) {
                  $status_absensi = 'H/LI/EO';
                  $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                  $nilai_jam = $selisih->h;
                  $nilai_menit = $selisih->i;
                  $total_menit_telat = $nilai_jam*60+$nilai_menit;
                  $menit_terlambat = $total_menit_telat;
                  $status_terlambat = 1;
                }else if(($value['check_in'] > $value['schedule_in']) && ($value['check_out'] >= $value['schedule_out'])) {
                  $status_absensi = 'H/LI';
                  $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                  $nilai_jam = $selisih->h;
                  $nilai_menit = $selisih->i;
                  $total_menit_telat = $nilai_jam*60+$nilai_menit;
                  $menit_terlambat = $total_menit_telat;
                  $status_terlambat = 1;
                }else if(($value['check_in'] <= $value['schedule_in']) && ($value['check_out'] < $value['schedule_out'])) {
                  $status_absensi = 'H/EO';
                }else {
                  $status_absensi = 'H';
                }
              }else if($value['check_in'] == null && $value['check_out'] != null && $value['attendance_code'] != 'DL'){
                if($value['check_out'] < $value['schedule_out']) {
                  $status_absensi = 'H/NCI/EO';
                }else{
                  $status_absensi = 'H/NCI';
                }
              }else if($value['check_in'] != null && $value['check_out'] == null && $value['attendance_code'] != 'DL'){
                if($value['check_in'] > $value['schedule_in']) {
                  $status_absensi = 'H/LI/NCO';
                  $selisih  = date_diff(date_create($value['check_in']), date_create($value['schedule_in']));
                  $nilai_jam = $selisih->h;
                  $nilai_menit = $selisih->i;
                  $total_menit_telat = $nilai_jam*60+$nilai_menit;
                  $menit_terlambat = $total_menit_telat;
                }else{
                  $status_absensi = 'H/NCO';
                }
              }else{
                  $status_absensi = $value['attendance_code'];
              }

              
              foreach ($inisial as $keyinisial => $valueinisial) {
                # code...
                if($status_absensi == $valueinisial){
                  $status_potong_gaji = $val_stat_gaji[$keyinisial];
                  $status_potong_makan = $val_stat_makan[$keyinisial];
                }
              }

              $create_data [] = [
                      "id_karyawan"=>$id,
                      "nik"=>$value['employee_id'],
                      "tgl_absensi"=>$value['date'],
                      "status_absensi"=>$status_absensi,
                      "status_potong_makan"=>$status_potong_makan,
                      "status_potong_gaji"=>$status_potong_gaji,
                      "jam_masuk"=>$value['check_in'],
                      "jam_keluar"=>$value['check_out'],
                      "schedule_in"=>$value['schedule_in'],
                      "schedule_out"=>$value['schedule_out'],
                      "menit_terlambat"=>$menit_terlambat,
                      "status_terlambat"=>$status_terlambat,
                    ];
            //   DB::table("tm_dataabsensi")
            // ->where("id_dataabsensi", $cek->id_dataabsensi)
            // ->insert([
            //           "id_karyawan"=>$id,
            //           "nik"=>$value['nik'],
            //           "status_absensi"=>$value['attendance_code'],
            //           "status_potong_makan"=>$status_potong_makan,
            //           "status_potong_gaji"=>$status_potong_gaji,
            //           "jam_masuk"=>$value['check_in'],
            //           "jam_keluar"=>$value['check_out'],
            //           "schedule_in"=>$value['schedule_in'],
            //           "schedule_out"=>$value['schedule_out'],
            //         ]);
          }
      }

      if($create_data != null && $updatedata != null){
          DB::table("tm_dataabsensi")
            ->insert($create_data);

          foreach ($id_dataabsensi as $key => $value) {
            # code...
            DB::table("tm_dataabsensi")
            ->where("id_dataabsensi", $value)
            ->update($updatedata[$key]);
          }

      }else if($create_data != null && $updatedata == null){
          DB::table("tm_dataabsensi")
            ->insert($create_data);
      }else{
          foreach ($id_dataabsensi as $key => $value) {
            # code...
            DB::table("tm_dataabsensi")
            ->where("id_dataabsensi", $value)
            ->update($updatedata[$key]);
          }
      }

    // if(array_unique($create_data) >0 &&)
    // if(count(array_unique($cekdata))>0){
    //   $ket[] = 'nik dengan nama dibawah belum terdaftar';
    //   return array("status"=>'failed', "response"=>array_merge($ket,$cekdata));
    // }else{
      return array("status"=>'true', "response"=>'Upload Data Berhasil');
    // }
    // return array("status"=>$status, "response"=>implode(',', $response));
        # ---------------

        # ---------------
        /* ----------
         Logs
         ----------------------- */  
         // $qLog       = new LogModel;
         //    # ---------------;
         // $qLog->createLog("CREATE DATAABSENSI (" . $qdataabsensi->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
  }

}
