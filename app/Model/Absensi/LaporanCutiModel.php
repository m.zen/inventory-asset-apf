<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\UserModel;
use Excel;

class LaporanCutiModel extends Model
{
    protected $table    = "p_karyawan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $grupid = Auth::user()->group_id;
        if($grupid == 1 || $grupid == 4){
            $WhereCondition = [];
        }else{
            $WhereCondition = [
                                ["a.nik", Auth::user()->nik],
                              ];
        }

        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where($WhereCondition)
                            ->where("a.aktif", 1)
                            ->orderBy("a.id_karyawan", "DESC");

        if(session()->has("SES_SEARCH_DATACUTI")) {
            $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATACUTI") . "%")
                ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_DATACUTI") . "%")
                  ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_DATACUTI") . "%")
                  ->orwhere("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_DATACUTI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();
        return $result;
    }

    function getIdKaryawan($id) {

        $query  = DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik","a.nama_karyawan","a.email")
                            ->where("nik", $id);
        $result = $query->first();

        return $result;
    }

    public function importcsv($request){
        $rows = Excel::load($request->file('file_excel'))->get();
        DD($rows);
        foreach ($rows as $key => $value) {
            DB::table("p_karyawan")
                             ->where("nik" ,$value['employee_id'])
                              ->update([
                                    "sisacuti"=> $value['total'],
                               ]);
        }
    
    /* ----------
         Logs
        ----------------------- */
        $qLog       = new LogModel;
        # ---------------;
        $qLog->createLog("update penggajian dari import excel", Auth::user()->id, $request);
    }
}