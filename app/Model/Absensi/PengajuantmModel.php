<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\User;
use DateTime;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;
use Spatie\ImageOptimizer\Optimizers\Pngquant;
use Spatie\Image\Image;
use Mail;

class PengajuantmModel extends Model
{
    protected $table    = "tm_timeoff";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $nik = Auth::user()->nik;
        $p_karyawan = $this->getIdKaryawan($nik);
        $query  = DB::table("tm_timeoff as a")
                            ->select("a.*","b.id_karyawan","b.nama_karyawan","c.nama_jenisabsen")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("tm_jenisabsen as c","c.id_jenisabsen","=","a.id_status_karyawan")
                            ->where("a.id_karyawan", $p_karyawan->id_karyawan)
                            ->orderBy("a.tgl_awal", "DESC");
        if(session()->has("SES_SEARCH_DATAABSENSI")) {
            $query->where("c.nama_jenisabsen", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();
        return $result;
    }

    function getIdKaryawan($id) {

        $query  = DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik","a.nama_karyawan","a.email","a.sisacuti")
                            ->where("nik", $id);
        $result = $query->first();

        return $result;
    }

    public function getDatesFromRange($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y/m/d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
    }

    public function getDatesFromRange2($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
    }

    public function createData($request) {
        $nik = Auth::user()->nik;
        $name = Auth::user()->name;
        $userid = Auth::user()->id;

        // 1. Cek Group Jam Masuk
        $qgrupabsen = DB::table("p_karyawan")
        //             // ->join("tm_dataabsensi","p_karyawan.id_karyawan","tm_dataabsensi.id_karyawan")
        ->join("m_departemen","p_karyawan.id_departemen","m_departemen.id_departemen")
        ->join("tm_grupabsen","m_departemen.id_grupabsen","tm_grupabsen.id_grupabsen")
        // ->select('id_karyawan','jam_masuk','jam_masuk2','jam_keluar','jam_keluar2','nik')
        ->where("nik", $nik)
        ->first();
        // dd($qgrupabsen);

        //2. Ambil keterangan Deskripsi dari jenis Absen
        $qjenisabsen = DB::table("tm_jenisabsen")->where("id_jenisabsen", $request->id_status_karyawan)->first();
        
        $inisial = $qjenisabsen->inisial;
        $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
        // $tglygtelahdiambil= [];
        // foreach ($rangedate as $keyrange => $valuedate) {
        //     # code...

        // $cekdataabsensi = DB::table("tm_dataabsensi")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan],['tgl_absensi',"=",$valuedate]])->select('tgl_absensi')->first();
        //     if($cekdataabsensi != null){
        //     $tglygtelahdiambil[] = ' '.$valuedate;    
        //     }
        // }

        // if(!empty($tglygtelahdiambil)){
        //     $response = ['Tanggal pengajuan sudah pernah diambil'.implode(',',$tglygtelahdiambil)];
        //     return array("status"=>'failed',"response"=>$response);
        // }else{

        //3. Cek Pengajuan user (Pernah/baru)
        $cektimeoff = DB::table("tm_timeoff")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan]])->select('tm_timeoff.tgl_awal','tm_timeoff.tgl_akhir')->get();
        $hitungtglsama = 0;
            if(count($cektimeoff) > 0){
                $i =0;
                foreach ($cektimeoff as $key => $value) {
                    # code...
                    $rangelastdate = $this->getDatesFromRange2($value->tgl_awal,$value->tgl_akhir);
                    foreach ($rangelastdate as $key2 => $variable) {
                        # code...
                    $tglygtelahdiajukan[$i] = $variable; 
                    $i++;
                    }
                }

                foreach ($rangedate as $key3 => $value3) {
                    # code...
                    if(in_array($value3, $tglygtelahdiajukan)){
                        $hitungtglsama = $hitungtglsama+1;
                        $tglsama[] = $value3;
                    }
                }

            } 

        if($hitungtglsama > 0){
            $ket[] = 'Tanggal pengajuan sudah pernah diambil dan klik edit untuk perubahan';
            return array("status"=>'failed', "response"=>array_merge($ket,$tglsama));
        }else{

            $pengajuan =  DB::table('p_karyawan')
                        ->join('tm_dataapproval','p_karyawan.id_jabatan','tm_dataapproval.id_jabatan')
                        ->select('p_karyawan.id_karyawan','tm_dataapproval.id_cabang_approval', 'tm_dataapproval.id_departemen_approval','tm_dataapproval.id_approvaller','tm_dataapproval.approvallevel','tm_dataapproval.id_jabatan',
                        'p_karyawan.id_cabang','p_karyawan.id_departemen','p_karyawan.id_jabatan')
                        ->where([['nik',$nik],['tm_dataapproval.id_cabang', $qgrupabsen->id_cabang],['tm_dataapproval.id_departemen', $qgrupabsen->id_departemen]])
                        ->get();
            // $jumlahapprovalygterdaftar = 0;
            
            // 4. Cek Approverer (Ada/tidak ada)
            $totalygaktif =0;
            foreach ($pengajuan as $key_pengajuan => $value_pengajuan) {
                # code...
                if($value_pengajuan->id_approvaller == 0 || $value_pengajuan->id_approvaller == '' || $value_pengajuan->id_approvaller == null){
                    $jabatan = DB::table('m_jabatan')->select('nama_jabatan')->where('id_jabatan', $value_pengajuan->id_jabatan)->first();
                    $nama_jabatan = $jabatan->nama_jabatan;
                    // $jumlahapprovalygterdaftar = $jumlahapprovalygterdaftar+1;
                    $pesan = ['Jabatan '.strtolower($nama_jabatan).' tidak bisa melakukan pengajuan time off karena tidak memiliki approvaller'];
                    return array("status"=>'failed', "response"=>$pesan);
                }else{
                    $qp_karyawan = DB::table('p_karyawan')->where([['id_cabang', (int)$value_pengajuan->id_cabang_approval],['id_departemen', $value_pengajuan->id_departemen_approval],['id_jabatan', $value_pengajuan->id_approvaller],["aktif",1]])->first();
                    // $jabatan = DB::table('m_jabatan')->select('nama_jabatan')->where('id_jabatan', $value_pengajuan->id_jabatan)->first();
                    // $nama_jabatan = $jabatan->nama_jabatan;
                    if($qp_karyawan == null){
                        $totalygaktif = $totalygaktif+0;                     
                    }else{
                        $totalygaktif = $totalygaktif+1;
                    }
                }
            }
            
            if($totalygaktif == 0){
                $pesan = ['Status dari Approvaller tidak aktif'];
                        return array("status"=>'failed', "response"=>$pesan); 
            }



            if(count($pengajuan)>0){

            $qdataabsensi              = new PengajuantmModel;
            # ---------------
            $grAbasen           = $qdataabsensi->getIdKaryawan(Auth::user()->nik);
            /**/
            $qdataabsensi->id_karyawan          = setString($grAbasen->id_karyawan);
            $qdataabsensi->id_status_karyawan   = setString($request->id_status_karyawan);
            $qdataabsensi->komentar             = setString($request->komentar);
            $qdataabsensi->tgl_awal             = setYMD($request->tgl_awal,"/");
            $qdataabsensi->tgl_akhir            = setYMD($request->tgl_akhir,"/");
            // $qdataabsensi->id_delegasi           = setString($request->id_delegasi);
            $qdataabsensi->user_id              = setString(Auth::user()->id);
            $qdataabsensi->create_at            = setString(date('Y-m-d H:i:s'));
            $qdataabsensi->update_at            = setString(date('Y-m-d H:i:s'));

            if($request->foto != null){
            // Simpan file gabar folder server
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'profile '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/foto_timeoff', $namaFile);
            // $compress_images = $this->compressImage('app/foto_timeoff/'.$name, $foto);
            //             $factory = new \ImageOptimizer\OptimizerFactory();
            // $optimizer = $factory->get();

            //5. Kompres file gambar
            $filepath = public_path().'/app/foto_timeoff/'.$namaFile;
            Image::load($filepath)
               ->optimize()
               ->save($filepath);
            // $optimizerChain = OptimizerChainFactory::create();

            // $optimizerChain->optimize(public_path('app/foto_timeoff/'.$namaFile));
            // $optimizer->optimize($filepath);
            // dd($optimizer);
            $qdataabsensi->foto                 = $namaFile;
            }
			if($request->foto2 != null){
                $foto = $request->file('foto2');
                $datenow = date('Y-m-d');
                $namaFile = 'profile '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
                $foto->move('app/foto_timeoff', $namaFile);
                $filepath = public_path().'/app/foto_timeoff/'.$namaFile;
                Image::load($filepath)
                   ->optimize()
                   ->save($filepath);
                $qdataabsensi->foto                 = $namaFile;
            }

            # ---------------
            $qdataabsensi->save();

            //6. Cek hari libur
            $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
            foreach ($hari_libur as $key => $value) {
                # code...
                $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
            }

            foreach ($rangedate as $key => $value) {
            # code...
            $hari = new DateTime($value);
            $no_hari = $hari->format('w');
            $ceklibur = in_array($value, $tgl_libur);
            if($no_hari != 0 && $ceklibur == false){
            if($no_hari == 6){
                $scheduli_in = $qgrupabsen->jam_masuk2;
                $scheduli_out = $qgrupabsen->jam_keluar2;
            }else{
                $scheduli_in = $qgrupabsen->jam_masuk;
                $scheduli_out = $qgrupabsen->jam_keluar;
            }
            
            // 7. 
            $cek = DB::table("tm_dataabsensi")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan],['tgl_absensi',"=",$value]])->first();
            if($cek == null){
                if($request->id_status_karyawan == 1){
                    $ket_masuk             = setString($request->komentar);
                    $datapengajuan = [
                            'id_karyawan'=> $qgrupabsen->id_karyawan,
                            'nik'=> $nik,
                            'tgl_absensi'=> $value,
                            'schedule_in' => $scheduli_in,
                            'schedule_out' => $scheduli_out,
                            'ket_masuk' => $ket_masuk,
                            'status_absensi' => 'Waiting',
                            'status_potong_makan' => 1,
                            'status_potong_gaji' => 0,
                            'keterangan' => $ket_masuk,
                            'user_id' => $userid,
                        ];
                }else if($request->id_status_karyawan == 7){
                    $ket_keluar             = setString($request->komentar);
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'ket_keluar' => $ket_keluar,
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'keterangan' => $ket_keluar,
                        'user_id' => $userid,
                    ];
                }else{
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'keterangan' => setString($request->komentar),
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'user_id' => $userid,
                    ];
                }
                DB::table("tm_dataabsensi")->insert($datapengajuan);
            }else{
                if($request->id_status_karyawan == 1){
                    $ket_masuk             = setString($request->komentar);
                    $datapengajuan = [
                            'id_karyawan'=> $qgrupabsen->id_karyawan,
                            'nik'=> $nik,
                            'tgl_absensi'=> $value,
                            'schedule_in' => $scheduli_in,
                            'schedule_out' => $scheduli_out,
                            'ket_masuk' => $ket_masuk,
                            'status_absensi' => 'Waiting',
                            'status_potong_makan' => 1,
                            'status_potong_gaji' => 0,
                            'keterangan' => $ket_masuk,
                            'user_id' => $userid,
                        ];
                }else if($request->id_status_karyawan == 7){
                    $ket_keluar             = setString($request->komentar);
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'ket_keluar' => $ket_keluar,
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'keterangan' => $ket_keluar,
                        'user_id' => $userid,
                    ];
                }else{
                    $datapengajuan = [
                        'id_karyawan'=> $qgrupabsen->id_karyawan,
                        'nik'=> $nik,
                        'tgl_absensi'=> $value,
                        'schedule_in' => $scheduli_in,
                        'schedule_out' => $scheduli_out,
                        'keterangan' => setString($request->komentar),
                        'status_absensi' => 'Waiting',
                        'status_potong_makan' => 1,
                        'status_potong_gaji' => 0,
                        'user_id' => $userid,
                    ];
                }
                // $cek->update($datapengajuan);
                DB::table("tm_dataabsensi")->where([
                                            ['id_karyawan',"=", $qgrupabsen->id_karyawan],
                                            ['tgl_absensi',"=",$value],
                                            ])->update($datapengajuan);
            }
            }
            }
            
            
            $newdata = DB::table("tm_timeoff")->where([['id_karyawan',"=",$grAbasen->id_karyawan],['tgl_awal',"=",setYMD($request->tgl_awal,"/")],['tgl_akhir',"=",setYMD($request->tgl_akhir,"/")]])->select('*')->first();
            foreach ($pengajuan as $keypengajuan => $valuepengajuan) {
                # code...
                // $cekpengajuanabsen = DB::table('tm_pengajuanabsen')->where([['id_tm',$newdata->id_tm],['order', '=',1] ,['statusapproval', '!=',0]])->first();
                // if($cekpengajuanabsen == null){
                // $datapengajuanabsen = [
                //         'id_tm'=> $newdata->id_tm,
                //         'id_karyawan'=> $valuepengajuan->id_approvaller,
                //         'order'=> $valuepengajuan->approvallevel,
                //         'statusapproval'=> 0,
                //         'create_at' => setString(date('Y-m-d H:i:s')),
                //         'update_at' => setString(date('Y-m-d H:i:s')),
                //       ];
                // DB::table("tm_pengajuanabsen")->insert($datapengajuanabsen);
                // }
                $qp_karyawan = DB::table('p_karyawan')->where([['id_cabang', (int)$valuepengajuan->id_cabang_approval],['id_departemen', $valuepengajuan->id_departemen_approval],['id_jabatan', $valuepengajuan->id_approvaller],["aktif",1]])->first();
                // dd($valuepengajuan->id_approvaller, $qp_karyawan, $pengajuan);
                // dd($qp_karyawan->id_karyawan);
                    # code...
                if($qp_karyawan != null){
                    $datapengajuanabsen = [
                        'id_tm'=> $newdata->id_tm,
                        'id_karyawan'=> $qp_karyawan->id_karyawan,
                        'order'=> $valuepengajuan->approvallevel,
                        'create_at' => setString(date('Y-m-d H:i:s')),
                        'update_at' => setString(date('Y-m-d H:i:s')),
                      ]; 
                DB::table("tm_pengajuanabsen")->insert($datapengajuanabsen);
                if($qp_karyawan->email != null || $qp_karyawan->email != ''){
                    $title = '(Approvaller) Pengajuan Time Off dari '.$grAbasen->nama_karyawan;
                    $pesan = 'Pesan Terkirim';
                    
                     $data = array(
                       'tgl' => date('d-m-Y',strtotime($newdata->tgl_awal)).' s/d '.date('d-m-Y', strtotime($newdata->tgl_akhir)),
                       'nama_karyawan' => $grAbasen->nama_karyawan,
                       'link' => 'https://hrbs.mitrafin.co.id/daftarapproval/index',
                       'email' => $qp_karyawan->email,
                       'nama_pengirim' => $qp_karyawan->nama_karyawan,
                     );

                     $this->sendEmailApprove($data,$pesan,$title);
                    }
					$title = '(Notifikasi) Pengajuan Time Off dari '.$grAbasen->nama_karyawan;
                    $pesan = 'Pesan Terkirim';
					$data = array(
                       'tgl' => date('d-m-Y',strtotime($newdata->tgl_awal)).' s/d '.date('d-m-Y', strtotime($newdata->tgl_akhir)),
                       'nama_karyawan' => $grAbasen->nama_karyawan,
                       'link' => 'https://hrbs.mitrafin.co.id/daftarapproval/index',
					   'email' => 's.winardi@buana-sejahtera.com',
                       'nama_pengirim' => 'SAMUEL WINARDI',
                     );

                     $this->sendEmailApprove($data,$pesan,$title);
                }
            }
            // SELECT a.id_karyawan, a.id_jabatan, b.id_approvaller,b.approvallevel FROM p_karyawan AS a LEFT JOIN tm_dataapproval AS b  ON (b.id_jabatan = a.id_jabatan) WHERE a.nik="2000.0816.0188"

            // SELECT id_karyawan,nama_karyawan FROM p_karyawan WHERE id_jabatan=14

            return array("status"=>'success');
            }else{
            $pesan = ['Approval Tidak Terdaftar'];
            return array("status"=>'failed', "response"=>$pesan);

            }
        }
    // }
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE DATAABSENSI (" . $qdataabsensi->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
    }

    public function updateData($request) {
        $name = Auth::user()->name;
        $userid = Auth::user()->id;
        $nik = Auth::user()->nik;
        $karyawan = DB::table('p_karyawan')->where('nik', $nik)->select('id_karyawan')->first();
        
        $ceklasttimeoff = DB::table("tm_timeoff")->where('id_tm', $request->id_tm)->first();
        $cekendtimeoff = DB::table("tm_timeoff")->where([['id_tm','!=', $request->id_tm],['id_karyawan',$karyawan->id_karyawan]])->get();
        $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
        $hitungtglsama = 0;
        if(count($cekendtimeoff) > 0){
            $i =0;
        foreach ($cekendtimeoff as $key => $value) {
            # code...
            $rangelastdate = $this->getDatesFromRange2($value->tgl_awal,$value->tgl_akhir);
            foreach ($rangelastdate as $key2 => $variable) {
                # code...
            $tglygtelahdiajukan[$i] = $variable; 
            $i++;
            }
        }
        foreach ($rangedate as $key3 => $value3) {
            # code...
            if(in_array($value3, $tglygtelahdiajukan)){
                $hitungtglsama = $hitungtglsama+1;
                $tglsama[] = $value3;
            }
        }

        }
        if($hitungtglsama > 0){
            $ket[] = 'Tanggal pengajuan sudah pernah diambil ';
            return array("status"=>'failed', "response"=>array_merge($ket,$tglsama));
        }else{
            $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'), setYMD($request->tgl_akhir,'/'));
            $tglygtelahdiambil= [];

        // foreach ($rangedate as $keyrange => $valuedate) {
        //     # code...

        // $cekdataabsensi = DB::table("tm_dataabsensi")->where([['id_karyawan',"=",$qgrupabsen->id_karyawan],['tgl_absensi',"=",$valuedate]])->select('tgl_absensi')->first();
        //     if($cekdataabsensi != null){
        //     $tglygtelahdiambil[] = ' '.$valuedate;    
        //     }
        // }

        // if(!empty($tglygtelahdiambil)){
        //     $response = ['Tanggal pengajuan sudah pernah diambil'.implode(',',$tglygtelahdiambil)];
        //     return array("status"=>'failed',"response"=>$response);
        // }else{

        $rangelast = $this->getDatesFromRange2($ceklasttimeoff->tgl_awal, $ceklasttimeoff->tgl_akhir);
        $tgl_awal = setYMD($request->tgl_awal,"/");
        $tgl_akhir = setYMD($request->tgl_akhir,"/");
        $rangenew = $this->getDatesFromRange2($tgl_awal, $tgl_akhir);
        // foreach ($rangenew as $key => $value) {
        //     # code...
        //     if(in_array($value, $rangelast)){
        //         $tglygsama[] = $value;
        //     }
        //     // else{
        //     //     $tglygsama[] = $value;
        //     // }
        // }
        $tglygdihapuss = [];
        $tglygdihapus = [];
        foreach ($rangelast as $key2 => $value2) {
            # code...
            if(in_array($value2, $rangenew)){
                $tglygdihapus[] = $value2;
            }
            else{
                $tglygdihapuss[] = $value2;
            }
        }

        if(count($tglygdihapuss)>0){
            foreach ($tglygdihapuss as $key3 => $value3) {
                # code...
                DB::table("tm_dataabsensi")->where([["id_karyawan", $ceklasttimeoff->id_karyawan],["tgl_absensi", $value3]])->delete();
            }
        }

        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
            foreach ($hari_libur as $keyy => $valuee) {
                # code...
                $tgl_libur[] = date('Y-m-d', strtotime($valuee->tgl_libur));
            }

        $qgrupabsen = DB::table("p_karyawan")
            //             // ->join("tm_dataabsensi","p_karyawan.id_karyawan","tm_dataabsensi.id_karyawan")
            ->join("m_departemen","p_karyawan.id_departemen","m_departemen.id_departemen")
            ->join("tm_grupabsen","m_departemen.id_grupabsen","tm_grupabsen.id_grupabsen")
            // ->select('id_karyawan','jam_masuk','jam_masuk2','jam_keluar','jam_keluar2','nik')
            ->where("nik", $nik)
            ->first();

        foreach ($rangenew as $key4 => $value4) {
            $cekvalue = DB::table("tm_dataabsensi")->where([["id_karyawan", $ceklasttimeoff->id_karyawan],["tgl_absensi", $value4]])->first();
            $qgrupabsen = DB::table("p_karyawan")->where("id_karyawan", $ceklasttimeoff->id_karyawan)
                     // ->join("tm_dataabsensi","p_karyawan.id_karyawan","tm_dataabsensi.id_karyawan")
                        ->join("m_departemen","p_karyawan.id_departemen","m_departemen.id_departemen")
                        ->join("tm_grupabsen","m_departemen.id_grupabsen","tm_grupabsen.id_grupabsen")
                        ->select('id_karyawan','jam_masuk','jam_masuk2','jam_keluar','jam_keluar2')
                        ->first();
            $qjenisabsen = DB::table("tm_jenisabsen")->where("id_jenisabsen", $request->id_status_karyawan)->first();
            

            $inisial = $qjenisabsen->inisial;
            if($cekvalue != null){
                $datapengajuan = [
                        'status_absensi' => setString($inisial),
                        // 'update_at' => setString(date('Y-m-d H:i:s')),
                      ];
                DB::table("tm_dataabsensi")
                             ->where([["id_karyawan", $cekvalue->id_karyawan],["tgl_absensi", $value4]])
                            ->update($datapengajuan);
            }else{
                $qkaryawan = DB::table("p_karyawan")->where("id_karyawan", $ceklasttimeoff->id_karyawan)->first();
                $hari = new DateTime($value4);
                $no_hari = $hari->format('w');
                $ceklibur = in_array($value4, $tgl_libur);
                if($no_hari != 7 && $ceklibur == false){
                    if($no_hari == 6){
                        $scheduli_in = $qgrupabsen->jam_masuk2;
                        $scheduli_out = $qgrupabsen->jam_keluar2;
                    }else{
                        $scheduli_in = $qgrupabsen->jam_masuk;
                        $scheduli_out = $qgrupabsen->jam_keluar;
                    }
                    $datapengajuan = [
                            'id_karyawan'=> $ceklasttimeoff->id_karyawan,
                            'nik'=> $qkaryawan->nik,
                            'tgl_absensi'=> $value4,
                            'schedule_in' => $scheduli_in,
                            'schedule_out' => $scheduli_out,
                            'jam_masuk' => '00:00:00',
                            'jam_keluar' => '00:00:00',
                            'status_absensi' => 'Waiting',
                            'user_id' => $userid,
                          ];
                    DB::table("tm_dataabsensi")->insert($datapengajuan);
                }
            }

        }
        if($request->file('foto') == null && $request->file('foto2') == null)
        {
            $gambarlama = DB::table('tm_timeoff')->where('id_tm', $request->id_tm)->first();
            if($request->id_status_karyawan == 2 || $request->id_status_karyawan == 4 || $request->id_status_karyawan == 20){
                $namaFile = $gambarlama->foto;
            }else{
                $namaFile = null;
            }
        }else if($request->file('foto') != null && $request->file('foto2') == null)
        {
            $foto = $request->file('foto');
            $datenow = date('Y-m-d');
            $namaFile = 'profile '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
            $namaFile2 = 'profile2 '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/foto_timeoff', $namaFile);
            $filepath = public_path().'/app/foto_timeoff/'.$namaFile;
            $filepath2 = public_path().'/app/foto_timeoff/'.$namaFile2;
            Image::load($filepath)
               ->optimize()
               ->save($filepath);
        }else if($request->file('foto') == null && $request->file('foto2') != null)
        {
            $foto = $request->file('foto2');
            $datenow = date('Y-m-d');
            $namaFile = 'profile '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
            $namaFile2 = 'profile2 '.$name.' '.$datenow . '.' . $foto->getClientOriginalExtension();
            $foto->move('app/foto_timeoff', $namaFile);
            $filepath = public_path().'/app/foto_timeoff/'.$namaFile;
            $filepath2 = public_path().'/app/foto_timeoff/'.$namaFile2;
            Image::load($filepath)
               ->optimize()
               ->save($filepath);
        }

         DB::table("tm_timeoff")
                             ->where("id_tm", $request->id_tm)
                            ->update([ "id_status_karyawan"=>setString($request->id_status_karyawan),
                                        "tgl_awal"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir"=>setYMD($request->tgl_akhir,"/"),
                                        // "id_delegasi"=>setString($request->id_delegasi),
                                        "foto"=>$namaFile,
                                        "komentar"=>setString($request->komentar),
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        return array("status"=>'success');
        }
    // }                        
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE DATAABSENSI(" . $request->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
    }

    public function compressImage($source_image, $image_info) {
    $image_info2 = $image_info->getClientOriginalExtension();
    // dd($image_info2, $image_info['mimeType']);
    if ($image_info['mimeType'] == 'jpeg') {
      $source_image = imagecreatefromjpeg($source_image);
      imagejpeg($source_image, $compress_image, 75);
    } elseif ($image_info['mime'] == 'image/gif') {
      $source_image = imagecreatefromgif($source_image);
      imagegif($source_image, $compress_image, 75);
    } elseif ($image_info['mime'] == 'image/png') {
      $source_image = imagecreatefrompng($source_image);
      imagepng($source_image, $compress_image, 6);
    }
    return $compress_image;
  }

    public function getProfile($id) {
        $query  = DB::table("tm_timeoff as a")
                            ->join('tm_pengajuanabsen as b','a.id_tm','b.id_tm')
                            ->join("tm_jenisabsen as c","c.id_jenisabsen","=","a.id_status_karyawan")
                            ->select("a.*",'b.tgl_approved','b.statusapproval', 'c.nama_jenisabsen')
                            ->where([["a.id_tm", $id]])
                            ->orderBy("id_karyawan", "DESC");

        $result = $query->get();

        return $result;
    }

    public function getDataAbsensi($id) {
        $query  = DB::table("tm_dataabsensi as a")
                            ->where([["id_karyawan", $id]])
                            ->orderBy("id_karyawan", "DESC");

        $result = $query->get();

        return $result;
    }

    public function removeData($request) {
        // dd($request->all());
        $getabsensi = DB::table("tm_timeoff")->where("id_tm", $request->id_tm)->first();
        $id_karyawan = $getabsensi->id_karyawan;
        $tglawal = $getabsensi->tgl_awal;
        $tglakhir = $getabsensi->tgl_akhir;
        $rangelast = $this->getDatesFromRange2($tglawal, $tglakhir);
        foreach ($rangelast as $key => $value) {
            # code...
            $cek_tm_dataabsensi = DB::table("tm_dataabsensi")->where([["id_karyawan", $id_karyawan],["tgl_absensi",$value]])->first();
            if($cek_tm_dataabsensi != null){
                $jam_masuk = $cek_tm_dataabsensi->jam_masuk;
                $jam_keluar = $cek_tm_dataabsensi->jam_keluar;
                                
                $grubabsen = DB::table("p_karyawan as a")
                            ->select("c.*")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->leftjoin("tm_grupabsen as c","c.id_grupabsen","=","b.id_grupabsen")
                            ->where("id_karyawan", $id_karyawan)
                            ->first();
                $hari = new DateTime($value);
                $kode_hari = $hari->format('w');
                if($kode_hari == 6){
                    $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk2));
                    $schedule_out = date('H:i', strtotime($grubabsen->jam_keluar2));
                }else{
                    $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk));
                    $schedule_out = date('H:i',strtotime($grubabsen->jam_keluar));
                }
                
                $status_absensi = "";
                if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                        $status_absensi = 'H';
                }else if($jam_masuk != null && $jam_keluar == null){
                    // $status_absensi = 'H/NCI';
                    if(date('h:i', strtotime($jam_masuk)) > $schedule_in ){
                        $status_absensi = 'H/LI/NCO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if(date('h:i', strtotime($jam_masuk)) <= $schedule_in ){
                        $status_absensi = 'H/NCO';
                    }
                    $status_potong_makan = 1;
                    $status_potong_gaji = 0;
                    $status_terlambat = 0;
                }else if($jam_masuk == null && $jam_keluar != null){
                    // $status_absensi = 'H/NCI';
                    if($jam_keluar < $schedule_out ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($jam_keluar > $schedule_out ){
                        $status_absensi = 'H/NCI';
                    }
                    $status_potong_makan = 1;
                    $status_potong_gaji = 0;
                    $status_terlambat = 0;
                }else if($jam_masuk != null && $jam_keluar != null){
                    if(($jam_masuk != '00:00:00' || $jam_masuk != '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                        if(date('h:i', strtotime($jam_masuk)) <= $schedule_in ){
                            $status_absensi = 'H/NCO';
                            $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }else if(date('h:i', strtotime($jam_masuk)) > $schedule_in ){
                            $status_absensi = 'H/LI/NCO';
                        }
                        $status_potong_makan = 1;
                        $status_potong_gaji = 0;
                        $status_terlambat = 0;
                    }else if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar != '00:00:00' || $jam_keluar != '00:00')){
                        if($jam_keluar <= $schedule_out ){
                            $status_absensi = 'H/NCI/EO';
                        }else if($jam_keluar > $schedule_out ){
                            $status_absensi = 'H/NCI';
                        }
                        $status_potong_makan = 1;
                        $status_potong_gaji = 0;
                        $status_terlambat = 0;
                                
                    }else{
                        // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                        if(date('h:i', strtotime($jam_masuk)) <= $schedule_in && $jam_keluar < $schedule_out){
                            $status_absensi = 'H/EO';
                            $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;

                            $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $jamkerja = $total_menit_telat;
                            if($jamkerja > 240){
                                $status_potong_makan = 0;
                                $status_potong_gaji = 0;
                                $status_terlambat = 0;
                            }else{
                                $status_potong_makan = 1;
                                $status_potong_gaji = 0;
                                $status_terlambat = 0;
                            }
                        }elseif(date('h:i', strtotime($jam_masuk)) <= $schedule_in && $jam_keluar >= $schedule_out){
                            $status_absensi = 'H';
                            $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                            $status_potong_makan = 0;
                            $status_potong_gaji = 0;
                            $status_terlambat = 0;
                        }elseif(date('h:i', strtotime($jam_masuk)) > $schedule_in && $jam_keluar < $schedule_out){
                            $status_absensi = 'H/LI/EO';
                            $status_terlambat = 0;

                            $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $jamkerja = $total_menit_telat;
                            if($jamkerja > 240){
                                $status_potong_makan = 0;
                                $status_potong_gaji = 0;
                            }else{
                                $status_potong_makan = 1;
                                $status_potong_gaji = 0;
                            }

                        }elseif(date('h:i', strtotime($jam_masuk)) > $schedule_in && $jam_keluar >= $schedule_out){
                            $status_absensi = 'H/LI';
                            $status_terlambat = 0;

                            $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $jamkerja = $total_menit_telat;
                            if($jamkerja > 240){
                                $status_potong_makan = 0;
                                $status_potong_gaji = 0;
                            }else{
                                $status_potong_makan = 1;
                                $status_potong_gaji = 0;
                            }
                        }
                    }
                }
                if($status_absensi != ""){
                DB::table("tm_dataabsensi")
                    ->where([["id_karyawan", $id_karyawan],["tgl_absensi", $value]])
                ->update([ 
                            "status_absensi"=>$status_absensi,
                            "status_potong_makan"=>$status_potong_makan,
                            "status_potong_gaji"=>$status_potong_gaji,
                            "status_terlambat"=>$status_terlambat,
                            "keterangan"=>'',
                            "ket_masuk"=>'',
                            "ket_keluar"=>'',
                        ]);
                }else{
                    DB::table("tm_dataabsensi")->where([["id_karyawan", $id_karyawan],["tgl_absensi",$value]])->delete();
                }
            }
        }
        
        DB::table("tm_timeoff")
                             ->where("id_tm", $request->id_tm)->delete();
        DB::table("tm_pengajuanabsen")
                             ->where("id_tm", $request->id_tm)->delete();
        
                           
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE DAFTARABSEN (" . $request->id_daftargaji . ") " . strtoupper($request->nama_daftargaji), Auth::user()->id, $request);
    }

    public function sendEmailApprove($data, $pesan, $title){
      $pesan = "Isi pesan";
      // $body = array('name'=>"Sam Jose", "body" => "body nih");
      Mail::send(['html' => 'email.notifrequest'], $data, function($message) use ($pesan, $title, $data){
              $message->to($data['email'], $data['nama_pengirim'])
                      ->subject($title)
                      ->setBody($pesan, 'text/plain');
              $message->from('mitrafinancesejahtera@gmail.com','Admin');
          });
    }
}
