<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use DateTime;
class DataabsensiModel extends Model
{
    protected $table    = "tm_dataabsensi";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $grupid = Auth::user()->group_id;
        if($grupid == 1){
            $WhereCondition = [];
            $query  = DB::table("tm_dataabsensi as a")
                            ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->where($WhereCondition)
                             ->orderBy("b.nama_karyawan", "ASC")
                             ->orderBy("id_karyawan", "ASC")
                            ->orderBy("tgl_absensi", "ASC")
                            ->orderBy("id_dataabsensi", "ASC");
        }else{
            if($grupid < 5){
                $p_karyawan = DB::table('p_karyawan as a')->where([['nik',Auth::user()->nik]])->first();
                $id_karyawan = $p_karyawan->id_karyawan;
                
                $WhereCondition =[
                                    ['d.id_cabang_approval',$p_karyawan->id_cabang],
                                    ['d.id_departemen_approval',$p_karyawan->id_departemen],
                                    ['d.id_approvaller',$p_karyawan->id_jabatan],  
                                    // ['b.id_departemen',$p_karyawan->id_departemen],  
                    ];
                $qdataapproval = DB::table('tm_dataapproval as d','b.id_approvaller','a.id_jabatan')
                                ->where($WhereCondition)->get()->toArray();
                
                $id_cabang = array_column($qdataapproval, 'id_cabang');
                $id_departemen = array_column($qdataapproval, 'id_departemen');
                $id_jabatan = array_column($qdataapproval, 'id_jabatan');
                $id_cabang_approval = array_column($qdataapproval, 'id_cabang_approval');
                $id_departemen_approval = array_column($qdataapproval, 'id_departemen_approval');
                $id_approvaller = array_column($qdataapproval, 'id_approvaller');

                array_push($id_cabang, $p_karyawan->id_cabang);
                array_push($id_departemen, $p_karyawan->id_departemen);
                array_push($id_jabatan,  $p_karyawan->id_jabatan);

                $query  =   DB::table("tm_dataabsensi as a")
                            ->select(
                                    "a.id_dataabsensi","a.tgl_absensi","a.id_karyawan","a.nik","a.jam_masuk", "a.jam_keluar","a.status_absensi","a.keterangan",
                                    "b.nama_karyawan","b.id_departemen",
                                    "c.nama_cabang"
                                    )
                    // ->addSelect("b.id_departemen")
                    ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                    ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                    ->leftjoin("tm_dataapproval as d","d.id_jabatan","b.id_jabatan")
                    // // ->where($WhereCondition)
                    // ->where("d.id_cabang_approval",$p_karyawan->id_cabang)
                    // ->where("d.id_departemen_approval",$p_karyawan->id_departemen)
                    // ->where("d.id_approvaller",$p_karyawan->id_jabatan)
                   ;
            }else{
                $WhereCondition = [
                                    ["b.nik", Auth::user()->nik],
                                ];
                $query  = DB::table("tm_dataabsensi as a")
                    ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang")
                    ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                    ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                    ->where($WhereCondition)
                        ->orderBy("id_karyawan", "ASC")
                    ->orderBy("tgl_absensi", "ASC")
                    ->orderBy("id_dataabsensi", "ASC");
                }
            }

        
        if(session()->has("SES_SEARCH_DATAABSENSI_FROMDATE") && session()->has("SES_SEARCH_DATAABSENSI_TODATE") && session()->has("SES_SEARCH_DATAABSENSI")) {
            if($grupid >1 && $grupid < 5){
                $query->whereBetween("a.tgl_absensi", array(session()->get("SES_SEARCH_DATAABSENSI_FROMDATE"), session()->get("SES_SEARCH_DATAABSENSI_TODATE")))
                ->where('b.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
				->whereIn("b.id_cabang",$id_cabang)
                ->whereIn("b.id_departemen",$id_departemen)
                ->whereIn("b.id_jabatan",$id_jabatan)
                // ->whereIn("d.id_cabang_approval",$id_cabang_approval)
                // ->whereIn("d.id_departemen_approval",$id_departemen_approval)
                ->orWhere("a.id_karyawan",$p_karyawan->id_karyawan)
                ->whereIn("d.id_approvaller",$id_approvaller)
                ->groupBy(
                        "a.id_dataabsensi","a.tgl_absensi","a.id_karyawan","a.nik","a.jam_masuk","a.jam_keluar","a.status_absensi","a.keterangan",
                        "b.nama_karyawan","b.id_departemen",
                        "c.nama_cabang"
                        )
                ->orderBy("b.nama_karyawan", "ASC")
                ->orderBy("a.tgl_absensi", "ASC");
            }else{
                $query->whereBetween('a.tgl_absensi', array(session()->get("SES_SEARCH_DATAABSENSI_FROMDATE"), session()->get("SES_SEARCH_DATAABSENSI_TODATE")))
                ->where('b.nama_karyawan', "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
                //->orwhere("c.nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
                ->orwhere("b.nik", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
                ;
            }
        }else if(session()->has("SES_SEARCH_DATAABSENSI_FROMDATE") && session()->has("SES_SEARCH_DATAABSENSI_TODATE")){
            if($grupid >1 && $grupid < 5){
                $query->whereBetween("a.tgl_absensi", array(session()->get("SES_SEARCH_DATAABSENSI_FROMDATE"), session()->get("SES_SEARCH_DATAABSENSI_TODATE")))
                ->whereIn("b.id_cabang",$id_cabang)
                ->whereIn("b.id_departemen",$id_departemen)
                ->whereIn("b.id_jabatan",$id_jabatan)
                // ->whereIn("d.id_cabang_approval",$id_cabang_approval)
                // ->whereIn("d.id_departemen_approval",$id_departemen_approval)
                ->orWhere("a.id_karyawan",$p_karyawan->id_karyawan)
                ->whereIn("d.id_approvaller",$id_approvaller)
                ->groupBy(
                        "a.id_dataabsensi","a.tgl_absensi","a.id_karyawan","a.nik","a.jam_masuk","a.jam_keluar","a.status_absensi","a.keterangan",
                        "b.nama_karyawan","b.id_departemen",
                        "c.nama_cabang"
                        )
                ->orderBy("b.nama_karyawan", "ASC")
                ->orderBy("a.tgl_absensi", "ASC");
            }else{
                $query->whereBetween("a.tgl_absensi", array(session()->get("SES_SEARCH_DATAABSENSI_FROMDATE"), session()->get("SES_SEARCH_DATAABSENSI_TODATE")));
            }
        }else if(session()->has("SES_SEARCH_DATAABSENSI")) {
            $query->where("b.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
            //->orwhere("c.nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
            //->orwhere("b.nik", "LIKE", "%" . session()->get("SES_SEARCH_DATAABSENSI") . "%")
        ;
        
        }
        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }
        $result = $query->get();
        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("tm_dataabsensi as a")
                            ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->where("id_dataabsensi", $id)
                            ->orderBy("nama_karyawan", "DESC");

        $result = $query->get();

        return $result;
    }    
 function getGroupAbsen($id) {

        $query  = DB::table("p_karyawan as a")
                            ->select("b.jam_masuk as jam_masuk","b.jam_keluar as jam_keluar")
                            ->leftjoin("tm_grupabsen as b","b.id_grupabsen","=","a.id_grupabsen")
                            ->where("id_karyawan", $id);
        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
     //    $qdataabsensi              = new DataabsensiModel;
     //    # ---------------
     //   // $qdataabsensi->kode       = setString($request->kode);
     // //   $qdataabsensi->nik              = setString($request->nik);


     //    $grAbasen           = $qdataabsensi->getGroupAbsen($request->id_karyawan)->first;
        $status_absensi = $request->status_absensi;
        $menit_terlambat = 0;
        $status_terlambat = 0;
        $jam_masuk = null;
        $jam_keluar = null;
        $grubabsen = DB::table("p_karyawan as a")
                            ->select("c.*","a.nik")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->leftjoin("tm_grupabsen as c","c.id_grupabsen","=","b.id_grupabsen")
                            ->where("id_karyawan", $request->id_karyawan)
                            ->first();
        $nik = $grubabsen->nik;
        $hari = new DateTime(setYMD($request->tgl_absensi,"/"));
        $kode_hari = $hari->format('w');
            if($kode_hari == 6){
                $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk2));
                $schedule_out = date('H:i', strtotime($grubabsen->jam_keluar2));
            }else{
                $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk));
                $schedule_out = date('H:i',strtotime($grubabsen->jam_keluar));
            }
        if($status_absensi == 'H')
        {
            $jam_masuk = $request->jam_masuk;
            $jam_keluar = $request->jam_keluar;
            
            
            if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                    $status_absensi = 'H';
                    $jam_masuk = null;
                    $jam_keluar = null;
            }else if($jam_masuk != null && $jam_keluar == null){
                // $status_absensi = 'H/NCI';
                if($jam_masuk > $schedule_in ){
                    $status_absensi = 'H/LI/NCO';
                    $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                    $nilai_jam = $selisih->h;
                    $nilai_menit = $selisih->i;
                    $total_menit_telat = $nilai_jam*60+$nilai_menit;
                    $menit_terlambat = $total_menit_telat;
                }else if($jam_masuk <= $schedule_in ){
                    $status_absensi = 'H/NCO';
                }
            }else if($jam_masuk == null && $jam_keluar != null){
                // $status_absensi = 'H/NCI';
                if($jam_keluar < $schedule_out ){
                    $status_absensi = 'H/NCI/EO';
                }else if($jam_keluar > $schedule_out ){
                    $status_absensi = 'H/NCI';
                }
            }else if($jam_masuk != null && $jam_keluar != null){
                if(($jam_masuk != '00:00:00' || $jam_masuk != '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                    if($jam_masuk <= $schedule_in ){
                        $status_absensi = 'H/NCO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if($jam_masuk > $schedule_in ){
                        $status_absensi = 'H/LI/NCO';
                    }

                }else if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar != '00:00:00' || $jam_keluar != '00:00')){
                    if($jam_keluar < $schedule_out ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($jam_keluar >= $schedule_out ){
                        $status_absensi = 'H/NCI';
                    }
                }else{
                    // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                    if($jam_masuk <= $schedule_in && $jam_keluar < $schedule_out){
                        $status_absensi = 'H/EO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }elseif($jam_masuk <= $schedule_in && $jam_keluar >= $schedule_out){
                        $status_absensi = 'H';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }elseif($jam_masuk > $schedule_in && $jam_keluar < $schedule_out){
                        $status_absensi = 'H/LI/EO';
                        $status_terlambat = 1;
                    }elseif($jam_masuk > $schedule_in && $jam_keluar >= $schedule_out){
                        $status_absensi = 'H/LI';
                        $status_terlambat = 1;
                    }
                }
            }
        }
        $inisial =['DL','CT','I','S','A','H','H/NCI/EO','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC','IK','OD'];
              $val_stat_makan = [0,1,1,1,1,0,1,1,0,0,0,1,1,0,1,1,0];
              $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0];
              $status_potong_gaji = 0;
              $status_potong_makan = 0;
        foreach ($inisial as $keyinisial => $valueinisial) {
                # code...
                if($status_absensi == $valueinisial){
                  $status_potong_gaji = $val_stat_gaji[$keyinisial];
                  $status_potong_makan = $val_stat_makan[$keyinisial];
                }
              }
        $id = DB::table("tm_dataabsensi")
                            ->insertGetId([ 
                                        "id_karyawan"=>$request->id_karyawan,
                                        "nik"=>$nik,
                                        "tgl_absensi"=>setYMD($request->tgl_absensi,"/"),
                                        "status_absensi"=>$status_absensi,
                                        "schedule_in"=>$schedule_in,
                                        "schedule_out"=>$schedule_out,
                                        "jam_masuk"=>$jam_masuk,
                                        "jam_keluar"=>$jam_keluar,
                                        "status_potong_makan"=>$status_potong_makan,
                                        "status_potong_gaji"=>$status_potong_gaji,
                                        "status_terlambat"=>$status_terlambat,
                                        "menit_terlambat"=>$menit_terlambat,
                                        "user_id"=>setString(Auth::user()->id),
                                         "create_at"=>setString(date('Y-m-d H:i:s')),   
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE DATAABSENSI (" . $id . ") " . strtoupper($request->status_absensi), Auth::user()->id, $request);
    }

    public function updateData($request) {
        $status_absensi = $request->status_absensi;
        $menit_terlambat = 0;
        $status_terlambat = 0;
        $jam_masuk = null;
        $jam_keluar = null;
        $grubabsen = DB::table("p_karyawan as a")
                            ->select("c.*")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->leftjoin("tm_grupabsen as c","c.id_grupabsen","=","b.id_grupabsen")
                            ->where("id_karyawan", $request->id_karyawan)
                            ->first();
            $hari = new DateTime(setYMD($request->tgl_absensi,"/"));
        $kode_hari = $hari->format('w');
            if($kode_hari == 6){
                $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk2));
                $schedule_out = date('H:i', strtotime($grubabsen->jam_keluar2));
            }else{
                $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk));
                $schedule_out = date('H:i',strtotime($grubabsen->jam_keluar));
            }
        if($status_absensi == 'H')
        {
            $jam_masuk = $request->jam_masuk;
            $jam_keluar = $request->jam_keluar;
            
            
            if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                    $status_absensi = 'H';
                    $jam_masuk = null;
                    $jam_keluar = null;
            }else if($jam_masuk != null && $jam_keluar == null){
                // $status_absensi = 'H/NCI';
                if($jam_masuk > $schedule_in ){
                    $status_absensi = 'H/LI/NCO';
                    $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                    $nilai_jam = $selisih->h;
                    $nilai_menit = $selisih->i;
                    $total_menit_telat = $nilai_jam*60+$nilai_menit;
                    $menit_terlambat = $total_menit_telat;
                }else if($jam_masuk <= $schedule_in ){
                    $status_absensi = 'H/NCO';
                }
            }else if($jam_masuk == null && $jam_keluar != null){
                // $status_absensi = 'H/NCI';
                if($jam_keluar < $schedule_out ){
                    $status_absensi = 'H/NCI/EO';
                }else if($jam_keluar >= $schedule_out ){
                    $status_absensi = 'H/NCI';
                }
            }else if($jam_masuk != null && $jam_keluar != null){
                if(($jam_masuk != '00:00:00' || $jam_masuk != '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                    if($jam_masuk <= $schedule_in ){
                        $status_absensi = 'H/NCO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if($jam_masuk > $schedule_in ){
                        $status_absensi = 'H/LI/NCO';
                    }

                }else if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar != '00:00:00' || $jam_keluar != '00:00')){
                    if($jam_keluar < $schedule_out ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($jam_keluar >= $schedule_out ){
                        $status_absensi = 'H/NCI';
                    }
                }else{
                    // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                    if($jam_masuk <= $schedule_in && $jam_keluar < $schedule_out){
                        $status_absensi = 'H/EO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }elseif($jam_masuk <= $schedule_in && $jam_keluar >= $schedule_out){
                        $status_absensi = 'H';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }elseif($jam_masuk > $schedule_in && $jam_keluar < $schedule_out){
                        $status_absensi = 'H/LI/EO';
                        $status_terlambat = 1;
                    }elseif($jam_masuk > $schedule_in && $jam_keluar >= $schedule_out){
                        $status_absensi = 'H/LI';
                        $status_terlambat = 1;
                    }
                }
            }
        }else {
            $jam_masuk = $request->jam_masuk == "00:00" ? null : $request->jam_masuk;
            $jam_keluar = $request->jam_keluar == "00:00" ? null : $request->jam_keluar;
        }

        $inisial =['DL','CT','I','S','A','H','H/NCI/EO','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC','IK','OD','KR'];
              $val_stat_makan = [0,1,1,1,1,0,1,1,0,0,0,1,1,0,1,1,0,0];
              $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0];
              $status_potong_gaji = 0;
              $status_potong_makan = 0;
        foreach ($inisial as $keyinisial => $valueinisial) {
                # code...
                if($status_absensi == $valueinisial){
                  $status_potong_gaji = $val_stat_gaji[$keyinisial];
                  $status_potong_makan = $val_stat_makan[$keyinisial];
                }
              }

         DB::table("tm_dataabsensi")
                             ->where("id_dataabsensi", $request->id_dataabsensi)
                            ->update([ 
                                        "tgl_absensi"=>setYMD($request->tgl_absensi,"/"),
                                        "schedule_in"=>$schedule_in,
                                        "schedule_out"=>$schedule_out,
                                        "status_absensi"=>$status_absensi,
                            	    	"jam_masuk"=>$jam_masuk,
                            	    	"jam_keluar"=>$jam_keluar,
                            	    	"status_potong_makan"=>$status_potong_makan,
                                        "status_potong_gaji"=>$status_potong_gaji,
                                        "status_terlambat"=>$status_terlambat,
                                        "menit_terlambat"=>$menit_terlambat,
                                        "user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE DATAABSENSI(" . $request->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("tm_dataabsensi")
                             ->where("id_dataabsensi", $request->id_dataabsensi)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE DATAABSENSI (" . $request->id_dataabsensi . ") " . strtoupper($request->nama_dataabsensi), Auth::user()->id, $request);
    }
	public function getDatesFromRange2($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }
}
