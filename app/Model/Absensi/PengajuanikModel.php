<?php

namespace App\Model\Absensi;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\MasterModel;
use Datetime;

class PengajuanikModel extends Model
{
    protected $table    = "m_profilesystem";
    public $timestamps= false ;

    public function getProfile() {
        $query  = DB::table("m_profilesystem")
                            ->select("bulanpayroll","tahunpayroll","tgl_payrollawal","tgl_payrollakhir","tgl_absenawal","tgl_absenakhir");
                           
        $result = $query->get();

        return $result;
    } 

    public function prosesData($request) 
    {
        $tgl_ijin_khusus        = date('Y-m-d', strtotime($request->tgl_ijin_khusus));
        $cabang                 = $request->id_cabang;
        $karyawan               = $request->id_karyawan;
        // $tgl_absen_akhir      = $request->Tgl_Absen_akhir;
        
        $hari = new DateTime($tgl_ijin_khusus);
        $kode_hari = $hari->format('w');

        $key = array_search('0', $karyawan);
        if (false !== $key) {
            unset($karyawan[$key]);
        }
        $tgl = setYMD($request->tgl_ijin_khusus,"/");
        if(count($karyawan) > 0){
            foreach($karyawan as $value){
                $grubabsen = DB::table("p_karyawan as a")
                            ->select("a.nik","c.*")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->leftjoin("tm_grupabsen as c","c.id_grupabsen","=","b.id_grupabsen")
                            ->where("id_karyawan", $value)
                            ->first();
                $hari = new DateTime($tgl);
                $kode_hari = $hari->format('w');
                if($kode_hari == 6){
                    $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk2));
                    $schedule_out = date('H:i', strtotime($grubabsen->jam_keluar2));
                }else{
                    $schedule_in = date('H:i', strtotime($grubabsen->jam_masuk));
                    $schedule_out = date('H:i',strtotime($grubabsen->jam_keluar));
                }
                $cek = DB::table('tm_dataabsensi')->where([['id_karyawan', $value], ['tgl_absensi',$tgl]])->first();
                if($cek == null){
                    DB::table("tm_dataabsensi")
                    ->insert([ 
                            "nik"=> $grubabsen->nik,
                            "id_karyawan"=> $value,
                            "tgl_absensi"=> $tgl,
                            "status_absensi"=> "IK",
                            "schedule_in"=> $schedule_in,
                            "schedule_out"=> $schedule_out,
                            "status_potong_makan"=>"0",
                            "status_potong_gaji"=>"0",
                            ]);
                }else{
                    DB::table("tm_dataabsensi")
                        ->where('id_dataabsensi', $cek->id_dataabsensi)
                        ->update([ 
                            "tgl_absensi"=> $tgl,
                            "status_absensi"=> "IK",
                            "schedule_in"=> $schedule_in,
                            "schedule_out"=> $schedule_out,
                            "status_potong_makan"=>"0",
                            "status_potong_gaji"=>"0",
                            ]);
                }
            }
        }

        $qLog       = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE PENGAJUAN IJIN KHUSUS TANGGAL(" . $tgl . ")  UNTUK CABANG" . strtoupper($cabang), Auth::user()->id, $request);
    }

    public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }

    public function getDatesFromRange2($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }
       
}
