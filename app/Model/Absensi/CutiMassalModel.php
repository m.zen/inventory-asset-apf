<?php

namespace App\Model\Absensi;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\MasterModel;
use Datetime;

class CutiMassalModel extends Model
{
    protected $table    = "m_profilesystem";
    public $timestamps= false ;

    public function getProfile() {
        $query  = DB::table("m_profilesystem")
                            ->select("bulanpayroll","tahunpayroll","tgl_payrollawal","tgl_payrollakhir","tgl_absenawal","tgl_absenakhir");
                           
        $result = $query->get();

        return $result;
    } 

    public function prosesData($request) 
    {
        $Tgl_Cuti_Massal       = $request->Tgl_Cuti_Massal;
        $Bulan_ke              = $request->Bulan_ke;
        $Tahun                 = $request->Tahun;
        // $tgl_absen_akhir      = $request->Tgl_Absen_akhir;
        $range_absen           = str_replace(' ', '', $Tgl_Cuti_Massal);	
        $range_absen           = explode(',', $range_absen);
        
        for($i = 1; $i <= 31; $i++){
            $no_date[] =$i;
        }
        foreach($range_absen as $value){
            if(in_array($value, $no_date)){
                $tgl_absen[] = date('Y-m-d', strtotime($Tahun.'-'.$Bulan_ke.'-'.$value));
            }
        }
        // dd($range_absen, $tgl_absen);
        $MasterModel = new MasterModel;
        $grubabsen = DB::table("p_karyawan as a")
                    ->select("a.id_karyawan as id", "a.nik as nik","a.sisacuti", "a.tgl_masuk", "a.tgl_batas_cuti", "c.*")
                    ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                    ->leftjoin("tm_grupabsen as c","c.id_grupabsen","=","b.id_grupabsen")
                    ->where([["aktif", 1]])
                    ->get();

        $tgl_sekarang = date('Y-m-d');
        
        foreach($grubabsen as $index => $value2){
            $tgl_masuk         = $value2->tgl_masuk;
            $interval          = date_diff(date_create($tgl_masuk),date_create($tgl_sekarang));
            $totalhari         = $interval->format("%a");
            $persen_tahun      = $totalhari/365;
            $int_tahun         = (int) $persen_tahun;
            // $interval 
            if($persen_tahun < 1){
                $jumlahtahun = $int_tahun+1;
                $jumlahtahun2 = $jumlahtahun+1;
            }else{
                $jumlahtahun = $int_tahun;
                $jumlahtahun2 = $jumlahtahun+1;
            }
            $tgl_awalcuti = date('Y-m-d', strtotime($value2->tgl_masuk. ' + '.$jumlahtahun.' years'));
            $tgl_akhircuti = date('Y-m-d', strtotime($value2->tgl_masuk. ' + '.$jumlahtahun2.' years'));
            $beetween_awal_akhir_cuti = $this->getDatesFromRange2($tgl_awalcuti, $tgl_akhircuti);
            foreach($tgl_absen  as $indextgl => $valuetgl){
                $hari = new DateTime($valuetgl);
                $kode_hari = $hari->format('w');
                
                if($kode_hari == 6){
                $schedule_in = date('H:i', strtotime($value2->jam_masuk2));
                $schedule_out = date('H:i', strtotime($value2->jam_keluar2));
                }else{
                $schedule_in = date('H:i', strtotime($value2->jam_masuk));
                $schedule_out = date('H:i',strtotime($value2->jam_keluar));
                }

                $cek_absensi =  DB::table('tm_dataabsensi')->where([['id_Karyawan', $value2->id], ['tgl_absensi', $valuetgl]])->first();
                if($cek_absensi == null){
                    $tgl_batas_cuti_akhir = $value2->tgl_batas_cuti;
                    $tgl_batas_cuti_awal = date('Y-m-d', strtotime($tgl_batas_cuti_akhir. ' - 1 years'));
                    $beetween_batas_awal_akhir_cuti = $this->getDatesFromRange2($tgl_batas_cuti_awal, $tgl_batas_cuti_akhir);
                    $p_karyawan_baru = DB::table('p_karyawan')->where('id_karyawan', $value2->id)->select('sisacuti','tgl_batas_cuti')->first();
                            
                    // $nilaicuti = $value2->sisacuti;
                    // $sisacuti = $nilaicuti - 1;
                    // if($sisacuti < 0){
                    //     DB::table("tm_dataabsensi")
                    //         ->insertGetId([ 
                    //                     "id_karyawan"=>$value2->id,
                    //                     "nik"=> $value2->nik,
                    //                     "tgl_absensi"=> $valuetgl,
                    //                     "status_absensi"=> "CT",
                    //                     "schedule_in"=>$schedule_in,
                    //                     "schedule_out"=>$schedule_out,
                    //                     "status_potong_makan"=>"1",
                    //                     "status_potong_gaji"=>"1",
                    //                   ]);
                    // }else{
                    //     DB::table("tm_dataabsensi")
                    //         ->insertGetId([ 
                    //                     "id_karyawan"=>$value2->id,
                    //                     "nik"=> $value2->nik,
                    //                     "tgl_absensi"=> $valuetgl,
                    //                     "status_absensi"=> "CT",
                    //                     "schedule_in"=>$schedule_in,
                    //                     "schedule_out"=>$schedule_out,
                    //                     "status_potong_makan"=>"0",
                    //                     "status_potong_gaji"=>"0",
                    //                   ]);
                    // }

                    // DB::table('p_karyawan')->where('id_karyawan',$value2->id)->update(["sisacuti"=>$sisacuti]);

                        if(in_array($valuetgl, $beetween_batas_awal_akhir_cuti)){
                            $nilai_cuti = $p_karyawan_baru->sisacuti;
                            $sisacuti = $nilai_cuti-1;
                            DB::table('p_karyawan')
                            ->where('id_karyawan', $value2->id)
                            ->update([
                                    "sisacuti" => $sisacuti,
                                    ]);
                            if($sisacuti < 0){
                                DB::table("tm_dataabsensi")
                                ->insertGetId([ 
                                            "id_karyawan"=>$value2->id,
                                            "nik"=> $value2->nik,
                                            "tgl_absensi"=> $valuetgl,
                                            "status_absensi"=> "CT",
                                            "schedule_in"=>$schedule_in,
                                            "schedule_out"=>$schedule_out,
                                            "status_potong_makan"=>"1",
                                            "status_potong_gaji"=>"1",
                                            ]);
                            }else{
                                    DB::table("tm_dataabsensi")
                                        ->insertGetId([ 
                                                    "id_karyawan"=>$value2->id,
                                                    "nik"=> $value2->nik,
                                                    "tgl_absensi"=> $valuetgl,
                                                    "status_absensi"=> "CT",
                                                    "schedule_in"=>$schedule_in,
                                                    "schedule_out"=>$schedule_out,
                                                    "status_potong_makan"=>"0",
                                                    "status_potong_gaji"=>"0",
                                                    ]);
                            }
                        }else{
                            if($valuetgl < $tgl_batas_cuti_awal){
                                $nilai_cuti = $p_karyawan_baru->sisacuti;
                                $sisacuti = $nilai_cuti-1;

                                DB::table('p_karyawan')
                                    ->where('id_karyawan', $value2->id)
                                    ->update([
                                            "tgl_batas_cuti" => $tgl_akhircuti,
                                            "sisacuti" => $sisacuti,
                                            ]);

                                if($sisacuti < 0){
                                    DB::table("tm_dataabsensi")
                                    ->insertGetId([ 
                                                "id_karyawan"=>$value2->id,
                                                "nik"=> $value2->nik,
                                                "tgl_absensi"=> $valuetgl,
                                                "status_absensi"=> "CT",
                                                "schedule_in"=>$schedule_in,
                                                "schedule_out"=>$schedule_out,
                                                "status_potong_makan"=>"1",
                                                "status_potong_gaji"=>"1",
                                                ]);
                                }else{
                                        DB::table("tm_dataabsensi")
                                            ->insertGetId([ 
                                                        "id_karyawan"=>$value2->id,
                                                        "nik"=> $value2->nik,
                                                        "tgl_absensi"=> $valuetgl,
                                                        "status_absensi"=> "CT",
                                                        "schedule_in"=>$schedule_in,
                                                        "schedule_out"=>$schedule_out,
                                                        "status_potong_makan"=>"0",
                                                        "status_potong_gaji"=>"0",
                                                        ]);
                                }
                            }else{
                                    
                                    if($valuetgl > $tgl_batas_cuti_akhir){
                                    $nilai_cuti = $p_karyawan_baru->sisacuti;
                                    $sisacuti = $nilai_cuti-1;
                            
                                    DB::table('p_karyawan')
                                    ->where('id_karyawan', $value2->id)
                                    ->update([
                                            "tgl_batas_cuti" => $tgl_akhircuti,
                                            "sisacuti" => $sisacuti,
                                            ]);
                                    if($sisacuti < 0){
                                        DB::table("tm_dataabsensi")
                                        ->insertGetId([ 
                                                    "id_karyawan"=>$value2->id,
                                                    "nik"=> $value2->nik,
                                                    "tgl_absensi"=> $valuetgl,
                                                    "status_absensi"=> "CT",
                                                    "schedule_in"=>$schedule_in,
                                                    "schedule_out"=>$schedule_out,
                                                    "status_potong_makan"=>"1",
                                                    "status_potong_gaji"=>"1",
                                                    ]);
                                    }else{
                                            DB::table("tm_dataabsensi")
                                                ->insertGetId([ 
                                                            "id_karyawan"=>$value2->id,
                                                            "nik"=> $value2->nik,
                                                            "tgl_absensi"=> $valuetgl,
                                                            "status_absensi"=> "CT",
                                                            "schedule_in"=>$schedule_in,
                                                            "schedule_out"=>$schedule_out,
                                                            "status_potong_makan"=>"0",
                                                            "status_potong_gaji"=>"0",
                                                            ]);
                                    }
                                }else{
                                    $nilai_cuti = $p_karyawan_baru->sisacuti;
                                    $sisacuti = $nilai_cuti-1;
                                    
                                    DB::table('p_karyawan')
                                    ->where('id_karyawan', $value2->id)
                                    ->update([
                                            "sisacuti" => $sisacuti,
                                            ]);

                                    if($sisacuti < 0){
                                        DB::table("tm_dataabsensi")
                                        ->insertGetId([ 
                                                    "id_karyawan"=>$value2->id,
                                                    "nik"=> $value2->nik,
                                                    "tgl_absensi"=> $valuetgl,
                                                    "status_absensi"=> "CT",
                                                    "schedule_in"=>$schedule_in,
                                                    "schedule_out"=>$schedule_out,
                                                    "status_potong_makan"=>"1",
                                                    "status_potong_gaji"=>"1",
                                                    ]);
                                    }else{
                                            DB::table("tm_dataabsensi")
                                                ->insertGetId([ 
                                                            "id_karyawan"=>$value2->id,
                                                            "nik"=> $value2->nik,
                                                            "tgl_absensi"=> $valuetgl,
                                                            "status_absensi"=> "CT",
                                                            "schedule_in"=>$schedule_in,
                                                            "schedule_out"=>$schedule_out,
                                                            "status_potong_makan"=>"0",
                                                            "status_potong_gaji"=>"0",
                                                            ]);
                                    }
                                }
                                
                            }
                            
                        }

                }else{
                    //jika tidak berulang atau status absensi sebelumnya sudah CT atau IDC
                    if(!in_array($cek_absensi->status_absensi, ['CT', 'IDC'])){
                    $tgl_batas_cuti_akhir = $value2->tgl_batas_cuti;
                    $tgl_batas_cuti_awal = date('Y-m-d', strtotime($tgl_batas_cuti_akhir. ' - 1 years'));
                    $beetween_batas_awal_akhir_cuti = $this->getDatesFromRange2($tgl_batas_cuti_awal, $tgl_batas_cuti_akhir);
                    $p_karyawan_baru = DB::table('p_karyawan')->where('id_karyawan', $value2->id)->select('sisacuti','tgl_batas_cuti')->first();
                            
                        if(in_array($valuetgl, $beetween_batas_awal_akhir_cuti)){
                            $nilai_cuti = $p_karyawan_baru->sisacuti;
                            $sisacuti = $nilai_cuti-1;
                            DB::table('p_karyawan')
                            ->where('id_karyawan', $value2->id)
                            ->update([
                                    "sisacuti" => $sisacuti,
                                    ]);
                            if($sisacuti < 0){
                                DB::table("tm_dataabsensi")
                                ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                ->update([ 
                                            "id_karyawan"=>$value2->id,
                                            "nik"=> $value2->nik,
                                            "tgl_absensi"=> $valuetgl,
                                            "status_absensi"=> "CT",
                                            "schedule_in"=>$schedule_in,
                                            "schedule_out"=>$schedule_out,
                                            "status_potong_makan"=>"1",
                                            "status_potong_gaji"=>"1",
                                            ]);
                            }else{
                                    DB::table("tm_dataabsensi")
                                    ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                    ->update([
                                                    "id_karyawan"=>$value2->id,
                                                    "nik"=> $value2->nik,
                                                    "tgl_absensi"=> $valuetgl,
                                                    "status_absensi"=> "CT",
                                                    "schedule_in"=>$schedule_in,
                                                    "schedule_out"=>$schedule_out,
                                                    "status_potong_makan"=>"0",
                                                    "status_potong_gaji"=>"0",
                                                    ]);
                            }
                        }else{
                            if($valuetgl < $tgl_batas_cuti_awal){
                                $nilai_cuti = $p_karyawan_baru->sisacuti;
                                $sisacuti = $nilai_cuti-1;

                                DB::table('p_karyawan')
                                    ->where('id_karyawan', $value2->id)
                                    ->update([
                                            "tgl_batas_cuti" => $tgl_akhircuti,
                                            "sisacuti" => $sisacuti,
                                            ]);

                                if($sisacuti < 0){
                                    DB::table("tm_dataabsensi")
                                    ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                    ->update([
                                                "id_karyawan"=>$value2->id,
                                                "nik"=> $value2->nik,
                                                "tgl_absensi"=> $valuetgl,
                                                "status_absensi"=> "CT",
                                                "schedule_in"=>$schedule_in,
                                                "schedule_out"=>$schedule_out,
                                                "status_potong_makan"=>"1",
                                                "status_potong_gaji"=>"1",
                                                ]);
                                }else{
                                        DB::table("tm_dataabsensi")
                                        ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                        ->update([
                                                        "id_karyawan"=>$value2->id,
                                                        "nik"=> $value2->nik,
                                                        "tgl_absensi"=> $valuetgl,
                                                        "status_absensi"=> "CT",
                                                        "schedule_in"=>$schedule_in,
                                                        "schedule_out"=>$schedule_out,
                                                        "status_potong_makan"=>"0",
                                                        "status_potong_gaji"=>"0",
                                                        ]);
                                }
                            }else{
                                    
                                    if($valuetgl > $tgl_batas_cuti_akhir){
                                    $nilai_cuti = $p_karyawan_baru->sisacuti;
                                    $sisacuti = $nilai_cuti-1;
                            
                                    DB::table('p_karyawan')
                                    ->where('id_karyawan', $value2->id)
                                    ->update([
                                            "tgl_batas_cuti" => $tgl_akhircuti,
                                            "sisacuti" => $sisacuti,
                                            ]);
                                    if($sisacuti < 0){
                                        DB::table("tm_dataabsensi")
                                        ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                        ->update([ 
                                                    "id_karyawan"=>$value2->id,
                                                    "nik"=> $value2->nik,
                                                    "tgl_absensi"=> $valuetgl,
                                                    "status_absensi"=> "CT",
                                                    "schedule_in"=>$schedule_in,
                                                    "schedule_out"=>$schedule_out,
                                                    "status_potong_makan"=>"1",
                                                    "status_potong_gaji"=>"1",
                                                    ]);
                                    }else{
                                            DB::table("tm_dataabsensi")
                                            ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                            ->update([ 
                                                            "id_karyawan"=>$value2->id,
                                                            "nik"=> $value2->nik,
                                                            "tgl_absensi"=> $valuetgl,
                                                            "status_absensi"=> "CT",
                                                            "schedule_in"=>$schedule_in,
                                                            "schedule_out"=>$schedule_out,
                                                            "status_potong_makan"=>"0",
                                                            "status_potong_gaji"=>"0",
                                                            ]);
                                    }
                                }else{
                                    $nilai_cuti = $p_karyawan_baru->sisacuti;
                                    $sisacuti = $nilai_cuti-1;
                                    
                                    DB::table('p_karyawan')
                                    ->where('id_karyawan', $value2->id)
                                    ->update([
                                            "sisacuti" => $sisacuti,
                                            ]);

                                    if($sisacuti < 0){
                                        DB::table("tm_dataabsensi")
                                        ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                        ->update([ 
                                                    "id_karyawan"=>$value2->id,
                                                    "nik"=> $value2->nik,
                                                    "tgl_absensi"=> $valuetgl,
                                                    "status_absensi"=> "CT",
                                                    "schedule_in"=>$schedule_in,
                                                    "schedule_out"=>$schedule_out,
                                                    "status_potong_makan"=>"1",
                                                    "status_potong_gaji"=>"1",
                                                    ]);
                                    }else{
                                            DB::table("tm_dataabsensi")
                                            ->where([['id_karyawan', $value2->id],['tgl_absensi',$valuetgl]])
                                            ->update([ 
                                                            "id_karyawan"=>$value2->id,
                                                            "nik"=> $value2->nik,
                                                            "tgl_absensi"=> $valuetgl,
                                                            "status_absensi"=> "CT",
                                                            "schedule_in"=>$schedule_in,
                                                            "schedule_out"=>$schedule_out,
                                                            "status_potong_makan"=>"0",
                                                            "status_potong_gaji"=>"0",
                                                            ]);
                                    }
                                }
                                
                            }
                            
                        }
                    }
                }
            }
        }

    }

    public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }

    public function getDatesFromRange2($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }
       
}
