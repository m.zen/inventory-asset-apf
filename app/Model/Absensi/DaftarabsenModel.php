<?php

namespace App\Model\Absensi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class DaftarabsenModel extends Model
{
     protected $table    = "p_karyawan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik", "a.nama_karyawan","a.noabsen","a.id_grupabsen",
                              "b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_grupabsen")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("tm_grupabsen as e","e.id_grupabsen","=","a.id_grupabsen")
                            
                            ->orderBy("a.noabsen", "ASC");




        if(session()->has("SES_SEARCH_DAFTARABSEN")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_DAFTARABSEN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  =DB::table("p_karyawan as a")
        					->select("a.id_karyawan","a.nik", "a.nama_karyawan","a.noabsen","a.id_grupabsen",
                            "b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.id_karyawan", $id)
                            ->orderBy("a.nama_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }    


    public function updateData($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "noabsen"=>setString($request->noabsen),
                            	"id_grupabsen"=>setString($request->id_grupabsen),
                                "user_id"=>setString(Auth::user()->id),
                            	"update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);


                          
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE DAFTARABSEN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_daftargaji")
                             ->where("id_daftargaji", $request->id_daftargaji)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE DAFTARABSEN (" . $request->id_daftargaji . ") " . strtoupper($request->nama_daftargaji), Auth::user()->id, $request);
    }
}
