<?php

namespace App\Model\Absensi;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use DateTime;

class PeriodeAbsenModel extends Model
{
    protected $table    = "m_profilesystem";
    public $timestamps= false ;

    public function getProfile() {
        $query  = DB::table("m_profilesystem")
                            ->select("bulanpayroll","tahunpayroll","tgl_payrollawal","tgl_payrollakhir","tgl_absenawal","tgl_absenakhir");
                           
        $result = $query->get();

        return $result;
    } 

   function getLibur($tgl_libur)
    {
         $query  = DB::table("m_harilibur")
                            ->select("tgl_libur","nama_harilibur")
                            ->where("tgl_libur",$tgl_libur);
                           
        $result = $query->get();

        return $result;

    }


   function getAbsensi($id_karyawan,$tgl_absensi)
    {
         $query  = DB::table("tm_dataabsensi")
                            ->select("id_karyawan","tgl_absensi")
                              ->where([['id_karyawan',$id_karyawan],['tgl_absensi',$tgl_absensi]]);
                           
        $result = $query->get();
        

        return $result;

    }

    public function updateData($request) 
    {
        $tgl_absen_awal       = setYMD($request->Tgl_Absen_awal,"/");
        $tgl_absen_akhir      = setYMD($request->Tgl_Absen_akhir,"/");	
        // $periode_berjalan     = $request->Periode_Berjalan;	
        // $range_absen          = $this->getDatesFromRange($tgl_absen_awal, $tgl_absen_akhir);

        //$qData  = DB::select("SELECT  * FROM  p_karyawan where aktif=1 order by id_karyawan
        //                ");

       $qData  =DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik")
                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_level as c","c.id_level","=","b.id_level")
                            ->where("a.aktif",1)
                            ->where("uang_makan",">",0);
         $qData=$qData->get();
                            


 
     
        foreach($qData as $row) 
        {

               $begin = new DateTime( $tgl_absen_awal );
               $end   = new DateTime( $tgl_absen_akhir );

            for($i = $begin; $i <= $end; $i->modify('+1 day'))
            {
                $tgl2[]= $i->format("Y-m-d");
                $tgl= $i->format("Y-m-d");


                $cekabsen=$this->getAbsensi($row->id_karyawan,$tgl)->first();
                // //dd($row->nama_karyawan,$tgl,$cekabsen);
                // if($row->id_karyawan==597)
                //     {
                //         dd("ada");
                //     }
                $create_data=[];
                if($cekabsen==null)
                {
                  
                    $ceklibur = $this->getLibur($tgl)->first();
                    if($ceklibur==null)
                    {
                        $status_absensi = "A";
                        $status_potong_makan = 1;
                        $status_potong_gaji = 0; //Potong Gaji Mulai Bulan Agustus 2019

                    }
                    else
                    {
                        $status_absensi = "";
                        $status_potong_makan = 0;
                        $status_potong_gaji = 0;

                    }
                     $create_data[] = [
                            "id_karyawan"=> $row->id_karyawan,
                            "nik"=> $row->nik,
                            "tgl_absensi"=> $tgl,
                            "status_absensi"=> $status_absensi,
                            "status_potong_makan"=> $status_potong_makan,
                            "status_potong_gaji"=> $status_potong_gaji,
                        ];

                    DB::table("tm_dataabsensi")->insert($create_data);
                }
               
               
            }
            
              


        }


        // DB::table("m_profilesystem")
        //                         ->update([  "tgl_absenawal"=>$tgl_absen_awal,
        //                                     "tgl_absenakhir"=>$tgl_absen_akhir
        //                                   ]);
       

      




        
        // $create_data = [];  
        // $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        // foreach ($hari_libur as $key => $value) {
        //     # code...
        //     $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        // }         
        // if($periode_berjalan == "Sekarang")
        // {
        //     foreach($p_karyawan as $key => $value){
        //         foreach($range_absen as $key2 => $value2){
        //            $cek = DB::table('tm_dataabsensi')
       // ->where([['id_karyawan',$value->id_karyawan],['tgl_absensi', $value2]])->first();
        //            if($cek == null)
        //            {
        //             $hari = new DateTime($value2);
        //             $nohari = $hari->format('w');
        //             if(in_array($value2, $tgl_libur)){
        //                 $status_absensi = "";
        //                 $status_potong_makan = 0;
        //                 $status_potong_gaji = 0;
        //             }else if($nohari == 0){
        //                 $status_absensi = "";
        //                 $status_potong_makan = 0;
        //                 $status_potong_gaji = 0;
        //             }else{
        //                 $status_absensi = "A";
        //                 $status_potong_makan = 1;
        //                 $status_potong_gaji = 0;
        //             }
        //                 $create_data[] = [
        //                     "id_karyawan"=> $value->id_karyawan,
        //                     "nik"=> $value->nik,
        //                     "tgl_absensi"=> $value2,
        //                     "status_absensi"=> $status_absensi,
        //                     "status_potong_makan"=> $status_potong_makan,
        //                     "status_potong_gaji"=> $status_potong_gaji,
        //                 ];
        //            }
        //         }
        //     }
        //     DB::table("tm_dataabsensi")->insert($create_data);
        // }else{
        //     foreach($p_karyawan as $key => $value){
        //         foreach($range_absen as $key2 => $value2){
        //         //    $cek = DB::table('tm_dataabsensi')->where([['id_karyawan',$value->id_karyawan],['tgl_absensi', $value2]])->first();
        //         //    if($cek == null){
        //             $hari = new DateTime($value2);
        //             $nohari = $hari->format('w');
        //             if(in_array($value2, $tgl_libur)){
        //                 $status_absensi = "";
        //                 $status_potong_makan = 0;
        //                 $status_potong_gaji = 0;
        //             }else if($nohari == 0){
        //                 $status_absensi = "";
        //                 $status_potong_makan = 0;
        //                 $status_potong_gaji = 0;
        //             }else{
        //                 $status_absensi = "A";
        //                 $status_potong_makan = 1;
        //                 $status_potong_gaji = 0;
        //             }
        //                 $create_data[] = [
        //                     "id_karyawan"=> $value->id_karyawan,
        //                     "nik"=> $value->nik,
        //                     "tgl_absensi"=> $value2,
        //                     "status_absensi"=> $status_absensi,
        //                     "status_potong_makan"=> $status_potong_makan,
        //                     "status_potong_gaji"=> $status_potong_gaji,
        //                 ];
                  
        //         //    }
        //         }
        //     }
        //     DB::table("tm_dataabsensi")->insert($create_data);
        // }
        
    }

    public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }
       
}
