<?php

namespace App\Model\Absensi;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\MasterModel;
use Datetime;

class JadwalabsenModel extends Model
{
    protected $table    = "tm_jadwal";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("tm_jadwal")
                            ->leftjoin('tm_grupabsen','tm_grupabsen.id_grupabsen','tm_jadwal.id_grupabsen')
                            ->leftjoin('p_karyawan','p_karyawan.nik','tm_jadwal.nik')
                            ->select("id_jadwal","nama_karyawan", "tm_jadwal.nik","nama_grupabsen","jam_masuk","jam_keluar","jam_masuk2","jam_keluar2","jam_masuk3","jam_keluar3","tanggal")
                            ->orderBy("p_karyawan.nama_karyawan", "ASC")
                            ->orderBy("tm_jadwal.tanggal", "DESC");

        if(session()->has("SES_SEARCH_JADWALABSEN")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_JADWALABSEN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("tm_jadwal")
                            ->select('tm_jadwal.*','p_karyawan.id_cabang')
                            ->leftjoin('p_karyawan','p_karyawan.nik','tm_jadwal.nik')
                            ->where('id_jadwal', $id);
        $result = $query->get();

        return $result;
    } 

    public function prosesData($request) 
    {
        $tgl_ijin_khusus        = date('Y-m-d', strtotime($request->tgl_ijin_khusus));
        $cabang                 = $request->id_cabang;
        $karyawan               = $request->id_karyawan;
        // $tgl_absen_akhir      = $request->Tgl_Absen_akhir;
        
        $hari = new DateTime($tgl_ijin_khusus);
        $kode_hari = $hari->format('w');

        $key = array_search('0', $karyawan);
        if (false !== $key) {
            unset($karyawan[$key]);
        }
        $tglawal = setYMD($request->tgl_awal,"/");
        $tglakhir = setYMD($request->tgl_akhir,"/");
        $tgl_tgl = $this->getDatesFromRange2($tglawal, $tglakhir);
        if(count($karyawan) > 0){
            foreach($karyawan as $value){
                $p_karyawan = DB::table('p_karyawan')->select('nik')->where('id_karyawan', $value)->first();
                if(!is_null($p_karyawan)){
                    foreach($tgl_tgl as $val){
                        $jadwal = DB::table('tm_jadwal')->where([['nik', $p_karyawan->nik], ['tanggal', $val]])->first();
                        if(is_null($jadwal)){
                            DB::table("tm_jadwal")
                            ->insert([ 
                                    "nik"=> $p_karyawan->nik,
                                    "tanggal"=> $val,
                                    "id_grupabsen"=> $request->id_grupabsen,
                                    ]);
                        }
                        else{
                            DB::table("tm_jadwal")
                                ->where('id_jadwal', $jadwal->id_jadwal)
                                ->update([ 
                                    "nik"=> $jadwal->nik,
                                    "tanggal"=> $val,
                                    "id_grupabsen"=> $request->id_grupabsen,
                                    ]);
                        }
                    }
                }
            }
        }

        $qLog       = new LogModel;
            # ---------------;
        $qLog->createLog("CREATE JADWAL ABSEN TANGGAL(" . $tglawal . ")  UNTUK CABANG" . strtoupper($cabang), Auth::user()->id, $request);
    }

    public function updateData($request) {
        DB::table("tm_jadwal")
                            ->where("id_jadwal", $request->id_jadwal)
                           ->update([ 
                                    "id_grupabsen"=>$request->id_grupabsen,
                                    "tanggal"=>setYMD($request->tanggal,"/"),
                                     ]);
                          
                           
       # ---------------
      
       /* ----------
        Logs
       ----------------------- */
           $qLog       = new LogModel;
           # ---------------;
           $qLog->createLog("UPDATE GRUPABSEN(" . $request->id_grupabsen . ") " . strtoupper($request->nama_grupabsen), Auth::user()->id, $request);
   }

   public function removeData($request) {
    DB::table("tm_jadwal")
                        ->where("id_jadwal", $request->id_jadwal)->delete();
                       
                      
 
   /* ----------
    Logs
   ----------------------- */
       $qLog       = new LogModel;
       # ---------------;
       $qLog->createLog("DELETE GRUPABSEN (" . $request->id_grupabsen . ") " . strtoupper($request->nama_grupabsen), Auth::user()->id, $request);
}

    public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }

    public function getDatesFromRange2($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }
       
}
