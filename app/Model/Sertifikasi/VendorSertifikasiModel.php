<?php

namespace App\Model\Sertifikasi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class VendorSertifikasiModel extends Model
{
    protected $table    = "sf_vendorsertifikasi";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("sf_vendorsertifikasi")
                            ->select("id_vendorsertifikasi", "nama_vendorsertifikasi")
                            ->orderBy("id_vendorsertifikasi", "DESC");

        if(session()->has("SES_SEARCH_VENDORSERTIFIKASI")) {
            $query->where("nama_vendorsertifikasi", "LIKE", "%" . session()->get("SES_SEARCH_VENDORSERTIFIKASI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("sf_vendorsertifikasi")
                            ->select("id_vendorsertifikasi", "nama_vendorsertifikasi")
                            ->where("id_vendorsertifikasi", $id)
                            ->orderBy("nama_vendorsertifikasi", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qvendorsertifikasi              = new MasterSertifikasiModel;
        # ---------------
       // $qmastertraining->kode       = setString($request->kode);
        //$qmastertraining->kategori_vendorsertifikasi  = setString($request->kategori_vendorsertifikasi);
        $qvendorsertifikasi->nama_vendorsertifikasi      = setString($request->nama_vendorsertifikasi);
        $qvendorsertifikasi->user_id                  = setString(Auth::user()->id);
        $qvendorsertifikasi->create_at                = setString(date('Y-m-d H:i:s'));
        $qvendorsertifikasi->update_at                = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
       $qvendorsertifikasi->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE MASTERTRAINING (" .$qvendorsertifikasi->id_vendorsertifikasi . ") " . strtoupper($request->nama_vendorsertifikasi), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("sf_vendorsertifikasi")
                             ->where("id_vendorsertifikasi", $request->id_vendorsertifikasi)
                            ->update([ "nama_vendorsertifikasi"=>$request->nama_vendorsertifikasi,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE MASTERTRAINING(" . $request->id_vendorsertifikasi . ") " . strtoupper($request->nama_vendorsertifikasi), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("sf_vendorsertifikasi")
                             ->where("id_vendorsertifikasi", $request->id_vendorsertifikasi)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE MASTERTRAINING (" . $request->id_vendorsertifikasi . ") " . strtoupper($request->nama_vendorsertifikasi), Auth::user()->id, $request);
    }
}
