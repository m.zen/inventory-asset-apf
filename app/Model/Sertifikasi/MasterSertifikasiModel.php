<?php

namespace App\Model\Sertifikasi;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class MasterSertifikasiModel extends Model
{
    protected $table    = "sf_mastersertifikasi";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("sf_mastersertifikasi")
                            ->select("id_mastersertifikasi", "nama_mastersertifikasi")
                            ->orderBy("id_mastersertifikasi", "DESC");

        if(session()->has("SES_SEARCH_MASTERSERTIFIKASI")) {
            $query->where("nama_mastersertifikasi", "LIKE", "%" . session()->get("SES_SEARCH_MASTERSERTIFIKASI") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("sf_mastersertifikasi")
                            ->select("id_mastersertifikasi", "nama_mastersertifikasi")
                            ->where("id_mastersertifikasi", $id)
                            ->orderBy("nama_mastersertifikasi", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qmastersertifikasi              = new MasterSertifikasiModel;
        # ---------------
       // $qmastertraining->kode       = setString($request->kode);
        //$qmastertraining->kategori_mastersertifikasi  = setString($request->kategori_mastersertifikasi);
        $qmastersertifikasi->nama_mastersertifikasi      = setString($request->nama_mastersertifikasi);
        $qmastersertifikasi->user_id                  = setString(Auth::user()->id);
        $qmastersertifikasi->create_at                = setString(date('Y-m-d H:i:s'));
        $qmastersertifikasi->update_at                = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
       $qmastersertifikasi->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE MASTERTRAINING (" .$qmastersertifikasi->id_mastersertifikasi . ") " . strtoupper($request->nama_mastersertifikasi), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("sf_mastersertifikasi")
                             ->where("id_mastersertifikasi", $request->id_mastersertifikasi)
                            ->update([ "nama_mastersertifikasi"=>$request->nama_mastersertifikasi,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE MASTERTRAINING(" . $request->id_mastersertifikasi . ") " . strtoupper($request->nama_mastersertifikasi), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("sf_mastersertifikasi")
                             ->where("id_mastersertifikasi", $request->id_mastersertifikasi)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE MASTERTRAINING (" . $request->id_mastersertifikasi . ") " . strtoupper($request->nama_mastersertifikasi), Auth::user()->id, $request);
    }
}
