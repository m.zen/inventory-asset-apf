<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class KaryawankeluarModel extends Model
{
     protected $table    = "p_karyawankeluar";
    public $timestamps= false ;


    public function getList($request=null, $offset=null, $limit=null) {
         $query  = DB::table("p_karyawankeluar as a")
                            ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang","d.nama_departemen","e.nama_jabatan",db::Raw("DATE_FORMAT(a.tgl_keluar, '%d/%m/%Y') as Tanggal_keluar"))
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","b.id_departemen")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","b.id_jabatan")
                          //  ->where("a.aktif", 0)
                            ->Where('a.status', "PROSES")

                            ->orderBy("a.tgl_keluar", "DESC");




        if(session()->has("SES_SEARCH_KARYAWANKELUAR")) {
            $query->where("b.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANKELUAR") . "%")
                  ->orwhere("c.nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANKELUAR") . "%")
                    ->orwhere("b.nik", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANKELUAR") . "%");
                  
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }





    public function getListKRY($request=null, $offset=null, $limit=null) {
         $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                          //  ->where("a.aktif", 0)
                            ->Where('a.tgl_keluar', '!=', null)

                            ->orderBy("a.tgl_keluar", "DESC");




        if(session()->has("SES_SEARCH_KARYAWANKELUAR")) {
            $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANKELUAR") . "%")
                  ->orwhere("b.nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANKELUAR") . "%")
                    ->orwhere("a.nik", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANKELUAR") . "%");
                  
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
          $query  = DB::table("p_karyawankeluar as a")
                            ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang","d.nama_departemen","e.nama_jabatan")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","b.id_departemen")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","b.id_jabatan")
                          //  ->where("a.aktif", 0)
                            ->where("a.id_karyawankeluar", $id)
                            ->orderBy("b.nama_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }   

       public function getKry($id) {
        $query  = DB::table("p_karyawan")
                            ->select("id_cabang","id_departemen","id_jabatan")
                            ->where("id_karyawan", $id);

        $result = $query->get()->first();

        return $result;
    }      

       public function createData($request) {
              
        $qkrykeluar              = new KaryawankeluarModel;

        # ---------------
       // $qperingatan->kode       = setString($request->kode);
        $id            = $request->id_karyawan;
        $qKary         = $this->getKry($id);
        $id_departemen = $qKary->id_departemen;
        $id_cabang     = $qKary->id_cabang;
        $id_jabatan    = $qKary->id_jabatan;
        $id_approve    = $request->id_approve;
        
      
        $qkrykeluar->id_karyawan       = setString($request->id_karyawan);
        //$qperingatan->no_surat          = setString($request->no_surat);
        //$qperingatan->tgl_pengajuan     = setYMD($request->tgl_sp,"/");
        $qkrykeluar->tgl_keluar        = setYMD($request->tgl_keluar,"/");
         $qkrykeluar->id_alasankeluar = setString($request->id_alasankeluar);
        $qkrykeluar->keterangan_keluar = setString($request->keterangan_keluar);
        $qkrykeluar->id_jabatan        = setString($id_jabatan);
        $qkrykeluar->id_departemen     = setString($id_departemen);
        $qkrykeluar->id_cabang         = setString($id_cabang);
        $qkrykeluar->status            = "PROSES";
        $qkrykeluar->user_id           = setString(Auth::user()->id);
        $qkrykeluar->create_at         = setString(date('Y-m-d H:i:s'));
        $qkrykeluar->update_at         = setString(date('Y-m-d H:i:s'));
        
        
        # ---------------
        $qkrykeluar->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE peringatan (" . $qkrykeluar->id_karyawankeluar . ") " . strtoupper($request->id_karyawankeluar), Auth::user()->id, $request);

    } 
      
    public function createDatakry($request) {
              
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                                "id_alasankeluar" => setString($request->id_alasankeluar),
                                "keterangan_keluar" => setString($request->keterangan_keluar),
                                "user_id"=>setString(Auth::user()->id),
                                "update_at"=>setString(date('Y-m-d H:i:s'))    
                                ]);

            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);

    }

    public function updateData($request) {

 

             DB::table("p_karyawankeluar")
                             ->where("id_karyawankeluar", $request->id_karyawankeluar)
                            ->update(["tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                               "id_alasankeluar" => setString($request->id_alasankeluar),
                                "keterangan_keluar" => setString($request->keterangan_keluar),
                                "user_id"=>setString(Auth::user()->id),
                                "update_at"=>setString(date('Y-m-d H:i:s'))    
                                ]);

     
    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWANKELUAR(" . $request->id_karyawan . ") " . strtoupper($request->id_karyawankeluar), Auth::user()->id, $request);
    }
public function updateapprove($request) {
  
         DB::table("p_karyawankeluar")
                             ->where("id_karyawankeluar", $request->id_karyawankeluar)
                            ->update([ "status"=>"APPROVE",
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
          $kry  = $this->getProfile($request->id_karyawankeluar)->first();
          
                         
          DB::table("p_karyawan")
                             ->where("id_karyawan", $kry->id_karyawan)
                            ->update([ "tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                            			"aktif"=>0,
                                  "alasan_keluar"=>setString($request->id_alasankeluar),
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
    public function removeData($request) {
         DB::table("p_karyawankeluar")
                             ->where("id_karyawankeluar", $request->id_karyawankeluar)->delete();

                $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);





            }
           
}
