<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Payroll\ProsesgajiModel;
class KontrakkerjaModel extends Model
{
    protected $table    = "pa_kontrakkerja";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        
                $query  = DB::table("pa_kontrakkerja as a")
                           ->select("*","e.nama_karyawan",
                             "b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.tempat_lahir","e.tgl_lahir","a.keterangan_kontrak","a.tgl_awal","a.tgl_akhir","a.tgl_keluar")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->orderBy("id_kontrakkerja", "asc");
        



        if(session()->has("SES_SEARCH_KONTRAK")) {
            $query->where("e.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KONTRAK") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }


public function getLastID() {
        $query  = DB::table("p_karyawan")
                            ->select("id_karyawan")
                            ->orderBy("id_karyawan", "desc");

        $result = $query->get();

        return $result;
    }    
// $query  = DB::table("p_karyawan")
//                             ->select("*")
//                             ->where('aktif','=',1);
//         $data['totalKaryawan'] =  $query->count();
//         $data['totalkontrak'] = DB::table("vw_jatuhtempo")->count() ;
//         $data['totalakankontrak'] =  DB::table("vw_akanjthtempo")->count() ;

public function getList_jtk($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        if ($idkaryawan="All")
        {
                $query  = DB::table("vw_jatuhtempo as a")
                           ->select("a.id_karyawan","a.nama_karyawan","a.nik", "a.tgl_akhir",
                              "c.nama_cabang","d.nama_departemen","e.nama_jabatan","a.keterangan_kontrak")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","b.id_departemen")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","b.id_jabatan")
                            ->orderBy("a.tgl_akhir", "DESC");
        }
        else
        {
            $query  = DB::table("vw_jatuhtempo")
                           ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_karyawan", "asc");
    
        }
        



        if(session()->has("SES_SEARCH_KONTRAK")) {
            $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_JTK_KONTRAK") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

public function getList_akanjtk($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        if ($idkaryawan="All")
        {
                $query  = DB::table("vw_akanjthtempo as a")
                           ->select("a.id_karyawan","a.nama_karyawan","a.nik", "a.tgl_akhir",
                              "c.nama_cabang","d.nama_departemen","e.nama_jabatan","a.keterangan_kontrak")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","b.id_departemen")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","b.id_jabatan")
                            ->orderBy("a.tgl_akhir", "DESC");
        }
        else
        {
            $query  = DB::table("vw_jatuhtempo")
                           ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_karyawan", "asc");
    
        }
        



        if(session()->has("SES_SEARCH_KONTRAK")) {
            $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_JTK_KONTRAK") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }


public function getList_kontrak($request=null, $offset=null, $limit=null) {
     $query  = DB::table("p_karyawan as a")
                           ->select("a.id_karyawan","a.nama_karyawan","a.nik", "a.tgl_awal","a.tgl_akhir", 
                             "b.nama_cabang","c.nama_departemen","d.nama_jabatan","a.keterangan_kontrak")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                             ->orderBy("a.tgl_akhir", "asc")
                            ->where("a.aktif",1)
                            ->where("a.status_kerja","Kontrak");
                            // ->whereMonth('a.tgl_akhir', '=', 4)
                            // ->whereYear('a.tgl_akhir', '=', date('Y'));
                            

         if(session()->has("SES_SEARCH_BULAN")) {
             $month = substr(session()->get("SES_SEARCH_BULAN"), 0, 2);
            $year = substr(session()->get("SES_SEARCH_BULAN"), 3, 4);
           
            $query->whereYear("a.tgl_akhir", '=', $year);
            $query->whereMonth("a.tgl_akhir", '=' , $month);
        }

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_CABANG"));
            }
            
        }

        if(session()->has("SES_SEARCH_KONTRAKALL")) {
            $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KONTRAKALL") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }



public function getList_percobaanjt($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        if ($idkaryawan="All")
        {
                $query  = DB::table("vw_jth_percobaan as a")
                           ->select("a.id_karyawan","a.nama_karyawan","a.nik", "a.tgl_percobaan",
                              "c.nama_cabang","d.nama_departemen","e.nama_jabatan","a.keterangan_kontrak")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_cabang as c","c.id_cabang","=","b.id_cabang")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","b.id_departemen")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","b.id_jabatan")
                            ->orderBy("a.tgl_percobaan", "DESC");
        }
        else
        {
            $query  = DB::table("vw_jth_percobaan")
                           ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_karyawan", "asc");
    
        }
        



        if(session()->has("SES_SEARCH_PERCOBAAN")) {
            $query->where("a.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PERCOBAAN") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }




    public function getCetakKontrak($id)
    {
        $query  = DB::table("pa_kontrakkerja as a")
                           ->select("a.id_kontrakkerja","a.id_karyawan","e.nama_karyawan","a.nik", "a.tgl_awal","a.tgl_akhir",
                              "a.keterangan_kontrak","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.tempat_lahir","e.tgl_lahir","a.gaji_pokok"
                              ,"e.no_ktp","e.jenis_kelamin","alamat","a.tunj_jabatan","a.tunj_jabatan","a.tunj_transport","a.tunj_makan","f.nama_level",
                              "a.tunj_kemahalan","a.tunj_jaga","e.tgl_masuk")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_level as f","f.id_level","=","d.id_level")
                            ->where("id_kontrakkerja",$id)
                            ->orderBy("id_kontrakkerja", "asc");
                    

        $result = $query->get();

        return $result;
    }
    public function getProfileKry($id) {
        $query  =DB::table("p_karyawan")
                            ->select("*",db::Raw("DATE_ADD(tgl_masuk,INTERVAL 3 MONTH) as Tanggal_percobaan"),db::Raw("DATE_ADD(tgl_masuk,INTERVAL 15 MONTH) as tanggal_akhir"))
                            ->where("id_karyawan", $id)
                            ->orderBy("id_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_kontrakkerja as a")
                            ->select("a.*","b.nama_karyawan")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->where("id_kontrakkerja", $id)
                            ->orderBy("id_kontrakkerja", "asc");

        $result = $query->get();

        return $result;
    }    

    public function CekKontrak($idkry,$tglawal,$tglakhir) {
       
        $query  = DB::table("pa_kontrakkerja")
                            ->select("*")
                            ->where("id_karyawan", $idkry)
                            ->where("tgl_awal", setYMD($tglawal,"/"))
                            ->where("tgl_akhir", setYMD($tglakhir,"/"));
        $result = $query->count();
        

        return $result;
    }
 
    public function createData($request) {
        $qkontrakkerja     = new KontrakkerjaModel;
        $qdatagaji         = $this->getProfileKry($request->id_karyawan)->first();
        //dd( $qdatagaji->id_cabang, $qdatagaji->id_departemen, $qdatagaji->id_jabatan);
      
      
       // $qkontrakkerja->kode       = setString($request->kode);
        if($request->status_kontrak=="reguler")
        {
                $qkontrakkerja->id_karyawan             = setString($request->id_karyawan);
                $qkontrakkerja->nik                     = $qdatagaji->nik;
                $qkontrakkerja->id_cabang               = $qdatagaji->id_cabang;
                $qkontrakkerja->id_departemen           = $qdatagaji->id_departemen ;
                $qkontrakkerja->id_jabatan       	    = $qdatagaji->id_jabatan;
                $qkontrakkerja->no_ktp                  = $qdatagaji->no_ktp;
                $qkontrakkerja->keterangan_kontrak      = setString($request->keterangan_kontrak);
                $qkontrakkerja->tgl_awal          	    = setYMD($request->tgl_awal,"/"); 
                $qkontrakkerja->tgl_akhir       	    = setYMD($request->tgl_akhir,"/"); 
                $qkontrakkerja->status_proses           = "PROSES";
                $qkontrakkerja->status_kontrak          = "reguler"; 
                $qkontrakkerja->gaji_pokok              = $qdatagaji->gaji_pokok;
                $qkontrakkerja->tunj_jabatan            = $qdatagaji->tunj_jabatan;
                $qkontrakkerja->tunj_transport          = $qdatagaji->tunj_transport;
                $qkontrakkerja->tunj_jaga               = $qdatagaji->tunj_jaga;
                $qkontrakkerja->tunj_kemahalan          = $qdatagaji->tunj_kemahalan;
                $qkontrakkerja->tunj_makan              = $qdatagaji->tunj_makan;
                $qkontrakkerja->user_id                 = setString(Auth::user()->id);
                $qkontrakkerja->create_at               = setString(date('Y-m-d H:i:s'));
                $qkontrakkerja->update_at               = setString(date('Y-m-d H:i:s'));
                $qkontrakkerja->save();
        }
        else
        {
                $qkontrakkerja->id_karyawan             = setString($request->id_karyawan);
                $qkontrakkerja->id_cabang               = $qdatagaji->id_cabang;
                $qkontrakkerja->id_departemen           = $qdatagaji->id_departemen ;
                $qkontrakkerja->id_jabatan              = $qdatagaji->id_jabatan;
                $qkontrakkerja->no_ktp                  = $qdatagaji->no_ktp;
                $qkontrakkerja->nik                     = $qdatagaji->nik;
                $qkontrakkerja->nik_baru                = $request->nik_baru;
                $qkontrakkerja->tgl_keluar              = setYMD($request->tgl_keluar,"/");
                $qkontrakkerja->keterangan_kontrak      = "KONTRAK 1";
                $qkontrakkerja->tgl_awal                = setYMD($request->tgl_awal,"/"); 
                $qkontrakkerja->tgl_akhir               = setYMD($request->tgl_akhir,"/"); 
                $qkontrakkerja->status_proses           = "PROSES";
                $qkontrakkerja->status_kontrak          = "perubahan"; 
                $qkontrakkerja->gaji_pokok              = $qdatagaji->gaji_pokok;
                $qkontrakkerja->tunj_jabatan            = $qdatagaji->tunj_jabatan;
                $qkontrakkerja->tunj_transport          = $qdatagaji->tunj_transport;
                $qkontrakkerja->tunj_jaga               = $qdatagaji->tunj_jaga;
                $qkontrakkerja->tunj_kemahalan          = $qdatagaji->tunj_kemahalan;
                $qkontrakkerja->tunj_makan              = $qdatagaji->tunj_makan;

                $qkontrakkerja->user_id                 = setString(Auth::user()->id);
                $qkontrakkerja->create_at               = setString(date('Y-m-d H:i:s'));
                $qkontrakkerja->update_at               = setString(date('Y-m-d H:i:s'));
                $qkontrakkerja->save();
        

        }

        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE kontrakkerja (" . $qkontrakkerja->id_kontrakkerja . ") " . strtoupper($request->id_kontrakkerja), Auth::user()->id, $request);
    }

    public function updateData($request) {

        if( empty($request->input("nik_baru")) )
        {
                 DB::table("pa_kontrakkerja")
                             ->where("id_kontrakkerja", $request->id)
                            ->update([ "tgl_awal"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir"=>setYMD($request->tgl_akhir,"/"),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        }
        else
        {
            DB::table("pa_kontrakkerja")
                             ->where("id_kontrakkerja", $request->id)
                            ->update([ "tgl_awal"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir"=>setYMD($request->tgl_akhir,"/"),
                                        "tgl_keluar"=>setYMD($request->tgl_keluar,"/"),
                                        "nik_baru"=>$request->nik_baru,
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        }

    
        
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE kontrakkerja(" . $request->id_kontrakkerja . ") " . strtoupper($request->nama_kontrakkerja), Auth::user()->id, $request);
    }
    public function updateaktivasiData($request) {
        
        
         

         if( empty($request->input("nik_baru")) )
        {
                 DB::table("pa_kontrakkerja")
                             ->where("id_kontrakkerja", $request->id)
                            ->update([ "status_proses"=>"APPROVE",
                                         "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        
             if($request->keterangan_kontrak=="KONTRAK 1")
             {
                    DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "tgl_awal"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir"=>setYMD($request->tgl_akhir,"/"),
                                        "status_kerja"=>"KONTRAK",
                                       "keterangan_kontrak"      => setString($request->keterangan_kontrak),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);  
             }
             else if($request->keterangan_kontrak=="KONTRAK 2")
             {
                DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "tgl_awal2"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir2"=>setYMD($request->tgl_akhir,"/"),
                                       "keterangan_kontrak"      => setString($request->keterangan_kontrak),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]); 
                                       
                             DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "tgl_awal2"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir2"=>setYMD($request->tgl_akhir,"/"),
                                       "keterangan_kontrak"      => setString($request->keterangan_kontrak),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);  

             }

      
            

        }
        else
        {

              /*-----------------------------------------------------------------
            1. Buat Data Karyawan Baru
            
        --------------------------------------------------------------------*/    
      
         $qkaryawan              = new KaryawanModel;
           
   //      $tgl_awalcuti =date("Y-m-d H:i:s", strtotime("+1 years", strtotime(setYMD($request->tgl_masuk, "/"))));
   //      $tgl_akhircuti =date("Y-m-d H:i:s", strtotime("+2 years", strtotime(setYMD($request->tgl_masuk, "/"))));
   // //     dd(setYMD($request->tgl_masuk, "/"),setYMD($request->tgl_lahir,"/"), $tgl_akhircuti);
        # ---------------
       // $qkaryawan->kode       = setString($request->kode);
        $qProfiele                      =    $qkaryawan->getProfile($request->id_karyawan)->first();

        //dd($qProfiele);
        
        //Cek Gaji

        
        if(empty($qProfiele->bpjs_kes_kry))
        {
                 $bpjs_kes_kry   =  0;
        }
        else
        {
             $bpjs_kes_kry   =   $qProfiele->bpjs_kes_kry;
        }
        if(empty($qProfiele->bpjs_kes_prs))
        {
                $bpjs_kes_prs   =   0;
        }
       else
       {
            $bpjs_kes_prs   =   $qProfiele->bpjs_kes_prs;
       }
        if(empty($qProfiele->jht_kry))
        {

            $jht_kry        =   0;
        }
        else
        {
            $jht_kry        =   $qProfiele->jht_kry;

        }
        
        if(empty($qProfiele->jht_prs))
        {
            $jht_prs        =   0;
        }
        else
        {
            $jht_prs        =   $qProfiele->jht_prs;
        }
        
         if(empty($qProfiele->jp_kry))
        {
           $jp_kry         =   0;
        }
        else
        {
            $jp_kry         =   $qProfiele->jp_kry;
        }
       if(empty($qProfiele->jp_prs))
        {
          $jp_prs         =   0;
        }
        else
        {
            $jp_prs         =   $qProfiele->jp_prs;
        }

        if(empty($qProfiele->jkk))
        {
          $jkk            =   0;
        }
        else
        {
            $jkk            =   $qProfiele->jkk;
        }
         if(empty($qProfiele->jkm))
        {
           $jkm            =   0;
        }
        else
        {
            $jkm            =   $qProfiele->jkm;
        }
         if(empty($qProfiele->plafon))
        {
           $plafon         =   0;
        }
        else
        {
            $plafon         =   $qProfiele->plafon;
        }







        $qkaryawan->nik                 = setString($request->nik_baru);
        $qkaryawan->nik_lama            = setString($request->nik);
        $qkaryawan->nama_karyawan       = setString($qProfiele->nama_karyawan);
        $qkaryawan->id_cabang           = setString($qProfiele->id_cabang);
        $qkaryawan->id_departemen       = setString($qProfiele->id_departemen);
        $qkaryawan->id_jabatan          = setString($qProfiele->id_jabatan);
        $qkaryawan->tgl_masuk           = setYMD($request->tgl_awal,"/");
        $qkaryawan->no_rekening         = $qProfiele->no_rekening;
        $qkaryawan->anrekening          = $qProfiele->anrekening;
        $qkaryawan->email               = setString($qProfiele->email);
        $qkaryawan->noabsen             = setString($qProfiele->noabsen);
        $qkaryawan->no_ktp              = $qProfiele->no_ktp;
        $qkaryawan->tempat_lahir        = $qProfiele->tempat_lahir;
        $qkaryawan->tgl_lahir           = $qProfiele->tgl_lahir;
        $qkaryawan->agama               = $qProfiele->agama;
        $qkaryawan->jenis_kelamin       = $qProfiele->jenis_kelamin;
        $qkaryawan->status_nikah        = $qProfiele->status_nikah;
        $qkaryawan->id_pendidikan       = $qProfiele->id_pendidikan;
        $qkaryawan->alamat              = $qProfiele->alamat;
        $qkaryawan->alamat_valid        = $qProfiele->alamat_valid;
        $qkaryawan->no_telpon           = $qProfiele->no_telpon;
        $qkaryawan->tgl_awal           = setYMD($request->tgl_awal,"/");
        $qkaryawan->tgl_akhir           = setYMD($request->tgl_akhir,"/");
        $qkaryawan->tgl_awalcuti        = $qProfiele->tgl_awalcuti ;
        $qkaryawan->tgl_akhircuti       = $qProfiele->tgl_akhircuti ;
        $qkaryawan->tgl_batas_cuti       = $qProfiele->tgl_batas_cuti ;
        $qkaryawan->tunj_jabatan        = $qProfiele->tunj_jabatan ;
        $qkaryawan->tunj_transport      = $qProfiele->tunj_transport ;
        $qkaryawan->jenispajak          =2;
        $qkaryawan->aktif          =1;
        $qkaryawan->status_kerja        ="KONTRAK";
        $qkaryawan->keterangan_kontrak  = "KONTRAK 1" ;
        $qkaryawan->sisacuti            =$qProfiele->sisacuti;
        $qkaryawan->gaji_pokok = setNoComma($qProfiele->gaji_pokok);
        $qkaryawan->rate_bpjstk = setNoComma($qProfiele->rate_bpjstk);
        $qkaryawan->rate_bpjskes = setNoComma($qProfiele->rate_bpjskes);
        $qkaryawan->tunj_jabatan = setNoComma($qProfiele->tunj_jabatan);
        $qkaryawan->tunj_transport = setNoComma($qProfiele->tunj_transport);
        $qkaryawan->tunj_jaga = setNoComma($qProfiele->tunj_jaga);
        $qkaryawan->tunj_kemahalan = setNoComma($qProfiele->tunj_kemahalan);
        $qkaryawan->tunj_makan = setNoComma($qProfiele->tunj_makan);
        $qkaryawan->bpjs_kes_kry = $bpjs_kes_kry;
        $qkaryawan->bpjs_kes_prs = $bpjs_kes_prs;
        $qkaryawan->jht_kry =  $jht_kry;
        $qkaryawan->jht_prs = $jht_prs;
        $qkaryawan->jp_kry = $jp_kry;
        $qkaryawan->jp_prs = $jp_prs;
        $qkaryawan->jkk = $jkk;
        $qkaryawan->jkm = $jkm;
        $qkaryawan->plafon =empty($qProfiele->plafon) ? 0 : setNoComma($qProfiele->plafon);     
        $qkaryawan->id_ptkp = $qProfiele->id_ptkp;
        $qkaryawan->jenispajak = 2; 
        $qkaryawan->nonpwp = $qProfiele->nonpwp;
        $qkaryawan->nobpjskes = $qProfiele->nobpjskes;
        $qkaryawan->nobjstk = $qProfiele->nobjstk;
        $qkaryawan->id_bank = $qProfiele->id_bank;
        $qkaryawan->no_rekening = $qProfiele->no_rekening;
        $qkaryawan->anrekening = $qProfiele->anrekening;
        $qkaryawan->noabsen = $qProfiele->noabsen;
        $qkaryawan->id_grading = $qProfiele->id_grading;



       
        $qkaryawan->save();


       /*-----------------------------------------------------------------------------
            
            Koperasi

       -----------------------------------------------------------------------------*/
      
       $id_karyawanbaru = $this->getLastID()->first();

       DB::table("p_koperasi")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "id_karyawan"=>$id_karyawanbaru->id_karyawan,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


 /*-----------------------------------------------------------------------------
            
           Absen
 -----------------------------------------------------------------------------*/
     $periodeAbsen = New ProsesgajiModel;
     $periodeAbsen = $periodeAbsen->getProfile()->first();

     $tgl_absenawal = $periodeAbsen->tgl_absenawal; 
     $tgl_absenakhir = $periodeAbsen->tgl_absenakhir;
     $id_karyawanbaru = $this->getLastID()->first();
       DB::table("tm_dataabsensi")
                             ->where("id_karyawan", $request->id_karyawan)
                             ->whereBetween("tgl_absensi",[$tgl_absenawal,$tgl_absenakhir])
                            ->update([ "id_karyawan"=>$id_karyawanbaru->id_karyawan,
                                "nik"=>$request->nik_baru,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


      DB::table("tm_timeoff")
                             ->where("id_karyawan", $request->id_karyawan)
                             ->where("status","Approve")
                            ->update([ "id_karyawan"=>$id_karyawanbaru->id_karyawan,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);






       /*-----------------------------------------------------------------------------
            
            Resign Karyawan

       -----------------------------------------------------------------------------*/

          DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                                "nik_lama"=>$request->id_karyawan,
                                "aktif"=>0,

                                       "alasan_keluar"=>"Habis Kontrak",
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);

          

        }
  
      
        
                           
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE kontrakkerja(" . $request->id_kontrakkerja . ") " . strtoupper($request->nama_kontrakkerja), Auth::user()->id, $request);
    }

    public function PerubahanApprove($request) {

        /*-----------------------------------------------------------------
            1. Buat Data Karyawan Baru
            
        --------------------------------------------------------------------*/    
      
         $qkaryawan              = new KaryawanModel;
           
   //      $tgl_awalcuti =date("Y-m-d H:i:s", strtotime("+1 years", strtotime(setYMD($request->tgl_masuk, "/"))));
   //      $tgl_akhircuti =date("Y-m-d H:i:s", strtotime("+2 years", strtotime(setYMD($request->tgl_masuk, "/"))));
   // //     dd(setYMD($request->tgl_masuk, "/"),setYMD($request->tgl_lahir,"/"), $tgl_akhircuti);
        # ---------------
       // $qkaryawan->kode       = setString($request->kode);
        $qProfiele                      =    $qkaryawan->getProfile($request->id_karyawan)->first();

        //dd($qProfiele);
        
        //Cek Gaji

        
        if(empty($qProfiele->bpjs_kes_kry))
        {
                 $bpjs_kes_kry   =  0;
        }
        else
        {
             $bpjs_kes_kry   =   $qProfiele->bpjs_kes_kry;
        }
        if(empty($qProfiele->bpjs_kes_prs))
        {
                $bpjs_kes_prs   =   0;
        }
       else
       {
            $bpjs_kes_prs   =   $qProfiele->bpjs_kes_prs;
       }
        if(empty($qProfiele->jht_kry))
        {

            $jht_kry        =   0;
        }
        else
        {
            $jht_kry        =   $qProfiele->jht_kry;

        }
        
        if(empty($qProfiele->jht_prs))
        {
            $jht_prs        =   0;
        }
        else
        {
            $jht_prs        =   $qProfiele->jht_prs;
        }
        
         if(empty($qProfiele->jp_kry))
        {
           $jp_kry         =   0;
        }
        else
        {
            $jp_kry         =   $qProfiele->jp_kry;
        }
       if(empty($qProfiele->jp_prs))
        {
          $jp_prs         =   0;
        }
        else
        {
            $jp_prs         =   $qProfiele->jp_prs;
        }

        if(empty($qProfiele->jkk))
        {
          $jkk            =   0;
        }
        else
        {
            $jkk            =   $qProfiele->jkk;
        }
         if(empty($qProfiele->jkm))
        {
           $jkm            =   0;
        }
        else
        {
            $jkm            =   $qProfiele->jkm;
        }
         if(empty($qProfiele->plafon))
        {
           $plafon         =   0;
        }
        else
        {
            $plafon         =   $qProfiele->plafon;
        }







        $qkaryawan->nik                 = setString($request->nik_baru);
        $qkaryawan->nama_karyawan       = setString($qProfiele->nama_karyawan);
        $qkaryawan->id_cabang           = setString($qProfiele->id_cabang);
        $qkaryawan->id_departemen       = setString($qProfiele->id_departemen);
        $qkaryawan->id_jabatan          = setString($qProfiele->id_jabatan);
        $qkaryawan->tgl_masuk           = setYMD($request->tgl_awal,"/");
        $qkaryawan->no_rekening         = $qProfiele->no_rekening;
        $qkaryawan->anrekening          = $qProfiele->anrekening;
        $qkaryawan->email               = setString($qProfiele->email);
        $qkaryawan->noabsen             = setString($qProfiele->noabsen);
        $qkaryawan->no_ktp              = $qProfiele->no_ktp;
        $qkaryawan->tempat_lahir        = $qProfiele->tempat_lahir;
        $qkaryawan->tgl_lahir           = $qProfiele->tgl_lahir;
        $qkaryawan->agama               = $qProfiele->agama;
        $qkaryawan->jenis_kelamin       = $qProfiele->jenis_kelamin;
        $qkaryawan->status_nikah        = $qProfiele->status_nikah;
        $qkaryawan->id_pendidikan       = $qProfiele->id_pendidikan;
        $qkaryawan->alamat              = $qProfiele->alamat;
        $qkaryawan->alamat_valid        = $qProfiele->alamat_valid;
        $qkaryawan->no_telpon           = $qProfiele->no_telpon;
        $qkaryawan->tgl_awal           = setYMD($request->tgl_awal,"/");
        $qkaryawan->tgl_akhir           = setYMD($request->tgl_akhir,"/");
        $qkaryawan->tgl_awalcuti        = $qProfiele->tgl_awalcuti ;
        $qkaryawan->tgl_akhircuti       = $qProfiele->tgl_akhircuti ;
        $qkaryawan->tgl_batas_cuti       = $qProfiele->tgl_batas_cuti ;
        $qkaryawan->tunj_jabatan        = $qProfiele->tunj_jabatan ;
        $qkaryawan->tunj_transport      = $qProfiele->tunj_transport ;
        $qkaryawan->jenispajak          =2;
        $qkaryawan->aktif          =1;
        $qkaryawan->status_kerja        ="KONTRAK";
        $qkaryawan->keterangan_kontrak  = "KONTRAK 1" ;
        $qkaryawan->sisacuti            =$qProfiele->sisacuti;
        $qkaryawan->gaji_pokok = setNoComma($qProfiele->gaji_pokok);
        $qkaryawan->rate_bpjstk = setNoComma($qProfiele->rate_bpjstk);
        $qkaryawan->rate_bpjskes = setNoComma($qProfiele->rate_bpjskes);
        $qkaryawan->tunj_jabatan = setNoComma($qProfiele->tunj_jabatan);
        $qkaryawan->tunj_transport = setNoComma($qProfiele->tunj_transport);
        $qkaryawan->tunj_jaga = setNoComma($qProfiele->tunj_jaga);
        $qkaryawan->tunj_kemahalan = setNoComma($qProfiele->tunj_kemahalan);
        $qkaryawan->tunj_makan = setNoComma($qProfiele->tunj_makan);
        $qkaryawan->bpjs_kes_kry = $bpjs_kes_kry;
        $qkaryawan->bpjs_kes_prs = $bpjs_kes_prs;
        $qkaryawan->jht_kry =  $jht_kry;
        $qkaryawan->jht_prs = $jht_prs;
        $qkaryawan->jp_kry = $jp_kry;
        $qkaryawan->jp_prs = $jp_prs;
        $qkaryawan->jkk = $jkk;
        $qkaryawan->jkm = $jkm;
        $qkaryawan->plafon =empty($qProfiele->plafon) ? 0 : setNoComma($qProfiele->plafon);     
        $qkaryawan->id_ptkp = $qProfiele->id_ptkp;
        $qkaryawan->jenispajak = 2; 
        $qkaryawan->nonpwp = $qProfiele->nonpwp;
        $qkaryawan->nobpjskes = $qProfiele->nobpjskes;
        $qkaryawan->nobjstk = $qProfiele->nobjstk;
        $qkaryawan->id_bank = $qProfiele->id_bank;
        $qkaryawan->no_rekening = $qProfiele->no_rekening;
        $qkaryawan->anrekening = $qProfiele->anrekening;
        $qkaryawan->noabsen = $qProfiele->noabsen;
        $qkaryawan->id_grading = $qProfiele->id_grading;



       
        $qkaryawan->save();


       /*-----------------------------------------------------------------------------
            
            Koperasi

       -----------------------------------------------------------------------------*/
      
       $id_karyawanbaru = $this->getLastID()->first();

       DB::table("p_koperasi")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "id_karyawan"=>$id_karyawanbaru->id_karyawan,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


 /*-----------------------------------------------------------------------------
            
           Absen
 -----------------------------------------------------------------------------*/
     $periodeAbsen = New ProsesgajiModel;
     $periodeAbsen = $periodeAbsen->getProfile()->first();

     $tgl_absenawal = $periodeAbsen->tgl_absenawal; 
     $tgl_absenakhir = $periodeAbsen->tgl_absenakhir;
     $id_karyawanbaru = $this->getLastID()->first();
       DB::table("tm_dataabsensi")
                             ->where("id_karyawan", $request->id_karyawan)
                             ->whereBetween("tgl_absensi",[$tgl_absenawal,$tgl_absenakhir])
                            ->update([ "id_karyawan"=>$id_karyawanbaru->id_karyawan,
                                "nik"=>$request->nik_baru,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


      DB::table("tm_timeoff")
                             ->where("id_karyawan", $request->id_karyawan)
                             ->where("status","Approve")
                            ->update([ "id_karyawan"=>$id_karyawanbaru->id_karyawan,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);






       /*-----------------------------------------------------------------------------
            
            Resign Karyawan

       -----------------------------------------------------------------------------*/

          DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                                "nik_lama"=>$request->id_karyawan,
                                "aktif"=>0,

                                       "alasan_keluar"=>"Habis Kontrak",
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);




       
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE KARYAWAN (" . $qkaryawan->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }



    public function removeData($request) {
         DB::table("pa_kontrakkerja")
            ->where("id_kontrakkerja", $request->id_kontrakkerja)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE kontrakkerja (" . $request->id_kontrakkerja . ") " . strtoupper($request->nama_kontrakkerja), Auth::user()->id, $request);
    }
}
