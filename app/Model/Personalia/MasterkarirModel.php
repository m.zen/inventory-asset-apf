<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class MasterkarirModel extends Model
{
    protected $table    = "pa_masterkarir";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("pa_masterkarir")
                            ->select("id_masterkarir", "nama_masterkarir")
                            ->orderBy("id_masterkarir", "DESC");

        if(session()->has("SES_SEARCH_MASTERKARIR")) {
            $query->where("nama_masterkarir", "LIKE", "%" . session()->get("SES_SEARCH_MASTERKARIR") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_masterkarir")
                            ->select("id_masterkarir", "nama_masterkarir")
                            ->where("id_masterkarir", $id)
                            ->orderBy("nama_masterkarir", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qmasterkarir              = new masterkarirModel;
        # ---------------
       // $qmasterkarir->kode       = setString($request->kode);
      
        $qmasterkarir->nama_masterkarir      = setString($request->nama_masterkarir);
        $qmasterkarir->user_id          = setString(Auth::user()->id);
        $qmasterkarir->create_at        = setString(date('Y-m-d H:i:s'));
        $qmasterkarir->update_at        = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qmasterkarir->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE masterkarir (" . $qmasterkarir->id_masterkarir . ") " . strtoupper($request->nama_masterkarir), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("m_masterkarir")
                             ->where("id_masterkarir", $request->id_masterkarir)
                            ->update([ "nama_masterkarir"=>$request->nama_masterkarir,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE masterkarir(" . $request->id_masterkarir . ") " . strtoupper($request->nama_masterkarir), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_masterkarir")
                             ->where("id_masterkarir", $request->id_masterkarir)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE pa_asterkarir (" . $request->id_masterkarir . ") " . strtoupper($request->nama_masterkarir), Auth::user()->id, $request);
    }
}
