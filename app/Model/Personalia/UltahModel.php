<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\UserModel;

class UltahModel extends Model
{
	 protected $table    = "p_karyawan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*",db::Raw("DATE_FORMAT(a.tgl_lahir, '%d/%m/%Y') as Tanggal_Lahir"),"b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif", 1)
                            ->orderBy("a.tgl_masuk", "ASC");
        

        
        if(session()->has("SES_SEARCH_UT_CABANG")) {
            if(session()->get("SES_SEARCH_UT_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_UT_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_UT_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_UT_DEPARTEMEN") != '-'){
                $query->where("a.id_departemen", session()->get("SES_SEARCH_UT_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_UT_JABATAN")) {
            if(session()->get("SES_SEARCH_UT_JABATAN") != '-'){
                $query->where("a.id_jabatan", session()->get("SES_SEARCH_UT_JABATAN"));
            }
            
        }


        if(session()->has("SES_SEARCH_UT_KARYAWAN")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWAN") . "%")
                  ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWAN") . "%")
                  ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWAN") . "%")
                  ->orwhere("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
    //
}
