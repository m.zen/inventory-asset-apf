<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class PermanenkerjaModel extends Model
{
     protected $table    = "pa_permanenkerja";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {

 if ($idkaryawan="All")
        {
                $query  = DB::table("pa_permanenkerja as a")
                           ->select("a.id_permanenkerja","a.id_karyawan","e.nama_karyawan","e.nik", "a.tgl_awal","a.tgl_akhir","a.tgl_tandatangan","a.status_proses","a.aktif",
                              "a.statuskerja","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.tempat_lahir","e.tgl_lahir","a.gaji_pokok"
                              ,"e.no_ktp","e.jenis_kelamin","alamat","a.tunj_jabatan","a.tunj_jabatan","a.tunj_transport","a.tunj_makan",
                              "a.tunj_kemahalan","a.tunj_jaga")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->orderBy("id_permanenkerja", "asc");
        }
        else
        {
            $query  = DB::table("pa_kontrakkerja")
                           ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_permanenkerja", "asc");
    
        }








        $query  = DB::table("pa_permanenkerja")
                           ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_permanenkerja", "asc");




        if(session()->has("SES_SEARCH_PERMANEN")) {
            $query->where("status_proses", "LIKE", "%" . session()->get("SES_SEARCH_PERMANEN") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getCetakPermanen($id)
    {
        $query  = DB::table("pa_permanenkerja as a")
                           ->select("a.id_permanenkerja","a.no_surat","a.id_karyawan","e.nama_karyawan","e.nik","e.tgl_masuk", "a.tgl_awal",
                              "a.statuskerja","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.tempat_lahir","a.gaji_pokok"
                              ,"e.no_ktp","e.jenis_kelamin","alamat","a.tunj_jabatan","a.tunj_jabatan","a.tunj_transport","a.tunj_makan",
                              "a.tunj_kemahalan","a.tunj_jaga")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->where("id_permanenkerja",$id)
                            ->orderBy("id_permanenkerja", "asc");
                    

        $result = $query->get();

        return $result;
    }
    public function getProfileKry($id) {
        $query  =DB::table("p_karyawan")
                            ->select("id_cabang","id_departemen","id_jabatan","gaji_pokok","tunj_transport",
                                "tunj_jabatan","tunj_jaga","tunj_kemahalan","tunj_makan")
                            ->where("id_karyawan", $id)
                            ->orderBy("id_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }
    public function getFormatskt()
     {
        $query  = DB::table("m_profilesystem")
                           ->select("tahunsktetap","formatsktetap","nosktetap");
        $result = $query->get();

        return $result;
    }    

    public function getProfile($id) {
        $query  = DB::table("pa_permanenkerja")
                            ->select("*")
                            ->where("id_permanenkerja", $id)
                            ->orderBy("id_permanenkerja", "asc");

        $result = $query->get();

        return $result;
    }    

    public function CekPermanen($idkry,$tglawal,$tglakhir) {
       
        $query  = DB::table("pa_permanenkerja")
                            ->select("*")
                            ->where("id_karyawan", $idkry)
                            ->where("tgl_awal", setYMD($tglawal,"/"));
                            
        $result = $query->count();
        

        return $result;
    }
 
    public function createData($request) {
        $qpermanenkerja              = new PermanenkerjaModel;
        $qdatagaji         = $this->getProfileKry($request->id_karyawan)->first();
        # ---------------
       // $qpermanenkerja->kode       = setString($request->kode);
        $qViewFormatsurat      = $qpermanenkerja->getFormatskt()->first();
        $nourut                = (string)$qViewFormatsurat->nosktetap;
        $formatsk              = $qViewFormatsurat->formatsktetap;
        $tahunskp              = (string)$qViewFormatsurat->tahunsktetap;
        $bulanskp              = getBulanRomawi(date("m", strtotime($request->tgl_awal)));
        $no_surat              =   sprintf("%03s",$nourut) .$formatsk ."/" .$bulanskp ."/" .$tahunskp;
        $qpermanenkerja->id_karyawan            = setString($request->id_karyawan);
        $qpermanenkerja->id_cabang              = setString($request->id_cabang);
        $qpermanenkerja->id_departemen          = setString($request->id_departemen );
        $qpermanenkerja->id_jabatan       	    = setString($request->id_jabatan);
        $qpermanenkerja->no_surat       	    = $no_surat;
        $qpermanenkerja->statuskerja            = "PERMANEN";
        $qpermanenkerja->tgl_awal          	    = setYMD($request->tgl_awal,"/"); 
        $qpermanenkerja->gaji_pokok    		    = setNoComma($request->gaji_pokok);
        $qpermanenkerja->tunj_jabatan		    = ($request->tunj_jabatan==null)?0:setNoComma($request->tunj_jabatan);
        $qpermanenkerja->tunj_transport		    = ($request->tunj_transport==null)?0:setNoComma($request->tunj_transport);
        $qpermanenkerja->tunj_jaga               = ($request->tunj_jaga==null)?0:setNoComma($request->tunj_jaga);
        $qpermanenkerja->tunj_kemahalan          = ($request->tunj_kemahalan==null)?0:setNoComma($request->tunj_kemahalan);
        $qpermanenkerja->tunj_makan              = ($request->tunj_makan==null)?0:setNoComma($request->tunj_makan);
        $qpermanenkerja->gaji_pokok_awal         =  $qdatagaji->gaji_pokok_awal;
        $qpermanenkerja->tunj_jabatan_awal       =  $qdatagaji->tunj_jabatan_awal;
        $qpermanenkerja->tunj_transport_awal     =  $qdatagaji->tunj_transport_awal;
        $qpermanenkerja->tunj_jaga_awal          =  $qdatagaji->tunj_jaga_awal;
        $qpermanenkerja->tunj_kemahalan_awal     =  $qdatagaji->tunj_kemahalan_awal;
        $qpermanenkerja->tunj_makan_awal         =  $qdatagaji->tunj_makan_awal;
        $qpermanenkerja->status_proses           = "Proses";
        $qpermanenkerja->aktif                   = "Non Aktif";
        $qpermanenkerja->user_id                 = setString(Auth::user()->id);
        $qpermanenkerja->create_at               = setString(date('Y-m-d H:i:s'));
        $qpermanenkerja->update_at               = setString(date('Y-m-d H:i:s'));
        # ---------------
        $qpermanenkerja->save();
          /* ----------
			update No Urut SKP
          ------------*/

         DB::update("UPDATE m_profilesystem SET nosktetap = nosktetap+1"); 






        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE permanenkerja (" . $qpermanenkerja->id_permanenkerja . ") " . strtoupper($request->id_permanenkerja), Auth::user()->id, $request);
    }

    public function updateData($request) {
    	$qpermanenkerja              = new PermanenkerjaModel;
        # ---------------
       // $qpermanenkerja->kode       = setString($request->kode);
        $qViewFormatsurat      = $qpermanenkerja->getFormatskt()->first();
        $nourut                = (string)$qViewFormatsurat->nosktetap;
        $formatsk              = $qViewFormatsurat->formatsktetap;
        $tahunskp              = (string)$qViewFormatsurat->tahunsktetap;
        $bulanskp              = getBulanRomawi(date("m", strtotime(setYMD($request->tgl_awal,"/"))));
      
        $no_surat              =   sprintf("%03s",$nourut) ."/" .$formatsk ."/" .$bulanskp ."/" .$tahunskp;
       
         DB::table("pa_permanenkerja")
                             ->where("id_permanenkerja", $request->id)
                            ->update(["id_cabang"=>$request->id_cabang,
                            	       "no_surat"=>$no_surat,
                            			"id_departemen"=>$request->id_departemen,
                            			"id_jabatan"=>$request->id_jabatan,
                            			"statuskerja"=>$request->statuskerja,
                                        "tgl_awal"=>setYMD($request->tgl_awal,"/"),
                                        "gaji_pokok"=>setNoComma($request->gaji_pokok),
                                        "tunj_jabatan"=>setNoComma($request->tunj_jabatan),
                                        "tunj_transport"=>setNoComma($request->tunj_transport),
                                        "tunj_jaga"=>setNoComma($request->tunj_jaga),
                                        "tunj_kemahalan"=>setNoComma($request->tunj_kemahalan),
                                        "tunj_makan"=>setNoComma($request->tunj_makan),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE permanenkerja(" . $request->id_permanenkerja . ") " . strtoupper($request->nama_permanenkerja), Auth::user()->id, $request);
    }
    public function updateaktivasiData($request) {
        
        
         DB::table("pa_permanenkerja")
                             ->where("id_permanenkerja", $request->id)
                            ->update([ "status_proses"=>"SELESAI",
                                        "tgl_tandatangan"=>setYMD($request->tgl_tandatangan,"/"),
                                        "aktif"=>"AKTIF",
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        

        //update karyawan
        if($request->updatekaryawan=='2')
        {
           
            DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "tgl_awal"=>$request->tgl_awal,
                                        "status_kerja"=>"TETAP",
                                        "gaji_pokok"=> setNoComma($request->gaji_pokok),
                                        "tunj_jabatan"=>($request->tunj_jabatan==null)?0:setNoComma($request->tunj_jabatan),
                                        "tunj_transport"=> ($request->tunj_transport==null)?0:setNoComma($request->tunj_transport),
                                        "tunj_jaga"=> ($request->tunj_jaga==null)?0:setNoComma($request->tunj_jaga),
                                        "tunj_kemahalan"=> ($request->tunj_kemahalan==null)?0:setNoComma($request->tunj_kemahalan),
                                        "tunj_makan" => ($request->tunj_makan==null)?0:setNoComma($request->tunj_makan),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);    
        }                    

        
                           
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE permanenkerja(" . $request->id_permanenkerja . ") " . strtoupper($request->nama_permanenkerja), Auth::user()->id, $request);
    }
    public function removeData($request) {
         DB::table("pa_permanenkerja")
            ->where("id_permanenkerja", $request->id_permanenkerja)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE permanenkerja (" . $request->id_permanenkerja . ") " . strtoupper($request->nama_permanenkerja), Auth::user()->id, $request);
    }
}
