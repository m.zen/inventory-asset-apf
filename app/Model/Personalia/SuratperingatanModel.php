<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class SuratperingatanModel extends Model
{
    protected $table    = "pa_peringatan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("pa_peringatan as a")
                           ->select("a.*","b.nama_karyawan",db::Raw("DATE_FORMAT(a.tgl_berlaku, '%d/%m/%Y') as Tanggal_berlaku") ,db::Raw("DATE_FORMAT(a.tgl_akhir, '%d/%m/%Y') as Tanggal_akhir"))
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->where("a.status_proses","=","APPROVE")
                            ->orderBy("id_peringatan", "asc");

        if(session()->has("SES_SEARCH_AWAL")) {
             $awal = session()->get("SES_SEARCH_AWAL");
             $awal = setYMD($awal,"/");
        }
        if(session()->has("SES_SEARCH_AKHIR")) {
             $akhir = session()->get("SES_SEARCH_AKHIR");
             $akhir = setYMD($akhir,"/");   
           
            $query->whereBetween("a.tgl_berlaku", [$awal, $akhir]);
            
        }

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_TINGKATSP")) {
            if(session()->get("SES_SEARCH_TINGKATSP") != '-'){
                $query->where("tingkat_sp", session()->get("SES_SEARCH_TINGKATSP"));
            }
            
        }
        if(session()->has("SES_SEARCH_KATEGORISP")) {
            if(session()->get("SES_SEARCH_KATEGORISP") != '-'){
                $query->where("kategori_sp", session()->get("SES_SEARCH_KATEGORISP"));
            }
            
        }
        
        




        if(session()->has("SES_SEARCH_PERINGATAN")) {
            $query->where("b.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PERINGATAN") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
    public function getList_proses($request=null, $offset=null, $limit=null) {
        $query  = DB::table("pa_peringatan as a")
                           ->select("a.*","b.nama_karyawan",db::Raw("DATE_FORMAT(a.tgl_berlaku, '%d/%m/%Y') as Tanggal_berlaku"))
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->where("a.status_proses","!=","APPROVE")
                            ->orderBy("id_peringatan", "asc");

     if(session()->has("SES_SEARCH_PERINGATAN")) {
            $query->where("b.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PROSESPERINGATAN") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getCetaksp($id)
    {
       
        $query  = DB::table("pa_peringatan as a")
                           ->select("a.id_peringatan","a.id_karyawan","b.nama_karyawan","b.nik","a.no_surat", "a.tgl_berlaku","a.tgl_akhir","a.tgl_sp",
                           	"a.tingkat_sp","c.nama_jabatan","a.keterangan","d.nama_cabang")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_jabatan as c","c.id_jabatan","=","b.id_jabatan")
                             ->leftjoin("m_cabang as d","d.id_cabang","=","b.id_cabang")
                            ->where("id_peringatan", $id)
                            ->orderBy("a.id_peringatan", "asc");
     
        $result = $query->get();

        return $result;
    }
   

    public function getProfile($id) {
        $query  = DB::table("pa_peringatan")
                            ->select("*")
                            ->where("id_peringatan", $id)
                            ->orderBy("id_peringatan", "asc");

        $result = $query->get();

        return $result;
    }  
     public function getKry($id) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.id_cabang","a.id_departemen","a.id_jabatan","b.nama_jabatan",
                                "a.nama_karyawan")
                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                            ->where("a.id_karyawan", $id);

        $result = $query->get()->first();

        return $result;
    }      

    public function CekSP($no_surat) {
       
        $query  = DB::table("pa_peringatan")
                            ->select("*")
                            ->where("no_surat", $no_surat);
                            
        $result = $query->count();
        

        return $result;
    }
 
    public function createData($request) {
        $qperingatan              = new SuratperingatanModel;

        # ---------------
       // $qperingatan->kode       = setString($request->kode);
        $id            = $request->id_karyawan;
        $qKary         = $this->getKry($id);
        $id_departemen = $qKary->id_departemen;
        $id_cabang     = $qKary->id_cabang;
        $id_jabatan    = $qKary->id_jabatan;
        $id_approve    = $request->id_approve;
        $qKary2        = $this->getKry($id_approve);
        $pejabat = $qKary2->nama_karyawan;
        $jabatan = $qKary2->nama_jabatan;


      
        $qperingatan->id_karyawan       = setString($request->id_karyawan);
        $qperingatan->no_surat          = setString($request->no_surat);
        $qperingatan->tgl_sp            = setYMD($request->tgl_sp,"/"); 
        $qperingatan->tingkat_sp        = setString($request->tingkat_sp);
        $qperingatan->kategori_sp       = setString($request->kategori_sp);
       // $qperingatan->keterangan        = setString($request->keterangan);
        $qperingatan->tgl_berlaku       = setYMD($request->tgl_berlaku,"/"); 
        $qperingatan->tgl_akhir       	= setYMD($request->tgl_akhir,"/");
        $qperingatan->id_jabatan        = setString($id_jabatan);
        $qperingatan->id_departemen     = setString($id_departemen);
        $qperingatan->id_cabang         = setString($id_cabang);
        $qperingatan->id_approve         = setString($id_approve);
        $qperingatan->pejabat           = setString($pejabat);
        $qperingatan->jabatan_disurat   = setString($request->jabatan_disurat);
        $qperingatan->status_proses     = "PROSES";
        $qperingatan->mengingat1        = setString($request->mengingat1);
        $qperingatan->mengingat2        = setString($request->mengingat2);
        $qperingatan->mengingat3        = setString($request->mengingat3);
        $qperingatan->mengingat4        = setString($request->mengingat4);
        $qperingatan->menimbang1        = setString($request->menimbang1);
        $qperingatan->menimbang2        = setString($request->menimbang2);
        $qperingatan->menimbang3        = setString($request->menimbang3);
        $qperingatan->menimbang4        = setString($request->menimbang4);
        $qperingatan->tembusan1        = setString($request->tembusan1);
        $qperingatan->tembusan2        = setString($request->tembusan2);
        $qperingatan->tembusan3        = setString($request->tembusan3);
        $qperingatan->tembusan4        = setString($request->tembusan4);
        $qperingatan->tembusan5        = setString($request->tembusan5);
        $qperingatan->user_id           = setString(Auth::user()->id);
        $qperingatan->create_at         = setString(date('Y-m-d H:i:s'));
        $qperingatan->update_at         = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qperingatan->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE peringatan (" . $qperingatan->id_peringatan . ") " . strtoupper($request->id_peringatan), Auth::user()->id, $request);
    }

    public function updateData($request) {

        $id            = $request->id_karyawan;
        $qKary         = $this->getKry($id);
        $id_departemen = $qKary->id_departemen;
        $id_cabang     = $qKary->id_cabang;
        $id_jabatan    = $qKary->id_jabatan;
        $id_approve    = $request->id_approve;
         $qKary2        = $this->getKry($id_approve);
        $pejabat = $qKary2->nama_karyawan;
        $jabatan = $qKary2->nama_jabatan;

         DB::table("pa_peringatan")
                             ->where("id_peringatan", $request->id)
                            ->update([ "no_surat"=>$request->no_surat,
                            			"id_karyawan"=>$request->id_karyawan,
                                        "tgl_sp"=>setYMD($request->tgl_sp,"/"),
                            			"tingkat_sp"=>$request->tingkat_sp,
                                        "kategori_sp"=> setString($request->kategori_sp),
                            		    "keterangan"=>setString($request->keterangan),
                                        "tgl_berlaku"=>setYMD($request->tgl_berlaku,"/"),
                                        "tgl_akhir"=>setYMD($request->tgl_akhir,"/"),
                                        "id_approve"=> setString($request->id_approve),
                                        "id_jabatan" => setString($id_jabatan),
                                        "id_departemen"  => setString($id_departemen),
                                        "id_cabang"  => setString($id_cabang),
                                        "pejabat"   => setString($pejabat),
                                        "id_approve"=>setString($id_approve),
                                        "pejabat"=>setString($pejabat),
                                        "jabatan_disurat"=>setString($request->jabatan_disurat),
                                        "mengingat1"=>setString($request->mengingat1),
                                        "mengingat2"=>setString($request->mengingat2),
                                        "mengingat3"=>setString($request->mengingat3),
                                        "mengingat4"=>setString($request->mengingat4),
                                        "menimbang1"=>setString($request->menimbang1),
                                        "menimbang2"=>setString($request->menimbang2),
                                        "menimbang3"=>setString($request->menimbang3),
                                        "menimbang4"=>setString($request->menimbang4),
                                        "tembusan1"=>setString($request->tembusan1),
                                        "tembusan2"=>setString($request->tembusan2),
                                        "tembusan3"=>setString($request->tembusan3),
                                        "tembusan4"=>setString($request->tembusan4),
                                        "tembusan5"=>setString($request->tembusan5),
                                          "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE peringatan(" . $request->id_peringatan . ") " . strtoupper($request->nama_peringatan), Auth::user()->id, $request);
    }
    public function update_approve($request) {
         DB::table("pa_peringatan")
                             ->where("id_peringatan", $request->id)
                            ->update([ "status_proses"=>$request->status_proses,

                                        "komentar"=>$request->komentar,
                                        
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
       
                           
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE peringatan(" . $request->id_peringatan . ") " . strtoupper($request->nama_peringatan), Auth::user()->id, $request);
    }
    public function removeData($request) {
         DB::table("pa_peringatan")
            ->where("id_peringatan", $request->id_peringatan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE peringatan (" . $request->id_peringatan . ") " . strtoupper($request->nama_peringatan), Auth::user()->id, $request);
    }
}
