<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\UserModel;
use Excel;
use App\Model\Master\CabangModel;
use App\Model\Master\ProfilesystemModel;
// ------------------- START -------------------
use App\Model\Master\CoreModel;
// -------------------  END  -------------------
class CalonKaryawanModel extends Model
{
     protected $table    = "p_calon_karyawan";
    public $timestamps= false ;
    // Rule::unique('p_calon_karyawan')->ignore($user->id, 'user_id');

    public function getList($request=null, $offset=null, $limit=null) {
            $query  = DB::table("p_calon_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.status_kerja", "CALON")
                            ->orwhere("a.status_kerja", "PROSES")
                            ->orwhere("a.status_kerja", "AKTIF")
                            ->orwhere("a.status_kerja", "TIDAK AKTIF")
                            // ->where("a.aktif", 1)
                            ->orderBy("a.id_karyawan", "DESC");

        if(session()->has("SES_SEARCH_KARYAWAN")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%")
                ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%")
                  ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%")
                  ->orwhere("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();
        return $result;
    }

    public function lastIdKaryawan(){
        $query = DB::table("p_karyawan")->orderBy("id_karyawan", "DESC")->select("id_karyawan")->first();
        return $query;
    }

    public function lastIdCalonKaryawan(){
        $query = DB::table("p_calon_karyawan")->orderBy("id_karyawan", "DESC")->select("id_karyawan")->first();
        return $query;
    }
    
    public function getStatuspajak($id_ptkp)
    {
      $query  = DB::table("p_ptkp")
                ->select("status")
                ->where("id_ptkp",$id_ptkp)->get()->first();
                           
       if(empty($query))
       {
            $result=0;
       }
       else
       {
        $result = $query->status; 
       }    
       

        return $result;

    } 
    public function getDetailOrangTuaKeluarga($id) {
        $query  = DB::table("pa_keluarga")
                            ->select("*")
                            ->where("id_karyawan", $id)
                            ->where("hubungan", "Ibu")
                            ->orwhere("hubungan", "Ayah")
                            ->orderBy("id_keluarga", "asc");

        $result = $query->get();

        return $result;
    } 
    public function getProfile($id) {
        
	$query  =DB::table("p_calon_karyawan as a")
                            ->select("a.*","a.tgl_masuk","a.tgl_keluar","a.status_kerja","b.nama_cabang","c.nama_departemen","d.nama_jabatan","f.nama_pendidikan"
                            ,"g.status")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_pendidikan as f","f.id_pendidikan","=","a.id_pendidikan") 
                            ->leftjoin("p_ptkp as g","g.id_ptkp","=","a.id_ptkp") 
                            ->where("a.id_karyawan", $id)
                            ->orderBy("a.nama_karyawan", "DESC");
    
        $result = $query->get();

        return $result;
    }    


   public function getDetailKeluarga($id) {
        $query  = DB::table("pa_keluarga")
                            ->select("*")
                            ->where("id_karyawan", $id)
                           
                            ->orderBy("id_keluarga", "asc");

        $result = $query->get();

        return $result;
    }    
public function getDetailKontak($id) {
        $query  = DB::table("pa_kontak")
                            ->select("*")
                            ->where("id_karyawan", $id)
                            ->orderBy("id_kontak", "asc");

        $result = $query->get();

        return $result;
    }   

 public function getDetailPendidikan($id) {
        $query  = DB::table("pa_rpendidikan")
                            ->select("*")
                            ->where("id_karyawan", $id)
                           
                            ->orderBy("id_rpendidikan", "asc");

        $result = $query->get();

        return $result;
    }  

    public function getDetailPengalaman($id) {
        $query  = DB::table("pa_pengalaman")
                            ->select("*")
                            ->where("id_karyawan", $id)
                           
                            ->orderBy("id_pengalaman", "asc");

        $result = $query->get();
       
        return $result;
    }
    public function getJabatan($id) {
        $query  = DB::table("m_jabatan as a")
                             ->select("b.id_level","a.tunj_transport","b.tunj_jabatan")
                            ->leftjoin("m_level as b","b.id_level","=","a.id_level")
                            ->where("a.id_jabatan", $id);
        $result = $query->get();

        return $result;
    }         
    public function createData($request) {

        $qkaryawan              = new CalonKaryawanModel;
        $qProfilSystem          = new ProfilesystemModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        $getProfileSystem       = $qProfilSystem->getProfile(1)->first();
        $nourut = $getProfileSystem->no_urut+1;
        $id_karyawan = $request->id_karyawan;
        if($nourut == $id_karyawan){
            $newIdk = $nourut;
        }else{
            $newIdk = $id_karyawan;
        }
        $karakterid             = "00000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $id_karyawan            = setString($cut_text.$newIdk);
        // $tgl_awalcuti =date("Y-m-d H:i:s", strtotime("+1 years", strtotime(setYMD($request->tgl_masuk, "/"))));
        // $tgl_akhircuti =date("Y-m-d H:i:s", strtotime("+2 years", strtotime(setYMD($request->tgl_masuk, "/"))));
        $tgl_awalcuti = date('Y-m-d', strtotime($request->tgl_masuk. ' + 1 years'));
        $tgl_akhircuti = date('Y-m-d', strtotime($request->tgl_masuk. ' + 2 years'));
   //     dd(setYMD($request->tgl_masuk, "/"),setYMD($request->tgl_lahir,"/"), $tgl_akhircuti);
        # ---------------
        $qkaryawan->id_karyawan         = setString($newIdk);
        $qkaryawan->nik                 = setString($request->nik);
        $qkaryawan->nama_karyawan       = setString($request->nama_karyawan);
        $qkaryawan->nama_panggilan       = setString($request->nama_panggilan);
        $qkaryawan->id_cabang           = setString($request->id_cabang);
        $qkaryawan->id_departemen       = setString($request->id_departemen);
        $qkaryawan->id_jabatan          = setString($request->id_jabatan);
        $qkaryawan->tgl_melamar         = setYMD($request->tgl_melamar,"/");
        $qkaryawan->no_rekening         = $request->no_rekening;
        $qkaryawan->anrekening          = $request->anrekening;
        $qkaryawan->no_kk               = setString($request->no_kk);
        $qkaryawan->email               = setString($request->email);
        $qkaryawan->noabsen             = setString($request->noabsen);
        $qkaryawan->no_ktp              = $request->no_ktp;
        $qkaryawan->no_sim              = $request->no_sim;
        $qkaryawan->nonpwp              = $request->nonpwp;
        $qkaryawan->nobpjskes           = $request->nobpjskes;
        $qkaryawan->nobjstk             = $request->nobjstk;
        $qkaryawan->tempat_lahir        = $request->tempat_lahir;
        $qkaryawan->tgl_lahir           = setYMD($request->tgl_lahir,"/");
        $qkaryawan->jenis_kelamin       = $request->jenis_kelamin;
        $qkaryawan->golongan_darah      = $request->golongan_darah;
        $qkaryawan->agama               = $request->agama;
        $qkaryawan->status_nikah        = $request->status_nikah;
        $qkaryawan->id_pendidikan       = $request->id_pendidikan;
        $qkaryawan->jurusan             = setString($request->jurusan);
        //$qkaryawan->id_ptkp             = $request->id_ptkp;
        $qkaryawan->id_bank          	= $request->id_bank;
        $qkaryawan->alamat              = setString($request->alamat);
        $qkaryawan->nama_ibu            = setString($request->nama_ibu);
        $qkaryawan->alamat_valid        = $request->alamat_valid;
        $qkaryawan->no_telpon           = $request->no_telpon;
        $qkaryawan->hp                  = str_replace('-', '', $request->hp);
        $qkaryawan->tgl_awalcuti        = $tgl_awalcuti ;
        $qkaryawan->tgl_akhircuti       = $tgl_akhircuti ;
        // $qkaryawan->sisacuti            =0;
        $qkaryawan->tgl_berlaku_ktp     = setYMD($request->tgl_berlaku_ktp,"/");
        $qkaryawan->status_kerja        = "CALON";
        $qkaryawan->user_id             = setString(Auth::user()->id);
        $qkaryawan->create_at           = setString(date('Y-m-d H:i:s'));
        $qkaryawan->update_at           = setString(date('Y-m-d H:i:s'));
        
        # ---------------
        $qkaryawan->save();

        DB::table("m_profilesystem")->where("id_profilesystem", 1)->update([ "no_urut"=> setString($newIdk)]);
       
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE KARYAWAN (" . $qkaryawan->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function updateData($request) {

        $qkaryawan              = new CalonKaryawanModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        $tunj_jabatan            = $qJabatan->tunj_jabatan;
        $tunj_transport          = $qJabatan->tunj_transport;




        
                         DB::table("p_calon_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ 
                                "nama_karyawan"=>$request->nama_karyawan,
                                "nama_panggilan"=>$request->nama_panggilan,
                                "id_cabang"=>$request->id_cabang,
                                "id_departemen"=>$request->id_departemen,
                                "id_jabatan"=>$request->id_jabatan,
                                "tgl_melamar"=>setYMD($request->tgl_melamar, "/"),
                                "email"=>$request->email,
                                "noabsen"=>$request->noabsen,
                                "no_ktp"=>$request->no_ktp,
                                "no_sim"=>$request->no_sim,
                                "tempat_lahir"=>$request->tempat_lahir,
                                "tgl_lahir"=>setYMD($request->tgl_lahir, "/"),
                                "tgl_berlaku_ktp"=>setYMD($request->tgl_berlaku_ktp, "/"),
                                "jenis_kelamin"=>$request->jenis_kelamin, 
                                //"id_ptkp"=>$request->id_ptkp, 
                                "id_bank"=>$request->id_bank, 
                                "nonpwp"=>$request->nonpwp,
                                "nobpjskes"=>$request->nobpjskes,
                                "nobjstk"=>$request->nobjstk,
                                "no_rekening"=>$request->no_rekening,
                                "anrekening"=>$request->asrekening,
                                "agama"=>$request->agama,
                                "status_nikah"=>$request->status_nikah,
                                "golongan_darah"=>$request->golongan_darah,
                                "id_pendidikan"=>$request->id_pendidikan,
                                "jurusan"=>setString($request->jurusan),
                                "alamat"=>$request->alamat,
                                "nama_ibu"=> setString($request->nama_ibu),
                                "alamat_valid"=>$request->alamat_valid,
                                "no_telpon"=>$request->no_telpon,
                                "hp"=> str_replace('-', '', $request->hp),
                                // "perusahaan"=>$request->perusahaan,
                                "user_id"=>setString(Auth::user()->id),
                                "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function prosesAjukan($request) {

        $qkaryawan              = new KaryawanModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        
                         DB::table("p_calon_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "nama_panggilan"=>$request->nama_panggilan,
				"nama_karyawan"=>$request->nama_karyawan,
                                 "status_kerja" => "PROSES",
                                 "tgl_pengajuan" => setYMD($request->tgl_pengajuan, "/"),
                                //  "user_id"=> setString(Auth::user()->id),
                                        //  "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function prosesApprove($request) {

        $qkaryawan              = new KaryawanModel;
        $qCabang                = new CabangModel;
        $qProfilSystem          = new ProfilesystemModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        $getCabang              = $qCabang->getProfile($request->id_cabang)->first();
        $getProfileSystem       = $qProfilSystem->getProfile(1)->first();
        $id_karyawan            = (int)$request->id_karyawan;
        
        $cek_calon = DB::table("p_calon_karyawan")->where("id_karyawan", $id_karyawan)->first();
        $nama_inisialjabatan= '';
        if($request->id_cabang == 1){
            $qinisial = DB::table("m_inisialjabatan")
                        ->where([
                                    ["kategoricabang", "pusat"],
                                    ["id_jabatan", $request->id_jabatan]
                                ])
                        ->first();
            if($qinisial != null){
                $nama_inisialjabatan = $qinisial->nama_inisialjabatan;
            }
        }else{
            $qinisial = DB::table("m_inisialjabatan")
                        ->where([
                                    ["kategoricabang", "Cabang"],
                                    ["id_departemen", $request->id_departemen],
                                    ["id_jabatan", $request->id_jabatan]
                                ])
                        ->first();
            if($qinisial != null){
                $nama_inisialjabatan = $qinisial->nama_inisialjabatan;
            }
        }

        if($cek_calon == null){
            $ambiltgl = substr($request->tgl_masuk,0,2);                      
            $ambilbln = substr($request->tgl_masuk,3,2);                      
            $ambilthn = substr($request->tgl_masuk,8,2);
            $kodecabang = $getCabang->kode_cabang;  
            $kodecabang = substr($kodecabang,-2);  
            
            // $nourut = $getProfileSystem->no_urut+1;
            $nik =  $ambiltgl.$ambilbln.'.'.$ambilthn.$kodecabang.'.'.$id_karyawan;
            $noabsen = str_replace('.','',$nik);
            $kodeawal = substr($noabsen,0,1);
            if($kodeawal >1){
                $noabsen= "1".$noabsen;
            }


            DB::table("p_calon_karyawan")
                ->where("id_karyawan", $request->id_karyawan)
                    ->update([ 
                            "nik"=> setString($nik),
                            "noabsen"=> setString($noabsen),
                            "nama_inisialjabatan"=> setString($nama_inisialjabatan),
                            "id_cabang_hire"=> $request->id_cabang,
                            "tgl_masuk"=>setYMD($request->tgl_masuk, "/"),
                            "lama_kontrak_percobaan" => $request->lama_kontrak_percobaan,
                            "status_kerja" => setString($request->status_kerja),
                            "alasan_reject" => null,
                            //  "user_id"=>setString(Auth::user()->id),
                            "update_at"=>setString(date('Y-m-d H:i:s'))    
                            ]);
        
        }else{
            $ambiltgl = substr($request->tgl_masuk,0,2);                      
            $ambilbln = substr($request->tgl_masuk,3,2);                      
            $ambilthn = substr($request->tgl_masuk,8,2);
            $kodecabang = $getCabang->kode_cabang;  
            $kodecabang = substr($kodecabang, -2);  

            $nik =  $ambiltgl.$ambilbln.'.'.$ambilthn.$kodecabang.'.'.$id_karyawan;
            $noabsen = str_replace('.','',$nik);
            $kodeawal = substr($noabsen,0,1);
            if($kodeawal >1){
                $noabsen= "1".$noabsen;
            }
            DB::table("p_calon_karyawan")
                ->where("id_karyawan", $id_karyawan)
                    ->update([ 
                            "nik"=> setString($nik),
                            "noabsen"=> setString($noabsen),
                            "nama_inisialjabatan"=> setString($nama_inisialjabatan),
                            "id_cabang_hire"=> $request->id_cabang,
                            "tgl_masuk"=>setYMD($request->tgl_masuk, "/"),
                            "lama_kontrak_percobaan" => $request->lama_kontrak_percobaan,
                            "status_kerja" => setString($request->status_kerja),
                            "alasan_reject" => null,
                            //  "user_id"=>setString(Auth::user()->id),
                            "update_at"=>setString(date('Y-m-d H:i:s'))    
                            ]);
        }

        $cek = DB::table("p_karyawan")->where("id_karyawan", $id_karyawan)->first();
        
        if(null == $cek){
            //salin calon
            $getdataId = DB::table("p_calon_karyawan")->where("id_karyawan", $id_karyawan)->get();
            
            $p_karyawan = json_decode($getdataId, true);
            $p_karyawan[0]["id_karyawan"] = $id_karyawan;
            $p_karyawan[0]["aktif"] = 1;
            DB::table("p_karyawan")
                        ->insert([
                                    $p_karyawan[0]
                                ]);
    
            // ------------------- START -------------------
            $qCore      = new CoreModel;
            $id_cabang_core     = (strlen($p_karyawan[0]["id_cabang"]) == 1) ? "00" . ($p_karyawan[0]["id_cabang"]-1) : "0" . ($p_karyawan[0]["id_cabang"]-1);
            $request->request->add(["nik"=>$p_karyawan[0]["nik"]
                                        , "nama_karyawan"=>$p_karyawan[0]["nama_karyawan"]
                                            , "id_cabang"=> $id_cabang_core
                                                , "id_jabatan"=>$p_karyawan[0]["id_jabatan"]
                                                    , "tgl_masuk"=>$p_karyawan[0]["tgl_masuk"]]);

            $qCore->createDataKaryawanCore($request);
            // -------------------  END  -------------------
            // DB::table("m_profilesystem")->where("id_profilesystem", 1)->update([ "no_urut"=> setString($nourut)]);
        }else{
            $getdataId = DB::table("p_calon_karyawan")->where("id_karyawan", $id_karyawan)->get();
            
            $p_karyawan = json_decode($getdataId, true);
            $p_karyawan[0]["id_karyawan"] = $id_karyawan;
            $p_karyawan[0]["aktif"] = 1;

            DB::table("p_karyawan")
                    ->where("id_karyawan", $request->id_karyawan)
                        ->update(
                                    $p_karyawan[0]
                                );
            // ------------------- START -------------------
            $qCore      = new CoreModel;
            $id_cabang_core     = (strlen($p_karyawan[0]["id_cabang"]) == 1) ? "00" . ($p_karyawan[0]["id_cabang"]-1) : "0" . ($p_karyawan[0]["id_cabang"]-1);
            $request->request->add(["nik"=>$p_karyawan[0]["nik"]
                                        , "nama_karyawan"=>$p_karyawan[0]["nama_karyawan"]
                                            , "id_cabang"=> $id_cabang_core
                                                , "id_jabatan"=>$p_karyawan[0]["id_jabatan"]
                                                    , "tgl_masuk"=>$p_karyawan[0]["tgl_masuk"]]);
            
            $qCore->createDataKaryawanCore($request);
            // -------------------  END  -------------------
        }
        
        
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function prosesTolak($request) {

        $qkaryawan              = new KaryawanModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        
        DB::table("p_calon_karyawan")
            ->where("id_karyawan", $request->id_karyawan)
        ->update([ 
            "status_kerja" => "CALON",
            "alasan_reject" => setString($request->alasan_reject),
            //  "user_id"=>setString(Auth::user()->id),
                    //  "update_at"=>setString(date('Y-m-d H:i:s'))    
                ]);
        # ---------------
        // DB::table("p_karyawan")->where("id_karyawan", $request->id_karyawan)->delete();
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

public function updateresignData($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                                       "alasan_keluar"=>$request->alasan_keluar,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
    public function removeData($request) {
         DB::table("p_calon_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE KARYAWAN (" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function importcsv($request){
        $rows = Excel::load($request->file('file_excel'))->get();
        foreach ($rows as $key => $value) {
            DB::table("p_karyawan")
                             ->where("nik" ,$value['employee_id'])
                              ->update([
                                    "sisacuti"=> $value['total'],
                               ]);
        }
    
    /* ----------
         Logs
        ----------------------- */
        $qLog       = new LogModel;
        # ---------------;
        $qLog->createLog("update penggajian dari import excel", Auth::user()->id, $request);
    }
}
