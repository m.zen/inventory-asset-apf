<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class PkwttModel extends Model
{
    protected $table    = "pa_pkwtt";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_pkwtt as a")
                           ->select("a.id_pkwtt","a.nama_pkwtt","status_proses","a.id_karyawan","a.no_surat", "a.tgl_pkwtt","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_karyawan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangbaru")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenbaru")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                             ->where("status_proses","=","APPROVE")   
                            ->orderBy("id_pkwtt", "desc");



        if(session()->has("SES_SEARCH_AWAL")) {
             $awal = session()->get("SES_SEARCH_AWAL");
             $awal = setYMD($awal,"/");
        }
        if(session()->has("SES_SEARCH_AKHIR")) {
             $akhir = session()->get("SES_SEARCH_AKHIR");
             $akhir = setYMD($akhir,"/");   
           
            $query->whereBetween("a.tgl_pkwtt", [$awal, $akhir]);
            
        }

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("id_cabangbaru", session()->get("SES_SEARCH_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $query->where("id_departemenbaru", session()->get("SES_SEARCH_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_JABATAN")) {
            if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $query->where("id_jabatanbaru", session()->get("SES_SEARCH_JABATAN"));
            }
            
        }
        if(session()->has("SES_SEARCH_NAMAPKWTT")) {
            if(session()->get("SES_SEARCH_NAMAPKWTT") != '-'){
                $query->where("nama_pkwtt", session()->get("SES_SEARCH_NAMAPKWTT"));
            }
            
        }

        if(session()->has("SES_SEARCH_INQPKWTT")) {
            $query->where("e.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_INQPKWTT") . "%")
             ->orwhere("a.no_surat", "LIKE", "%" . session()->get("SES_SEARCH_INQPKWTT") . "%")
                  ->orwhere("e.nik", "LIKE", "%" . session()->get("SES_SEARCH_INQPKWTT") . "%")
                  ->orwhere("a.nama_pkwtt", "LIKE", "%" . session()->get("SES_SEARCH_INQPKWTT") . "%");;
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

public function getList_proses($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_pkwtt as a")
                           ->select("a.id_pkwtt","a.nama_pkwtt","status_proses","a.id_karyawan","a.no_surat", "a.tgl_pkwtt","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_karyawan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangbaru")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenbaru")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->where("status_proses","!=","APPROVE")

                            ->orderBy("id_pkwtt", "desc");



        if(session()->has("SES_SEARCH_PKWTT")) {
            $query->where("e.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_PKWTT") . "%")
             ->orwhere("a.no_surat", "LIKE", "%" . session()->get("SES_SEARCH_PKWTT") . "%")
                  ->orwhere("e.nik", "LIKE", "%" . session()->get("SES_SEARCH_PKWTT") . "%")
                  ->orwhere("a.nama_pkwtt", "LIKE", "%" . session()->get("SES_SEARCH_PKWTT") . "%");;
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
    public function getProfile($id) {
        $query  = DB::table("pa_pkwtt")
                            ->select("*")
                            ->where("id_pkwtt", $id)
                           
                            ->orderBy("id_pkwtt", "asc");

        $result = $query->get();

        return $result;
    }    

     public function getProfilekry($id) {
        $query  = DB::table("p_karyawan")
                            ->select("id_cabang","id_departemen","id_jabatan")
                            ->where("id_karyawan", $id);
                          
        $result = $query->get();

        return $result;
    }    
  public function getCetakpkwtt($id)
    {
        $query  = DB::table("pa_pkwtt as a")
                           ->select("a.*","e.nama_karyawan","e.nik", "a.tgl_pkwtt","b.nama_cabang as nama_cabangawal","d.nama_jabatan as nama_jabatanawal","f.nama_jabatan as nama_jabatanbaru","g.nama_jabatan as nama_jabatantj","h.nama_cabang as nama_cabangbaru"
                              )
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangawal")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenawal")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanawal")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_jabatan as f","f.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("m_jabatan as g","g.id_jabatan","=","a.id_jabatantj")
                             ->leftjoin("m_cabang as h","h.id_cabang","=","a.id_cabangbaru")
                            ->where("id_pkwtt",$id)
                            ->orderBy("id_pkwtt", "asc");
                    

        $result = $query->get();

        return $result;
    }
 
    public function createData($request) {
        $qpkwtt              = new PkwttModel;
        $qKaryawan           = $this-> getProfilekry($request->id_karyawan)->first();
        //dd($qKaryawan);
       // dd($qKaryawan->id_cabang,$qKaryawan->id_jabatan);
        # ---------------
       // $qpkwtt->kode       = setString($request->kode);
        $qpkwtt->no_surat  			= setString($request->no_surat);
        $qpkwtt->id_karyawan         = setString($request->id_karyawan);
        $qpkwtt->id_cabangawal       = setString($qKaryawan->id_cabang);
        $qpkwtt->id_departemenawal   = setString($qKaryawan->id_departemen);
        $qpkwtt->id_jabatanawal      = setString($qKaryawan->id_jabatan);
        $qpkwtt->id_cabangbaru       = setString($request->id_cabangbaru);
        $qpkwtt->id_departemenbaru   = setString($request->id_departemenbaru );
        $qpkwtt->id_jabatanbaru      = setString($request->id_jabatanbaru);
        $qpkwtt->id_jabatantj      = setString($request->id_jabatantj);
        $qpkwtt->nama_pkwtt	         = setString($request->nama_pkwtt);
        $qpkwtt->tgl_pkwtt           = setYMD($request->tgl_pkwtt,"/"); 
        $qpkwtt->keterangan_pkwtt    =  setString($request->keterangan_pkwtt);
        $qpkwtt->status_proses       =  "PROSES";
        $qpkwtt->tembusan1           =  setString($request->tembusan1);
        $qpkwtt->tembusan2           =  setString($request->tembusan2);
        $qpkwtt->tembusan3           =  setString($request->tembusan3);
        $qpkwtt->tembusan4           =  setString($request->tembusan4);
        $qpkwtt->tembusan5            =  setString($request->tembusan5);



        
        
        

        // if($request->nama_pkwtt=="PROMOSI")
        // {
        //    if(!is_null($qpkwtt->tgl_probation))
        //    {
        //           $qpkwtt->tgl_probation       = setYMD($request->tgl_probation,"/"); 
        //    }
        //    else
        //    {
        //         $qpkwtt->tgl_probation       = null; 
        //    }
        // }
       
       
        $qpkwtt->status_proses       = "PROSES";
        $qpkwtt->user_id             = setString(Auth::user()->id);
        $qpkwtt->create_at           = setString(date('Y-m-d H:i:s'));
        $qpkwtt->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qpkwtt->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE pkwtt (" . $qpkwtt->id_pkwtt . ") " . strtoupper($request->id_pkwtt), Auth::user()->id, $request);
    }

    public function updateData($request) {
        $tgl_prob=null;
        if($request->nama_pkwtt=="PROMOSI")
        {
           if(!is_null($request->tgl_probation))
           {
                  $tgl_prob       = setYMD($request->tgl_probation,"/"); 
           }
           else
           {
                $tgl_prob       = null; 
           }
        }
         DB::table("pa_pkwtt")
                             ->where("id_pkwtt", $request->id)
                            ->update([ "no_surat"     =>$request->no_surat,
                            			"id_cabangbaru"     =>$request->id_cabangbaru,
                            			"id_departemenbaru" =>$request->id_departemenbaru,
                            			"id_jabatanbaru"    =>$request->id_jabatanbaru,
                            			"nama_pkwtt"        => setString($request->nama_pkwtt),
                                  "keterangan_pkwtt"  => setString($request->keterangan_pkwtt),
                                  "tgl_pkwtt"         => setYMD($request->tgl_pkwtt,"/"),
                                   "id_jabatantj"     => setString($request->id_jabatantj),
                                  "tembusan1"         =>  setString($request->tembusan1),
                                  "tembusan2"         =>  setString($request->tembusan2),
                                  "tembusan3"         =>  setString($request->tembusan3),
                                  "tembusan4"         =>  setString($request->tembusan4),
                                  "tembusan5"         =>  setString($request->tembusan5),
    
                                         "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE pkwtt(" . $request->id_pkwtt . ") " . strtoupper($request->nama_pkwtt), Auth::user()->id, $request);
    }




    public function updateData_approve($request) {
       
         DB::table("pa_pkwtt")
                             ->where("id_pkwtt", $request->id)
                            ->update([ "status_proses"=>$request->status_proses,
                                  "komentar"=>$request->komentar,
                                  
                                         "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                    ]);
     

          DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "id_cabang"       => setString($request->id_cabangbaru),
                                        "id_departemen"   => setString($request->id_departemenbaru ),
                                        "id_jabatan"      => setString($request->id_jabatanbaru),
                                       "user_id"          =>setString(Auth::user()->id),
                                       "update_at"        =>setString(date('Y-m-d H:i:s'))]);
                       
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE pkwtt(" . $request->id_pkwtt . ") " . strtoupper($request->nama_pkwtt), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_pkwtt")
            ->where("id_pkwtt", $request->id)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE pkwtt (" . $request->id_pkwtt . ") " . strtoupper($request->nama_pkwtt), Auth::user()->id, $request);
    }
}
