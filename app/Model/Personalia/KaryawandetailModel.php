<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class KaryawandetailModel extends Model
{
    protected $table    = "p_karyawan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik", "a.nama_karyawan","a.email","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->orderBy("a.id_cabang","a.id_karyawan", "DESC");




        if(session()->has("SES_SEARCH_KARYAWAN")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%");
                  ->where("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%");

        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  =DB::table("p_karyawan as a")
                            ->select("a.id_karyawan","a.nik","a.email", "a.nama_karyawan","a.tgl_masuk","a.tgl_akhir","a.status_kerja","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.id_karyawan", $id)
                            ->orderBy("a.nama_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qkaryawan              = new KaryawanModel;
        # ---------------
       // $qkaryawan->kode       = setString($request->kode);
        $qkaryawan->nik                   = setString($request->nik);
        $qkaryawan->nama_karyawan        = setString($request->nama_karyawan);
        $qkaryawan->id_cabang            = setString($request->id_cabang);
        $qkaryawan->id_departemen        = setString($request->id_departemen);
        $qkaryawan->id_jabatan            = setString($request->id_jabatan);
        $qkaryawan->tgl_masuk            = setYMD($request->tgl_masuk,"/");
        $qkaryawan->tgl_akhir            = setYMD($request->tgl_akhir,"/");
        $qkaryawan->status_kerja         = setString($request->status_kerja);
        $qkaryawan->email                = setString($request->email);
        $qkaryawan->user_id              = setString(Auth::user()->id);
        $qkaryawan->create_at            = setString(date('Y-m-d H:i:s'));
        $qkaryawan->update_at            = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qkaryawan->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE KARYAWAN (" . $qkaryawan->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "nama_karyawan"=>$request->nama_karyawan,
                                "id_cabang"=>$request->id_cabang,
                                "id_departemen"=>$request->id_departemen,
                                "id_jabatan"=>$request->id_jabatan,
                                "tgl_masuk"=>setYMD($request->tgl_masuk, "/"),
                                "tgl_akhir"=>setYMD($request->tgl_akhir, "/"),
                                "status_kerja"=>$request->status_kerja,
                                "email"=>$request->email,
                                                             
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE KARYAWAN (" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
}
