<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
use App\Model\Master\UserModel;

class KaryawanModel extends Model
{
     protected $table    = "p_karyawan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*",db::Raw("DATE_FORMAT(a.tgl_masuk, '%d/%m/%Y') as Tanggal_masuk"),"b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif", 1)
                            ->orderBy("a.tgl_masuk", "ASC");
        

        if(session()->has("SES_SEARCH_AWAL")) {
             $awal = session()->get("SES_SEARCH_AWAL");
             $awal = setYMD($awal,"/");
             
        }
        if(session()->has("SES_SEARCH_AKHIR")) {
             $akhir = session()->get("SES_SEARCH_AKHIR");
             $akhir = setYMD($akhir,"/");   
           
            $query->whereBetween("a.tgl_masuk", [$awal, $akhir]);

            
        }                    

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $query->where("a.id_departemen", session()->get("SES_SEARCH_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_JABATAN")) {
            if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $query->where("a.id_jabatan", session()->get("SES_SEARCH_JABATAN"));
            }
            
        }

         if(session()->has("SES_SEARCH_STATUSKERJA")) {
            if(session()->get("SES_SEARCH_STATUSKERJA") != '-'){
                $query->where("a.status_kerja", session()->get("SES_SEARCH_STATUSKERJA"));
            }
            
        }

        if(session()->has("SES_SEARCH_KARYAWAN")) {
            //$query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%")
              //    ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%")
                //  ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%")
                  //->orwhere("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWAN") . "%");

		$query->whereRaw("(nama_karyawan LIKE '%" . session()->get("SES_SEARCH_KARYAWAN") . "%' 
			OR nama_cabang LIKE '%" . session()->get("SES_SEARCH_KARYAWAN") . "%' 
			OR nik LIKE '%" . session()->get("SES_SEARCH_KARYAWAN") . "%' 
			OR nama_departemen LIKE '%" . session()->get("SES_SEARCH_KARYAWAN") . "%')");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
    public function getList_nonaktif($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*",db::Raw("DATE_FORMAT(a.tgl_keluar, '%d/%m/%Y') as Tanggal_keluar"),"b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_alasankeluar",db::Raw("DATE_FORMAT(a.tgl_masuk, '%d/%m/%Y') as Tanggal_masuk"))
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                             ->leftjoin("pa_alasankeluar as e","e.id_alasankeluar","=","a.alasan_keluar") 
                            ->where("a.aktif", 0)
                            ->orderBy("a.tgl_keluar", "ASC");

        

        if(session()->has("SES_SEARCH_AWAL")) {
             $awal = session()->get("SES_SEARCH_AWAL");
             $awal = setYMD($awal,"/");
             
        }
        if(session()->has("SES_SEARCH_AKHIR")) {
             $akhir = session()->get("SES_SEARCH_AKHIR");
             $akhir = setYMD($akhir,"/");   
           
            $query->whereBetween("a.tgl_keluar", [$awal, $akhir]);
           

            
        }                    

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $query->where("a.id_departemen", session()->get("SES_SEARCH_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_JABATAN")) {
            if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $query->where("a.id_jabatan", session()->get("SES_SEARCH_JABATAN"));
            }
            
        }

        if(session()->has("SES_SEARCH_KARYAWANNONAKTIF")) {
           // $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%")
                //  ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%")
                //  ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%")
                //  ->orwhere("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%");

		 $query->whereRaw("(nama_karyawan LIKE '%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%' 
			OR nama_cabang LIKE '%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%' 
			OR nik LIKE '%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%' 
			OR nama_departemen LIKE '%" . session()->get("SES_SEARCH_KARYAWANNONAKTIF") . "%')");

        }
        //dd(session()->get("SES_SEARCH_CABANG"));

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }



 public function getList_ulangtahun($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*",db::Raw("DATE_FORMAT(a.tgl_lahir, '%d/%m/%Y') as Tanggal_lahir"),"b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_alasankeluar",db::Raw("DATE_FORMAT(a.tgl_masuk, '%d/%m/%Y') as TanggaFl_masuk"))
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                             ->leftjoin("pa_alasankeluar as e","e.id_alasankeluar","=","a.alasan_keluar") 
                            ->where("a.aktif", 1)
                ->whereBetween(db::Raw("DATE_FORMAT(tgl_lahir,'%m %d')"), [db::Raw("DATE_FORMAT(CURDATE(),'%m %d') 
AND DATE_FORMAT((INTERVAL 3 DAY + CURDATE()),'%m %d')"), db::Raw("DATE_FORMAT((INTERVAL 3 DAY + CURDATE()),'%m %d')")])    

                            ->orderBy("a.tgl_keluar", "ASC");

              

        if(session()->has("SES_SEARCH_UT_CABANG")) {
            if(session()->get("SES_SEARCH_UT_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_UT_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_UT_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_UT_DEPARTEMEN") != '-'){
                $query->where("a.id_departemen", session()->get("SES_SEARCH_UT_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_UT_JABATAN")) {
            if(session()->get("SES_SEARCH_UT_JABATAN") != '-'){
                $query->where("a.id_jabatan", session()->get("SES_SEARCH_UT_JABATAN"));
            }
            
        }

        if(session()->has("SES_SEARCH_UT_KARYAWANNONAKTIF")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWANNONAKTIF") . "%")
                  ->orwhere("nama_cabang", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWANNONAKTIF") . "%")
                  ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWANNONAKTIF") . "%")
                  ->orwhere("nama_departemen", "LIKE", "%" . session()->get("SES_SEARCH_UT_KARYAWANNONAKTIF") . "%");
        }
        //dd(session()->get("SES_SEARCH_UT_CABANG"));

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getList_jaminan($request=null, $offset=null, $limit=null) {
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","e.nama_jaminan","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                             ->leftjoin("pa_jaminan as e","e.id_jaminan","=","a.id_jaminan") 
                            ->where("a.id_jaminan",">", 0)
              
                            ->orderBy("a.tgl_keluar", "ASC");

              

        if(session()->has("SES_SEARCH_JMN_CABANG")) {
            if(session()->get("SES_SEARCH_JMN_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_JMN_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_JMN_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_JMN_DEPARTEMEN") != '-'){
                $query->where("a.id_departemen", session()->get("SES_SEARCH_JMN_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_JMN_JABATAN")) {
            if(session()->get("SES_SEARCH_JMN_JABATAN") != '-'){
                $query->where("a.id_jabatan", session()->get("SES_SEARCH_JMN_JABATAN"));
            }
            
        }
        if(session()->has("SES_SEARCH_AMBIL")) {
            
                $query->where("a.status_pengembalian", session()->get("SES_SEARCH_AMBIL"));
               
               // dd(session()->get("SES_SEARCH_AMBIL"));
            
        }

        if(session()->has("SES_SEARCH_JMN_KARYAWAN")) {
            $query->where("nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_JMN_KARYAWAN") . "%")
                   ->orwhere("nik", "LIKE", "%" . session()->get("SES_SEARCH_JMN_KARYAWAN") . "%");
                  
        }
        //dd(session()->get("SES_SEARCH_UT_CABANG"));

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getStatuspajak($id_ptkp)
    {
      $query  = DB::table("p_ptkp")
                ->select("status")
                ->where("id_ptkp",$id_ptkp)->get()->first();
                           
       if(empty($query))
       {
            $result=0;
       }
       else
       {
        $result = $query->status; 
       }    
       

        return $result;

    } 
    public function getProfile($id) {
        $query  =DB::table("p_karyawan as a")
                            ->select("a.*","a.tgl_masuk","a.tgl_keluar","a.status_kerja","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_pendidikan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_pendidikan as e","e.id_pendidikan","=","a.id_pendidikan")
                            
                            ->where("a.id_karyawan", $id)
                            ->orderBy("a.nama_karyawan", "DESC");

    
        $result = $query->get();

        return $result;
    }    


   public function getDetailKeluarga($id) {
        $query  = DB::table("pa_keluarga")
                            ->select("*")
                            ->where("id_karyawan", $id)
                           
                            ->orderBy("id_keluarga", "asc");

        $result = $query->get();

        return $result;
    }    
public function getDetailKontak($id) {
        $query  = DB::table("pa_kontak")
                            ->select("*")
                            ->where("id_karyawan", $id)
                            ->orderBy("id_kontak", "asc");

        $result = $query->get();

        return $result;
    }   

 public function getDetailPendidikan($id) {
        $query  = DB::table("pa_rpendidikan")
                            ->select("*")
                            ->where("id_karyawan", $id)
                           
                            ->orderBy("id_rpendidikan", "asc");

        $result = $query->get();

        return $result;
    }  

    public function getDetailPengalaman($id) {
        $query  = DB::table("pa_pengalaman")
                            ->select("*")
                            ->where("id_karyawan", $id)
                           
                            ->orderBy("id_pengalaman", "asc");

        $result = $query->get();
       
        return $result;
    }
    public function getJabatan($id) {
        $query  = DB::table("m_jabatan as a")
                             ->select("b.id_level","a.tunj_transport","b.tunj_jabatan")
                            ->leftjoin("m_level as b","b.id_level","=","a.id_level")
                            ->where("a.id_jabatan", $id);
        $result = $query->get();

        return $result;
    } 
     public function getCabang($id) {
        $query  = DB::table("m_cabang")
                             ->select("kode_cabang")
                            ->where("id_cabang", $id);
        $result = $query->get();

        return $result;
    }           
    public function createData($request) {

        $qkaryawan              = new KaryawanModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        $tunj_jabatan            = $qJabatan->tunj_jabatan;
        $tunj_transport          = $qJabatan->tunj_transport;
      
        $tgl_awalcuti =date("Y-m-d H:i:s", strtotime("+1 years", strtotime(setYMD($request->tgl_masuk, "/"))));
        $tgl_akhircuti =date("Y-m-d H:i:s", strtotime("+2 years", strtotime(setYMD($request->tgl_masuk, "/"))));
   //     dd(setYMD($request->tgl_masuk, "/"),setYMD($request->tgl_lahir,"/"), $tgl_akhircuti);
        # ---------------
       // $qkaryawan->kode       = setString($request->kode);
        $qkaryawan->nik                 = setString($request->nik);
        $qkaryawan->nama_karyawan       = setString($request->nama_karyawan);
        $qkaryawan->id_cabang           = setString($request->id_cabang);
        $qkaryawan->id_departemen       = setString($request->id_departemen);
        $qkaryawan->id_jabatan          = setString($request->id_jabatan);
        //$qkaryawan->tgl_melamar         = setYMD($request->tgl_melamar,"/");
        $qkaryawan->tgl_masuk           = setYMD($request->tgl_masuk,"/");
        $qkaryawan->tgl_percobaan       = setYMD($request->tgl_percobaan,"/");
        $qkaryawan->no_rekening         = $request->no_rekening;
        $qkaryawan->anrekening          = $request->anrekening;
        $qkaryawan->id_cabang_hire      = setString($request->id_cabang);
        $qkaryawan->email               = setString($request->email);
        $qkaryawan->noabsen             = setString($request->noabsen);
        $qkaryawan->no_ktp              = $request->no_ktp;
        $qkaryawan->tgl_berlaku_ktp     = setYMD($request->tgl_berlaku_ktp,"/");
        $qkaryawan->tempat_lahir        = $request->tempat_lahir;
        $qkaryawan->tgl_lahir           = setYMD($request->tgl_lahir,"/");
        $qkaryawan->agama               = $request->agama;
        $qkaryawan->status_nikah        = $request->status_nikah;
        $qkaryawan->jml_anak            = $request->jml_anak;
        $qkaryawan->nama_ibu            = $request->nama_ibu;
        $qkaryawan->id_pendidikan       = $request->id_pendidikan;
        $qkaryawan->golongan_darah      = $request->golongan_darah;
        $qkaryawan->alamat              = $request->alamat;
        $qkaryawan->alamat_valid        = $request->alamat_valid;
        $qkaryawan->no_telpon           = $request->no_telpon;
        $qkaryawan->tgl_awalcuti        = $tgl_awalcuti ;
        $qkaryawan->tgl_akhircuti       = $tgl_akhircuti ;
        $qkaryawan->tunj_jabatan        = $tunj_jabatan ;
        $qkaryawan->tunj_transport      = $tunj_transport ;
        $qkaryawan->sisacuti            =0;
        $qkaryawan->user_id             = setString(Auth::user()->id);
        $qkaryawan->create_at           = setString(date('Y-m-d H:i:s'));
        $qkaryawan->update_at           = setString(date('Y-m-d H:i:s'));
        
        # ---------------
        $qkaryawan->save();
       
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE KARYAWAN (" . $qkaryawan->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function updateData($request) {

        $qkaryawan              = new KaryawanModel;
        $qJabatan               = $this->getJabatan($request->id_jabatan)->first();
        $tunj_jabatan            = $qJabatan->tunj_jabatan;
        $tunj_transport          = $qJabatan->tunj_transport;

                    $id_ptkp ="TK/0";
                   if( $request->status_nikah=="KAWIN")
                  {
                     $id_ptkp="K/" . $request->jml_anak;
                  }
                  else
                  {

                      $id_ptkp="TK/" . $request->jml_anak;
                  }


                  

        
                         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "nama_karyawan"=>$request->nama_karyawan,
                                "id_cabang"=>$request->id_cabang,
                                 "id_cabang_hire" => setString($request->id_cabang_hire),
                                "id_departemen"=>$request->id_departemen,
                                "id_jabatan"=>$request->id_jabatan,
                                //"tgl_melamar"=>setYMD($request->tgl_melamar,"/"),
                                "tgl_masuk"=>setYMD($request->tgl_masuk, "/"),
                                "email"=>$request->email,
                                "noabsen"=>$request->noabsen,
                                "no_ktp"=>$request->no_ktp,
                                "tempat_lahir"=>$request->tempat_lahir,
                                "tgl_lahir"=>setYMD($request->tgl_lahir, "/"),
                                "nama_ibu"=>$request->nama_ibu,
                                "id_ptkp"=>$request->id_ptkp, 
                                "nonpwp"=>$request->nonpwp,
                                "nobpjskes"=>$request->nobpjskes,
                                "nobjstk"=>$request->nobjstk,
                                "no_rekening"=>$request->no_rekening,
                                "anrekening"=>$request->asrekening,
                                "agama"=>$request->agama,
				"jenis_kelamin"=>$request->jenis_kelamin,
                                "status_nikah"=>$request->status_nikah,
                                //"jml_anak"=>$request->jml_anak,
                                "id_pendidikan"=>$request->id_pendidikan,
                                "golongan_darah" => $request->golongan_darah,
                                "alamat"=>$request->alamat,
                                "alamat_valid"=>$request->alamat_valid,
                                "no_telpon"=>$request->no_telpon,
                                "hp"=>$request->hp,
                                 "tunj_jabatan" => $qJabatan->tunj_jabatan,
                                "tunj_transport" => $qJabatan->tunj_transport,
                                 "id_jaminan"=> $request->id_jaminan,
                                "no_jaminan"=> $request->no_jaminan ,
                                "nama_dijaminan"=> $request->nama_jaminan, 
                                 "user_id"=>setString(Auth::user()->id),
                                 "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);

             
          


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
    public function updateData_jaminan($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "tgl_pengembalian"=>setYMD($request->tgl_pengembalian, "/"),
                                       "status_pengembalian"=>"2",
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

      public function updateDataNik($request) {

        $qkaryawan              = new KaryawanModel;
        $ambiltgl = substr($request->tgl_masuk,0,2);                      
            $ambilbln = substr($request->tgl_masuk,3,2);                      
            $ambilthn = substr($request->tgl_masuk,8,2);
            $nourut   = substr($request->nik,10,4);
            $kodecabang = $this->getCabang($request->id_cabang)->first(); 
            $kodecabang = substr($kodecabang->kode_cabang,-2); 
             $nik =  $ambiltgl.$ambilbln.'.'.$ambilthn.$kodecabang.'.'.$nourut;
           

                         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "nik"=>$nik,
                                "id_cabang"=>$request->id_cabang,
                                "tgl_masuk"=>setYMD($request->tgl_masuk, "/"),
                                 "user_id"=>setString(Auth::user()->id),
                                 "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);
                            DB::table("p_penggajian")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "nik"=>$nik,
                                "id_cabang"=>$request->id_cabang,
                                "tgl_masuk"=>setYMD($request->tgl_masuk, "/"),
                                 "user_id"=>setString(Auth::user()->id),
                                 "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);
                             DB::table("tm_dataabsensi")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "nik"=>$nik,
                                 "user_id"=>setString(Auth::user()->id),
                                 "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);

             
          


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
    public function updateDatabatalkeluar($request) {

       
                         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "aktif"=>1,
                                "tgl_keluar"=>null,
                                 "user_id"=>setString(Auth::user()->id),
                                 "update_at"=>setString(date('Y-m-d H:i:s'))    
                                 ]);

             
          


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
public function updateresignData($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([ "tgl_keluar"=>setYMD($request->tgl_keluar, "/"),
                                       "alasan_keluar"=>$request->alasan_keluar,
                                       "user_id"=>setString(Auth::user()->id),
                                       "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                            DB::table("p_karyawankeluar")
                             ->where("id_karyawan", $request->id_karyawan)->delete();


    

                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE KARYAWAN(" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }
    public function removeData($request) {
         DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE KARYAWAN (" . $request->id_karyawan . ") " . strtoupper($request->nama_karyawan), Auth::user()->id, $request);
    }

    public function getProfileResign($id) {
$query =DB::table("p_karyawan as a")
->select("a.*",db::Raw("DATE_FORMAT(a.tgl_keluar, '%d/%m/%Y') as Tanggal_keluar"),"b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_alasankeluar","f.nama_pendidikan","g.nama_bank","h.keterangan_keluar")
->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
->leftjoin("pa_alasankeluar as e","e.id_alasankeluar","=","a.alasan_keluar") 
->leftjoin("m_pendidikan as f","f.id_pendidikan","=","a.id_pendidikan") 
->leftjoin("p_bank as g","g.id_bank","=","a.id_bank") 
->leftjoin("p_karyawankeluar as h","h.id_karyawan","=","a.id_karyawan") 
->where("a.id_karyawan", $id)
->orderBy("a.nama_karyawan", "DESC");
 
 
$result = $query->get();
 
return $result;
}
}
