<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class KarirModel extends Model
{
    protected $table    = "pa_karir";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_karir as a")
                           ->select("a.id_karir","a.nama_karir","status_proses","a.id_karyawan","a.no_surat",db::Raw("DATE_FORMAT(a.tgl_karir, '%d/%m/%Y') as Tanggal_karir") ,"b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_karyawan","e.nik")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangbaru")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenbaru")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                             ->where("status_proses","=","APPROVE")   
                            ->orderBy("id_karir", "desc");



        if(session()->has("SES_SEARCH_AWAL")) {
             $awal = session()->get("SES_SEARCH_AWAL");
             $awal = setYMD($awal,"/");
        }
        if(session()->has("SES_SEARCH_AKHIR")) {
             $akhir = session()->get("SES_SEARCH_AKHIR");
             $akhir = setYMD($akhir,"/");   
           
            $query->whereBetween("a.tgl_karir", [$awal, $akhir]);
            
        }

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("id_cabangbaru", session()->get("SES_SEARCH_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $query->where("id_departemenbaru", session()->get("SES_SEARCH_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_JABATAN")) {
            if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $query->where("id_jabatanbaru", session()->get("SES_SEARCH_JABATAN"));
            }
            
        }
        if(session()->has("SES_SEARCH_NAMAKARIR")) {
            if(session()->get("SES_SEARCH_NAMAKARIR") != '-'){
                $query->where("nama_karir", session()->get("SES_SEARCH_NAMAKARIR"));
            }
            
        }

        if(session()->has("SES_SEARCH_INQKARIR")) {
            $query->where("e.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_INQKARIR") . "%")
             ->orwhere("a.no_surat", "LIKE", "%" . session()->get("SES_SEARCH_INQKARIR") . "%")
                  ->orwhere("e.nik", "LIKE", "%" . session()->get("SES_SEARCH_INQKARIR") . "%")
                  ->orwhere("a.nama_karir", "LIKE", "%" . session()->get("SES_SEARCH_INQKARIR") . "%");;
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

public function getList_proses($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_karir as a")
                           ->select("a.id_karir","a.nama_karir","status_proses","a.id_karyawan","a.no_surat",db::Raw("DATE_FORMAT(a.tgl_karir, '%d/%m/%Y') as Tanggal_karir"),"b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_karyawan","e.nik")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangbaru")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenbaru")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->where("status_proses","!=","APPROVE")

                            ->orderBy("id_karir", "desc");



        if(session()->has("SES_SEARCH_KARIR")) {
            $query->where("e.nama_karyawan", "LIKE", "%" . session()->get("SES_SEARCH_KARIR") . "%")
             ->orwhere("a.no_surat", "LIKE", "%" . session()->get("SES_SEARCH_KARIR") . "%")
                  ->orwhere("e.nik", "LIKE", "%" . session()->get("SES_SEARCH_KARIR") . "%")
                  ->orwhere("a.nama_karir", "LIKE", "%" . session()->get("SES_SEARCH_KARIR") . "%");;
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }
    public function getProfile($id) {
        $query  = DB::table("pa_karir")
                            ->select("*")
                            ->where("id_karir", $id)
                           
                            ->orderBy("id_karir", "asc");

        $result = $query->get();

        return $result;
    }    

     public function getProfilekry($id) {
        $query  = DB::table("p_karyawan")
                            ->select("id_cabang","id_departemen","id_jabatan")
                            ->where("id_karyawan", $id);
                          
        $result = $query->get();

        return $result;
    }    
  public function getCetakkarir($id)
    {
        $query  = DB::table("pa_karir as a")
                           ->select("a.*","e.nama_karyawan","e.nik", "a.tgl_karir","b.nama_cabang as nama_cabangawal","c.nama_departemen as nama_departemenawal","d.nama_jabatan as nama_jabatanawal","f.nama_jabatan as nama_jabatanbaru","g.nama_jabatan as nama_jabatantj","h.nama_cabang as nama_cabangbaru"
                              )
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangawal")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenawal")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanawal")
                            ->leftjoin("p_karyawan as e","e.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_jabatan as f","f.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("m_jabatan as g","g.id_jabatan","=","a.id_jabatantj")
                             ->leftjoin("m_cabang as h","h.id_cabang","=","a.id_cabangbaru")
                            ->where("id_karir",$id)
                            ->orderBy("id_karir", "asc");
                    

        $result = $query->get();

        return $result;
    }
 
    public function createData($request) {
        $qkarir              = new KarirModel;
        $qKaryawan           = $this-> getProfilekry($request->id_karyawan)->first();
        //dd($qKaryawan);
       // dd($qKaryawan->id_cabang,$qKaryawan->id_jabatan);
        # ---------------
       // $qkarir->kode       = setString($request->kode);
        $qkarir->no_surat  			= setString($request->no_surat);
        $qkarir->id_karyawan         = setString($request->id_karyawan);
        $qkarir->id_cabangawal       = setString($qKaryawan->id_cabang);
        $qkarir->id_departemenawal   = setString($qKaryawan->id_departemen);
        $qkarir->id_jabatanawal      = setString($qKaryawan->id_jabatan);
        $qkarir->id_cabangbaru       = setString($request->id_cabangbaru);
        $qkarir->id_departemenbaru   = setString($request->id_departemenbaru );
        $qkarir->id_jabatanbaru      = setString($request->id_jabatanbaru);
        $qkarir->id_jabatantj      = setString($request->id_jabatantj);
        $qkarir->nama_karir	         = setString($request->nama_karir);
        $qkarir->tgl_karir           = setYMD($request->tgl_karir,"/"); 
        $qkarir->keterangan_karir    =  setString($request->keterangan_karir);
        $qkarir->status_proses       =  "PROSES";
        $qkarir->tembusan1           =  setString($request->tembusan1);
        $qkarir->tembusan2           =  setString($request->tembusan2);
        $qkarir->tembusan3           =  setString($request->tembusan3);
        $qkarir->tembusan4           =  setString($request->tembusan4);
        $qkarir->tembusan5            =  setString($request->tembusan5);



        
        
        

        // if($request->nama_karir=="PROMOSI")
        // {
        //    if(!is_null($qkarir->tgl_probation))
        //    {
        //           $qkarir->tgl_probation       = setYMD($request->tgl_probation,"/"); 
        //    }
        //    else
        //    {
        //         $qkarir->tgl_probation       = null; 
        //    }
        // }
       
       
        $qkarir->status_proses       = "PROSES";
        $qkarir->user_id             = setString(Auth::user()->id);
        $qkarir->create_at           = setString(date('Y-m-d H:i:s'));
        $qkarir->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qkarir->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE karir (" . $qkarir->id_karir . ") " . strtoupper($request->id_karir), Auth::user()->id, $request);
    }

    public function updateData($request) {
        $tgl_prob=null;
        if($request->nama_karir=="PROMOSI")
        {
           if(!is_null($request->tgl_probation))
           {
                  $tgl_prob       = setYMD($request->tgl_probation,"/"); 
           }
           else
           {
                $tgl_prob       = null; 
           }
        }
         DB::table("pa_karir")
                             ->where("id_karir", $request->id)
                            ->update([ "no_surat"     =>$request->no_surat,
                            			"id_cabangbaru"     =>$request->id_cabangbaru,
                            			"id_departemenbaru" =>$request->id_departemenbaru,
                            			"id_jabatanbaru"    =>$request->id_jabatanbaru,
                            			"nama_karir"        => setString($request->nama_karir),
                                  "keterangan_karir"  => setString($request->keterangan_karir),
                                  "tgl_karir"         => setYMD($request->tgl_karir,"/"),
                                   "id_jabatantj"     => setString($request->id_jabatantj),
                                  "tembusan1"         =>  setString($request->tembusan1),
                                  "tembusan2"         =>  setString($request->tembusan2),
                                  "tembusan3"         =>  setString($request->tembusan3),
                                  "tembusan4"         =>  setString($request->tembusan4),
                                  "tembusan5"         =>  setString($request->tembusan5),
    
                                         "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE karir(" . $request->id_karir . ") " . strtoupper($request->nama_karir), Auth::user()->id, $request);
    }

public function updateDatabatalapprove($request) {
        $tgl_prob=null;
        if($request->nama_karir=="PROMOSI")
        {
           if(!is_null($request->tgl_probation))
           {
                  $tgl_prob       = setYMD($request->tgl_probation,"/"); 
           }
           else
           {
                $tgl_prob       = null; 
           }
        }
         DB::table("pa_karir")
                             ->where("id_karir", $request->id)
                            ->update([ "status_proses" => "PROSES",
                                                                           "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                    ]);

          //Update Kry ke posisi sebelumnya

          $query  = DB::table("pa_karir")
                           ->select("id_karir","id_karyawan","id_jabatanawal","id_cabangawal","id_departemenawal","tgl_karir")
                              
                            ->where("id_karir",$request->id);
          $result = $query->get()->first();
         // dd($result);
          if ($result->id_cabangawal ==0)
          {  
            // Cek ke data sebelumnya
            $query2  = DB::table("pa_karir")
                           ->select("id_karir","id_karyawan","id_jabatanbaru","id_cabangbaru","id_departemenbaru","tgl_karir")
                              
                            ->where("id_karyawan",$result->id_karyawan)
                            ->where("tgl_karir","<",$result->tgl_karir)
                            ->orderBy("tgl_karir","desc");
                             $result2 = $query2->get()->first();
          //dd($result2->id_karyawan,$result2->id_jabatanbaru,$result2->id_departemenbaru,$result2->id_cabangbaru);
               DB::table("p_karyawan")
                             ->where("id_karyawan", $result2->id_karyawan)
                            ->update([ "id_cabang" =>$result2->id_cabangbaru,
                                       "id_departemen" =>$result2->id_departemenbaru,
                                       "id_jabatan" =>$result2->id_jabatanbaru,
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                    ]);  


          }
          else
          {
                DB::table("p_karyawan")
                             ->where("id_karyawan", $result->id_karyawan)
                            ->update([ "id_cabang" =>$result->id_cabangawal ,
                                       "id_departemen" =>$result->id_departemenawal ,
                                       "id_jabatan" =>$result->id_jabatanawal ,
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                    ]);

          }
        

                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE karir(" . $request->id_karir . ") " . strtoupper($request->nama_karir), Auth::user()->id, $request);
    }



    public function updateData_approve($request) {
       
         DB::table("pa_karir")
                             ->where("id_karir", $request->id)
                            ->update([ "status_proses"=>$request->status_proses,
                                  "komentar"=>$request->komentar,
                                  
                                         "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                                    ]);
     

          DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "id_cabang"       => setString($request->id_cabangbaru),
                                        "id_departemen"   => setString($request->id_departemenbaru ),
                                        "id_jabatan"      => setString($request->id_jabatanbaru),
                                       "user_id"          =>setString(Auth::user()->id),
                                       "update_at"        =>setString(date('Y-m-d H:i:s'))]);
                       
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE karir(" . $request->id_karir . ") " . strtoupper($request->nama_karir), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_karir")
            ->where("id_karir", $request->id)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE karir (" . $request->id_karir . ") " . strtoupper($request->nama_karir), Auth::user()->id, $request);
    }
}
