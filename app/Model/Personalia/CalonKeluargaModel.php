<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class CalonKeluargaModel extends Model
{
    protected $table    = "pa_keluarga";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_keluarga")
                            ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_keluarga", "asc");

        if(session()->has("SES_SEARCH_CALON_KELUARGA")) {
            $query->where("nama_keluarga", "LIKE", "%" . session()->get("SES_SEARCH_CALON_KELUARGA") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_keluarga")
                            ->select("*")
                            ->where("id_keluarga", $id)
                           
                            ->orderBy("id_keluarga", "asc");

        $result = $query->get();

        return $result;
    }    

 
    public function createData($request) {
        $qkeluarga              = new CalonKeluargaModel;
        # ---------------
       // $qkeluarga->kode       = setString($request->kode);
        $qkeluarga->nama_keluarga       = setString($request->nama_keluarga);
        $qkeluarga->id_karyawan         = setString($request->id_karyawan);
        $qkeluarga->hubungan            = setString($request->hubungan);
        $qkeluarga->tempat_lahir        = setString($request->tempat_lahir);
        $qkeluarga->jenis_kelamin       = setString($request->jenis_kelamin);
        $qkeluarga->tgl_lahir           = setYMD($request->tgl_lahir,"/");
        $qkeluarga->pendidikan          = setString($request->pendidikan);
        $qkeluarga->pekerjaan           = setString($request->pekerjaan);
        $qkeluarga->user_id             = setString(Auth::user()->id);
        $qkeluarga->create_at           = setString(date('Y-m-d H:i:s'));
        $qkeluarga->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qkeluarga->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE keluarga (" . $qkeluarga->id_keluarga . ") " . strtoupper($request->batasbawah), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("pa_keluarga")
                             ->where("id_keluarga", $request->id)
                            ->update([ "nama_keluarga"=>$request->nama_keluarga,
                            			 "hubungan"=>$request->hubungan,
                                        "jenis_kelamin"=>$request->jenis_kelamin,
                                        "tempat_lahir"=>$request->tempat_lahir,
                                        "tgl_lahir"=>setYMD($request->tgl_lahir,"/"),
                                        "pendidikan"=>$request->pendidikan,
                                        "pekerjaan"=>$request->pekerjaan,   
                                        "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE keluarga(" . $request->id_keluarga . ") " . strtoupper($request->nama_keluarga), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_keluarga")
                             ->where("id_keluarga", $request->id)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE keluarga (" . $request->id_keluarga . ") " . strtoupper($request->nama_keluarga), Auth::user()->id, $request);
    }
}
