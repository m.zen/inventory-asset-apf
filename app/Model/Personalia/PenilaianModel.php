<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class PenilaianModel extends Model
{
     protected $table    = "pa_penilaian";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null) {
        $query  = DB::table("pa_penilaian")
                            ->select("id_penilaian", "nama_penilaian","kategori_penilaian")
                            ->orderBy("kategori_penilaian","id_penilaian", "DESC");

        if(session()->has("SES_SEARCH_PENILAIAN")) {
            $query->where("nama_penilaian", "LIKE", "%" . session()->get("SES_SEARCH_PENILAIAN") . "%");
            $query->orWhere("kategori_penilaian", "LIKE", "%" . session()->get("SES_SEARCH_PENILAIAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_penilaian")
                            ->select("id_penilaian", "nama_penilaian","kategori_penilaian")
                            ->where("id_penilaian", $id)

                            ->orderBy("nama_penilaian", "DESC");

        $result = $query->get();

        return $result;
    }    

    public function createData($request) {
        $qpenilaian              = new PenilaianModel;
        # ---------------
       // $qpenilaian->kode       = setString($request->kode);
        $qpenilaian->kategori_penilaian  = setString($request->kategori_penilaian);
        $qpenilaian->nama_penilaian      = setString($request->nama_penilaian);
        $qpenilaian->user_id             = setString(Auth::user()->id);
        $qpenilaian->create_at           = setString(date('Y-m-d H:i:s'));
        $qpenilaian->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qpenilaian->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE PENILAIAN (" . $qpenilaian->id_penilaian . ") " . strtoupper($request->nama_penilaian), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("pa_penilaian")
                             ->where("id_penilaian", $request->id_penilaian)
                            ->update([ "nama_penilaian"=>$request->nama_penilaian,
                            			"kategori_penilaian"=>$request->kategori_penilaian,
                            	    	"user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE PENILAIAN(" . $request->id_penilaian . ") " . strtoupper($request->nama_penilaian), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("t_penilaian")
                             ->where("id_penilaian", $request->id_penilaian)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE PENILAIAN (" . $request->id_penilaian . ") " . strtoupper($request->nama_penilaian), Auth::user()->id, $request);
    }
}
