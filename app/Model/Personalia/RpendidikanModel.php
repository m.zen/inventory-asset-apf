<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class RpendidikanModel extends Model
{
    protected $table    = "pa_rpendidikan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_rpendidikan")
                            ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_rpendidikan", "asc");

        if(session()->has("SES_SEARCH_RPENDIDIKAN")) {
            $query->where("nama_rpendidikan", "LIKE", "%" . session()->get("SES_SEARCH_RPENDIDIKAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_rpendidikan")
                            ->select("*")
                            ->where("id_rpendidikan", $id)
                           
                            ->orderBy("id_rpendidikan", "asc");

        $result = $query->get();

        return $result;
    }    

 
    public function createData($request) {
        $qrpendidikan              = new rpendidikanModel;
        # ---------------
       // $qrpendidikan->kode       = setString($request->kode);
        $qrpendidikan->id_pendidikan       = setString($request->id_pendidikan);
        $qrpendidikan->id_karyawan         = setString($request->id_karyawan);
        $qrpendidikan->nama_sekolah        = setString($request->nama_sekolah);
        $qrpendidikan->kota_sekolah        = setString($request->kota_sekolah);
        $qrpendidikan->tahun_awal          = setString($request->tahun_awal);
        $qrpendidikan->tahun_akhir        = setString($request->tahun_akhir);
         $qrpendidikan->user_id             = setString(Auth::user()->id);
        $qrpendidikan->create_at           = setString(date('Y-m-d H:i:s'));
        $qrpendidikan->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qrpendidikan->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE rpendidikan (" . $qrpendidikan->id_rpendidikan . ") " . strtoupper($request->batasbawah), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("pa_rpendidikan")
                             ->where("id_rpendidikan", $request->id)
                            ->update([ "id_pendidikan"=>$request->id_pendidikan,
                            			"nama_sekolah"=>$request->nama_sekolah,
                                        "kota_sekolah"=>$request->kota_sekolah,
                                        "tahun_awal"=>$request->tahun_awal,
                                        "tahun_akhir"=>$request->tahun_akhir,
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE rpendidikan(" . $request->id_rpendidikan . ") " . strtoupper($request->nama_rpendidikan), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_rpendidikan")
                             ->where("id_rpendidikan", $request->id)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE rpendidikan (" . $request->id_rpendidikan . ") " . strtoupper($request->nama_rpendidikan), Auth::user()->id, $request);
    }
}
