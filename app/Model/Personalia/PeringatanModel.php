<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class PeringatanModel extends Model
{
    protected $table    = "pa_peringatan";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_peringatan")
                           ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_peringatan", "asc");




        if(session()->has("SES_SEARCH_PERINGATAN")) {
            $query->where("status_proses", "LIKE", "%" . session()->get("SES_SEARCH_PERINGATAN") . "%");
        }


        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getCetaksp($id)
    {
       
        $query  = DB::table("pa_peringatan as a")
                           ->select("a.id_peringatan","a.id_karyawan","b.nama_karyawan","b.nik","a.no_surat", "a.tgl_berlaku","a.tgl_akhir",
                           	"a.jenis_peringatan","c.nama_jabatan","a.keterangan","d.nama_cabang")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_jabatan as c","c.id_jabatan","=","b.id_jabatan")
                             ->leftjoin("m_cabang as d","d.id_cabang","=","b.id_cabang")
                            ->where("id_peringatan", $id)
                            ->orderBy("a.id_peringatan", "asc");
     
        $result = $query->get();

        return $result;
    }
   

    public function getProfile($id) {
        $query  = DB::table("pa_peringatan")
                            ->select("*")
                            ->where("id_peringatan", $id)
                            ->orderBy("id_peringatan", "asc");

        $result = $query->get();

        return $result;
    }    

    public function CekSP($no_surat) {
       
        $query  = DB::table("pa_peringatan")
                            ->select("*")
                            ->where("no_surat", $no_surat);
                            
        $result = $query->count();
        

        return $result;
    }
 
    public function createData($request) {
        $qperingatan              = new PeringatanModel;
        # ---------------
       // $qperingatan->kode       = setString($request->kode);
        $qperingatan->no_surat  	    = setString($request->no_surat);
        $qperingatan->id_karyawan       = setString($request->id_karyawan);
        $qperingatan->jenis_peringatan  = setString($request->jenis_peringatan);
        $qperingatan->tgl_berlaku       = setYMD($request->tgl_berlaku,"/"); 
        $qperingatan->tgl_akhir       	= setYMD($request->tgl_akhir,"/"); 
        //$qperingatan->keterangan  	    = setString($request->keterangan);
        

        $qperingatan->status_proses     = "PROSES";
        $qperingatan->user_id           = setString(Auth::user()->id);
        $qperingatan->create_at         = setString(date('Y-m-d H:i:s'));
        $qperingatan->update_at         = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qperingatan->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE peringatan (" . $qperingatan->id_peringatan . ") " . strtoupper($request->id_peringatan), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("pa_peringatan")
                             ->where("id_peringatan", $request->id)
                            ->update([ "no_surat"=>$request->no_surat,
                            			"id_cabang"=>$request->id_cabang,
                            			"id_departemen"=>$request->id_departemen,
                            			"id_jabatan"=>$request->id_jabatan,
                            			"statuskerja"=>$request->statuskerja,
                                        "tgl_awal"=>setYMD($request->tgl_awal,"/"),
                                        "tgl_akhir"=>setYMD($request->tgl_akhir,"/"),
                                     	 "gaji_pokok"=>setNoComma($request->gaji_pokok),
                                        "tunj_jabatan"=>setNoComma($request->tunj_jabatan),
                                        "tunj_transport"=>setNoComma($request->tunj_transport),
                                        "tunj_jaga"=>setNoComma($request->tunj_jaga),
                                        "tunj_kemahalan"=>setNoComma($request->tunj_kemahalan),
                                        "tunj_makan"=>setNoComma($request->tunj_makan),
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE peringatan(" . $request->id_peringatan . ") " . strtoupper($request->nama_peringatan), Auth::user()->id, $request);
    }
    public function updateaktivasiData($request) {
         DB::table("pa_peringatan")
                             ->where("id_peringatan", $request->id)
                            ->update([ "status_proses"=>"SELESAI",
                                        "tgl_tandatangan"=>setYMD($request->tgl_tandatangan,"/"),
                                        "aktiv"=>"AKTIF",
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
        DB::table("p_karyawan")
                             ->where("id_karyawan", $request->id_karyawan)
                            ->update([  "tgl_awal"=>$request->tgl_awal,
                                        "tgl_akhir"=>$request->tgl_akhir,
                                        "status_kerja"=>$request->status_kerja,
                                        "user_id"=>setString(Auth::user()->id),
                                        "update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);
                           
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE peringatan(" . $request->id_peringatan . ") " . strtoupper($request->nama_peringatan), Auth::user()->id, $request);
    }
    public function removeData($request) {
         DB::table("pa_peringatan")
            ->where("id_peringatan", $request->id_peringatan)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE peringatan (" . $request->id_peringatan . ") " . strtoupper($request->nama_peringatan), Auth::user()->id, $request);
    }
}
