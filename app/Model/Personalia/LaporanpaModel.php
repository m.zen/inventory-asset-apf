<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class LaporanpaModel extends Model
{
     protected $table    = "p_karyawan";



  public function getKaryawan()
    {
    	  $cabang=$request->id_cabangbaru;
        $departemen = $request->id_departemenbaru;
        $jabatan    = $request->id_jabatanbaru;
       
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","b.kode_cabang","c.nama_departemen","d.nama_jabatan","e.nama_pendidikan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                              ->leftjoin("m_pendidikan as e","e.id_pendidikan","=","a.id_pendidikan")
                            ->where('aktif','=',1)
                            ->orderBy("a.id_cabang", "DESC");
                            if($cabang != "-")
                           {
                                $query= $query->where('a.id_cabang','=', $cabang);
                           }
                           if($departemen != "-")
                           {
                                $query= $query->where('a.id_departemen','=',$departemen);
                           }
                           if($jabatan != "-")
                           {
                                $query= $query->where('a.id_jabatan','=',$jabatan);
                           }

		$result = $query->get();

        return $result;
    }    
    public function getKaryawanmasuk($request)
    {
           $cabang=$request->id_cabangbaru;
           $departemen = $request->id_departemenbaru;
           $jabatan    = $request->id_jabatanbaru;
           
            
    	  $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang"."b.kode_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            
                            
                            ->whereBetween('tgl_masuk', [setYMD($request->TglAwal,"/"), setYMD($request->TglAkhir,"/")])

                         ->orderBy("a.id_cabang", "DESC");
                           if($cabang != "-")
                           {
                                $query= $query->where('a.id_cabang','=', $cabang);
                           }
                           if($departemen != "-")
                           {
                                $query= $query->where('a.id_departemen','=',$departemen);
                           }
                           if($jabatan != "-")
                           {
                                $query= $query->where('a.id_jabatan','=',$jabatan);
                           }

		$result = $query->get();


        return $result;
    }

    public function getKaryawankeluar($request)
    {
    	  $cabang=$request->id_cabangbaru;
        $departemen = $request->id_departemenbaru;
        $jabatan    = $request->id_jabatanbaru;
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->whereBetween('tgl_keluar', [setYMD($request->TglAwal,"/"), setYMD($request->TglAkhir,"/")])
                            
                            ->orderBy("a.id_cabang", "DESC");
                           if($cabang != "-")
                           {
                                $query= $query->where('a.id_cabang','=', $cabang);
                           }
                           if($departemen != "-")
                           {
                                $query= $query->where('a.id_departemen','=',$departemen);
                           }
                           if($jabatan != "-")
                           {
                                $query= $query->where('a.id_jabatan','=',$jabatan);
                           }

		$result = $query->get();

        return $result;
    }

     public function getKaryawanjatuhtempo($request)
    {
       $cabang=$request->id_cabangbaru;
        $departemen = $request->id_departemenbaru;
        $jabatan    = $request->id_jabatanbaru;
    	  $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where('tgl_keluar','=',null)
                            ->where('aktif','=',1)
                            ->whereBetween('tgl_akhir', [setYMD($request->TglAwal,"/"), setYMD($request->TglAkhir,"/")])
                            ->orderBy("a.id_cabang", "DESC");
                            if($cabang != "-")
                           {
                                $query= $query->where('a.id_cabang','=', $cabang);
                           }
                           if($departemen != "-")
                           {
                                $query= $query->where('a.id_departemen','=',$departemen);
                           }
                           if($jabatan != "-")
                           {
                                $query= $query->where('a.id_jabatan','=',$jabatan);
                           }

		$result = $query->get();

        return $result;
    }




}
