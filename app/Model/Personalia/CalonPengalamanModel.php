<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;

class CalonPengalamanModel extends Model
{
     protected $table    = "pa_pengalaman";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_pengalaman")
                            ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_pengalaman", "asc");

        if(session()->has("SES_SEARCH_PENGALAMAN")) {
            $query->where("perusahaan", "LIKE", "%" . session()->get("SES_SEARCH_PENGALAMAN") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_pengalaman")
                            ->select("*")
                            ->where("id_pengalaman", $id)
                           
                            ->orderBy("id_pengalaman", "asc");

        $result = $query->get();
       
        return $result;
    }    

 
    public function createData($request) {
        $qpengalaman              = new pengalamanModel;
        # ---------------
       // $qpengalaman->kode       = setString($request->kode);
        $qpengalaman->perusahaan          = setString($request->perusahaan);
        $qpengalaman->id_karyawan         = setString($request->id_karyawan);
        $qpengalaman->jabatan             = setString($request->jabatan);
        $qpengalaman->kota                = setString($request->kota);
        $qpengalaman->tahun_awal          = $request->tahun_awal;
        $qpengalaman->tahun_akhir         = $request->tahun_akhir;
        $qpengalaman->user_id             = setString(Auth::user()->id);
        $qpengalaman->create_at           = setString(date('Y-m-d H:i:s'));
        $qpengalaman->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        
        # ---------------
        $qpengalaman->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE pengalaman (" . $qpengalaman->id_pengalaman . ") " . strtoupper($request->batasbawah), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("pa_pengalaman")
                             ->where("id_pengalaman", $request->id)
                            ->update([ "perusahaan"=>$request->perusahaan,
                            			 "kota"=>$request->kota,
                                        "jabatan"=>$request->jabatan,
                                        "tahun_awal"=>$request->tahun_awal,
                                        "tahun_akhir"=>$request->tahun_akhir,
                                         "user_id"=>setString(Auth::user()->id),
                                         "update_at"=>setString(date('Y-m-d H:i:s'))    
                            	      ]);
                           


                          
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE pengalaman(" . $request->id_pengalaman . ") " . strtoupper($request->nama_pengalaman), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_pengalaman")
                             ->where("id_pengalaman", $request->id)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE pengalaman (" . $request->id_pengalaman . ") " . strtoupper($request->nama_pengalaman), Auth::user()->id, $request);
    }
}
