<?php

namespace App\Model\Personalia;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Model\LogModel;
class KontakModel extends Model
{
      protected $table    = "pa_kontak";
    public $timestamps= false ;

    public function getList($request=null, $offset=null, $limit=null,$idkaryawan=null) {
        $query  = DB::table("pa_kontak")
                            ->select("*")
                            ->where("id_karyawan",$idkaryawan)
                            ->orderBy("id_kontak", "asc");

        if(session()->has("SES_SEARCH_kontak")) {
            $query->where("nama_kontak", "LIKE", "%" . session()->get("SES_SEARCH_kontak") . "%");
        }

        if($limit > 0) {
            $query->offset($offset);
            $query->limit($limit);
        }

        $result = $query->get();

        return $result;
    }

    public function getProfile($id) {
        $query  = DB::table("pa_kontak")
                            ->select("*")
                            ->where("id_kontak", $id)
                           
                            ->orderBy("id_kontak", "asc");

        $result = $query->get();

        return $result;
    }    

 
    public function createData($request) {
        $qkontak              = new kontakModel;
        # ---------------
       // $qkontak->kode       = setString($request->kode);

        

        $qkontak->nama_kontak       = setString($request->nama_kontak);
        $qkontak->id_karyawan         = setString($request->id_karyawan);
        $qkontak->hubungan            = setString($request->hubungan);
        $qkontak->alamat              = setString($request->alamat);
        $qkontak->telpon              = setString($request->telpon);
        $qkontak->user_id             = setString(Auth::user()->id);
        $qkontak->create_at           = setString(date('Y-m-d H:i:s'));
        $qkontak->update_at           = setString(date('Y-m-d H:i:s'));
        
        
        # ---------------
        $qkontak->save();
        /* ----------
         Logs
        ----------------------- */  
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("CREATE kontak (" . $qkontak->id_kontak . ") " . strtoupper($request->batasbawah), Auth::user()->id, $request);
    }

    public function updateData($request) {
         DB::table("pa_kontak")
                             ->where("id_kontak", $request->id)
                            ->update([ "nama_kontak"=>$request->nama_kontak,
                            			"hubungan"=>$request->hubungan,
                                        "alamat"=>$request->alamat,
                                        "telpon"=>$request->telpon,
                                        "user_id"=>setString(Auth::user()->id),
                            	         "update_at"=>setString(date('Y-m-d H:i:s'))	
                            	      ]);
                           
                            
        # ---------------
       
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("UPDATE kontak(" . $request->id_kontak . ") " . strtoupper($request->nama_kontak), Auth::user()->id, $request);
    }

    public function removeData($request) {
         DB::table("pa_kontak")
                             ->where("id_kontak", $request->id)->delete();
                            
                           
      
        /* ----------
         Logs
        ----------------------- */
            $qLog       = new LogModel;
            # ---------------;
            $qLog->createLog("DELETE kontak (" . $request->id_kontak . ") " . strtoupper($request->nama_kontak), Auth::user()->id, $request);
    }
}
