<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use DB;
use Auth;
use App\Model\Master\UserModel;

class ProfileComposer
{
    public function compose(View $view) {
        $qUser          = new UserModel();
        # ---------------
        $result 		= $qUser->getProfile(Auth::user()->id);
        # ---------------
        $view->with('Profile', $result);
    }
}
