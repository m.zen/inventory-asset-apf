<?php

namespace App\Http\Controllers\Payroll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;

use PDF;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\SlipModel;
use App\Model\Payroll\UpdategajiModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class SlipController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index() {
        $data["title"]         = "Slip Gaji";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lpslip/show";
        $nik                   = Auth::user()->nik; 
        $group_id              = Auth::user()->group_id; 
        $qView                 = getJenisslip();


        $qUpdategaji            = new UpdategajiModel;
        $qperiode               = $qUpdategaji->getPeriode()->first();
        $bulan                  = getMonthName($qperiode->bulanslip, "id");
        $tahun                  = $qperiode->tahunslip;
        $listbulan              = getSlipbulan();
        $qJenisLaporan         = getLaporangaji();
        if($group_id == 1){
        $data["fields"][]      = form_select(array("name"=>"bulan", "label"=>"Bulan", "mandatory"=>"yes", "source"=>$listbulan,"value"=>$qperiode->bulanslip));
        }else{
        $data["fields"][]      = form_text(array("name"=>"bulan", "label"=>"Bulan", "mandatory"=>"yes", "readonly"=>"readonly", "value"=>$qperiode->bulanslip));
        }
        $data["fields"][]      = form_text(array("name"=>"tahun", "label"=>"Tahun", "mandatory"=>"yes", "value"=>$tahun));
        if($group_id == 1){
        $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"NIK", "mandatory"=>"yes", "value"=> $nik));
        }else{
        $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"NIK", "mandatory"=>"yes", "readonly"=>"readonly", "value"=> $nik));
        }   
        // $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qView, "value"=>"1"));

        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;View&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.form", $data);
    }

    public function show(Request $request)
    {

     
        $data["title"]        = "Slip Gaji";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/lpslip/cetak";

        $nik       = $request->nik;
        $bulan        = $request->bulan;
        $tahun        = $request->tahun;

       

        /* ----------
         Inputgaji
        ----------------------- */
        $qMaster              = new MasterModel;
        $qSlip           = new SlipModel;
        $qSlip           = $qSlip->getGaji($nik,$bulan,$tahun)->first();
        if($qSlip==null)
        {

         echo '<script language="javascript">';
  echo 'alert(message successfully sent)';  //not showing an alert box.
  echo '</script>';
                   
          return redirect("/lpslip/index");

        }
        else
        {
            $data["fields"][]      = form_hidden(array("name"=>"nik", "label"=>"nik", "readonly"=>"readonly", "value"=>$nik));
        $data["fields"][]      = form_hidden(array("name"=>"bulan", "label"=>"bulan", "readonly"=>"readonly", "value"=>$bulan));
        $data["fields"][]      = form_hidden(array("name"=>"tahun", "label"=>"tahun", "readonly"=>"readonly", "value"=>$tahun));
       
        $pendapatan = $qSlip->gaji_pokok+$qSlip->tunj_jabatan+$qSlip->tunj_transport+$qSlip->tunj_khusus+$qSlip->tunj_jaga+$qSlip->tunj_kemahalan+ $qSlip->tunj_makan+$qSlip->uang_makan+ $qSlip->tunj_pph21+$qSlip->insentif+$qSlip->koreksiplus-$qSlip->koreksimin-$qSlip->pph21-$qSlip->potongan_absen;
        $potongan = $qSlip->simpanan_wajib+$qSlip->pinjaman_koperasi+$qSlip->bpjs_kes_kry+$qSlip->jht_kry+$qSlip->jp_kry;

        
          $data["gaji_pokok"]     = $qSlip->gaji_pokok;
          $data["tunj_jabatan"]   = $qSlip->tunj_jabatan;
          $data["tunj_transport"] = $qSlip->tunj_transport;
          $data["tunj_khusus"]    = $qSlip->tunj_khusus;
          $data["tunj_jaga"]      = $qSlip->tunj_jaga;
          $data["tunj_kemahalan"] = $qSlip->tunj_kemahalan;
          $data["tunj_makan"]     = $qSlip->tunj_makan;
          $data["uang_makan"]     = $qSlip->uang_makan;
          $data["tunj_pph21"]     = $qSlip->tunj_pph21;
          $data["insentif"]       = $qSlip->insentif; 
          $data["koreksiplus"]    = $qSlip->koreksiplus;
          $data["pendapatan"]     = $pendapatan;
          $data["koreksimin"]     = $qSlip->koreksimin;
          $data["simpanan_wajib"] = $qSlip->simpanan_wajib;
          $data["potongan_absen"] = $qSlip->potongan_absen;
          $data["pinjaman_koperasi"]= $qSlip->pinjaman_koperasi;
          $data["jht_kry"]        = $qSlip->jht_kry;
           $data["bpjs_kes_kry"] = $qSlip->bpjs_kes_kry;
          $data["pph21"]          = $qSlip->pph21 ; 
          $data["jp_kry"]          = $qSlip->jp_kry ;
          $data["potongan"]       = $potongan;

          $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Download&nbsp;&nbsp;"));
          $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));

          return view("Payroll.slip", $data);

        }
        

         
    }

    public function cetak(Request $request)
    {


      // $data["title"]        = "Slip Gaji";
      //   $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
      //   $data["form_act"]     = "/lpslip/cetak";

        $nik       = $request->nik;
        $bulan        = $request->bulan;
        $tahun        = $request->tahun;

       

        /* ----------
         Inputgaji
        ----------------------- */
        $qMaster              = new MasterModel;
        $qSlip                 = new SlipModel;
        $data['gaji']          = $qSlip->getGaji($nik,$bulan,$tahun);

       


       
      
       $pdf = PDF::loadView('Payroll.slippdf',  $data);
        $nama_file =   $qSlip->nama_karyawan;

        //$nama_file =  $qKontrakkerja->nama_karyawan & "-" & $qKontrakkerja->id_kontrakkerja;

      
       
       //$pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprkontrak", $data);
    }
}
