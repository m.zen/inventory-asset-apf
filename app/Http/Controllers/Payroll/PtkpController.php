<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Payroll\PtkpModel;
class PtkpController extends Controller
{
  public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        
        
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));



        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/ptkp/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qPtkp                  = new PtkpModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);


        $data["tabs"]          = array(array("label"=>"Tarif (%)", "url"=>"/tarif/index", "active"=>"")
                           ,array("label"=>"PTKP", "url"=>"/ptkp/index", "active"=>"active"));
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_ptkp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"Status"
                                                ,"name"=>"status"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                     array("label"=>"PTKP"
                                                ,"name"=>"ptkp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>"")); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PTKP" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PTKP");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PTKP");
        }
        # ---------------
        $data["select"]        = $qPtkp->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qPtkp->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.tablist", $data);
    }
/*
    public function add() {
        $data["title"]         = "Add Ptkp";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/ptkp/save";
        
        $qMaster               = new MasterModel;
        
        $qGroups               = $qMaster->getSelectGroup();
       // $qStatus             = getSelectStatusPtkp();

        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"nama_ptkp", "label"=>"Batas Bawah", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"nama_ptkp", "label"=>"Batas Atas", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"nama_ptkp", "label"=>"Ptkp", "mandatory"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }


/*
    public function save(Request $request) {
        $rules = array(
                      
                      'nama_ptkp' => 'required'                     );

        $messages = ['nama_ptkp.required' => 'Ptkp harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/ptkp/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qPtkp  = new PtkpModel;
            # ---------------
            $qPtkp->createData($request);
            # ---------------
            session()->flash("success_message", "Ptkp has been saved");
            # ---------------
            return redirect("/ptkp/index");
        }
    }
*/
    public function edit($id) {
        $data["title"]        = "Edit Ptkp";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/ptkp/update";
        /* ----------
         Ptkp
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPtkp             = new PtkpModel;
        /* ----------
         Source
        ----------------------- */
        $qPtkp             = $qPtkp->getProfile($id)->first();
        $qGroups           = $qMaster->getSelectGroup();

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id_ptkp", "label"=>"PTKP ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"status", "label"=>"Status", "readonly"=>"readonly", "value"=>$qPtkp->status));
        $data["fields"][]      = form_number(array("name"=>"ptkp", "label"=>"PTKP",  "mandatory"=>"yes", "value"=>number_format($qPtkp->ptkp,0)));
                
       
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));

        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'ptkp' => 'required|',
                    
                    'status' => 'required|'                          
        );

        $messages = [
                    'ptkp.required' => 'Ptkp harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/ptkp/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPtkp      = new PtkpModel;
            # ---------------
            $qPtkp->updateData($request);
            # ---------------
            session()->flash("success_message", "Ptkp has been updated");
            # ---------------
            return redirect("/ptkp/index");
        }
    }
/*
    public function delete($id) {
     
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qPtkp     = new PtkpModel;
            # ---------------
            $qPtkp->removeData($request);
            # ---------------
            session()->flash("success_message", "Ptkp has been removed");
        } else {
            session()->flash("error_message", "Ptkp cannot be removed");
        }
      # ---------------
        return redirect("/ptkp/index"); 
    }
    */
}
