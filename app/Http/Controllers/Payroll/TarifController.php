<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Payroll\TarifModel;

class TarifController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        
    	
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));



        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/tarif/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

 		


        $qMenu                  = new MenuModel;
        $qTarif                  = new TarifModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);


        $data["tabs"]          = array(array("label"=>"Tarif (%)", "url"=>"/tarif/index", "active"=>"active")
                                       ,array("label"=>"PTKP", "url"=>"/ptkp/index", "active"=>""));
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_tarif"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        						
                                    array("label"=>"Batas Bawah"
                                                ,"name"=>"batasbawah"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
									 array("label"=>"Batas Atas"
                                                ,"name"=>"batasatas"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
									 array("label"=>"Tarif (%)"
                                                ,"name"=>"tarif"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""));	

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_TARIF" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_TARIF");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_TARIF");
        }
        # ---------------
        $data["select"]        = $qTarif->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qTarif->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.tablist", $data);
    }

    public function add() {
        $data["title"]         = "Add Tarif";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/tarif/save";
        /* ----------
         Tarif
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
       // $qStatus			   = getSelectStatusTarif();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"nama_tarif", "label"=>"Batas Bawah", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"nama_tarif", "label"=>"Batas Atas", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"nama_tarif", "label"=>"Tarif", "mandatory"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_tarif' => 'required'                     );

        $messages = ['nama_tarif.required' => 'Tarif harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/tarif/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qTarif  = new TarifModel;
            # ---------------
            $qTarif->createData($request);
            # ---------------
            session()->flash("success_message", "Tarif has been saved");
            # ---------------
            return redirect("/tarif/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Tarif";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/tarif/update";
        /* ----------
         Tarif
        ----------------------- */
        $qMaster              = new MasterModel;
        $qTarif             = new TarifModel;
        /* ----------
         Source
        ----------------------- */
        $qTarif             = $qTarif->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_tarif", "label"=>"Tarif ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"batasbawah", "label"=>"Batas Bawah", "readonly"=>"readonly", "value"=>number_format($qTarif->batasbawah,0)));
        $data["fields"][]      = form_text(array("name"=>"batasatas", "label"=>"Batas Atas", "readonly"=>"readonly", "value"=>number_format($qTarif->batasatas,0)));
        $data["fields"][]      = form_text(array("name"=>"tarif", "label"=>"Tarif",  "mandatory"=>"yes", "value"=>$qTarif->tarif));
      	        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'tarif' => 'required|'                 
        );

        $messages = [
                    'tarif.required' => 'Tarif harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/tarif/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qTarif      = new TarifModel;
            # ---------------
            $qTarif->updateData($request);
            # ---------------
            session()->flash("success_message", "Tarif has been updated");
            # ---------------
            return redirect("/tarif/index");
        }
    }

    public function delete($id) {
     
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qTarif     = new TarifModel;
            # ---------------
            $qTarif->removeData($request);
            # ---------------
            session()->flash("success_message", "Tarif has been removed");
        } else {
            session()->flash("error_message", "Tarif cannot be removed");
        }
      # ---------------
        return redirect("/tarif/index"); 
    }
}
