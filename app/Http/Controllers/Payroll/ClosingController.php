<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Payroll\ClosingModel;
class ClosingController extends Controller
{
    public function __construct(Request $request)
     {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = "Closing Periode Gaji";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/closing/update";
        /* ----------
         Closing
        ----------------------- */
        $qMaster              = new MasterModel;
        $qClosing             = new ClosingModel;
        /* ----------
         Source
        ----------------------- */

         $data["tabs"]          = array( array("label"=>"Proses Gaji", "url"=>"/prosesgaji/index/", "active"=>""),
					array("label"=>"Update Gaji", "url"=>"/updategaji/index/", "active"=>""),
				       array("label"=>"Closing", "url"=>"/closing/index", "active"=>"active"));
                                 
        //$idClosing             = explode("&", $id); 
        $qClosing        	   = $qClosing->getProfile()->first();
        $NMonth                = getNextMonth($qClosing->bulanpayroll);
        $NYear				   = getNextYear($qClosing->bulanpayroll,$qClosing->tahunpayroll);	
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_hidden(array("name"=>"bulanpayroll", "label"=>"Bulan Berjalan","readonly"=>"readonly", "value"=>$qClosing->bulanpayroll));
        $data["fields"][]      = form_text(array("name"=>"bulanpayrolltext1", "label"=>"Bulan Berjalan","readonly"=>"readonly", "value"=>getMonthName($qClosing->bulanpayroll, "id")));
        $data["fields"][]      = form_text(array("name"=>"tahunpayroll", "label"=>"Tahun","readonly"=>"readonly", "value"=>$qClosing->tahunpayroll));

        // $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes","readonly"=>"readonly", "value"=>""));
        $data["fields"][]      = form_hidden(array("name"=>"bulanpayroll2", "label"=>"", "mandatory"=>"yes","value"=>getNextMonth($qClosing->bulanpayroll)));
        $data["fields"][]      = form_text(array("name"=>"bulanpayrolltext2", "label"=>"Bulan Berikutnya","readonly"=>"readonly", "mandatory"=>"yes","value"=>getMonthName($NMonth, "id")));
        $data["fields"][]      = form_text(array("name"=>"tahunpayroll2", "label"=>"Tahun","readonly"=>"readonly", "mandatory"=>"yes","value"=>$NYear));
        
        $data["fields"][]      = form_text(array("name"=>"tgl_payrollawal", "label"=>"Tanggal payroll (Awal)", "readonly"=>"readonly","value"=>$qClosing->tgl_payrollawal));
        $data["fields"][]      = form_text(array("name"=>"tgl_payrollakhir", "label"=>"Tanggl Payroll (Akhir)", "readonly"=>"readonly","value"=>$qClosing->tgl_payrollakhir));
      


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        
    }

    

   
    public function update(Request $request)
    {
        
        
        $rules = array(
                    'bulanpayrolltext2' => 'required|',
                    
                                          
        );

        $messages = [
                    'bulanpayrolltext2.required' => 'Bulan Berikutnya harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/closing/index/" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qClosing      = new ClosingModel;
            # ---------------
            $qClosing->updateData($request);
            # ---------------
            session()->flash("success_message", "Closing has been updated");
            # ---------------
          //  return redirect("/closing/index/");
            return redirect("/prosesgaji/index/");
        }
     }
}
