<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Payroll\UpdategajiModel;
use App\Model\Personalia\KaryawanModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Datetime;

class UpdateGajiController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        // dd(session()->has("SES_SEARCH_UPDATEGAJI"));        
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/updategaji/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qUpdategaji            = new UpdategajiModel;
        $qMaster                = new MasterModel;
        $qperiode               = $qUpdategaji->getPeriode()->first();
        $NMonth                 = getNextMonth($qperiode->bulanpayroll);
        $NYear				          = getNextYear($qperiode->bulanpayroll,$qUpdategaji->tahunpayroll);	
        /* ----------
         Fields
        ----------------------- */

        /* ----------
         Source
        ----------------------- */
       // $qKaryawan                = $qMaster->getKaryawan($idkry)->first();
        //$data["form_act_add"]     = "/updategaji/add/";
        $data["form_act_edit"]    = "/updategaji/edit";
        $data["form_act_pph"]    = "/updategaji/pph";
        $data["form_act_export"]    = "/updategaji/export";
        $data["form_act_import"]    = "/updategaji/import";
        
        //$data["form_act_delete"]  = "/updategaji/delete";
       
        
        # ---------------*/
        	 $data["tabs"]   = array(
                                 array("label"=>"Proses Gaji", "url"=>"/prosesgaji/index/", "active"=>""), 
                                 array("label"=>"Update Gaji", "url"=>"/updategaji/index/", "active"=>"active"),
                                 array("label"=>"Closing", "url"=>"/closing/index", "active"=>""));
         
     // $data["fields"][]    = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fieldsHeader"][]    = form_hidden(array("name"=>"bulanpayroll", "label"=>"Bulan Berjalan","readonly"=>"readonly", "value"=>$qperiode->bulanpayroll));
        $data["fieldsHeader"][]    = form_text(array("name"=>"bulanpayrolltext1", "label"=>"Bulan Berjalan","readonly"=>"readonly", "value"=>getMonthName($qperiode->bulanpayroll, "id")));
        $data["fieldsHeader"][]    = form_text(array("name"=>"tahunpayroll", "label"=>"Tahun","readonly"=>"readonly", "value"=>$qperiode->tahunpayroll));
        
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));         

        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_penggajian"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"NIK"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                          ,"add-style"=>""),
                                     array("label"=>"Medical"
                                                ,"name"=>"tunj_khusus"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),     
                                       array("label"=>"Koreksi Plus"
                                                ,"name"=>"koreksiplus"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),                     

										                array("label"=>"Koreksi Min"
                                                ,"name"=>"koreksimin"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                   array("label"=>"Insentif"
                                                ,"name"=>"insentif"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Bonus"
                                                ,"name"=>"bonus"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Pesangon"
                                                ,"name"=>"pesangon"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),   


                                      array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     
                                      array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),


                                      
                                     
                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_UPDATEGAJI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_UPDATEGAJI");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_UPDATEGAJI");
        }
        # ---------------
        $data["select"]        = $qUpdategaji->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qUpdategaji->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        // dd($)
       // return view("default.list", $data);
         return view("default.updategaji", $data);
    }

   
    public function edit($id) {
        $data["title"]        = "Edit Updategaji";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/updategaji/update";
        /* ----------
         Updategaji
        ----------------------- */
        $qMaster              = new MasterModel;
        $qUpdategaji          = new UpdategajiModel;
        /* ----------
         Source
        ----------------------- */
        //$idUpdategaji             = explode("&", $id); 
        $qUpdategaji           = $qUpdategaji->getProfile($id)->first();
        
		

        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Proses", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly","value"=>$qUpdategaji->nik));
        $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly","value"=>$qUpdategaji->nama_karyawan));
          $data["fields"][]      = form_currency(array("name"=>"insentif", "label"=>"Insentif",  "mandatory"=>"", "value"=>number_format($qUpdategaji->insentif,0)));
           $data["fields"][]      = form_currency(array("name"=>"tunj_khusus", "label"=>"Medical",  "mandatory"=>"", "value"=>number_format($qUpdategaji->tunj_khusus,0)));

              $data["fields"][]      = form_currency(array("name"=>"bonus", "label"=>"Bonus",  "mandatory"=>"", "value"=>number_format($qUpdategaji->bonus,0)));
               $data["fields"][]      = form_currency(array("name"=>"pesangon", "label"=>"Pesangon",  "mandatory"=>"", "value"=>number_format($qUpdategaji->pesangon,0)));
      
      
        $data["fields"][]      = form_currency(array("name"=>"koreksiplus", "label"=>"Koreksi Plus",  "mandatory"=>"", "value"=>number_format($qUpdategaji->koreksiplus,0)));
        $data["fields"][]      = form_currency(array("name"=>"koreksimin", "label"=>"Koreksi Min", "value"=>number_format($qUpdategaji->koreksimin,0)));
      
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function pph(Request $request)
    {

         $qUpdategaji      = new UpdategajiModel;
            # ---------------
            $qUpdategaji->updatepph($request);
        return redirect("/updategaji/index");
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'koreksiplus' => 'required|',
                    
                                          
        );

        $messages = [
                    'koreksiplus' => 'koreksiplus Minimal 0',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/updategaji/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qUpdategaji      = new UpdategajiModel;
            # ---------------
            $qUpdategaji->updateData($request);
            # ---------------
            session()->flash("success_message", "Updategaji has been updated");
            # ---------------
            return redirect("/updategaji/index");
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/updategaji/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qUpdategaji             = new UpdategajiModel;

       //$idUpdategaji             = explode("&", $id); 
        $qUpdategaji        = $qUpdategaji->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan(); 
		$qStatuskerja          = getSelectStskerja();
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id_updategaji", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qUpdategaji->no_surat));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qUpdategaji->id_karyawan));
              
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id_updategaji") != 1) {
            $qUpdategaji     = new UpdategajiModel;
            # ---------------
            $qUpdategaji->removeData($request);
            # ---------------
            session()->flash("success_message", "Updategaji has been removed");
        } else {
            session()->flash("error_message", "Updategaji cannot be removed");
        }
      # ---------------
        return redirect("/updategaji/index/". $request->input("id_karyawan")); 
    }

    public function export()
    {
        
        $qUpdategaji            = new UpdategajiModel;
        $qperiode               = $qUpdategaji->getPeriode()->first();
        $bulan                  = getMonthName($qperiode->bulanpayroll, "id");
        $tahun                  = $qperiode->tahunpayroll;
        $tgl_payrollakhir       = $qperiode->tgl_payrollakhir;
        $tgl_payrollakhirplusebulan = date('Y-m-d', strtotime($tgl_payrollakhir. ' + 1 months'));
        $mKaryawan              = new KaryawanModel;

        $date = new DateTime();
        $manual_date = $date->setDate(1970, 01, 01);
        $timestamp = $manual_date->format('Y-m-d'); 
        $rangedate = $this->getDatesFromRange($timestamp, $tgl_payrollakhir);
        $rangedate2 = $this->getDatesFromRange($qperiode->tgl_payrollawal, $tgl_payrollakhirplusebulan);
        
        $query  = DB::table("p_penggajian as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_level","f.nama_bank")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_level as e","e.id_level","=","d.id_level")
                            ->leftjoin("p_bank as f","f.id_bank","=","a.id_bank")
                            ->where("a.bulan",$qperiode->bulanpayroll)
                            ->where("a.tahun",$tahun)
                            ->whereDate('a.tgl_masuk', '<=', $qperiode->tgl_payrollakhir)
                            ->orderBy("a.id_cabang","a.id_departemen", "ASC")
                            ->get();
        foreach($query as $key => $value){
          if($value->tgl_keluar == null ){
            $queryy[] = $value; 
          }else{
            if(in_array($value->tgl_keluar, $rangedate2)){
            $queryy[] = $value; 
            }
          }
        }
        // $query = $queryy;

  $styleArray = [
      'font' => [
          'bold' => true,
      ],
      'alignment' => [
          'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      ],
      'borders' => [
          'top' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
          ],
      ],
      'fill' => [
          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
          'rotation' => 90,
          'startColor' => [
              'argb' => 'FFA0A0A0',
          ],
          'endColor' => [
              'argb' => 'FFFFFFFF',
          ],
      ],
  ];






                            
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->getStyle('A3:AX3')->applyFromArray($styleArray);
       
      //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
  
        $sheet->setCellValue('A1', 'DAFTAR GAJI KARYAWAN');
        $sheet->setCellValue('A2', 'PERIODE : '.$bulan ." - ".$tahun);
        
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
          
        $sheet->setCellValue('A3', 'NO.');    
        $sheet->setCellValue('B3', 'N I K');
        $sheet->setCellValue('C3', 'NAMA');
        $sheet->setCellValue('D3', 'LEVEL');
        $sheet->setCellValue('E3', 'JABATAN');
        $sheet->setCellValue('F3', 'DEPARTEMEN');
        $sheet->setCellValue('G3', 'CABANG');
        $sheet->setCellValue('H3', 'STATUS KERJA');
        $sheet->setCellValue('I3', 'TANGGAL MASUK');
        $sheet->setCellValue('J3', 'TANGGAL KELUAR');
        $sheet->setCellValue('K3', 'NO REKENING');
        $sheet->setCellValue('L3', 'BANK');
        $sheet->setCellValue('M3', 'JENIS PAJAK');
        $sheet->setCellValue('N3', "STATUS PAJAK");

        $sheet->setCellValue('O3', 'NPWP');
        $sheet->setCellValue('P3', "NO BPJS KESEHATAN");
        $sheet->setCellValue('Q3', 'NO BPJS TK');

        $sheet->setCellValue('R3','RATE_BPJSTK');
        $sheet->setCellValue('S3','RATE_BPJSKES');
        $sheet->setCellValue('T3','GAJI_POKOKFULL');
        $sheet->setCellValue('U3','GAJI_POKOK');
        $sheet->setCellValue('V3','TUNJ_JABATAN');
        $sheet->setCellValue('W3','TUNJ_TRANSPORT');
        $sheet->setCellValue('X3','TUNJ_MEDICAL');
        $sheet->setCellValue('Y3','TUNJ_JAGA');
        $sheet->setCellValue('Z3','TUNJ_KEMAHALAN');
        $sheet->setCellValue('AA3','UANG MAKAN/HARI');
        $sheet->setCellValue('AB3','TUNJ_UANG_MAKAN');
        $sheet->setCellValue('AC3','INSENTIF');
        $sheet->setCellValue('AD3','KOREKSIPLUS');
        $sheet->setCellValue('AE3','TUNJ_PPH21');
        $sheet->setCellValue('AF3','TOTAL PENDAPATAN');
        $sheet->setCellValue('AG3','KOREKSIMIN');
        $sheet->setCellValue('AH3','SIMPANAN_WAJIB');
        $sheet->setCellValue('AI3','POTONGAN_ABSEN');
        $sheet->setCellValue('AJ3','PINJAMAN_KOPERASI');
        $sheet->setCellValue('AK3','BPJS_KES_KRY');
        $sheet->setCellValue('AL3','JHT_KRY');
        $sheet->setCellValue('AM3','JP_KRY');
        $sheet->setCellValue('AN3','PPH21');
        $sheet->setCellValue('AO3','TOTAL POTONGAN');
        $sheet->setCellValue('AP3','TOTAL THP');
        $sheet->setCellValue('AQ3','BPJS_KES_PRS');
        $sheet->setCellValue('AR3','JHT_PRS');
        $sheet->setCellValue('AS3','JKK');
        $sheet->setCellValue('AT3','JKM');
        $sheet->setCellValue('AU3','JP_PRS');
        $sheet->setCellValue('AV3','UMH');
        $sheet->setCellValue('AW3','HADIR');
       // $sheet->setCellValue('AX3','PENEMPATAN');
        
        $gaji_pokok=0;
        $tunj_jabatan=0;
        $tunj_transport=0;
        $tunj_khusus=0;
        $tunj_jaga=0;
        $tunj_kemahalan=0;
        $tunj_makan=0;
        $uang_makan=0;
        $insentif=0;
        $koreksiplus=0;
        $tunj_pph21=0;
        $totpendapatan=0;
        $koreksimin=0;
        $simpanan_wajib=0;
        $potongan_absen=0;
        $pinjaman_koperasi=0;
        $pph21=0;
        $totpotongan=0;
        $totthp=0;

        foreach($query as $row) 
        {
                  $pendapatan=$row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->uang_makan+$row->insentif+$row->koreksiplus+$row->tunj_pph21+$row->tunj_khusus;
                  $potongan=$row->koreksimin+$row->simpanan_wajib+$row->potongan_absen+$row->pinjaman_koperasi+$row->pph21+$row->bpjs_kes_kry+$row->jht_kry+$row->jp_kry;
                  $thp=$pendapatan-$potongan;
                  $statuspajak            = $qUpdategaji->getStatuspajak($row->id_ptkp);
                  $qKaryawan              = $mKaryawan->getProfile($row->id_karyawan);

                 


                  $jenispajak             = "Grossup"; 
                  if($row->jenispajak==1)
                  {

                  } 
                  
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nik);
                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('D'.$baris,$row->nama_level);
                  $sheet->setCellValue('E'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('F'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('G'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('H'.$baris,$row->status_kerja);
                  $sheet->setCellValue('I'.$baris,$row->tgl_masuk);
                  $sheet->setCellValue('J'.$baris,$row->tgl_keluar);
                  $sheet->setCellValue('K'.$baris,'="'.$row->no_rekening.'"');
                  $sheet->setCellValue('L'.$baris,$row->nama_bank);
                  $sheet->setCellValue('M'.$baris,$jenispajak);
                  $sheet->setCellValue('N'.$baris, $statuspajak);
          
                  $sheet->setCellValue('O'.$baris,$row->nonpwp);
                  $sheet->setCellValue('P'.$baris,$row->nobpjskes);
                  $sheet->setCellValue('Q'.$baris,$row->nobjstk);

                  $sheet->setCellvalue('R'.$baris,$row->rate_bpjstk);
                //  $sheet->setCellValue('Q'.$baris,sprintf("%0.2f",$row->rate_bpjstk),true)->getStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);



                  $sheet->setCellvalue('S'.$baris,$row->rate_bpjskes);
                  $sheet->setCellvalue('T'.$baris,$row->gaji_pokokfull);
                  $sheet->setCellvalue('U'.$baris,$row->gaji_pokok);
                  $sheet->setCellvalue('V'.$baris,$row->tunj_jabatan);
                  $sheet->setCellvalue('W'.$baris,$row->tunj_transport);
                  $sheet->setCellvalue('X'.$baris,$row->tunj_khusus);
                  $sheet->setCellvalue('Y'.$baris,$row->tunj_jaga);
                  $sheet->setCellvalue('Z'.$baris,$row->tunj_kemahalan);
                  $sheet->setCellvalue('AA'.$baris,$row->tunj_makan);
                  $sheet->setCellvalue('AB'.$baris,$row->uang_makan);
                  $sheet->setCellvalue('AC'.$baris,$row->insentif);
                  $sheet->setCellvalue('AD'.$baris,$row->koreksiplus);
                  $sheet->setCellvalue('AE'.$baris,$row->tunj_pph21);
                  $sheet->setCellvalue('AF'.$baris,$pendapatan);
                  $sheet->setCellvalue('AG'.$baris,$row->koreksimin);
                  $sheet->setCellvalue('AH'.$baris,$row->simpanan_wajib);
                  $sheet->setCellvalue('AI'.$baris,$row->potongan_absen);
                  $sheet->setCellvalue('AJ'.$baris,$row->pinjaman_koperasi);
                   $sheet->setCellvalue('AK'.$baris,$row->bpjs_kes_kry);
                  $sheet->setCellvalue('AL'.$baris,$row->jht_kry);
                  $sheet->setCellvalue('AM'.$baris,$row->jp_kry);
                  $sheet->setCellvalue('AN'.$baris,$row->pph21);
                  $sheet->setCellvalue('AO'.$baris,$potongan);
                  $sheet->setCellvalue('AP'.$baris,$thp);
                  $sheet->setCellvalue('AQ'.$baris,$row->bpjs_kes_prs);
                  $sheet->setCellvalue('AR'.$baris,$row->jht_prs);
                  $sheet->setCellvalue('AS'.$baris,$row->jkk);
                  $sheet->setCellvalue('AT'.$baris,$row->jkm);
                  $sheet->setCellvalue('AU'.$baris,$row->jp_prs);
                  $sheet->setCellvalue('AV'.$baris,$row->jml_hadir);
                  $sheet->setCellvalue('AW'.$baris,$row->makanperhari);
		 // $sheet->setCellvalue('AX'.$baris,$row->penempatan);
                  
                  $gaji_pokok     = $gaji_pokok+$row->gaji_pokok;
                  $tunj_jabatan    = $tunj_jabatan+$row->tunj_jabatan;
                  $tunj_transport  = $tunj_transport+$row->tunj_transport;
                  $tunj_khusus     = $tunj_khusus+$row->tunj_khusus;
                  $tunj_jaga       = $tunj_jaga+$row->tunj_jaga;
                  $tunj_kemahalan  = $tunj_kemahalan+$row->tunj_kemahalan;
                  $tunj_makan      = $tunj_makan+$row->tunj_makan;
                  $uang_makan      = $uang_makan+$row->uang_makan;
                  $insentif        = $insentif+$row->insentif;
                  $koreksiplus     = $koreksiplus+$row->koreksiplus;
                  $tunj_pph21      = $tunj_pph21+$row->tunj_pph21;
                  $totpendapatan   = $totpendapatan+$pendapatan;
                  $koreksimin      = $koreksimin+$row->koreksimin;
                  $simpanan_wajib  = $simpanan_wajib+$row->simpanan_wajib;
                  $potongan_absen  = $potongan_absen+$row->potongan_absen;
                  $pinjaman_koperasi = $pinjaman_koperasi+$row->pinjaman_koperasi;
                  $pph21           = $pph21+$row->pph21;
                  $totpotongan     = $totpotongan+$potongan;
                  $totthp          = $totthp+$thp;



                  //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
                  


                 $baris=$baris+1;
        }
                  $sheet->setCellValue('A'.$baris,"GRAND TOTAL");
                  $sheet->setCellvalue('U'.$baris,$gaji_pokok);
                  $sheet->setCellvalue('V'.$baris,$tunj_jabatan);
                  $sheet->setCellvalue('W'.$baris,$tunj_transport);
                  $sheet->setCellvalue('X'.$baris,$tunj_khusus);
                  $sheet->setCellvalue('Y'.$baris,$tunj_jaga);
                  $sheet->setCellvalue('Z'.$baris,$tunj_kemahalan);
                  $sheet->setCellvalue('AA'.$baris,$tunj_makan);
                  $sheet->setCellvalue('AB'.$baris,$uang_makan);
                  $sheet->setCellvalue('AC'.$baris,$insentif);
                  $sheet->setCellvalue('AD'.$baris,$koreksiplus);
                  $sheet->setCellvalue('AE'.$baris,$tunj_pph21);
                  $sheet->setCellvalue('AF'.$baris,$totpendapatan);
                  $sheet->setCellvalue('AG'.$baris,$koreksimin);
                  $sheet->setCellvalue('AH'.$baris,$simpanan_wajib);
                  $sheet->setCellvalue('AI'.$baris,$potongan_absen);
                  $sheet->setCellvalue('AJ'.$baris,$pinjaman_koperasi);
                  $sheet->setCellvalue('AK'.$baris,$pph21);
                  $sheet->setCellvalue('AL'.$baris,$totpotongan);
                  $sheet->setCellvalue('AM'.$baris,$totthp);

         $spreadsheet->getActiveSheet()->getStyle('A4:AT'.$baris)->getNumberFormat()->setFormatCode('#,##0');
          $spreadsheet->getActiveSheet()->getStyle('A'.$baris.':AT'.$baris)->applyFromArray($styleArray);
          //dd($spreadsheet);


        //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
        foreach(range('B','Z') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
                  ->setAutoSize(true);     
                  


          }

        
        
        



           $spreadsheet->getActiveSheet()->getColumnDimension("AA")
                  ->setAutoSize(true);
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "data gaji".$bulan.$tahun;  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   

    }

    public function import() {
      $data["title"]        = "Import";
      $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
      $data["form_act"]     = "/updategaji/importsave";
      /* ----------
       Updategaji
      ----------------------- */
      $qMaster              = new MasterModel;
      $qUpdategaji          = new UpdategajiModel;
      /* ----------
       Source
      ----------------------- */
      //$idUpdategaji             = explode("&", $id); 
      
      /* ----------
       Fields
      ----------------------- */
      $data["fields"][]      = form_upload(array("name"=>"file_excel", "label"=>"File Excel", "mandatory"=>"yes"));
    
      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
      $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
      # ---------------
      return view("default.form", $data);
  }

  public function importsave(Request $request)
    { 
        $rules = array(
                    'file_excel' => 'required|'                 
        );

        $messages = [
                    'file_excel' => 'Pilih file',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("updategaji/import")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qUpdategaji      = new UpdategajiModel;
            # ---------------
            $qUpdategaji->importcsv($request);
            # ---------------
            session()->flash("success_message", "update has been updated");
            # ---------------
            return redirect("/updategaji/index");
        }
    }

    public function getDatesFromRange($start, $end){
      $dates = array($start);
      while(end($dates) < $end){
          $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
      }
      return $dates;
  }

}
