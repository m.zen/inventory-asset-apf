<?php


namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\ProsesgajiModel;
use App\Model\Payroll\UpdategajiModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

      
class LaporankoperasiController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index() {
        $data["title"]         = "Laporan Koperasi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lpkop/cetak";


        $qUpdategaji            = new UpdategajiModel;
        $qperiode               = $qUpdategaji->getPeriode()->first();
        $bulan                  = getMonthName($qperiode->bulanpayroll, "id");
        $tahun                  = $qperiode->tahunpayroll;
       
        $qJenisLaporan         = getLaporankoperasi();
        $data["fields"][]      = form_text(array("name"=>"bulan", "label"=>"Bulan", "readonly"=>"readonly", "value"=>$bulan));
        $data["fields"][]      = form_text(array("name"=>"tahun", "label"=>"Tahun", "readonly"=>"readonly", "value"=>$tahun));
        $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qJenisLaporan, "value"=>"1"));

        # ---------------
        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }
    public function cetak(Request $request) {
        /* ----------
         Model
        ----------------------- */
        if ($request->jenis_laporan=="1")
        {
        	dd("test");
				        $qUpdategaji            = new UpdategajiModel;
				        $qperiode               = $qUpdategaji->getPeriode()->first();
				        $bulan                  = getMonthName($qperiode->bulanpayroll, "id");
				        $tahun                  = $qperiode->tahunpayroll;
				       
				        $mKaryawan              = new KaryawanModel;
				        


				        $query  = DB::table("p_penggajian as a")
				                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_level")
				                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
				                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
				                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
				                            ->leftjoin("m_level as e","e.id_level","=","d.id_level")
				                            
				                            ->where("a.bulan",$qperiode->bulanpayroll)
				                            ->where("a.simpanan_wajib",">",0)
				                            ->where("a.tahun",$tahun)
				                            ->orderBy("a.id_cabang","a.id_departemen", "ASC")->get();

				        
				  $styleArray = [
				      'font' => [
				          'bold' => true,
				      ],
				      'alignment' => [
				          'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				      ],
				      'borders' => [
				          'top' => [
				              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				          ],
				      ],
				      'fill' => [
				          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				          'rotation' => 90,
				          'startColor' => [
				              'argb' => 'FFA0A0A0',
				          ],
				          'endColor' => [
				              'argb' => 'FFFFFFFF',
				          ],
				      ],
				  ];






				                            
				        $spreadsheet = new Spreadsheet();
				        $sheet = $spreadsheet->getActiveSheet();
				        $spreadsheet->getActiveSheet()->getStyle('A3:AU3')->applyFromArray($styleArray);
				       
				      //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
				  
				        $sheet->setCellValue('A1', 'DAFTAR POTONGAN KOPERASI');
				        $sheet->setCellValue('A2', 'PERIODE : '.$bulan ." - ".$tahun);
				        
				        $baris = 4;
				        /*-------------------------
				        KOLOM DATA
				        ---------------------------*/
				          
				        $sheet->setCellValue('A3', 'NO.');    
				        $sheet->setCellValue('B3', 'N I K');
				        $sheet->setCellValue('C3', 'NAMA');
				        $sheet->setCellValue('D3', 'JABATAN');
				        $sheet->setCellValue('E3', 'LEVEL');
				        $sheet->setCellValue('F3', 'DEPARTEMEN');
				        $sheet->setCellValue('G3', 'CABANG');
				        $sheet->setCellValue('H3','SIMPANAN_WAJIB');
				        $sheet->setCellValue('I3','PINJAMAN_KOPERASI');
				        $sheet->setCellValue('J3','TOTAL POTONGAN KOPERASI');
				       
				        $simpanan_wajib=0;
				        $pinjaman_koperasi=0;
				        $totpotongan=0;

				        foreach($query as $row) 
				        {
				                  $potongan=$row->simpanan_wajib+$row->pinjaman_koperasi;
				                 $sheet->setCellValue('A'.$baris,$baris-3);
				                  $sheet->setCellValue('B'.$baris,$row->nik);
				                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
				                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
				                  $sheet->setCellValue('E'.$baris,$row->nama_level);
				                  $sheet->setCellValue('F'.$baris,$row->nama_departemen);
				                  $sheet->setCellValue('G'.$baris,$row->nama_cabang);
				                 
				                  $sheet->setCellvalue('H'.$baris,$row->simpanan_wajib);
				                  $sheet->setCellvalue('I'.$baris,$row->pinjaman_koperasi);
				                  $sheet->setCellvalue('J'.$baris,$potongan);
				                  
				                  $simpanan_wajib  = $simpanan_wajib+$row->simpanan_wajib;
				                  $pinjaman_koperasi = $pinjaman_koperasi+$row->pinjaman_koperasi;
				                  
				                  $totpotongan     = $totpotongan+$potongan;
				          



				                  //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
				                  


				                 $baris=$baris+1;
				        }
				                  $sheet->setCellValue('A'.$baris,"GRAND TOTAL");
				                
				                  $sheet->setCellvalue('H'.$baris,$simpanan_wajib);
				                  $sheet->setCellvalue('I'.$baris,$pinjaman_koperasi);
				                  $sheet->setCellvalue('J'.$baris,$totpotongan);
				                
				         $spreadsheet->getActiveSheet()->getStyle('H4:J'.$baris)->getNumberFormat()->setFormatCode('#,##0');
				          $spreadsheet->getActiveSheet()->getStyle('A'.$baris.':J'.$baris)->applyFromArray($styleArray);
				          //dd($spreadsheet);


				        //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
				        foreach(range('B','J') as $columnID)
				         {
				             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
				                  ->setAutoSize(true);
				             $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
				                  ->setAutoSize(true);     
				                  


				          }

				        
				        
				        



				        $writer = new Xlsx($spreadsheet);
				        // $writer->save('data karyawanlengkap.xlsx');
				        $judul    = "potongan koperasi".$bulan.$tahun;  
				        $name_file = $judul.'.xlsx';
				      // $path = storage_path('Laporan\\'.$name_file);
				      $path = public_path().'/app/'.$name_file;
				      $contents = is_dir($path);
				      // $headers = array('Content-Type' => File::mimeType($path));
				      // dd($path.'/'.$name_file,$contents);
				      $writer->save($path);
				      $filename   = str_replace("@", "/", $path);
				        # ---------------
				        header("Cache-Control: public");
				        header("Content-Description: File Transfer");
				        header("Content-Disposition: attachment; filename=".$name_file);
				        header("Content-Type: application/xlsx");
				        header("Content-Transfer-Encoding: binary");
				        # ---------------
				        require "$filename";
				        # ---------------
				        exit;
        }
        else if ($request->jenis_laporan=="2")
        {

        	 			$qUpdategaji            = new UpdategajiModel;
				        $qperiode               = $qUpdategaji->getPeriode()->first();
				        $bulan                  = getMonthName($qperiode->bulanpayroll, "id");
				        $tahun                  = $qperiode->tahunpayroll;
				       
				        $mKaryawan              = new KaryawanModel;
				        


				        $query  = DB::table("p_pinjaman as a")
				                            ->select("a.*","d.nama_cabang","e.nama_departemen","f.nama_jabatan","g.nama_level","c.nama_karyawan","c.nik")
				                            ->leftjoin("p_koperasi as b","b.id_koperasi","=","a.id_koperasi")
				                            ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
				                            ->leftjoin("m_cabang as d","d.id_cabang","=","c.id_cabang")
				                            ->leftjoin("m_departemen as e","e.id_departemen","=","c.id_departemen")
				                            ->leftjoin("m_jabatan as f","f.id_jabatan","=","c.id_jabatan")
				                            ->leftjoin("m_level as g","g.id_level","=","f.id_level")
				                             ->where("a.status_pinjaman","T")
				                            
				                             ->where("a.bulan",$qperiode->bulanpayroll)
				                            ->where("a.tahun",$tahun)
				                            ->orderBy("a.id_pinjaman", "DESC")->get();

				                            

				        
				  $styleArray = [
				      'font' => [
				          'bold' => true,
				      ],
				      'alignment' => [
				          'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				      ],
				      'borders' => [
				          'top' => [
				              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				          ],
				      ],
				      'fill' => [
				          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				          'rotation' => 90,
				          'startColor' => [
				              'argb' => 'FFA0A0A0',
				          ],
				          'endColor' => [
				              'argb' => 'FFFFFFFF',
				          ],
				      ],
				  ];






				                            
				        $spreadsheet = new Spreadsheet();
				        $sheet = $spreadsheet->getActiveSheet();
				        $spreadsheet->getActiveSheet()->getStyle('A3:AU3')->applyFromArray($styleArray);
				       
				      //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
				  
				        $sheet->setCellValue('A1', 'DAFTAR PINJAMAN KOPERASI');
				        $sheet->setCellValue('A2', 'PERIODE : '.$bulan ." - ".$tahun);
				        
				        $baris = 4;
				        /*-------------------------
				        KOLOM DATA
				        ---------------------------*/
				          
				        $sheet->setCellValue('A3', 'NO.');    
				        $sheet->setCellValue('B3', 'N I K');
				        $sheet->setCellValue('C3', 'NAMA');
				        $sheet->setCellValue('D3', 'JABATAN');
				        $sheet->setCellValue('E3', 'LEVEL');
				        $sheet->setCellValue('F3', 'DEPARTEMEN');
				        $sheet->setCellValue('G3', 'CABANG');
				        $sheet->setCellValue('H3','NAMA PINJAMAN');
				        $sheet->setCellValue('I3','TENOR');
				        $sheet->setCellValue('J3','PERIODE ANGSURAN');
				        $sheet->setCellValue('K3','ANSURAN');
				        $sheet->setCellValue('L3','JUMLAH BAYAR');
				        $sheet->setCellValue('M3','SISA ANGSURAN');
						$sheet->setCellValue('N3','TOTAL PINJAMAN');
						$sheet->setCellValue('O3','KETERANGAN');



				       
				        $simpanan_wajib=0;
				        $pinjaman_koperasi=0;
				        $totpotongan=0;

				        foreach($query as $row) 
				        {
				                 $total = $row->jumlah_pinjaman+$row->total_bunga;
				                 $bayar = $row->angsuran *($row->angsuranke);
				                 $sheet->setCellValue('A'.$baris,$baris-3);
				                  $sheet->setCellValue('B'.$baris,$row->nik);
				                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
				                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
				                  $sheet->setCellValue('E'.$baris,$row->nama_level);
				                  $sheet->setCellValue('F'.$baris,$row->nama_departemen);
				                  $sheet->setCellValue('G'.$baris,$row->nama_cabang);
				                  $sheet->setCellValue('H'.$baris,$row->nama_pinjaman);
							      $sheet->setCellValue('I'.$baris,$row->tenor);
							      $sheet->setCellValue('J'.$baris,$row->angsuranke);
							      $sheet->setCellValue('K'.$baris,$row->angsuran);
							      $sheet->setCellValue('L'.$baris,$bayar);
							      $sheet->setCellValue('M'.$baris,$row->saldo_angsuran);
								  $sheet->setCellValue('N'.$baris,$total);
								 // $sheet->setCellValue('O'.$baris,setString($row->keterangan));
				                  
				                


				                  //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
				                  


				                 $baris=$baris+1;
				        }
				                
				         $spreadsheet->getActiveSheet()->getStyle('J4:O'.$baris)->getNumberFormat()->setFormatCode('#,##0');
				          $spreadsheet->getActiveSheet()->getStyle('A'.$baris.':J'.$baris)->applyFromArray($styleArray);
				          //dd($spreadsheet);


				        //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
				        foreach(range('B','O') as $columnID)
				         {
				             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
				                  ->setAutoSize(true);
				             $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
				                  ->setAutoSize(true);     
				                  


				          }

				        
				        
				        



				        $writer = new Xlsx($spreadsheet);
				        // $writer->save('data karyawanlengkap.xlsx');
				        $judul    = "potongan koperasi".$bulan.$tahun;  
				        $name_file = $judul.'.xlsx';
				      // $path = storage_path('Laporan\\'.$name_file);
				      $path = public_path().'/app/'.$name_file;
				      $contents = is_dir($path);
				      // $headers = array('Content-Type' => File::mimeType($path));
				      // dd($path.'/'.$name_file,$contents);
				      $writer->save($path);
				      $filename   = str_replace("@", "/", $path);
				        # ---------------
				        header("Cache-Control: public");
				        header("Content-Description: File Transfer");
				        header("Content-Disposition: attachment; filename=".$name_file);
				        header("Content-Type: application/xlsx");
				        header("Content-Transfer-Encoding: binary");
				        # ---------------
				        require "$filename";
				        # ---------------
				        exit; 
         


        }
        
        
    }
}
