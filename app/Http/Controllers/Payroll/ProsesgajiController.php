<?php

namespace App\Http\Controllers\Payroll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Payroll\ProsesgajiModel;
class ProsesgajiController extends Controller
{
    public function __construct(Request $request)
     {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = "Prosesgaji Periode Gaji";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/prosesgaji/update";
        /* ----------
         Prosesgaji
        ----------------------- */
        $qMaster              = new MasterModel;
        $qProsesgaji             = new ProsesgajiModel;
        /* ----------
         Source
        ----------------------- */

          $data["tabs"]          = array(array("label"=>"Proses Gaji", "url"=>"/prosesgaji/index/", "active"=>"active"), 
                                 array("label"=>"Update Gaji", "url"=>"/updategaji/index/", "active"=>""),
				array("label"=>"Closing", "url"=>"/closing/index", "active"=>""));
                                 
        //$idProsesgaji             = explode("&", $id); 
        $qProsesgaji           = $qProsesgaji->getProfile()->first();
        $NMonth                = getNextMonth($qProsesgaji->bulanpayroll);
        $NYear				   = getNextYear($qProsesgaji->bulanpayroll,$qProsesgaji->tahunpayroll);	
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]    = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]    = form_hidden(array("name"=>"bulanpayroll", "label"=>"Bulan Berjalan","readonly"=>"readonly", "value"=>$qProsesgaji->bulanpayroll));
        $data["fields"][]    = form_text(array("name"=>"bulanpayrolltext1", "label"=>"Bulan Berjalan","readonly"=>"readonly", "value"=>getMonthName($qProsesgaji->bulanpayroll, "id")));
        $data["fields"][]    = form_text(array("name"=>"tahunpayroll", "label"=>"Tahun","readonly"=>"readonly", "value"=>$qProsesgaji->tahunpayroll));
        $data["fields"][]    = form_hidden(array("name"=>"tgl_payrollawal", "label"=>"tgl_payrollawal","readonly"=>"readonly", "value"=>$qProsesgaji->tgl_payrollawal));
        $data["fields"][]    = form_hidden(array("name"=>"tgl_payrollakhir", "label"=>"tgl_payrollakhir","readonly"=>"readonly", "value"=>$qProsesgaji->tgl_payrollakhir));
                  


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        
    }

    

   
    public function update(Request $request)
    {
        
        
        $rules = array(
                    'bulanpayrolltext1' => 'required|',
                    
                                          
        );

        $messages = [
                    'bulanpayrolltext1.required' => 'Bulan Berikutnya harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/prosesgaji/index/" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qProsesgaji      = new ProsesgajiModel;
            # ---------------
            $qProsesgaji->updateData($request);
            # ---------------
            session()->flash("success_message", "Prosesgaji has been updated");
            # ---------------
           return redirect("/updategaji/index/");
        }
     }
}
