<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Payroll\UpdategajiModel;
use App\Model\Payroll\HitungthrModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
class HitungthrController extends Controller
{
   public function __construct(Request $request)
     {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = "Proses Hitung THR";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/thr/update";
        /* ----------
         Proses THR
        ----------------------- */

        $qMaster              = new MasterModel;
        $qProsesthr           = new HitungthrModel;
        /* ----------
         Source
        ----------------------- */
/*
          $data["tabs"]          = array(array("label"=>"Proses Gaji", "url"=>"/Prosesthr/index/", "active"=>"active"), 
                                 array("label"=>"Update Gaji", "url"=>"/updategaji/index/", "active"=>""),
                                  array("label"=>"Closing", "url"=>"/closing/index", "active"=>""));
       */                          
       // $idProsesthr          = explode("&", $id); 
        $qProsesthr            = $qProsesthr->getProfile()->first();
        $NMonth                = getNextMonth($qProsesthr->bulanthr);
        $NYear				   = getNextYear($qProsesthr->bulanthr,$qProsesthr->tahunthr);	
        $qBulan               = getSelectBulan();
        /* ----------
         Fields
        ----------------------- */

       
         $data["fields"][]      = form_select(array("name"=>"bulanthr", "label"=>"Bulan THR","mandatory"=>"yes", "value"=>$qProsesthr->bulanthr, "source"=>$qBulan));

       $data["fields"][]    = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
               $data["fields"][]    = form_text(array("name"=>"tahunthr", "label"=>"Tahun THR","mandatory"=>"yes", "value"=>$qProsesthr->tahunthr));
          $data["fields"][]      = form_datepicker(array("name"=>"tgl_thr", "label"=>"Tanggal THR", "mandatory"=>"yes","value"=>displayDMY($qProsesthr->tgl_thr,"/"), "first_selected"=>"yes"));
       
                  


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        
    }

    

   
    public function update(Request $request)
    {
        
     
        $rules = array(
                    'bulanthr' => 'required|',
                    
                                          
        );

        $messages = [
                    'bulanthr.required' => 'Bulan Berikutnya harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/thr/index/" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qProsesthr      = new HitungthrModel;
            # ---------------
             $qProsesthr->updateData($request);
           // # ---------------
            session()->flash("success_message", "Proses thr has been updated");
            # ---------------
           return redirect("/thr/index/");
        }
     }

      public function laporan_thr()
    {
        $data["title"]        = "Laporan THR";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/laporanthr/cetak";
        /* ----------
         Proses THR
        ----------------------- */

        $qMaster              = new MasterModel;
        $qProsesthr           = new HitungthrModel;
        $mKaryawan            = new KaryawanModel;
        /* ----------
         Source
        ----------------------- */
/*
          $data["tabs"]          = array(array("label"=>"Closing", "url"=>"/closing/index", "active"=>""),
                                 array("label"=>"Proses Gaji", "url"=>"/Prosesthr/index/", "active"=>"active"), 
                                 array("label"=>"Update Gaji", "url"=>"/updategaji/index/", "active"=>""));
       */                          
       // $idProsesthr          = explode("&", $id); 
        $qProsesthr            = $qProsesthr->getProfile()->first();
        $NMonth                = getNextMonth($qProsesthr->bulanthr);
        $NYear                 = getNextYear($qProsesthr->bulanthr,$qProsesthr->tahunthr);  
        $qBulan               = getSelectBulan();
        /* ----------
         Fields
        ----------------------- */

       
         $data["fields"][]      = form_select(array("name"=>"bulanthr", "label"=>"Bulan THR","mandatory"=>"yes", "value"=>$qProsesthr->bulanthr, "source"=>$qBulan));

       $data["fields"][]    = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
               $data["fields"][]    = form_text(array("name"=>"tahunthr", "label"=>"Tahun THR","mandatory"=>"yes", "value"=>$qProsesthr->tahunthr));
       
       
                  


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        
    }

     public function cetak(Request $request)
    {
        
      
        $bulan                  = $request->bulanthr;
        $tahun                  = $request->tahunthr;
        
        $query  = DB::table("p_thr as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_level")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_level as e","e.id_level","=","d.id_level")
                            ->where("a.bulan",$bulan)
                            ->where("a.tahun",$tahun)
                            ->orderBy("a.id_cabang","a.id_departemen", "ASC")
                            ->get();
        //                     dd($query);
        // foreach($query as $key => $value){
        //   if($value->tgl_keluar == null ){
        //     $queryy[] = $value; 
        //   }else{
        //     if(in_array($value->tgl_keluar, $rangedate2)){
        //     $queryy[] = $value; 
        //     }
        //   }
        // }
        // $query = $queryy;

  $styleArray = [
      'font' => [
          'bold' => true,
      ],
      'alignment' => [
          'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      ],
      'borders' => [
          'top' => [
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
          ],
      ],
      'fill' => [
          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
          'rotation' => 90,
          'startColor' => [
              'argb' => 'FFA0A0A0',
          ],
          'endColor' => [
              'argb' => 'FFFFFFFF',
          ],
      ],
  ];






                            
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $spreadsheet->getActiveSheet()->getStyle('A3:V3')->applyFromArray($styleArray);
       
      //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
  
        $sheet->setCellValue('A1', 'DAFTAR THR');
        $sheet->setCellValue('A2', 'PERIODE : '.$bulan ." - ".$tahun);
        
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
          
        $sheet->setCellValue('A3', 'NO.');    
        $sheet->setCellValue('B3', 'N I K');
        $sheet->setCellValue('C3', 'NAMA');
        $sheet->setCellValue('D3', 'JABATAN');
        $sheet->setCellValue('E3', 'DEPARTEMEN');
        $sheet->setCellValue('F3', 'CABANG');
        $sheet->setCellValue('G3', 'STATUS KERJA');
        $sheet->setCellValue('H3', 'TANGGAL MASUK');
        $sheet->setCellValue('I3', 'LAMA (TAHUN)');
        $sheet->setCellValue('J3', 'LAMA (BULAN)');
        $sheet->setCellValue('K3', 'LAMA (HARI)');
        $sheet->setCellValue('L3', 'NO REKENING');
        $sheet->setCellValue('M3', 'A/N REKENING');
        $sheet->setCellValue('N3', 'JENIS PAJAK');
        $sheet->setCellValue('O3', "STATUS PAJAK");
        $sheet->setCellValue('P3','GAJI_POKOK');
        $sheet->setCellValue('Q3','THR');
        $sheet->setCellValue('R3','TUNJ_PPH21');
        $sheet->setCellValue('S3','PENDAPATAN');
        $sheet->setCellValue('T3','PPH21');
        $sheet->setCellValue('U3','TOTAL POTONGAN');
        $sheet->setCellValue('V3','TOTAL THP');
        $gaji_pokok=0;
        $thr=0;
        $tunj_pph21=0;
        $totpendapatan=0;
        $pph21=0;
        $totpotongan=0;
        $totthp=0;

        foreach($query as $row) 
        {
                  $pendapatan=$row->thr+$row->tunj_pph21;
                  $potongan=$row->pph21;
                  $thp=$pendapatan-$potongan;
                  $qUpdategaji             = new UpdategajiModel;
                  $mKaryawan              = new KaryawanModel;
                  $statuspajak            = $qUpdategaji->getStatuspajak($row->id_ptkp);
                  $qKaryawan              = $mKaryawan->getProfile($row->id_karyawan);

                 


                  $jenispajak             = "Grossup"; 
                  if($row->jenispajak==1)
                  {

                  } 
                  
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nik);
                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('G'.$baris,$row->status_kerja);
                  $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                  $sheet->setCellValue('I'.$baris,$row->masakerjatahun);
                  $sheet->setCellValue('J'.$baris,$row->masakerjabulan);
                  $sheet->setCellValue('K'.$baris,$row->masakerjahari);
                  
                  $sheet->setCellValue('L'.$baris,'="'.$row->no_rekening.'"');
                  $sheet->setCellValue('M'.$baris,$row->anrekening);
                  $sheet->setCellValue('N'.$baris,$jenispajak);
                  $sheet->setCellValue('O'.$baris, $statuspajak);
                  $sheet->setCellvalue('P'.$baris,$row->gaji_pokok);
                  $sheet->setCellvalue('Q'.$baris,$row->thr);
                  $sheet->setCellvalue('R'.$baris,$row->tunj_pph21);
                  $sheet->setCellvalue('S'.$baris,$pendapatan);
                  $sheet->setCellvalue('T'.$baris,$row->pph21);
                  $sheet->setCellvalue('U'.$baris,$potongan);
                  $sheet->setCellvalue('V'.$baris,$thp);
                  
                  $gaji_pokok     = $gaji_pokok+$row->gaji_pokok;
                  $thr            = $thr +$row->thr;
                  $tunj_pph21      = $tunj_pph21+$row->tunj_pph21;
                  $totpendapatan   = $totpendapatan+$pendapatan;
                  $pph21           = $pph21+$row->pph21;
                  $totpotongan     = $totpotongan+$potongan;
                  $totthp          = $totthp+$thp;



                  //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
                  


                 $baris=$baris+1;
        }
                  $sheet->setCellValue('A'.$baris,"GRAND TOTAL");
                  // $sheet->setCellvalue('U'.$baris,$gaji_pokok);
                  // $sheet->setCellvalue('V'.$baris,$tunj_jabatan);
                  // $sheet->setCellvalue('W'.$baris,$tunj_transport);
                  // $sheet->setCellvalue('X'.$baris,$tunj_khusus);
                  // $sheet->setCellvalue('Y'.$baris,$tunj_jaga);
                  // $sheet->setCellvalue('Z'.$baris,$tunj_kemahalan);
                  // $sheet->setCellvalue('AA'.$baris,$tunj_makan);
                  // $sheet->setCellvalue('AB'.$baris,$uang_makan);
                  // $sheet->setCellvalue('AC'.$baris,$insentif);
                  // $sheet->setCellvalue('AD'.$baris,$koreksiplus);
                  // $sheet->setCellvalue('AE'.$baris,$tunj_pph21);
                  // $sheet->setCellvalue('AF'.$baris,$totpendapatan);
                  // $sheet->setCellvalue('AG'.$baris,$koreksimin);
                  // $sheet->setCellvalue('AH'.$baris,$simpanan_wajib);
                  // $sheet->setCellvalue('AI'.$baris,$potongan_absen);
                  // $sheet->setCellvalue('AJ'.$baris,$pinjaman_koperasi);
                  // $sheet->setCellvalue('AK'.$baris,$pph21);
                  // $sheet->setCellvalue('AL'.$baris,$totpotongan);
                  // $sheet->setCellvalue('AM'.$baris,$totthp);

         $spreadsheet->getActiveSheet()->getStyle('P4:V'.$baris)->getNumberFormat()->setFormatCode('#,##0');
          $spreadsheet->getActiveSheet()->getStyle('A'.$baris.':Y'.$baris)->applyFromArray($styleArray);
          //dd($spreadsheet);


       //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
        foreach(range('B','V') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             // $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
             //      ->setAutoSize(true);     
                  


          }

        
        
        



           $spreadsheet->getActiveSheet()->getColumnDimension("V")
                  ->setAutoSize(true);
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "laporan thr ".$bulan.$tahun;  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   

    }

}
