<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\PinjamanModel;
use App\Model\Payroll\ProsesgajiModel;
use App\Model\Master\MasterModel;
use PDF;
class PinjamanController extends Controller
{
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/pinjaman/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qPinjaman              = new PinjamanModel;

        $data["tabs"]          = array(array("label"=>"Anggota Pinjaman", "url"=>"/koperasi/index", "active"=>"")
                           ,array("label"=>"Pinjaman Koperasi", "url"=>"/pinjaman/index", "active"=>"active"));
      
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_pinjaman"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Tanggal Pinjam"
                                                ,"name"=>"tgl_pinjaman"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Pokok Pinjamanan"
                                                ,"name"=>"jumlah_pinjaman"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Bunga(%)"
                                                ,"name"=>"bunga_pinjaman"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Jumlah Bunga"
                                                ,"name"=>"total_bunga"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Total Pinjaman"
                                                ,"name"=>"total"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tenor"
                                                ,"name"=>"tenor"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Angsuran"
                                                ,"name"=>"angsuran"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Angsuran Ke"
                                                ,"name"=>"angsuranke"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Saldo"
                                                ,"name"=>"saldo_angsuran"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>"")

                                       
                                    );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PINJAMAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PINJAMAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PINJAMAN");
        }
        # ---------------
        $data["select"]        = $qPinjaman->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qPinjaman->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.tablist", $data);
    }

    public function add() {
        $data["title"]         = "Add Pinjaman";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/pinjaman/save";
        /* ----------
         Pinjaman
        ----------------------- */
        $qMaster               = new MasterModel;
        $qProsesgaji           = new ProsesgajiModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
        $qAnggota              = $qMaster->getSelectAngota();
        $qProsesgaji           = $qProsesgaji->getProfile()->first();
        $qBulan                = getSelectBulan();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"id_koperasi", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qAnggota));
        $data["fields"][]      = form_text(array("name"=>"nama_pinjaman", "label"=>"Nama Pinjaman", "mandatory"=>"yes","value"=>"Pinjaman Koperaso"));
       
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_pinjaman", "label"=>"Tanggal Pinjam", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_currency(array("name"=>"jumlah_pinjaman", "label"=>"Jumlah Pinjaman",  "mandatory"=>"yes", "value"=>number_format(0,0)));
        $data["fields"][]      = form_number(array("name"=>"bunga_pinjaman", "label"=>"Bunga(%)", "value"=>number_format(0,0)));
    	  $data["fields"][]      = form_currency(array("name"=>"tenor", "label"=>"Lama Pinjaman (bulan)", "mandatory"=>"yes","value"=>number_format(0,0)));
        $data["fields"][]      = form_select(array("name"=>"bulanpayroll", "label"=>"Bulan (awal cicilan)", "value"=>$qProsesgaji->bulanpayroll, "source"=>$qBulan));
        $data["fields"][]      = form_text(array("name"=>"tahunpayroll", "label"=>"Tahun", "value"=>$qProsesgaji->tahunpayroll));
        $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Keterangan", "mandatory"=>""));


        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'tgl_pinjaman' => 'required'                     );

        $messages = ['tgl_pinjaman.required' => 'Karyawan  harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pinjaman/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qPinjaman  = new PinjamanModel;
            # ---------------
            $qPinjaman->createData($request);
            # ---------------
            session()->flash("success_message", "Pinjaman has been saved");
            # ---------------
            return redirect("/pinjaman/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Pinjaman";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/pinjaman/update";
        /* ----------
         Pinjaman
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPinjaman            = new PinjamanModel;
        /* ----------
         Source
        ----------------------- */
        $qPinjaman             = $qPinjaman->getProfile($id)->first();

        $qGroups               = $qMaster->getSelectGroup();
        $qAnggota              = $qMaster->getSelectAngota();

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_pinjaman", "label"=>"Pinjaman ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]     = form_select(array("name"=>"id_koperasi", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qAnggota));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_pinjaman", "label"=>"Tanggal Pinjam", "mandatory"=>"yes","value"=>displayDMY($qPinjaman->tgl_pinjaman,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_currency(array("name"=>"jumlah_pinjaman", "label"=>"Jumlah Pinjaman",  "mandatory"=>"yes", "value"=>number_format($qPinjaman->jumlah_pinjaman,0)));
        $data["fields"][]      = form_number(array("name"=>"bunga_pinjaman", "label"=>"Bunga(%)", "value"=>$qPinjaman->bunga_pinjaman));
    	$data["fields"][]      = form_currency(array("name"=>"tenor", "label"=>"Lama Pinjaman (bulan)", "mandatory"=>"yes","value"=>$qPinjaman->tenor));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'id_koperasi' => 'required|' ,
                    'tgl_pinjaman' => 'required|'             
        );

        $messages = [
                    'id_koperasi.required' => 'Karyawan harus diisi',
                    'tgl_pinjaman.required'=>'Tanggal Daftar harus isi'

        ];

 


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pinjaman/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qPinjaman      = new PinjamanModel;
            # ---------------
            $qPinjaman->updateData($request);
            # ---------------
            session()->flash("success_message", "Pinjaman has been updated");
            # ---------------
            return redirect("/pinjaman/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Pinjaman";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/pinjaman/remove";
        /* ----------
         Source
        ----------------------- */
          $qMaster              = new MasterModel;
        $qPinjaman             = new PinjamanModel;
        $qPinjaman             = $qPinjaman->getProfile($id)->first();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_pinjaman", "label"=>"Pinjaman ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan));
          
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qPinjaman     = new PinjamanModel;
            # ---------------
            $qPinjaman->removeData($request);
            # ---------------
            session()->flash("success_message", "Pinjaman has been removed");
        } else {
            session()->flash("error_message", "Pinjaman cannot be removed");
        }
      # ---------------
        return redirect("/pinjaman/index"); 
    }

    public function Cetak($id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCPinjaman        = new PinjamanModel;
      /* ----------
        Source
      ----------------------- */
      //$idKontrakkerja     = explode("&", $id); 
      $qPinjaman             = $qCPinjaman->getProfile($id);
      $qAngsuran             =$qCPinjaman->getAngsuranpinjaman($id);


      //dd($qAngsuran);
      $data["pinjaman"]      = $qPinjaman ;
      $data["angsuran"]      = $qAngsuran ;
      $pdf = PDF::loadView('Payroll.lprangsuran',  $data);
      $nama_file =  "Angsuran";

        //$nama_file =  $qKontrakkerja->nama_karyawan & "-" & $qKontrakkerja->id_kontrakkerja;

      
       $pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprkontrak", $data);

    }

public function pelunasan($id) {
        $data["title"]        = "Pelunasan Pinjaman";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/pinjaman/updatepenlunasan";
        /* ----------
         Pinjaman
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPinjaman            = new PinjamanModel;
        /* ----------
         Source
        ----------------------- */
        $qPinjaman             = $qPinjaman->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qAnggota              = $qMaster->getSelectAngota();

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_pinjaman", "label"=>"Pinjaman ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]     = form_select(array("name"=>"id_koperasi", "label"=>"Nama Karyawan", "mandatory"=>"yes", "readonly"=>"readonly","source"=>$qAnggota,"value"=>$qPinjaman->id_koperasi));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_pinjaman", "label"=>"Tanggal Pinjam", "mandatory"=>"yes","value"=>displayDMY($qPinjaman->tgl_pinjaman,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_currency(array("name"=>"jumlah_pinjaman", "label"=>"Jumlah Pinjaman",  "mandatory"=>"yes","readonly"=>"readonly", "value"=>number_format($qPinjaman->jumlah_pinjaman,0)));
        $data["fields"][]      = form_number(array("name"=>"bunga_pinjaman", "label"=>"Bunga(%)","readonly"=>"readonly","value"=>$qPinjaman->bunga_pinjaman));
      $data["fields"][]      = form_currency(array("name"=>"tenor", "label"=>"Lama Pinjaman (bulan)", "mandatory"=>"yes","readonly"=>"readonly","value"=>$qPinjaman->tenor));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function updatepenlunasan(Request $request)
    {
        $rules = array(
                    'id_koperasi' => 'required|' ,
                    'tgl_pinjaman' => 'required|'             
        );

        $messages = [
                    'id_koperasi.required' => 'Karyawan harus diisi',
                    'tgl_pinjaman.required'=>'Tanggal Daftar harus isi'

        ];

 


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pinjaman/pelunasan/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qPinjaman      = new PinjamanModel;
            # ---------------
            $qPinjaman->updateData_lunas($request);
            # ---------------
            session()->flash("success_message", "Pinjaman has been updated");
            # ---------------
            return redirect("/pinjaman/index");
        }
    }












    public function histori_cetak($id) 
    {
        /* ----------
         Model
        ----------------------- */
        $qPinjamanMdl             = new PinjamanModel;
        /* ----------
         Data
        ----------------------- */
        $qPinjaman            = $qPinjamanMdl->getProfile($id)->first();
        $qAngsuran             = $qPinjamanMdl->getAngsuranpinjaman($id);
        $Line                  = 190;
        $No                    = 0;
        /* ----------
         Report
        ----------------------- */
        Fpdf::AddPage();
        # ----------------

        Fpdf::SetLineWidth(0.3);
        Fpdf::SetFont("Arial", "B", 11);
        Fpdf::Ln(7);
       
        //$logo = public_path("logo/".$qDataDS->Logo);
        //Fpdf::Image($logo, 170, 13, 23, 20);
        
        Fpdf::Cell(190, 5, "HISTORI BAYAR", 0, 0, 'C');
        Fpdf::Ln(5);
        /*
        Fpdf::SetFont("Arial", "", 9);
        Fpdf::Cell(40, 5, "N I K", 0, 0, 'L');
        Fpdf::Cell(10, 5, ":", 0, 0, 'L');
        Fpdf::Cell(40, 5,"Nama", 0, 0, 'L');
        Fpdf::Ln(5);
        pdf::Cell(40, 5, "NAMA", 0, 0, 'L');
        Fpdf::Cell(10, 5, ":", 0, 0, 'L');
        Fpdf::Cell(40, 5, $qPinjaman->nama_karyawan, 0, 0, 'L');
        Fpdf::Ln(5);

*/
        Fpdf::Ln(10);
        if(count( $qAngsuran ) == 0) {
            Fpdf::SetFont("Arial", "", 9);
            Fpdf::Cell(190, 5, "Tidak ada jatuh tempo diperiode tersebut", 0, 0, 'L');
            Fpdf::Ln();
            Fpdf::Cell(190, 5, "Terima Kasih", 0, 0, 'L');
            # ----------------
            Fpdf::Output(); exit;
        } else 
        { 

            Fpdf::SetFont("Arial", "", 8);
            
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(7, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(20,5, 'CICILAN', 0, 0, 'L');
            Fpdf::Cell(30, 5, 'BULAN', 0, 0, 'L');
            Fpdf::Cell(30, 5, 'TAHUN', 0, 0, 'L');
            Fpdf::Cell(30, 5, 'ANGSURAN', 0, 0, 'L');
            Fpdf::Cell(30, 5, 'BAYAR ANGSURAN', 0, 0, 'L');
            Fpdf::Cell(30, 5, 'SALDO ANGSURAN', 0, 0, 'L');
                       
            Fpdf::Ln(7);
            Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
            Fpdf::Ln(3);
            foreach ( $qAngsuran  as $row) 
            {
                $No = $No + 1;
                Fpdf::SetFont("Arial", "", 8);
              Fpdf::Cell(7, 5, $No, 0, 0, 'C');
              Fpdf::Cell(20,5, $row->cicilanke, 0, 0, 'L');
              Fpdf::Cell(30, 5, $row->bulanangsur, 0, 0, 'L');
              Fpdf::Cell(30, 5, $row->tahunangsur, 0, 0, 'L');
              Fpdf::Cell(30, 5, number_format($row->jml_angsuran,0), 0, 0, 'R');
              Fpdf::Cell(30, 5, number_format($row->bayar_angsuran,0), 0, 0, 'R');
              Fpdf::Cell(30, 5, number_format($row->saldo_angsuran,0), 0, 0, 'R');
            
                Fpdf::Ln(8);
               
                }
              
            }
            
            Fpdf::Output(); exit;
        
    }
}
