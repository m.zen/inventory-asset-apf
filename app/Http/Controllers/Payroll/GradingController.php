<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Payroll\GradingModel;
class GradingController extends Controller
{
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/grading/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qGrading                  = new GradingModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_grading"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"Kode Grading"
                                                ,"name"=>"kode_grading"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Nama Grading"
                                                ,"name"=>"nama_grading"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
									array("label"=>"Batas Bawah"
                                                ,"name"=>"batasbawah"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),


        							array("label"=>"Batas Atas"
                                                ,"name"=>"batasatas"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>"")
);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_CABANG" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_CABANG");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_CABANG");
        }
        # ---------------
        $data["select"]        = $qGrading->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qGrading->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Grading";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/grading/save";
        /* ----------
         Grading
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
       // $qStatus			   = getSelectStatusGrading();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
	   $data["fields"][]      = form_text(array("name"=>"kode_grading", "label"=>"Kode Grading", "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_grading", "label"=>"Nama Grading", "mandatory"=>"yes"));
       $data["fields"][]      = form_currency(array("name"=>"batasbawah", "label"=>"Batas Bawah",  "mandatory"=>"yes","value"=>0));
       $data["fields"][]      = form_currency(array("name"=>"batasatas", "label"=>"Batas Bawah",  "mandatory"=>"yes","value"=>0));
            
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_grading' => 'required'                     );

        $messages = ['nama_grading.required' => 'Grading harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/Grading/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrading  = new GradingModel;
            # ---------------
            $qGrading->createData($request);
            # ---------------
            session()->flash("success_message", "Grading has been saved");
            # ---------------
            return redirect("/grading/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Grading";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/grading/update";
        /* ----------
         Grading
        ----------------------- */
        $qMaster              = new MasterModel;
        $qGrading             = new GradingModel;
        /* ----------
         Source
        ----------------------- */
        $qGrading             = $qGrading->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();

        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_grading", "label"=>"Grading ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
       $data["fields"][]      = form_text(array("name"=>"kode_grading", "label"=>"Kode Grading", "mandatory"=>"yes", "value"=>$qGrading->kode_grading));
       $data["fields"][]      = form_text(array("name"=>"nama_grading", "label"=>"Nama Grading", "mandatory"=>"yes","value"=>$qGrading->nama_grading));
       $data["fields"][]      = form_currency(array("name"=>"batasbawah", "label"=>"Batas Bawah", "mandatory"=>"yes", "value"=>number_format($qGrading->batasbawah,0)));
       $data["fields"][]      = form_currency(array("name"=>"batasatas", "label"=>"Batas Atas", "mandatory"=>"yes","value"=>number_format($qGrading->batasatas,0)));
             # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_grading' => 'required|'               
        );

        $messages = [
                    'nama_grading.required' => 'Grading harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/grading/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrading      = new GradingModel;
            # ---------------
            $qGrading->updateData($request);
            # ---------------
            session()->flash("success_message", "Grading has been updated");
            # ---------------
            return redirect("/grading/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Grading";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/grading/remove";
        /* ----------
         Source
        ----------------------- */
        $qGrading     = new GradingModel;
         $qGrading                 = $qGrading->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_grading", "label"=>"Grading ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_grading", "label"=>"Grading", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qGrading->nama_grading));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qGrading     = new GradingModel;
            # ---------------
            $qGrading->removeData($request);
            # ---------------
            session()->flash("success_message", "Grading has been removed");
        } else {
            session()->flash("error_message", "Grading cannot be removed");
        }
      # ---------------
        return redirect("/grading/index"); 
    }
}
