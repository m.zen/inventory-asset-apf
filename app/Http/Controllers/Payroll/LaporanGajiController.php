<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\ProsesgajiModel;
use App\Model\Payroll\UpdategajiModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Master\MasterModel;
use App\Model\Master\CabangModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class LaporanGajiController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index() {
        $data["title"]         = "Laporan Gaji";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lpgaji/cetak";


        $qUpdategaji            = new UpdategajiModel;
        $qMaster               = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $qperiode               = $qUpdategaji->getPeriode()->first();
        $bulan                  = getMonthName($qperiode->bulanpayroll, "id");
        $tahun                  = $qperiode->tahunpayroll;
        $qJenisLaporan         = getLaporangaji();
       
        $data["fields"][]      = form_hidden(array("name"=>"periode", "label"=>"Departemen ID", "readonly"=>"readonly", "value"=>$qperiode->bulanpayroll));
         $data["fields"][]      = form_text(array("name"=>"bulan", "label"=>"Bulan", "readonly"=>"readonly", "value"=>$bulan));
        $data["fields"][]      = form_text(array("name"=>"tahun", "label"=>"Tahun", "readonly"=>"readonly", "value"=>$tahun));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Perusahaan", "mandatory"=>"yes", "source"=>$qCabang));
        $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qJenisLaporan, "value"=>"1"));

        # ---------------
        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }

    public function laporanrekap(Request $request){
    	 $name_file = 'rekapgaji.xls';
    $fromfolder = public_path().'/'.$name_file;
    $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( $fromfolder );
     $sheet->getActiveSheet("GAJI")->setCellValue( "A1",$request->id_cabang );
      $sheet->getActiveSheet("REKAP")->setCellValue( "A1",$request->id_cabang );

              //   $query  = DB::table("m_divisi")
              //               ->select("*")
              //               ->orderBy("id_divisi","ASC");
                            
                            
              //        $tdivisi=$query->get();

                     
                    
              //        $kolom = "C";
              //        $baris = 5;

              //        $MT=0;
              //        $ST=0;
              //        $MK=0;
              //        $SK=0;
              //        foreach($tdivisi as $row) 

              //        {
                          
                        
              //                $query2  = DB::table("p_karyawan as a")
              //                               ->select(DB::raw('count(a.nik) as jumlah'),"a.status_kerja","b.id_level")
              //                               ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
              //                               ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
              //                               ->where("a.aktif", 1)
			           //          ->where("a.tgl_keluar",null)
              // ->where("a.tgl_keluar","=", null)
              //                               ->where("b.id_level","<",4)
              //                               ->where("c.id_divisi",$row->id_divisi)
              //                               ->groupBy("b.id_level","a.status_kerja");

              //                 $tKaryawan = $query2->get();
                        
                           
              //                 $baris =$baris + 1; 
              //                 // $sheet->getActiveSheet()->setCellValue( "B" .$baris, $row->nama_divisi); 
              //                  $dtMT=0;
              //                  $dtST=0;
              //                  $dtMK=0;
              //                  $dtSK=0; 
              //                 foreach($tKaryawan as $row2)  
              //                 {
              //                   //C =>MT, D=>ST,E=>MK,F=>SK
              //                   if($row2->id_level==3 and $row2->status_kerja=="TETAP")
              //                   {

              //                      // $sheet->getActiveSheet()->setCellValue( "C" .$baris,$row2->jumlah ); 
              //                       $MT=$MT+$row2->jumlah;
              //                       $dtMT=$dtMT+$row2->jumlah;
                    
              //                   }
              //                   if($row2->id_level==3 and ($row2->status_kerja=="KONTRAK" or $row2->status_kerja=="PERCOBAAN"))
              //                   {

              //                       //$sheet->getActiveSheet()->setCellValue( "E" .$baris, $row2->jumlah);  

              //                        $dtMK=$dtMK+$row2->jumlah;
              //                        $MK=$MK+$row2->jumlah;

              //                   }
                                

              //                   if($row2->id_level<3 and $row2->status_kerja=="TETAP")
              //                   {

              //                       //$sheet->getActiveSheet()->setCellValue( "D" .$baris, $row2->jumlah); 
              //                        $ST=$ST+$row2->jumlah;
              //                        $dtST=$dtST+$row2->jumlah;
                     
              //                   }
              //                   if($row2->id_level < 3 and ($row2->status_kerja=="KONTRAK" or $row2->status_kerja=="PERCOBAAN"))
              //                   {

              //                    //   $sheet->getActiveSheet()->setCellValue( "F" .$baris, $row2->jumlah);  
              //                       $dtSK=$dtSK+$row2->jumlah;  
              //                       $SK=$SK+$row2->jumlah; 

              //                   }
                               

              //                 }  
              //                    $sheet->getActiveSheet()->setCellValue( "C" .$baris, $dtMT);
              //                    $sheet->getActiveSheet()->setCellValue( "D" .$baris, $dtST);
              //                    $sheet->getActiveSheet()->setCellValue( "E" .$baris, $dtMK);
              //                    $sheet->getActiveSheet()->setCellValue( "F" .$baris, $dtSK);           

                               
                         
                            

              //        }
              //        $baris=$baris+1;
              //        //   $sheet->getActiveSheet()->setCellValue( "B" .$baris, "Total");
              //          $sheet->getActiveSheet()->setCellValue( "C" .$baris, $MT);
              //          $sheet->getActiveSheet()->setCellValue( "D" .$baris, $ST);
              //          $sheet->getActiveSheet()->setCellValue( "E" .$baris, $MK);
              //          $sheet->getActiveSheet()->setCellValue( "F" .$baris, $SK);












    $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($sheet);
    
    $new_name_file = 'rekapgajibaru.xlsx';
    $tofolder = public_path().'/'.$new_name_file;
    $writer->save( $tofolder );
    $filename   = str_replace("@", "/", $tofolder);
    # ---------------
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=".$new_name_file);
    header("Content-Type: application/xlsx");
    header("Content-Transfer-Encoding: binary");
    # ---------------
    require "$filename";
    # ---------------
    exit;


    }
    public function cetak(Request $request) {
        /* ----------
         Model
        ----------------------- */
        $qCabang             = new CabangModel;
        /* ----------
         Source
        ----------------------- */
        $qCabang  = $qCabang->getProfile($request->id_cabang)->first();
        if ($request->jenis_laporan=="1")
        {
		       	
        		 	 $name_file = 'rekapgaji.xls';
				    $fromfolder = public_path().'/'.$name_file;
				    $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( $fromfolder );
				    
     // $sheet->getActiveSheet("RAKAP")->setCellValue( "C2",$request->id_cabang );
				 $styleArray = [
			      'font' => [
			          'bold' => true,
			      ],
			      'alignment' => [
			          'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
			      ],
			      'borders' => [
			          'top' => [
			              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			          ],
			      ],
			      'fill' => [
			          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
			          'rotation' => 90,
			          'startColor' => [
			              'argb' => 'FFA0A0A0',
			          ],
			          'endColor' => [
			              'argb' => 'FFFFFFFF',
			          ],
			      ],
			  ];






                            
        // $spreadsheet = new Spreadsheet();
        // $sheet = $spreadsheet->getActiveSheet();
        // $spreadsheet->getActiveSheet()->getStyle('A3:AX3')->applyFromArray($styleArray);


                $query  = DB::table("p_penggajian as a")
				                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_level","f.status")
				                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
				                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
				                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
				                            ->leftjoin("m_level as e","e.id_level","=","d.id_level")
				                            ->leftjoin("p_ptkp as f","f.id_ptkp","=","a.id_ptkp")
				                            
				                            
				                            ->where("a.bulan",$request->periode)
				                            ->where("a.tahun",$request->tahun)
				                            ->where("a.id_cabang",$request->id_cabang)
                                    ->orderBy("a.id_cabang","dsc")
				                            ->orderBy("a.nik","asc")->get();
                            
                            
                    // $tgaji=$query->get();
                    //dd($query,$request->bulan,$request->tahun );
				     $CABANG=$query;
				    $sheet->getActiveSheet("GAJI")->setCellValue( "B2", $qCabang->nama_cabang);
   					 $sheet->getActiveSheet("GAJI")->setCellValue( "B4","BULAN " .$request->bulan ." " .$request->tahun );
             $kolom = "B";
            $baris = 10;
            $nomor = 1;
            $tgaji_pokok=0;
					  $tuang_makan=0;
				    $ttunj_kemahalan=0;
             $ttunj_transport=0;
				    $ttunj_khusus=0;
					  $tbpjs_kes_prs=0;
					$ttunj_pph21=0;
					$tbpjs_kes_prs=0;
					$tjht_kry=0;
					$tsimpanan_wajib=0;
					$tpinjaman_koperasi=0;
					$tpph21=0;
					$thp= 0;
          $tbonus=0;
          $tpesangon=0;
          $tkoreksiplus=0;
          $tkoreksimin=0;
          $tinsentif=0;
          $tthr=0;
          $tbpjs_kes_prs=0;
          $ttotal_tunjangan=0;
          $ttotal_gaji1=0;
          $ttotal_gaji2=0;
          $ttotal_gaji3=0;
          $ttotpotongan1=0;
          $ttotpotongan2=0;
          $ttotpotongan3=0;
          $totpotongan1=0;
          $totpotongan2=0;
          $tthp=0;
          
          foreach($query as $row) 

                     {
                          
                        
                            
                                  $sheet->getActiveSheet("GAJI")->setCellValue('B'.$baris,$baris-9);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('C'.$baris,$row->nik);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('D'.$baris,$row->nama_karyawan);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('E'.$baris,$row->no_rekening);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('F'.$baris,"BCA");
                                  $sheet->getActiveSheet("GAJI")->setCellValue('G'.$baris,$row->nonpwp);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('H'.$baris, $row->status);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('I'.$baris,$row->tgl_masuk);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('J'.$baris,$row->nama_jabatan);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('K'.$baris,$row->nama_jabatan);
                                  $sheet->getActiveSheet("GAJI")->setCellValue('L'.$baris,$row->rate_bpjstk);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('M'.$baris,$row->gaji_pokok);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('N'.$baris,$row->tunj_makan);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('O'.$baris,$row->jml_hadir);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('P'.$baris,$row->uang_makan);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('Q'.$baris,$row->tunj_kemahalan);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('R'.$baris,$row->tunj_khusus);//medical
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('S'.$baris,$row->bpjs_kes_prs);//bpjs kesehatan
                                  $total_tunjangan= $row->uang_makan+$row->tunj_khusus+$row->tunj_kemahalan+$row->bpjs_kes_prs+$row->tunj_transport;
                                  $ttotal_tunjangan=$ttotal_tunjangan+ $total_tunjangan;
                                  
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('T'.$baris,$row->tunj_transport);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('U'.$baris,$total_tunjangan);
                                   $sheet->getActiveSheet("GAJI")->setCellvalue('V'.$baris,$row->koreksiplus);
                                  $total_gaji1= $row->gaji_pokok+$total_tunjangan+$row->insentif+$row->koreksiplus;
                                  $ttotal_gaji1=$ttotal_gaji1+$total_gaji1;
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('W'.$baris,$total_gaji1);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('X'.$baris,$row->thr);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('Y'.$baris,$row->bonus);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('Z'.$baris,$row->pesangon);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AA'.$baris,$row->tunj_pph21);
                                  
                                  $total_gaji2=$total_gaji1+$row->thr+$row->bonus+$row->pesangon+$row->tunj_pph21;
                                  $ttotal_gaji2=$ttotal_gaji2+$total_gaji2;
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AB'.$baris,$total_gaji2);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AC'.$baris,$row->bpjs_kes_prs);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AD'.$baris,$row->jht_kry);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AE'.$baris,$row->simpanan_wajib);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AF'.$baris,0);
                                  $totkoperasi = $row->simpanan_wajib+$row->pinjaman_koperasi;
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AG'.$baris,$row->pinjaman_koperasi);
                                  $totpotongan1 = $totkoperasi+$row->bpjs_kes_prs+$row->jht_kry;
                                  $ttotpotongan1=$ttotpotongan1+$totpotongan1;
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AH'.$baris, $totpotongan1);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AI'.$baris, $row->koreksimin);
                                  
                                   $totpotongan2 =  $totpotongan1+$row->koreksimin;
                                  $ttotpotongan2=$ttotpotongan2+$totpotongan2;
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AJ'.$baris, $totpotongan2);
                                  
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('AK'.$baris, $row->pph21);
                                  $thp= $total_gaji2-($totpotongan2+$row->pph21);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('Al'.$baris,$thp);
                                 $tgaji_pokok=$tgaji_pokok+$row->gaji_pokok;
                                 $tuang_makan=$tuang_makan+$row->uang_makan;
                                  $ttunj_kemahalan=$ttunj_kemahalan+$row->tunj_kemahalan;
                                  $ttunj_transport=$ttunj_transport+$row->tunj_transport ; 
                                  $ttunj_khusus=$ttunj_khusus+ $row->tunj_khusus;
                                  $ttunj_pph21=$ttunj_pph21+$row->tunj_pph21;
                                 $tbonus=$tbonus+$row->bonus;
                                 $tpesangon=$tpesangon+$row->pesangon;
                                 $tkoreksiplus=$tkoreksiplus+$row->koreksiplus;
                                 $tkoreksimin=$tkoreksimin+$row->koreksimin;
                                 $tinsentif=$tinsentif+$row->insentif;
                                 $tbpjs_kes_prs=$tbpjs_kes_prs+ $row->bpjs_kes_prs;
                                 $tjht_kry=$tjht_kry+$row->jht_kry;
                                 $tsimpanan_wajib=$tsimpanan_wajib+$row->simpanan_wajib;
                                 $tpinjaman_koperasi=$tpinjaman_koperasi+$row->pinjaman_koperasi;
                                 $tpph21=$tpph21+$row->pph21;

                                 $tthp=  $tthp+ $thp;

                                 $baris=$baris+1;
                                   }
                            $baris=$baris+1;
                                     $sheet->getActiveSheet("GAJI")->setCellvalue('M'.$baris,$tgaji_pokok);
                                  $sheet->getActiveSheet("GAJI")->setCellvalue('P'.$baris,$tuang_makan);
                          $sheet->getActiveSheet("GAJI")->setCellvalue('Q'.$baris,$ttunj_kemahalan);
                          $sheet->getActiveSheet("GAJI")->setCellvalue('R'.$baris,$ttunj_khusus);
                           $sheet->getActiveSheet("GAJI")->setCellvalue('S'.$baris,$tbpjs_kes_prs);
                          $sheet->getActiveSheet("GAJI")->setCellvalue('T'.$baris,$ttunj_transport );
                          $sheet->getActiveSheet("GAJI")->setCellvalue('U'.$baris,$ttotal_tunjangan);
                          $sheet->getActiveSheet("GAJI")->setCellvalue('V'.$baris,$tkoreksiplus);
                           $sheet->getActiveSheet("GAJI")->setCellvalue('W'.$baris,$ttotal_gaji1);
                           $sheet->getActiveSheet("GAJI")->setCellvalue('X'.$baris,$tthr);
                           $sheet->getActiveSheet("GAJI")->setCellvalue('Y'.$baris,$tbonus);
                           $sheet->getActiveSheet("GAJI")->setCellvalue('Z'.$baris,$tpesangon);
                           
                        $sheet->getActiveSheet("GAJI")->setCellvalue('AA'.$baris,$ttunj_pph21);
                        $sheet->getActiveSheet("GAJI")->setCellvalue('AB'.$baris,$ttotal_gaji2);
                        $sheet->getActiveSheet("GAJI")->setCellvalue('AC'.$baris,$tbpjs_kes_prs);
                        $sheet->getActiveSheet("GAJI")->setCellvalue('AB'.$baris,$ttotal_gaji2);
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AE'.$baris,$tsimpanan_wajib);
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AF'.$baris,0);
                         $totkoperasi = $tsimpanan_wajib+$tpinjaman_koperasi;
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AG'.$baris,$tpinjaman_koperasi);
                         $totpotongan = $totkoperasi+$tbpjs_kes_prs+$tjht_kry;
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AH'.$baris, $ttotpotongan1);
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AI'.$baris, $tkoreksimin);
                         $totpotongan = $totpotongan+$tkoreksimin;
                         
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AJ'.$baris, $ttotpotongan2);
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AK'.$baris, $tpph21);
                         
                         $sheet->getActiveSheet("GAJI")->setCellvalue('AL'.$baris,$tthp);



             $sheet->getActiveSheet("GAJI")->getStyle('B10:AL'.$baris)->getNumberFormat()->setFormatCode('#,##0');
         $sheet->getActiveSheet("GAJI")->getStyle('B10:AH'.$baris)->applyFromArray($styleArray);








			    $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($sheet);
			    
			    $new_name_file = 'rekapgajibaru.xlsx';
			    $tofolder = public_path().'/'.$new_name_file;
			    $writer->save( $tofolder );
			    $filename   = str_replace("@", "/", $tofolder);
			    # ---------------
			    header("Cache-Control: public");
			    header("Content-Description: File Transfer");
			    header("Content-Disposition: attachment; filename=".$new_name_file);
			    header("Content-Type: application/xlsx");
			    header("Content-Transfer-Encoding: binary");
			    # ---------------
			    require "$filename";
			    # ---------------
			    exit;

        }
        else if ($request->jenis_laporan=="3")
        {

                       $qUpdategaji            = new UpdategajiModel;
				        $qperiode               = $qUpdategaji->getPeriode()->first();
				        $bulan                  = getMonthName($qperiode->bulanpayroll, "id");
				        $tahun                  = $qperiode->tahunpayroll;
				       
				        $mKaryawan              = new KaryawanModel;
				        


				        $query  = DB::table("p_penggajian as a")
				                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan","e.nama_level")
				                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
				                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
				                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
				                            ->leftjoin("m_level as e","e.id_level","=","d.id_level")
				                            
				                            ->where("a.bulan",$qperiode->bulanpayroll)
				                            ->where("a.tahun",$tahun)
				                            ->orderBy("a.id_cabang","a.nik", "ASC")->get();

				        
				  $styleArray = [
				      'font' => [
				          'bold' => true,
				      ],
				      'alignment' => [
				          'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				      ],
				      'borders' => [
				          'top' => [
				              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				          ],
				      ],
				      'fill' => [
				          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				          'rotation' => 90,
				          'startColor' => [
				              'argb' => 'FFA0A0A0',
				          ],
				          'endColor' => [
				              'argb' => 'FFFFFFFF',
				          ],
				      ],
				  ];






				                            
				        $spreadsheet = new Spreadsheet();
				        $sheet = $spreadsheet->getActiveSheet();
				        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
				       
				      //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
				  	        
				        $baris = 2;
				        /*-------------------------
				        KOLOM DATA
				        ---------------------------*/
				          
				        $sheet->setCellValue('A1', 'Acc No');    
				        $sheet->setCellValue('B1', 'Trans.Amount');
				        $sheet->setCellValue('C1', 'Emp.Number');
				        $sheet->setCellValue('D1', 'emp.Name');
				        $sheet->setCellValue('E1', 'Ket.');
				       
				        $gaji_pokok=0;
				        $tunj_jabatan=0;
				        $tunj_transport=0;
				        $tunj_khusus=0;
				        $tunj_jaga=0;
				        $tunj_kemahalan=0;
				        $tunj_makan=0;
				        $uang_makan=0;
				        $insentif=0;
				        $koreksiplus=0;
				        $tunj_pph21=0;
				        $totpendapatan=0;
				        $koreksimin=0;
				        $simpanan_wajib=0;
				        $potongan_absen=0;
				        $pinjaman_koperasi=0;
				        $pph21=0;
				        $totpotongan=0;
				        $totthp=0;

				        foreach($query as $row) 
				        {
				                  $pendapatan=$row->gaji_pokok+$row->tunj_jabatan+$row->tunj_transport+$row->tunj_jaga+$row->tunj_kemahalan+$row->tunj_makan+$row->uang_makan+$row->insentif+$row->koreksiplus+$row->tunj_pph21;
				                  $potongan=$row->koreksimin+$row->simpanan_wajib+$row->potongan_absen+$row->pinjaman_koperasi+$row->pph21;
				                  $thp=$pendapatan-$potongan;
				                  $statuspajak            = $qUpdategaji->getStatuspajak($row->id_ptkp);
				                  $qKaryawan              = $mKaryawan->getProfile($row->id_karyawan);
				                  $sheet->setCellValue('A'.$baris,$row->no_rekening);
				                  $sheet->setCellvalue('B'.$baris,$thp);
				                  $sheet->setCellValue('C'.$baris,$row->nik);
								  $sheet->setCellValue('D'.$baris,$row->nama_karyawan);
				                  $sheet->setCellValue('E'.$baris,$row->nama_departemen);
				                  $gaji_pokok     = $gaji_pokok+$row->gaji_pokok;
				                  $tunj_jabatan    = $tunj_jabatan+$row->tunj_jabatan;
				                  $tunj_transport  = $tunj_transport+$row->tunj_transport;
				                  $tunj_khusus     = $tunj_khusus+$row->tunj_khusus;
				                  $tunj_jaga       = $tunj_jaga+$row->tunj_jaga;
				                  $tunj_kemahalan  = $tunj_kemahalan+$row->tunj_kemahalan;
				                  $tunj_makan      = $tunj_makan+$row->tunj_makan;
				                  $uang_makan      = $uang_makan+$row->uang_makan;
				                  $insentif        = $insentif+$row->insentif;
				                  $koreksiplus     = $koreksiplus+$row->koreksiplus;
				                  $tunj_pph21      = $tunj_pph21+$row->tunj_pph21;
				                  $totpendapatan   = $totpendapatan+$pendapatan;
				                  $koreksimin      = $koreksimin+$row->koreksimin;
				                  $simpanan_wajib  = $simpanan_wajib+$row->simpanan_wajib;
				                  $potongan_absen  = $potongan_absen+$row->potongan_absen;
				                  $pinjaman_koperasi = $pinjaman_koperasi+$row->pinjaman_koperasi;
				                  $pph21           = $pph21+$row->pph21;
				                  $totpotongan     = $totpotongan+$potongan;
				                  $totthp          = $totthp+$thp;



				                  //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
				                  


				                 $baris=$baris+1;
				        }
				                  $sheet->setCellValue('A'.$baris,"GRAND TOTAL");
				                  $sheet->setCellvalue('B'.$baris,$totthp);

				         $spreadsheet->getActiveSheet()->getStyle('B2:B'.$baris)->getNumberFormat()->setFormatCode('#.00');
				          $spreadsheet->getActiveSheet()->getStyle('A'.$baris.':E'.$baris)->applyFromArray($styleArray);
				          //dd($spreadsheet);


				        //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
				        foreach(range('A','E') as $columnID)
				         {
				             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
				                  ->setAutoSize(true);
				             $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
				                  ->setAutoSize(true);     
				                  


				          }

				        
				        
				        



				           $spreadsheet->getActiveSheet()->getColumnDimension("AA")
				                  ->setAutoSize(true);
				        $writer = new Xlsx($spreadsheet);
				        // $writer->save('data karyawanlengkap.xlsx');
				        $judul    = "data gaji transfer".$bulan.$tahun;  
				        $name_file = $judul.'.xlsx';
				      // $path = storage_path('Laporan\\'.$name_file);
				      $path = public_path().'/app/'.$name_file;
				      $contents = is_dir($path);
				      // $headers = array('Content-Type' => File::mimeType($path));
				      // dd($path.'/'.$name_file,$contents);
				      $writer->save($path);
				      $filename   = str_replace("@", "/", $path);
				        # ---------------
				        header("Cache-Control: public");
				        header("Content-Description: File Transfer");
				        header("Content-Disposition: attachment; filename=".$name_file);
				        header("Content-Type: application/xlsx");
				        header("Content-Transfer-Encoding: binary");
				        # ---------------
				        require "$filename";
				        # ---------------
				        exit;
        }
       
        
    }
}
