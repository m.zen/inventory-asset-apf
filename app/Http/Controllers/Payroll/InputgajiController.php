<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\InputgajiModel;
use App\Model\Master\MasterModel;
class InputgajiController extends Controller
{
      // protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/inputgaji/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qInputgaji                  = new InputgajiModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
						,"add-style"=>""),
                                            array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),             

                                    array("label"=>"Gaji Pokok"
                                                ,"name"=>"gaji_pokok"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Tunj. Jabatan"
                                                ,"name"=>"tunj_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                      
                                        array("label"=>"Tunj. Tranport"
                                                ,"name"=>"tunj_transport"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                        array("label"=>"Uang Makan/hari"
                                                ,"name"=>"tunj_makan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                         array("label"=>"Tunj. Kemahalan"
                                                ,"name"=>"tunj_kemahalan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
										);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_INPUTGAJI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_INPUTGAJI");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_INPUTGAJI");
        }
        # ---------------
        $data["select"]        = $qInputgaji->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qInputgaji->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    

    public function edit($id) {
        $data["title"]        = "Detail Gaji";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/inputgaji/update";
        /* ----------
         Inputgaji
        ----------------------- */
        $qMaster              = new MasterModel;
        $qMInputgaji           = new InputgajiModel;

        /* ----------
         Source
        ----------------------- */
        $qInputgaji           = $qMInputgaji->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();
        $qPtkp				  = $qMaster->getSelectPtkp();
        $qBank                = $qMaster->getSelectBank();
        $qGrading             = $qMaster->getSelectGrading();
        //$qTnjjabatan          = $qMInputgaji->getTnjjabatan($qInputgaji->id_level)->first();					

        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"NIK", "readonly"=>"readonly", "mandatory"=>"", "value"=>$qInputgaji->nik));
        $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama","readonly"=>"readonly",  "mandatory"=>"", "value"=>$qInputgaji->nama_karyawan));
        $data["fields"][]      = form_text(array("name"=>"nama_jabatan", "label"=>"Nama Jabatan","readonly"=>"readonly",  "mandatory"=>"", "value"=>$qInputgaji->nama_jabatan));
        $data["fields"][]      = form_text3(array("name"=>"", "label"=>"","readonly"=>"readonly",  "mandatory"=>"", "value"=>"Komponen Gaji"));
        
        $data["fields"][]      = form_text(array("name"=>"nonpwp", "label"=>"No NPWP",  "mandatory"=>"", "value"=>$qInputgaji->nonpwp));
        $data["fields"][]      = form_select(array("name"=>"id_ptkp", "label"=>"PTKP Status", "mandatory"=>"yes", "source"=>$qPtkp, "value"=>$qInputgaji->id_ptkp));
        $data["fields"][]      = form_text(array("name"=>"nobpjskes", "label"=>"No BPJS Kesehatan",  "mandatory"=>"", "value"=>$qInputgaji->nobpjskes));
        $data["fields"][]      = form_text(array("name"=>"nobjstk", "label"=>"No BPJS Tenanaga Kerja",  "mandatory"=>"", "value"=>$qInputgaji->nobjstk));
        $data["fields"][]      = form_select(array("name"=>"id_bank", "label"=>"Bank", "mandatory"=>"yes", "source"=> $qBank, "value"=>$qInputgaji->id_bank));
        $data["fields"][]      = form_text(array("name"=>"no_rekening", "label"=>"No Rekening",  "mandatory"=>"", "value"=>$qInputgaji->no_rekening));
        $data["fields"][]      = form_text(array("name"=>"asrekening", "label"=>"Atas Nama Rekening",  "mandatory"=>"", "value"=>$qInputgaji->anrekening));
      
        $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>"", "value"=>$qInputgaji->noabsen));
        $data["fields"][]      = form_select(array("name"=>"id_grading", "label"=>"Grading", "mandatory"=>"", "source"=> $qGrading, "value"=>$qInputgaji->id_grading));
         

        $data["fields"][]      = form_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok",  "mandatory"=>"yes", "value"=>number_format($qInputgaji->gaji_pokok)),0);
        $data["fields"][]      = form_currency(array("name"=>"rate_bpjstk", "label"=>"Rate BPJS TK",  "mandatory"=>"", "value"=>number_format($qInputgaji->rate_bpjstk)),0);
        $data["fields"][]      = form_currency(array("name"=>"rate_bpjskes", "label"=>"Rate BPJS kes",  "mandatory"=>"", "value"=>number_format($qInputgaji->rate_bpjskes)),0);
       
        $data["fields"][]      = form_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan", "value"=>number_format($qInputgaji->tunj_jabatan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport", "value"=>number_format($qInputgaji->tunj_transport,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga",  "value"=>number_format($qInputgaji->tunj_jaga,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan",  "value"=>number_format($qInputgaji->tunj_kemahalan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_makan", "label"=>"Uang Makan/hari",  "value"=>number_format($qInputgaji->tunj_makan,0)));
        $data["fields"][]      = form_currency(array("name"=>"plafon", "label"=>"Medical",  "value"=>number_format($qInputgaji->plafon,0)));
          
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'gaji_pokok' => 'required|'                 
        );

        $messages = [
                    'gaji_pokok' => 'Inputgaji harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/inputgaji/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qInputgaji      = new InputgajiModel;
            # ---------------
            $qInputgaji->updateData($request);
            # ---------------
            session()->flash("success_message", "Inputgaji has been updated");
            # ---------------
            return redirect("/inputgaji/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Inputgaji";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/inputgaji/remove";
        /* ----------
         Source
        ----------------------- */
        $qInputgaji     = new InputgajiModel;
         $qInputgaji                 = $qInputgaji->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_inputgaji", "label"=>"Inputgaji ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_inputgaji", "label"=>"Inputgaji", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qInputgaji->nama_inputgaji));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qInputgaji     = new InputgajiModel;
            # ---------------
            $qInputgaji->removeData($request);
            # ---------------
            session()->flash("success_message", "Inputgaji has been removed");
        } else {
            session()->flash("error_message", "Inputgaji cannot be removed");
        }
      # ---------------
        return redirect("/inputgaji/index"); 
    }


}
