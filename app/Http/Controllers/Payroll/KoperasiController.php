<?php

namespace App\Http\Controllers\Payroll;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\KoperasiModel;
use App\Model\Master\MasterModel;
class KoperasiController extends Controller
{
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/koperasi/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKoperasi                  = new KoperasiModel;

        $data["tabs"]          = array(array("label"=>"Anggota Koperasi", "url"=>"/koperasi/index", "active"=>"active")
                           ,array("label"=>"Pinjaman Koperasi", "url"=>"/pinjaman/index", "active"=>""));
      
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_koperasi"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tgl Daftar"
                                                ,"name"=>"tgl_daftar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Simpanan Wajib"
                                                ,"name"=>"simpanan_wajib"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"number"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>"")
                                    );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KOPERASI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KOPERASI");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KOPERASI");
        }
        # ---------------
        $data["select"]        = $qKoperasi->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKoperasi->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.tablist", $data);
    }

    public function add() {
        $data["title"]         = "Add Koperasi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/koperasi/save";
        /* ----------
         Koperasi
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
        //$qKaryawan             = $qMaster->getSelectKaryawan();
        $qKaryawan             = $qMaster->getSelectKopKaryawan();
        //dd($qKaryawan);
        
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_daftar", "label"=>"Tanggal Daftar", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
       $data["fields"][]      = form_currency(array("name"=>"simpanan_wajib", "label"=>"Simpanan Wajib", "value"=>number_format(0,0)));
    	$data["fields"][]      = form_hidden(array("name"=>"aktif", "label"=>"Kode", "mandatory"=>"", "value"=>"1"));
       
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'id_karyawan' => 'required'                     );

        $messages = ['id_karyawan.required' => 'Karyawan  harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/koperasi/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKoperasi  = new KoperasiModel;
            # ---------------
            $qKoperasi->createData($request);
            # ---------------
            session()->flash("success_message", "Koperasi has been saved");
            # ---------------
            return redirect("/koperasi/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Koperasi";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/koperasi/update";
        /* ----------
         Koperasi
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKoperasi             = new KoperasiModel;
        /* ----------
         Source
        ----------------------- */
        $qKoperasi             = $qKoperasi->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qKaryawan             = $qMaster->getSelectKaryawan();

        /* ----------
         Fields
        ----------------------- */
        
        $data["fields"][]      = form_hidden(array("name"=>"id_koperasi", "label"=>"Koperasi ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan,"value"=>$qKoperasi->id_karyawan));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_daftar", "label"=>"Tanggal Daftar", "mandatory"=>"yes","value"=>displayDMY($qKoperasi->tgl_daftar,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_currency(array("name"=>"simpanan_wajib", "label"=>"Simpanan Wajib", "value"=>number_format($qKoperasi->simpanan_wajib,0)));
       $data["fields"][]      = form_hidden(array("name"=>"aktif", "label"=>"Kode", "mandatory"=>"", "value"=>"1"));
            
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'id_karyawan' => 'required|' ,
                    'tgl_daftar' => 'required|'             
        );

        $messages = [
                    'id_karyawan.required' => 'Karyawan harus diisi',
                    'tgl_daftar.required'=>'Tanggal Daftar harus isi'

        ];

 

        $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/koperasi/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKoperasi      = new KoperasiModel;
            # ---------------
            $qKoperasi->updateData($request);
            # ---------------
            session()->flash("success_message", "Koperasi has been updated");
            # ---------------
            return redirect("/koperasi/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Koperasi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/koperasi/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKoperasi             = new KoperasiModel;
        $qKoperasi             = $qKoperasi->getProfile($id)->first();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_koperasi", "label"=>"Koperasi ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan));
          
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qKoperasi     = new KoperasiModel;
            # ---------------
            $qKoperasi->removeData($request);
            # ---------------
            session()->flash("success_message", "Koperasi has been removed");
        } else {
            session()->flash("error_message", "Koperasi cannot be removed");
        }
      # ---------------
        return redirect("/koperasi/index"); 
    }
}
