<?php

namespace App\Http\Controllers\Training;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Training\ProvidertrainingModel;
use App\Model\Master\MasterModel;
class ProvidertrainingController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/providertraining/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qProvidertraining                  = new ProvidertrainingModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_providertraining"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"Nama Provider"
                                                ,"name"=>"nama_providertraining"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"No Telpon"
                                                ,"name"=>"telp_providertraining"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
        							array("label"=>"No Fax"
                                                ,"name"=>"fax_providertraining"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
        							array("label"=>"Email"
                                                ,"name"=>"email_providertraining"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),

        							array("label"=>"Alamat"
                                                ,"name"=>"alamat_providertraining"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>"")	

        							);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PROVIDERTRAINING" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PROVIDERTRAINING");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PROVIDERTRAINING");
        }
        # ---------------
        $data["select"]        = $qProvidertraining->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qProvidertraining->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Providertraining";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/providertraining/save";
        /* ----------
         Providertraining
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
       // $qStatus			   = getSelectStatusProvidertraining();
        $qKtgtraining		  = getKatgtraining();	
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
	   $data["fields"][]      = form_text(array("name"=>"nama_providertraining", "label"=>"Nama Provider", "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"alamat_providertraining", "label"=>"Alamat", "mandatory"=>"yes"));
	   $data["fields"][]      = form_text(array("name"=>"telp_providertraining", "label"=>"Telpon", "mandatory"=>"yes"));
	   $data["fields"][]      = form_text(array("name"=>"fax_providertraining", "label"=>"Fax", "mandatory"=>"yes"));
	   $data["fields"][]      = form_text(array("name"=>"email_providertraining", "label"=>"Email", "mandatory"=>"yes"));
	  



       //$data["fields"][]      = form_select(array("name"=>"kategori_providertraining", "label"=>"Level", "mandatory"=>"yes", "source"=>$qKtgtraining));
           
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_providertraining' => 'required'                     );

        $messages = ['nama_providertraining.required' => 'Providertraining harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/Providertraining/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qProvidertraining  = new ProvidertrainingModel;
            # ---------------
            $qProvidertraining->createData($request);
            # ---------------
            session()->flash("success_message", "Providertraining has been saved");
            # ---------------
            return redirect("/providertraining/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Providertraining";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/providertraining/update";
        /* ----------
         Providertraining
        ----------------------- */
        $qMaster             = new MasterModel;
        $qProvidertraining   = new ProvidertrainingModel;
        /* ----------
         Source
        ----------------------- */
        $qProvidertraining    = $qProvidertraining->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_providertraining", "label"=>"Providertraining ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nama_providertraining", "label"=>"Nama Provider",  "mandatory"=>"yes", "value"=>$qProvidertraining->nama_providertraining));
        $data["fields"][]      = form_text(array("name"=>"alamat_providertraining", "label"=>"Alamat", "mandatory"=>"yes", "value"=>$qProvidertraining->alamat_providertraining));
	    $data["fields"][]      = form_text(array("name"=>"telp_providertraining", "label"=>"Telpon", "mandatory"=>"yes", "value"=>$qProvidertraining->telp_providertraining));
	    $data["fields"][]      = form_text(array("name"=>"fax_providertraining", "label"=>"Fax", "mandatory"=>"yes" ,"value"=>$qProvidertraining->fax_providertraining));
	    $data["fields"][]      = form_text(array("name"=>"email_providertraining", "label"=>"Email", "mandatory"=>"yes" ,"value"=>$qProvidertraining->email_providertraining));
	  	        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_providertraining' => 'required|'               
        );

        $messages = [
                    'nama_providertraining.required' => 'Providertraining harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/providertraining/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qProvidertraining      = new ProvidertrainingModel;
            # ---------------
            $qProvidertraining->updateData($request);
            # ---------------
            session()->flash("success_message", "Providertraining has been updated");
            # ---------------
            return redirect("/providertraining/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Providertraining";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/providertraining/remove";
        /* ----------
         Source
        ----------------------- */
        $qProvidertraining     = new ProvidertrainingModel;
         $qProvidertraining                 = $qProvidertraining->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_providertraining", "label"=>"Providertraining ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_providertraining", "label"=>"Providertraining", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qProvidertraining->nama_providertraining));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qProvidertraining     = new ProvidertrainingModel;
            # ---------------
            $qProvidertraining->removeData($request);
            # ---------------
            session()->flash("success_message", "Providertraining has been removed");
        } else {
            session()->flash("error_message", "Providertraining cannot be removed");
        }
      # ---------------
        return redirect("/providertraining/index"); 
    }
}
