<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use File;
use App\User;
use App\Model\MenuModel;
use App\Model\Laporan\LaporanTransferModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class LaporanTransferController extends Controller
{
    public function __construct(Request $request)
    {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/laporan_transfer/proses";
        /* ----------
         Fields
        ----------------------- */
        // $data["fields"][]       = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_transfer_awal", "label"=>"Tanggal Transfer Awal", "mandatory"=>"yes","value"=>"", "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_transfer_akhir", "label"=>"Tanggal Transfer Akhir", "mandatory"=>"yes","value"=>"", "first_selected"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }
   
    public function proses(Request $request)
    {
        $rules = array(
                    'tgl_transfer_awal' => 'required|',
                    'tgl_transfer_akhir' => 'required|',
        );

        $messages = [
                    'tgl_transfer_awal.required' => 'Tanggal transfer awal harus diisi',
                    'tgl_transfer_akhir.required' => 'Tanggal transfer akhir harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/laporan_transfer/index" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qData  = new LaporanTransferModel;
            # ---------------
            $query  = $qData->getDataLaporan($request);
            
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];
    
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
            $baris = 2;
            /*-------------------------
            KOLOM DATA
            ---------------------------*/
            $sheet->setCellValue('A1', 'No.');
            $sheet->setCellValue('B1', 'Tanggal Transfer');
            $sheet->setCellValue('C1', 'Nama Barang');
            $sheet->setCellValue('D1', 'No Asset');
            $sheet->setCellValue('E1', 'Cabang Asal');
            $sheet->setCellValue('F1', 'Departemen Asal');
            $sheet->setCellValue('G1', 'PIC/Lokasi Asal');
            $sheet->setCellValue('H1', 'Cabang Tujuan');
            $sheet->setCellValue('I1', 'Departemen Tujuan');
            $sheet->setCellValue('J1', 'Lokasi/PIC Tujuan');
            $sheet->setCellValue('K1', 'Keterangan');

            foreach($query as $row) 
            {
                $sheet->setCellValue('A'.$baris, $baris-1);
                $sheet->setCellValue('B'.$baris, $row->tgl_transfer);
                $sheet->setCellValue('C'.$baris, $row->nama_barang);
                $sheet->setCellValue('D'.$baris, $row->no_asset);
                $sheet->setCellValue('E'.$baris, $row->dari_nama_cabang);
                $sheet->setCellValue('F'.$baris, $row->dari_nama_departemen);
                $sheet->setCellValue('G'.$baris, $row->dari_nama_lokasi);
                $sheet->setCellValue('H'.$baris, $row->ke_nama_cabang);
                $sheet->setCellValue('I'.$baris, $row->ke_nama_departemen);
                $sheet->setCellValue('J'.$baris, $row->ke_nama_lokasi);
                $sheet->setCellValue('K'.$baris, $row->keterangan);
    
                $baris=$baris+1;
            }
    
            foreach(range('B','L') as $columnID)
            {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
                
                    
            }
            $spreadsheet->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);

            $writer = new Xlsx($spreadsheet);
            $judul    = "Laporan Transfer Asset";  
            $name_file = $judul.'.xlsx';
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'. $name_file .'"'); 
            header('Cache-Control: max-age=0');
            
            $writer->save('php://output');
        }
    }
}
