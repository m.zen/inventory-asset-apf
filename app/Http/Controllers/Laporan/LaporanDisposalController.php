<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use File;
use App\User;
use App\Model\MenuModel;
use App\Model\Laporan\LaporanDisposalModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class LaporanDisposalController extends Controller
{
    public function __construct(Request $request)
    {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/laporan_disposal/proses";
        /* ----------
         Fields
        ----------------------- */
        // $data["fields"][]       = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_disposal_awal", "label"=>"Tanggal Disposal Awal", "mandatory"=>"yes","value"=>"", "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_disposal_akhir", "label"=>"Tanggal Disposal Akhir", "mandatory"=>"yes","value"=>"", "first_selected"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }
   
    public function proses(Request $request)
    {
        $rules = array(
                    'tgl_disposal_awal' => 'required|',
                    'tgl_disposal_akhir' => 'required|',
        );

        $messages = [
                    'tgl_disposal_awal.required' => 'Tanggal disposal awal harus diisi',
                    'tgl_disposal_akhir.required' => 'Tanggal disposal akhir harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/laporan_disposal/index" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qData  = new LaporanDisposalModel;
            # ---------------
            $query  = $qData->getDataLaporan($request);

            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];
    
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
            $baris = 2;
            /*-------------------------
            KOLOM DATA
            ---------------------------*/
            $sheet->setCellValue('A1', 'No.');
            $sheet->setCellValue('B1', 'Tanggal Disposal');
            $sheet->setCellValue('C1', 'Nama Barang');
            $sheet->setCellValue('D1', 'No Asset');
            $sheet->setCellValue('E1', 'Cabang Asal');
            $sheet->setCellValue('F1', 'Departemen Asal');
            $sheet->setCellValue('G1', 'PIC/Lokasi Asal');
            $sheet->setCellValue('H1', 'Nama Disposal');
            $sheet->setCellValue('I1', 'Keterangan');

            foreach($query as $row) 
            {
                $sheet->setCellValue('A'.$baris, $baris-1);
                $sheet->setCellValue('B'.$baris, $row->tgl_hapus);
                $sheet->setCellValue('C'.$baris, $row->nama_barang);
                $sheet->setCellValue('D'.$baris, $row->no_asset);
                $sheet->setCellValue('E'.$baris, $row->nama_cabang);
                $sheet->setCellValue('F'.$baris, $row->nama_departemen);
                $sheet->setCellValue('G'.$baris, $row->nama_lokasi);
                $sheet->setCellValue('H'.$baris, $row->nama_disposal);
                $sheet->setCellValue('I'.$baris, $row->keterangan);
    
                $baris=$baris+1;
            }
    
            foreach(range('B','I') as $columnID)
            {
                $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
                
                    
            }
            $spreadsheet->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);

            $writer = new Xlsx($spreadsheet);
            $judul    = "Laporan Disposal Asset";  
            $name_file = $judul.'.xlsx';
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'. $name_file .'"'); 
            header('Cache-Control: max-age=0');
            
            $writer->save('php://output');
        }
    }
}
