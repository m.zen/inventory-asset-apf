<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\ProfilesystemModel;
use App\Model\Master\MasterModel;
class ProfilesystemController extends Controller
{
	  protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/profilesystem/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qProfilesystem                  = new ProfilesystemModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_profilesystem"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"Nama Perusahaan"
                                                ,"name"=>"nama_perusahaan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""));
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_CABANG" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_CABANG");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_CABANG");
        }
        # ---------------
        $data["select"]        = $qProfilesystem->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qProfilesystem->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    

    public function add() {
        $data["title"]         = "Add Profilesystem";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/profilesystem/save";
        /* ----------
         Profilesystem
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
       // $qStatus			   = getSelectStatusProfilesystem();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
	   $data["fields"][]      = form_text(array("name"=>"nama_profilesystem", "label"=>"Profilesystem", "mandatory"=>"yes"));
              
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_profilesystem' => 'required'                     );

        $messages = ['nama_profilesystem.required' => 'Profilesystem harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/Profilesystem/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qProfilesystem  = new ProfilesystemModel;
            # ---------------
            $qProfilesystem->createData($request);
            # ---------------
            session()->flash("success_message", "Profilesystem has been saved");
            # ---------------
            return redirect("/profilesystem/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Profilesystem";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/profilesystem/update";
        /* ----------
         Profilesystem
        ----------------------- */
        $qMaster              = new MasterModel;
        $qProfilesystem             = new ProfilesystemModel;
        /* ----------
         Source
        ----------------------- */
        $qProfilesystem       = $qProfilesystem->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        /* ----------
         Fields
        ----------------------- */
    
 		$data["fields"][]      = form_hidden(array("name"=>"id_profilesystem", "label"=>"Profilesystem ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nama_perusahaan", "label"=>"Nama Perusahaan",  "mandatory"=>"yes", "value"=>$qProfilesystem->nama_perusahaan));
        $data["fields"][]      = form_text(array("name"=>"alamat_perusahaan", "label"=>"Alamat",  "mandatory"=>"yes", "value"=>$qProfilesystem->alamat_perusahaan));
        $data["fields"][]      = form_text(array("name"=>"bulanpayroll", "label"=>"Bulan Payroll","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qProfilesystem->bulanpayroll));
        $data["fields"][]      = form_text(array("name"=>"tahunpayroll", "label"=>"Tahun Payroll","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qProfilesystem->tahunpayroll));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_absenawal", "label"=>"Tanggal Awal Absensi", "mandatory"=>"yes","value"=>displayDMY($qProfilesystem->tgl_absenawal,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_absenakhir", "label"=>"Tanggal Akhir Absensi", "mandatory"=>"yes","value"=>displayDMY($qProfilesystem->tgl_absenakhir,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_payrollawal", "label"=>"Tanggal Awal Payroll", "mandatory"=>"yes","value"=>displayDMY($qProfilesystem->tgl_payrollawal,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_payrollakhir", "label"=>"Tanggal Akhir Payroll", "mandatory"=>"yes","value"=>displayDMY($qProfilesystem->tgl_payrollakhir,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_number(array("name"=>"max_rate_bpjstk", "label"=>"Maks Rate BPJS TK",  "mandatory"=>"yes", "value"=>number_format($qProfilesystem->max_rate_bpjstk,0)));
        $data["fields"][]      = form_number(array("name"=>"max_rate_bpjskes", "label"=>"Maks Rate BPJS Kes",  "mandatory"=>"yes", "value"=>number_format($qProfilesystem->max_rate_bpjskes,0)));
        $data["fields"][]      = form_select(array("name"=>"dibuatoleh", "label"=>"Divisi", "mandatory"=>"yes", "source"=>$qKaryawan,"value"=>$qProfilesystem->dibuatoleh));
        $data["fields"][]      = form_select(array("name"=>"diperiksaoleh", "label"=>"Diperiksa Oleh",  "mandatory"=>"yes","source"=>$qKaryawan, "value"=>$qProfilesystem->diperiksaoleh));
        $data["fields"][]      = form_select(array("name"=>"disetujuioleh", "label"=>"Disetujui Oleh",  "mandatory"=>"yes", "source"=>$qKaryawan,"value"=>$qProfilesystem->disetujuioleh));



        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_perusahaan' => 'required|'               
        );

        $messages = [
                    'nama_perusahaan.required' => 'Profilesystem harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/profilesystem/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qProfilesystem      = new ProfilesystemModel;
            # ---------------
            $qProfilesystem->updateData($request);
            # ---------------
            session()->flash("success_message", "Profilesystem has been updated");
            # ---------------
            return redirect("/profilesystem/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Profilesystem";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/profilesystem/remove";
        /* ----------
         Source
        ----------------------- */
        $qProfilesystem     = new ProfilesystemModel;
         $qProfilesystem                 = $qProfilesystem->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_profilesystem", "label"=>"Profilesystem ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_profilesystem", "label"=>"Profilesystem", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qProfilesystem->nama_profilesystem));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qProfilesystem     = new ProfilesystemModel;
            # ---------------
            $qProfilesystem->removeData($request);
            # ---------------
            session()->flash("success_message", "Profilesystem has been removed");
        } else {
            session()->flash("error_message", "Profilesystem cannot be removed");
        }
      # ---------------
        return redirect("/profilesystem/index"); 
    }
    
}
