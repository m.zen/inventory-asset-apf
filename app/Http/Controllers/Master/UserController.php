<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\UserModel;
use App\Model\Master\MasterModel;

class UserController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
      	$data["title"]	        = ucwords(strtolower($this->PROT_ModuleName));
      	$data["parent"]         = ucwords(strtolower($this->PROT_Parent));
      	$data["form_act"]       = "/user/index";
      	$data["active_page"]    = (empty($page)) ? 1 : $page;
      	$data["offset"] 	      = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
      	/* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qUser                  = new UserModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                        array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                        array("label"=>"Nama"
                                                ,"name"=>"name"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>""
                                                            ,"add-style"=>""),
                                        array("label"=>"Group"
                                                ,"name"=>"group_name"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                        array("label"=>"Status"
                                                ,"name"=>"status_code"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"flag"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""));
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_USER" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_USER");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_USER");
        }
        # ---------------
        $data["select"]        = $qUser->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qUser->getList($request->input("text_search"));
      	# ---------------
      	$data["record"]        = count($data["query"]);
      	$data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add User";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/user/save";
        /* ----------
         Model
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
        /* ----------
         Tabs
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes", "first_selected"=>"yes"));
       
        $data["fields"][]      = form_text(array("name"=>"name", "label"=>"Nama User", "mandatory"=>"yes", "focus_field"=>"password"));
         $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Alamat Email", "mandatory"=>""));
        $data["fields"][]      = form_select(array("name"=>"group_id", "label"=>"Nama Group", "mandatory"=>"yes", "source"=>$qGroups));
        $data["fields"][]      = form_password(array("name"=>"password", "label"=>"Password", "mandatory"=>"yes"));
        $data["fields"][]      = form_password(array("name"=>"password_confirm", "label"=>"Konfirmasi Password", "mandatory"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
      	$rules = array(
              		    'email' => 'required|',
                        'name' => 'required|',
                        'password' => 'required|min:3',
                        'password_confirm' => 'required|min:3|same:password',
        );

  	    $messages = [
            	        'email.required' => 'Email harus diisi',
            	        'name.required' => 'Nama harus diisi',
            	        'password.required' => 'Password harus diisi',
            	        'password_confirm.required' => 'Konfirmasi Password harus diisi',
            	        'password_confirm.same' => 'Konfirmasi Password harus sama',
  	    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/user/add")
                ->withErrors($validator)
                ->withInput();
        } else {
  	        $qUser 	= new UserModel;
  	        # ---------------
  	        $qUser->createData($request);
            # ---------------
            session()->flash("success_message", "User has been saved");
	   		    # ---------------
       	    return redirect("/user/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit User";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/user/update";
        /* ----------
         Model
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qUser                 = User::find($id);
        $qStatus 			   = getSelectStatusUser();
        $qGroups               = $qMaster->getSelectGroup();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
         $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes","value"=>$qUser->nik, "first_selected"=>"yes"));
                $data["fields"][]      = form_text(array("name"=>"name", "label"=>"Nama User", "mandatory"=>"yes", "value"=>$qUser->name, "first_selected"=>"yes", "focus_field"=>"password"));       

        $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Alamat Email", "readonly"=>"", "mandatory"=>"", "value"=>$qUser->email));

        $data["fields"][]      = form_select(array("name"=>"group_id", "label"=>"Nama Group", "mandatory"=>"yes", "source"=>$qGroups, "value"=>$qUser->group_id));
        $data["fields"][]      = form_password(array("name"=>"password", "label"=>"Password", "mandatory"=>"yes", "value"=>env("APPS_PWDDEF")));
        $data["fields"][]      = form_password(array("name"=>"password_confirm", "label"=>"Konfirmasi Password", "mandatory"=>"yes", "value"=>env("APPS_PWDDEF")));
        $data["fields"][]      = form_radio(array("name"=>"user_status", "label"=>"Status", "mandatory"=>"yes", "source"=>$qStatus, "value"=>$qUser->user_status));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
      	$rules = array(
            		   
                    'name' => 'required|',
                    'password' => 'required|min:3',
                    'password_confirm' => 'required|min:3|same:password',
        );

  	    $messages = [
          	    	   'name.required' => 'Nama harus diisi',
          	        'password.required' => 'Password harus diisi',
          	        'password_confirm.required' => 'Konfirmasi Password harus diisi',
          	        'password_confirm.same' => 'Konfirmasi Password harus sama',
  	    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/user/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qUser      = new UserModel;
            # ---------------
            $qUser->updateData($request);
	   		    # ---------------
            session()->flash("success_message", "User has been updated");
            # ---------------
	       	  return redirect("/user/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete User";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/user/remove";
        /* ----------
         Source
        ----------------------- */
        $qUser                 = User::find($id);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Alamat Email", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qUser->email));
        $data["fields"][]      = form_text(array("name"=>"name", "label"=>"Nama User", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qUser->name));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
      	if($request->input("id") != 1) {
            $qUser      = new UserModel;
            # ---------------
            $qUser->removeData($request);
            # ---------------
            session()->flash("success_message", "User has been removed");
  	   	} else {
            session()->flash("error_message", "User cannot be removed");
        }
   		# ---------------
       	return redirect("/user/index");
    }

    public function changepassword() {
        $id                    = Auth::user()->id;
        $data["label"]         = "User";
        $data["title"]         = "Change Password";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/user/updatepassword";
        /* ----------
         Source
        ----------------------- */
        $qUser                 = User::find($id);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Alamat Email", "readonly"=>"readonly", "value"=>$qUser->email));
        $data["fields"][]      = form_password(array("name"=>"current_password", "label"=>"Password Saat ini", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_password(array("name"=>"new_password", "label"=>"Password Baru", "mandatory"=>"yes"));
        $data["fields"][]      = form_password(array("name"=>"password_confirm", "label"=>"Konfirmasi Password", "mandatory"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function updatepassword(Request $request) {
        $rules = array(
                    'current_password' => 'required|min:3',
                    'new_password' => 'required|min:3',
                    'password_confirm' => 'required|min:3|same:new_password',
        );

        $messages = [
                    'current_password.required' => 'Passsword saat ini harus diisi',
                    'new_password.required' => 'Password baru harus diisi',
                    'password_confirm.required' => 'Konfirmasi Password baru harus diisi',
                    'password_confirm.same' => 'Konfirmasi Password harus sama',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("user/changepassword")
                ->withErrors($validator)
                ->withInput();
        } else {
            if(Hash::check($request->current_password, Auth::User()->password)) {
                $qUser      = new UserModel;
                # ---------------
                $qUser->updatePassword($request);
                # ---------------
                return redirect("/");
            } else {
                session()->flash("error_message", "Current password salah");
                # ---------------
                return redirect("user/changepassword");
            }
        }
    }
}
