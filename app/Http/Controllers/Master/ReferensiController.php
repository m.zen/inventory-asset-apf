<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use File;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\ReferensiModel;
use App\Model\Master\MasterModel;

class ReferensiController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request)
    {
        # ---------------
        $uri = getUrl() . "/index";
        # ---------------
        $qMenu = new MenuModel;
        $rs = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent = $rs[0]->parent_name;
        $this->PROT_ModuleName = $rs[0]->name;
        $this->PROT_ModuleId = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent" => $this->PROT_Parent, "SHR_Module" => $this->PROT_ModuleName));
    }

    public function index(Request $request, $page = null)
    {
        $data["title"] = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/referensi/index";
        $data["active_page"] = (empty($page)) ? 1 : $page;
        $data["offset"] = (empty($data["active_page"])) ? 0 : ($data["active_page"] - 1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu = new MenuModel;
        $qRef = new ReferensiModel;
        $qMaster = new MasterModel;
        # ---------------
        $data["action"] = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        $data["filtered_info"] = array();
        $qStatus = getStatusproses();
       
        /* ----------
         Table header
        ------------------ */
        $data["table_header"] = array(
            array(
                "label" => "ID", "name" => "id_referensi", "align" => "center", "item-align" => "center", "item-format" => "checkbox", "item-class" => "", "width" => "5%", "add-style" => ""
            ),
            array(
                "label" => "No Referensi", "name" => "no_referensi", "align" => "center", "item-align" => "left", "item-format" => "normal", "item-class" => "", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Jenis Referensi", "name" => "jenis_referensi", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),


        );
        # ---------------
        if ($request->has('text_search')) {
            session(["SES_SEARCH_REFERENSI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"] = $request->session()->get("SES_SEARCH_REFERENSI");
        } else {
            $data["text_search"] = $request->session()->get("SES_SEARCH_REFERENSI");
        }
        # ---------------
        $data["select"] = $qRef->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"] = $qRef->getList($request->input("text_search"));
        # ---------------
        $data["record"] = count($data["query"]);
        $data["pagging"] = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add()
    {
        $data["title"] = "Add Dokumen Referensi";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/referensi/save";
        /* ----------
         Asset
        ----------------------- */
        $qMaster = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;
        /* ----------
        
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];

        $qJref = array_merge($collection, getSelectJenisRef());
        
      
     
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][] = form_text(array("name" => "no_referensi", "label" => "No Ref", "mandatory" => ""));
        $data["fields"][] = form_select(array("name" => "jenis_referensi", "label" => "Jenis Dokumen", "mandatory" => "yes", "source" => $qJref));
        $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Dokumen", "mandatory" => "yes"));
     
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request)
    {
        $rules = array(

            'no_referensi' => 'required'
        );

        $messages = ['no_referensi.required' => 'No Referensi tidak boleh kosong'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/referensi/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qRef = new ReferensiModel;
            # ---------------
            $qRef->createData($request);
            # ---------------
            session()->flash("success_message", "Referensi Dokumen Baru has been saved");
            # ---------------
            return redirect("/referensi/index");
        }
    }

    public function edit($id)
    {
        $data["title"] = "Edit Referensi Dokumen";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/referensi/update";
        
  /* ----------
         Asset
        ----------------------- */
        $qMaster = new MasterModel;
        /* ----------
         Source
        ----------------------- */

        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;


        $qRef = new ReferensiModel;
        /* ----------
         Source
        ----------------------- */
        $qRef = $qRef->getProfile($id)->first();
       
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qJref = array_merge($collection, getSelectJenisRef());
        $filepath = public_path() . '/app/photo_referensi/' . $qRef->gambar_referensi;
 
       // dd($qTransMasuk);
        $data["fields"][] = form_hidden(array("name" => "id_referensi", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));
         $data["fields"][] = form_text(array("name" => "no_referensi", "label" => "No Referensi", "mandatory" => "","value"=>$qRef->no_referensi));
        $data["fields"][] = form_select(array("name" => "jenis_referensi", "label" => "Jenis Dokumen", "mandatory" => "yes", "source" => $qJref,"value"=>$qRef->jenis_referensi));
        if (empty($qRef->gambar_referensi)) {
            $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Dokumen", "mandatory" => "yes", "value" => $filepath));

        } else {
            $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Dokumen", "mandatory" => "", "value" => $filepath));

        }
        $data["fields"][] = form_hidden(array("name" => "nama_gambar", "label" => "Nama Gambar", "mandatory" => "", "value" => $qRef->gambar_referensi));
    
        //$data["fields"][] = form_upload3(array("name" => "foto2", "label" => "Foto"));
        if (!empty($qRef->gambar_referensi)) {

            $vurl = url('') . "/app/photo_dokumen/" . $qRef->gambar_referensi;
            $pathImage = url('') . "/app/img/icon/remove.png";
            $vurlDelete = url('') . "/referensi/deletefile/" . $id . "/" . $qRef->gambar_referensi;
            $data["fields"][] = "<div class=\"form-group\">
                                      <label class=\"col-md-3 control-label\"></label>
                                      <div class=\"col-md-9\">
                                                <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a><a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>
                                      </div></div>";
        }
    
   
      


        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Update"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
       // dd($request);
        $rules = array(
            'nama_referensi' => 'id_referensi|'
        );

        $messages = [
            'nama_referensi.required' => 'Asset harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/referensi/edit/" . $request->input("id_referensi"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qRef = new ReferensiModel;
            # ---------------
            $qRef->updateData($request);
            # ---------------
            session()->flash("success_message", "Data Referensi has been updated");
            # ---------------
            return redirect("/referensi/index");
        }
    }

    public function delete($id)
    {
        $data["title"] = "Delete Dokumen Referensi";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/referensi/remove";
        /* ----------
         Source
        ----------------------- */
        $qRef = new ReferensiModel;
        $qRef = $qAsset->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][] = form_hidden(array("name" => "id_referensi", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "DELETE"));
        $data["fields"][] = form_text(array("name" => "nama_referensi", "label" => "Asset", "readonly" => "readonly", "mandatory" => "yes", "value" => $qAsset->nama_referensi));
        
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Delete"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request)
    {
        if ($request->input("id") != 1) {
            $qAsset = new AssetModel;
            # ---------------
            $qAsset->removeData($request);
            # ---------------
            session()->flash("success_message", "Asset has been removed");
        } else {
            session()->flash("error_message", "Asset cannot be removed");
        }
      # ---------------
        return redirect("/referensi/index");
    }
    public function deletefile($id, $namafile)
    {
        $tm_timeoff = DB::table('m_referensi')->where('id_referensi', $id)->first();
        $file_path = public_path() . '/app/photo_dokumen/' . $tm_timeoff->gambar_referensi;
        File::delete($file_path);
        DB::table('m_referensi')->where('id_referensi', $id)->update(['gambar_referensi' => null]);
        session()->flash('success_message', 'Foto berhasil dihapus.');
      # ---------------
        return redirect("/referensi/edit/" . $id);
    }
}
