<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\BarangModel;
use App\Model\Master\MasterModel;
class BarangController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/barang/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qBarang                  = new BarangModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_barang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Kode Barang"
                                                ,"name"=>"kode_barang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Barang"
                                                            ,"name"=>"nama_barang"
                                                              ,"align"=>"center"
                                                                ,"item-align"=>"left"
                                                                  ,"item-format"=>"normal"
                                                                    ,"item-class"=>""
                                                                      ,"width"=>"25%"
                                                                        ,"add-style"=>""),
                                    array("label"=>"Grup Barang"
                                                            ,"name"=>"nama_grupbarang"
                                                               ,"align"=>"center"
                                                                  ,"item-align"=>"left"
                                                                    ,"item-format"=>"normal"
                                                                      ,"item-class"=>""
                                                                         ,"width"=>"15%"
                                                                            ,"add-style"=>""),
                                    array("label"=>"Merk"
                                                            ,"name"=>"nama_merk"
                                                              ,"align"=>"center"
                                                                ,"item-align"=>"left"
                                                                   ,"item-format"=>"normal"
                                                                       ,"item-class"=>""
                                                                          ,"width"=>"15%"
                                                                            ,"add-style"=>""),
                                    array("label"=>"Ketegori"
                                                            ,"name"=>"nama_kategori"
                                                               ,"align"=>"center"
                                                                  ,"item-align"=>"left"
                                                                    ,"item-format"=>"normal"
                                                                       ,"item-class"=>""
                                                                          ,"width"=>"15%"
                                                                           ,"add-style"=>""),   
                                    array("label"=>"Keterangan"
                                                            ,"name"=>"keterangan"
                                                                ,"align"=>"center"
                                                                    ,"item-align"=>"left"
                                                                       ,"item-format"=>"normal"
                                                                          ,"item-class"=>""
                                                                             ,"width"=>"15%"
                                                                                ,"add-style"=>""),                                   
            
                                );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_BARANG" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_BARANG");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_BARANG");
        }
        # ---------------
        $data["select"]        = $qBarang->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qBarang->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------

        
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Barang";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/barang/save";
        /* ----------
         Barang
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
        
        ----------------------- */
        $collection             = [ (object)
                [
                'id' => '-',
                'name' => '--Pilih--'
                ]
        ];
        $qGroups               = $qMaster->getSelectGroup();
        $qGroupbarang          =  array_merge($collection,$qMaster->getSelectGrupbarang());
        $qMerk                 = array_merge($collection,$qMaster->getSelectMerk());
        $qKategori             = array_merge($collection,$qMaster->getSelectKategori());
        

       
      
     
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"kode_barang", "label"=>"Kode Barang", "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_barang", "label"=>"Nama Barang", "mandatory"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"id_grupbarang", "label"=>"Grup Barang", "mandatory"=>"yes", "source"=>$qGroupbarang));
       $data["fields"][]      = form_select(array("name"=>"id_merk", "label"=>"Merk", "mandatory"=>"", "source"=>$qMerk));
       $data["fields"][]      = form_select(array("name"=>"id_kategori", "label"=>"Kategori", "mandatory"=>"yes", "source"=>$qKategori));
       $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Keterangan", "mandatory"=>""));
      
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_barang' => 'required'                     );

        $messages = ['nama_barang.required' => 'Barang harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/barang/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qBarang  = new BarangModel;
            # ---------------
            $qBarang->createData($request);
            # ---------------
            session()->flash("success_message", "Barang has been saved");
            # ---------------
            return redirect("/barang/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Barang";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/barang/update";
        /* ----------
         Barang
        ----------------------- */
        $qMaster              = new MasterModel;
        $qBarang             = new BarangModel;
        /* ----------
         Source
        ----------------------- */
        $qBarang             = $qBarang->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();
        $collection             = [ (object)
                [
                'id' => '-',
                'name' => '--Pilih--'
                ]
        ];
        $qGroups               = $qMaster->getSelectGroup();
        $qGroupbarang          =  array_merge($collection,$qMaster->getSelectGrupbarang());
        $qMerk                 = array_merge($collection,$qMaster->getSelectMerk());
        $qKategori             = array_merge($collection,$qMaster->getSelectKategori());
      
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_barang", "label"=>"Barang ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"kode_barang", "label"=>"Barang",  "mandatory"=>"yes", "value"=>$qBarang->kode_barang));
        $data["fields"][]      = form_text(array("name"=>"nama_barang", "label"=>"Barang",  "mandatory"=>"yes", "value"=>$qBarang->nama_barang));
        $data["fields"][]      = form_select(array("name"=>"id_grupbarang", "label"=>"Grup Barang", "mandatory"=>"yes", "source"=>$qGroupbarang,"value"=>$qBarang->id_grupbarang));
        $data["fields"][]      = form_select(array("name"=>"id_merk", "label"=>"Merk", "mandatory"=>"", "source"=>$qMerk,"value"=>$qBarang->id_merk));
        $data["fields"][]      = form_select(array("name"=>"id_kategori", "label"=>"Kategori", "mandatory"=>"yes", "source"=>$qKategori,"value"=>$qBarang->id_kategori));
        $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Keterangan", "mandatory"=>"","value"=>$qBarang->keterangan));
       
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_barang' => 'required|'               
        );

        $messages = [
                    'nama_barang.required' => 'Barang harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/barang/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qBarang      = new BarangModel;
            # ---------------
            $qBarang->updateData($request);
            # ---------------
            session()->flash("success_message", "Barang has been updated");
            # ---------------
            return redirect("/barang/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Barang";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/barang/remove";
        /* ----------
         Source
        ----------------------- */
        $qBarang     = new BarangModel;
         $qBarang                 = $qBarang->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_barang", "label"=>"Barang ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_barang", "label"=>"Barang", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qBarang->nama_barang));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qBarang     = new BarangModel;
            # ---------------
            $qBarang->removeData($request);
            # ---------------
            session()->flash("success_message", "Barang has been removed");
        } else {
            session()->flash("error_message", "Barang cannot be removed");
        }
      # ---------------
        return redirect("/barang/index"); 
    }
}
