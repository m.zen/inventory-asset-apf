<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\GrupbarangModel;
use App\Model\Master\MasterModel;
class GrupbarangController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/grupbarang/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qGrupbarang                  = new GrupbarangModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"  
                                                      ,"name"=>"id_grupbarang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                     							array("label"=>"Grupbarang"
                                                ,"name"=>"nama_grupbarang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_GRUPBARANG" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_GRUPBARANG");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_GRUPBARANG");
        }
        # ---------------
        $data["select"]        = $qGrupbarang->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qGrupbarang->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Grupbarang";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/grupbarang/save";
        /* ----------
         Grupbarang
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
       
      
     
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_grupbarang", "label"=>"Grup Barang", "mandatory"=>"yes"));
       
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_grupbarang' => 'required'                     );

        $messages = ['nama_grupbarang.required' => 'Grupbarang harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/grupbarang/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrupbarang  = new GrupbarangModel;
            # ---------------
            $qGrupbarang->createData($request);
            # ---------------
            session()->flash("success_message", "Grupbarang has been saved");
            # ---------------
            return redirect("/grupbarang/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Grupbarang";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/grupbarang/update";
        /* ----------
         Grupbarang
        ----------------------- */
        $qMaster              = new MasterModel;
        $qGrupbarang             = new GrupbarangModel;
        /* ----------
         Source
        ----------------------- */
        $qGrupbarang             = $qGrupbarang->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();
      
       
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_grupbarang", "label"=>"Grupbarang ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
       $data["fields"][]      = form_text(array("name"=>"nama_grupbarang", "label"=>"Grup Barang",  "mandatory"=>"yes", "value"=>$qGrupbarang->nama_grupbarang));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_grupbarang' => 'required|'               
        );

        $messages = [
                    'nama_grupbarang.required' => 'Grupbarang harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/grupbarang/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrupbarang      = new GrupbarangModel;
            # ---------------
            $qGrupbarang->updateData($request);
            # ---------------
            session()->flash("success_message", "Grupbarang has been updated");
            # ---------------
            return redirect("/grupbarang/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Grupbarang";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/grupbarang/remove";
        /* ----------
         Source
        ----------------------- */
        $qGrupbarang     = new GrupbarangModel;
         $qGrupbarang                 = $qGrupbarang->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_grupbarang", "label"=>"Grupbarang ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_grupbarang", "label"=>"Grupbarang", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qGrupbarang->nama_grupbarang));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qGrupbarang     = new GrupbarangModel;
            # ---------------
            $qGrupbarang->removeData($request);
            # ---------------
            session()->flash("success_message", "Grupbarang has been removed");
        } else {
            session()->flash("error_message", "Grupbarang cannot be removed");
        }
      # ---------------
        return redirect("/grupbarang/index"); 
    }
}
