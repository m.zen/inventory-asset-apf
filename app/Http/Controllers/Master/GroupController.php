<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use App\Model\MenuModel;
use App\Model\GroupMenuModel;
use App\Model\Master\GroupModel;
use App\Model\Master\MasterModel;

class GroupController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]	        = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/group/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"] 	    = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qGroup                 = new GroupModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                        array("label"=>"Name"
                                                ,"name"=>"name"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"95%"
                                                            ,"add-style"=>""));
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_GROUP" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_GROUP");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_GROUP");
        }
        # ---------------
        $data["select"]        = $qGroup->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qGroup->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Group";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/group/save";
        /* ----------
         Model
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"name", "label"=>"Nama Group", "mandatory"=>"yes", "first_selected"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
      	$rules = array(
                      'name' => 'required|',
        );

  	    $messages = [
            	        'name.required' => 'Nama harus diisi',
  	    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/group/add")
                ->withErrors($validator)
                ->withInput();
        } else {
  	        $qGroup 	= new GroupModel;
  	        # ---------------
  	        $qGroup->createData($request);
            # ---------------
            session()->flash("success_message", "Group has been saved");
	   		    # ---------------
       	    return redirect("/group/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Group";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/group/update";
        /* ----------
         Model
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroup                = GroupModel::find($id);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"name", "label"=>"Nama User", "mandatory"=>"yes", "value"=>$qGroup->name, "first_selected"=>"yes", "focus_field"=>"password"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request) {
      	$rules = array(
                    'name' => 'required|',
        );

  	    $messages = [
          	        'name.required' => 'Nama harus diisi',
  	    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/group/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGroup      = new GroupModel;
            # ---------------
            $qGroup->updateData($request);
	   		    # ---------------
            session()->flash("success_message", "Group has been updated");
            # ---------------
	       	  return redirect("/group/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Group";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/group/remove";
        /* ----------
         Source
        ----------------------- */
        $qGroup                = GroupModel::find($id);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"name", "label"=>"Nama User", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qGroup->name));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
      	if($request->input("id") != 1) {
            $qGroup      = new GroupModel;
            # ---------------
            $qGroup->removeData($request);
            # ---------------
            session()->flash("success_message", "Group has been removed");
  	   	} else {
            session()->flash("error_message", "Group cannot be removed");
        }
   		  # ---------------
       	return redirect("/group/index");
    }

    public function privilege($id) {
        $data["title"]        = "Privilege";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/group/setprivilege";
        /* ----------
         Model
        ----------------------- */
        $qGroupMenu            = new GroupMenuModel;
        $qGroup                = GroupModel::find($id);
        /* ----------
         Source
        ----------------------- */
        $data["privilage"]     = $qGroupMenu->getAllMenus($id);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"group", "label"=>"Nama Group", "readonly"=>"readonly", "value"=>$qGroup->name));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Save"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("group.formchecklist", $data);
    }

    public function setprivilege(Request $request) {
        $qGroupMenu     = new GroupMenuModel;
        # ---------------
        $qGroupMenu->removeDataByGroup($request);
        # ---------------
        $Unit   = count($request->input("check_menu"));
        # ---------------
        for($i=0; $i<$Unit; $i++) {
            $qGroupMenu->createData($request, $i);
        }
        # ---------------
        session()->flash("success_message", "Privilege has been updated");
        # ---------------
        return redirect("/group/index");
    }
}
