<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\GroupumModel;
use App\Model\Master\MasterModel;

class GroupumController extends Controller
{
   public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }


    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/groupum/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qJabatan                  = new GroupumModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */

        //dd($qJabatan);
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_groupum"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Uang Makan per hari"
                                                ,"name"=>"uang_makan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""));
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_JABATAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JABATAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JABATAN");
        }
        # ---------------
        $data["select"]        = $qJabatan->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qJabatan->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Jabatan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/groupum/save";
        /* ----------
         Jabatan
        ----------------------- */
        $qMaster               = new MasterModel;

        /* ----------
         Source
        ----------------------- */
        $qCabang               = $qMaster->getSelectCabang();
        $qJabatan               = $qMaster->getSelectJabatan();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan));
       $data["fields"][]      = form_currency(array("name"=>"uang_makan", "label"=>"Uang Makan per hari", "value"=>number_format(0,0)));
         
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'uang_makan' => 'required'                     );

        $messages = ['uang_makan.required' => 'Uang Makan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/groupum/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qJabatan  = new GroupumModel;
            # ---------------
            $qJabatan->createData($request);
            # ---------------
            session()->flash("success_message", "Jabatan has been saved");
            # ---------------
            return redirect("/groupum/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Uang Makan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/groupum/update";
        /* ----------
         Jabatan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qGroup             = new GroupumModel;
        /* ----------
         Source
        ----------------------- */
        $qGroup             = $qGroup->getProfile($id)->first();
        $qCabang              = $qMaster->getSelectCabang();
        $qJabatan               = $qMaster->getSelectJabatan();
        //dd($qGroups);
        /* ----------
         Fields
        ----------------------- */
       // $qGroups               = $qMaster->getSelectGroup();
        $data["fields"][]      = form_hidden(array("name"=>"id_groupum", "label"=>"Grup ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes","value"=>$qGroup->id_cabang, "source"=>$qCabang));
        $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes","value"=>$qGroup->id_jabatan, "source"=>$qJabatan));
        $data["fields"][]      = form_currency(array("name"=>"uang_makan", "label"=>"Tnj. Transport", "value"=>number_format($qGroup->uang_makan,0)));    
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'uang_makan' => 'required|' ,
        );

        $messages = [
                    'uang_makan.required'=>'Uang Makan harus isi'

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/groupum/edit/" . $request->input("id_groupum"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qJabatan      = new GroupumModel;
            # ---------------
            $qJabatan->updateData($request);
            # ---------------
            session()->flash("success_message", "Jabatan has been updated");
            # ---------------
            return redirect("/groupum/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Uang Makan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/groupum/remove";
        /* ----------
         Source
        ----------------------- */
        $qGroup     = new GroupumModel;
         $qGroup    = $qGroup->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_groupum", "label"=>"Jabatan ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_jabatan", "label"=>"Jabatan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qGroup->nama_jabatan));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qJabatan     = new GroupumModel;
            # ---------------
            $qJabatan->removeData($request);
            # ---------------
            session()->flash("success_message", "Jabatan has been removed");
        } else {
            session()->flash("error_message", "Jabatan cannot be removed");
        }
      # ---------------
        return redirect("/groupum/index"); 
    }
}
