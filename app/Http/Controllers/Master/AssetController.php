<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\AssetModel;
use App\Model\Master\MasterModel;
class AssetController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/asset/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qAsset                  = new AssetModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_asset"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"No Asset"
                                                ,"name"=>"no_asset"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                   array("label"=>"Nama Barang"
                                                ,"name"=>"nama_barang"
                                                    ,"align"=>"center"
                                                       ,"item-align"=>"left"
                                                          ,"item-format"=>"normal"
                                                              ,"width"=>"25%"
                                                                 ,"add-style"=>""),
                                    array("label"=>"Keterangan"
                                                ,"name"=>"keterangan"
                                                    ,"align"=>"center"
                                                        ,"item-align"=>"left"
                                                           ,"item-format"=>"normal"
                                                              ,"width"=>"25%"
                                                                ,"add-style"=>""),
                                    array("label"=>"Tgl Perolehan"
                                                ,"name"=>"tgl_perolehan"
                                                   ,"align"=>"center"
                                                       ,"item-align"=>"left"
                                                           ,"item-format"=>"normal"
                                                              ,"width"=>"25%"
                                                                 ,"add-style"=>""),
                                                             
            
                                );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_ASSET" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_ASSET");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_ASSET");
        }
        # ---------------
        $data["select"]        = $qAsset->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qAsset->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Asset";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/asset/save";
        /* ----------
         Asset
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
        $qMaster               = new MasterModel;
        /* ----------
        
        ----------------------- */
        $collection             = [ (object)
                [
                'id' => '-',
                'name' => '--Pilih--'
                ]
        ];
        $qBarang          =  array_merge($collection,$qMaster->getSelectBarang());
        $qCabang          = array_merge($collection,$qMaster->getSelectCabang());
        $qLokasi          = array_merge($collection,$qMaster->getSelectLokasi());
        $qStatusasset     =  array_merge($collection,getSelectStatusAsset());  
      
     
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"id_barang", "label"=>"Asset", "mandatory"=>"yes","source"=>$qBarang));
        $data["fields"][]      = form_text(array("name"=>"no_asset", "label"=>"No Asset", "mandatory"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_perolehan", "label"=>"Tgl Perolehan", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>""));
        $data["fields"][]      = form_select(array("name"=>"status_asset", "label"=>"Status Kepemilikan", "mandatory"=>"yes","source"=>$qStatusasset));
        $data["fields"][]      = form_currency(array("name"=>"harga_perolehan", "label"=>"Harga", "mandatory"=>"yes","value"=>number_format(0)),0);
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes","source"=>$qCabang));
        $data["fields"][]      = form_select(array("name"=>"id_lokasi", "label"=>"Lokasi/PIC", "mandatory"=>"yes","source"=>$qLokasi));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'no_asset' => 'required'                     );

        $messages = ['no_asset.required' => 'Asset harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/asset/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qAsset  = new AssetModel;
            # ---------------
            $qAsset->createData($request);
            # ---------------
            session()->flash("success_message", "Asset has been saved");
            # ---------------
            return redirect("/asset/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Asset";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/asset/update";
        /* ----------
         Asset
        ----------------------- */
        $qMaster              = new MasterModel;
        $qAsset             = new AssetModel;
        /* ----------
         Source
        ----------------------- */
        $qAsset             = $qAsset->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();
      
       
        /* ----------
         Fields
        ----------------------- */
        $collection             = [ (object)
                [
                'id' => '-',
                'name' => '--Pilih--'
                ]
        ];
        $qBarang          =  array_merge($collection,$qMaster->getSelectBarang());
        $qCabang          = array_merge($collection,$qMaster->getSelectCabang());
        $qLokasi          = array_merge($collection,$qMaster->getSelectLokasi());
        $qStatusasset     =  array_merge($collection,getSelectStatusAsset());  
             
    
        $data["fields"][]      = form_hidden(array("name"=>"id_asset", "label"=>"Asset ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_select(array("name"=>"id_barang", "label"=>"Asset", "mandatory"=>"yes","source"=>$qBarang,"value"=>$qAsset->id_barang));
        $data["fields"][]      = form_text(array("name"=>"no_asset", "label"=>"No Asset", "mandatory"=>"yes","value"=>$qAsset->no_asset));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_perolehan", "label"=>"Tgl Perolehan", "mandatory"=>"yes","value"=>displayDMY($qAsset->tgl_perolehan,"/")));
        $data["fields"][]      = form_select(array("name"=>"status_asset", "label"=>"Status Kepemilikan", "mandatory"=>"yes","source"=>$qStatusasset,"value"=>$qAsset->status_asset));
        $data["fields"][]      = form_currency(array("name"=>"harga_perolehan", "label"=>"Harga", "mandatory"=>"yes","value"=>number_format($qAsset->harga_perolehan)),0);
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes","source"=>$qCabang,"value"=>$qAsset->id_cabang));
        $data["fields"][]      = form_select(array("name"=>"id_lokasi", "label"=>"Lokasi/PIC", "mandatory"=>"yes","source"=>$qLokasi,"value"=>$qAsset->id_lokasi));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_asset' => 'required|'               
        );

        $messages = [
                    'nama_asset.required' => 'Asset harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/asset/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qAsset      = new AssetModel;
            # ---------------
            $qAsset->updateData($request);
            # ---------------
            session()->flash("success_message", "Asset has been updated");
            # ---------------
            return redirect("/asset/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Asset";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/asset/remove";
        /* ----------
         Source
        ----------------------- */
        $qAsset     = new AssetModel;
         $qAsset                 = $qAsset->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_asset", "label"=>"Asset ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_asset", "label"=>"Asset", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qAsset->nama_asset));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qAsset     = new AssetModel;
            # ---------------
            $qAsset->removeData($request);
            # ---------------
            session()->flash("success_message", "Asset has been removed");
        } else {
            session()->flash("error_message", "Asset cannot be removed");
        }
      # ---------------
        return redirect("/asset/index"); 
    }
}
