<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\DepartemenModel;
use App\Model\Master\MasterModel;

class DepartemenController extends Controller
{
   protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
       
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/departemen/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qDepartemen                  = new DepartemenModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                ));
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_DEPARTEMEN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
        }
        # ---------------
        $data["select"]        = $qDepartemen->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDepartemen->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Departemen";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/departemen/save";
        /* ----------
         Departemen
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
         $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '--Pilih--'
                                    ]
                                ];

         /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_departemen", "label"=>"Departemen", "mandatory"=>"yes"));
       //$data["fields"][]      = form_select(array("name"=>"id_divisi", "label"=>"Divisi", "mandatory"=>"yes", "source"=>$qDivisi));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_departemen' => 'required'                     );

        $messages = ['nama_departemen.required' => 'Departemen harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/Departemen/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qDepartemen  = new DepartemenModel;
            # ---------------
            $qDepartemen->createData($request);
            # ---------------
            session()->flash("success_message", "Departemen has been saved");
            # ---------------
            return redirect("/departemen/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Departemen";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/departemen/update";
        /* ----------
         Departemen
        ----------------------- */
        $qMaster              = new MasterModel;
        $qDepartemen             = new DepartemenModel;
        /* ----------
         Source
        ----------------------- */
        $qDepartemen             = $qDepartemen->getProfile($id)->first();
   

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_departemen", "label"=>"Departemen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nama_departemen", "label"=>"Departemen",  "mandatory"=>"yes", "value"=>$qDepartemen->nama_departemen));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_departemen' => 'required|' ,
                             
        );

        $messages = [
                    'nama_departemen.required' => 'Departemen harus diisi',
                   

        ];

 

        $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/departemen/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qDepartemen      = new DepartemenModel;
            # ---------------
            $qDepartemen->updateData($request);
            # ---------------
            session()->flash("success_message", "Departemen has been updated");
            # ---------------
            return redirect("/departemen/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Departemen";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/departemen/remove";
        /* ----------
         Source
        ----------------------- */
        $qDepartemen     = new DepartemenModel;
         $qDepartemen                 = $qDepartemen->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_departemen", "label"=>"Departemen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_departemen", "label"=>"Departemen", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qDepartemen->nama_departemen));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qDepartemen     = new DepartemenModel;
            # ---------------
            $qDepartemen->removeData($request);
            # ---------------
            session()->flash("success_message", "Departemen has been removed");
        } else {
            session()->flash("error_message", "Departemen cannot be removed");
        }
      # ---------------
        return redirect("/departemen/index"); 
    }
}
