<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use Auth;
use App\Model\LogModel;
class DashboardController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct() {
        # ---------------
        $uri          			    = \Request::segment(1);
        # ---------------
        $this->PROT_Parent 	    = "Dashboard";
        $this->PROT_ModuleName  = "Dashboard";
        $this->PROT_ModuleId    = 1;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index() {
        $data["title"]         = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        /* ----------
        Dashboard*/
        //Total Karyawan

        $qAkses = DB::table("sys_users")
                            ->select("nik","group_id")
                            ->where('nik','=',Auth::user()->nik)->first();
        $data['hakAskses'] =  $qAkses->group_id; 

    //    if ($qAkses->group_id != 1 and $qAkses->group_id!=4 )
    //     {    $qidKry = DB::table("p_karyawan")
    //                             ->select("id_karyawan")
    //                             ->where('nik','=',Auth::user()->nik)->get()->first();

                               
    //         $data['idKry'] = "/detail/index/". $qidKry->id_karyawan;    
    //     }
        






        // $query  = DB::table("p_karyawan")
        //                     ->select("*")
        //                     ->where('aktif','=',1);
        $data['totalKaryawan'] = 10; // $query->count();
        $data['totalkontrak'] = 20; //DB::table("vw_jatuhtempo")->count() ;
        $data['totalakankontrak'] = 30; // DB::table("vw_akanjthtempo")->count() ;

      // $query  = DB::select("SELECT nik,nama_karyawan,tgl_lahir FROM p_karyawan  WHERE aktif=1 and  DATE_FORMAT(tgl_lahir,'%m %d') BETWEEN DATE_FORMAT(CURDATE(),'%m %d')  AND DATE_FORMAT((INTERVAL 3 DAY + CURDATE()),'%m %d') ORDER BY DATE_FORMAT(tgl_lahir,'%m %d ')");

                

        
         $data['ultah'] = 10;// count($query);
                           


        //Time Off

        
      $data["tabs"]          = array(array("label"=>"Data Karyawan", "url"=>"/tarif/index", "active"=>"active")
                                 ,array("label"=>"Kehadiran", "url"=>"/ptkp/index", "active"=>"")
                                
                                        );

   

         
       
        return view("dashboard", $data);

         
    }
}
