<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\KontrakkerjaModel;
use PDF;

class KaryawankontrakController extends Controller
{
   protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawankontrak/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawankontrak       = new KontrakkerjaModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */

        $data["tabs"]          = array(array("label"=>"Kontrak Baru", "url"=>"/karyawankontrak/index", "active"=>"active")
                                       ,array("label"=>"Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/jtk_index", "active"=>"")
                                         ,array("label"=>"Akan Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/akanjtk_index", "active"=>"")
                                         ,array("label"=>"Jatuh Tempo Percobaan", "url"=>"/karyawankontrak/percobaan_jt_index", "active"=>""));
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_kontrakkerja"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							 array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                         array("label"=>"N I K Baru"
                                                ,"name"=>"nik_baru"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
        							  array("label"=>"Nama"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
        							   array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
        							  
                                
                                      array("label"=>"Tgl Awal"
                                                ,"name"=>"tgl_awal"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                        
                                          array("label"=>"Keterangan"
                                                ,"name"=>"keterangan_kontrak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                          array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KONTRAK" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAK");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAK");
        }
        # ---------------
        $data["select"]        = $qKaryawankontrak->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawankontrak->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.tablist", $data);
    }

    public function add() {
        $data["title"]         = "Add PKWT Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawankontrak/save";
       
       
       // $qKaryawankeluar            = new KaryawankeluarModel;

       
 		$qMaster               = new MasterModel;
       // $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		    $qJabatan              = $qMaster->getSelectJabatan(); 
		    $qStatuskerja          = getSelectStskerja();
        $qKontrakkerjamdl      = new KontrakkerjaModel;
        $qListKry              = $qMaster->getSelectKaryawankontrak1();
        
        //$qViewKaryawan        = $qKontrakkerjamdl->getProfileKry($idkry)->first();  
       // $qViewFormatsurat     = $qKontrakkerjamdl->getProfileKry($idkry)->first(); 
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan","withnull"=>"whithnull","mandatory"=>"yes", "source"=>$qListKry));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        

       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Save"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'id_karyawan' => 'required'                     );

        $messages = ['id_karyawan.required' => 'Nama Karyawan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawankontrak/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawankontrak  = new KontrakkerjaModel;
            # ---------------

            $qKaryawankontrak->createData($request);
            # ---------------
            session()->flash("success_message", "Karyawan kontrak has been saved");
            # ---------------
            return redirect("/karyawankontrak/index/");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Kontrak Kerja (PKWT)";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawankontrak/update";
        /* ----------
         Kontrakkerja
        ----------------------- */
        $qMaster           = new MasterModel;
        $qKontrakkerja             = new KontrakkerjaModel;
        /* ----------
         Source
        ----------------------- */
        //$idKontrakkerja             = explode("&", $id); 
        $qKontrakkerja        = $qKontrakkerja->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan();
		
		$qStatuskerja          = getSelectStskerja();
        
        /* ----------
         Fields
        ----------------------- */
        if($qKontrakkerja->status_kontrak=="reguler")
        {
            $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Kontrakkerja", "readonly"=>"readonly", "value"=>$id));
            $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
            $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qKontrakkerja->id_karyawan));
            $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes","readonly"=>"readonly","value"=>$qKontrakkerja->nik));
            
            $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKontrakkerja->nama_karyawan));
          
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_awal,"/"), "first_selected"=>"yes"));
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_akhir,"/"), "first_selected"=>"yes"));
             $data["fields"][]      = form_text(array("name"=>"keterangan_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","readonly"=>"readonly","value"=> $qKontrakkerja->keterangan_kontrak));
       }
       else
       {
            $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Kontrakkerja", "readonly"=>"readonly", "value"=>$id));
            $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
            $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qKontrakkerja->id_karyawan));
            $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes","readonly"=>"readonly","value"=>$qKontrakkerja->nik));
            
            $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKontrakkerja->nama_karyawan));
                    $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar","readonly"=>"readonly", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_keluar,"/")));
      

       $data["fields"][]      = form_text(array("name"=>"nik_baru", "label"=>"N I K Baru","readonly"=>"",  "mandatory"=>"yes", "value"=>$qKontrakkerja->nik_baru));
          
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_awal,"/"), "first_selected"=>"yes"));
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_akhir,"/"), "first_selected"=>"yes"));
             $data["fields"][]      = form_text(array("name"=>"keterangan_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","readonly"=>"readonly","value"=> $qKontrakkerja->keterangan_kontrak));


       }

       
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
         $rules = array(
                      
                      'id_karyawan' => 'required'                     );

        $messages = ['id_karyawan.required' => 'Nama Karyawan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawankontrak/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawankontrak      = new KontrakKerjaModel;
            # ---------------
            $qKaryawankontrak->updateData($request);
            # ---------------
            session()->flash("success_message", "Karyawan kontrak has been updated");
            # ---------------
            return redirect("/karyawankontrak/index");
        }
    }




    public function delete($id) {
        $data["title"]         = "Delete Karyawankeluar";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/Karyawankontrak/remove";
        /* ----------
         Source
        ----------------------- */
        $qKaryawankeluar     = new KaryawankeluarModel;
         $qKaryawankeluar                 = $qKaryawankeluar->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_Karyawankontrak", "label"=>"Karyawankeluar ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_Karyawankontrak", "label"=>"Karyawankeluar", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawankeluar->nama_Karyawankontrak));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qKaryawankeluar     = new KaryawankeluarModel;
            # ---------------
            $qKaryawankeluar->removeData($request);
            # ---------------
            session()->flash("success_message", "Karyawankeluar has been removed");
        } else {
            session()->flash("error_message", "Karyawankeluar cannot be removed");
        }
      # ---------------
        return redirect("/Karyawankontrak/index"); 
    }

public function Cetak($id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCKontrakkerja     = new KontrakkerjaModel;
      /* ----------
        Source
      ----------------------- */
      //$idKontrakkerja     = explode("&", $id); 
      $qKontrakkerja        = $qCKontrakkerja->getCetakKontrak($id)->first();
      // $qNama                = $qCKontrakkerja->getCetakKontrak($id)->first();
      // $data["pkwt"]         =  $qKontrakkerja ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      $qKontrakkerja->gaji_pokok = $qKontrakkerja->gaji_pokok >0 ? 'Rp. '.setTitik($qKontrakkerja->gaji_pokok).',-' : "Rp. -";
      $qKontrakkerja->tunj_jabatan = $qKontrakkerja->tunj_jabatan >0 ? 'Rp. '.setTitik($qKontrakkerja->tunj_jabatan).',-' : "Rp. -";
      $qKontrakkerja->tunj_transport = $qKontrakkerja->tunj_transport >0 ? 'Rp. '.setTitik($qKontrakkerja->tunj_transport).',-' : "Rp. -";
      $qKontrakkerja->tunj_makan = $qKontrakkerja->tunj_makan >0 ? 'Rp. '.setTitik($qKontrakkerja->tunj_makan).',-/hari/kehadiran' : "Rp. -";
      $qKontrakkerja->tunj_kemahalan = $qKontrakkerja->tunj_kemahalan >0 ? 'Rp. '.setTitik($qKontrakkerja->tunj_kemahalan).',-' : "Rp. -";
      $qKontrakkerja->tunj_jaga = $qKontrakkerja->tunj_jaga >0 ? 'Rp. '.setTitik($qKontrakkerja->tunj_jaga).',-' : "Rp. -";
      $qKontrakkerja->tgl_lahir = date('d-m-Y', strtotime($qKontrakkerja->tgl_lahir));
      
      $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('temp.docx');
      $templateProcessor->setValue('nama', $qKontrakkerja->nama_karyawan);
      $templateProcessor->setValue('tempat_lahir', $qKontrakkerja->tempat_lahir);
      $templateProcessor->setValue('tgl_lahir', $qKontrakkerja->tgl_lahir);
      $templateProcessor->setValue('jenis_kelamin', $qKontrakkerja->jenis_kelamin);
      $templateProcessor->setValue('alamat', $qKontrakkerja->alamat);
      $templateProcessor->setValue('no_ktp', $qKontrakkerja->no_ktp);
      $templateProcessor->setValue('gaji_pokok', $qKontrakkerja->gaji_pokok);
      $templateProcessor->setValue('tunj_jabatan', $qKontrakkerja->tunj_jabatan);
      $templateProcessor->setValue('tunj_transport', $qKontrakkerja->tunj_transport);
      $templateProcessor->setValue('tunj_makan', $qKontrakkerja->tunj_makan);
      $templateProcessor->setValue('tunj_kemahalan', $qKontrakkerja->tunj_kemahalan);
      $templateProcessor->setValue('tunj_jaga', $qKontrakkerja->tunj_jaga);

      //menambahkan baris tabel dengan jumlah baris 2
      // $templateProcessor->cloneRow('userId', 2);
      // $values = [
      //             ['userId' => 1, 'userName' => 'Batman', 'userAddress' => 'Gotham City'],
      //             ['userId' => 2, 'userName' => 'Superman', 'userAddress' => 'Metropolis'],
      //           ];
      // foreach($values as $i => $v){
      //   $templateProcessor->setValue('userId#'.$v["userId"], $v["userName"]);
      // }
      // $templateProcessor->cloneRowAndSetValue('userId', $values);
      
      // $templateProcessor->cloneTable('h1_bug_table', 2);
      $templateProcessor->saveAs('MyWordFile.docx');
      
      $new_name_file = 'MyWordFile.docx';
      //Load temp file
      $tofolder = public_path().'/'.$new_name_file;
      // $phpWordpdf = \PhpOffice\PhpWord\IOFactory::load($tofolder); 

      // //Save it
      // $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWordpdf , 'PDF');
      // $xmlWriter->save('result.pdf');

      return response()->download($tofolder)
      ->deleteFileAfterSend(true)
      ;
      
      
    }

public function Cetak2( $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCKontrakkerja     = new KontrakkerjaModel;
      /* ----------
        Source
      ----------------------- */
      //$idKontrakkerja     = explode("&", $id); 
      $qKontrakkerja        = $qCKontrakkerja->getCetakKontrak($id);
      $qNama                = $qCKontrakkerja->getCetakKontrak($id)->first();
      $data["pkwt"]         =  $qKontrakkerja ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
       // return view('Personalia.lprkontrak',  $data);
      $pdf = PDF::loadView('Personalia.lprkontrak', $data);
      return $pdf->download('Kontrak '.$qNama->nama_karyawan.'.pdf');   

    }
    public function aktivasi($id) 
    {
        $data["title"]        = "Approval Proses";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawankontrak/updateaktivasi";
        $qMaster           = new MasterModel;
        $qKontrakkerja             = new KontrakkerjaModel;
        $qKontrakkerja        = $qKontrakkerja->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
    $qJabatan              = $qMaster->getSelectJabatan();
    
    $qStatuskerja          = getSelectStskerja();
        
        /* ----------
         Fields
        ----------------------- */
        if($qKontrakkerja->status_kontrak=="reguler")
        {
            $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Kontrakkerja", "readonly"=>"readonly", "value"=>$id));
            $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
            $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qKontrakkerja->id_karyawan));
            $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes","readonly"=>"readonly","value"=>$qKontrakkerja->nik));
            
            $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKontrakkerja->nama_karyawan));
          
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_awal,"/"), "first_selected"=>"yes"));
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_akhir,"/"), "first_selected"=>"yes"));
             $data["fields"][]      = form_text(array("name"=>"keterangan_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","readonly"=>"readonly","value"=> $qKontrakkerja->keterangan_kontrak));
       }
       else
       {
            $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Kontrakkerja", "readonly"=>"readonly", "value"=>$id));
            $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
            $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qKontrakkerja->id_karyawan));
            $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes","readonly"=>"readonly","value"=>$qKontrakkerja->nik));
            
            $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKontrakkerja->nama_karyawan));
                    $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar","readonly"=>"readonly", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_keluar,"/")));
      

       $data["fields"][]      = form_text(array("name"=>"nik_baru", "label"=>"N I K Baru","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qKontrakkerja->nik_baru));
          
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_awal,"/"), "first_selected"=>"yes"));
            $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_akhir,"/"), "first_selected"=>"yes"));
             $data["fields"][]      = form_text(array("name"=>"keterangan_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","readonly"=>"readonly","value"=> $qKontrakkerja->keterangan_kontrak));


       }

           

        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);    
            

    }

public function updateaktivasi(Request $request)
    {
        
        
        $rules = array(
                    'tgl_awal' => 'required|',
                    
                    'tgl_akhir' => 'required|'                          
        );

        $messages = [
                    'tgl_awal.required' => 'Tanggal awal harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawankontrak/aktifasi/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKontrakkerja      = new KontrakkerjaModel;
            # ---------------
            $qKontrakkerja->updateaktivasiData($request);
            # ---------------
            session()->flash("success_message", "Kontrakkerja has been updated");
            # ---------------
            return redirect("/karyawankontrak/index");
        }
    }

/*
  Jatuh Tempo
*/
  public function jtk_index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawankontrak/jtk_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawankontrak       = new KontrakkerjaModel;
        # ---------------
        //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

      $data["tabs"]          = array(array("label"=>"Kontrak Baru", "url"=>"/karyawankontrak/index", "active"=>"")
                                       ,array("label"=>"Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/jtk_index", "active"=>"active")
                                         ,array("label"=>"Akan Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/akanjtk_index", "active"=>"")
                                         ,array("label"=>"Jatuh Tempo Percobaan", "url"=>"/karyawankontrak/percobaan_jt_index", "active"=>""));

         $data["form_act_edit"]    = "/karyawankontrak/perpanjanan_kontrak";
        // $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>""));
        //   $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>""));
        //   $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>""));
          //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          
        /* ----------
         Table header
        ----------------------- */

        
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                       array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                     
                        array("label"=>"Nama"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                         array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                        
                                
                                     
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Keterangan Kontrak"
                                                ,"name"=>"keterangan_kontrak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>"")

                                        );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_JTK_KONTRAK" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JTK_KONTRAK");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JTK_KONTRAK");
        }
        # ---------------
        $data["select"]        = $qKaryawankontrak-> getList_jtk($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawankontrak-> getList_jtk($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.jtklist", $data);
    }

     public function akanjtk_index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawankontrak/akanjtk_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawankontrak       = new KontrakkerjaModel;
        # ---------------
        //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

      $data["tabs"]          = array(array("label"=>"Kontrak Baru", "url"=>"/karyawankontrak/index", "active"=>"")
                                       ,array("label"=>"Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/jtk_index", "active"=>"")
                                         ,array("label"=>"Akan Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/akanjtk_index", "active"=>"active")
                                         ,array("label"=>"Jatuh Tempo Percobaan", "url"=>"/karyawankontrak/percobaan_jt_index", "active"=>""));

         $data["form_act_edit"]    = "/karyawankontrak/perpanjanan_kontrak";
        // $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>""));
        //   $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>""));
        //   $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>""));
          //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          
        /* ----------
         Table header
        ----------------------- */

        
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                       array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                     
                        array("label"=>"Nama"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                         array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                        
                                
                                     
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Keterangan Kontrak"
                                                ,"name"=>"keterangan_kontrak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>"")

                                        );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_AJTK_KONTRAK" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_AJTK_KONTRAK");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_AJTK_KONTRAK");
        }
        # ---------------
        $data["select"]        = $qKaryawankontrak-> getList_akanjtk($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawankontrak-> getList_akanjtk($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.jtklist", $data);
    }



    public function percobaan_jt_index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawankontrak/percobaan_jt_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawankontrak       = new KontrakkerjaModel;
        # ---------------
        //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

      $data["tabs"]          = array(array("label"=>"Kontrak Baru", "url"=>"/karyawankontrak/index", "active"=>"")
                                       ,array("label"=>"Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/jtk_index", "active"=>"")
                                         ,array("label"=>"Akan Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/akanjtk_index", "active"=>"")
                                         ,array("label"=>"Jatuh Tempo Percobaan", "url"=>"/karyawankontrak/percobaan_jt_index", "active"=>"active"));

         $data["form_act_edit"]    = "/karyawankontrak/perpanjanan_kontrak";
        // $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>""));
        //   $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>""));
        //   $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>""));
          //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          
        /* ----------
         Table header
        ----------------------- */

        
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                       array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                     
                        array("label"=>"Nama"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                         array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                        
                                
                                     
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_percobaan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Keterangan Kontrak"
                                                ,"name"=>"keterangan_kontrak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>"")

                                        );
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PERCOBAAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERCOBAAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERCOBAAN");
        }
        # ---------------
        $data["select"]        = $qKaryawankontrak-> getList_percobaanjt($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawankontrak-> getList_percobaanjt($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.jtklist", $data);
    }

    public function perpanjanan_kontrak($id) {
        $data["title"]         = "Add PKWT Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawankontrak/save";
       
       
       // $qKaryawankeluar            = new KaryawankeluarModel;

       
    $qMaster               = new MasterModel;
       // $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
    $qJabatan                  = $qMaster->getSelectJabatan(); 
    $qStatuskerja              = getSelectStskerja();
        $qKontrakkerjamdl      = new KontrakkerjaModel;
        $qKontrakkerjamdl       = $qKontrakkerjamdl->getProfileKry($id)->first();
        $qListKry              = $qMaster->getSelectKaryawankontrak1();
        $qKeterangan_kontrak   ="";
          $tgl_akhir  = date("Y-m-d",strtotime("+1 years",strtotime($qKontrakkerjamdl->tgl_akhir)));
          $tgl_percobaan=$qKontrakkerjamdl->Tanggal_percobaan;
          $tgl_akhirkontrak1 =$qKontrakkerjamdl->tanggal_akhir;
        if($qKontrakkerjamdl->keterangan_kontrak=="PERCOBAAN")
        {
                $qKeterangan_kontrak   ="KONTRAK 1";
                 $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry,"value"=>$id));
                $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=> displayDMY($tgl_percobaan,"/")));
                $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=> displayDMY($tgl_akhirkontrak1,"/"), "first_selected"=>"yes"));
                $data["fields"][]      = form_text(array("name"=>"keterangan_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","value"=> "KONTRAK 1"));
                 $data["fields"][]      = form_text(array("name"=>"status_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","value"=> "reguler"));

                # ---------------
        }
        else if($qKontrakkerjamdl->keterangan_kontrak=="KONTRAK 1" or $qKontrakkerjamdl->keterangan_kontrak=="KONTRAK1")
        {
		
             $qKeterangan_kontrak   ="KONTRAK 2";
              $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry,"value"=>$id));
                $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=> displayDMY($qKontrakkerjamdl->tgl_akhir,"/")));
                $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=> displayDMY($tgl_akhir,"/"), "first_selected"=>"yes"));
                $data["fields"][]      = form_text(array("name"=>"keterangan_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","value"=> "KONTRAK 2"));
                 $data["fields"][]      = form_text(array("name"=>"status_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","value"=> "reguler"));
		
                # ---------------

        }

        else if($qKontrakkerjamdl->keterangan_kontrak=="KONTRAK 2" or $qKontrakkerjamdl->keterangan_kontrak=="KONTRAK2")
        {
                
		$qKeterangan_kontrak   ="KONTRAK 1";
                 $qListKry              = $qMaster->getSelectKaryawankontrak2();
                 $tgl_awal  = date("Y-m-d",strtotime("+1 months",strtotime($qKontrakkerjamdl->tgl_akhir)));
                 $tgl_akhir  = date("Y-m-d",strtotime("+1 years",strtotime($tgl_awal)));


         
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan","value"=>$id,  "source"=>$qListKry));
      
        $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar","readonly"=>"readonly", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerjamdl->tgl_akhir,"/")));
      

       $data["fields"][]      = form_text(array("name"=>"nik_baru", "label"=>"N I K Baru","readonly"=>"",  "mandatory"=>"yes", "value"=>""));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=> displayDMY($tgl_awal,"/"), "first_selected"=>"yes"));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=> displayDMY($tgl_akhir,"/"), "first_selected"=>"yes"));


        $data["fields"][]      = form_text(array("name"=>"status_kontrak", "label"=>"Keterangan", "mandatory"=>"yes","value"=> "perubahan"));
        
		


        }

        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Save"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));


      
     
       
        # ---------------
        return view("default.form", $data);
    }
    public function inq_index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawankontrak/inq_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawankontrak       = new KontrakkerjaModel;

        # ---------------
        //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

      $data["tabs"]          = array(array("label"=>"Kontrak Baru", "url"=>"/karyawankontrak/index", "active"=>"")
                                       ,array("label"=>"Jatuh Tempo Kontrak", "url"=>"/karyawankontrak/jtk_index", "active"=>"")
                                         ,array("label"=>"Inquery Kontrak", "url"=>"/karyawankontrak/inq_index", "active"=>"active"));

         $data["form_act_edit"]    = "/karyawankontrak/perpanjanan_kontrak";
        
        $qMaster               = new MasterModel;
        $qListKry              = $qMaster->getSelectKaryawankontrak();
        $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
        $qListKry            = array_merge($collection,$qListKry);

        $qCabang                = $qMaster->getSelectCabang();
        $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
         $qCabang            = array_merge($collection,$qCabang);
        
        //$qViewKaryawan        = $qKontrakkerjamdl->getProfileKry($idkry)->first();  
       // $qViewFormatsurat     = $qKontrakkerjamdl->getProfileKry($idkry)->first(); 
             
         $data["fields"][]      = form_datepicker2(array("name"=>"bulan", "label"=>"Bulan", "mandatory"=>"","value"=>date("m/Y"), "first_selected"=>"yes"));
       
         $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang));
           $data["form_act_cari"]    = "/karyawankontrak/inq_index";
            $data["form_act_all"]    = "/karyawankontrak/inq_index";
      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));
          //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          
        /* ----------
         Table header
        ----------------------- */

        
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                       array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                        array("label"=>"Nama"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                         array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"normal"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                        
                                
                                     
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>"")
                                        );
        # ---------------
        
        if($request->has('bulan')) {
            session(["SES_SEARCH_BULAN" => $request->input("bulan")]);
            # ---------------
            $data["bulan"]   = $request->session()->get("SES_SEARCH_BULAN");
        } else {
            $data["bulan"]   = $request->session()->get("SES_SEARCH_BULAN");
        }

        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
        }

        if($request->has('text_search')) {
            session(["SES_SEARCH_KONTRAKALL" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAKALL");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAKALL");
        }
        # ---------------
     

        $data["select"]        = $qKaryawankontrak-> getList_kontrak($request, $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawankontrak-> getList_kontrak($request);
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }
    
}
