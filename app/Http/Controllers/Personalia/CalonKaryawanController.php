<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Personalia\CalonKaryawanModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Payroll\InputgajiModel;
use App\Model\Master\UserModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use App\Model\Master\ProfilesystemModel;

class CalonKaryawanController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }


    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawan/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawan              = new CalonKaryawanModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                      array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                    array("label"=>"Status"
                                    ,"name"=>"status_kerja"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"15%"
                                                ,"add-style"=>""),
                                    array("label"=>"Tanggal Melamar"
                                    ,"name"=>"tgl_melamar"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"15%"
                                                ,"add-style"=>"")
                                               );
          
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KARYAWAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWAN");
        }
        # ---------------
        $data["select"]        = $qKaryawan->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawan->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Calon Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkaryawan/save";
        $data["url_select"]    = "/calonkaryawan/get_profile/";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster               = new MasterModel;
        $qKaryawan  = new CalonKaryawanModel;
        $qProfilSystem  = new ProfilesystemModel;
        $getProfileSystem       = $qProfilSystem->getProfile(1)->first();
        /* ----------
         Source
        ----------------------- */
        $new_hire            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'New Hire'
                                    ]
                                ];
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qKaryawanNonAktif      = $qMaster->getSelectKaryawanNonAktif();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qPtkp                  = $qMaster->getSelectPtkp();
        $qGolDarah              = getSelectGolonganDarah();
        $idKaryawan             = $qKaryawan->lastIdKaryawan();
        $idCalonKaryawan        = $qKaryawan->lastIdCalonKaryawan();
        $lastidK                = $getProfileSystem->no_urut;
        $newIdk                 = $lastidK+1;
        $karakterid             = "00000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
	$qBank                = $qMaster->getSelectBank();
        // DB::table("m_profilesystem")->where("id_profilesystem", 1)->update([ "no_urut"=> setString($newIdk)]);
        
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID", "mandatory"=>"yes", "value"=>$newIdk));
       $data["fields"][]      = form_select(array("name"=>"id_karyawan_non_aktif", "label"=>"RO", "source"=>array_merge($new_hire, $qKaryawanNonAktif)));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
       $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes"));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku_ktp", "label"=>"Tanggal Berlaku KTP", "value"=>date("d/m/Y"), "first_selected"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"no_kk", "label"=>"No Kartu Keluarga", "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_panggilan", "label"=>"Nama Panggilan", "mandatory"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "withnull"=>"withnull", "mandatory"=>"yes",  "source"=>$qJabatan));
    //    $data["fields"][]      = form_text(array("name"=>"perusahaan", "label"=>"Perusahaan", "mandatory"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qCabang));
       $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qPendidikan));
       $data["fields"][]      = form_text(array("name"=>"jurusan", "label"=>"Jurusan"));
       $data["fields"][]      = form_select(array("name"=>"status_nikah", "label"=>"Status Nikah", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qStatusNikah));
       //$data["fields"][]      = form_select(array("name"=>"id_grading", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan));
        $data["fields"][]      = form_text(array("name"=>"nonpwp", "label"=>"No NPWP",  "mandatory"=>""));
        //$data["fields"][]      = form_select(array("name"=>"id_ptkp", "label"=>"PTKP Status", "withnull"=>"withnull", "mandatory"=>"yes",  "source"=>$qPtkp));
        $data["fields"][]      = form_text(array("name"=>"nobpjskes", "label"=>"No BPJS Kesehatan",  "mandatory"=>""));
        $data["fields"][]      = form_text(array("name"=>"nobjstk", "label"=>"No BPJS Tenanaga Kerja",  "mandatory"=>""));
  
        $data["fields"][]      = form_select(array("name"=>"id_bank", "label"=>"Bank", "withnull"=>"withnull", "source"=>$qBank));
	$data["fields"][]      = form_text(array("name"=>"no_rekening", "label"=>"No Rekening",  "mandatory"=>""));
       	$data["fields"][]      = form_text(array("name"=>"asrekening", "label"=>"Atas Nama Rekening",  "mandatory"=>""));
      
    //    $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Email"));
       $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"Data Personal"));
       $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>"yes"));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamin", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qJK));
       $data["fields"][]      = form_select(array("name"=>"agama", "label"=>"Agama", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qAgama ));
       $data["fields"][]      = form_select(array("name"=>"golongan_darah", "label"=>"Golongan Darah", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qGolDarah));
        $data["fields"][]      = form_textarea(array("name"=>"alamat", "label"=>"Alamat Tinggal", "mandatory"=>"yes", "row"=>"2"));
        $data["fields"][]      = form_text(array("name"=>"nama_ibu", "label"=>"Nama Ibu"));

        $data["fields"][]      = form_text(array("name"=>"no_telpon", "label"=>"No Telpon", "mandatory"=>""));
       $data["fields"][]      = form_text4(array("name"=>"hp", "label"=>"hp", "mandatory"=>"yes"));
       $data["fields"][]      = form_hidden(array("name"=>"password", "label"=>"password", "mandatory"=>"yes","value"=>"password"));
       
  # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form_calon", $data);
    }

    public function save(Request $request) 
    {   
        $no_ktp = str_replace('.', '', $request->no_ktp);
        $cek = DB::table('p_karyawan')->where([['no_ktp', $no_ktp],['aktif', 1]])->first();
        $cek2 = DB::table('p_karyawan')->where([['no_ktp', $request->no_ktp],['aktif', 1]])->first();
        if($cek != null){
            return redirect("/calonkaryawan/add")
                ->withErrors(['No KTP existing'])
                ->withInput();
        }else if($cek2 != null){
            return redirect("/calonkaryawan/add")
                ->withErrors(['No KTP existing'])
                ->withInput();
        }

        $rules = array(
                        'id_karyawan' => 'unique:p_calon_karyawan',
                        // 'id_karyawan' => 'unique:p_karyawan',
                        'email' => 'nullable|unique:sys_users',
                        // 'no_ktp' => 'nullable|unique:p_karyawan',
                        // 'no_rekening' => 'nullable|unique:p_karyawan',
                        // 'id_departemen' => 'required|id_departemen:1',
                        'nama_karyawan' => 'required', 
                        // 'id_departemen' => 'required_unless:0' 
                        );

        $messages = [
                    'id_karyawan.unique' => 'ID sudah digunakan',
                    'email.unique' => 'Email sudah digunakan',
                    // 'no_ktp.unique' => 'No KTP sudah digunakan',
                    // 'no_rekening.unique' => 'No Rekening sudah digunakan',
                    // 'id_departemen.required' => 'Pilih',
                    'nama_karyawan.required' => 'Karyawan harus diisi',
                    // 'id_departemen.required_unless:0' => 'Karyawan harus diisi',
                    ];
        // dd($request->all(),$request->jenis_kelamin == 0);
        $validator = Validator::make($request->all(), $rules, $messages);
        if($request->id_departemen == 0){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Departemen"])->withInput();}else
        if($request->id_jabatan == 0){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Jabatan"])->withInput();}else
        if($request->id_cabang == 0){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Cabang"])->withInput();}else
        if($request->id_pendidikan == "0"){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Pendidikan"])->withInput();}else
        if($request->status_nikah == "0") { return redirect("/calonkaryawan/add")->withErrors(["Pilih Status Nikah"])->withInput();}else
        //if($request->id_ptkp == 0){  return redirect("/calonkaryawan/add")->withErrors(["Pilih PTKP Status"])->withInput();}else
        if($request->jenis_kelamin == "0"){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Jenis Kelamin"])->withInput();}else
        if($request->agama == "0"){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Agama"])->withInput();}else
        if($request->golongan_darah == "0"){  return redirect("/calonkaryawan/add")->withErrors(["Pilih Golongan Darah"])->withInput();}
        
        if ($validator->fails()) {
            return redirect("/calonkaryawan/add")
                ->withErrors($validator)
                ->withInput();
        } else 
        {
   // DB::beginTransaction();
     //       try{



            $qKaryawan  = new CalonKaryawanModel;
            $qUser      = new UserModel;
            
            $qKaryawan->createData($request);
            // $qUser->createData($request);
            session()->flash("success_message", "Karyawan has been saved");
          //  DB::commit();

       ///  } catch (\Exception $e) {
              //  DB::rollback();
                
            //}

            # ---------------
            return redirect("/calonkaryawan/index");
        }
    }

    public function edit($id) 
    {
        $data["title"]        = "Edit Calon Karyawan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/calonkaryawan/update";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan            = new CalonKaryawanModel;
        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        // dd($qKaryawan);
        
        if($qKaryawan->status_kerja == 'PROSES' || $qKaryawan->status_kerja == 'AKTIF'){
            return redirect("/calonkaryawan/index")
                ->withErrors(['pengajuan sudah diterima dan tidak bisa di edit'])
                ->withInput();    
        }else{
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qGolDarah              = getSelectGolonganDarah();
         $qPtkp                 = $qMaster->getSelectPtkp();

        $newIdk                 = $id; 
        $karakterid             = "00000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
	$qBank                = $qMaster->getSelectBank();
         // dd($qKaryawan->id_pendidikan);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "value"=>$newIdk));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
    //    $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qKaryawan->nik));
       $data["fields"][]      =  form_text(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "readonly"=>"readonly", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_melamar,"/")));
       $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes", "value"=>$qKaryawan->no_ktp));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku_ktp", "label"=>"Tanggal Berlaku KTP", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_berlaku_ktp,"/")));
       $data["fields"][]      = form_text(array("name"=>"no_kk", "label"=>"No Kartu Keluarga", "mandatory"=>"yes", "value"=>$qKaryawan->no_kk));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan",  "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_text(array("name"=>"nama_panggilan", "label"=>"Nama Panggilan",  "mandatory"=>"yes", "value"=>$qKaryawan->nama_panggilan));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
    //    $data["fields"][]      = form_text(array("name"=>"perusahaan", "label"=>"Perusahaan", "mandatory"=>"yes", "value"=>$qKaryawan->perusahaan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "withnull"=>"withnull", "mandatory"=>"yes","source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "withnull"=>"withnull", "mandatory"=>"yes", "value"=>$qKaryawan->id_pendidikan,"source"=>$qPendidikan));
       $data["fields"][]      = form_text(array("name"=>"jurusan", "label"=>"Jurusan",  "mandatory"=>"", "value"=>$qKaryawan->jurusan));
       $data["fields"][]      = form_select(array("name"=>"status_nikah", "label"=>"Status", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qStatusNikah,"value"=>$qKaryawan->status_nikah));
        $data["fields"][]      = form_text(array("name"=>"nonpwp", "label"=>"No NPWP",  "mandatory"=>"", "value"=>$qKaryawan->nonpwp));
        //$data["fields"][]      = form_select(array("name"=>"id_ptkp", "label"=>"PTKP Status", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qPtkp, "value"=>$qKaryawan->id_ptkp));
        $data["fields"][]      = form_text(array("name"=>"nobpjskes", "label"=>"No BPJS Kesehatan",  "mandatory"=>"", "value"=>$qKaryawan->nobpjskes));
        $data["fields"][]      = form_text(array("name"=>"nobjstk", "label"=>"No BPJS Tenanaga Kerja",  "mandatory"=>"", "value"=>$qKaryawan->nobjstk));
  

       $data["fields"][]      = form_select(array("name"=>"id_bank", "label"=>"Bank", "withnull"=>"withnull", "source"=>$qBank,"value"=>$qKaryawan->id_bank));
       $data["fields"][]      = form_text(array("name"=>"no_rekening", "label"=>"No Rekening",  "mandatory"=>"", "value"=>$qKaryawan->no_rekening));
       $data["fields"][]      = form_text(array("name"=>"asrekening", "label"=>"Atas Nama Rekening",  "mandatory"=>"", "value"=>$qKaryawan->anrekening));
    //    $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>"yes", "value"=>$qKaryawan->noabsen));
       $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Email", "value"=>$qKaryawan->email));
       $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"BIODATA KARYAWAN"));
    //    $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes", "value"=>$qKaryawan->no_ktp));
       $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>"yes","value"=>$qKaryawan->tempat_lahir));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "withnull"=>"withnull", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_lahir,"/")));
       $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamin", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qJK,"value"=>$qKaryawan->jenis_kelamin));
       $data["fields"][]      = form_select(array("name"=>"agama", "label"=>"Agama", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qAgama,"value"=>$qKaryawan->agama));
       $data["fields"][]      = form_select(array("name"=>"golongan_darah", "label"=>"Golongan Darah", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qGolDarah, "value"=>$qKaryawan->golongan_darah));
    //    $data["fields"][]      = form_textarea(array("name"=>"alamat_valid", "label"=>"Alamat KTP", "mandatory"=>"yes", "row"=>"2","value"=>$qKaryawan->alamat_valid));
       $data["fields"][]      = form_textarea(array("name"=>"alamat", "label"=>"Alamat Tinggal", "mandatory"=>"yes", "row"=>"2","value"=>$qKaryawan->alamat));
       $data["fields"][]      = form_text(array("name"=>"nama_ibu", "label"=>"Nama Ibu", "value"=>$qKaryawan->nama_ibu));
       $data["fields"][]      = form_text(array("name"=>"no_telpon", "label"=>"No Telpon", "mandatory"=>"","value"=>$qKaryawan->no_telpon));
       $data["fields"][]      = form_text4(array("name"=>"hp", "label"=>"hp", "mandatory"=>"yes","value"=>$qKaryawan->hp));
       
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
        }
    
    }
    public function update(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($request->id_departemen == 0){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Departemen"])->withInput();}else
        if($request->id_jabatan == 0){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Jabatan"])->withInput();}else
        if($request->id_cabang == 0){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Cabang"])->withInput();}else
        if($request->id_pendidikan == "0"){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Pendidikan"])->withInput();}else
        if($request->status_nikah == "0") { return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Status Nikah"])->withInput();}else
        //if($request->id_ptkp == 0){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih PTKP Status"])->withInput();}else
        if($request->jenis_kelamin == "0"){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Jenis Kelamin"])->withInput();}else
        if($request->agama == "0"){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Agama"])->withInput();}else
        if($request->golongan_darah == "0"){  return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))->withErrors(["Pilih Golongan Darah"])->withInput();}
        
        if ($validator->fails())
         {
            return redirect("/calonkaryawan/edit/" . $request->input("id_karyawan"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new CalonKaryawanModel;
            $qUser      = new UserModel;
           
            # ---------------
            $qKaryawan->updateData($request);
            // $qUser->updateDatakry($request);
            
           return redirect("/calonkaryawan/index");

        }

    }
   
    public function ajukan($id) {
        $data["title"]         = "Ajukan Calon Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkaryawan/prosesajukan";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new CalonKaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
       /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_text(array("name"=>"nama_panggilan", "label"=>"Nama Panggilan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_panggilan));
       $data["fields"][]      =  form_text(array("name"=>"tgl_pengajuan", "label"=>"Tanggal Pengajuan", "readonly"=>"readonly","mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_melamar,"/")));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Ajukan"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }
  
    public function prosesajukan(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
         {
            return redirect("/calonkaryawan/ajukan/" . $request->input("id_karyawan"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new CalonKaryawanModel;
            # ---------------
            $qKaryawan->prosesAjukan($request);
           return redirect("/calonkaryawan/index");

        }

    }

    public function approve($id) {
        $data["title"]         = "Approve Calon Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkaryawan/prosesapprove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new CalonKaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $statusKerja            = getSelectStskerja();
       /* ----------
         Fields
        ----------------------- */
        if($qKaryawan->status_kerja == 'CALON'){
            return redirect("/calonkaryawan/index")
                ->withErrors(['Status calon karyawan belum proses'])
                ->withInput();    
        }else{
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_text(array("name"=>"nama_panggilan", "label"=>"Nama Panggilan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_panggilan));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_masuk", "label"=>"Tanggal Terima", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_masuk,"/")));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       $data["fields"][]      = form_select(array("name"=>"status_kerja", "label"=>"Status Karyawan", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$statusKerja,"value"=>$qKaryawan->status_kerja));
       $data["fields"][]      = form_text(array("name"=>"lama_kontrak_percobaan", "label"=>"Lama Kontrak/Percobaan (bulan)", "mandatory"=>"yes", "value"=>$qKaryawan->lama_kontrak_percobaan));
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Approve"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        }
    }

    public function prosesapprove(Request $request)
    {
        $rules = array(
                    'nama_karyawan' => 'required|'   ,    
                    'tgl_masuk' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi',
                    'tgl_masuk.required' => 'Tanggal Terima harus diisi'

        ];

        //  $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
        //               'Alamat.required' => 'Alamat harus diisi',
        //               'Telp.required' => 'Telp harus diisi',
        //               'PIC.required'=>'PIC harus diisi',    ];
        if($request->status_kerja == "0"){
            return redirect("/calonkaryawan/approve/" . $request->input("id_karyawan"))
                ->withErrors(["Pilih Status Karyawan"])
                ->withInput();
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
         {
            return redirect("/calonkaryawan/approve/" . $request->input("id_karyawan"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new CalonKaryawanModel;
            $qUser      = new UserModel;
            # ---------------
            $qKaryawan->prosesApprove($request);
            $qUser->createData($request);
            return redirect("/calonkaryawan/index");

        }

    }

    public function tolak($id) {
        $data["title"]         = "Return Calon Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkaryawan/prosestolak";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new CalonKaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
       /* ----------
         Fields
        ----------------------- */
        if($qKaryawan->status_kerja != 'PROSES' ){
            return redirect("/calonkaryawan/index")
                ->withErrors(['Status calon karyawan masih '.$qKaryawan->status_kerja])
                ->withInput();    
        }else{
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_text(array("name"=>"nama_panggilan", "label"=>"Nama Panggilan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_panggilan));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_melamar,"/")));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       $data["fields"][]      = form_text(array("name"=>"alasan_reject", "label"=>"Alasan", "mandatory"=>"yes","value"=>$qKaryawan->alasan_reject));
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Return"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        }
    }
  
    public function prosestolak(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails())
         {
            return redirect("/calonkaryawan/tolak/" . $request->input("id_karyawan"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new CalonKaryawanModel;
            $qUser      = new UserModel;
            # ---------------
            $qKaryawan->prosesTolak($request);
            //$qUser->prosesTolak($request);
           return redirect("/calonkaryawan/index");

        }

    }

    public function view($id) 
    {
        $data["title"]        = "View Calon Karyawan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/calonkaryawan/cetak/".$id;
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan            = new CalonKaryawanModel;
        $qKaryawan     = $qKaryawan->getProfile($id)->first();
      
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qGolDarah              = getSelectGolonganDarah();
         $qPtkp                 = $qMaster->getSelectPtkp();

        $newIdk                 = $id; 
        $karakterid             = "0000000000000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$newIdk));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
    //    $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qKaryawan->nik));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_melamar,"/")));
       $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes", "value"=>$qKaryawan->no_ktp));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku_ktp", "label"=>"Tanggal Berlaku KTP", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_berlaku_ktp,"/")));
       $data["fields"][]      = form_text(array("name"=>"no_kk", "label"=>"No Kartu Keluarga", "mandatory"=>"yes", "value"=>$qKaryawan->no_kk));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan",  "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
    //    $data["fields"][]      = form_text(array("name"=>"perusahaan", "label"=>"Perusahaan", "mandatory"=>"yes", "value"=>$qKaryawan->perusahaan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "value"=>$qKaryawan->id_pendidikan,"source"=>$qPendidikan));
       $data["fields"][]      = form_select(array("name"=>"status_nikah", "label"=>"Status", "mandatory"=>"yes", "source"=>$qStatusNikah,"value"=>$qKaryawan->status_nikah));
        $data["fields"][]      = form_text(array("name"=>"nonpwp", "label"=>"No NPWP",  "mandatory"=>"", "value"=>$qKaryawan->nonpwp));
        //$data["fields"][]      = form_select(array("name"=>"id_ptkp", "label"=>"PTKP Status", "mandatory"=>"yes", "source"=>$qPtkp, "value"=>$qKaryawan->id_ptkp));
        $data["fields"][]      = form_text(array("name"=>"nobpjskes", "label"=>"No BPJS Kesehatan",  "mandatory"=>"", "value"=>$qKaryawan->nobpjskes));
        $data["fields"][]      = form_text(array("name"=>"nobjstk", "label"=>"No BPJS Tenanaga Kerja",  "mandatory"=>"", "value"=>$qKaryawan->nobjstk));
  

       $data["fields"][]      = form_text(array("name"=>"no_rekening", "label"=>"No Rekening",  "mandatory"=>"", "value"=>$qKaryawan->no_rekening));
       $data["fields"][]      = form_text(array("name"=>"asrekening", "label"=>"Atas Nama Rekening",  "mandatory"=>"", "value"=>$qKaryawan->anrekening));
    //    $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>"yes", "value"=>$qKaryawan->noabsen));
       $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Email", "value"=>$qKaryawan->email));
       $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"BIODATA KARYAWAN"));
    //    $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes", "value"=>$qKaryawan->no_ktp));
       $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>"yes","value"=>$qKaryawan->tempat_lahir));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_lahir,"/")));
       $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamin", "mandatory"=>"yes", "source"=>$qJK,"value"=>$qKaryawan->jenis_kelamin));
       $data["fields"][]      = form_select(array("name"=>"agama", "label"=>"Agama", "mandatory"=>"yes", "source"=>$qAgama,"value"=>$qKaryawan->agama));
       $data["fields"][]      = form_select(array("name"=>"golongan_darah", "label"=>"Golongan Darah", "mandatory"=>"yes", "source"=>$qGolDarah, "value"=>$qKaryawan->golongan_darah));
    //    $data["fields"][]      = form_textarea(array("name"=>"alamat_valid", "label"=>"Alamat KTP", "mandatory"=>"yes", "row"=>"2","value"=>$qKaryawan->alamat_valid));
       $data["fields"][]      = form_textarea(array("name"=>"alamat", "label"=>"Alamat Tinggal", "mandatory"=>"yes", "row"=>"2","value"=>$qKaryawan->alamat));
       $data["fields"][]      = form_text(array("name"=>"no_telpon", "label"=>"No Telpon", "mandatory"=>"","value"=>$qKaryawan->no_telpon));
       $data["fields"][]      = form_text4(array("name"=>"hp", "label"=>"hp", "mandatory"=>"yes","value"=>$qKaryawan->hp));
       
       # ---------------
       $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cetak"));
       $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
    
    }

 public function resign($id) {
        $data["title"]         = "Karyawan Keluar";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawan/updateresign";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new CalonKaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->nama_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_keluar,"/")));
       $data["fields"][]      = form_text(array("name"=>"alasan_keluar", "label"=>"Alasan Keluar", "mandatory"=>"yes", "value"=>$qKaryawan->alasan_keluar));
      
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

public function updateresign(Request $request)
    {
        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawan/resign/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawan      = new CalonKaryawanModel;
            # ---------------
            $qKaryawan->updateresignData($request);
            # ---------------
            session()->flash("success_message", "Karyawan has been resigned");
            # ---------------
            return redirect("/karyawan/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkaryawan/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new CalonKaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
       /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "mandatory"=>"yes","value"=> $qKaryawan->tgl_masuk));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_text(array("name"=>"nama_panggilan", "label"=>"Nama Panggilan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_panggilan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        // if($request->input("id") != 1) {
            $qKaryawan     = new CalonKaryawanModel;
            # ---------------
            $qKaryawan->removeData($request);
            # ---------------
            session()->flash("success_message", "Karyawan has been removed");
        // } else {
        //     session()->flash("error_message", "Karyawan cannot be removed");
        // }
      # ---------------
        return redirect("/calonkaryawan/index"); 
    }

    //Cetak CV

    public function cetakcv($id) 
    {

        $qMaster              = new MasterModel;
        $qKaryawanm            = new CalonKaryawanModel;
        $qKaryawan             = $qKaryawanm->getProfile($id)->first();
        $qKeluarga             = $qKaryawanm->getDetailKeluarga($id);
        $qKontak               = $qKaryawanm->getDetailKontak($id);
        $qPendidikan           = $qKaryawanm->getDetailPendidikan($id);
        $qPengalaman           = $qKaryawanm->getDetailPengalaman($id);
      /*
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = getSelectPendidikan();
       
       */ 
        $Line                  = 185;
        Fpdf::AddPage();
            
            
            $logo = public_path("logo/logo-bsm.png");
            Fpdf::Image($logo, 25, 18, 30, 22);
            Fpdf::SetLineWidth(0.3);
            Fpdf::SetFont("Arial", "B", 14);
            Fpdf::Cell(185, 40, "PT. BUANA SEJAHTERA MULTI DANA", 0, 0, 'C');
            Fpdf::Ln(5);
            Fpdf::SetLineWidth(0.3);
            Fpdf::SetFont("Arial", "B", 12);
            Fpdf::Cell(185, 50, 'DATA PERSONAL KARYAWAN', 0, 0, 'C');
            Fpdf::Ln(40);
            Fpdf::SetFont("Arial", "", 10);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'N I K', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nik, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Nama Karyawan', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_karyawan, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Cabang', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_cabang, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Departemen', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_departemen, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Jabatan', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_jabatan, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Tgl. Masuk', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, displayDMY($qKaryawan->tgl_masuk,"/"), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Status Kerja', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->status_kerja, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'No KTP', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
           Fpdf::Cell(65, 5, setString($qKaryawan->no_ktp), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Jenis Kelamin', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->jenis_kelamin, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Tempat Lahir', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->tempat_lahir, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Tgl. Lahir', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65,5, displayDMY($qKaryawan->tgl_lahir,"/"), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Agama', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->agama, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Pendidikan', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65,5, $qKaryawan->id_pendidikan, 0, 0, 'L');
            Fpdf::Ln(5);
           Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Status', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65,5, $qKaryawan->status_nikah, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Alamat', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            if (strlen($qKaryawan->alamat) > 0) {
                    
                    Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)), 0, 75), 0, 0, 'L');
                    Fpdf::Ln(5);
                }
                if (strlen($qKaryawan->alamat) > 75) {
                    
                    Fpdf::Cell(65, 5, '', 0, 0, 'R');
                    Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)), 75, 150), 0, 0, 'L');
                    Fpdf::Ln(5);
                }
                if (strlen($qKaryawan->alamat) > 150) {
                  
                    Fpdf::Cell(65, 5, '', 0, 0, 'R');
                    Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)),150, 225), 0, 0, 'L');
                    Fpdf::Ln(5);
                }
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'No Telpon', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->no_telpon, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'No. HP', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->hp, 0, 0, 'L');
                    
            Fpdf::Ln(10);
            Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA KELUARGA', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(10, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(50, 5, 'NAMA', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'HUBUNGAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TEMPAT', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TANGGAL', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'JENIS', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'PENDIDIKAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'PEKERJAAN', 0, 0, 'C');
            Fpdf::Ln(5);
            Fpdf::Cell(10, 5, '', 0, 0, 'C');
            Fpdf::Cell(50, 5, 'KARYAWAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, '', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'LAHIR', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'LAHIR', 0, 0, 'C');
            Fpdf::Cell(20, 5, '', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'KELAMIN', 0, 0, 'C');
            Fpdf::Cell(20, 5, '', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qKeluarga ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qKeluarga  as $row) {

                  Fpdf::Cell(10, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(50, 5, $row->nama_keluarga, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->hubungan, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tempat_lahir, 0, 0, 'L');
                  Fpdf::Cell(20, 5, displayDMY($row->tgl_lahir,"/"), 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->jenis_kelamin, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->pendidikan, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->pekerjaan, 0, 0, 'L');
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

                      # ----------------
            Fpdf::Ln(10);

           
           Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA KONTAK LAIN', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(10, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(40, 5, 'NAMA', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'HUBUNGAN', 0, 0, 'C');
            Fpdf::Cell(90, 5, 'ALAMAT', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'NO TELPON', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qKontak ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qKontak  as $row) {

                  Fpdf::Cell(10, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(40, 5, $row->nama_kontak, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->hubungan, 0, 0, 'L');
                  Fpdf::Cell(90, 5, $row->alamat, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->telpon, 0, 0, 'L');
                  
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

                      # ----------------
            Fpdf::Ln(10);

            Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA RIWAYAT PENDIDIKAN', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(20, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'JENJANG SEKOLAH', 0, 0, 'C');
            Fpdf::Cell(90, 5, 'NAMA SEKOLAH', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'KOTA', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AWAL', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AKHIR', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qPendidikan ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qPendidikan  as $row) {

                  Fpdf::Cell(20, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(20, 5, $row->id_pendidikan, 0, 0, 'L');
                  Fpdf::Cell(90, 5, $row->nama_sekolah, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->kota_sekolah, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tahun_awal, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tahun_akhir, 0, 0, 'L');
                  
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

          Fpdf::Ln(10);

            Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA PENGALAMAN KERJA', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(20, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(50, 5, 'PERUSAHAAN', 0, 0, 'C');
            Fpdf::Cell(30, 5, 'KOTA', 0, 0, 'C');
            Fpdf::Cell(40, 5, 'JABATAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AWAL', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AKHIR', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qPengalaman ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qPengalaman  as $row) {

                  Fpdf::Cell(20, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(50, 5, $row->perusahaan, 0, 0, 'L');
                  Fpdf::Cell(30, 5, $row->kota, 0, 0, 'L');
                  Fpdf::Cell(40, 5, $row->jabatan, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tahun_awal, 0, 0, 'C');
                  Fpdf::Cell(20, 5, $row->tahun_akhir, 0, 0, 'C');
                  
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

//ID  Pendidikan  Nama Sekolah  Kota  Tahun Awal  Tahun Akhir

            Fpdf::Output();
       

    }

     public function export()
    {   
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif",1)
                            ->orderBy("a.id_cabang","ASC")
                            ->orderBy("a.id_departemen","ASC")->get();

        

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR KARYAWAN AKTIF');
        $spreadsheet->getActiveSheet()->getStyle('A3:Z3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
        
        $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'N I K');
        $sheet->setCellValue('C3', 'NAMA');
        $sheet->setCellValue('D3', 'JABATAN');
        $sheet->setCellValue('E3', 'DEPARTEMEN');
        $sheet->setCellValue('F3', 'CABANG');
        $sheet->setCellValue('G3', 'STATUS KERJA');
        $sheet->setCellValue('H3', 'TANGGAL MASUK');
        $sheet->setCellValue('I3', 'STATUS NIKAH');
        $sheet->setCellValue('J3', 'AGAMA');
        $sheet->setCellValue('K3', 'PENDIDIKAN');
        $sheet->setCellValue('L3', 'JENIS KELAMIN');
        $sheet->setCellValue('M3', 'TEMPAT LAHIR');
        $sheet->setCellValue('N3', 'TANGGAL LAHIR');
        $sheet->setCellValue('O3', 'ALAMAT KTP');
        $sheet->setCellValue('P3', 'ALAMAT TINGGAL');
        $sheet->setCellValue('Q3', 'NO HP');
        $sheet->setCellValue('R3','TELP RUMAH');
        $sheet->setCellValue('S3', 'NO REKENING');
        $sheet->setCellValue('T3', 'A/N REKENING');
        $sheet->setCellValue('U3', "EMAIL");
        $sheet->setCellValue('P3', 'NO KTP');
        $sheet->setCellValue('W3', "STATUS PAJAK");
        $sheet->setCellValue('X3', 'NPWP');
        $sheet->setCellValue('Y3', "NO BPJS KESEHATAN");
        $sheet->setCellValue('Z3', 'NO BPJS TK');
        $sheet->setCellValue('AA3','NO ABSEN');

        
        foreach($query as $row) 
        {
      
                  $qKaryawan            = new CalonKaryawanModel;
                  $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                  $pendidikan           = $row->id_pendidikan;
                  if ($pendidikan=="UND")
                  {
                    $pendidikan="UNDER SMA";
                     
                  }
                  else if ($pendidikan=="DIP")
                  {
                    $pendidikan="DIPLOMA";
                     
                  }
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nik);
                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('G'.$baris,$row->status_kerja);
                  $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                  $sheet->setCellValue('I'.$baris,$row->status_nikah);
                  $sheet->setCellValue('J'.$baris,$row->agama);
                  $sheet->setCellValue('K'.$baris,$pendidikan);
                  $sheet->setCellValue('L'.$baris,$row->jenis_kelamin);
                  $sheet->setCellValue('M'.$baris,$row->tempat_lahir);
                  $sheet->setCellValue('N'.$baris,$row->tgl_lahir);
                  $sheet->setCellValue('O'.$baris,$row->alamat_valid);
                  $sheet->setCellValue('P'.$baris,$row->alamat );
                  $sheet->setCellValue('Q'.$baris,$row->hp);
                  $sheet->setCellValue('R'.$baris,$row->telp_rumah);
                  $sheet->setCellValue('S'.$baris,$row->no_rekening);
                  $sheet->setCellValue('T'.$baris,$row->anrekening);
                  $sheet->setCellValue('U'.$baris,$row->email);
                  $sheet->setCellValue('V'.$baris,'="'.$row->no_ktp.'"');
                  $sheet->setCellValue('W'.$baris,$statuspajak);
                  $sheet->setCellValue('X'.$baris,$row->nonpwp);
                  $sheet->setCellValue('Y'.$baris,$row->nobpjskes);
                  $sheet->setCellValue('Z'.$baris,$row->nobjstk);
                  $sheet->setCellValue('AA'.$baris,$row->noabsen);


       
                 $baris=$baris+1;
        }

        foreach(range('B','Z') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
         $spreadsheet->getActiveSheet()->getColumnDimension('AA')
                  ->setAutoSize(true);

        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "data karyawanlengkap";  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   

    }

    public function exportresign()
    {   
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif",0)
                            ->orderBy("a.id_cabang","ASC")
                            ->orderBy("a.id_departemen","ASC")->get();

        

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR KARYAWAN TIDAK AKTIF');
        $spreadsheet->getActiveSheet()->getStyle('A3:Z3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
        
        $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'N I K');
        $sheet->setCellValue('C3', 'NAMA');
        $sheet->setCellValue('D3', 'JABATAN');
        $sheet->setCellValue('E3', 'DEPARTEMEN');
        $sheet->setCellValue('F3', 'CABANG');
        $sheet->setCellValue('G3', 'STATUS KERJA');
        $sheet->setCellValue('H3', 'TANGGAL MASUK');
        $sheet->setCellValue('I3', 'STATUS NIKAH');
        $sheet->setCellValue('J3', 'AGAMA');
        $sheet->setCellValue('K3', 'PENDIDIKAN');
        $sheet->setCellValue('L3', 'JENIS KELAMIN');
        $sheet->setCellValue('M3', 'TEMPAT LAHIR');
        $sheet->setCellValue('N3', 'TANGGAL LAHIR');
        $sheet->setCellValue('O3', 'ALAMAT KTP');
        $sheet->setCellValue('P3', 'ALAMAT TINGGAL');
        $sheet->setCellValue('Q3', 'NO HP');
        $sheet->setCellValue('R3','TELP RUMAH');
        $sheet->setCellValue('S3', 'NO REKENING');
        $sheet->setCellValue('T3', 'A/N REKENING');
        $sheet->setCellValue('U3', "EMAIL");
        $sheet->setCellValue('P3', 'NO KTP');
        $sheet->setCellValue('W3', "STATUS PAJAK");
        $sheet->setCellValue('X3', 'NPWP');
        $sheet->setCellValue('Y3', "NO BPJS KESEHATAN");
        $sheet->setCellValue('Z3', 'NO BPJS TK');
        $sheet->setCellValue('AA3','NO ABSEN');

        
        foreach($query as $row) 
        {
      
                  $qKaryawan            = new CalonKaryawanModel;
                  $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                  $pendidikan           = $row->id_pendidikan;
                  if ($pendidikan=="UND")
                  {
                    $pendidikan="UNDER SMA";
                     
                  }
                  else if ($pendidikan=="DIP")
                  {
                    $pendidikan="DIPLOMA";
                     
                  }
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nik);
                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('G'.$baris,$row->status_kerja);
                  $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                  $sheet->setCellValue('I'.$baris,$row->status_nikah);
                  $sheet->setCellValue('J'.$baris,$row->agama);
                  $sheet->setCellValue('K'.$baris,$pendidikan);
                  $sheet->setCellValue('L'.$baris,$row->jenis_kelamin);
                  $sheet->setCellValue('M'.$baris,$row->tempat_lahir);
                  $sheet->setCellValue('N'.$baris,$row->tgl_lahir);
                  $sheet->setCellValue('O'.$baris,$row->alamat_valid);
                  $sheet->setCellValue('P'.$baris,$row->alamat );
                  $sheet->setCellValue('Q'.$baris,$row->hp);
                  $sheet->setCellValue('R'.$baris,$row->telp_rumah);
                  $sheet->setCellValue('S'.$baris,$row->no_rekening);
                  $sheet->setCellValue('T'.$baris,$row->anrekening);
                  $sheet->setCellValue('U'.$baris,$row->email);
                  $sheet->setCellValue('V'.$baris,'="'.$row->no_ktp.'"');
                  $sheet->setCellValue('W'.$baris,$statuspajak);
                  $sheet->setCellValue('X'.$baris,$row->nonpwp);
                  $sheet->setCellValue('Y'.$baris,$row->nobpjskes);
                  $sheet->setCellValue('Z'.$baris,$row->nobjstk);
                  $sheet->setCellValue('AA'.$baris,$row->noabsen);


       
                 $baris=$baris+1;
        }

        foreach(range('B','Z') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
         $spreadsheet->getActiveSheet()->getColumnDimension('AA')
                  ->setAutoSize(true);

        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "data karyawanl tidak aktif lengkap";  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   

    }

    public function import() {
        $data["title"]        = "Import";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/importsave";
        /* ----------
         Updatesisacuti
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_upload(array("name"=>"file_excel", "label"=>"File Excel", "mandatory"=>"yes"));
      
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }
  
    public function importsave(Request $request)
      { 
          $rules = array(
                      'file_excel' => 'required|'                 
          );
  
          $messages = [
                      'file_excel' => 'Pilih file',
  
          ];
  
          $validator = Validator::make($request->all(), $rules, $messages);
  
          if ($validator->fails()) {
                return redirect("/Karyawan/import")
                ->withErrors($validator)
                ->withInput();
          } else {
            $qKaryawan  = new CalonKaryawanModel;
            $qUser      = new UserModel;
            
            $qKaryawan->importcsv($request);
            # ---------------
            session()->flash("success_message", "update has been updated");
            # ---------------
            return redirect("/Karyawan/index");
          }
      }
  
      public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }

    public function laporan(){
        $data["title"]         = "Export Calon Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkaryawan/exportproses";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
        
         $qKaryawan             = array_merge($collection,$qKaryawan);
         $qStatusAbsensi        = getStatusHadir();
         $qJenisLaporan         = getJenisLaporan();
         $qJenisCabang          = array_merge($collection,$qMaster->getJenisCabang());
         $parameter_system      = DB::table('m_profilesystem')->select('tgl_absenawal','tgl_absenakhir')->first();
         $tgl_absenawal         = date('d/m/Y', strtotime($parameter_system->tgl_absenawal));
         $tgl_absenakhir        = date('d/m/Y', strtotime($parameter_system->tgl_absenakhir));
       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_periode_masuk_awal", "label"=>"Tanggal Periode Awal", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_periode_masuk_akhir", "label"=>"Tanggal Periode Akhir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
         $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Nama Cabang", "mandatory"=>"yes", "source"=>$qJenisCabang));

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;laporan&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
    }

    public function exportproses(Request $request){
    
    $id_cabang = $request->id_cabang;
    // $id_karyawan = $request->id_karyawan;
    // $jenis_laporan = $request->jenis_laporan;
    // $status_absensi = $request->status_absensi;
    $tgl_periode_awal = setYMD($request->tgl_periode_masuk_awal,'/');
    $tgl_periode_akhir = setYMD($request->tgl_periode_masuk_awal,'/');

            if($id_cabang =='-'){
                $whereCondition = [];
            }else{
                $whereCondition = [
                    ['a.id_cabang',$id_cabang],
                  ];
            }
    
            $query  =DB::table("p_calon_karyawan as a")
            ->select("a.*","a.tgl_masuk","a.tgl_keluar","a.status_kerja","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
            ->where($whereCondition)
            ->orderBy("a.nama_karyawan", "DESC")
            ->get();    
        
           
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'FFA0A0A0',
                    ],
                    'endColor' => [
                        'argb' => 'FFFFFFFF',
                    ],
                ],
            ];
    
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'DAFTAR CALON KARYAWAN');
            $spreadsheet->getActiveSheet()->getStyle('A3:Z3')->applyFromArray($styleArray);
            $baris = 4;
            /*-------------------------
            KOLOM DATA
            ---------------------------*/
            
            $sheet->setCellValue('A3', 'NO.');
            $sheet->setCellValue('B3', 'N I K');
            $sheet->setCellValue('C3', 'NAMA');
            $sheet->setCellValue('D3', 'JABATAN');
            $sheet->setCellValue('E3', 'DEPARTEMEN');
            $sheet->setCellValue('F3', 'CABANG');
            $sheet->setCellValue('G3', 'STATUS KERJA');
            $sheet->setCellValue('H3', 'TANGGAL MASUK');
            $sheet->setCellValue('I3', 'STATUS NIKAH');
            $sheet->setCellValue('J3', 'AGAMA');
            $sheet->setCellValue('K3', 'PENDIDIKAN');
            $sheet->setCellValue('L3', 'JENIS KELAMIN');
            $sheet->setCellValue('M3', 'TEMPAT LAHIR');
            $sheet->setCellValue('N3', 'TANGGAL LAHIR');
            $sheet->setCellValue('O3', 'ALAMAT KTP');
            $sheet->setCellValue('P3', 'ALAMAT TINGGAL');
            $sheet->setCellValue('Q3', 'NO HP');
            $sheet->setCellValue('R3','TELP RUMAH');
            $sheet->setCellValue('S3', 'NO REKENING');
            $sheet->setCellValue('T3', 'A/N REKENING');
            $sheet->setCellValue('U3', "EMAIL");
            $sheet->setCellValue('V3', 'NO KTP');
            $sheet->setCellValue('W3', "STATUS PAJAK");
            $sheet->setCellValue('X3', 'NPWP');
            $sheet->setCellValue('Y3', "NO BPJS KESEHATAN");
            $sheet->setCellValue('Z3', 'NO BPJS TK');
            $sheet->setCellValue('AA3','NO ABSEN');
    
            
            foreach($query as $row) 
            {
          
                      $qKaryawan            = new CalonKaryawanModel;
                      $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                      $pendidikan           = $row->id_pendidikan;
                      if ($pendidikan=="UND")
                      {
                        $pendidikan="UNDER SMA";
                         
                      }
                      else if ($pendidikan=="DIP")
                      {
                        $pendidikan="DIPLOMA";
                         
                      }
                      $sheet->setCellValue('A'.$baris,$baris-3);
                      $sheet->setCellValue('B'.$baris,$row->nik);
                      $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                      $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                      $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                      $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                      $sheet->setCellValue('G'.$baris,$row->status_kerja);
                      $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                      $sheet->setCellValue('I'.$baris,$row->status_nikah);
                      $sheet->setCellValue('J'.$baris,$row->agama);
                      $sheet->setCellValue('K'.$baris,$pendidikan);
                      $sheet->setCellValue('L'.$baris,$row->jenis_kelamin);
                      $sheet->setCellValue('M'.$baris,$row->tempat_lahir);
                      $sheet->setCellValue('N'.$baris,$row->tgl_lahir);
                      $sheet->setCellValue('O'.$baris,$row->alamat_valid);
                      $sheet->setCellValue('P'.$baris,$row->alamat );
                      $sheet->setCellValue('Q'.$baris,$row->hp);
                      $sheet->setCellValue('R'.$baris,$row->telp_rumah);
                      $sheet->setCellValue('S'.$baris,$row->no_rekening);
                      $sheet->setCellValue('T'.$baris,$row->anrekening);
                      $sheet->setCellValue('U'.$baris,$row->email);
                      $sheet->setCellValue('V'.$baris,'="'.$row->no_ktp.'"');
                      $sheet->setCellValue('W'.$baris,$statuspajak);
                      $sheet->setCellValue('X'.$baris,$row->nonpwp);
                      $sheet->setCellValue('Y'.$baris,$row->nobpjskes);
                      $sheet->setCellValue('Z'.$baris,$row->nobjstk);
                      $sheet->setCellValue('AA'.$baris,$row->noabsen);
    
    
           
                     $baris=$baris+1;
            }
    
            foreach(range('B','Z') as $columnID)
             {
                 $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                      ->setAutoSize(true);
                 
                      
             }
             $spreadsheet->getActiveSheet()->getColumnDimension('AA')
                      ->setAutoSize(true);
    
            
            $writer = new Xlsx($spreadsheet);
            // $writer->save('data karyawanlengkap.xlsx');
            $judul    = "data calon karyawan";  
            $name_file = $judul.'.xlsx';
          // $path = storage_path('Laporan\\'.$name_file);
          $path = public_path().'/app/'.$name_file;
          $contents = is_dir($path);
          // $headers = array('Content-Type' => File::mimeType($path));
          // dd($path.'/'.$name_file,$contents);
          $writer->save($path);
          $filename   = str_replace("@", "/", $path);
            # ---------------
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=".$name_file);
            header("Content-Type: application/xlsx");
            header("Content-Transfer-Encoding: binary");
            # ---------------
            require "$filename";
            # ---------------
            exit;
    }

    public function get_profile($id) {

        $qKaryawan      = new KaryawanModel;
        $qDataKaryawan  = $qKaryawan->getProfile($id);
        
        // Return dalam bentuk json untuk diproses oleh jquery
        return json_encode($qDataKaryawan->first());
  
      }

    public function cetak($id){
        $qMaster              = new MasterModel;
        $qKaryawanm            = new CalonKaryawanModel;
        $qKaryawan             = $qKaryawanm->getProfile($id)->first();
        $qKeluarga             = $qKaryawanm->getDetailOrangTuaKeluarga($id)->first();
        $qKontak               = $qKaryawanm->getDetailKontak($id);
        $qPendidikan           = $qKaryawanm->getDetailPendidikan($id);
        $qPengalaman           = $qKaryawanm->getDetailPengalaman($id);
        $newIdk                 = $id; 

        $karakterid             = "0000000000000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
      /*
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = getSelectPendidikan();
       
       */ 
        $Line                  = 185;
        Fpdf::AddPage();
            
            
            $logo = public_path()."/app/img/unknown.png";
            Fpdf::Image($logo, 10, 35, 50, 50);
            
            Fpdf::SetLineWidth(0.3);
            Fpdf::SetFont("Arial", "B", 14);
            Fpdf::Cell(185, 40, "DATA CALON KARYAWAN", 0, 0, 'L');
            Fpdf::Ln(30);
            Fpdf::SetFont("Arial", "", 10);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'ID TEMP', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $newIdk, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'NAMA', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->nama_karyawan, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'JENIS KELAMIN', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, strtoupper($qKaryawan->jenis_kelamin), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'JABATAN/DIVISI', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->nama_jabatan.' / '.$qKaryawan->nama_departemen, 0, 0, 'J');
            Fpdf::Ln(5);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'PERUSAHAAN', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, "PT. ARTHA PRIMA FINANCE", 0, 0, 'L');
            // Fpdf::Cell(75, 5, displayDMY($qKaryawan->tgl_masuk,"/"), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'CABANG/OUTLET', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->nama_cabang, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(55, 5, '', 0, 0, 'L');
            Fpdf::Cell(45, 5, 'TANGGAL MELAMAR', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, displayDMY($qKaryawan->tgl_melamar,"/"), 0, 0, 'L');
            Fpdf::Ln(20);
            Fpdf::Cell(25, 5, 'NO NPWP', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->nonpwp, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'NO KTP/NO KK', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->no_ktp .' / '.$qKaryawan->no_kk, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'NAMA IBU KANDUNG', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->nama_ibu, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'TEMPAT/TGL LAHIR', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->tempat_lahir .'-'. displayDMY($qKaryawan->tgl_lahir,"/"), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'STATUS PERNIKAHAN', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, strtoupper($qKaryawan->status), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'ALAMAT', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::MultiCell(140, 5, $qKaryawan->alamat, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'TELP/HP', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->no_telpon.' / '.$qKaryawan->hp, 0, 0, 'J');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'PENDIDIKAN/AGAMA', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->nama_pendidikan.' / '.strtoupper($qKaryawan->agama), 0, 0, 'J');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'GOLONGAN DARAH', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->golongan_darah, 0, 0, 'J');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'NO REKENING', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->no_rekening, 0, 0, 'J');
            Fpdf::Ln(5);
            
            
            // if (strlen($qKaryawan->alamat) > 0) {
                    
            //         Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)), 0, 75), 0, 0, 'L');
            //         Fpdf::Ln(5);
            //     }
            //     if (strlen($qKaryawan->alamat) > 75) {
                    
            //         Fpdf::Cell(65, 5, '', 0, 0, 'R');
            //         Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)), 75, 150), 0, 0, 'L');
            //         Fpdf::Ln(5);
            //     }
            //     if (strlen($qKaryawan->alamat) > 150) {
                  
            //         Fpdf::Cell(65, 5, '', 0, 0, 'R');
            //         Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)),150, 225), 0, 0, 'L');
            //         Fpdf::Ln(5);
            //     }
            Fpdf::Ln(10);
            Fpdf::SetFont("Arial", "B", 10);
            if($qKaryawan->nama_ibu != null){
                Fpdf::Cell(185, 5, 'DATA REFERENSI/ALAMAT MENDADAK', 0, 0, 'L');
                Fpdf::Ln(5);
                Fpdf::Cell(70, 0.1, '', 1, 0, 'C');
                Fpdf::Ln(2);
                Fpdf::SetFont("Arial", "", 10);
                Fpdf::Cell(25, 5, 'NAMA REFERENSI', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, $qKaryawan->nama_ibu, 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'NO KTP', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'HUBUNGAN', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, 'ORANG TUA', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'KETERANGAN LAIN', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'ALAMAT', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(20);
            }else{
                Fpdf::Cell(185, 5, 'DATA REFERENSI/ALAMAT MENDADAK', 0, 0, 'L');
                Fpdf::Ln(5);
                Fpdf::Cell(70, 0.1, '', 1, 0, 'C');
                Fpdf::Ln(2);
                Fpdf::SetFont("Arial", "", 10);
                Fpdf::Cell(25, 5, 'NAMA REFERENSI', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'NO KTP', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'HUBUNGAN', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'KETERANGAN LAIN', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(5);
                Fpdf::Cell(25, 5, 'ALAMAT', 0, 0, 'L');
                Fpdf::Cell(32, 5, ':', 0, 0, 'R');
                Fpdf::Cell(75, 5, '', 0, 0, 'J');
                Fpdf::Ln(20);
            }

            Fpdf::Cell(185, 5, 'DATA BANK', 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(70, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "", 10);
            Fpdf::Cell(25, 5, 'NAMA BANK', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, '', 0, 0, 'J');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'ATAS NAMA REKENING', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->anrekening, 0, 0, 'J');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, 'NO REKENING', 0, 0, 'L');
            Fpdf::Cell(32, 5, ':', 0, 0, 'R');
            Fpdf::Cell(75, 5, $qKaryawan->no_rekening, 0, 0, 'J');
            Fpdf::Ln(5);
            
            
            

//ID  Pendidikan  Nama Sekolah  Kota  Tahun Awal  Tahun Akhir

            Fpdf::Output();
        exit;
            
    }
   
}
