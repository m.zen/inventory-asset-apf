<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Personalia\KaryawankeluarModel;
use App\Model\Master\MasterModel;

class KaryawankeluarController extends Controller
{
    
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawankeluar/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawankeluar                  = new KaryawankeluarModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawankeluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
        							array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
        							array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
        							array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
        							array("label"=>"Tgl Keluar"
                                                ,"name"=>"Tanggal_keluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                      array("label"=>"Status Proses"
                                                ,"name"=>"status"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
        							);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KARYAWANKELUAR" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWANKELUAR");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWANKELUAR");
        }
        # ---------------
        $data["select"]        = $qKaryawankeluar->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawankeluar->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Karyawan Keluar";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawankeluar/save";
       
        $qMaster               = new MasterModel;
       // $qKaryawankeluar            = new KaryawankeluarModel;

        $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qListKry              = $qMaster->getSelectKaryawan();
        $qListKry              = array_merge($collection,$qListKry);
        $qAlasan               = $qMaster->getAlasankeluar();
        $qAlasan              = array_merge($collection,$qAlasan);
        
        
       $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"id_alasankeluar", "label"=>"Alasan Keluar",  "source"=>$qAlasan));
       $data["fields"][]      = form_text(array("name"=>"keterangan_keluar", "label"=>"Keterangan Keluar", "mandatory"=>"yes"));
      
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'id_karyawan' => 'required'                     );

        $messages = ['id_karyawan.required' => 'Nama Karyawan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawankeluar/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawankeluar  = new KaryawankeluarModel;
            # ---------------

            $qKaryawankeluar->createData($request);
            # ---------------
            session()->flash("success_message", "Karyawankeluar has been saved");
            # ---------------
            return redirect("/karyawankeluar/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Karyawankeluar";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawankeluar/update";
        /* ----------
         Karyawankeluar
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawankeluar      = new KaryawankeluarModel;
        $qListKry              = $qMaster->getSelectKaryawan();
        $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qListKry              = $qMaster->getSelectKaryawan();
        $qListKry              = array_merge($collection,$qListKry);
        $qAlasan               = $qMaster->getAlasankeluar();
        $qAlasan              = array_merge($collection,$qAlasan);
        
        /* ----------
         Source
        ----------------------- */
        $qKaryawankeluar      = $qKaryawankeluar->getProfile($id)->first();

       
         if($qKaryawankeluar->status=="APPROVE")
         {

            session()->flash("error_message", "Data tidak bisa diedit");
            return redirect("/karyawankeluar/index"); 
         }
         else
         {


             $data["fields"][]      = form_hidden(array("name"=>"id_karyawankeluar", "label"=>"Karyawankeluar ID", "readonly"=>"readonly", "value"=>$id));
             $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
             $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry,"value"=>$qKaryawankeluar->id_karyawan));
             $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"yes","value"=>displayDMY($qKaryawankeluar->tgl_keluar,"/")));

             $data["fields"][]      = form_select(array("name"=>"id_alasankeluar", "label"=>"Alasan Keluar","value"=>$qKaryawankeluar->id_alasankeluar,  "source"=>$qAlasan));
       $data["fields"][]      = form_text(array("name"=>"keterangan_keluar", "label"=>"Keterangan Keluar", "mandatory"=>"yes","value"=>$qKaryawankeluar->keterangan_keluar));


                      
              # ---------------
              $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
              $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
              # ---------------
              return view("default.form", $data);
         }

    }

    public function update(Request $request)
    {
         $rules = array(
                      
                      'id_karyawan' => 'required'                     );

        $messages = ['id_karyawan.required' => 'Nama Karyawan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawankeluar/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawankeluar      = new KaryawankeluarModel;
            # ---------------
            $qKaryawankeluar->updateData($request);
            # ---------------
            session()->flash("success_message", "Karyawankeluar has been updated");
            # ---------------
            return redirect("/karyawankeluar/index");
        }
    }




    public function delete($id) {
        $data["title"]         = "Delete Karyawankeluar";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawankeluar/remove";
        /* ----------
         Source
        ----------------------- */
       $qMaster              = new MasterModel;
        $qKaryawankeluar      = new KaryawankeluarModel;
        $qListKry              = $qMaster->getSelectKaryawan();
        $qAlasan               = $qMaster->getAlasankeluar();
       
        /* ----------
         Source
        ----------------------- */
        $qKaryawankeluar      = $qKaryawankeluar->getProfile($id)->first();
       
         if($qKaryawankeluar->status=="APPROVE")
         {

            session()->flash("error_message", "Data tidak bisa diedit");
            return redirect("/karyawankeluar/index"); 
         }
         else
         {

             $data["fields"][]      = form_hidden(array("name"=>"id_karyawankeluar", "label"=>"Karyawankeluar ID", "readonly"=>"readonly", "value"=>$id));
             $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"Delete"));
             $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry,"value"=>$qKaryawankeluar->id_karyawan));
              $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"yes","value"=>displayDMY($qKaryawankeluar->tgl_keluar,"/")));

           $data["fields"][]      = form_select(array("name"=>"id_alasankeluar", "label"=>"Alasan Keluar","value"=>$qKaryawankeluar->id_alasankeluar,  "source"=>$qAlasan));
       $data["fields"][]      = form_text(array("name"=>"keterangan_keluar", "label"=>"Keterangan Keluar", "mandatory"=>"yes","value"=>$qKaryawankeluar->keterangan_keluar));
         

            // $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"","value"=>""));
             //$data["fields"][]      = form_text(array("name"=>"alasan_keluar", "label"=>"Alasan Keluar", "mandatory"=>"","value"=>""));

                      
              # ---------------
              $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
              $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
              # ---------------
              return view("default.form", $data);
         }
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qKaryawankeluar     = new KaryawankeluarModel;
            # ---------------
            $qKaryawankeluar->removeData($request);
            # ---------------
            session()->flash("success_message", "Karyawankeluar has been removed");
        } else {
            session()->flash("error_message", "Karyawankeluar cannot be removed");
        }
      # ---------------
        return redirect("/karyawankeluar/index"); 
    }




    public function approve($id) {
        $data["title"]        = "Approve Karyawan Keluar";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawankeluar/updateapprove";
        /* ----------
         Karyawankeluar
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawankeluar      = new KaryawankeluarModel;
        $qListKry              = $qMaster->getSelectKaryawan();
        $qAlasan               = $qMaster->getAlasankeluar();
        
        /* ----------
         Source
        ----------------------- */
        $qKaryawankeluar      = $qKaryawankeluar->getProfile($id)->first();
       
        
        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawankeluar", "label"=>"Karyawankeluar ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
       // $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan","readonly"=>"readonly",  "value"=>$qKaryawankeluar->id_karyawan));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawankeluar", "readonly"=>"readonly", "value"=>$qKaryawankeluar->nama_karyawan));
       
              $data["fields"][]      = form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "readonly"=>"readonly","mandatory"=>"yes","value"=>displayDMY($qKaryawankeluar->tgl_keluar,"/")));
      $data["fields"][]      = form_select(array("name"=>"id_alasankeluar", "label"=>"Alasan Keluar","value"=>$qKaryawankeluar->id_alasankeluar,  "source"=>$qAlasan));
       $data["fields"][]      = form_text(array("name"=>"keterangan_keluar", "label"=>"Keterangan Keluar", "mandatory"=>"yes","value"=>$qKaryawankeluar->keterangan_keluar));
         

      	        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function updateapprove(Request $request)
    {
         $rules = array(
                      
                      'id_karyawankeluar' => 'required'                     );

        $messages = ['id_karyawankeluar.required' => 'Nama Karyawan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawankeluar/approve/" . $request->input("id_karyawankeluar"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawankeluar      = new KaryawankeluarModel;
            # ---------------
            $qKaryawankeluar->updateapprove($request);
            # ---------------
            session()->flash("success_message", "Karyawan keluar has been updated");
            # ---------------
            return redirect("/karyawankeluar/index");
        }
    }
}
