<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\KontrakkerjaModel;
use PDF;
class KontrakkerjaController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = "Perjanjian Keja Waktu Tertentu "; //ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/kontrakkerja/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

       


        $qMenu                  = new MenuModel;
        $qKontrakkerja          = new KontrakkerjaModel;
        $qMaster                = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qKaryawan                = $qMaster->getKaryawan($idkry)->first();
        $data["form_act_add"]     = "/kontrakkerja/add/".$idkry;
        $data["form_act_edit"]    = "/kontrakkerja/edit";
        $data["form_act_delete"]  = "/kontrakkerja/delete";
        $data["form_act_print"]  = "/kontrakkerja/cetak";
        $data["form_act_aktifasi"]  = "/kontrakkerja/aktif";
       
       
       
        
        # ---------------*/
        
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));
         

        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"PKWT (Kontrak)", "url"=>"/kontrakkerja/index/".$idkry, "active"=>"active"),
                                 array("label"=>"PKWTT (Tetap)", "url"=>"/permanenkerja/index/".$idkry, "active"=>""), 
                                 array("label"=>"Mutasi", "url"=>"/karir/index/".$idkry, "active"=>""), 
                                 array("label"=>"Peringatan", "url"=>"/peringatan/index/".$idkry, "active"=>""),
                                 
                                 
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_kontrakkerja"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                      array("label"=>"Tgl Awal"
                                                ,"name"=>"tgl_awal"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                        array("label"=>"Tgl Tanda Tangan"
                                                ,"name"=>"tgl_tandatangan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                          array("label"=>"Keterangan"
                                                ,"name"=>"keterangan_kontrak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                          array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                          


                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KONTRAK" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAK");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAK");
        }
        # ---------------
        //dd($qKontrakkerja->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry));
        $data["select"]        = $qKontrakkerja->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qKontrakkerja->getList($request->input("text_search"));
        # ---- -----------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.karirkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Kontrak Kerja (PKWT)";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/kontrakkerja/save";
        
        $qMaster               = new MasterModel;

        $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		    $qJabatan              = $qMaster->getSelectJabatan(); 
		    $qStatuskerja          = getSelectStskerja();
        $qKontrakkerjamdl      = new KontrakkerjaModel;
        /* ----------
         Source
        ----------------------- */
        //$idKontrakkerja             = explode("&", $id); 
       // $qKontrakkerja        = $qKontrakkerjamdl->getProfile($id)->first();
        $qViewKaryawan        = $qKontrakkerjamdl->getProfileKry($idkry)->first();  $qViewFormatsurat       = $qKontrakkerjamdl->getProfileKry($idkry)->first(); 
              
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKaryawan->id_cabang));        
        $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKaryawan->id_departemen));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKaryawan->id_jabatan));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"gaji_pokok_awal", "label"=>"Gaji Pokok Awal",  "readonly"=>"readonly", "value"=>number_format($qViewKaryawan->gaji_pokok,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok",  "mandatory"=>"yes", "value"=>number_format($qViewKaryawan->gaji_pokok,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok",  "mandatory"=>"yes", "value"=>number_format($qViewKaryawan->gaji_pokok,0)));
             
       //dd(($qViewKaryawan->tunj_jabatan==null)?0:number_format($qViewKaryawan->tunj_jabatan,0));  


        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jabatan_awal", "label"=>"Tnj. Jabatan Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_jabatan,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan", "value"=>number_format($qViewKaryawan->tunj_jabatan,0)));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_transport_awal", "label"=>"Tnj. Tranport Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_transport,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport", "value"=>number_format($qViewKaryawan->tunj_transport,0)));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jaga_awal", "label"=>"Tnj. Jaga awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_jaga,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga", "value"=>number_format($qViewKaryawan->tunj_jaga,0)));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_kemahalan_awal", "label"=>"Tnj. Kemahalan Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_kemahalan,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan", "value"=>number_format($qViewKaryawan->tunj_kemahalan,0)));
       
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_makan_awal", "label"=>"Tnj. Makan Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_makan,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_makan", "label"=>"Tnj. Makan", "value"=>number_format($qViewKaryawan->tunj_makan,0)));
   
      

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



/*

 public function save(Request $request) {
        $rules = array(
                      'email' => 'required|',
                        'name' => 'required|',
                        'password' => 'required|min:3',
                        'password_confirm' => 'required|min:3|same:password',
        );

        $messages = [
                      'email.required' => 'Email harus diisi',
                      'name.required' => 'Nama harus diisi',
                      'password.required' => 'Password harus diisi',
                      'password_confirm.required' => 'Konfirmasi Password harus diisi',
                      'password_confirm.same' => 'Konfirmasi Password harus sama',
        ];


*/


    public function save(Request $request) {
       

        $rules = array(
                      
                      'id_cabang' => 'required|',
                      'id_departemen' => 'required|',
                      'id_jabatan' => 'required|',
                      'tgl_awal' => 'required|',
                      'tgl_akhir' => 'required|',
                      'gaji_pokok'=> 'required|',

                       );

        $messages = ['id_cabang.required' => 'Cabang harus diisi',
                     'id_departemen.required' => 'Departemen harus diisi',
                     'id_jabatan.required' => 'Jabatan harus diisi',
                     'tgl_awal.required' => 'Tgl Awal harus diisi',
                     'tgl_akhir.required' => 'Tgl akhir harus diisi',
                     'gaji_pokok.required' => 'Gaji pokok harus diisi',
                    
           ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/kontrakkerja/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKontrakkerja  = new KontrakkerjaModel;
            

            if($qKontrakkerja->CekKontrak($request->id_karyawan,$request->tgl_awal,$request->tgl_akhir) > 0)
            {

                session()->flash("error_message", "Status Kerja sudah diinput");
                return redirect("/kontrakkerja/add/". $request->id_karyawan);
            }
            else
            {
                 $qKontrakkerja->createData($request);
                 session()->flash("success_message", "Wilayah has been saved");
                 return redirect("/kontrakkerja/index/". $request->id_karyawan);

            }



        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Kontrak Kerja (PKWT)";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/kontrakkerja/update";
        /* ----------
         Kontrakkerja
        ----------------------- */
        $qMaster           = new MasterModel;
        $qKontrakkerja             = new KontrakkerjaModel;
        /* ----------
         Source
        ----------------------- */
        //$idKontrakkerja             = explode("&", $id); 
        $qKontrakkerja        = $qKontrakkerja->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan();
		
		$qStatuskerja          = getSelectStskerja();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Kontrakkerja", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qKontrakkerja->id_karyawan));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKontrakkerja->id_cabang));        
        $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKontrakkerja->id_departemen));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKontrakkerja->id_jabatan));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_awal,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_akhir,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok",  "mandatory"=>"yes", "value"=>number_format($qKontrakkerja->gaji_pokok,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan", "value"=>number_format($qKontrakkerja->tunj_jabatan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport", "value"=>number_format($qKontrakkerja->tunj_transport,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga", "value"=>number_format($qKontrakkerja->tunj_jaga,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan", "value"=>number_format($qKontrakkerja->tunj_kemahalan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_makan", "label"=>"Tnj. Makan", "value"=>number_format($qKontrakkerja->tunj_makan,0)));

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                      
                      'id_cabang' => 'required|',
                      'id_departemen' => 'required|',
                      'id_jabatan' => 'required|',
                      'tgl_awal' => 'required|',
                      'tgl_akhir' => 'required|',
                      'gaji_pokok'=> 'required|',

                       );

        $messages = ['id_cabang.required' => 'Cabang harus diisi',
                     'id_departemen.required' => 'Departemen harus diisi',
                     'id_jabatan.required' => 'Jabatan harus diisi',
                     'tgl_awal.required' => 'Tgl Awal harus diisi',
                     'tgl_akhir.required' => 'Tgl akhir harus diisi',
                     'gaji_pokok.required' => 'Gaji pokok harus diisi',
                    
           ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/kontrakkerja/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKontrakkerja      = new KontrakkerjaModel;
            # ---------------
            $qKontrakkerja->updateData($request);
            # ---------------
            session()->flash("success_message", "Kontrakkerja has been updated");
            # ---------------
            return redirect("/kontrakkerja/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/kontrakkerja/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKontrakkerja             = new KontrakkerjaModel;

       //$idKontrakkerja             = explode("&", $id); 
        $qKontrakkerja        = $qKontrakkerja->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan(); 
		$qStatuskerja          = getSelectStskerja();
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id_kontrakkerja", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qKontrakkerja->no_surat));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qKontrakkerja->id_karyawan));
              
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id_kontrakkerja") != 1) {
            $qKontrakkerja     = new KontrakkerjaModel;
            # ---------------
            $qKontrakkerja->removeData($request);
            # ---------------
            session()->flash("success_message", "Kontrakkerja has been removed");
        } else {
            session()->flash("error_message", "Kontrakkerja cannot be removed");
        }
      # ---------------
        return redirect("/kontrakkerja/index/". $request->input("id_karyawan")); 
    }

   
    public function Cetak( $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCKontrakkerja     = new KontrakkerjaModel;
      /* ----------
        Source
      ----------------------- */
      //$idKontrakkerja     = explode("&", $id); 
      $qKontrakkerja        = $qCKontrakkerja->getCetakKontrak($id);
      $qNama                = $qCKontrakkerja->getCetakKontrak($id)->first();
      $data["pkwt"] =  $qKontrakkerja ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      
        $pdf = PDF::loadView('Personalia.lprkontrak',  $data);
        $nama_file =  $qNama->nama_karyawan;

        //$nama_file =  $qKontrakkerja->nama_karyawan & "-" & $qKontrakkerja->id_kontrakkerja;

      
       $pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprkontrak", $data);

    }
    public function aktivasi($id) 
    {
        $data["title"]        = "Update Status Proses";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/kontrakkerja/updateaktivasi";
        /* ----------
         Kontrakkerja
        ----------------------- */
        $qMaster           = new MasterModel;
        $qKontrakkerja             = new KontrakkerjaModel;
        /* ----------
         Source
        ----------------------- */
        //$idKontrakkerja             = explode("&", $id); 
        $qKontrakkerja        = $qKontrakkerja->getProfile($id)->first();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Kontrakkerja", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly","value"=>$qKontrakkerja->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Tgl Awal", "readonly"=>"readonly","value"=>$qKontrakkerja->tgl_awal));
        $data["fields"][]      = form_text(array("name"=>"tgl_akhir", "label"=>"Tgl Akhir", "readonly"=>"readonly","value"=>$qKontrakkerja->tgl_akhir));
        $data["fields"][]      = form_hidden(array("name"=>"status_kerja", "label"=>"Status", "readonly"=>"readonly","value"=>$qKontrakkerja->statuskerja));
        $data["fields"][]      = form_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>number_format($qKontrakkerja->gaji_pokok,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan","readonly"=>"readonly", "value"=>number_format($qKontrakkerja->tunj_jabatan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport","readonly"=>"readonly", "value"=>number_format($qKontrakkerja->tunj_transport,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga","readonly"=>"readonly", "value"=>number_format($qKontrakkerja->tunj_jaga,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan","readonly"=>"readonly", "value"=>number_format($qKontrakkerja->tunj_kemahalan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_makan", "label"=>"Tnj. Makan","readonly"=>"readonly", "value"=>number_format($qKontrakkerja->tunj_makan,0)));








        $data["fields"][]      = form_text(array("name"=>"status_proses", "label"=>"Status Proses","readonly"=>"readonly","mandatory"=>"yes","value"=>"SELESAI"));
        if ($qKontrakkerja->status_proses=="SELESAI" and $qKontrakkerja->aktif=="TIDAK AKTIF") 
        {
           $data["fields"][]      = form_text(array("name"=>"tgl_tandatangan", "label"=>"Tgl Tanda Tangan","readonly"=>"readonly","value"=>$qKontrakkerja->tgl_tandatangan));
        }
        else
        {
           $data["fields"][]      = form_datepicker(array("name"=>"tgl_tandatangan", "label"=>"Tgl Tanda Tangan", "mandatory"=>"yes","value"=>displayDMY($qKontrakkerja->tgl_tandatangan,"/"), "first_selected"=>"yes"));
        }
        $data["fields"][]     = form_checklist2(array("name"=>"updatekaryawan", "label"=>"Update Data Karyawan", "checked"=>2,"value"=>2 ));
           

        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);    
            

    }

public function updateaktivasi(Request $request)
    {
        
        
        $rules = array(
                    'tgl_tandatangan' => 'required|',
                    
                    'tgl_tandatangan' => 'required|'                          
        );

        $messages = [
                    'tgl_tandatangan.required' => 'Tanggal TTD harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/kontrakkerja/aktif/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKontrakkerja      = new KontrakkerjaModel;
            # ---------------
            $qKontrakkerja->updateaktivasiData($request);
            # ---------------
            session()->flash("success_message", "Kontrakkerja has been updated");
            # ---------------
            return redirect("/kontrakkerja/index/". $request->input("id_karyawan"));
        }
    }
}
