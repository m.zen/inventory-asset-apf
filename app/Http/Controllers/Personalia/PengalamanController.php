<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Master\UserModel;
use App\Model\Personalia\PengalamanModel;
class PengalamanController extends Controller
{
   public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/pengalaman/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qPengalaman              = new PengalamanModel;
        $qMaster                = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        //$data["form_act_add"]  = "/pengalaman/add/".$idkry;
        //$data["form_act_edit"]  = "/pengalaman/add/".$idkry."/".$;

         $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        $data["form_act_add"]    = "/pengalaman/add/".$idkry;
         $data["label_add"]       = "Add";
         $data["form_act_edit"]   = "/pengalaman/edit";
         $data["label_edit"]       = "Edit";
         $data["form_act_delete"] = "/pengalaman/delete";
         $data["label_delete"]    = "Delete";
        
        # ---------------*/
        //Akses user
        $qUser                  = new UserModel;
        $qhakAkses              = $qUser->getAksesuser(Auth::user()->id)->first();
        $data["hakakses"]       = $qhakAkses;
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));


        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);


        $data["tabs"]          = array(array("label"=>"Biodata", "url"=>"/detail/index/".$idkry, "active"=>""),
                                  array("label"=>"Keluarga", "url"=>"/keluarga/index/".$idkry, "active"=>""),
                                 array("label"=>"Kontak Lain", "url"=>"/kontak/index/".$idkry, "active"=>""), 
                                 array("label"=>"Pendidikan", "url"=>"/rpendidikan/index/".$idkry, "active"=>""),
                                 array("label"=>"Pengalaman", "url"=>"/pengalaman/index/".$idkry, "active"=>"Active"),
                                
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_pengalaman"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"Nama Perusahaan"
                                                ,"name"=>"perusahaan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Kota"
                                                ,"name"=>"kota"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Jabatan"
                                                ,"name"=>"jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tahun Awal"
                                                ,"name"=>"tahun_awal"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tahun Akhir"
                                                ,"name"=>"tahun_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     

                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PENGALAMAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PENGALAMAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PENGALAMAN");
        }
        # ---------------
        $data["select"]        = $qPengalaman->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qPengalaman->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.dtlkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Pengalaman";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/pengalaman/save";
        
        $qMaster               = new MasterModel;
        
                
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_text(array("name"=>"perusahaan", "label"=>"Nama Perusahaan", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"kota", "label"=>"Kota", "mandatory"=>"yes"));        
        $data["fields"][]      = form_text(array("name"=>"jabatan", "label"=>"Jabatan", "mandatory"=>"yes"));        
        $data["fields"][]      = form_text(array("name"=>"tahun_awal", "label"=>"Tahun Awal", "mandatory"=>"yes","value"=>date("Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"tahun_akhir", "label"=>"Tahun Akhir", "mandatory"=>"yes","value"=>date("Y"), "first_selected"=>"yes"));
            # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



    public function save(Request $request) {
       

        $rules = array(
                      
                      "perusahaan" => 'required'                     );

        $messages = ['nama_pengalaman.required' => 'Pengalaman harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pengalaman/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else {
            $qPengalaman  = new PengalamanModel;
            # ---------------
            $qPengalaman->createData($request);
            # ---------------
            session()->flash("success_message", "Pengalaman has been saved");
            # ---------------
            return redirect("/pengalaman/index/". $request->id_karyawan);
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Pengalaman";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/pengalaman/update";
        /* ----------
         Pengalaman
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPengalaman             = new PengalamanModel;
        /* ----------
         Source
        ----------------------- */
        //$idpengalaman             = explode("&", $id); 
        $qPengalaman           = $qPengalaman->getProfile($id)->first();
      
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Pengalaman", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qPengalaman->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"perusahaan", "label"=>"Nama Perusahaan", "mandatory"=>"yes","value"=>$qPengalaman->perusahaan ));
        $data["fields"][]      = form_text(array("name"=>"kota", "label"=>"Kota", "mandatory"=>"yes","value"=>$qPengalaman->kota));        
        $data["fields"][]      = form_text(array("name"=>"jabatan", "label"=>"Jabatan", "mandatory"=>"yes","value"=>$qPengalaman->jabatan));        
        $data["fields"][]      = form_text(array("name"=>"tahun_awal", "label"=>"Tahun Awal", "mandatory"=>"yes","value"=>$qPengalaman->tahun_awal));
        $data["fields"][]      = form_text(array("name"=>"tahun_akhir", "label"=>"Tahun Akhir", "mandatory"=>"yes","value"=>$qPengalaman->tahun_akhir));
            # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'perusahaan' => 'required|',
                    
                    'jabatan' => 'required|'                          
        );

        $messages = [
                    'perusahaan.required' => 'Nama Pengalaman harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pengalaman/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPengalaman      = new PengalamanModel;
            # ---------------
            $qPengalaman->updateData($request);
            # ---------------
            session()->flash("success_message", "Pengalaman has been updated");
            # ---------------
            return redirect("/pengalaman/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/pengalaman/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPengalaman             = new PengalamanModel;

       //$idpengalaman             = explode("&", $id); 
        $qPengalaman              = $qPengalaman->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubungan();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusPengalaman();
        $qPendidikan           = getSelectPendidikan();  
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Pengalaman", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qPengalaman->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
      
        $data["fields"][]      = form_text(array("name"=>"nama_pengalaman", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qPengalaman->nama_pengalaman));
        $data["fields"][]      = form_select(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes", "source"=>$qHubungan, "value"=>$qPengalaman->hubungan));        
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qPengalaman     = new PengalamanModel;
            # ---------------
            $qPengalaman->removeData($request);
            # ---------------
            session()->flash("success_message", "Pengalaman has been removed");
        } else {
            session()->flash("error_message", "Pengalaman cannot be removed");
        }
      # ---------------
        return redirect("/pengalaman/index/". $request->input("id_karyawan")); 
    }
}
