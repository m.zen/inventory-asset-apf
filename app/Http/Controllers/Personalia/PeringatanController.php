<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use PDF;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\PeringatanModel;
class PeringatanController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/peringatan/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qPeringatan          = new PeringatanModel;
        $qMaster                = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qKaryawan                = $qMaster->getKaryawan($idkry)->first();
        $data["form_act_add"]     = "/peringatan/add/".$idkry;
        $data["form_act_edit"]    = "/peringatan/edit";
        $data["form_act_delete"]  = "/peringatan/delete";
        $data["form_act_print"]  = "/peringatan/cetak";
        $data["form_act_aktifasi"]  = "/peringatan/aktif";
       
       
       
        
        # ---------------*/
        
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));
         

        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"Status Kerja", "url"=>"/peringatan/index", "active"=>""),
                                array("label"=>"PKWTT (Tetap)", "url"=>"/sktetap/index/".$idkry, "active"=>""), 
                                 array("label"=>"Mutasi", "url"=>"/karir/index/".$idkry, "active"=>""), 
                                 array("label"=>"Peringatan", "url"=>"/peringatan/index/".$idkry, "active"=>"active"), 
                                 
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_peringatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"No Surat"
                                                ,"name"=>"no_surat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Jenis Peringatan"
                                                ,"name"=>"jenis_peringatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
      
                                      array("label"=>"Tgl Berlaku"
                                                ,"name"=>"tgl_berlaku"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                        array("label"=>"Tgl Terima"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                         array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                        

                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PERINGATAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERINGATAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERINGATAN");
        }
        # ---------------
        $data["select"]        = $qPeringatan->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qPeringatan->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.karirkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Peringatan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/peringatan/save";
        
        $qMaster               = new MasterModel;
        $qPeringatanmdl      = new PeringatanModel;
        $qSP                 = getSelectPeringatan();
        
        /* ----------
         Source
        ----------------------- */
        //$idPeringatan             = explode("&", $id); 
       // $qPeringatan        = $qPeringatanmdl->getProfile($id)->first();
           
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"jenis_peringatan", "label"=>"Jenis Peringatan", "mandatory"=>"yes", "source"=>$qSP));        
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Alasan", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Alasan", "mandatory"=>"yes"));
        
        

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



    public function save(Request $request) {
       

        $rules = array(
                      
                      'no_surat' => 'required'                     );

        $messages = ['no_surat.required' => 'No Surat harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/peringatan/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPeringatan  = new PeringatanModel;
            

            if($qPeringatan-> CekSP($request->no_surat) > 0)
            {

                session()->flash("error_message", "No Surat Sudah digunakan");
                return redirect("/peringatan/add/". $request->id_karyawan);
            }
            else
            {
                 $qPeringatan->createData($request);
                 session()->flash("success_message", "Wilayah has been saved");
                 return redirect("/peringatan/index/". $request->id_karyawan);

            }



        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Peringatan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/peringatan/update";
        /* ----------
         Peringatan
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPeringatan      = new PeringatanModel;
        /* ----------
         Source
        ----------------------- */
        //$idPeringatan             = explode("&", $id); 
        $qPeringatan          = $qPeringatan->getProfile($id)->first();
        $qSP                  = getSelectPeringatan();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Peringatan", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qPeringatan->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qPeringatan->no_surat));
        $data["fields"][]      = form_select(array("name"=>"jenis_peringatan", "label"=>"Jenis Peringatan", "mandatory"=>"yes", "source"=>$qSP,"value"=>$qPeringatan->jenis_peringatan));        
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_berlaku,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_akhir,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Alasan", "mandatory"=>"yes","value"=>$qPeringatan->keterangan));
        
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/peringatan/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPeringatan      = new PeringatanModel;
            # ---------------
            $qPeringatan->updateData($request);
            # ---------------
            session()->flash("success_message", "Peringatan has been updated");
            # ---------------
            return redirect("/peringatan/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/peringatan/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPeringatan             = new PeringatanModel;

       //$idPeringatan             = explode("&", $id); 
        $qPeringatan        = $qPeringatan->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan(); 
		$qStatuskerja          = getSelectStskerja();
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id_peringatan", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qPeringatan->no_surat));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qPeringatan->id_karyawan));
              
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id_peringatan") != 1) {
            $qPeringatan     = new PeringatanModel;
            # ---------------
            $qPeringatan->removeData($request);
            # ---------------
            session()->flash("success_message", "Peringatan has been removed");
        } else {
            session()->flash("error_message", "Peringatan cannot be removed");
        }
      # ---------------
        return redirect("/peringatan/index/". $request->input("id_karyawan")); 
    }

    public function Cetak( $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCPeringatan      = new PeringatanModel;
        /* ----------
         Source
                 ----------------------- */
        //$idPeringatan             = explode("&", $id); 
       // $qPeringatan          = $qPeringatan->getProfile($id)->first();
       
      /* ----------
        Source
      ----------------------- */

      
      //$idKontrakkerja     = explode("&", $id); 
      $qDisiplinSP       = $qCPeringatan->getCetaksp($id);
      $qNama             = $qCPeringatan->getCetaksp($id)->first();
      $data["pkwt"]      = $qDisiplinSP ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      
        $pdf = PDF::loadView('Personalia.lprsrtperingatan',  $data);
        $nama_file =  $qNama->nama_karyawan;

        //$nama_file =  $qKontrakkerja->nama_karyawan & "-" & $qKontrakkerja->id_kontrakkerja;

      
       $pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprkontrak", $data);

    }
    public function aktivasi($id) 
    {
        $data["title"]        = "Update Status Proses";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/peringatan/updateaktivasi";
        /* ----------
         Peringatan
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPeringatan             = new PeringatanModel;
        /* ----------
         Source
        ----------------------- */
        //$idPeringatan             = explode("&", $id); 
        $qPeringatan        = $qPeringatan->getProfile($id)->first();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Peringatan", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly","value"=>$qPeringatan->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Tgl Awal", "readonly"=>"readonly","value"=>$qPeringatan->tgl_awal));
        $data["fields"][]      = form_text(array("name"=>"tgl_akhir", "label"=>"Tgl Akhir", "readonly"=>"readonly","value"=>$qPeringatan->tgl_akhir));
        $data["fields"][]      = form_text(array("name"=>"status_kerja", "label"=>"Status", "readonly"=>"readonly","value"=>$qPeringatan->statuskerja));
     
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "readonly"=>"readonly","value"=>$qPeringatan->no_surat));
        $data["fields"][]      = form_text(array("name"=>"status_proses", "label"=>"Status Proses","readonly"=>"readonly","mandatory"=>"yes","value"=>"SELESAI"));
        if ($qPeringatan->status_proses=="SELESAI" and $qPeringatan->aktiv=="TIDAK AKTIF") 
        {
           $data["fields"][]      = form_text(array("name"=>"tgl_tandatangan", "label"=>"Tgl Tanda Tangan","readonly"=>"readonly","value"=>$qPeringatan->tgl_tandatangan));
        }
        else
        {
           $data["fields"][]      = form_datepicker(array("name"=>"tgl_tandatangan", "label"=>"Tgl Tanda Tangan", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_tandatangan,"/"), "first_selected"=>"yes"));
        }
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);    
            

    }

public function updateaktivasi(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/peringatan/aktivasi/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPeringatan      = new PeringatanModel;
            # ---------------
            $qPeringatan->updateaktivasiData($request);
            # ---------------
            session()->flash("success_message", "Peringatan has been updated");
            # ---------------
            return redirect("/peringatan/index/". $request->input("id_karyawan"));
        }
    }
}
