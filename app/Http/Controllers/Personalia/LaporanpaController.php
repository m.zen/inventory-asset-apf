<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Payroll\ProsesgajiModel;
use App\Model\Personalia\LaporanpaModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
class LaporanpaController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function karyawanbaru_index() {
        $data["title"]         = "Laporan Karyawan Masuk";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lprkaryawanbaru/cetak";
        /* ----------
         Model
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
          $qMaster               = new MasterModel;
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qKaryawan             = $qMaster->getSelectKaryawan();
        $qKaryawan             = array_merge($collection,$qKaryawan);
        $qCabangs              = $qMaster->getSelectCabang();
        $qCabangs              = array_merge($collection,$qCabangs);
        $qDepartemens          = $qMaster->getSelectDepartemen();  
    $qDepartemens          = array_merge($collection,$qDepartemens);
        $qJabatan              = $qMaster->getSelectJabatan(); 
    $qJabatan              = array_merge($collection,$qJabatan);
         $qJenisLaporan         = getFilelaporan();
        $data["fields"][]      = form_datepicker(array("name"=>"TglAwal", "label"=>"Tanggal Awal", "mandatory"=>"yes", "value"=>date("01/m/Y")));
        $data["fields"][]      = form_datepicker(array("name"=>"TglAkhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes", "value"=>date("d/m/Y")));
 $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>""));

         $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qJenisLaporan, "value"=>"pdf"));

        # ---------------
        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }
    public function karyawanbaru_cetak(Request $request) {
        /* ----------
         Model
        ----------------------- */
        if ($request->jenis_laporan=="pdf")
        {
                 $qLaporan              = new LaporanpaModel;
                /* ----------
                 Data
                ----------------------- */
                $qKarawanmasuk         = $qLaporan->getKaryawanmasuk($request);
                $Line                  = 190;
                $No                    = 0;
                /* ----------
                 Report
                ----------------------- */
                Fpdf::AddPage();
                # ----------------
                Fpdf::SetLineWidth(0.3);
                Fpdf::SetFont("Arial", "B", 11);
                Fpdf::Ln(7);
                //$logo = public_path("logo/".$qDataDS->Logo);
                //Fpdf::Image($logo, 170, 13, 23, 20);
                Fpdf::Cell(190, 5, "PT ARTHAPRIMA FINANCE", 0, 0, 'C');
                Fpdf::Ln(5);
                Fpdf::Cell(190, 5,"DATA KARYAWAN MASUK", 0, 0, 'C');
                Fpdf::Ln(10);
                if(count( $qKarawanmasuk) == 0) {
                    Fpdf::SetFont("Arial", "", 9);
                    Fpdf::Cell(190, 5, "Tidak ada karyawan masuk diperiode tersebut", 0, 0, 'L');
                    Fpdf::Ln();
                    Fpdf::Cell(190, 5, "Terima Kasih", 0, 0, 'L');
                    # ----------------
                    Fpdf::Output(); exit;
                } else 
                {

                    Fpdf::SetFont("Arial", "", 8);
                    Fpdf::Cell(15, 5, 'Tanggal', 0, 0, 'L');
                    Fpdf::Cell(5, 5, ':', 0, 0, 'L');
                    Fpdf::Cell(18, 5, date("d/m/Y", strtotime(setYMD($request->TglAwal,"/"))), 0, 0, 'L');
                    Fpdf::Cell(7, 5, 'S.D', 0, 0, 'L');
                    Fpdf::Cell(142, 5, date("d/m/Y", strtotime(setYMD($request->TglAkhir,"/"))), 0, 0, 'L');
                    Fpdf::Ln(7);
                    Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
                    Fpdf::Ln(2);
                    Fpdf::SetFont("Arial", "B", 8);
                    Fpdf::Cell(7, 5, 'NO.', 0, 0, 'C');
                    Fpdf::Cell(30,5, 'NIK', 0, 0, 'L');
                    Fpdf::Cell(50, 5, 'NAMA KARYAWAN', 0, 0, 'L');
                    Fpdf::Cell(50, 5, 'JABATAN', 0, 0, 'L');
                    Fpdf::Cell(35, 5, 'CABANG', 0, 0, 'L');
                    Fpdf::Cell(20, 5, 'TGL MASUK', 0, 0, 'C');
                    Fpdf::Ln(7);
                    Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
                    Fpdf::Ln(3);
                    foreach ($qKarawanmasuk as $row) 
                    {
                        $No = $No + 1;
                        Fpdf::SetFont("Arial", "", 8);
                        Fpdf::Cell(7, 5, $No, 0, 0, 'C');
                        Fpdf::Cell(30, 5, $row->nik, 0, 0, 'L');
                        Fpdf::Cell(50, 5, $row->nama_karyawan, 0, 0, 'L');
                         Fpdf::Cell(50, 5, $row->nama_jabatan, 0, 0, 'L');
                        Fpdf::Cell(35, 5, $row->nama_cabang, 0, 0, 'L');
                        Fpdf::Cell(20, 5,  $row->tgl_masuk, 0, 0, 'C');
                        Fpdf::Ln(8);
                       
                        }
                      
                    }
                    
                    Fpdf::Output(); exit;
        }
        else
        {

               $qLaporan              = new LaporanpaModel;
               $qKarawanmasuk         = $qLaporan->getKaryawanmasuk($request);

                    
              $styleArray = [
                  'font' => [
                      'bold' => true,
                  ],
                  'alignment' => [
                      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                  ],
                  'borders' => [
                      'top' => [
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      ],
                  ],
                  'fill' => [
                      'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                      'rotation' => 90,
                      'startColor' => [
                          'argb' => 'FFA0A0A0',
                      ],
                      'endColor' => [
                          'argb' => 'FFFFFFFF',
                      ],
                  ],
              ];






                                        
                    $spreadsheet = new Spreadsheet();
                    $sheet = $spreadsheet->getActiveSheet();
                    $spreadsheet->getActiveSheet()->getStyle('A3:I3')->applyFromArray($styleArray);
                   
                  //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
              
                    $sheet->setCellValue('A1', 'DAFTAR KAYAWAN MASUK');
                    $sheet->setCellValue('A2', 'PERIODE : '.$request->TglAwal ." - ".$request->TglAkhir);
                    
                    $baris = 4;
                    /*-------------------------
                    KOLOM DATA
                    ---------------------------*/
                      
                    $sheet->setCellValue('A3', 'NO.');    
                    $sheet->setCellValue('B3', 'N I K');
                    $sheet->setCellValue('C3', 'NAMA');
                    $sheet->setCellValue('D3', 'JABATAN');
                    $sheet->setCellValue('E3', 'DEPARTEMEN');
                    $sheet->setCellValue('F3', 'CABANG');
                    $sheet->setCellValue('G3', 'STATUS KERJA');
                    $sheet->setCellValue('H3', 'TANGGAL MASUK');
                    $sheet->setCellValue('I3', 'TANGGAL KELUAR');
                   
                    foreach($qKarawanmasuk as $row) 
                    {
                             
                              $sheet->setCellValue('A'.$baris,$baris-3);
                              $sheet->setCellValue('B'.$baris,$row->nik);
                              $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                              $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                              $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                              $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                              $sheet->setCellValue('G'.$baris,$row->status_kerja);
                              $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                              $sheet->setCellValue('I'.$baris,$row->tgl_keluar);
                              


                              //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
                              


                             $baris=$baris+1;
                    }
                            

                    

                    //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
                    foreach(range('B','I') as $columnID)
                     {
                         $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                              ->setAutoSize(true);
                         $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
                              ->setAutoSize(true);     
                              


                      }

                    
                    
                    



                       $spreadsheet->getActiveSheet()->getColumnDimension("AA")
                              ->setAutoSize(true);
                        $writer = new Xlsx($spreadsheet);
                        // $writer->save('data karyawanlengkap.xlsx');
                        $judul    = "Karyawanbaru";  
                        $name_file = $judul.'.xlsx';
                      // $path = storage_path('Laporan\\'.$name_file);
                      $path = public_path().'/app/'.$name_file;
                      $contents = is_dir($path);
                      // $headers = array('Content-Type' => File::mimeType($path));
                      // dd($path.'/'.$name_file,$contents);
                      $writer->save($path);
                      $filename   = str_replace("@", "/", $path);
                    # ---------------
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-Disposition: attachment; filename=".$name_file);
                    header("Content-Type: application/xlsx");
                    header("Content-Transfer-Encoding: binary");
                    # ---------------
                    require "$filename";
                    # ---------------
                    exit;
             }
       
        
    }


     public function laporansumm_index() {
        $data["title"]         = "Laporan Resume";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lprresume/cetakresume";
        /* ----------
         Model
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
         $qMaster               = new MasterModel;
         $collection            = [ (object)
                                    [
                                    'id' => '1',
                                    'name' => 'Pusat'
                                    ],
                                    [
                                    'id' => '2',
                                    'name' => 'Cabang'
                                    ]
                                ];
     
        $data["fields"][]      = form_datepicker(array("name"=>"TglAwal", "label"=>"Tanggal Awal", "mandatory"=>"yes", "value"=>date("01/m/Y")));
        $data["fields"][]      = form_datepicker(array("name"=>"TglAkhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes", "value"=>date("d/m/Y")));
 
         $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis",  "source"=>$collection, "value"=>"1"));

        # ---------------
          $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Show&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }


    public function resume_cetak(Request $request) {
       
       


              $styleArray = [
                  'font' => [
                      'bold' => true,
                  ],
                  'alignment' => [
                      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                  ],
                  'borders' => [
                      'top' => [
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      ],
                  ],
                  'fill' => [
                      'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                      'rotation' => 90,
                      'startColor' => [
                          'argb' => 'FFA0A0A0',
                      ],
                      'endColor' => [
                          'argb' => 'FFFFFFFF',
                      ],
                  ],
              ];






                                        
                    $spreadsheet = new Spreadsheet();
                    $sheet = $spreadsheet->getActiveSheet();
                   // $spreadsheet->getActiveSheet()->getStyle('A3:J3')->applyFromArray($styleArray);
                   
                  //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
              

                    
                    if($request->jenis_laporan==2)
                    {
                        $sheet->setCellValue('A1', 'LAPORAN RESUME (CABANG)');
                       $sheet->setCellValue('A2', 'PERIODE : '.$request->TglAwal ." - ".$request->TglAkhir);

                         $baris = 4;



                     //REFRESH DATA

                        


                          $query  = DB::table("m_inisialjabatan")
                            ->select("nama_inisialjabatan","kategoricabang","id_departemen","id_jabatan")
                             ->where("kategoricabang", "Cabang");
                            

                            
                     $tInisial=$query->get();

                           // dd($tInisial);
                    
                     foreach($tInisial as $row) 

                     {

                         DB::table("p_karyawan as a")
                         ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                         ->where("b.kategori", "Cabang")
                         ->where("a.id_departemen", $row->id_departemen)
                         ->where("a.id_jabatan", $row->id_jabatan)
                         ->where("a.aktif", 1)
                         ->update([  "a.nama_inisialjabatan"=>$row->nama_inisialjabatan,
                                        "a.user_id"=>setString(Auth::user()->id),
                                         "a.update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);

                     }
                     




                    /*-------------------------
                    KOLOM DATA
                    ---------------------------*/
                    // Jabatan
                    $query  = DB::table("p_karyawan as a")
                            ->select("a.nama_inisialjabatan","b.id_level")
                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif", 1)
                            ->where("a.nama_inisialjabatan", "<>","")
                            
                            ->where("id_cabang","!=",1)
                            ->orderBy("b.id_level","DESC")
                            ->groupBy("b.id_level")
                            ->groupBy("a.nama_inisialjabatan");
                          
                            
                            
                     $tJabatan=$query->get();

                  
                    
                     $kolom = "B";
                     foreach($tJabatan as $row) 

                     {
                          
                        
                           $sheet->setCellValue( $kolom .'4', $row->nama_inisialjabatan);  
                            $kolom++;

                     }
                     
                     $sheet->setCellValue( $kolom .'4', "Total");  
                           



                     // Cabang

                     $query  = DB::table("p_karyawan as a")
                            ->select("b.nama_cabang")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->where("a.aktif", 1)
                            ->where("a.id_cabang","!=",1)
                             ->orderBy("b.id_cabang","ASC")
                           
                            ->groupBy("b.id_cabang","b.nama_cabang");
                     $tCabang=$query->get();
                     $baris= 5;

                     foreach($tCabang as $row) 

                     {
                          
                        
                                //Nama Cabang
                                 $sheet->setCellValue( 'A' .$baris, $row->nama_cabang);  
                                

                                 //Cari JUmlah Jabatan
                                  $query  = DB::table("p_karyawan as a")
                                  ->select("a.nama_inisialjabatan")
                                  ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                                  ->where("a.aktif", 1)
                                  ->where("a.nama_inisialjabatan", "<>","")
                                  
                                  ->where("id_cabang","!=",1)
                                  ->orderBy("b.id_level","DESC")
                                  ->groupBy("b.id_level")
                                  ->groupBy("a.nama_inisialjabatan");
                                   $tJabatan=$query->get();
                                   $kolom = "B";
                                   $totalkry=0;
                                   foreach($tJabatan as $row2) 

                                   {
                                        
                                        //Cari Jumlah jabatan di Data Karyawan Aktif
                                        $query  = DB::table("p_karyawan as a")
                                            ->select(DB::raw('count(a.nama_inisialjabatan) as jumlah_jabatan'))
                                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                                            ->leftjoin("m_cabang as c","c.id_cabang","=","a.id_cabang")
                                            ->where("a.aktif", 1)
                                            ->where("a.id_cabang","!=",1)
                                            ->where("c.nama_cabang",$row->nama_cabang)
                                            ->where("a.nama_inisialjabatan",$row2->nama_inisialjabatan)
                                            ->groupBy("a.nama_inisialjabatan");

                                       $sumJabatan=$query->get()->first();
                                        $jml =0;
                                       if($sumJabatan == null)
                                       {
                                          $jml =0;
                                       }
                                       else
                                       {
                                         $jml =$sumJabatan->jumlah_jabatan;
                                       }
                                       $sheet->setCellValue( $kolom .$baris,$jml  );  
                                       $totalkry=$totalkry+ $jml;
                                       $kolom++;

                                   }
                                   $sheet->setCellValue( $kolom .$baris,$totalkry  ); 
                                    $baris++;
                           
                     }

                    }
                    else
                    {
                       $sheet->setCellValue('A1', 'LAPORAN RESUME (PUSAT)');
                       $sheet->setCellValue('A2', 'PERIODE : '.$request->TglAwal ." - ".$request->TglAkhir);

                       
                       //REFRESH DATA



                  $query  = DB::table("m_inisialjabatan")
                            ->select("nama_inisialjabatan","kategoricabang","id_departemen","id_jabatan")
                             ->where("nama_inisialjabatan", "Pusat");
                            
                            
                     $tInisial=$query->get();
                    
                     foreach($tInisial as $row) 

                     {

                         DB::table("p_karyawan as a")
                        ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")

                             ->where("b.kategori", "Pusat")
                             ->where("a.id_jabatan",  $row->id_jabatan)
                             ->where("a.aktif", 1)
                            ->update([  "a.nama_inisialjabatan"=>$row->nama_inisialjabatan,
                                        "a.user_id"=>setString(Auth::user()->id),
                                         "a.update_at"=>setString(date('Y-m-d H:i:s'))    
                                      ]);

                     }


                    /*-------------------------
                    KOLOM DATA
                    ---------------------------*/
                 
                         $baris = 4;
                    /*-------------------------
                    KOLOM DATA
                    ---------------------------*/
                    // Jabatan
                    $query  = DB::table("p_karyawan as a")
                            ->select("a.nama_inisialjabatan","b.id_level")
                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif", 1)
                            ->where("a.nama_inisialjabatan", "<>","")
                            
                            ->where("id_cabang","=",1)
                            ->orderBy("b.id_level","DESC")
                            ->groupBy("b.id_level")
                           
                            ->groupBy("a.nama_inisialjabatan");
                            
                            
                     $tJabatan=$query->get();
                    
                     $kolom = "B";
                     foreach($tJabatan as $row) 

                     {
                          
                        
                           $sheet->setCellValue( $kolom .'4', $row->nama_inisialjabatan);  
                            $kolom++;

                     }
                     
                     $sheet->setCellValue( $kolom .'4', "Total");  
                           



                     // Cabang

                     $query  = DB::table("p_karyawan as a")
                            ->select("b.nama_departemen")
                            ->leftjoin("m_departemen as b","b.id_departemen","=","a.id_departemen")
                            ->where("a.aktif", 1)
                            ->where("a.id_cabang","=",1)
                             ->orderBy("b.id_departemen","ASC")
                           
                            ->groupBy("b.id_departemen","b.nama_departemen");
                     $tCabang=$query->get();
                     $baris= 5;

                     foreach($tCabang as $row) 

                     {
                          
                        
                                //Nama Cabang
                                 $sheet->setCellValue( 'A' .$baris, $row->nama_departemen);  
                                

                                 //Cari JUmlah Jabatan
                                  $query  = DB::table("p_karyawan as a")
                                  ->select("a.nama_inisialjabatan")
                                  ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                                  ->where("a.aktif", 1)
                                  ->where("a.nama_inisialjabatan", "<>","")
                                  
                                  ->where("id_cabang","=",1)
                                  ->orderBy("b.id_level","DESC")
                                  ->groupBy("b.id_level")
                                  ->groupBy("a.nama_inisialjabatan");
                                   $tJabatan=$query->get();
                                   $kolom = "B";
                                   $totalkry=0;
                                   foreach($tJabatan as $row2) 

                                   {
                                        
                                        //Cari Jumlah jabatan di Data Karyawan Aktif
                                        $query  = DB::table("p_karyawan as a")
                                            ->select(DB::raw('count(a.nama_inisialjabatan) as jumlah_jabatan'))
                                            ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                                            ->where("a.aktif", 1)
                                            ->where("a.id_cabang","=",1)
                                            ->where("c.nama_departemen",$row->nama_departemen)
                                            ->where("a.nama_inisialjabatan",$row2->nama_inisialjabatan)
                                            ->groupBy("a.nama_inisialjabatan");

                                       $sumJabatan=$query->get()->first();
                                        $jml =0;
                                       if($sumJabatan == null)
                                       {
                                          $jml =0;
                                       }
                                       else
                                       {
                                         $jml =$sumJabatan->jumlah_jabatan;
                                       }
                                       $sheet->setCellValue( $kolom .$baris,$jml  );  
                                       $totalkry=$totalkry+ $jml;
                                       $kolom++;

                                   }
                                   $sheet->setCellValue( $kolom .$baris,$totalkry  ); 
                                    $baris++;
                           
                     }

                    }
                    
                    
                      
                   //  $sheet->setCellValue('A3', 'NO.');    
                   //  $sheet->setCellValue('B3', 'N I K');
                   //  $sheet->setCellValue('C3', 'NAMA');
                   //  $sheet->setCellValue('D3', 'JABATAN');
                   //  $sheet->setCellValue('E3', 'DEPARTEMEN');
                   //  $sheet->setCellValue('F3', 'CABANG');
                   //  $sheet->setCellValue('G3', 'STATUS KERJA');
                   //  $sheet->setCellValue('H3', 'TANGGAL MASUK');
                   //  $sheet->setCellValue('I3', 'TANGGAL KELUAR');
                   //  $sheet->setCellValue('J3', 'ALASAN KELAUAR');
                   // if(!empty($qKarawanmasuk))
                   // {
                   //      foreach($qKarawanmasuk as $row) 
                   //  {
                             
                   //            $sheet->setCellValue('A'.$baris,$baris-3);
                   //            $sheet->setCellValue('B'.$baris,$row->nik);
                   //            $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                   //            $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                   //            $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                   //            $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                   //            $sheet->setCellValue('G'.$baris,$row->status_kerja);
                   //            $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                   //            $sheet->setCellValue('I'.$baris,$row->tgl_keluar);
                   //            $sheet->setCellValue('J'.$baris,$row->alasan_keluar);
                              


                   //            //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
                              


                   //           $baris=$baris+1;
                   //  }
                            

                    

                    // //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
                    // foreach(range('B','J') as $columnID)
                    //  {
                    //      $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    //           ->setAutoSize(true);
                    //      $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
                    //           ->setAutoSize(true);     
                              


                    //   }


                   // }
                    
                    
                    
                    



                   //     $spreadsheet->getActiveSheet()->getColumnDimension("AA")
                   //            ->setAutoSize(true);
                        $writer = new Xlsx($spreadsheet);
                        // $writer->save('data karyawanlengkap.xlsx');
                        $judul    = "Laporan Resume";  
                        $name_file = $judul.'.xlsx';
                      // $path = storage_path('Laporan\\'.$name_file);
                      $path = public_path().'/app/'.$name_file;
                      $contents = is_dir($path);
                      // $headers = array('Content-Type' => File::mimeType($path));
                      // dd($path.'/'.$name_file,$contents);
                      $writer->save($path);
                      $filename   = str_replace("@", "/", $path);
                    # ---------------
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-Disposition: attachment; filename=".$name_file);
                    header("Content-Type: application/xlsx");
                    header("Content-Transfer-Encoding: binary");
                    # ---------------
                    require "$filename";
                    # ---------------
                    exit;



              

        
    }

     public function karyawankeluar_index() {
        $data["title"]         = "Laporan Karyawan Keluar";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lprkaryawankeluar/cetak";
        /* ----------
         Model
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
        $qMaster               = new MasterModel;
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qKaryawan             = $qMaster->getSelectKaryawan();
        $qKaryawan             = array_merge($collection,$qKaryawan);
        $qCabangs              = $qMaster->getSelectCabang();
        $qCabangs              = array_merge($collection,$qCabangs);
        $qDepartemens          = $qMaster->getSelectDepartemen();  
    $qDepartemens          = array_merge($collection,$qDepartemens);
        $qJabatan              = $qMaster->getSelectJabatan(); 
    $qJabatan              = array_merge($collection,$qJabatan);

        $qJenisLaporan         = getFilelaporan();
        $data["fields"][]      = form_datepicker(array("name"=>"TglAwal", "label"=>"Tanggal Awal", "mandatory"=>"yes", "value"=>date("01/m/Y")));
        $data["fields"][]      = form_datepicker(array("name"=>"TglAkhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes", "value"=>date("d/m/Y")));
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>""));
        $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qJenisLaporan, "value"=>"pdf"));

        # ---------------
        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }
    public function karyawankeluar_cetak(Request $request) {
       
        if ($request->jenis_laporan=="pdf")
        {


                    
                    $qLaporan              = new LaporanpaModel;
                    /* ----------
                     Data
                    ----------------------- */
                    $qKarawankeluar         = $qLaporan->getKaryawankeluar($request);
                    $Line                  = 190;
                    $No                    = 0;
                    /* ----------
                     Report
                    ----------------------- */
                    Fpdf::AddPage();
                    # ----------------
                    Fpdf::SetLineWidth(0.3);
                    Fpdf::SetFont("Arial", "B", 11);
                    Fpdf::Ln(7);
                    //$logo = public_path("logo/".$qDataDS->Logo);
                    //Fpdf::Image($logo, 170, 13, 23, 20);
                    Fpdf::Cell(190, 5, "PT ARTHAPRIMA FINANCE", 0, 0, 'C');
                    Fpdf::Ln(5);
                    Fpdf::Cell(190, 5,"DATA KARYAWAN KELUAR", 0, 0, 'C');
                    Fpdf::Ln(10);
                    if(count( $qKarawankeluar) == 0) {
                        Fpdf::SetFont("Arial", "", 9);
                        Fpdf::Cell(190, 5, "Tidak ada karyawan keluar diperiode tersebut", 0, 0, 'L');
                        Fpdf::Ln();
                        Fpdf::Cell(190, 5, "Terima Kasih", 0, 0, 'L');
                        # ----------------
                        Fpdf::Output(); exit;
                    } else 
                    {

                        Fpdf::SetFont("Arial", "", 8);
                        Fpdf::Cell(15, 5, 'Tanggal', 0, 0, 'L');
                        Fpdf::Cell(5, 5, ':', 0, 0, 'L');
                        Fpdf::Cell(18, 5, date("d/m/Y", strtotime(setYMD($request->TglAwal,"/"))), 0, 0, 'L');
                        Fpdf::Cell(7, 5, 'S.D', 0, 0, 'L');
                        Fpdf::Cell(142, 5, date("d/m/Y", strtotime(setYMD($request->TglAkhir,"/"))), 0, 0, 'L');
                        Fpdf::Ln(7);
                        Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
                        Fpdf::Ln(2);
                        Fpdf::SetFont("Arial", "B", 8);
                        Fpdf::Cell(7, 5, 'NO.', 0, 0, 'C');
                        Fpdf::Cell(30,5, 'NIK', 0, 0, 'L');
                        Fpdf::Cell(50, 5, 'NAMA KARYAWAN', 0, 0, 'L');
                        Fpdf::Cell(35, 5, 'CABANG', 0, 0, 'L');
                         Fpdf::Cell(20, 5, 'TGL MASUK', 0, 0, 'C');
                        Fpdf::Cell(20, 5, 'TGL KELUAR', 0, 0, 'C');
                        Fpdf::Cell(40, 5, 'ALASAN', 0, 0, 'L');
                        
                        Fpdf::Ln(7);
                        Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
                        Fpdf::Ln(3);
                        foreach ($qKarawankeluar as $row) 
                        {
                            $No = $No + 1;
                            Fpdf::SetFont("Arial", "", 8);
                            Fpdf::Cell(7, 5, $No, 0, 0, 'C');
                            Fpdf::Cell(30, 5, $row->nik, 0, 0, 'L');
                            Fpdf::Cell(50, 5, $row->nama_karyawan, 0, 0, 'L');
                            Fpdf::Cell(35, 5, $row->nama_cabang, 0, 0, 'L');
                            Fpdf::Cell(20, 5,  $row->tgl_masuk, 0, 0, 'C');
                            Fpdf::Cell(20, 5,  $row->tgl_keluar, 0, 0, 'C');
                            Fpdf::Cell(50, 5, $row->alasan_keluar, 0, 0, 'L');
                           
                            Fpdf::Ln(8);
                           
                            }
                          
                        }
                        
                        Fpdf::Output(); exit;

        }
        else
        {


               $qLaporan              = new LaporanpaModel;
               $qKarawanmasuk         = $qLaporan->getKaryawankeluar($request);

                    
              $styleArray = [
                  'font' => [
                      'bold' => true,
                  ],
                  'alignment' => [
                      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                  ],
                  'borders' => [
                      'top' => [
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      ],
                  ],
                  'fill' => [
                      'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                      'rotation' => 90,
                      'startColor' => [
                          'argb' => 'FFA0A0A0',
                      ],
                      'endColor' => [
                          'argb' => 'FFFFFFFF',
                      ],
                  ],
              ];






                                        
                    $spreadsheet = new Spreadsheet();
                    $sheet = $spreadsheet->getActiveSheet();
                    $spreadsheet->getActiveSheet()->getStyle('A3:J3')->applyFromArray($styleArray);
                   
                  //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
              
                    $sheet->setCellValue('A1', 'DAFTAR KAYAWAN KELUAR');
                    $sheet->setCellValue('A2', 'PERIODE : '.$request->TglAwal ." - ".$request->TglAkhir);
                    
                    $baris = 4;
                    /*-------------------------
                    KOLOM DATA
                    ---------------------------*/
                      
                    $sheet->setCellValue('A3', 'NO.');    
                    $sheet->setCellValue('B3', 'N I K');
                    $sheet->setCellValue('C3', 'NAMA');
                    $sheet->setCellValue('D3', 'JABATAN');
                    $sheet->setCellValue('E3', 'DEPARTEMEN');
                    $sheet->setCellValue('F3', 'CABANG');
                    $sheet->setCellValue('G3', 'STATUS KERJA');
                    $sheet->setCellValue('H3', 'TANGGAL MASUK');
                    $sheet->setCellValue('I3', 'TANGGAL KELUAR');
                    $sheet->setCellValue('J3', 'ALASAN KELAUAR');
                   if(!empty($qKarawanmasuk))
                   {
                        foreach($qKarawanmasuk as $row) 
                    {
                             
                              $sheet->setCellValue('A'.$baris,$baris-3);
                              $sheet->setCellValue('B'.$baris,$row->nik);
                              $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                              $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                              $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                              $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                              $sheet->setCellValue('G'.$baris,$row->status_kerja);
                              $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                              $sheet->setCellValue('I'.$baris,$row->tgl_keluar);
                              $sheet->setCellValue('J'.$baris,$row->alasan_keluar);
                              


                              //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
                              


                             $baris=$baris+1;
                    }
                            

                    

                    //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
                    foreach(range('B','J') as $columnID)
                     {
                         $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                              ->setAutoSize(true);
                         $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
                              ->setAutoSize(true);     
                              


                      }


                   }
                    
                    
                    
                    



                       $spreadsheet->getActiveSheet()->getColumnDimension("AA")
                              ->setAutoSize(true);
                        $writer = new Xlsx($spreadsheet);
                        // $writer->save('data karyawanlengkap.xlsx');
                        $judul    = "Karyawan Keluar";  
                        $name_file = $judul.'.xlsx';
                      // $path = storage_path('Laporan\\'.$name_file);
                      $path = public_path().'/app/'.$name_file;
                      $contents = is_dir($path);
                      // $headers = array('Content-Type' => File::mimeType($path));
                      // dd($path.'/'.$name_file,$contents);
                      $writer->save($path);
                      $filename   = str_replace("@", "/", $path);
                    # ---------------
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-Disposition: attachment; filename=".$name_file);
                    header("Content-Type: application/xlsx");
                    header("Content-Transfer-Encoding: binary");
                    # ---------------
                    require "$filename";
                    # ---------------
                    exit;



        }



        

        
    }



//-------------------

public function jatuhtempo_index() {
        $data["title"]         = "Laporan Jatuh Tempo Kontrak";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lprkontrak/cetak";
        /* ----------
         Model
        ----------------------- */
         $qMaster               = new MasterModel;
         $qProsesgaji             = new ProsesgajiModel;
         $qProsesgaji           = $qProsesgaji->getProfile()->first();
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qKaryawan             = $qMaster->getSelectKaryawan();
        $qKaryawan             = array_merge($collection,$qKaryawan);
        $qCabangs              = $qMaster->getSelectCabang();
        $qCabangs              = array_merge($collection,$qCabangs);
        $qDepartemens          = $qMaster->getSelectDepartemen();  
        $qDepartemens          = array_merge($collection,$qDepartemens);
        $qJabatan              = $qMaster->getSelectJabatan(); 
        $qJabatan              = array_merge($collection,$qJabatan);



        /* ----------
         Source
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
        $qJenisLaporan         = getFilelaporan();
        $data["fields"][]      = form_datepicker(array("name"=>"TglAwal", "label"=>"Tanggal Awal", "mandatory"=>"yes", "value"=>date("01/m/Y")));
        $data["fields"][]      = form_datepicker(array("name"=>"TglAkhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes", "value"=>date("d/m/Y")));
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>""));
        
        $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qJenisLaporan, "value"=>"pdf"));

        # ---------------
        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }
    public function jatuhtempo_cetak(Request $request) {
       
        if ($request->jenis_laporan=="pdf")
        {


                    
                    $qLaporan              = new LaporanpaModel;
                    /* ----------
                     Data
                    ----------------------- */
                    $qKarawankeluar         = $qLaporan->getKaryawanjatuhtempo($request);
                    $Line                  = 190;
                    $No                    = 0;
                    /* ----------
                     Report
                    ----------------------- */
                    Fpdf::AddPage();
                    # ----------------
                    Fpdf::SetLineWidth(0.3);
                    Fpdf::SetFont("Arial", "B", 11);
                    Fpdf::Ln(7);
                    //$logo = public_path("logo/".$qDataDS->Logo);
                    //Fpdf::Image($logo, 170, 13, 23, 20);
                    Fpdf::Cell(190, 5, "PT ARTHAPRIMA FINANCE", 0, 0, 'C');
                    Fpdf::Ln(5);
                    Fpdf::Cell(190, 5,"LAPORAN JATUH TEMPO KONTRAK KARYAWAN", 0, 0, 'C');
                    Fpdf::Ln(10);
                    if(count( $qKarawankeluar) == 0) {
                        Fpdf::SetFont("Arial", "", 9);
                        Fpdf::Cell(190, 5, "Tidak ada jatuh tempo kontrak diperiode tersebut", 0, 0, 'L');
                        Fpdf::Ln();
                        Fpdf::Cell(190, 5, "Terima Kasih", 0, 0, 'L');
                        # ----------------
                        Fpdf::Output(); exit;
                    } else 
                    {

                        Fpdf::SetFont("Arial", "", 8);
                        Fpdf::Cell(15, 5, 'Tanggal', 0, 0, 'L');
                        Fpdf::Cell(5, 5, ':', 0, 0, 'L');
                        Fpdf::Cell(18, 5, date("d/m/Y", strtotime(setYMD($request->TglAwal,"/"))), 0, 0, 'L');
                        Fpdf::Cell(7, 5, 'S.D', 0, 0, 'L');
                        Fpdf::Cell(142, 5, date("d/m/Y", strtotime(setYMD($request->TglAkhir,"/"))), 0, 0, 'L');
                        Fpdf::Ln(7);
                        Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
                        Fpdf::Ln(2);
                        Fpdf::SetFont("Arial", "B", 8);
                        Fpdf::Cell(7, 5, 'NO.', 0, 0, 'C');
                        Fpdf::Cell(30,5, 'NIK', 0, 0, 'L');
                        Fpdf::Cell(50, 5, 'NAMA KARYAWAN', 0, 0, 'L');
                        Fpdf::Cell(35, 5, 'CABANG', 0, 0, 'L');
                         Fpdf::Cell(20, 5, 'TGL KONTRAK', 0, 0, 'C');
                        Fpdf::Cell(20, 5, 'TGL AKHIR KONTRAK', 0, 0, 'C');
                      
                        
                        Fpdf::Ln(7);
                        Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
                        Fpdf::Ln(3);
                        foreach ($qKarawankeluar as $row) 
                        {
                            $No = $No + 1;
                            Fpdf::SetFont("Arial", "", 8);
                            Fpdf::Cell(7, 5, $No, 0, 0, 'C');
                            Fpdf::Cell(30, 5, $row->nik, 0, 0, 'L');
                            Fpdf::Cell(50, 5, $row->nama_karyawan, 0, 0, 'L');
                            Fpdf::Cell(35, 5, $row->nama_cabang, 0, 0, 'L');
                            Fpdf::Cell(20, 5,  $row->tgl_awal, 0, 0, 'C');
                            Fpdf::Cell(20, 5,  $row->tgl_akhir, 0, 0, 'C');
                                                   
                            Fpdf::Ln(8);
                           
                            }
                          
                        }
                        
                        Fpdf::Output(); exit;

        }
        else
        {


               $qLaporan              = new LaporanpaModel;
               $qKarawanmasuk         = $qLaporan->getKaryawanjatuhtempo($request);

                    
              $styleArray = [
                  'font' => [
                      'bold' => true,
                  ],
                  'alignment' => [
                      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                  ],
                  'borders' => [
                      'top' => [
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      ],
                  ],
                  'fill' => [
                      'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                      'rotation' => 90,
                      'startColor' => [
                          'argb' => 'FFA0A0A0',
                      ],
                      'endColor' => [
                          'argb' => 'FFFFFFFF',
                      ],
                  ],
              ];






                                        
                    $spreadsheet = new Spreadsheet();
                    $sheet = $spreadsheet->getActiveSheet();
                    $spreadsheet->getActiveSheet()->getStyle('A3:J3')->applyFromArray($styleArray);
                   
                  //$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setWrapText(true);
              
                    $sheet->setCellValue('A1', 'LAPORAN JATUH TEMPO KONTRAK');
                    $sheet->setCellValue('A2', 'PERIODE : '.$request->TglAwal ." - ".$request->TglAkhir);
                    
                    $baris = 4;
                    /*-------------------------
                    KOLOM DATA
                    ---------------------------*/
                      
                    $sheet->setCellValue('A3', 'NO.');    
                    $sheet->setCellValue('B3', 'N I K');
                    $sheet->setCellValue('C3', 'NAMA');
                    $sheet->setCellValue('D3', 'JABATAN');
                    $sheet->setCellValue('E3', 'DEPARTEMEN');
                    $sheet->setCellValue('F3', 'CABANG');
                    $sheet->setCellValue('G3', 'STATUS KERJA');
                    $sheet->setCellValue('H3', 'TANGGAL KONTRAK');
                    $sheet->setCellValue('I3', 'TANGGAL JATUH TEMPO KONTRAK');
               
                   if(!empty($qKarawanmasuk))
                   {
                        foreach($qKarawanmasuk as $row) 
                    {
                             
                              $sheet->setCellValue('A'.$baris,$baris-3);
                              $sheet->setCellValue('B'.$baris,$row->nik);
                              $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                              $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                              $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                              $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                              $sheet->setCellValue('G'.$baris,$row->status_kerja);
                              $sheet->setCellValue('H'.$baris,$row->tgl_awal);
                              $sheet->setCellValue('I'.$baris,$row->tgl_akhir);
                           
                              


                              //$sheet->setCellvalue('at'.$baris,$row->makanperhari)
                              


                             $baris=$baris+1;
                    }
                            

                    

                    //$spshObj = PhpOffice\PhpSpreadsheet\Spreadsheet;
                    foreach(range('B','J') as $columnID)
                     {
                         $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                              ->setAutoSize(true);
                         $spreadsheet->getActiveSheet()->getColumnDimension("A".$columnID)
                              ->setAutoSize(true);     
                              


                      }


                   }
                    
                    
                    
                    



                       $spreadsheet->getActiveSheet()->getColumnDimension("AA")
                              ->setAutoSize(true);
                        $writer = new Xlsx($spreadsheet);
                        // $writer->save('data karyawanlengkap.xlsx');
                        $judul    = "Karyawan Keluar";  
                        $name_file = $judul.'.xlsx';
                      // $path = storage_path('Laporan\\'.$name_file);
                      $path = public_path().'/app/'.$name_file;
                      $contents = is_dir($path);
                      // $headers = array('Content-Type' => File::mimeType($path));
                      // dd($path.'/'.$name_file,$contents);
                      $writer->save($path);
                      $filename   = str_replace("@", "/", $path);
                    # ---------------
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-Disposition: attachment; filename=".$name_file);
                    header("Content-Type: application/xlsx");
                    header("Content-Transfer-Encoding: binary");
                    # ---------------
                    require "$filename";
                    # ---------------
                    exit;



        }



        

        
    }



//-------------

     public function karyawanturnover_index() {
        $data["title"]         = "Laporan Turn Over Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lprkaryawanturnover/cetak";
        /* ----------
         Model
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_datepicker(array("name"=>"TglAwal", "label"=>"Tanggal Awal", "mandatory"=>"yes", "value"=>date("01/m/Y")));
        $data["fields"][]      = form_datepicker(array("name"=>"TglAkhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes", "value"=>date("d/m/Y")));
        # ---------------
        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Cetak&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }
    public function karyawanturnover_cetak(Request $request) {
        /* ----------
         Model
        ----------------------- */
        $qLaporan              = new LaporanpaModel;
        /* ----------
         Data
        ----------------------- */
        $qKarawankeluar         = $qLaporan->getKaryawankeluar($request);
        $qKarawanmasuk          = $qLaporan->getKaryawanmasuk($request);
        $Line                   = 190;
        $No                     = 0;
        /* ----------
         Report
        ----------------------- */
        Fpdf::AddPage();
        # ----------------
        Fpdf::SetLineWidth(0.3);
        Fpdf::SetFont("Arial", "B", 11);
        Fpdf::Ln(7);
        //$logo = public_path("logo/".$qDataDS->Logo);
        //Fpdf::Image($logo, 170, 13, 23, 20);
        Fpdf::Cell(190, 5, "PT ARTHAPRIMA FINANCE", 0, 0, 'C');
        Fpdf::Ln(5);
        Fpdf::Cell(190, 5,"DATA TURN OVER KARYAWAN", 0, 0, 'C');
        Fpdf::Ln(10);
        Fpdf::Cell(190, 5,"DAFTAR KARYAWAN MASUK", 0, 0, 'L');
        Fpdf::Ln(5);
        if(count( $qKarawanmasuk) == 0) {
            Fpdf::SetFont("Arial", "", 9);
            Fpdf::Cell(190, 5, "Tidak ada karyawan masuk diperiode tersebut", 0, 0, 'L');
            Fpdf::Ln();
            Fpdf::Cell(190, 5, "Terima Kasih", 0, 0, 'L');
            # ----------------
            Fpdf::Output(); exit;
        } else 
        {

            Fpdf::SetFont("Arial", "", 8);
            Fpdf::Cell(15, 5, 'Tanggal', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'L');
            Fpdf::Cell(18, 5, date("d/m/Y", strtotime(setYMD($request->TglAwal,"/"))), 0, 0, 'L');
            Fpdf::Cell(7, 5, 'S.D', 0, 0, 'L');
            Fpdf::Cell(142, 5, date("d/m/Y", strtotime(setYMD($request->TglAkhir,"/"))), 0, 0, 'L');
            Fpdf::Ln(7);
            Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(7, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(30,5, 'NIK', 0, 0, 'L');
            Fpdf::Cell(50, 5, 'NAMA KARYAWAN', 0, 0, 'L');
            Fpdf::Cell(50, 5, 'JABATAN', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'CABANG', 0, 0, 'L');
            Fpdf::Cell(20, 5, 'TGL MASUK', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
            Fpdf::Ln(3);
            foreach ($qKarawanmasuk as $row) 
            {
                $No = $No + 1;
                Fpdf::SetFont("Arial", "", 8);
                Fpdf::Cell(7, 5, $No, 0, 0, 'C');
                Fpdf::Cell(30, 5, $row->nik, 0, 0, 'L');
                Fpdf::Cell(50, 5, $row->nama_karyawan, 0, 0, 'L');
                 Fpdf::Cell(50, 5, $row->nama_jabatan, 0, 0, 'L');
                Fpdf::Cell(35, 5, $row->nama_cabang, 0, 0, 'L');
                Fpdf::Cell(20, 5,  $row->tgl_masuk, 0, 0, 'C');
                Fpdf::Ln(8);
               
                }
              
            }


        Fpdf::Ln(10);
        Fpdf::Cell(190, 5,"DAFTAR KARYAWAN KELUAR", 0, 0, 'C');
        Fpdf::Ln(5);


        if(count( $qKarawankeluar) == 0) {
            Fpdf::SetFont("Arial", "", 9);
            Fpdf::Cell(190, 5, "Tidak ada karyawan keluar diperiode tersebut", 0, 0, 'L');
            Fpdf::Ln();
            Fpdf::Cell(190, 5, "Terima Kasih", 0, 0, 'L');
            # ----------------
            Fpdf::Output(); exit;
        } else 
        {

            Fpdf::SetFont("Arial", "", 8);
            Fpdf::Cell(15, 5, 'Tanggal', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'L');
            Fpdf::Cell(18, 5, date("d/m/Y", strtotime(setYMD($request->TglAwal,"/"))), 0, 0, 'L');
            Fpdf::Cell(7, 5, 'S.D', 0, 0, 'L');
            Fpdf::Cell(142, 5, date("d/m/Y", strtotime(setYMD($request->TglAkhir,"/"))), 0, 0, 'L');
            Fpdf::Ln(7);
            Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(7, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(30,5, 'NIK', 0, 0, 'L');
            Fpdf::Cell(50, 5, 'NAMA KARYAWAN', 0, 0, 'L');
            Fpdf::Cell(50, 5, 'JABATAN', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'CABANG', 0, 0, 'L');
            Fpdf::Cell(20, 5, 'TGL KELUAR', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell($Line, 0.1, '', 1, 0, 'L');
            Fpdf::Ln(3);
            foreach ($qKarawankeluar as $row) 
            {
                $No = $No + 1;
                Fpdf::SetFont("Arial", "", 8);
                Fpdf::Cell(7, 5, $No, 0, 0, 'C');
                Fpdf::Cell(30, 5, $row->nik, 0, 0, 'L');
                Fpdf::Cell(50, 5, $row->nama_karyawan, 0, 0, 'L');
                 Fpdf::Cell(50, 5, $row->nama_jabatan, 0, 0, 'L');
                Fpdf::Cell(35, 5, $row->nama_cabang, 0, 0, 'L');
                Fpdf::Cell(20, 5,  $row->tgl_keluar, 0, 0, 'C');
                Fpdf::Ln(8);
               
                }
              
            }
            
            Fpdf::Output(); exit;
        
    }






    public function karyawanaktif_index() {
        $data["title"]         = "Laporan Karyawan Aktif";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/lprkaryawanaktif/cetak";
        /* ----------
         Model
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
         $qMaster               = new MasterModel;
         $qProsesgaji             = new ProsesgajiModel;
         $qProsesgaji           = $qProsesgaji->getProfile()->first();
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qKaryawan             = $qMaster->getSelectKaryawan();
        $qKaryawan             = array_merge($collection,$qKaryawan);
        $qCabangs              = $qMaster->getSelectCabang();
        $qCabangs              = array_merge($collection,$qCabangs);
        $qDepartemens          = $qMaster->getSelectDepartemen();  
        $qDepartemens          = array_merge($collection,$qDepartemens);
        $qJabatan              = $qMaster->getSelectJabatan(); 
        $qJabatan              = array_merge($collection,$qJabatan);
        /* ----------
         Fields
        ----------------------- */
                # ---------------

        $qJenisLaporan         = getFilelaporan();
        $data["fields"][]      = form_datepicker(array("name"=>"TglAwal", "label"=>"Tanggal Awal", "mandatory"=>"yes", "value"=>date("01/m/Y")));
        $data["fields"][]      = form_datepicker(array("name"=>"TglAkhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes", "value"=>date("d/m/Y")));
        # ---------------
        
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>""));
        
       

        $data["buttons"][]     = form_button_window(array("name"=>"button_window", "label"=>"&nbsp;&nbsp;Export (Excel)&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
       return view("default.laporanform", $data);
    }
    public function karyawanaktif_cetak(Request $request) {
       


       $aktif=1;
       $TglAwal = $request->TglAwal;
       $TglAkhir = $request->TglAkhir;
       $awal =setYMD($TglAwal,"/");
       $akhir=setYMD($TglAkhir,"/");
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","b.kode_cabang","c.nama_departemen","d.nama_jabatan","e.nama_pendidikan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                              ->leftjoin("m_pendidikan as e","e.id_pendidikan","=","a.id_pendidikan")
                            ->where("a.aktif",$aktif)
                            ->whereBetween("a.tgl_masuk", [$awal, $akhir])
                            ->orderBy("a.id_cabang","ASC")
                            ->orderBy("a.id_departemen","ASC")->get();

        

              $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'rotation' => 90,
                        'startColor' => [
                            'argb' => 'FFA0A0A0',
                        ],
                        'endColor' => [
                            'argb' => 'FFFFFFFF',
                        ],
                    ],
                ];

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $sheet->setCellValue('A1', 'DAFTAR KARYAWAN AKTIF');
                $spreadsheet->getActiveSheet()->getStyle('A3:AA3')->applyFromArray($styleArray);
                $baris = 4;
                /*-------------------------
                KOLOM DATA
                ---------------------------*/
                
                $sheet->setCellValue('A3', 'NO.');
                $sheet->setCellValue('B3', 'CABANG');
                $sheet->setCellValue('C3', 'N I K');
                $sheet->setCellValue('D3', 'NAMA');
                $sheet->setCellValue('E3', 'JABATAN');
                $sheet->setCellValue('F3', 'DEPARTEMEN');
                $sheet->setCellValue('G3', 'STATUS KEPEGAWAIAN');
                $sheet->setCellValue('H3', 'TANGGAL MASUK');
                $sheet->setCellValue('I3', 'STATUS KARYAWAN');
                $sheet->setCellValue('J3', 'AGAMA');
                $sheet->setCellValue('K3', 'PENDIDIKAN');
                $sheet->setCellValue('L3', 'JENIS KELAMIN');
                $sheet->setCellValue('M3', 'TEMPAT LAHIR');
                $sheet->setCellValue('N3', 'TANGGAL LAHIR');
                $sheet->setCellValue('O3', 'ALAMAT KTP');
                $sheet->setCellValue('P3', 'ALAMAT TINGGAL');
                $sheet->setCellValue('Q3', 'NO HP');
                $sheet->setCellValue('R3','TELP RUMAH');
                $sheet->setCellValue('S3', 'NO REKENING');
                $sheet->setCellValue('T3', 'A/N REKENING');
                $sheet->setCellValue('U3', "EMAIL");
                $sheet->setCellValue('P3', 'NO KTP');
                $sheet->setCellValue('W3', "STATUS PAJAK");
                $sheet->setCellValue('X3', 'NPWP');
                $sheet->setCellValue('Y3', "NO BPJS KESEHATAN");
                $sheet->setCellValue('Z3', 'NO BPJS TK');
                $sheet->setCellValue('AA3','NO ABSEN');


                
                foreach($query as $row) 
                {
              
                          $qKaryawan            = new KaryawanModel;
                          $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                          $pendidikan           = $row->id_pendidikan;
                          if ($pendidikan=="UND")
                          {
                            $pendidikan="UNDER SMA";
                             
                          }
                          else if ($pendidikan=="DIP")
                          {
                            $pendidikan="DIPLOMA";
                             
                          }
                          $sheet->setCellValue('A'.$baris,$baris-3);
                          $sheet->setCellValue('B'.$baris,$row->kode_cabang ." " .$row->nama_cabang);
                          $sheet->setCellValue('C'.$baris,$row->nik);
                          $sheet->setCellValue('D'.$baris,$row->nama_karyawan);
                          $sheet->setCellValue('E'.$baris,$row->nama_jabatan);
                          $sheet->setCellValue('F'.$baris,$row->nama_departemen);
                          $sheet->setCellValue('G'.$baris,$row->status_kerja);
                          $sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                          $sheet->setCellValue('I'.$baris,$row->status_nikah);
                          $sheet->setCellValue('J'.$baris,$row->agama);
                          $sheet->setCellValue('K'.$baris,$row->nama_pendidikan);
                          $sheet->setCellValue('L'.$baris,$row->jenis_kelamin);
                          $sheet->setCellValue('M'.$baris,$row->tempat_lahir);
                          $sheet->setCellValue('N'.$baris,$row->tgl_lahir);
                          $sheet->setCellValue('O'.$baris,$row->alamat_valid);
                          $sheet->setCellValue('P'.$baris,$row->alamat );
                          $sheet->setCellValue('Q'.$baris,$row->hp);
                          $sheet->setCellValue('R'.$baris,$row->telp_rumah);
                          $sheet->setCellValue('S'.$baris,$row->no_rekening);
                          $sheet->setCellValue('T'.$baris,$row->anrekening);
                          $sheet->setCellValue('U'.$baris,$row->email);
                          $sheet->setCellValue('V'.$baris,$row->no_ktp);
                          $sheet->setCellValue('W'.$baris,$statuspajak);
                          $sheet->setCellValue('X'.$baris,$row->nonpwp);
                          $sheet->setCellValue('Y'.$baris,$row->nobpjskes);
                          $sheet->setCellValue('Z'.$baris,$row->nobjstk);
                          $sheet->setCellValue('AA'.$baris,$row->noabsen);
                        

               
                         $baris=$baris+1;
                }

                foreach(range('B','Z') as $columnID)
                 {
                     $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                          ->setAutoSize(true);
                     
                     


                  }
                   $spreadsheet->getActiveSheet()->getColumnDimension('AA')
                          ->setAutoSize(true);


                
                $writer = new Xlsx($spreadsheet);
                // $writer->save('data karyawanlengkap.xlsx');
                $judul    = "data karyawanlengkap";  
                $name_file = $judul.'.xlsx';
              // $path = storage_path('Laporan\\'.$name_file);
              $path = public_path().'/app/'.$name_file;
              $contents = is_dir($path);
              // $headers = array('Content-Type' => File::mimeType($path));
              // dd($path.'/'.$name_file,$contents);
              $writer->save($path);
              $filename   = str_replace("@", "/", $path);
                # ---------------
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=".$name_file);
                header("Content-Type: application/xlsx");
                header("Content-Transfer-Encoding: binary");
                # ---------------
                require "$filename";
                # ---------------
                exit;


       
    }


}
