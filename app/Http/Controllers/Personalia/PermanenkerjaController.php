<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\PermanenkerjaModel;
use PDF;
class PermanenkerjaController extends Controller
{
     public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = "Perjanjian Keja Waktu Tidak Tertentu (Permanen)"; //ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/permanenkerja/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;

        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qPermanenkerja          = new PermanenkerjaModel;
        $qMaster                = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qKaryawan                = $qMaster->getKaryawan($idkry)->first();
        $data["form_act_add"]     = "/permanenkerja/add/".$idkry;
        $data["form_act_edit"]    = "/permanenkerja/edit";
        $data["form_act_delete"]  = "/permanenkerja/delete";
        $data["form_act_print"]  = "/permanenkerja/cetak";
        $data["form_act_aktifasi"]  = "/permanenkerja/aktif";
       
       
       
        
        # ---------------*/
        
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));
         

        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"PKWT (Permanen)", "url"=>"/kontrakkerja/index/".$idkry, "active"=>""),
                                 array("label"=>"PKWTT (Tetap)", "url"=>"/permanenkerja/index/".$idkry, "active"=>"active"), 
                                 array("label"=>"Mutasi", "url"=>"/karir/index/".$idkry, "active"=>""), 
                                 array("label"=>"Peringatan", "url"=>"/peringatan/index/".$idkry, "active"=>""),
                                 
                                 
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_permanenkerja"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
       								array("label"=>"No Surat"
                                                ,"name"=>"no_surat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                
                                      array("label"=>"Tgl Pengangkatan"
                                                ,"name"=>"tgl_awal"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       
                                        array("label"=>"Tgl Tanda Tangan"
                                                ,"name"=>"tgl_tandatangan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                          array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                          array("label"=>"Status Aktif"
                                                ,"name"=>"aktif"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),


                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PERMANEN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERMANEN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERMANEN");
        }
        # ---------------
        //dd($qPermanenkerja->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry));
        $data["select"]        = $qPermanenkerja->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qPermanenkerja->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.karirkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Permanen Kerja (PKWTT)";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/permanenkerja/save";
        
        $qMaster               = new MasterModel;

        $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan(); 
		$qStatuskerja          = getSelectStskerja();
        $qPermanenkerjamdl      = new PermanenkerjaModel;
        
        /* ----------
         Source
        ----------------------- */
        //$idPermanenkerja             = explode("&", $id); 
       // $qPermanenkerja        = $qPermanenkerjamdl->getProfile($id)->first();
        $qViewKaryawan         = $qPermanenkerjamdl->getProfileKry($idkry)->first(); 
        $qViewFormatsurat      = $qPermanenkerjamdl->getFormatskt()->first();
        $nourut                = (string)$qViewFormatsurat->nosktetap;
        $formatsk              = $qViewFormatsurat->formatsktetap;
        $tahunskp              = (string)$qViewFormatsurat->tahunsktetap;
        $bulanskp              = getBulanRomawi(date("m"));
        $no_surat              =   sprintf("%03s",$nourut) ."/" .$formatsk ."/" .$bulanskp ."/" .$tahunskp;
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "readonly"=>"readonly", "value"=>$no_surat ));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKaryawan->id_cabang));        
        $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKaryawan->id_departemen));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKaryawan->id_jabatan));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"gaji_pokok_awal", "label"=>"Gaji Pokok Awal",  "readonly"=>"readonly", "value"=>number_format($qViewKaryawan->gaji_pokok,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok",  "mandatory"=>"yes", "value"=>number_format($qViewKaryawan->gaji_pokok,0)));
             
       //dd(($qViewKaryawan->tunj_jabatan==null)?0:number_format($qViewKaryawan->tunj_jabatan,0));  


        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jabatan_awal", "label"=>"Tnj. Jabatan Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_jabatan,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan", "value"=>number_format($qViewKaryawan->tunj_jabatan,0)));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_transport_awal", "label"=>"Tnj. Tranport Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_transport,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport", "value"=>number_format($qViewKaryawan->tunj_transport,0)));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jaga_awal", "label"=>"Tnj. Jaga awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_jaga,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga", "value"=>number_format($qViewKaryawan->tunj_jaga,0)));
        
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_kemahalan_awal", "label"=>"Tnj. Kemahalan Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_kemahalan,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan", "value"=>number_format($qViewKaryawan->tunj_kemahalan,0)));
       
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_makan_awal", "label"=>"Tnj. Makan Awal","readonly"=>"readonly", "value"=>number_format($qViewKaryawan->tunj_makan,0)));
        $data["fields"][]      = formsmall_currency(array("name"=>"tunj_makan", "label"=>"Tnj. Makan", "value"=>number_format($qViewKaryawan->tunj_makan,0)));
   
      

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



/*

 public function save(Request $request) {
        $rules = array(
                      'email' => 'required|',
                        'name' => 'required|',
                        'password' => 'required|min:3',
                        'password_confirm' => 'required|min:3|same:password',
        );

        $messages = [
                      'email.required' => 'Email harus diisi',
                      'name.required' => 'Nama harus diisi',
                      'password.required' => 'Password harus diisi',
                      'password_confirm.required' => 'Konfirmasi Password harus diisi',
                      'password_confirm.same' => 'Konfirmasi Password harus sama',
        ];


*/


    public function save(Request $request) {
       

        $rules = array(
                      
                      'id_cabang' => 'required|',
                      'id_departemen' => 'required|',
                      'id_jabatan' => 'required|',
                      'tgl_awal' => 'required|',
                     
                      'gaji_pokok'=> 'required|',

                       );

        $messages = ['id_cabang.required' => 'Cabang harus diisi',
                     'id_departemen.required' => 'Departemen harus diisi',
                     'id_jabatan.required' => 'Jabatan harus diisi',
                     'tgl_awal.required' => 'Tgl Awal harus diisi',
                    
                     'gaji_pokok.required' => 'Gaji pokok harus diisi',
                    
           ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/permanenkerja/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPermanenkerja  = new PermanenkerjaModel;
            

            if($qPermanenkerja->CekPermanen($request->id_karyawan,$request->tgl_awal,$request->tgl_akhir) > 0)
            {

                session()->flash("error_message", "Status Kerja sudah diinput");
                return redirect("/permanenkerja/add/". $request->id_karyawan);
            }
            else
            {
                 $qPermanenkerja->createData($request);
                 session()->flash("success_message", "Wilayah has been saved");
                 return redirect("/permanenkerja/index/". $request->id_karyawan);

            }



        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Permanen Kerja (PKWTT)";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/permanenkerja/update";
        /* ----------
         Permanenkerja
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPermanenkerja             = new PermanenkerjaModel;
        /* ----------
         Source
        ----------------------- */
        //$idPermanenkerja             = explode("&", $id); 
        $qPermanenkerja        = $qPermanenkerja->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan();
		
		$qStatuskerja          = getSelectStskerja();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Permanenkerja", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"Nomor Surat", "readonly"=>"readonly", "value"=>$qPermanenkerja->no_surat));
       
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qPermanenkerja->id_karyawan));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qPermanenkerja->id_cabang));        
        $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qPermanenkerja->id_departemen));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qPermanenkerja->id_jabatan));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Pengangkatan", "mandatory"=>"yes","value"=>displayDMY($qPermanenkerja->tgl_awal,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok",  "mandatory"=>"yes", "value"=>number_format($qPermanenkerja->gaji_pokok,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan", "value"=>number_format($qPermanenkerja->tunj_jabatan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport", "value"=>number_format($qPermanenkerja->tunj_transport,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga", "value"=>number_format($qPermanenkerja->tunj_jaga,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan", "value"=>number_format($qPermanenkerja->tunj_kemahalan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_makan", "label"=>"Tnj. Makan", "value"=>number_format($qPermanenkerja->tunj_makan,0)));

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                      
                      'id_cabang' => 'required|',
                      'id_departemen' => 'required|',
                      'id_jabatan' => 'required|',
                      'tgl_awal' => 'required|',
                      'gaji_pokok'=> 'required|',

                       );

        $messages = ['id_cabang.required' => 'Cabang harus diisi',
                     'id_departemen.required' => 'Departemen harus diisi',
                     'id_jabatan.required' => 'Jabatan harus diisi',
                     'tgl_awal.required' => 'Tgl Awal harus diisi',
                     'gaji_pokok.required' => 'Gaji pokok harus diisi',
                    
           ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/permanenkerja/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPermanenkerja      = new PermanenkerjaModel;
            # ---------------
            $qPermanenkerja->updateData($request);
            # ---------------
            session()->flash("success_message", "Permanenkerja has been updated");
            # ---------------
            return redirect("/permanenkerja/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/permanenkerja/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPermanenkerja             = new PermanenkerjaModel;

       //$idPermanenkerja             = explode("&", $id); 
        $qPermanenkerja        = $qPermanenkerja->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan(); 
		$qStatuskerja          = getSelectStskerja();
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id_permanenkerja", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qPermanenkerja->no_surat));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qPermanenkerja->id_karyawan));
              
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id_permanenkerja") != 1) {
            $qPermanenkerja     = new PermanenkerjaModel;
            # ---------------
            $qPermanenkerja->removeData($request);
            # ---------------
            session()->flash("success_message", "Permanenkerja has been removed");
        } else {
            session()->flash("error_message", "Permanenkerja cannot be removed");
        }
      # ---------------
        return redirect("/permanenkerja/index/". $request->input("id_karyawan")); 
    }

   
    public function Cetak( $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCPermanenkerja   = new PermanenkerjaModel;
      /* ----------
        Source
      ----------------------- */
      //$idPermanenkerja     = explode("&", $id); 
      $qPermanenkerja    = $qCPermanenkerja->getCetakPermanen($id);
      $qNama             = $qCPermanenkerja->getCetakPermanen($id)->first();
      $data["pkwt"]      =  $qPermanenkerja ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      
        $pdf = PDF::loadView('Personalia.lprssk_tetap',  $data);
        $nama_file =  $qNama->nama_karyawan;

        //$nama_file =  $qPermanenkerja->nama_karyawan & "-" & $qPermanenkerja->id_permanenkerja;

      
       $pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprpermanen", $data);

    }
    public function aktivasi($id) 
    {
        $data["title"]        = "Update Status Proses";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/permanenkerja/updateaktivasi";
        /* ----------
         Permanenkerja
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPermanenkerja             = new PermanenkerjaModel;
        /* ----------
         Source
        ----------------------- */
        //$idPermanenkerja             = explode("&", $id); 
        $qPermanenkerja        = $qPermanenkerja->getProfile($id)->first();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Permanenkerja", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly","value"=>$qPermanenkerja->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Tgl Awal", "readonly"=>"readonly","value"=>$qPermanenkerja->tgl_awal));
        $data["fields"][]      = form_hidden(array("name"=>"status_kerja", "label"=>"Status", "readonly"=>"readonly","value"=>$qPermanenkerja->statuskerja));
        $data["fields"][]      = form_currency(array("name"=>"gaji_pokok", "label"=>"Gaji Pokok","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>number_format($qPermanenkerja->gaji_pokok,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jabatan", "label"=>"Tnj. Jabatan","readonly"=>"readonly", "value"=>number_format($qPermanenkerja->tunj_jabatan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_transport", "label"=>"Tnj. Tranport","readonly"=>"readonly", "value"=>number_format($qPermanenkerja->tunj_transport,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_jaga", "label"=>"Tnj. Jaga","readonly"=>"readonly", "value"=>number_format($qPermanenkerja->tunj_jaga,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_kemahalan", "label"=>"Tnj. Kemahalan","readonly"=>"readonly", "value"=>number_format($qPermanenkerja->tunj_kemahalan,0)));
        $data["fields"][]      = form_currency(array("name"=>"tunj_makan", "label"=>"Tnj. Makan","readonly"=>"readonly", "value"=>number_format($qPermanenkerja->tunj_makan,0)));

        $data["fields"][]      = form_text(array("name"=>"status_proses", "label"=>"Status Proses","readonly"=>"readonly","mandatory"=>"yes","value"=>"SELESAI"));
        if ($qPermanenkerja->status_proses=="SELESAI" and $qPermanenkerja->aktif=="TIDAK AKTIF") 
        {
           $data["fields"][]      = form_text(array("name"=>"tgl_tandatangan", "label"=>"Tgl Tanda Tangan","readonly"=>"readonly","value"=>$qPermanenkerja->tgl_tandatangan));
        }
        else
        {
           $data["fields"][]      = form_datepicker(array("name"=>"tgl_tandatangan", "label"=>"Tgl Tanda Tangan", "mandatory"=>"yes","value"=>displayDMY($qPermanenkerja->tgl_tandatangan,"/"), "first_selected"=>"yes"));
        }
        $data["fields"][]     = form_checklist2(array("name"=>"updatekaryawan", "label"=>"Update Data Karyawan", "checked"=>2,"value"=>2 ));
           

        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);    
            

    }

public function updateaktivasi(Request $request)
    {
        
        
        $rules = array(
                    'tgl_tandatangan' => 'required|',
                    
                    'tgl_tandatangan' => 'required|'                          
        );

        $messages = [
                    'tgl_tandatangan.required' => 'Tanggal TTD harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/permanenkerja/aktif/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPermanenkerja      = new PermanenkerjaModel;
            # ---------------
            $qPermanenkerja->updateaktivasiData($request);
            # ---------------
            session()->flash("success_message", "Permanenkerja has been updated");
            # ---------------
            return redirect("/permanenkerja/index/". $request->input("id_karyawan"));
        }
    }
}
