<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\UserModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\KontakModel;
class KontakController extends Controller
{
   public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/kontak/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qKontak              = new KontakModel;
        $qMaster                = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
       
         $data["form_act_add"]    = "/kontak/add/".$idkry;
         $data["label_add"]       = "Add";
         $data["form_act_edit"]   = "/kontak/edit";
         $data["label_edit"]       = "Edit";
         $data["form_act_delete"] = "/kontak/delete";
         $data["label_delete"]    = "Delete";
        
        # ---------------*/
        $qUser                  = new UserModel;
        $qhakAkses              = $qUser->getAksesuser(Auth::user()->id)->first();
        $data["hakakses"]      = $qhakAkses;
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));


        // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);


        $data["tabs"]          = array(array("label"=>"Biodata", "url"=>"/detail/index/".$idkry, "active"=>""),
                                 array("label"=>"Keluarga", "url"=>"/keluarga/index/".$idkry, "active"=>""),
                                 array("label"=>"Kontak Lain", "url"=>"/kontak/index/".$idkry, "active"=>"active"), 
                                 array("label"=>"Pendidikan", "url"=>"/rpendidikan/index/".$idkry, "active"=>""),
                                  array("label"=>"Pengalaman", "url"=>"/pengalaman/index/".$idkry, "active"=>""),
                                
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_kontak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"Nama"
                                                ,"name"=>"nama_kontak"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Hubungan"
                                                ,"name"=>"hubungan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Alamat"
                                                ,"name"=>"alamat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                      array("label"=>"No Telpon"
                                                ,"name"=>"telpon"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     

                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KONTAK" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTAK");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTAK");
        }
        # ---------------
        $data["select"]        = $qKontak->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qKontak->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.dtlkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Kontak";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/kontak/save";
        
        $qMaster               = new MasterModel;
        
        $qGroups               = $qMaster->getSelectGroup();
         
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_text(array("name"=>"nama_kontak", "label"=>"Nama", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes"));        
        $data["fields"][]      = form_text(array("name"=>"alamat", "label"=>"Alamat", "mandatory"=>"yes"));        
        $data["fields"][]      = form_text(array("name"=>"telpon", "label"=>"No Telpon", "mandatory"=>"yes"));
            # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



    public function save(Request $request) {
       

        $rules = array(
                      
                      'nama_kontak' => 'required'                     );

        $messages = ['nama_kontak.required' => 'Kontak harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/kontak/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKontak  = new KontakModel;
            # ---------------
            $qKontak->createData($request);
            # ---------------
            session()->flash("success_message", "Kontak has been saved");
            # ---------------
            return redirect("/kontak/index/". $request->id_karyawan);
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Kontak";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/kontak/update";
        /* ----------
         Kontak
        ----------------------- */
        $qMaster           = new MasterModel;
        $qKontak             = new KontakModel;
        /* ----------
         Source
        ----------------------- */
        //$idkontak             = explode("&", $id); 
        $qKontak              = $qKontak->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubungan();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusKontak();
        $qPendidikan           = getSelectPendidikan();  
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Kontak", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qKontak->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
      
        $data["fields"][]      = form_text(array("name"=>"nama_kontak", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qKontak->nama_kontak));
        $data["fields"][]      = form_text(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes",  "value"=>$qKontak->hubungan));        
        $data["fields"][]      = form_text(array("name"=>"alamat", "label"=>"Alamat", "mandatory"=>"yes", "value"=>$qKontak->alamat));
        $data["fields"][]      = form_text(array("name"=>"telpon", "label"=>"No Telpon", "mandatory"=>"no","value"=>$qKontak->telpon));
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'nama_kontak' => 'required|',
                    
                    'hubungan' => 'required|'                          
        );

        $messages = [
                    'nama_kontak.required' => 'Nama Kontak harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/kontak/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKontak      = new KontakModel;
            # ---------------
            $qKontak->updateData($request);
            # ---------------
            session()->flash("success_message", "Kontak has been updated");
            # ---------------
            return redirect("/kontak/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/kontak/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKontak             = new KontakModel;

       //$idkontak             = explode("&", $id); 
        $qKontak              = $qKontak->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubungan();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusKontak();
        $qPendidikan           = getSelectPendidikan();  
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Kontak", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qKontak->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
      
        $data["fields"][]      = form_text(array("name"=>"nama_kontak", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qKontak->nama_kontak));
        $data["fields"][]      = form_select(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes", "source"=>$qHubungan, "value"=>$qKontak->hubungan));        
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request)
     {
        if($request->input("id") != 1) {
            $qKontak     = new KontakModel;
            # ---------------
            $qKontak->removeData($request);
            # ---------------
            session()->flash("success_message", "Kontak has been removed");
        } else {
            session()->flash("error_message", "Kontak cannot be removed");
        }
      # ---------------
        return redirect("/kontak/index/". $request->input("id_karyawan")); 
    }
    public function print($jenislaporan, $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qKontrakkerja     = new KontrakkerjaModel;
      /* ----------
        Source
      ----------------------- */
      //$idKontrakkerja     = explode("&", $id); 
      $qKontrakkerja        = $qKontrakkerja->getProfileLapKontrak($id)->first();

      $data['field'] = $qKontrakkerja;
      //format 1
      $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      $data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      
        $pdf = PDF::loadView('Personalia.lprkontrak', $data);
        $nama_file = 'PKWT';

      
      $pdf->save(storage_path().'_filename.pdf');
      
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');


    }


}
