<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\UserModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\CalonKeluargaModel;

class CalonKeluargaController extends Controller
{
     public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/calonkeluarga/index/".$idkry;
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qKeluarga              = new CalonKeluargaModel;
        $qMaster                = new MasterModel;
       
        /* ----------
         Source
        ----------------------- */
        $qKaryawan             = $qMaster->getCalonKaryawan($idkry)->first();
                     
        // $data["form_act_add"]  = "/calonkeluarga/add/".$idkry;
        //Akses user
        $qUser                  = new UserModel;
        $qhakAkses              = $qUser->getAksesuser(Auth::user()->id)->first();
        $data["hakakses"]       = $qhakAkses;
        
        // $data["form_act_add"]  = "/kontak/add/".$idkry;
        // $data["form_act_edit"]  = "/keluarga/add/".$idkry;
        
        # ---------------*/
        
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));


        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        if(count($data["action"]) > 0 ){
          foreach($data["action"] as $key=> $value){
              $url = $value->url.'/'.$idkry;
              $data["action"][$key]->url = $url;
          }
        }

        $data["tabs"]          = array(array("label"=>"Biodata", "url"=>"/detailcalonkaryawan/index/".$idkry, "active"=>""),
                                 array("label"=>"Keluarga", "url"=>"/calonkeluarga/index/".$idkry, "active"=>"active"),
                                 array("label"=>"Kontak Lain", "url"=>"/calonkontak/index/".$idkry, "active"=>""), 
                                 array("label"=>"Pendidikan", "url"=>"/calonrpendidikan/index/".$idkry, "active"=>""),
                                  array("label"=>"Pengalaman", "url"=>"/calonpengalaman/index/".$idkry, "active"=>""),
                                  // array("label"=>"Psikotes", "url"=>"/psikotes/index/".$idkry, "active"=>""),
                                 
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_keluarga"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"Nama"
                                                ,"name"=>"nama_keluarga"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Hubungan"
                                                ,"name"=>"hubungan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tempat Lahir"
                                                ,"name"=>"tempat_lahir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"TglLahir"
                                                ,"name"=>"tgl_lahir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Jenis Kelamin"
                                                ,"name"=>"jenis_kelamin"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Pendidikan"
                                                ,"name"=>"pendidikan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Pekerjaan"
                                                ,"name"=>"pekerjaan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),

                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_CALON_KELUARGA" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_CALON_KELUARGA");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_CALON_KELUARGA");
        }
        # ---------------
        $data["select"]        = $qKeluarga->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qKeluarga->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
       
         return view("default.dtlkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Keluarga";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkeluarga/save";
        
        $qMaster               = new MasterModel;
        
        $qGroups               = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubunganKeluarga();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusKeluarga();
        $qPendidikan           = getSelectPendidikan();    
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_text(array("name"=>"nama_keluarga", "label"=>"Nama", "mandatory"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes", "source"=>$qHubungan));        
        $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamim", "mandatory"=>"yes", "source"=>$qJK));        
        $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "source"=>$qPendidikan));        
        $data["fields"][]      = form_text(array("name"=>"pekerjaan", "label"=>"Pekerjaan", "mandatory"=>"no"));
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



    public function save(Request $request) {
       

        $rules = array(
                      
                      'nama_keluarga' => 'required'                     );

        $messages = ['nama_keluarga.required' => 'Keluarga harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/calonkeluarga/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKeluarga  = new CalonKeluargaModel;
            # ---------------
            $qKeluarga->createData($request);
            # ---------------
            session()->flash("success_message", "Keluarga has been saved");
            # ---------------
            return redirect("/calonkeluarga/index/". $request->id_karyawan);
        }
    }

    public function edit($idk, $id) {
        $data["title"]        = "Edit Keluarga";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/calonkeluarga/update";
        /* ----------
         Keluarga
        ----------------------- */
        $qMaster           = new MasterModel;
        $qKeluarga             = new CalonKeluargaModel;
        /* ----------
         Source
        ----------------------- */
        //$idkeluarga             = explode("&", $id); 
        $qKeluarga              = $qKeluarga->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubunganKeluarga();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusKeluarga();
        $qPendidikan           = getSelectPendidikan();  
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Keluarga", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qKeluarga->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
      
        $data["fields"][]      = form_text(array("name"=>"nama_keluarga", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qKeluarga->nama_keluarga));
        $data["fields"][]      = form_select(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes", "source"=>$qHubungan, "value"=>$qKeluarga->hubungan));        
        $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamim", "mandatory"=>"yes", "source"=>$qJK, "value"=>$qKeluarga->jenis_kelamin));        
      
        $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>"yes", "value"=>$qKeluarga->tempat_lahir));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "mandatory"=>"yes", "value"=>displayDMY($qKeluarga->tgl_lahir,"/")));
        $data["fields"][]      = form_select(array("name"=>"pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "source"=>$qPendidikan, "value"=>$qKeluarga->pendidikan));        
        $data["fields"][]      = form_text(array("name"=>"pekerjaan", "label"=>"Pekerjaan", "mandatory"=>"no","value"=>$qKeluarga->pekerjaan));
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'nama_keluarga' => 'required|',
                    
                    'hubungan' => 'required|'                          
        );

        $messages = [
                    'nama_keluarga.required' => 'Nama Keluarga harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/calonkeluarga/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKeluarga      = new CalonKeluargaModel;
            # ---------------
            $qKeluarga->updateData($request);
            # ---------------
            session()->flash("success_message", "Keluarga has been updated");
            # ---------------
            return redirect("/calonkeluarga/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($idk, $id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/calonkeluarga/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKeluarga             = new CalonKeluargaModel;

       //$idkeluarga             = explode("&", $id); 
        $qKeluarga              = $qKeluarga->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubunganKeluarga();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusKeluarga();
        $qPendidikan           = getSelectPendidikan();  
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Keluarga", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qKeluarga->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
      
        $data["fields"][]      = form_text(array("name"=>"nama_keluarga", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qKeluarga->nama_keluarga));
        $data["fields"][]      = form_select(array("name"=>"hubungan", "label"=>"Hubungan", "mandatory"=>"yes", "source"=>$qHubungan, "value"=>$qKeluarga->hubungan));        
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
            $qKeluarga     = new CalonKeluargaModel;
            # ---------------
            $qKeluarga->removeData($request);
            # ---------------
            session()->flash("success_message", "Keluarga has been removed");
      # ---------------
        return redirect("/calonkeluarga/index/". $request->input("id_karyawan")); 
    }
    
}
