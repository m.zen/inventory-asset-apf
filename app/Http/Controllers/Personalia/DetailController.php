<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\KaryawanModel;
class DetailController extends Controller
{
     public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));

    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/keluarga/index";
       // $data["active_page"]    = (empty($page)) ? 1 : $page;
        //$data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        // $qMenu                  = new MenuModel;
        // $qMaster                = new MasterModel;
        // /* ----------
        //  Source
        // ----------------------- */
        // $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        // //$data["form_act_add"]  = "/keluarga/add/".$idkry;
        // //$data["form_act_edit"]  = "/keluarga/add/".$idkry."/".$;
        
        // # ---------------*/
        
         


        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);


        $data["tabs"]          = array(array("label"=>"Biodata", "url"=>"/detail/index/".$idkry, "active"=>"active"),
                                 array("label"=>"Keluarga", "url"=>"/keluarga/index/".$idkry, "active"=>""),
                                 array("label"=>"Kontak Lain", "url"=>"/kontak/index/".$idkry, "active"=>""), 
                                 array("label"=>"Pendidikan", "url"=>"/rpendidikan/index/".$idkry, "active"=>""),
                                 array("label"=>"Pengalaman", "url"=>"/pengalaman/index/".$idkry, "active"=>"")
                           );
        /* ----------
         Detail Data
        ----------------------- */
        // $qMaster              = new MasterModel;
        // $qKaryawan            = new KaryawanModel;
        // $qKaryawan     = $qKaryawan->getProfile($idkry)->first();
      
        // $qGroups                = $qMaster->getSelectGroup();
        // $qCabang                = $qMaster->getSelectCabang();
        // $qDepartemen            = $qMaster->getSelectDepartemen();
        // $qJabatan               = $qMaster->getSelectJabatan();
        // $qStskerja              = getSelectStskerja();
        // $qJabatan               = $qMaster->getSelectJabatan();
        // $qStskerja              = getSelectStskerja();
        // $qAgama                 = getSelectAgama();
        // $qStatusNikah           = getSelectStatusNikah();
        // $qJK                    = getSelectJenisKelamin();
        // $qPendidikan            = getSelectPendidikan();

        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_text(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "readonly"=>"readonly","value"=>""));
       $data["fields"][]      = form_text(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "readonly"=>"readonly","value"=>""));
       
       
       
      return view("default.formdtl", $data);

       // return view("default.dtlkaryawan", $data);
    

       
    }

    
}
