<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\KarirModel;
use App\Model\Personalia\KaryawanModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PDF;
class KarirController extends Controller
{
    public function __construct(Request $request)
     {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        
        $data["title"]          = "Promosi/Mutasi/Demosi";//ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karir/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu     = new MenuModel;
        $qKarir    = new KarirModel;
        $qMaster   = new MasterModel;

        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          $data["tabs"]          = array(array("label"=>"Proses Pengajuan", "url"=>"/karir/index", "active"=>"active"),
                                array("label"=>"Inquery Data", "url"=>"/karir/inqkarir_index", "active"=>"")
                                 
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                     array("label"=>"No Surat"
                                                ,"name"=>"no_surat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     array("label"=>"NIK"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Jenis Mutasi"
                                                ,"name"=>"nama_karir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),

                                     
                                       array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Jabatan Baru"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tanggal"
                                                ,"name"=>"Tanggal_karir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),



                                       

                                     ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KARIR" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARIR");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARIR");
        }
        # ---------------
        $data["select"]        = $qKarir->getList_proses($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKarir->getList_proses($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.tablist", $data);
    }

    public function add() {
        //dd($request);
        $data["title"]         = "Add Promosi/Mutasi/Demosi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karir/save";
           $data["url_select"]    = "/karir/get_profile/";
      
        
        $qMaster               = new MasterModel;
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        $qKaryawan             = $qMaster->getSelectKaryawan();
        $qKaryawan             = array_merge($collection,$qKaryawan);
        $qCabangs              = $qMaster->getSelectCabang();
        $qCabangs              = array_merge($collection,$qCabangs);
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		    $qDepartemens          = array_merge($collection,$qDepartemens);
        $qJabatan              = $qMaster->getSelectJabatan(); 
		    $qJabatan              = array_merge($collection,$qJabatan);
        $qKarir		             = getSelectKarir();
        $qKarir                = array_merge($collection,$qKarir);
        
       

        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan, "value"=>""));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_karir", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
       
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>""));        
        $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>""));
       
         $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes"));
         // $data["fields"][]      = form_datepicker(array("name"=>"tgl_probation", "label"=>"Tanggal Probation (Promosi)", "first_selected"=>"yes"));
         $data["fields"][]      = form_text(array("name"=>"keterangan_karir", "label"=>"Keterangan", "mandatory"=>"","value"=>""));
          $data["fields"][]      = form_select(array("name"=>"nama_karir", "label"=>"Jenis Mutasi", "mandatory"=>"yes", "source"=>$qKarir));
           $data["fields"][]      = form_select(array("name"=>"id_jabatantj", "label"=>"Bertanggung Jawab Kepada", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>""));
            $data["fields"][]      = form_text(array("name"=>"tembusan1", "label"=>"Tembusan1", "mandatory"=>""));
           $data["fields"][]      = form_text(array("name"=>"tembusan2", "label"=>"Tembusan2", "mandatory"=>""));
            $data["fields"][]      = form_text(array("name"=>"tembusan3", "label"=>"Tembusan3", "mandatory"=>""));
             $data["fields"][]      = form_text(array("name"=>"tembusan4", "label"=>"Tembusan4", "mandatory"=>""));
              $data["fields"][]      = form_text(array("name"=>"tembusan5", "label"=>"Tembusan5", "mandatory"=>""));

       
       
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("Personalia.karir_form", $data);
    }



    public function save(Request $request) {
       

        $rules = array(
                      
                      'no_surat' => 'required'                     );

        $messages = ['no_surat.required' => 'No Surat harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karir/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKarir  = new KarirModel;
            # ---------------
            $qKarir->createData($request);
            # ---------------
            session()->flash("success_message", "Karir has been saved");
            # ---------------
            return redirect("/karir/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Promosi/Mutasi/Demosi";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karir/update";
        /* ----------
         Karir
        ----------------------- */
        $qMaster           = new MasterModel;
        $qMKarir             = new KarirModel;
        /* ----------
         Source
        ----------------------- */
        //$idKarir             = explode("&", $id); 
        $qKarir                = $qMKarir->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
        $qJabatan              = $qMaster->getSelectJabatan();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        
        
        $qDBKarir              = getSelectKarir();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Karir", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan, "value"=>$qKarir->id_karyawan));
         
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_karir", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>displayDMY($qKarir->tgl_karir,"/"), "first_selected"=>"yes"));
        // $data["fields"][]      = form_datepicker(array("name"=>"tgl_probation", "label"=>"Tanggal Probation (Promosi)","value"=>displayDMY($qKarir->tgl_probation,"/"), "first_selected"=>"yes"));
        
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKarir->id_cabangbaru));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKarir->id_departemenbaru));
       $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKarir->id_jabatanbaru));
 
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qKarir->no_surat));
         $data["fields"][]      = form_text(array("name"=>"keterangan_karir", "label"=>"Keterangan", "mandatory"=>"","value"=>$qKarir->keterangan_karir));
          $data["fields"][]      = form_select(array("name"=>"nama_karir", "label"=>"Jenis Mutasi", "mandatory"=>"yes", "source"=>$qDBKarir,"value"=>$qKarir->nama_karir));
        $data["fields"][]      = form_select(array("name"=>"id_jabatantj", "label"=>"Bertanggung Jawab Kepada", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKarir->id_jabatantj));
           $data["fields"][]      = form_text(array("name"=>"tembusan1", "label"=>"Tembusan1", "mandatory"=>"","value"=>$qKarir->tembusan1));
           $data["fields"][]      = form_text(array("name"=>"tembusan2", "label"=>"Tembusan2", "mandatory"=>"","value"=>$qKarir->tembusan2));
            $data["fields"][]      = form_text(array("name"=>"tembusan3", "label"=>"Tembusan3", "mandatory"=>"","value"=>$qKarir->tembusan3));
             $data["fields"][]      = form_text(array("name"=>"tembusan4", "label"=>"Tembusan4", "mandatory"=>"","value"=>$qKarir->tembusan4));
              $data["fields"][]      = form_text(array("name"=>"tembusan5", "label"=>"Tembusan5", "mandatory"=>"","value"=>$qKarir->tembusan5));

      

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }


    public function batalapprove($id) {
        $data["title"]        = "Batal Approve";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karir/updatebatalapprove";
        /* ----------
         Karir
        ----------------------- */
        $qMaster           = new MasterModel;
        $qMKarir             = new KarirModel;
        /* ----------
         Source
        ----------------------- */
        //$idKarir             = explode("&", $id); 
        $qKarir                = $qMKarir->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
        $qJabatan              = $qMaster->getSelectJabatan();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        
        
        $qDBKarir              = getSelectKarir();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Karir", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan, "value"=>$qKarir->id_karyawan));
         
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_karir", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>displayDMY($qKarir->tgl_karir,"/"), "first_selected"=>"yes"));
        // $data["fields"][]      = form_datepicker(array("name"=>"tgl_probation", "label"=>"Tanggal Probation (Promosi)","value"=>displayDMY($qKarir->tgl_probation,"/"), "first_selected"=>"yes"));
        
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKarir->id_cabangbaru));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKarir->id_departemenbaru));
       $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKarir->id_jabatanbaru));
 
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qKarir->no_surat));
         $data["fields"][]      = form_text(array("name"=>"keterangan_karir", "label"=>"Keterangan", "mandatory"=>"","value"=>$qKarir->keterangan_karir));
          $data["fields"][]      = form_select(array("name"=>"nama_karir", "label"=>"Jenis Mutasi", "mandatory"=>"yes", "source"=>$qDBKarir,"value"=>$qKarir->nama_karir));
        $data["fields"][]      = form_select(array("name"=>"id_jabatantj", "label"=>"Bertanggung Jawab Kepada", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKarir->id_jabatantj));
           $data["fields"][]      = form_text(array("name"=>"tembusan1", "label"=>"Tembusan1", "mandatory"=>"","value"=>$qKarir->tembusan1));
           $data["fields"][]      = form_text(array("name"=>"tembusan2", "label"=>"Tembusan2", "mandatory"=>"","value"=>$qKarir->tembusan2));
            $data["fields"][]      = form_text(array("name"=>"tembusan3", "label"=>"Tembusan3", "mandatory"=>"","value"=>$qKarir->tembusan3));
             $data["fields"][]      = form_text(array("name"=>"tembusan4", "label"=>"Tembusan4", "mandatory"=>"","value"=>$qKarir->tembusan4));
              $data["fields"][]      = form_text(array("name"=>"tembusan5", "label"=>"Tembusan5", "mandatory"=>"","value"=>$qKarir->tembusan5));

      

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Update&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }





 public function delete($id) {
        $data["title"]        = "Edit Promosi/Mutasi/Demosi";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karir/remove";
        /* ----------
         Karir
        ----------------------- */
        $qMaster           = new MasterModel;
        $qMKarir             = new KarirModel;
        /* ----------
         Source
        ----------------------- */
        //$idKarir             = explode("&", $id); 
        $qKarir                = $qMKarir->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
        $qJabatan              = $qMaster->getSelectJabatan();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        
        
        $qDBKarir              = getSelectKarir();
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Karir", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan, "value"=>$qKarir->id_karyawan));
         
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_karir", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>displayDMY($qKarir->tgl_karir,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_probation", "label"=>"Tanggal Probation (Promosi)","value"=>displayDMY($qKarir->tgl_probation,"/"), "first_selected"=>"yes"));
        
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKarir->id_cabangbaru));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKarir->id_departemenbaru));
       $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKarir->id_jabatanbaru));
 
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qKarir->no_surat));
         $data["fields"][]      = form_text(array("name"=>"keterangan_karir", "label"=>"Keterangan", "mandatory"=>"","value"=>$qKarir->keterangan_karir));
          $data["fields"][]      = form_select(array("name"=>"nama_karir", "label"=>"Jenis Mutasi", "mandatory"=>"yes", "source"=>$qDBKarir,"value"=>$qKarir->nama_karir));
      

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Delete&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karir/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKarir      = new KarirModel;
            # ---------------
            $qKarir->updateData($request);
            # ---------------
            session()->flash("success_message", "Karir has been updated");
            # ---------------
            return redirect("/karir/index");
        }
     }

      public function updatebatalapprove(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karir/batalapprove/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKarir      = new KarirModel;
            # ---------------
            $qKarir->updateDatabatalapprove($request);
            # ---------------
            session()->flash("success_message", "Karir has been updated");
            # ---------------
            return redirect("/karir/inqkarir_index");
        }
     }
public function remove(Request $request) {
        
            $qKarir      = new KarirModel;
            # ---------------
            $qKarir->removeData($request);
            # ---------------
            session()->flash("success_message", "Karyawan has been removed");
        
      # ---------------
        return redirect("/karir/index"); 
    }



     public function approve($id) {
        $data["title"]        = "Approval Promosi/Mutasi/Demosi";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karir/update_approve";
        /* ----------
         Karir
        ----------------------- */
        $qMaster           = new MasterModel;
        $qMKarir             = new KarirModel;
        /* ----------
         Source
        ----------------------- */
        //$idKarir             = explode("&", $id); 
        $qKarir                = $qMKarir->getProfile($id)->first();
        $qCabangs              = $qMaster->getCabang($qKarir->id_cabangbaru)->get()->first();
        $qDepartemens          = $qMaster->getDepartemen($qKarir->id_departemenbaru)->get()->first();  
        $qJabatan              = $qMaster->getJabatan($qKarir->id_jabatanbaru)->get()->first();
        $qKaryawan             = $qMaster->getSelectKaryawan();
        
        $qDBKarir              = getSelectKarir();
        $qApprove              = getListApprove();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
        $qJabatan              = $qMaster->getSelectJabatan();
        
        /* ----------
         Fields
        ----------------------- */
       
         
       
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Karir", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan, "value"=>$qKarir->id_karyawan));
        
        $data["fields"][]      = form_select(array("name"=>"id_cabangbaru", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabangs, "value"=>$qKarir->id_cabangbaru));        
        $data["fields"][]      = form_select(array("name"=>"id_departemenbaru", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemens, "value"=>$qKarir->id_departemenbaru));
       $data["fields"][]      = form_select(array("name"=>"id_jabatanbaru", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan, "value"=>$qKarir->id_jabatanbaru));


        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "readonly"=>"readonly","value"=>$qKarir->no_surat));
         $data["fields"][]      = form_text(array("name"=>"keterangan_karir", "label"=>"Keterangan", "readonly"=>"readonly" ,"value"=>$qKarir->keterangan_karir));
         $data["fields"][]      = form_text(array("name"=>"nama_karir", "label"=>"Keterangan", "readonly"=>"readonly" ,"value"=>$qKarir->nama_karir));
          $data["fields"][]      = form_select(array("name"=>"status_proses", "label"=>"Approval", "mandatory"=>"yes", "source"=> $qApprove, "value"=>""));
        
        $data["fields"][]      = form_text(array("name"=>"Komentar", "label"=>"Komentar", "value"=>""));
         


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update_approve(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karir/approve/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qKarir      = new KarirModel;
            # ---------------
            $qKarir->updateData_approve($request);
            # ---------------
            session()->flash("success_message", "Karir has been updated");
            # ---------------
            return redirect("/karir/index");
        }
     }

      public function get_profile_karyawan($id) {

      $qKaryawan      = new KaryawanModel;
      $qDataKaryawan  = $qKaryawan->getProfile($id);
      
      // Return dalam bentuk json untuk diproses oleh jquery
      return json_encode($qDataKaryawan->first());

    }

    
    public function inqkarir_index(Request $request, $page=null)
    {
        $data["title"]          = "Inquery Data Promosi/Demosi/Mutasi";
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karir/inqkarir_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKarir      = new KarirModel;


        # ---------------
        //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

                $data["tabs"]          = array(array("label"=>"Proses Pengajuan", "url"=>"/karir/index", "active"=>""),
                                array("label"=>"Inquery Data", "url"=>"/karir/inqkarir_index", "active"=>"active"));


         $data["form_act_export"]    = "/karir/export";
         $data["label_export"]    = "Export Data";


        $qAkses = DB::table("sys_users")
                            ->select("nik","group_id")
                            ->where('nik','=',Auth::user()->nik)->first();
        $data['hakAskses'] =  $qAkses->group_id; 

       if ($qAkses->group_id == 1)
       {
         $data["form_act_edit"]    =  "/karir/batalapprove";
         $data["label_edit"]    = "Batal Approve";

       }
        
        
        $qMaster               = new MasterModel;
        $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
        $qCabang             = $qMaster->getSelectCabang();
        $qCabang             = array_merge($collection,$qCabang);
        $qDepartemens        = $qMaster->getSelectDepartemen();
        $qDepartemens        = array_merge($collection,$qDepartemens);
        $qJabatan            = $qMaster->getSelectJabatan();
        $qJabatan            = array_merge($collection,$qJabatan);
        $qKarirList          = getSelectKarir();
        $qKarirList          = array_merge($collection,$qKarirList);

        
        //$qViewKaryawan        = $qKontrakkerjamdl->getProfileKry($idkry)->first();  
       // $qViewFormatsurat     = $qKontrakkerjamdl->getProfileKry($idkry)->first(); 

         if($request->has('tgl_awal')) {
            session(["SES_SEARCH_AWAL" => $request->input("tgl_awal")]);
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = displayDMY("2005-01-01",'/');
        }

       if($request->has('tgl_akhir')) {
            session(["SES_SEARCH_AKHIR" => $request->input("tgl_akhir")]);
            # ---------------
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir = date("d/m/Y");
        }


        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_CABANG");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = $request->session()->get("SES_SEARCH_DEPARTEMEN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = $request->session()->get("SES_SEARCH_JABATAN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = "-";

        }
        if($request->has('nama_karir')) {
            session(["SES_SEARCH_NAMAKARIR" => $request->input("nama_karir")]);
            # ---------------
            $data["nama_karir"]   = $request->session()->get("SES_SEARCH_NAMAKARIR");
            $nama_karir           = $request->session()->get("SES_SEARCH_NAMAKARIR");

        }
        else
        {

            $data["nama_karir"]   = $request->session()->get("SES_SEARCH_NAMAKARIR");
            $nama_karir           = "-";

        }


       
        

        if($request->has('text_search')) {
            session(["SES_SEARCH_INQKARIR" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_INQKARIR");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_INQKARIR");
        }
             
       
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Masuk (Awal)", "value"=>$TglAwal));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Masuk (Akhir)", "value"=>$TglAkhir));   
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
          $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemens,"value"=>$id_departemen));
          $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
          $data["fields"][]      = form_select(array("name"=>"nama_karir", "label"=>"Jenis Mutasi", "mandatory"=>"", "source"=>$qKarirList,"value"=>$nama_karir));
      
      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));
          // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                     array("label"=>"No Surat"
                                                ,"name"=>"no_surat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"NIK"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                          array("label"=>"Jenis Mutasi"
                                                ,"name"=>"nama_karir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),

                                       array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Jabatan Baru"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tanggal"
                                                ,"name"=>"Tanggal_karir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),



                                       

                                     );


       # ---------------

         if($request->has('tgl_awal')) {
            session(["SES_SEARCH_AWAL" => $request->input("tgl_awal")]);
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
        }

       if($request->has('tgl_akhir')) {
            session(["SES_SEARCH_AKHIR" => $request->input("tgl_akhir")]);
            # ---------------
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
        }


        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
        }
        if($request->has('id_departemen')) {
            session(["SES_SEARCH_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN
              ");
        }
        if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SSES_SEARCH_JABATAN");
        }
        if($request->has('nama_karir')) {
            session(["SES_SEARCH_NAMAKARIR" => $request->input("nama_karir")]);
            # ---------------
            $data["nama_karir"]   = $request->session()->get("SSES_SEARCH_NAMAKARIR");
        }









        if($request->has('text_search')) {
            session(["SES_SEARCH_KONTRAKALL" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAKALL");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KONTRAKALL");
        }
        if($request->has('text_search')) {
            session(["SES_SEARCH_INQKARIR" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_INQKARIR");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_INQKARIR");
        }
        # ---------------
        $data["select"]        = $qKarir->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKarir->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }
     public function export(Request $request)
    {
        if($request->session()->get("SES_SEARCH_AWAL")) {
            # ---------------
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $TglAwal            = displayDMY("2005-01-01",'/');
        }

       if($request->session()->get("SES_SEARCH_AKHIR")) {
            # ---------------
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $TglAkhir = date("d/m/Y");
        }

       $id_cabang="";
       $tingkat_sp="";
       $kategori_sp;
       if(session()->get("SES_SEARCH_CABANG") != '-'){
                $d_cabang= $request->session()->get("SES_SEARCH_CABANG");
       }
            
        if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $id_departemenbaru=$request->session()->get("SES_SEARCH_DEPARTEMEN");
        }
            
        if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $id_jabatanbaru=$request->session()->get("SES_SEARCH_JABATAN");
         }
         if(session()->get("SES_SEARCH_NAMAKARIR") != '-'){
                $nama_karir=$request->session()->get("SES_SEARCH_NAMAKARIR");
         }
        

          $awal =setYMD($TglAwal,"/");
          $akhir=setYMD($TglAkhir,"/");   
         $query  = DB::table("pa_karir as a")
                            ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang","d.nama_departemen","e.nama_jabatan","f.nama_cabang as nama_cabangawal","g.nama_departemen as nama_departemenawal","h.nama_jabatan as nama_jabatanawal")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                          
                            ->leftjoin("m_cabang as c","c.id_cabang","=","a.id_cabangbaru")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","a.id_departemenbaru")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","a.id_jabatanbaru")
                            ->leftjoin("m_cabang as f","f.id_cabang","=","a.id_cabangawal")
                            ->leftjoin("m_departemen as g","g.id_departemen","=","a.id_departemenawal")
                            ->leftjoin("m_jabatan as h","h.id_jabatan","=","a.id_jabatanawal")
                            
                             ->whereBetween("a.tgl_karir", [$awal , $akhir])  
                             ->orderBy("a.id_karir","ASC")->get();
                            
        

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR SURAT MUTASI');
        $sheet->setCellValue('A2', 'PERIODE TANGGAL : ' .$TglAwal ." SAMPAI " .$TglAkhir);
        $spreadsheet->getActiveSheet()->getStyle('A3:M3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
        
        $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'NAMA');
        $sheet->setCellValue('C3', 'N I K');
         $sheet->setCellValue('D3', 'JABATAN AWAL');
        $sheet->setCellValue('E3', 'DEPARTEMEN AWAL');
        $sheet->setCellValue('F3', 'CABANG AWAL');
        $sheet->setCellValue('G3', 'JABATAN BARU');
        $sheet->setCellValue('H3', 'DEPARTEMEN BARU');
        $sheet->setCellValue('I3', 'CABANG BARU');
        $sheet->setCellValue('J3', 'TANGGAL SURAT');
        $sheet->setCellValue('K3', 'JENIS MUTASI');
        $sheet->setCellValue('L3', 'NO SK');
        $sheet->setCellValue('M3', 'KETERANGAN');
      
       
       
        
        foreach($query as $row) 
        {
      
                  $cabangawal="";
                  if (!empty($row->nama_cabangawal))
                  {
                        $cabangawal=$row->nama_cabangawal;
                       
                  }
                   
                  $departemenawal = "";
                  if (!empty($row->nama_departemenawal))
                   {
                         $departemenawal=$row->nama_departemenawal;
                  }
                   $jabatanawal = "";
                  if (!empty($row->nama_jabatanawal))
                   
                   {
                      $jabatanawal=$row->nama_jabatanawal;
                  }

                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('C'.$baris,$row->nik);
                  $sheet->setCellValue('D'.$baris,$jabatanawal);
                  $sheet->setCellValue('E'.$baris,$departemenawal);
                  $sheet->setCellValue('F'.$baris, $cabangawal);
                  $sheet->setCellValue('G'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('H'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('I'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('J'.$baris,displayDMY($row->tgl_karir,"/"));
                  $sheet->setCellValue('K'.$baris,$row->nama_karir);
                  $sheet->setCellValue('L'.$baris,$row->no_surat);
                  $sheet->setCellValue('M'.$baris,$row->keterangan_karir);
                  
                  
       
                 $baris=$baris+1;
        }

        foreach(range('B','M') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
        
        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "KarirExport";  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   


    }


     public function Cetak( $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCKarir           = new KarirModel;
        /* ----------
         Source
                 ----------------------- */
        //$idPeringatan             = explode("&", $id); 
       // $qPeringatan          = $qPeringatan->getProfile($id)->first();
       
      /* ----------
        Source
      ----------------------- */

      
      //$idKontrakkerja     = explode("&", $id); 
      $qKarir            = $qCKarir->getCetakkarir($id);
      $qNama             = $qCKarir->getCetakkarir($id)->first();
      $data["karir"]      = $qKarir ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      
        $pdf = PDF::loadView('Personalia.lprsskpro_mutasi_demosi',  $data);
        $nama_file =  $qNama->nama_karyawan;

        //$nama_file =  $qKontrakkerja->nama_karyawan & "-" & $qKontrakkerja->id_kontrakkerja;

      
       $pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprkontrak", $data);

    }
}
