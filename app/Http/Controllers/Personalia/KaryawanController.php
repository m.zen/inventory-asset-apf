<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Personalia\KaryawanModel;
use App\Model\Payroll\InputgajiModel;
use App\Model\Master\UserModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PDF;
class KaryawanController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }


    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawan/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawan              = new KaryawanModel;
        $qMaster                = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];

         $qCabang                = array_merge($collection,$qCabang);
         $qDepartemen            = $qMaster->getSelectDepartemen();
         $qDepartemen            = array_merge($collection,$qDepartemen);
         $qJabatan               = $qMaster->getSelectJabatan();
         $qJabatan               = array_merge($collection,$qJabatan);
         $qStskerja              = getSelectStskerja();
         $qStskerja              = array_merge($collection,$qStskerja);;
         
         
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        
        ----------------------- */
       // $data["form_act_edit"]    = "/karyawan/edit";
       // $data["label_edit"]    = "Perubahan Data";

        if($request->has('tgl_awal')) {
            session(["SES_SEARCH_AWAL" => $request->input("tgl_awal")]);
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = displayDMY("2000-01-01",'/');
        }

       if($request->has('tgl_akhir')) {
            session(["SES_SEARCH_AKHIR" => $request->input("tgl_akhir")]);
            # ---------------
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir = date("d/m/Y");
        }
        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_CABANG");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = $request->session()->get("SES_SEARCH_DEPARTEMEN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = $request->session()->get("SES_SEARCH_JABATAN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = "-";

        }
        if($request->has('status_kerja')) {
            session(["SES_SEARCH_STATUSKERJA" => $request->input("status_kerja")]);
            # ---------------
            $data["status_kerja"]   = $request->session()->get("SES_SEARCH_STATUSKERJA");
            $status_kerja           = $request->session()->get("SES_SEARCH_STATUSKERJA");

        }
        else
        {

            $data["nama_karir"]   = $request->session()->get("SES_SEARCH_STATUSKERJA");
            $status_kerja           = "-";

        }


        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Masuk (Awal)", "value"=>$TglAwal));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Masuk (Akhir)", "value"=>$TglAkhir));       
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemen,"value"=>$id_departemen));
      $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
        $data["fields"][]      = form_select(array("name"=>"status_kerja", "label"=>"Status Kerja", "mandatory"=>"", "source"=> $qStskerja,"value"=>$status_kerja));
        

      


      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));

          $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"Karyawan Aktif", "url"=>"/karyawan/index", "active"=>"active")
                                       ,array("label"=>"Karyawan Non Aktif", "url"=>"/karyawan/nonaktif_index", "active"=>"")
                                        , array("label"=>"Karyawan Ulang Tahun", "url"=>"/karyawan/ulangtahun_index", "active"=>"")
                                        , array("label"=>"Jaminan", "url"=>"/karyawan/jaminan_index", "active"=>""));

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""), 
                                    array("label"=>"Tgl Masuk"
                                                ,"name"=>"Tanggal_masuk"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                    array("label"=>"Status Kerja"
                                                ,"name"=>"status_kerja"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""));
          

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KARYAWAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWAN");
        }
        # ---------------
        $data["select"]        = $qKaryawan->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawan->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }

    public function add() {
        $data["title"]         = "Add Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawan/save";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster               = new MasterModel;

        /* ----------
         Source
        ----------------------- */
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getSelectStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qPtkp                 = $qMaster->getSelectPtkp();
        $qGolongandarah         = getSelectGoldarah();
        $qPtkp                 = $qMaster->getSelectPtkp(); /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "mandatory"=>"yes"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>""));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_masuk", "label"=>"Tanggal Masuk", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_percobaan", "label"=>"Tanggal Percobaan", "mandatory"=>"","value"=>date("d/m/Y"), "first_selected"=>"yes"));
      
       //$data["fields"][]      = form_select(array("name"=>"id_grading", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan));
        $data["fields"][]      = form_text(array("name"=>"nonpwp", "label"=>"No NPWP",  "mandatory"=>""));
        $data["fields"][]      = form_select(array("name"=>"id_ptkp", "label"=>"PTKP Status", "mandatory"=>"yes", "source"=>$qPtkp));
        $data["fields"][]      = form_text(array("name"=>"nobpjskes", "label"=>"No BPJS Kesehatan",  "mandatory"=>""));
        $data["fields"][]      = form_text(array("name"=>"nobjstk", "label"=>"No BPJS Tenaga Kerja",  "mandatory"=>""));
  
       $data["fields"][]      = form_text(array("name"=>"no_rekening", "label"=>"No Rekening",  "mandatory"=>""));
       $data["fields"][]      = form_text(array("name"=>"asrekening", "label"=>"Atas Nama Rekening",  "mandatory"=>""));
      
       $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>""));
       $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Email", "mandatory"=>""));
       $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"Data Personal"));
       $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku_ktp", "label"=>"Tanggal Berlaku KTP", "mandatory"=>"","value"=>date("d/m/Y"), "first_selected"=>"yes"));
      
       $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>""));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
       $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamin", "mandatory"=>"yes", "source"=>$qJK));
       $data["fields"][]      = form_select(array("name"=>"agama", "label"=>"Agama", "mandatory"=>"yes", "source"=>$qAgama ));
       
       $data["fields"][]      = form_select(array("name"=>"status_nikah", "label"=>"Status Pernikahan", "mandatory"=>"yes", "source"=>$qStatusNikah));
       $data["fields"][]      = form_number(array("name"=>"jml_anak", "label"=>"Jumlah Anak",  "value"=>number_format(0,0)));


       $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "source"=>$qPendidikan));
       
       $data["fields"][]      = form_select(array("name"=>"golongan_darah", "label"=>"Golongan Darah", "mandatory"=>"", "source"=>$qGolongandarah));
       $data["fields"][]      = form_textarea(array("name"=>"alamat_valid", "label"=>"Alamat KTP", "mandatory"=>"yes", "row"=>"2"));
       $data["fields"][]      = form_textarea(array("name"=>"alamat", "label"=>"Alamat Tinggal", "mandatory"=>"yes", "row"=>"2"));
       
       $data["fields"][]      = form_text(array("name"=>"no_telpon", "label"=>"No Telpon", "mandatory"=>""));
       $data["fields"][]      = form_text(array("name"=>"hp", "label"=>"hp", "mandatory"=>"yes"));
       $data["fields"][]      = form_hidden(array("name"=>"password", "label"=>"password", "mandatory"=>"yes","value"=>"password"));
       
  # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) 
    {
        $rules = array(
                      
                      'nama_karyawan' => 'required'                     );

        $messages = ['nama_karyawan.required' => 'Karyawan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/Karyawan/add")
                ->withErrors($validator)
                ->withInput();
        } else 
        {
   // DB::beginTransaction();
     //       try{



            $qKaryawan  = new KaryawanModel;
            $qUser      = new UserModel;
            
            $qKaryawan->createData($request);
            $qUser->createData($request);
            session()->flash("success_message", "Karyawan has been saved");
          //  DB::commit();

       ///  } catch (\Exception $e) {
              //  DB::rollback();
                
            //}

            # ---------------
            return redirect("/karyawan/index");
        }
    }

    public function edit($id) 
    {
        $data["title"]        = "Edit Karyawan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/update";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        $qKaryawan     = $qKaryawan->getProfile($id)->first();
      
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           =  getStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qGolongandarah         = getSelectGoldarah();
        $qPtkp                 = $qMaster->getSelectPtkp();
         $qJaminan                = $qMaster->getSelectJaminan();
         // dd($qKaryawan->id_pendidikan);
        /* ----------
         
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qKaryawan->nik));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan",  "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
        $data["fields"][]      = form_select(array("name"=>"id_cabang_hire", "label"=>"Cabang Point Of hire", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang_hire));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_melamar", "label"=>"Tanggal Melamar", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_melamar,"/")));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_masuk", "label"=>"Tanggal Masuk", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_masuk,"/")));
        $data["fields"][]      = form_text(array("name"=>"nonpwp", "label"=>"No NPWP",  "mandatory"=>"", "value"=>$qKaryawan->nonpwp));
       //   $data["fields"][]      = form_text(array("name"=>"nobpjskes", "label"=>"No BPJS Kesehatan",  "mandatory"=>"", "value"=>$qKaryawan->nobpjskes));
        $data["fields"][]      = form_text(array("name"=>"nobjstk", "label"=>"No BPJS Tenanaga Kerja",  "mandatory"=>"", "value"=>$qKaryawan->nobjstk));
  

       $data["fields"][]      = form_text(array("name"=>"no_rekening", "label"=>"No Rekening",  "mandatory"=>"", "value"=>$qKaryawan->no_rekening));
       $data["fields"][]      = form_text(array("name"=>"asrekening", "label"=>"Atas Nama Rekening",  "mandatory"=>"", "value"=>$qKaryawan->anrekening));
      
       $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>"", "value"=>$qKaryawan->noabsen));
       $data["fields"][]      = form_text(array("name"=>"email", "label"=>"Email", "mandatory"=>"", "value"=>$qKaryawan->email));
       $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"BIODATA KARYAWAN"));
       $data["fields"][]      = form_number(array("name"=>"no_ktp", "label"=>"No KTP/ Identitas", "mandatory"=>"yes", "value"=>$qKaryawan->no_ktp));
       $data["fields"][]      = form_text(array("name"=>"tempat_lahir", "label"=>"Tempat Lahir", "mandatory"=>"yes","value"=>$qKaryawan->tempat_lahir));
       $data["fields"][]      = form_datepicker(array("name"=>"tgl_lahir", "label"=>"Tanggal Lahir", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_lahir,"/")));
       $data["fields"][]      = form_select(array("name"=>"jenis_kelamin", "label"=>"Jenis Kelamin", "mandatory"=>"yes", "source"=>$qJK,"value"=>$qKaryawan->jenis_kelamin));
       $data["fields"][]      = form_select(array("name"=>"agama", "label"=>"Agama", "mandatory"=>"yes", "source"=>$qAgama,"value"=>$qKaryawan->agama));
       $data["fields"][]      = form_select(array("name"=>"status_nikah", "label"=>"Status Pernikahan", "mandatory"=>"yes", "source"=>$qStatusNikah,"value"=>$qKaryawan->status_nikah));
       // $data["fields"][]      = form_number(array("name"=>"jml_anak", "label"=>"Jumlah Anak",  "value"=>number_format($qKaryawan->jml_anak,0)));

           $data["fields"][]      = form_text(array("name"=>"nama_ibu", "label"=>"Nama Ibu Kandung", "mandatory"=>"","value"=>$qKaryawan->nama_ibu));
   
       $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "value"=>$qKaryawan->id_pendidikan,"source"=>$qPendidikan));
       $data["fields"][]      = form_select(array("name"=>"golongan_darah", "label"=>"Golongan Darah", "mandatory"=>"", "value"=>$qKaryawan->golongan_darah,"source"=>$qGolongandarah));
       
       $data["fields"][]      = form_textarea(array("name"=>"alamat_valid", "label"=>"Alamat KTP", "mandatory"=>"yes", "row"=>"2","value"=>$qKaryawan->alamat_valid));
       $data["fields"][]      = form_textarea(array("name"=>"alamat", "label"=>"Alamat Tinggal", "mandatory"=>"yes", "row"=>"2","value"=>$qKaryawan->alamat));
       $data["fields"][]      = form_text(array("name"=>"no_telpon", "label"=>"No Telpon", "mandatory"=>"","value"=>$qKaryawan->no_telpon));
       $data["fields"][]      = form_text(array("name"=>"hp", "label"=>"hp", "mandatory"=>"","value"=>$qKaryawan->hp));
          $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"DOKUMEN JAMINAN"));
        $data["fields"][]      = form_select(array("name"=>"id_jaminan", "label"=>"Jenis Jaminan", "mandatory"=>"", "withnull"=>"withnull","source"=> $qJaminan,"value"=>$qKaryawan->id_jaminan));
        $data["fields"][]      = form_text(array("name"=>"no_jaminan", "label"=>"No Jaminan", "mandatory"=>"","value"=>$qKaryawan->no_jaminan));
         $data["fields"][]      = form_text(array("name"=>"nama_jaminan", "label"=>"Atas Nama Jaminan", "mandatory"=>"","value"=>$qKaryawan->nama_dijaminan));
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
    
    }

     public function batalkeluar($id) {
        $data["title"]        = "Batal Keluar Karyawan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/updatebatalkeluar";
        /* ----------
         Karyawankeluar
        ----------------------- */
      
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        $qKaryawan     = $qKaryawan->getProfile($id)->first();
      
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        
         // dd($qKaryawan->id_pendidikan);
        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qKaryawan->nik));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan",  "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
        
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_keluar,"/")));
      
       
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
         

    }

    public function update(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
         {
            return redirect("/karyawan/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new KaryawanModel;
            $qUser      = new UserModel;
           
            # ---------------
            $qKaryawan->updateData($request);
            $qUser->updateDatakry($request);
            
           return redirect("/karyawan/index");

        }

    }
   
    public function updatebatalkeluar(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
         {
            return redirect("/karyawan/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new KaryawanModel;
           
            # ---------------
            $qKaryawan->updateDatabatalkeluar($request);
           // $qUser->updateDatakry($request);
            
           return redirect("/karyawan/index");

        }

    }
  
   public function jaminan_index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawan/jaminan_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawan              = new KaryawanModel;
        $qMaster                = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
           $statusjaminan         = [ (object)
                                    [
                                    'id' => '1',
                                    'name' => 'BSM (Aktif)'
                                    ],
                                    [
                                    'id' => '2',
                                    'name' => 'BSM (Non Aktif)'
                                    ],
                                    [

                                    'id' => '3',
                                    'name' => 'Ex Karyawan'
                                    ],
                                  

                                ];
         $qCabang                = array_merge($collection,$qCabang);
         $qDepartemen            = $qMaster->getSelectDepartemen();
         $qDepartemen            = array_merge($collection,$qDepartemen);
         $qJabatan               = $qMaster->getSelectJabatan();
         $qJabatan               = array_merge($collection,$qJabatan);
         
        # ---------------
        // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        
        ----------------------- */
      
        // $data["form_act_edit"]    =  "/karyawan/batalkeluar";
        // $data["form_act_export"]    =  "/karyawan/nonaktif_export";
        // $data["form_act_view"]    =  "/karyawan/nonaktif_view";
        
        //  $data["label_edit"]    = "Batal Keluar";
        //  $data["label_export"]  = "Export Data";
        //  $data["label_view"]    = "View";

       $data["form_act_edit"]    =  "/karyawan/jaminan_keluar";
        $data["form_act_export"]    =  "/karyawan/jaminan_export";
        //$data["form_act_view"]    =  "/karyawan/nonaktif_view";
        
        $data["label_edit"]    = "Pengambilan Jaminan";
         $data["label_export"]  = "Export Data";
        // $data["label_view"]    = "Print Review";
       
        if($request->has('id_cabang')) {
            session(["SES_SEARCH_JMN_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_JMN_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_JMN_CABANG");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_JMN_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_JMN_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JMN_DEPARTEMEN");
            $id_departemen           = $request->session()->get("SES_SEARCH_JMN_DEPARTEMEN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JMN_DEPARTEMEN");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JMN_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_JMN_JABATAN");
            $id_jabatan           = $request->session()->get("SES_SEARCH_JMN_JABATAN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_UT_JABATAN");
            $id_jabatan           = "-";

        }
         if($request->has('status_pengembalian')) {
            session(["SES_SEARCH_AMBIL" => $request->input("status_pengembalian")]);
            # ---------------
            $data["status_pengembalian"]   = $request->session()->get("status_pengembalian");
            $status_pengembalian           = $request->session()->get("status_pengembalian");

        }
        else
        {

            $data["status_pengembalian"]   = $request->session()->get("SES_SEARCH_AMBIL");
            $status_pengembalian           = "1";

        }
       

     $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemen,"value"=>$id_departemen));
      $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
       $data["fields"][]      = form_select(array("name"=>"status_pengembalian", "label"=>"Posisi Jaminan", "mandatory"=>"", "source"=> $statusjaminan,"value"=>$status_pengembalian));

      


      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));

          // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"Karyawan Aktif", "url"=>"/karyawan/index", "active"=>"")
                                       ,array("label"=>"Karyawan Non Aktif", "url"=>"/karyawan/nonaktif_index", "active"=>"")
                                        , array("label"=>"Karyawan Ulang Tahun", "url"=>"/karyawan/ulangtahun_index", "active"=>"")
                                        , array("label"=>"Jaminan", "url"=>"/karyawan/jaminan_index", "active"=>"active")
                                        );

        //$data["form_act_export"]    =  "/karyawan/jaminan_export";
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                      array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                     array("label"=>"Jenis Jaminan"
                                                ,"name"=>"nama_jaminan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                          ,"add-style"=>""),
                                     array("label"=>"No Jaminan"
                                                ,"name"=>"no_jaminan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                    array("label"=>"Nama Di Jaminan"
                                                ,"name"=>"nama_dijaminan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                                                                             

                                       );
          

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_JMN_KARYAWAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JMN_KARYAWAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JMN_KARYAWAN");
        }
        # ---------------
        $data["select"]        = $qKaryawan->getList_jaminan($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawan->getList_jaminan($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }



   public function gantinik($id) 
    {
        $data["title"]        = "Ganti NIK ";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/updatenik";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        $qKaryawan     = $qKaryawan->getProfile($id)->first();
      
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
          // dd($qKaryawan->id_pendidikan);
        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K","readonly"=>"readonly",  "mandatory"=>"", "value"=>$qKaryawan->nik));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan",  "mandatory"=>"yes","readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
        $data["fields"][]      = form_hidden(array("name"=>"id_jabatan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$qKaryawan->id_jabatan));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_masuk", "label"=>"Tanggal Masuk", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_masuk,"/")));

       
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
    
    }


    public function updatenik(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
         {
            return redirect("/karyawan/gantinik/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new KaryawanModel;
            $qUser      = new UserModel;
           
            # ---------------
            $qKaryawan->updateDatanik($request);
            $qUser->updateDatakry($request);
            
            return redirect("/karyawan/index");

        }

    }


 public function resign($id) {
        $data["title"]         = "Karyawan Keluar";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawan/updateresign";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new KaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->nama_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_keluar", "label"=>"Tanggal Keluar", "mandatory"=>"yes","value"=>displayDMY($qKaryawan->tgl_keluar,"/")));
       $data["fields"][]      = form_text(array("name"=>"alasan_keluar", "label"=>"Alasan Keluar", "mandatory"=>"yes", "value"=>$qKaryawan->alasan_keluar));
      
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

public function updateresign(Request $request)
    {
        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karyawan/resign/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKaryawan      = new KaryawanModel;
            # ---------------
            $qKaryawan->updateresignData($request);
            # ---------------
            session()->flash("success_message", "Karyawan has been resigned");
            # ---------------
            return redirect("/karyawan/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/karyawan/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan     = new KaryawanModel;

        $qKaryawan     = $qKaryawan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
       /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
       $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang,"value"=>$qKaryawan->id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qKaryawan->id_departemen));
       $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qKaryawan->id_jabatan));
       $data["fields"][]      =  form_datepicker(array("name"=>"tgl_masuk", "label"=>"Tanggal Masuk", "mandatory"=>"yes","value"=> $qKaryawan->tgl_masuk));
       
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qKaryawan     = new KaryawanModel;
            # ---------------
            $qKaryawan->removeData($request);
            # ---------------
            session()->flash("success_message", "Karyawan has been removed");
        } else {
            session()->flash("error_message", "Karyawan cannot be removed");
        }
      # ---------------
        return redirect("/karyawan/index"); 
    }

    //Cetak CV

    public function cetakcv($id) 
    {

        $qMaster              = new MasterModel;
        $qKaryawanm            = new KaryawanModel;
        $qKaryawan             = $qKaryawanm->getProfile($id)->first();
        $qKeluarga             = $qKaryawanm->getDetailKeluarga($id);
        $qKontak               = $qKaryawanm->getDetailKontak($id);
        $qPendidikan           = $qKaryawanm->getDetailPendidikan($id);
        $qPengalaman           = $qKaryawanm->getDetailPengalaman($id);
      /*
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getSelectStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = getSelectPendidikan();
       
       */ 
        $Line                  = 185;
        Fpdf::AddPage();
            
            
            $logo = public_path("logo/logo-bsm.png");
            Fpdf::Image($logo, 25, 18, 30, 22);
            Fpdf::SetLineWidth(0.3);
            Fpdf::SetFont("Arial", "B", 14);
            Fpdf::Cell(185, 40, "PT. BUANA SEJAHTERA MULTI DANA", 0, 0, 'C');
            Fpdf::Ln(5);
            Fpdf::SetLineWidth(0.3);
            Fpdf::SetFont("Arial", "B", 12);
            Fpdf::Cell(185, 50, 'DATA PERSONAL KARYAWAN', 0, 0, 'C');
            Fpdf::Ln(40);
            Fpdf::SetFont("Arial", "", 10);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'N I K', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nik, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Nama Karyawan', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_karyawan, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Cabang', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_cabang, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Departemen', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_departemen, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Jabatan', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->nama_jabatan, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Tgl. Masuk', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, displayDMY($qKaryawan->tgl_masuk,"/"), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Status Kerja', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->status_kerja, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'No KTP', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
           Fpdf::Cell(65, 5, setString($qKaryawan->no_ktp), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Jenis Kelamin', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->jenis_kelamin, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Tempat Lahir', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->tempat_lahir, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Tgl. Lahir', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65,5, displayDMY($qKaryawan->tgl_lahir,"/"), 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Agama', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->agama, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Pendidikan', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65,5, $qKaryawan->id_pendidikan, 0, 0, 'L');
            Fpdf::Ln(5);
           Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Status', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65,5, $qKaryawan->status_nikah, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'Alamat', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            if (strlen($qKaryawan->alamat) > 0) {
                    
                    Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)), 0, 75), 0, 0, 'L');
                    Fpdf::Ln(5);
                }
                if (strlen($qKaryawan->alamat) > 75) {
                    
                    Fpdf::Cell(65, 5, '', 0, 0, 'R');
                    Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)), 75, 150), 0, 0, 'L');
                    Fpdf::Ln(5);
                }
                if (strlen($qKaryawan->alamat) > 150) {
                  
                    Fpdf::Cell(65, 5, '', 0, 0, 'R');
                    Fpdf::Cell(75, 5, substr(ucwords(strtolower($qKaryawan->alamat)),150, 225), 0, 0, 'L');
                    Fpdf::Ln(5);
                }
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'No Telpon', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->no_telpon, 0, 0, 'L');
            Fpdf::Ln(5);
            Fpdf::Cell(25, 5, '', 0, 0, 'L');
            Fpdf::Cell(35, 5, 'No. HP', 0, 0, 'L');
            Fpdf::Cell(5, 5, ':', 0, 0, 'R');
            Fpdf::Cell(65, 5, $qKaryawan->hp, 0, 0, 'L');
                    
            Fpdf::Ln(10);
            Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA KELUARGA', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(10, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(50, 5, 'NAMA', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'HUBUNGAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TEMPAT', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TANGGAL', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'JENIS', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'PENDIDIKAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'PEKERJAAN', 0, 0, 'C');
            Fpdf::Ln(5);
            Fpdf::Cell(10, 5, '', 0, 0, 'C');
            Fpdf::Cell(50, 5, 'KARYAWAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, '', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'LAHIR', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'LAHIR', 0, 0, 'C');
            Fpdf::Cell(20, 5, '', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'KELAMIN', 0, 0, 'C');
            Fpdf::Cell(20, 5, '', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qKeluarga ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qKeluarga  as $row) {

                  Fpdf::Cell(10, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(50, 5, $row->nama_keluarga, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->hubungan, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tempat_lahir, 0, 0, 'L');
                  Fpdf::Cell(20, 5, displayDMY($row->tgl_lahir,"/"), 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->jenis_kelamin, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->pendidikan, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->pekerjaan, 0, 0, 'L');
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

                      # ----------------
            Fpdf::Ln(10);

           
           Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA KONTAK LAIN', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(10, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(40, 5, 'NAMA', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'HUBUNGAN', 0, 0, 'C');
            Fpdf::Cell(90, 5, 'ALAMAT', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'NO TELPON', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qKontak ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qKontak  as $row) {

                  Fpdf::Cell(10, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(40, 5, $row->nama_kontak, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->hubungan, 0, 0, 'L');
                  Fpdf::Cell(90, 5, $row->alamat, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->telpon, 0, 0, 'L');
                  
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

                      # ----------------
            Fpdf::Ln(10);

            Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA RIWAYAT PENDIDIKAN', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(20, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'JENJANG SEKOLAH', 0, 0, 'C');
            Fpdf::Cell(90, 5, 'NAMA SEKOLAH', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'KOTA', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AWAL', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AKHIR', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qPendidikan ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qPendidikan  as $row) {

                  Fpdf::Cell(20, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(20, 5, $row->id_pendidikan, 0, 0, 'L');
                  Fpdf::Cell(90, 5, $row->nama_sekolah, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->kota_sekolah, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tahun_awal, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tahun_akhir, 0, 0, 'L');
                  
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

          Fpdf::Ln(10);

            Fpdf::SetFont("Arial", "B", 10);
            
            Fpdf::Cell(185, 5, 'DATA PENGALAMAN KERJA', 0, 0, 'C');
            Fpdf::Ln(10);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(2);
            Fpdf::SetFont("Arial", "B", 8);
            Fpdf::Cell(20, 5, 'NO.', 0, 0, 'C');
            Fpdf::Cell(50, 5, 'PERUSAHAAN', 0, 0, 'C');
            Fpdf::Cell(30, 5, 'KOTA', 0, 0, 'C');
            Fpdf::Cell(40, 5, 'JABATAN', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AWAL', 0, 0, 'C');
            Fpdf::Cell(20, 5, 'TAHUN AKHIR', 0, 0, 'C');
            Fpdf::Ln(7);
            Fpdf::Cell(185, 0.1, '', 1, 0, 'C');
            Fpdf::Ln(3);
            $No=1;
            if (count($qPengalaman ) == 0) {
                Fpdf::Cell(185, 5, 'Data Kosong', 0, 0, 'C');
                Fpdf::Ln(5);
            } else {
              Fpdf::SetFont("Arial", "", 8);
                foreach ($qPengalaman  as $row) {

                  Fpdf::Cell(20, 5, $No, 0, 0, 'C');
                  Fpdf::Cell(50, 5, $row->perusahaan, 0, 0, 'L');
                  Fpdf::Cell(30, 5, $row->kota, 0, 0, 'L');
                  Fpdf::Cell(40, 5, $row->jabatan, 0, 0, 'L');
                  Fpdf::Cell(20, 5, $row->tahun_awal, 0, 0, 'C');
                  Fpdf::Cell(20, 5, $row->tahun_akhir, 0, 0, 'C');
                  
                  Fpdf::Ln(4);
                  
                
                    $No++;
                               
                

            }
          }

//ID  Pendidikan  Nama Sekolah  Kota  Tahun Awal  Tahun Akhir

            Fpdf::Output();
       

    }

     public function export(Request $request)
    {   




       

         
          $query  = DB::table("p_karyawan as a")
                            ->select("a.*",db::Raw("DATE_ADD(tgl_masuk,INTERVAL 3 MONTH) as Tanggal_percobaan"),"b.nama_cabang","b.kode_cabang","c.nama_departemen","d.nama_jabatan","e.nama_pendidikan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("m_pendidikan as e","e.id_pendidikan","=","a.id_pendidikan")
                            

                            ->where("a.aktif",1)
                            // ->where("a.id_cabang",$idcabang)
                            // ->where("a.id_departemen","=",$iddepartemen)
                            // ->where("a.id_jabatan","=",$idjabatan)
                            //  ->whereBetween("a.tgl_masuk", [$awal , $akhir]) 
                            ->orderBy("a.id_cabang","ASC")
                            ->orderBy("a.id_departemen","ASC")
                            ->orderBy("a.id_jabatan","ASC");


       // //  if($request->session()->get("SES_SEARCH_AWAL")) {
       // //      # ---------------
       // //      $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
       // //  } else {
       // //      $TglAwal            = displayDMY("2005-01-01",'/');
       // //  }

       // // if($request->session()->get("SES_SEARCH_AKHIR")) {
       // //      # ---------------
       // //      $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
       // //  } else {
       // //      $TglAkhir = date("d/m/Y");
       // //  }
       

       // $idcabang="";
       // $iddepartemen="";
       // $idjabatan="";
       // if(session()->get("SES_SEARCH_CABANG") != '-'){
       //          $idcabang= $request->session()->get("SES_SEARCH_CABANG");
       // }
            
       //  if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
       //          $iddepartemen=$request->session()->get("SES_SEARCH_DEPARTEMEN");
       //  }
            
       //  if(session()->get("SES_SEARCH_JABATAN") != '-'){
       //          $idjabatan=$request->session()->get("SES_SEARCH_JABATAN");
       //   }
        $awal="";
        $akhir="";
        if(session()->has("SES_SEARCH_AWAL")) {
             $awal = session()->get("SES_SEARCH_AWAL");
             $awal = setYMD($awal,"/");
             
        }
        if(session()->has("SES_SEARCH_AKHIR")) {
             $akhir = session()->get("SES_SEARCH_AKHIR");
             $akhir = setYMD($akhir,"/");   
           
            $query->whereBetween("a.tgl_masuk", [$awal, $akhir]);

            
        }                    

        if(session()->has("SES_SEARCH_CABANG")) {
            if(session()->get("SES_SEARCH_CABANG") != '-'){
                $query->where("a.id_cabang", session()->get("SES_SEARCH_CABANG"));
            }
            
        }
        if(session()->has("SES_SEARCH_DEPARTEMEN")) {
          
            if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $query->where("a.id_departemen", session()->get("SES_SEARCH_DEPARTEMEN"));
            }
            
        }
        if(session()->has("SES_SEARCH_JABATAN")) {
            if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $query->where("a.id_jabatan", session()->get("SES_SEARCH_JABATAN"));
            }
            
        }
      


                           
            

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR KARYAWAN AKTIF');
        $sheet->setCellValue('A2', 'PERIODE :' .$awal ." S/D " .$akhir);
        
        $spreadsheet->getActiveSheet()->getStyle('A3:W3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
        
        $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'CABANG');
        $sheet->setCellValue('C3', 'N I K');
        $sheet->setCellValue('D3', 'NAMA');
        $sheet->setCellValue('E3', 'JABATAN');
        $sheet->setCellValue('F3', 'DEPARTEMEN');
        $sheet->setCellValue('G3', 'STATUS KEPEGAWAIAN');
        $sheet->setCellValue('H3', 'TANGGAL MASUK');
        $sheet->setCellValue('I3', '3 BULAN');
        $sheet->setCellValue('J3', 'STATUS KARYAWAN');
        //$sheet->setCellValue('K3', 'STATUS PAJAK');
        $sheet->setCellValue('K3', 'AGAMA');
        $sheet->setCellValue('L3', 'PENDIDIKAN');
        $sheet->setCellValue('M3', 'JENIS KELAMIN');
        $sheet->setCellValue('N3', 'GOLONGAN DARAH');
        $sheet->setCellValue('O3', 'TEMPAT LAHIR');
        $sheet->setCellValue('P3', 'TANGGAL LAHIR');
        $sheet->setCellValue('Q3', 'ALAMAT KTP');
        $sheet->setCellValue('R3', 'ALAMAT TINGGAL');
        $sheet->setCellValue('S3', 'NO HP');
        $sheet->setCellValue('T3','TELP RUMAH');
        $sheet->setCellValue('U3', 'NO KTP');
        $sheet->setCellValue('V3', "TGL BERLAKU KTP");
        $sheet->setCellValue('W3', 'NO BPJS TK');
       

        $query=$query->get();
        foreach($query as $row) 
        {
      
                  $qKaryawan            = new KaryawanModel;
                  $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                  $pendidikan           = $row->id_pendidikan;
                  $stn ="TK/0";
		  $stn2 ="TK/0";
                  if($row->status_nikah=="KAWIN")
                  {
                      $stn="K/" .$row->jml_anak;
    			$stn2="K/" .$row->jml_anak;
                  
                  }
                  else
                  {

                      $stn="TK/" .$row->jml_anak;
		     $stn2="TK/" .$row->jml_anak;
		      if($row->status_nikah=="CERAI")
			{
				$stn2="HB/" .$row->jml_anak;

			}
	

                  }

                  
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,substr($row->kode_cabang, '-2') . " " .$row->nama_cabang);
                  $sheet->setCellValue('C'.$baris,$row->nik);
                  $sheet->setCellValue('D'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('E'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('F'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('G'.$baris,$row->status_kerja);
                 // $sheet->setCellValue('H'.$baris,displayDMY($row->tgl_masuk,"/"));
                   $sheet->setCellValue('H'.$baris,'=DATEVALUE("' .$row->tgl_masuk .'")');
                  //$sheet->setCellValue('H'.$baris,$row->tgl_masuk);
                 
                  $sheet->setCellValue('I'.$baris,'=DATEVALUE("' .$row->Tanggal_percobaan.'")');
                  $sheet->setCellValue('J'.$baris,$row->status_nikah);
                  //$sheet->setCellValue('K'.$baris,$row->jml_anak);
                 // $sheet->setCellValue('K'.$baris,$stn);
                  
                  $sheet->setCellValue('K'.$baris,$row->agama);
                  $sheet->setCellValue('L'.$baris,$row->nama_pendidikan);
                  $sheet->setCellValue('M'.$baris,$row->jenis_kelamin);
                  $sheet->setCellValue('N'.$baris,$row->golongan_darah);
                  $sheet->setCellValue('O'.$baris,$row->tempat_lahir);
                  $sheet->setCellValue('P'.$baris,'=DATEVALUE("' .$row->tgl_lahir.'")');
                  $sheet->setCellValue('Q'.$baris,$row->alamat_valid);
                  $sheet->setCellValue('R'.$baris,$row->alamat );
                  $sheet->setCellValue('S'.$baris,$row->hp);
                  $sheet->setCellValue('T'.$baris,$row->telp_rumah);
                  $sheet->setCellValue('U'.$baris,'="'.$row->no_ktp.'"');
                  $sheet->setCellValue('V'.$baris,$row->tgl_berlaku_ktp);
                  $sheet->setCellValue('W'.$baris,'="'.$row->nobjstk.'"');
                 

       
                 $baris=$baris+1;
        }
        // $spreadsheet->getActiveSheet()->getStyle('X4:X'.$baris)->getNumberFormat()->setFormatCode('#,##0');
         $spreadsheet->getActiveSheet()->getStyle('U4:U'.$baris)->getNumberFormat()->setFormatCode('0');
         $spreadsheet->getActiveSheet()->getStyle('W4:W'.$baris)->getNumberFormat()->setFormatCode('0');
         $spreadsheet->getActiveSheet()->getStyle('H4:H'.$baris)->getNumberFormat()->setFormatCode('DD-MM-YYYY');
         //->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DMYSLASH);

          $spreadsheet->getActiveSheet()->getStyle('I4:I'.$baris)->getNumberFormat()->setFormatCode('DD-MM-YYYY');
         //->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

          $spreadsheet->getActiveSheet()->getStyle('P4:P'.$baris)->getNumberFormat()->setFormatCode('DD-MM-YYYY');
         //->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_XLSX15);




        foreach(range('B','W') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
       // $spreadsheet->getActiveSheet()->getStyle('X')
//->getNumberFormat();

        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "data karyawanlengkap";  
        //$judul    = "KryKeluarExport";  
        $name_file = $judul.'.xlsx';
      	header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $name_file .'"'); 
        header('Cache-Control: max-age=0');
        
        $writer->save('php://output');

   

    }

    public function exportresign()
    {   
        $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->where("a.aktif",0)
                            ->orderBy("a.id_cabang","ASC")
                            ->orderBy("a.id_departemen","ASC")->get();

        

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR KARYAWAN TIDAK AKTIF');
        $spreadsheet->getActiveSheet()->getStyle('A3:Z3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
        
        $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'N I K');
        $sheet->setCellValue('C3', 'NAMA');
        $sheet->setCellValue('D3', 'JABATAN');
        $sheet->setCellValue('E3', 'DEPARTEMEN');
        $sheet->setCellValue('F3', 'CABANG');
        $sheet->setCellValue('G3', 'STATUS KEPEGAWAIAN');
        $sheet->setCellValue('H3', 'TANGGAL MASUK');
        $sheet->setCellValue('I3', 'STATUS KARYAWAN');
        $sheet->setCellValue('J3', 'AGAMA');
        $sheet->setCellValue('K3', 'PENDIDIKAN');
        $sheet->setCellValue('L3', 'JENIS KELAMIN');
        $sheet->setCellValue('M3', 'TEMPAT LAHIR');
        $sheet->setCellValue('N3', 'TANGGAL LAHIR');
        $sheet->setCellValue('O3', 'ALAMAT KTP');
        $sheet->setCellValue('P3', 'ALAMAT TINGGAL');
        $sheet->setCellValue('Q3', 'NO HP');
        $sheet->setCellValue('R3','TELP RUMAH');
        $sheet->setCellValue('S3', 'NO REKENING');
        $sheet->setCellValue('T3', 'A/N REKENING');
        $sheet->setCellValue('U3', "EMAIL");
        $sheet->setCellValue('P3', 'NO KTP');
        $sheet->setCellValue('W3', "STATUS PAJAK");
        $sheet->setCellValue('X3', 'NPWP');
        $sheet->setCellValue('Y3', "NO BPJS KESEHATAN");
        $sheet->setCellValue('Z3', 'NO BPJS TK');
        $sheet->setCellValue('AA3','NO ABSEN');

        
        foreach($query as $row) 
        {
      
                  $qKaryawan            = new KaryawanModel;
                  $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                  $pendidikan           = $row->id_pendidikan;
                  if ($pendidikan=="UND")
                  {
                    $pendidikan="UNDER SMA";
                     
                  }
                  else if ($pendidikan=="DIP")
                  {
                    $pendidikan="DIPLOMA";
                     
                  }
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nik);
                  $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('G'.$baris,$row->status_kerja);
                  $sheet->setCellValue('H'.$baris,displayDMY($row->tgl_masuk,"/"));
                  $sheet->setCellValue('I'.$baris,$row->status_nikah);
                  $sheet->setCellValue('J'.$baris,$row->agama);
                  $sheet->setCellValue('K'.$baris,$pendidikan);
                  $sheet->setCellValue('L'.$baris,$row->jenis_kelamin);
                  $sheet->setCellValue('M'.$baris,$row->tempat_lahir);
                  $sheet->setCellValue('N'.$baris,displayDMY($row->tgl_lahir,"/"));
                  $sheet->setCellValue('O'.$baris,$row->alamat_valid);
                  $sheet->setCellValue('P'.$baris,$row->alamat );
                  $sheet->setCellValue('Q'.$baris,$row->hp);
                  $sheet->setCellValue('R'.$baris,$row->telp_rumah);
                  $sheet->setCellValue('S'.$baris,$row->no_rekening);
                  $sheet->setCellValue('T'.$baris,$row->anrekening);
                  $sheet->setCellValue('U'.$baris,$row->email);
                  $sheet->setCellValue('V'.$baris,'="'.$row->no_ktp.'"');
                  $sheet->setCellValue('W'.$baris,$statuspajak);
                  $sheet->setCellValue('X'.$baris,$row->nonpwp);
                  $sheet->setCellValue('Y'.$baris,$row->nobpjskes);
                  $sheet->setCellValue('Z'.$baris,$row->nobjstk);
                  $sheet->setCellValue('AA'.$baris,$row->noabsen);


       
                 $baris=$baris+1;
        }

        foreach(range('B','Z') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
         $spreadsheet->getActiveSheet()->getStyle('V4:V'.$baris)->getNumberFormat()->setFormatCode('0');
          $spreadsheet->getActiveSheet()->getStyle('W4:W'.$baris)->getNumberFormat()->setFormatCode('0');
         $spreadsheet->getActiveSheet()->getColumnDimension('AA')
                  ->setAutoSize(true);

        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "data karyawanl tidak aktif lengkap";  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   

    }

    public function import() {
        $data["title"]        = "Import";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/importsave";
        /* ----------
         Updatesisacuti
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_upload(array("name"=>"file_excel", "label"=>"File Excel", "mandatory"=>"yes"));
      
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }
  
    public function importsave(Request $request)
      { 
          $rules = array(
                      'file_excel' => 'required|'                 
          );
  
          $messages = [
                      'file_excel' => 'Pilih file',
  
          ];
  
          $validator = Validator::make($request->all(), $rules, $messages);
  
          if ($validator->fails()) {
                return redirect("/Karyawan/import")
                ->withErrors($validator)
                ->withInput();
          } else {
            $qKaryawan  = new KaryawanModel;
            $qUser      = new UserModel;
            
            $qKaryawan->importcsv($request);
            # ---------------
            session()->flash("success_message", "update has been updated");
            # ---------------
            return redirect("/Karyawan/index");
          }
      }
  
      public function getDatesFromRange($start, $end){
        $dates = array($start);
        while(end($dates) < $end){
            $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
        }
        return $dates;
    }


     public function nonaktif_index(Request $request, $page=null)
    {
         $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawan/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawan              = new KaryawanModel;
        $qMaster                = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];

         $qCabang                = array_merge($collection,$qCabang);
         $qDepartemen            = $qMaster->getSelectDepartemen();
         $qDepartemen            = array_merge($collection,$qDepartemen);
         $qJabatan               = $qMaster->getSelectJabatan();
         $qJabatan               = array_merge($collection,$qJabatan);
         $qStskerja              = getSelectStskerja();
         $qStskerja              = array_merge($collection,$qStskerja);;
         
         
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        
        ----------------------- */
       // $data["form_act_edit"]    = "/karyawan/edit";
       // $data["label_edit"]    = "Perubahan Data";

        if($request->has('tgl_awal')) {
            session(["SES_SEARCH_AWAL" => $request->input("tgl_awal")]);
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = displayDMY("2000-01-01",'/');
        }

       if($request->has('tgl_akhir')) {
            session(["SES_SEARCH_AKHIR" => $request->input("tgl_akhir")]);
            # ---------------
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir = date("d/m/Y");
        }
        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_CABANG");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = $request->session()->get("SES_SEARCH_DEPARTEMEN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = $request->session()->get("SES_SEARCH_JABATAN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = "-";

        }
        if($request->has('status_kerja')) {
            session(["SES_SEARCH_STATUSKERJA" => $request->input("status_kerja")]);
            # ---------------
            $data["status_kerja"]   = $request->session()->get("SES_SEARCH_STATUSKERJA");
            $status_kerja           = $request->session()->get("SES_SEARCH_STATUSKERJA");

        }
        else
        {

            $data["nama_karir"]   = $request->session()->get("SES_SEARCH_STATUSKERJA");
            $status_kerja           = "-";

        }


        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Masuk (Awal)", "value"=>$TglAwal));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Masuk (Akhir)", "value"=>$TglAkhir));       
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemen,"value"=>$id_departemen));
      $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
        $data["fields"][]      = form_select(array("name"=>"status_kerja", "label"=>"Status Kerja", "mandatory"=>"", "source"=> $qStskerja,"value"=>$status_kerja));
        

      


      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));

          $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"Karyawan Aktif", "url"=>"/karyawan/index", "active"=>"")
                                       ,array("label"=>"Karyawan Non Aktif", "url"=>"/karyawan/nonaktif_index", "active"=>"active")
                                        , array("label"=>"Karyawan Ulang Tahun", "url"=>"/karyawan/ulangtahun_index", "active"=>"")
                                        , array("label"=>"Jaminan", "url"=>"/karyawan/jaminan_index", "active"=>""));

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                      array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                     array("label"=>"Tgl Masuk"
                                                ,"name"=>"Tanggal_masuk"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                       array("label"=>"Tgl Keluar"
                                                ,"name"=>"Tanggal_keluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                     array("label"=>"Alasan Keluar"
                                                ,"name"=>"nama_alasankeluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""));
          

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KARYAWAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWAN");
        }
        # ---------------
        $data["select"]        = $qKaryawan->getList_nonaktif($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawan->getList_nonaktif($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }
 public function ulangtahun_index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawan/ulangtahun_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawan              = new KaryawanModel;
        $qMaster                = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
         $qCabang                = array_merge($collection,$qCabang);
         $qDepartemen            = $qMaster->getSelectDepartemen();
         $qDepartemen            = array_merge($collection,$qDepartemen);
         $qJabatan               = $qMaster->getSelectJabatan();
         $qJabatan               = array_merge($collection,$qJabatan);
         
        # ---------------
        // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        
        ----------------------- */
      
        // $data["form_act_edit"]    =  "/karyawan/batalkeluar";
        // $data["form_act_export"]    =  "/karyawan/nonaktif_export";
        // $data["form_act_view"]    =  "/karyawan/nonaktif_view";
        
        //  $data["label_edit"]    = "Batal Keluar";
        //  $data["label_export"]  = "Export Data";
        //  $data["label_view"]    = "View";


       
        if($request->has('id_cabang')) {
            session(["SES_SEARCH_UT_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_UT_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_UT_CABANG");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_UT_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_UT_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_UT_DEPARTEMEN");
            $id_departemen           = $request->session()->get("SES_SEARCH_UT_DEPARTEMEN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_UT_DEPARTEMEN");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_UT_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_UT_JABATAN");
            $id_jabatan           = $request->session()->get("SES_SEARCH_UT_JABATAN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_UT_JABATAN");
            $id_jabatan           = "-";

        }
       

     $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemen,"value"=>$id_departemen));
      $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
        

      


      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));

          // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"Karyawan Aktif", "url"=>"/karyawan/index", "active"=>"")
                                       ,array("label"=>"Karyawan Non Aktif", "url"=>"/karyawan/nonaktif_index", "active"=>"")
                                        , array("label"=>"Karyawan Ulang Tahun", "url"=>"/karyawan/ulangtahun_index", "active"=>"active")
                                          , array("label"=>"Jaminan", "url"=>"/karyawan/jaminan_index", "active"=>""));

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                      array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                     array("label"=>"Tgl Lahir"
                                                ,"name"=>"Tanggal_lahir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>"")
                                       );
          

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_UT_KARYAWAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_UT_KARYAWAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_UT_KARYAWAN");
        }
        # ---------------
        $data["select"]        = $qKaryawan->getList_ulangtahun($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawan->getList_ulangtahun($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }






     public function nonaktif_export(Request $request)
    {
        if($request->session()->get("SES_SEARCH_AWAL")) {
            # ---------------
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $TglAwal            = displayDMY("2005-01-01",'/');
        }

       if($request->session()->get("SES_SEARCH_AKHIR")) {
            # ---------------
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $TglAkhir = date("d/m/Y");
        }

       $idcabang="";
       $iddepartemen="";
       $idjabatan="";
       if(session()->get("SES_SEARCH_CABANG") != '-'){
                $idcabang= $request->session()->get("SES_SEARCH_CABANG");
       }
            
        if(session()->get("SES_SEARCH_DEPARTEMEN") != '-'){
                $iddepartemen=$request->session()->get("SES_SEARCH_DEPARTEMEN");
        }
            
        if(session()->get("SES_SEARCH_JABATAN") != '-'){
                $idjabatan=$request->session()->get("SES_SEARCH_JABATAN");
         }
         
          $awal =setYMD($TglAwal,"/");
          $akhir=setYMD($TglAkhir,"/");   
          $query  = DB::table("p_karyawan as a")
                            ->select("a.*","b.nama_cabang","b.kode_cabang","c.nama_departemen","d.nama_jabatan","e.nama_alasankeluar","f.nama_pendidikan","g.keterangan_keluar")
                            ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                            ->leftjoin("pa_alasankeluar as e","e.id_alasankeluar","=","a.alasan_keluar")
                             ->leftjoin("m_pendidikan as f","f.id_pendidikan","=","a.id_pendidikan")
                             ->leftjoin("p_karyawankeluar as g","g.id_karyawan","=","a.id_karyawan")
                            
                            ->where("a.aktif", 0)
                            ->whereBetween("a.tgl_keluar", [$awal, $akhir])
                            // ->where("a.id_cabang", $idcabang)
                            // ->where("a.id_departemen", $iddepartemen)
                            // ->where("a.id_jabatan", $idjabatan)
                            ->orderBy("a.id_karyawan", "DESC");

                            

                            
        

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR KARYAWAN NON AKTIF');
        $sheet->setCellValue('A2', 'PERIODE TANGGAL : ' .$TglAwal ." SAMPAI " .$TglAkhir);
        $spreadsheet->getActiveSheet()->getStyle('A3:Z3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/

     
        
       $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'CABANG');
        $sheet->setCellValue('C3', 'N I K');
        $sheet->setCellValue('D3', 'NAMA');
        $sheet->setCellValue('E3', 'JABATAN');
        $sheet->setCellValue('F3', 'DEPARTEMEN');
        $sheet->setCellValue('G3', 'STATUS KEPEGAWAIAN');
        $sheet->setCellValue('H3', 'TANGGAL MASUK');
        $sheet->setCellValue('I3', 'TANGGAL KELUAR');
        $sheet->setCellValue('J3', 'NO BPJS TK');
        $sheet->setCellValue('K3', 'STATUS KARYAWAN');
        $sheet->setCellValue('L3', 'JML ANAK');
        $sheet->setCellValue('M3', 'AGAMA');
        $sheet->setCellValue('N3', 'PENDIDIKAN');
        $sheet->setCellValue('O3', 'JENIS KELAMIN');
        $sheet->setCellValue('P3', 'GOLONGAN DARAH');
        $sheet->setCellValue('Q3', 'TEMPAT LAHIR');
        $sheet->setCellValue('R3', 'TANGGAL LAHIR');
        $sheet->setCellValue('S3', 'ALAMAT KTP');
        $sheet->setCellValue('T3', 'ALAMAT TINGGAL');
        $sheet->setCellValue('U3', 'NO HP');
        $sheet->setCellValue('V3','TELP RUMAH');
        $sheet->setCellValue('W3', 'NO KTP');
        $sheet->setCellValue('X3', "TGL BERLAKU KTP");
        $sheet->setCellValue('Y3', "ALASAN KELUAR");
        $sheet->setCellValue('Z3', "KETERANGAN KELUAR");


       

        
        foreach($query->get() as $row) 
        {
      
                  
                  $qKaryawan            = new KaryawanModel;
                  $statuspajak          = $qKaryawan->getStatuspajak($row->id_ptkp);
                  $pendidikan           = $row->id_pendidikan;
                  
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,substr($row->kode_cabang, -2) . " " .$row->nama_cabang);
                  $sheet->setCellValue('C'.$baris,$row->nik);
                  $sheet->setCellValue('D'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('E'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('F'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('G'.$baris,$row->status_kerja);
                  $sheet->setCellValue('H'.$baris,displayDMY($row->tgl_masuk,"/"));
                  $sheet->setCellValue('I'.$baris,displayDMY($row->tgl_keluar,"/"));
                  $sheet->setCellValue('J'.$baris,$row->nobjstk);
                  $sheet->setCellValue('K'.$baris,$row->status_nikah);
                  $sheet->setCellValue('L'.$baris,$row->jml_anak);
                  $sheet->setCellValue('M'.$baris,$row->agama);
                  $sheet->setCellValue('N'.$baris,$row->nama_pendidikan);
                  $sheet->setCellValue('O'.$baris,$row->jenis_kelamin);
                  $sheet->setCellValue('P'.$baris,$row->golongan_darah);
                  $sheet->setCellValue('Q'.$baris,$row->tempat_lahir);
                  $sheet->setCellValue('R'.$baris,displayDMY($row->tgl_lahir,"/"));
                  $sheet->setCellValue('S'.$baris,$row->alamat_valid);
                  $sheet->setCellValue('T'.$baris,$row->alamat );
                  $sheet->setCellValue('U'.$baris,$row->hp);
                  $sheet->setCellValue('V'.$baris,$row->telp_rumah);
                  $sheet->setCellValue('W'.$baris,'="'.$row->no_ktp.'"');
                  $sheet->setCellValue('X'.$baris,displayDMY($row->tgl_berlaku_ktp,"/"));
                  $sheet->setCellValue('X'.$baris,displayDMY($row->tgl_berlaku_ktp,"/"));
                  $sheet->setCellValue('Y'.$baris,$row->nama_alasankeluar);
                  $sheet->setCellValue('Z'.$baris,$row->keterangan_keluar);


        
       
                 $baris=$baris+1;
        }

        foreach(range('B','Z') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
         $spreadsheet->getActiveSheet()->getStyle('V4:V'.$baris)->getNumberFormat()->setFormatCode('0');
          $spreadsheet->getActiveSheet()->getStyle('W4:W'.$baris)->getNumberFormat()->setFormatCode('0');
        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "KryKeluarExport";  
        $name_file = $judul.'.xlsx';
      	header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $name_file .'"'); 
        header('Cache-Control: max-age=0');
        
        $writer->save('php://output');

   


    }
    public function jaminan_keluar($id) 
    {
        $data["title"]        = "Edit Karyawan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/update_jaminan";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        $qKaryawan     = $qKaryawan->getProfile($id)->first();
      
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
          $qJabatan               = $qMaster->getSelectJabatan();
      
         $qJaminan                = $qMaster->getSelectJaminan();
           

      
         // dd($qKaryawan->id_pendidikan);
        /* ----------
         Fields
        ----------------------- */
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K","readonly"=>"readonly",  "mandatory"=>"yes", "value"=>$qKaryawan->nik));
       $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Karyawan",  "mandatory"=>"yes", "value"=>$qKaryawan->nama_karyawan));
             
        $data["fields"][]      = form_select(array("name"=>"id_jaminan", "label"=>"Jenis Jaminan", "mandatory"=>"", "withnull"=>"withnull","source"=> $qJaminan,"value"=>$qKaryawan->id_jaminan));
        $data["fields"][]      = form_text(array("name"=>"no_jaminan", "label"=>"No Jaminan", "mandatory"=>"","value"=>$qKaryawan->no_jaminan));
         $data["fields"][]      = form_text(array("name"=>"nama_jaminan", "label"=>"Atas Nama Jaminan", "mandatory"=>"","value"=>$qKaryawan->nama_dijaminan));
         $data["fields"][]      =  form_datepicker(array("name"=>"tgl_pengembalian", "label"=>"Tanggal Penembalian", "mandatory"=>"yes","value"=> displayDMY($qKaryawan->tgl_pengembalian,"/")));
      




       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
    
    }
 public function update_jaminan(Request $request)
    {

        $rules = array(
                    'nama_karyawan' => 'required|'       
        );

        $messages = [
                    'nama_karyawan.required' => 'Karyawan harus diisi'

        ];

         $messages = ['CabangPerusahaan.required' => 'CabangPerusahaan harus diisi',
                      'Alamat.required' => 'Alamat harus diisi',
                      'Telp.required' => 'Telp harus diisi',
                      'PIC.required'=>'PIC harus diisi',    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
         {
            return redirect("/karyawan/jaminan_keluar/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {

            $qKaryawan      = new KaryawanModel;
            $qUser      = new UserModel;
           
            # ---------------
            $qKaryawan->updateData_jaminan($request);
            //$qUser->updateDatakry($request);
            
           return redirect("/karyawan/jaminan_index");

        }

    }
   
    public function printview_percobaan($id){
        $data["title"]        = "Cetak Percobaan Karyawan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/karyawan/cetak_percobaan";
        /* ----------
         Karyawan
        ----------------------- */
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        $qKaryawan     = $qKaryawan->getProfileResign($id)->first();
        if($qKaryawan->status_kerja =="PERCOBAAN" || $qKaryawan->status_kerja =="PROBITION" ){
       $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$id));
       $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"put"));
       $data["fields"][]      = form_currency(array("name"=>"gaji_pokok", "label"=>"GAJI POKOK", "value"=>0));
       $data["fields"][]      = form_currency(array("name"=>"t_uang_makan", "label"=>"Tunjangan Uang Makan", "value"=>0));
       $data["fields"][]      = form_currency(array("name"=>"take_home_pay", "label"=>"Take Home Pay", "value"=>0));
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cetak"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        return view("default.form", $data);
        }else{
            return redirect("/karyawan/index")
                ->withErrors(['Status Bukan Percobaan '])
                ->withInput();
        }
    }

    public function cetak_percobaan(Request $request){
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        
        $qKaryawan     = DB::table('p_karyawan as a')
                        ->select('a.*','b.nama_jabatan','c.nama_departemen')
                        ->leftjoin('m_jabatan as b','b.id_jabatan','a.id_jabatan')
                        ->leftjoin('m_departemen as c','c.id_departemen','a.id_departemen')
                        ->where([
                                ['a.id_karyawan',$request->id_karyawan],
                                ])
                        ->first();
        
        
        /*'001/SMP-APF-00/HRD/I/2019'*/
        $no_registrasi = $qKaryawan->no_registrasi;
        $status_kerja = $qKaryawan->status_kerja;
        
        $request->gaji_pokok = empty($request->gaji_pokok) ? null : setNoComma($request->gaji_pokok);
        $request->t_uang_makan = empty($request->t_uang_makan) ? null : setNoComma($request->t_uang_makan);
        $request->take_home_pay = empty($request->take_home_pay) ? null : setNoComma($request->take_home_pay);
                        
        if($no_registrasi == null){
            if($status_kerja == 'PERCOBAAN'){
                $profile_system = DB::table('m_profilesystem')->first();
                $no_registrasi = $profile_system->no_registrasi;
                $format_registrasi = $profile_system->format_registrasi;
                $tgl_masuk = $qKaryawan->tgl_masuk; 
                $bulanke   = date('m',strtotime($tgl_masuk)); 
                $tahun   = date('Y',strtotime($tgl_masuk)); 
                $convertinttoroman = integerToRoman($bulanke);
                $kodecabang = $qKaryawan->id_cabang;  
                $kodecabang = substr($kodecabang,-2);
                
                $newnumber = $no_registrasi+1;
                $no_registrasi = $newnumber.$format_registrasi.$kodecabang.'/HRD/'.$convertinttoroman.'/'.$tahun; 
                DB::table('m_profilesystem')->where('id_profilesystem',$profile_system->id_profilesystem)->update(['no_registrasi'=>$newnumber]);
                DB::table('p_karyawan')->where('id_karyawan',$qKaryawan->id_karyawan)->update(['no_registrasi'=>$no_registrasi]);
            }
        }
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getSelectStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qGolongandarah         = getSelectGoldarah();
        $qPtkp                 = $qMaster->getSelectPtkp();
         // dd($qKaryawan->id_pendidikan);

        $newIdk                 = $request->id_karyawan; 

        $karakterid             = "0000000000000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
        $Line                  = 185;

        $jabatanmanagerpusat = DB::table('p_karyawan as a')
                    ->select('a.nama_karyawan','b.nama_jabatan')
                    ->leftjoin('m_jabatan as b','b.id_jabatan','a.id_jabatan')
                    ->where([
                            ['a.id_cabang',1],
                            ['a.id_departemen',6],
                            ['a.id_jabatan',31],
                            ['a.aktif',1],
                            ])
                    ->first();

        Fpdf::AddPage('P','A4');
        Fpdf::SetMargins(25, 20, 20);
        // Fpdf::SetAutoPageBreak(true, 30);
        // 
        # ----------------
        Fpdf::SetFont("Arial", "B", 12);
        // Fpdf::write(12,'Hello World! Hello World! Hello World! Hello World!');
        Fpdf::Cell(195, 7, "SURAT MASA PERCOBAAN 3 BULAN KARYAWAN BARU", 0, 1, 'C');
        // Fpdf::Ln(7);
        Fpdf::SetFont("Arial", "I", 11);
        Fpdf::Cell(170, 7, "No :".$no_registrasi, 0, 1, 'C');
        Fpdf::Ln();
        // Fpdf::SetFont("Arial", "", 10);
        // Fpdf::Multicell(0, 5, "Pada ", 0,'L');
        // Fpdf::Ln(10);
        Fpdf::SetFont("Arial", "", 11);
        Fpdf::Cell(5,7,'1.',0,0,'L');
        Fpdf::Cell(50,7,'Nama',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(102,7,$jabatanmanagerpusat->nama_karyawan,0,'J');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Jabatan',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(112,7,$jabatanmanagerpusat->nama_jabatan,0,'J');
        // Fpdf::Ln();
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Multicell(169, 7, "Dalam hal ini mewakili PT. ARTHA PRIMA FINANCE, untuk selanjutnya di sebut PERUSAHAAN",0, 'J');
        // Fpdf::Ln();
        Fpdf::SetFont("Arial", "B", 11);
        Fpdf::Cell(5,7,'2.',0,0,'L');
        Fpdf::Cell(50,7,'DATA PRIBADI',0,0,'L');
        Fpdf::Ln();
        Fpdf::SetFont("Arial", "", 11);
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Nama Lengkap',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(115,7,$qKaryawan->nama_karyawan,0,'L');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Tempat/Tanggal Lahir',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(115,7,$qKaryawan->tempat_lahir.'/'.date('d-m-Y', strtotime($qKaryawan->tgl_lahir)),0,'L');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'No. Identita (KTP)',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(115,7,$qKaryawan->no_ktp,0,'L');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Alamat Tempat Tinggal',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::Multicell(115,7,setString($qKaryawan->alamat),0,'J');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'No. Telpon',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(115,7,$qKaryawan->no_telpon,0,'L');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Jabatan/Divisi',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(115,7,$qKaryawan->nama_jabatan.' / '.$qKaryawan->nama_departemen,0,'L');
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Mulai Bekerja',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        Fpdf::MultiCell(115,7, date('d-m-Y', strtotime($qKaryawan->tgl_masuk)),0,'L');
        // Fpdf::Ln();

        Fpdf::Cell(5,7,'3.',0,0,'L');
        Fpdf::SetFont("Arial", "B", 11);
        Fpdf::Cell(50,7,'RINCIAN GAJI',0,0,'L');
        Fpdf::Ln();
        Fpdf::SetFont("Arial", "", 11);
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'GAJI POKOK',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        if($request->gaji_pokok == null){
            Fpdf::Cell(7,7,'Rp. ',0,0,'L');
            Fpdf::SetFont("Arial", "U", 11);
            Fpdf::Multicell(108,7,'              ,- (terbilang)',0,'J');
            // Fpdf::Cell(12,7,'Rp. ',0,0,'R');
        }else{
            Fpdf::Multicell(115,7,'Rp. '.setTitik($request->gaji_pokok).',-'.' ('.penyebut($request->gaji_pokok).' )',0,'J');
        }
        Fpdf::SetFont("Arial", "", 11);
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Tunjangan Uang Makan',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        if($request->t_uang_makan == null){
            Fpdf::Cell(7,7,'Rp. ',0,0,'L');
            Fpdf::SetFont("Arial", "U", 11);
            Fpdf::Multicell(108,7,'              ,- /hari',0,'J');
        }else{
            Fpdf::Multicell(115,7,'Rp. '.setTitik($request->t_uang_makan).',- /hari',0,'J');
        }
        Fpdf::SetFont("Arial", "B", 10);
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(50,7,'Take Home Pay',0,0,'L');
        Fpdf::Cell(3,7,': ',0,0,'L');
        if($request->take_home_pay == null){
            Fpdf::Cell(7,7,'Rp. ',0,0,'L');
            Fpdf::SetFont("Arial", "U", 11);
            Fpdf::Multicell(108,7,'              ,- /nett/bulan',0,'J');
        }else{
            Fpdf::Multicell(115,7,'Rp. '.setTitik($request->take_home_pay).',- /nett/bulan',0,'J');
        }
        Fpdf::SetFont("Arial", "B", 11);
        Fpdf::Cell(5,7,'4.',0,0,'L');
        Fpdf::Cell(50,7,'KETENTUAN MASA PERCOBAAN :',0,0,'L');
        Fpdf::Ln();

        Fpdf::SetFont("Arial", "", 11);
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(5,7,'1.',0,0,'L');
        Fpdf::Multicell(160, 7, "Masa percobaan berlaku selama 3 (tiga) bulan, apabila sebelum masa percobaan berakhir dan karyawan dianggap tidak mampu, perusahaan berhak memutuskan hubungan kerja, karyawan tidak dapat menuntut kompensasi dalam bentuk apa pun.",0, 'J');
        // Fpdf::Ln();
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(5,7,'2.',0,0,'L');
        Fpdf::Multicell(160, 7, "Jam kerja Senin hingga Jum'at mulai Pukul  08.30 sampai dengan Pukul 16.30 WIB, dan hari Sabtu mulai Pukul 08.30 sampai dengan Pukul 12.30 WIB. Jam istirahat makan siang adalah dari Pukul 12.00 sampai dengan Pukul 13.00.",0, 'J');
        // Fpdf::Ln();
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(5,7,'3.',0,0,'L');
        Fpdf::Multicell(160, 7, "Bersedia menaati dan melaksanakan ketentuan perusahaan dan bersedia melaksanakan setiap tugas yang diberikan dengan penuh tanggung jawab.",0, 'J');
        // Fpdf::Ln();
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(5,7,'4.',0,0,'L');
        Fpdf::Multicell(160, 7, "Bersedia untuk ditempatkan pada seluruh kantor cabang dan dipindahkan atau dimutasikan, yang disesuaikan dengan kebutuhan kantor cabang yang ditempatkan.",0, 'J');
        // Fpdf::Ln();
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(5,7,'5.',0,0,'L');
        Fpdf::Multicell(160, 7, "Setelah masa percobaan 3 (tiga) bulan akan dievaluasi untuk ditentukan layak atau tidak dilanjutkan ke jenjang berikutnya yakni perpanjangan kontrak.",0, 'J');
        // Fpdf::Ln();
        Fpdf::Cell(5,7,'',0,0,'L');
        Fpdf::Cell(5,7,'6.',0,0,'L');
        Fpdf::Multicell(160, 5, "Surat ini berlaku sejak tanggal dibuat sampai dengan 3 (tiga) bulan kedepan.",0, 'J');
        Fpdf::Ln();


        //ttd
        Fpdf::Cell(15,7,'Jakarta,',0,0,'L');
        Fpdf::SetFont("Arial", "U", 11);
        Fpdf::Cell(25,7,'                            ',0,0,'L');
        Fpdf::Ln(20);



        Fpdf::SetFont("Arial", "U", 11);
        Fpdf::Cell(50,7,'( '.$jabatanmanagerpusat->nama_karyawan.' )',0,0,'C');
        Fpdf::Cell(70,7,'',0,0,'C');
        Fpdf::Cell(50,5,'( '.$qKaryawan->nama_karyawan.' )',0,0,'C');
        Fpdf::Ln();
        Fpdf::SetFont("Arial", "", 11);
        Fpdf::Cell(50,7,$jabatanmanagerpusat->nama_jabatan,0,0,'C');
        Fpdf::Cell(70,7,'',0,0,'C');
        Fpdf::Cell(50,5,'KARYAWAN',0,0,'C');
        Fpdf::Output();
        
        exit;
            
    }

public function nonaktif_cetak($id){
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        //$qMKarir             = new KarirModel;
        /* ----------
         Source
        ----------------------- */
        //$idKarir             = explode("&", $id); 
        $qKarir                = DB::table('pa_karir as a')
                                ->select("a.id_karir","a.nama_karir","status_proses","a.id_karyawan","a.no_surat","a.tgl_karir",db::Raw("DATE_FORMAT(a.tgl_karir, '%d/%m/%Y') as Tanggal_karir")
                                ,"b.nama_cabang","c.nama_departemen","d.nama_jabatan"
                                ,"e.nama_cabang as nama_cabangawal","f.nama_departemen as nama_departemenawal","g.nama_jabatan as nama_jabatanawal"
                                )
                                ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangbaru")
                                ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenbaru")
                                ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanbaru")
                                ->leftjoin("m_cabang as e","e.id_cabang","=","a.id_cabangawal")
                                ->leftjoin("m_departemen as f","f.id_departemen","=","a.id_departemenawal")
                                ->leftjoin("m_jabatan as g","g.id_jabatan","=","a.id_jabatanawal")
                                ->where('id_karyawan',$id)
                                ->orderBy('a.tgl_karir','DESC')
                                ->get();
        $qKaryawan     = $qKaryawan->getProfileResign($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getSelectStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qGolongandarah         = getSelectGoldarah();
        $qPtkp                 = $qMaster->getSelectPtkp();

        $newIdk                 = $id; 

        $karakterid             = "0000000000000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
      
      if($qKaryawan->status_nikah=="KAWIN")
      {
          $qKaryawan->status_nikah="K/" .$qKaryawan->jml_anak;
          // $stn2="K/" .$row->jml_anak;
      }
      else if($qKaryawan->status_nikah=="TIDAK KAWIN")
      {
          $qKaryawan->status_nikah="TK/" .$qKaryawan->jml_anak;
      
      }
      else if($qKaryawan->status_nikah=="CERAI")
      {
          $qKaryawan->status_nikah="HB/" .$qKaryawan->jml_anak;

      }
    
        $path = public_path()."/app/img/unknown.png";
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $data = collect($qKaryawan);
        $data["id_temp"]        = $newIdk;
        $data["path"]           = $base64;
        $data["history"]        = $qKarir;
        $data["tgl_lahir"] = displayDMY($data["tgl_lahir"],"/");
        $data["tgl_melamar"] = displayDMY($data["tgl_melamar"],"/");
        $data["tgl_masuk"]      = displayDMY($data["tgl_masuk"],"/");
	$data["tgl_keluar"]      = displayDMY($data["tgl_keluar"],"/");
        $hubungan = $data["nama_ibu"] == null?'':'ORANG TUA';
        $data["hubungan"] = $hubungan;
        $data["perusahaan"] = "PT. ARTHA PRIMA FINANCE";
        
        $pdf = PDF::loadView('laporan.datanonaktifkaryawan',$data);
	return $pdf->stream('data karyawan.pdf',array('Attachment'=>0));
        //return $pdf->download('data karyawan.pdf'); 
            
    }

    public function cetak_all($id){
        $qMaster              = new MasterModel;
        $qKaryawan            = new KaryawanModel;
        //$qMKarir             = new KarirModel;
        /* ----------
         Source
        ----------------------- */
        //$idKarir             = explode("&", $id); 
        $qKarir                = DB::table('pa_karir as a')
                                ->select("a.id_karir","a.nama_karir","status_proses","a.id_karyawan","a.no_surat","a.tgl_karir",db::Raw("DATE_FORMAT(a.tgl_karir, '%d/%m/%Y') as Tanggal_karir")
                                ,"b.nama_cabang","c.nama_departemen","d.nama_jabatan"
                                ,"e.nama_cabang as nama_cabangawal","f.nama_departemen as nama_departemenawal","g.nama_jabatan as nama_jabatanawal"
                                )
                                ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabangbaru")
                                ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemenbaru")
                                ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatanbaru")
                                ->leftjoin("m_cabang as e","e.id_cabang","=","a.id_cabangawal")
                                ->leftjoin("m_departemen as f","f.id_departemen","=","a.id_departemenawal")
                                ->leftjoin("m_jabatan as g","g.id_jabatan","=","a.id_jabatanawal")
                                ->where('id_karyawan',$id)
                                ->orderBy('a.tgl_karir','DESC')
                                ->get();
        $qKaryawan     = $qKaryawan->getProfileResign($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qCabang                = $qMaster->getSelectCabang();
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qJabatan               = $qMaster->getSelectJabatan();
        $qStskerja              = getSelectStskerja();
        $qAgama                 = getSelectAgama();
        $qStatusNikah           = getSelectStatusNikah();
        $qJK                    = getSelectJenisKelamin();
        $qPendidikan            = $qMaster->getSelectPendidikan();
        $qGolongandarah         = getSelectGoldarah();
        $qPtkp                 = $qMaster->getSelectPtkp();

        $newIdk                 = $id; 

        $karakterid             = "0000000000000000000";
        $countkarakterid        = strlen($karakterid);
        $countnewIdk            = strlen($newIdk);
        $selisih                = $countkarakterid-$countnewIdk;
        $cut_text               = substr($karakterid, 0, $selisih);
        $newIdk                 = setString($cut_text.$newIdk);
      
      if($qKaryawan->status_nikah=="KAWIN")
      {
          $qKaryawan->status_nikah="K/" .$qKaryawan->jml_anak;
          // $stn2="K/" .$row->jml_anak;
      }
      else if($qKaryawan->status_nikah=="TIDAK KAWIN")
      {
          $qKaryawan->status_nikah="TK/" .$qKaryawan->jml_anak;
      
      }
      else if($qKaryawan->status_nikah=="CERAI")
      {
          $qKaryawan->status_nikah="HB/" .$qKaryawan->jml_anak;

      }
    
        $path = public_path()."/app/img/unknown.png";
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $data = collect($qKaryawan);
        $data["id_temp"]        = $newIdk;
        $data["path"]           = $base64;
        $data["history"]        = $qKarir;
        $data["tgl_lahir"] = displayDMY($data["tgl_lahir"],"/");
        $data["tgl_melamar"] = displayDMY($data["tgl_melamar"],"/");
        $data["tgl_masuk"]      = displayDMY($data["tgl_masuk"],"/");
        $hubungan = $data["nama_ibu"] == null?'':'ORANG TUA';
        $data["hubungan"] = $hubungan;
        $data["perusahaan"] = "PT. ARTHA PRIMA FINANCE";
        
        $pdf = PDF::loadView('laporan.datakaryawan',$data);
        return $pdf->stream('data karyawan.pdf',array('Attachment'=>0));
	//return $pdf->download('data non aktif karyawan.pdf');      
    }
   
}
