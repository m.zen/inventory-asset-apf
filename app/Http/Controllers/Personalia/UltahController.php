<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Personalia\UltahControllernModel;
use App\Model\Master\UserModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;


class UltahController extends Controller
{
    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }


    public function index(Request $request, $page=null)
    {
       $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/karyawan/nonaktif_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKaryawan              = new UltahModel;
        $qMaster                = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
         $qCabang                = array_merge($collection,$qCabang);
         $qDepartemen            = $qMaster->getSelectDepartemen();
         $qDepartemen            = array_merge($collection,$qDepartemen);
         $qJabatan               = $qMaster->getSelectJabatan();
         $qJabatan               = array_merge($collection,$qJabatan);
         
        # ---------------
        // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        
        ----------------------- */
      
        $data["form_act_edit"]    =  "/karyawan/batalkeluar";
        $data["form_act_export"]    =  "/karyawan/nonaktif_export";
        $data["form_act_view"]    =  "/karyawan/nonaktif_view";
        
         $data["label_edit"]    = "Batal Keluar";
         $data["label_export"]  = "Export Data";
         $data["label_view"]    = "View";


        if($request->has('tgl_awal')) {
            session(["SES_SEARCH_AWAL" => $request->input("tgl_awal")]);
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = displayDMY("2000-01-01",'/');
        }

       if($request->has('tgl_akhir')) {
            session(["SES_SEARCH_AKHIR" => $request->input("tgl_akhir")]);
            # ---------------
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir = date("d/m/Y");
        }
        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_CABANG");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_DEPARTEMEN" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = $request->session()->get("SES_SEARCH_DEPARTEMEN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JABATAN" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = $request->session()->get("SES_SEARCH_JABATAN");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JABATAN");
            $id_jabatan           = "-";

        }
       


        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Masuk (Awal)", "value"=>$TglAwal));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Masuk (Akhir)", "value"=>$TglAkhir));       
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
       $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemen,"value"=>$id_departemen));
      $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
        

      


      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));

          // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

        $data["tabs"]          = array(array("label"=>"Karyawan Aktif", "url"=>"/karyawan/index", "active"=>"")
                                       ,array("label"=>"Karyawan Non Aktif", "url"=>"/karyawan/nonaktif_index", "active"=>"active")
                                        );

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                      array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                                ,"name"=>"nama_departemen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                                ,"name"=>"nama_jabatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                     array("label"=>"Tgl Lahir"
                                                ,"name"=>"Tanggal_lahir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>"")
                                       );
          

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_KARYAWANNONAKTIF" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWANNONAKTIF");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_KARYAWANNONAKTIF");
        }
        # ---------------
        $data["select"]        = $qKaryawan->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKaryawan->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }

}
