<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Master\UserModel;
use App\Model\Personalia\RpendidikanModel;
class RpendidikanController extends Controller
{
     public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index($idkry,Request $request, $page=null)
    {
        
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/rpendidikan/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */

        


        $qMenu                  = new MenuModel;
        $qRpendidikan           = new RpendidikanModel;
        $qMaster                = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qKaryawan             = $qMaster->getKaryawan($idkry)->first();
        $data["form_act_add"]    = "/rpendidikan/add/".$idkry;
         $data["label_add"]       = "Add";
         $data["form_act_edit"]   = "/rpendidikan/edit";
         $data["label_edit"]       = "Edit";
         $data["form_act_delete"] = "/rpendidikan/delete";
         $data["label_delete"]    = "Delete";

        //Akses user
        $qUser                  = new UserModel;
        $qhakAkses              = $qUser->getAksesuser(Auth::user()->id)->first();
        $data["hakakses"]       = $qhakAkses;
        
        # ---------------*/
        
            
          $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
          $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"N I K", "readonly"=>"readonly", "value"=>$qKaryawan->nik));
          $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "value"=>$qKaryawan->nama_karyawan));


        // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);


        $data["tabs"]          = array(array("label"=>"Biodata", "url"=>"/detail/index/".$idkry, "active"=>""),
                                 array("label"=>"Keluarga", "url"=>"/keluarga/index/".$idkry, "active"=>""),
                                 array("label"=>"Kontak Lain", "url"=>"/kontak/index/".$idkry, "active"=>""), 
                                 array("label"=>"Pendidikan", "url"=>"/rpendidikan/index/".$idkry, "active"=>"active"),
                                 array("label"=>"Pengalaman", "url"=>"/pengalaman/index/".$idkry, "active"=>""),
                                
                           );
        /* ----------
         Table header
        ----------------------- */

        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_rpendidikan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
       							array("label"=>"Pendidikan"
                                                ,"name"=>"id_pendidikan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),	
                                
                                    array("label"=>"Nama Sekolah"
                                                ,"name"=>"nama_sekolah"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Kota"
                                                ,"name"=>"kota_sekolah"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      
                                      array("label"=>"Tahun Awal"
                                                ,"name"=>"tahun_awal"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      array("label"=>"Tahun Akhir"
                                                ,"name"=>"tahun_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                      ); 

        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_RPENDIDIKAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_RPENDIDIKAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_RPENDIDIKAN");
        }
        # ---------------
        $data["select"]        = $qRpendidikan->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage,$idkry);
        $data["query"]         = $qRpendidikan->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.dtlkaryawan", $data);
    }

    public function add($idkry) {
        //dd($request);
        $data["title"]         = "Add Rpendidikan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/rpendidikan/save";
        
        $qMaster               = new MasterModel;
        
        $qGroups               = $qMaster->getSelectGroup();
        // $qStatus             = getSelectStatusRpendidikan();
        $qPendidikan           = getSelectPendidikan();    
        
        
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$idkry));
        $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "source"=> $qPendidikan));        
        $data["fields"][]      = form_text(array("name"=>"nama_sekolah", "label"=>"Nama Sekolah", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"kota_sekolah", "label"=>"Kota", "mandatory"=>"yes"));
       
        $data["fields"][]      = form_number(array("name"=>"tahun_awal", "label"=>"Tahun Awal",  "mandatory"=>"yes"));
        $data["fields"][]      = form_number(array("name"=>"tahun_akhir", "label"=>"Tahun Akhir",  "mandatory"=>"yes"));
          # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }



    public function save(Request $request) {
       

        $rules = array(
                      
                      'id_pendidikan' => 'required'                     );

        $messages = ['id_pendidikan.required' => 'Pendidikan harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/rpendidikan/add/". $request->id_karyawan)
                ->withErrors($validator)
                ->withInput();
        } else {
            $qRpendidikan  = new RpendidikanModel;
            # ---------------
            $qRpendidikan->createData($request);
            # ---------------
            session()->flash("success_message", "Rpendidikan has been saved");
            # ---------------
            return redirect("/rpendidikan/index/". $request->id_karyawan);
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Rpendidikan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/rpendidikan/update";
        /* ----------
         Rpendidikan
        ----------------------- */
        $qMaster           = new MasterModel;
        $qRpendidikan             = new RpendidikanModel;
        /* ----------
         Source
        ----------------------- */
        //$idrpendidikan             = explode("&", $id); 
        $qRpendidikan              = $qRpendidikan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        // $qStatus             = getSelectStatusRpendidikan();
        $qPendidikan           = getSelectPendidikan();  
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Rpendidikan", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qRpendidikan->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
       
        $data["fields"][]      = form_select(array("name"=>"id_pendidikan", "label"=>"Pendidikan", "mandatory"=>"yes", "source"=> $qPendidikan,"value"=>$qRpendidikan->id_pendidikan));        
        $data["fields"][]      = form_text(array("name"=>"nama_sekolah", "label"=>"Nama Sekolah", "mandatory"=>"yes", "value"=>$qRpendidikan->nama_sekolah));
        $data["fields"][]      = form_text(array("name"=>"kota_sekolah", "label"=>"Kota", "mandatory"=>"yes", "value"=>$qRpendidikan->kota_sekolah));
       
        $data["fields"][]      = form_number(array("name"=>"tahun_awal", "label"=>"Tahun Awal",  "mandatory"=>"yes","value"=>$qRpendidikan->tahun_awal));
        $data["fields"][]      = form_number(array("name"=>"tahun_akhir", "label"=>"Tahun Akhir",  "mandatory"=>"yes","value"=>$qRpendidikan->tahun_akhir));
        
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'id_pendidikan' => 'required|'                
        );

        $messages = [
                    'id_pendidikan.required' => 'Nama Pendidikan harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/rpendidikan/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qRpendidikan      = new RpendidikanModel;
            # ---------------
            $qRpendidikan->updateData($request);
            # ---------------
            session()->flash("success_message", "Rpendidikan has been updated");
            # ---------------
            return redirect("/rpendidikan/index/". $request->input("id_karyawan"));
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/rpendidikan/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qRpendidikan             = new RpendidikanModel;

       //$idrpendidikan             = explode("&", $id); 
        $qRpendidikan              = $qRpendidikan->getProfile($id)->first();
        $qGroups                = $qMaster->getSelectGroup();
        $qHubungan             = getSelectHubungan();
        $qJK                   = getSelectKelamin();
       // $qStatus             = getSelectStatusRpendidikan();
        $qPendidikan           = getSelectPendidikan();  
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Rpendidikan", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_text(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "readonly"=>"readonly", "value"=>$qRpendidikan->id_karyawan));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
      
        $data["fields"][]      = form_text(array("name"=>"id_pendidikan", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qRpendidikan->id_pendidikan));
         $data["fields"][]      = form_text(array("name"=>"nama_sekolah", "label"=>"Nama", "mandatory"=>"yes", "value"=>$qRpendidikan->nama_sekolah));
       
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qRpendidikan     = new RpendidikanModel;
            # ---------------
            $qRpendidikan->removeData($request);
            # ---------------
            session()->flash("success_message", "Rpendidikan has been removed");
        } else {
            session()->flash("error_message", "Rpendidikan cannot be removed");
        }
      # ---------------
        return redirect("/rpendidikan/index/". $request->input("id_karyawan")); 
    }
}
