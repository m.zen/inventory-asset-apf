<?php

namespace App\Http\Controllers\Personalia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Personalia\SuratperingatanModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PDF;

class SuratperingatanController extends Controller
{
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/suratperingatan/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qPeringatan            = new SuratperingatanModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */

        $data["tabs"]          = array(array("label"=>"Pengajuan Surat Peringatan", "url"=>"/suratperingatan/index", "active"=>"active")
                                       ,array("label"=>"Inquery Surat Peringatan", "url"=>"/suratperingatan/inqsp_index", "active"=>"")
                                        );

         $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_peringatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"No Surat"
                                                ,"name"=>"no_surat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),

                                    array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Tgl SP"
                                                ,"name"=>"tgl_sp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
      

                                     array("label"=>"Tingkat SP"
                                                ,"name"=>"tingkat_sp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Kategori SP"
                                                ,"name"=>"kategori_sp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),


                                      array("label"=>"Tgl Berlaku"
                                                ,"name"=>"Tanggal_berlaku"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       
                                       
                                         array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                        

                                     ); 

       
         
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_PROSESPERINGATAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PROSESPERINGATAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PROSESPERINGATAN");
        }
        # ---------------
        $data["select"]        = $qPeringatan->getList_proses($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qPeringatan->getList_proses($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
       // return view("default.list", $data);
         return view("default.tablist", $data);
    }

    public function add() {
        //dd($request);
        $data["title"]         = "Surat Peringatan Baru";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/suratperingatan/save";
        
        $qMaster               = new MasterModel;
         $qPeringatanmdl        = new SuratperingatanModel;
         $collection             = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => '-Pilih-'
                                    ]
                                ];
        


        $qSP                   = getSelectPeringatan();
        $qSP                   = array_merge($collection,$qSP);
        $qKatSP                = getSelectKategoriperingatan();
        $qKatSP                   = array_merge($collection,$qKatSP);
        
        $qListKry              = $qMaster-> getSelectKaryawan();
        $qListKry             = array_merge($collection,$qListKry);
                 
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes"));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_sp", "label"=>"Tanggal SP", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
      
         $data["fields"][]      = form_select(array("name"=>"tingkat_sp", "label"=>"Tingkat SP", "mandatory"=>"yes", "source"=>$qSP));  
          $data["fields"][]      = form_select(array("name"=>"kategori_sp", "label"=>"Kategori Pelanggaran", "mandatory"=>"yes", "source"=>$qKatSP));  
         
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
         $data["fields"][]      = form_select(array("name"=>"id_approve", "label"=>"Tanda Tangan",  "source"=>$qListKry));
         $data["fields"][]      = form_text(array("name"=>"jabatan_disurat", "label"=>"Jabatan", "mandatory"=>"yes","value"=>""));
         
         $data["fields"][]      = form_text(array("name"=>"mengingat1", "label"=>"Mengingat 1", "mandatory"=>"yes","value"=>""));
         $data["fields"][]      = form_text(array("name"=>"mengingat2", "label"=>"Mengingat 2", "mandatory"=>"","value"=>""));
         $data["fields"][]      = form_text(array("name"=>"mengingat3", "label"=>"Mengingat 3", "mandatory"=>"","value"=>""));
         $data["fields"][]      = form_text(array("name"=>"mengingat4", "label"=>"Mengingat 4", "mandatory"=>"","value"=>""));
         
          $data["fields"][]      = form_text(array("name"=>"menimbang1", "label"=>"Menimbang 1", "mandatory"=>"yes","value"=>""));

        $data["fields"][]      = form_text(array("name"=>"menimbang2", "label"=>"Menimbang 2", "mandatory"=>"","value"=>""));
          $data["fields"][]      = form_text(array("name"=>"menimbang3", "label"=>"Menimbang 3", "mandatory"=>"","value"=>""));
         $data["fields"][]      = form_text(array("name"=>"menimbang4", "label"=>"Menimbang 4", "mandatory"=>"","value"=>""));
         
          $data["fields"][]      = form_text(array("name"=>"tembusan1", "label"=>"Tembusan 1", "mandatory"=>""));
           $data["fields"][]      = form_text(array("name"=>"tembusan2", "label"=>"Tembusan 2", "mandatory"=>""));
            $data["fields"][]      = form_text(array("name"=>"tembusan3", "label"=>"Tembusan 3", "mandatory"=>""));
             $data["fields"][]      = form_text(array("name"=>"tembusan4", "label"=>"Tembusan 4", "mandatory"=>""));
              $data["fields"][]      = form_text(array("name"=>"tembusan5", "label"=>"Tembusan 5", "mandatory"=>""));
       
        
        

     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }


    public function save(Request $request) {
       

        $rules = array(
                      
                      'no_surat' => 'required'                     );

        $messages = ['no_surat.required' => 'No Surat harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/suratperingatan/add")
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPeringatan  = new SuratperingatanModel;
            

            if($qPeringatan-> CekSP($request->no_surat) > 0)
            {

                session()->flash("error_message", "No Surat Sudah digunakan");
                return redirect("/suratperingatan/add");
            }
            else
            {
                 $qPeringatan->createData($request);
                 session()->flash("success_message", "Wilayah has been saved");
                 return redirect("/suratperingatan/index");

            }



        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Peringatan";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/suratperingatan/update";
        /* ----------
         Peringatan
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPeringatan      = new SuratperingatanModel;
        /* ----------
         Source
        ----------------------- */
        //$idPeringatan             = explode("&", $id); 
        $qPeringatan          = $qPeringatan->getProfile($id)->first();
        $qSP                  = getSelectPeringatan();
        $qListKry              = $qMaster-> getSelectKaryawan();
        $qKatSP                = getSelectKategoriperingatan();
        
   
         
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_text(array("name"=>"id", "label"=>"ID Peringatan", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry,"value"=>$qPeringatan->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qPeringatan->no_surat));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_sp", "label"=>"Tanggal SP", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_sp,"/"), "first_selected"=>"yes"));
        
        $data["fields"][]      = form_select(array("name"=>"tingkat_sp", "label"=>"Tingkat SP", "mandatory"=>"yes", "source"=>$qSP,"value"=>$qPeringatan->tingkat_sp));
        $data["fields"][]      = form_select(array("name"=>"kategori_sp", "label"=>"Kategori Pelanggaran", "mandatory"=>"yes", "source"=>$qKatSP,"value"=>$qPeringatan->kategori_sp));  
       
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_berlaku,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_akhir,"/"), "first_selected"=>"yes"));
          $data["fields"][]      = form_select(array("name"=>"id_approve", "label"=>"Tanda Tangan",  "source"=>$qListKry,"value"=>$qPeringatan->id_approve));
          $data["fields"][]      = form_text(array("name"=>"jabatan_disurat", "label"=>"Jabatan", "mandatory"=>"yes","value"=>$qPeringatan->jabatan_disurat));
         
         $data["fields"][]      = form_text(array("name"=>"mengingat1", "label"=>"Mengingat 1", "mandatory"=>"yes","value"=>$qPeringatan->mengingat1));
         $data["fields"][]      = form_text(array("name"=>"mengingat2", "label"=>"Mengingat 2", "mandatory"=>"","value"=>$qPeringatan->mengingat2));
         $data["fields"][]      = form_text(array("name"=>"mengingat3", "label"=>"Mengingat 3", "mandatory"=>"","value"=>$qPeringatan->mengingat3));
         $data["fields"][]      = form_text(array("name"=>"mengingat4", "label"=>"Mengingat 4", "mandatory"=>"","value"=>$qPeringatan->mengingat4));
         

          $data["fields"][]      = form_text(array("name"=>"menimbang1", "label"=>"Menimbang 1", "mandatory"=>"yes","value"=>$qPeringatan->menimbang1));

        $data["fields"][]      = form_text(array("name"=>"menimbang2", "label"=>"Menimbang 2", "mandatory"=>"","value"=>$qPeringatan->menimbang2));
          $data["fields"][]      = form_text(array("name"=>"menimbang3", "label"=>"Menimbang 3", "mandatory"=>"","value"=>$qPeringatan->menimbang3));
         $data["fields"][]      = form_text(array("name"=>"menimbang4", "label"=>"Menimbang 4", "mandatory"=>"","value"=>$qPeringatan->menimbang4));
         
          $data["fields"][]      = form_text(array("name"=>"tembusan1", "label"=>"Tembusan 1", "mandatory"=>"","value"=>$qPeringatan->tembusan1));
           $data["fields"][]      = form_text(array("name"=>"tembusan2", "label"=>"Tembusan 2", "mandatory"=>"","value"=>$qPeringatan->tembusan2));
            $data["fields"][]      = form_text(array("name"=>"tembusan3", "label"=>"Tembusan 3", "mandatory"=>"","value"=>$qPeringatan->tembusan3));
             $data["fields"][]      = form_text(array("name"=>"tembusan4", "label"=>"Tembusan 4", "mandatory"=>"","value"=>$qPeringatan->tembusan1,"value"=>$qPeringatan->tembusan4));
              $data["fields"][]      = form_text(array("name"=>"tembusan5", "label"=>"Tembusan 5", "mandatory"=>"","value"=>$qPeringatan->tembusan5));
       
        
     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/suratperingatan/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPeringatan      = new SuratperingatanModel;
            # ---------------
            $qPeringatan->updateData($request);
            # ---------------
            session()->flash("success_message", "Peringatan has been updated");
            # ---------------
            return redirect("/suratperingatan/index");
        }
    }

    public function delete($id) 
    {
    
        $data["title"]         = "Delete Karyawan";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/suratperingatan/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPeringatan             = new PeringatanModel;

       //$idPeringatan             = explode("&", $id); 
        $qPeringatan        = $qPeringatan->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qCabangs              = $qMaster->getSelectCabang();
        $qDepartemens          = $qMaster->getSelectDepartemen();  
		$qJabatan              = $qMaster->getSelectJabatan(); 
		$qStatuskerja          = getSelectStskerja();
        /* ----------
         Fields
        ----------------------- */
         $data["fields"][]      = form_text(array("name"=>"id_peringatan", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qPeringatan->no_surat));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID Karyawan", "mandatory"=>"yes","value"=>$qPeringatan->id_karyawan));
              
       # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);

    }

    public function remove(Request $request) {
        if($request->input("id_peringatan") != 1) {
            $qPeringatan     = new PeringatanModel;
            # ---------------
            $qPeringatan->removeData($request);
            # ---------------
            session()->flash("success_message", "Peringatan has been removed");
        } else {
            session()->flash("error_message", "Peringatan cannot be removed");
        }
      # ---------------
        return redirect("/suratperingatan/index/". $request->input("id_karyawan")); 
    }

    public function Cetak( $id)
    {
      # code...
      $qMaster           = new MasterModel;
      $qCPeringatan      = new SuratperingatanModel;
        /* ----------
         Source
                 ----------------------- */
        //$idPeringatan             = explode("&", $id); 
       // $qPeringatan          = $qPeringatan->getProfile($id)->first();
       
      /* ----------
        Source
      ----------------------- */

      
      //$idKontrakkerja     = explode("&", $id); 
      $qDisiplinSP       = $qCPeringatan->getCetaksp($id);
      $qNama             = $qCPeringatan->getCetaksp($id)->first();
      $data["pkwt"]      = $qDisiplinSP ;
      //format 1
     // $data['field']->tgl_sekarang = setString(date('d-M-Y'));
      //format 2
      //$data['field']->tgl_sekarang2 = setString(date('d/m/Y'));
        // Send data to the view using loadView function of PDF facade
      
        $pdf = PDF::loadView('Personalia.lprsrtperingatan',  $data);
        $nama_file =  $qNama->nama_karyawan;

        //$nama_file =  $qKontrakkerja->nama_karyawan & "-" & $qKontrakkerja->id_kontrakkerja;

      
       $pdf->save(storage_path().'_filename.pdf');
             
        // Finally, you can download the file using download function
      return $pdf->download($nama_file.'.pdf');
       //return view("Personalia.lprkontrak", $data);

    }
    public function approve($id) 
    {
        $data["title"]        = "Update Status Proses";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/suratperingatan/update_approve";
        /* ----------
         Peringatan
        ----------------------- */
        $qMaster           = new MasterModel;
        $qPeringatan             = new SuratperingatanModel;
        /* ----------
         Source
        ----------------------- */
      
        $qPeringatan          = $qPeringatan->getProfile($id)->first();
        $qSP                  = getSelectPeringatan();
        $qListKry              = $qMaster-> getSelectKaryawan();
        $qKatSP                = getSelectKategoriperingatan();
        $qApprove              = getListApprove();
        
     
         
        
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id", "label"=>"ID Peringatan", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan",  "source"=>$qListKry,"value"=>$qPeringatan->id_karyawan));
        $data["fields"][]      = form_text(array("name"=>"no_surat", "label"=>"No Surat", "mandatory"=>"yes","value"=>$qPeringatan->no_surat));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_sp", "label"=>"Tanggal SP", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_sp,"/"), "first_selected"=>"yes"));
        
        $data["fields"][]      = form_select(array("name"=>"", "label"=>"Tingkat SP", "mandatory"=>"yes", "source"=>$qSP,"value"=>$qPeringatan->tingkat_sp));
        $data["fields"][]      = form_select(array("name"=>"kategori_sp", "label"=>"Kategori Pelanggaran", "mandatory"=>"yes", "source"=>$qKatSP,"value"=>$qPeringatan->kategori_sp));  
         
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_berlaku", "label"=>"Tanggal Berlaku", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_berlaku,"/"), "first_selected"=>"yes"));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "mandatory"=>"yes","value"=>displayDMY($qPeringatan->tgl_akhir,"/"), "first_selected"=>"yes"));
            $data["fields"][]      = form_select(array("name"=>"id_approve", "label"=>"Tanda Tangan",  "source"=>$qListKry,"value"=>$qPeringatan->id_approve));

         $data["fields"][]      = form_text(array("name"=>"mengingat1", "label"=>"Mengingat 1", "mandatory"=>"yes","value"=>$qPeringatan->mengingat1));
         $data["fields"][]      = form_text(array("name"=>"mengingat2", "label"=>"Mengingat 2", "mandatory"=>"","value"=>$qPeringatan->mengingat2));
         $data["fields"][]      = form_text(array("name"=>"mengingat3", "label"=>"Mengingat 3", "mandatory"=>"","value"=>$qPeringatan->mengingat3));
         $data["fields"][]      = form_text(array("name"=>"mengingat4", "label"=>"Mengingat 4", "mandatory"=>"","value"=>$qPeringatan->mengingat4));
         

          $data["fields"][]      = form_text(array("name"=>"menimbang1", "label"=>"Menimbang 1", "mandatory"=>"yes","value"=>$qPeringatan->menimbang1));

        $data["fields"][]      = form_text(array("name"=>"menimbang2", "label"=>"Menimbang 2", "mandatory"=>"","value"=>$qPeringatan->menimbang2));
          $data["fields"][]      = form_text(array("name"=>"menimbang3", "label"=>"Menimbang 3", "mandatory"=>"","value"=>$qPeringatan->menimbang3));
         $data["fields"][]      = form_text(array("name"=>"menimbang4", "label"=>"Menimbang 4", "mandatory"=>"","value"=>$qPeringatan->menimbang4));
         
          $data["fields"][]      = form_text(array("name"=>"tembusan1", "label"=>"Tembusan 1", "mandatory"=>"","value"=>$qPeringatan->tembusan1));
           $data["fields"][]      = form_text(array("name"=>"tembusan2", "label"=>"Tembusan 2", "mandatory"=>"","value"=>$qPeringatan->tembusan2));
            $data["fields"][]      = form_text(array("name"=>"tembusan3", "label"=>"Tembusan 3", "mandatory"=>"","value"=>$qPeringatan->tembusan3));
             $data["fields"][]      = form_text(array("name"=>"tembusan4", "label"=>"Tembusan 4", "mandatory"=>"","value"=>$qPeringatan->tembusan1,"value"=>$qPeringatan->tembusan4));
              $data["fields"][]      = form_text(array("name"=>"tembusan5", "label"=>"Tembusan 5", "mandatory"=>"","value"=>$qPeringatan->tembusan5));
        
           $data["fields"][]      = form_select(array("name"=>"status_proses", "label"=>"Approval", "mandatory"=>"yes", "source"=> $qApprove, "value"=>""));
        
        $data["fields"][]      = form_text(array("name"=>"Komentar", "label"=>"Komentar", "value"=>""));
         
       



        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);    
            

    }

public function update_approve(Request $request)
    {
        
        
        $rules = array(
                    'no_surat' => 'required|',
                    
                    'no_surat' => 'required|'                          
        );

        $messages = [
                    'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/suratperingatan/update_approve/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qPeringatan      = new SuratperingatanModel;
            # ---------------
            $qPeringatan->update_approve($request);
            # ---------------
            session()->flash("success_message", "Peringatan has been updated");
            # ---------------
            return redirect("/suratperingatan/index");
        }
    }
      public function inqsp_index(Request $request, $page=null)
    {

        $data["title"]          = "Inquery Data Surat Peringatan";
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/suratperingatan/inqsp_index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qKarir                 = new SuratperingatanModel;
        $qKatSP                 = getSelectKategoriperingatan();


        # ---------------
        //$data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);

$data["tabs"]          = array(array("label"=>"Pengajuan Surat Peringatan", "url"=>"/suratperingatan/index", "active"=>"")
                                       ,array("label"=>"Inquery Surat Peringatan", "url"=>"/suratperingatan/inqsp_index", "active"=>"active")
                                        );


         $data["form_act_export"]    = "/suratperingatan/export";
         $data["label_export"]    = "Export Data";
        
        $qMaster               = new MasterModel;
        $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
        $qCabang             = $qMaster->getSelectCabang();
        $qCabang             = array_merge($collection,$qCabang);
        $qDepartemens        = $qMaster->getSelectDepartemen();
        $qDepartemens        = array_merge($collection,$qDepartemens);
        $qJabatan            = $qMaster->getSelectJabatan();
        $qJabatan            = array_merge($collection,$qJabatan);
        $qKatSP              = array_merge($collection,$qKatSP);
        $qSP                  = getSelectPeringatan();
        $qSP                  = array_merge($collection,$qSP);
        
        
        //$qViewKaryawan        = $qKontrakkerjamdl->getProfileKry($idkry)->first();  
       // $qViewFormatsurat     = $qKontrakkerjamdl->getProfileKry($idkry)->first(); 
        # ---------------
        
        

         if($request->has('tgl_awal')) {
            session(["SES_SEARCH_AWAL" => $request->input("tgl_awal")]);
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_AWAL");
            $TglAwal            = displayDMY("2005-01-01",'/');
        }

       if($request->has('tgl_akhir')) {
            session(["SES_SEARCH_AKHIR" => $request->input("tgl_akhir")]);
            # ---------------
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_AKHIR");
            $TglAkhir = date("d/m/Y");
        }


        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = $request->session()->get("SES_SEARCH_CABANG");
        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG");
            $id_cabang           = "-";

        }

        if($request->has('tingkat_sp')) {
            session(["SES_SEARCH_TINGKATSP" => $request->input("tingkat_sp")]);
            # ---------------
            $data["tingkat_sp"]   = $request->session()->get("SES_SEARCH_TINGKATSP");
            $tingkat_sp           = $request->session()->get("SES_SEARCH_TINGKATSP");
            
        }
        else
        {
            $data["tingkat_sp"]   = $request->session()->get("SES_SEARCH_TINGKATSP");
            $tingkat_sp           = "-";

        }

        if($request->has('kategori_sp')) {
            session(["SES_SEARCH_KATEGORISP" => $request->input("kategori_sp")]);
            # ---------------
            $data["kategori_sp'"] = $request->session()->get("SES_SEARCH_KATEGORISP");
            $kategori_sp          = $request->session()->get("SES_SEARCH_KATEGORISP");
        }
        else
        {
            $data["kategori_sp"]   = $request->session()->get("SES_SEARCH_KATEGORISP");
            $kategori_sp           = "-"; 
        }
        
        

        if($request->has('text_search')) {
            session(["SES_SEARCH_PERINGATAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERINGATAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PERINGATAN");
        }
             
        
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awa;", "value"=>$TglAwal));
        $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir", "value"=>$TglAkhir, "first_selected"=>"yes"));
        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
          $data["fields"][]      = form_select(array("name"=>"tingkat_sp", "label"=>"Tingkat SP",  "source"=>$qSP,"value"=>$tingkat_sp));
        $data["fields"][]      = form_select(array("name"=>"kategori_sp", "label"=>"Kategori Pelanggaran",  "source"=>$qKatSP,"value"=>$kategori_sp));

        
      $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));
          // $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
          
        /* ----------
         Table header
        ----------------------- */
        


         $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_peringatan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                
                                    array("label"=>"No Surat"
                                                ,"name"=>"no_surat"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                            ,"add-style"=>""),

                                    array("label"=>"Nama Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"20%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Tgl SP"
                                                ,"name"=>"tgl_sp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
      

                                     array("label"=>"Tingkat SP"
                                                ,"name"=>"tingkat_sp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                     array("label"=>"Kategori SP"
                                                ,"name"=>"kategori_sp"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),

                                      array("label"=>"Tgl Berlaku"
                                                ,"name"=>"Tanggal_berlaku"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       array("label"=>"Tgl Akhir"
                                                ,"name"=>"tgl_akhir"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                       
                                         array("label"=>"Status Proses"
                                                ,"name"=>"status_proses"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""));



       
        # ---------------
        $data["select"]        = $qKarir->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qKarir->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }
    public function export(Request $request)
    {
        if($request->session()->get("SES_SEARCH_AWAL")) {
            # ---------------
            $TglAwal            = $request->session()->get("SES_SEARCH_AWAL");
        } else {
            $TglAwal            = displayDMY("2005-01-01",'/');
        }

       if($request->session()->get("SES_SEARCH_AKHIR")) {
            # ---------------
            $TglAkhir            = $request->session()->get("SES_SEARCH_AKHIR");
        } else {
            $TglAkhir = date("d/m/Y");
        }

       $id_cabang="";
       $tingkat_sp="";
       $kategori_sp;
       if(session()->get("SES_SEARCH_CABANG") != '-'){
                $d_cabang= $request->session()->get("SES_SEARCH_CABANG");
       }
            
        if(session()->get("SES_SEARCH_TINGKATSP") != '-'){
                $tingkat_sp=$request->session()->get("SES_SEARCH_TINGKATSP");
        }
            
        if(session()->get("SES_SEARCH_KATEGORISP") != '-'){
                $kategori_sp=$request->session()->get("SES_SEARCH_KATEGORISP");
         }
          $awal =setYMD($TglAwal,"/");
          $akhir=setYMD($TglAkhir,"/");   
         $query  = DB::table("pa_peringatan as a")
                            ->select("a.*","b.nik","b.nama_karyawan","c.nama_cabang","d.nama_departemen","e.nama_jabatan")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                          
                            ->leftjoin("m_cabang as c","c.id_cabang","=","a.id_cabang")
                            ->leftjoin("m_departemen as d","d.id_departemen","=","a.id_departemen")
                            ->leftjoin("m_jabatan as e","e.id_jabatan","=","a.id_jabatan")
                           
                             ->whereBetween("a.tgl_berlaku", [$awal , $akhir])  
                             ->orderBy("a.id_peringatan","ASC")->get();
                            
        

      $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DAFTAR SURAT PERINGATAN');
        $sheet->setCellValue('A2', 'PERIODE TANGGAL : ' .$TglAwal ." SAMPAI " .$TglAkhir);
        $spreadsheet->getActiveSheet()->getStyle('A3:L3')->applyFromArray($styleArray);
        $baris = 4;
        /*-------------------------
        KOLOM DATA
        ---------------------------*/
        
        $sheet->setCellValue('A3', 'NO.');
        $sheet->setCellValue('B3', 'NAMA');
        $sheet->setCellValue('C3', 'N I K');
        $sheet->setCellValue('D3', 'JABATAN');
        $sheet->setCellValue('E3', 'DEPARTEMEN');
        $sheet->setCellValue('F3', 'CABANG');
        $sheet->setCellValue('G3', 'TANGGAL SURAT');
        $sheet->setCellValue('H3', 'JENIS SP');
        $sheet->setCellValue('I3', 'KATEGORI');
        $sheet->setCellValue('J3', 'PEJABAT');
        $sheet->setCellValue('K3', 'JABATAN');
        $sheet->setCellValue('L3', 'ALASAN');
       

        
        foreach($query as $row) 
        {
      
                  
                  $sheet->setCellValue('A'.$baris,$baris-3);
                  $sheet->setCellValue('B'.$baris,$row->nama_karyawan);
                  $sheet->setCellValue('C'.$baris,$row->nik);
                  $sheet->setCellValue('D'.$baris,$row->nama_jabatan);
                  $sheet->setCellValue('E'.$baris,$row->nama_departemen);
                  $sheet->setCellValue('F'.$baris,$row->nama_cabang);
                  $sheet->setCellValue('G'.$baris,displayDMY($row->tgl_berlaku,"/"));
                  $sheet->setCellValue('H'.$baris,$row->tingkat_sp);
                  $sheet->setCellValue('I'.$baris,$row->kategori_sp);
                  $sheet->setCellValue('J'.$baris,$row->pejabat);
                  $sheet->setCellValue('K'.$baris,$row->jabatan_disurat);
                  $sheet->setCellValue('L'.$baris,$row->keterangan ." " .$row->menimbang1 ." " .$row->menimbang2);
                  
       
                 $baris=$baris+1;
        }

        foreach(range('B','L') as $columnID)
         {
             $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
             
                  
         }
        
        
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "SPExport";  
        $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      $path = public_path().'/app/'.$name_file;
      $contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      $writer->save($path);
      $filename   = str_replace("@", "/", $path);
        # ---------------
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$name_file);
        header("Content-Type: application/xlsx");
        header("Content-Transfer-Encoding: binary");
        # ---------------
        require "$filename";
        # ---------------
        exit;

   


    }
}
