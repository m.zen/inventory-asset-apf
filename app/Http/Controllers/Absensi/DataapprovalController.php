<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DataapprovalModel;
use App\Model\Master\MasterModel;
class DataapprovalController extends Controller
{
       protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/dataapproval/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qDataapproval             = new DataapprovalModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_dataapproval"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang/Kota"
                                    ,"name"=>"nama_cabang"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"14%"
                                                ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                    ,"name"=>"nama_departemen"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"14%"
                                                ,"add-style"=>""),
                                    array("label"=>"Jabatan"
                                    ,"name"=>"nama_jabatan"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"14%"
                                                ,"add-style"=>""),
                                    array("label"=>"Cabang/Kota"
                                    ,"name"=>"nama_cabangb"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"14%"
                                                ,"add-style"=>""),
                                    array("label"=>"Departemen"
                                    ,"name"=>"nama_departemenb"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"14%"
                                                ,"add-style"=>""),
                                    array("label"=>"Disetujui oleh"
                                    ,"name"=>"approved"
                                        ,"align"=>"center"
                                        ,"item-align"=>"center"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"14%"
                                                ,"add-style"=>""),
        							array("label"=>"Level"
                                                ,"name"=>"approvallevel"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"14%"
                                                            ,"add-style"=>""),

        							);
	$collection             = [ (object)
            [
            'id' => '-',
            'name' => 'Semua'
            ]
        ];  

        $qMaster                = new MasterModel;
        $qCabang                = $qMaster->getSelectCabang();
        $qCabang                = array_merge($collection,$qCabang);
        $qDepartemen            = $qMaster->getSelectDepartemen();
        $qDepartemen            = array_merge($collection,$qDepartemen);
        $qJabatan               = $qMaster->getSelectJabatan();
        $qJabatan               = array_merge($collection,$qJabatan);     

        if($request->has('id_cabang')) {
            session(["SES_SEARCH_CABANG_USER" => $request->input("id_cabang")]);
            # ---------------
            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG_USER");
            $id_cabang           = $request->session()->get("SES_SEARCH_CABANG_USER");

        }
        else
        {

            $data["id_cabang"]   = $request->session()->get("SES_SEARCH_CABANG_USER");
            $id_cabang           = "-";

        }

        if($request->has('id_departemen')) {
            session(["SES_SEARCH_DEPARTEMEN_USER" => $request->input("id_departemen")]);
            # ---------------
            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN_USER");
            $id_departemen           = $request->session()->get("SES_SEARCH_DEPARTEMEN_USER");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_DEPARTEMEN_USER");
            $id_departemen           = "-";

        }
         if($request->has('id_jabatan')) {
            session(["SES_SEARCH_JABATAN_USER" => $request->input("id_jabatan")]);
            # ---------------
            $data["id_jabatan"]   = $request->session()->get("SES_SEARCH_JABATAN_USER");
            $id_jabatan           = $request->session()->get("SES_SEARCH_JABATAN_USER");

        }
        else
        {

            $data["id_departemen"]   = $request->session()->get("SES_SEARCH_JABATAN_USER");
            $id_jabatan           = "-";

        }

        $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"", "source"=>$qCabang,"value"=>$id_cabang));
        $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Departemen", "mandatory"=>"", "source"=>$qDepartemen,"value"=>$id_departemen));
        $data["fields"][]      = form_select(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"", "source"=>$qJabatan,"value"=>$id_jabatan));
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Cari Data"));
        $data["tabs"]          = array(array("label"=>ucwords(strtolower($this->PROT_ModuleName)) , "url"=>"/karyawan/index", "active"=>"active"));
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_DATAAPPROVAL" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAAPPROVAL");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAAPPROVAL");
        }
        # ---------------
        $data["select"]        = $qDataapproval->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDataapproval->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.inqkontraklist", $data);
    }

    public function add() {
        $data["title"]         = "Add Dataapproval";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/dataapproval/save";
        $data["url_select"]    = "/calonkaryawan/get_profile/";
        /* ----------
         Dataapproval
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qApproveds               = $qMaster->getSelectApprovaller();
        $qJabatan                 = $qMaster->getSelectJabatan();
        $qcabang                  = $qMaster->getJenisCabang();
        //hilangkan duplikasi
        foreach($qcabang as $key => $value){
            $cabang[$value->id] = $value;
        }
        $qcabang = $cabang;
        $qDepartemen                = $qMaster->getJenisDepartemenPusat();
       // $qStatus			   = getSelectStatusDataapproval();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
       $data["fields"][]     = form_select(array("name"=>"id_cabang", "label"=>"Pusat/Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qcabang));
       $data["fields"][]     = form_select2(array("name"=>"id_departemen", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen));
       $data["fields"][]     = form_select2(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan));
       $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"Approvaller"));
       $data["fields"][]     = form_select(array("name"=>"id_cabang_approvaller", "label"=>"Pusat/Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qcabang));
       $data["fields"][]     = form_select2(array("name"=>"id_departemen_approvaller", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen));
       $data["fields"][]     = form_select2(array("name"=>"id_approvaller", "label"=>"Disetujui oleh", "mandatory"=>"yes", "source"=>$qApproveds));
       $data["fields"][]     = form_text(array("name"=>"approvallevel", "label"=>"Level Approval", "mandatory"=>"yes"));
                # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.formselect", $data);
    }

    public function save(Request $request) {
        if($request->id_cabang == null){
            return redirect("/dataapproval/add")
                ->withErrors(['Pilih Cabang'])
                ->withInput();
        }else if($request->id_departemen == null){
            return redirect("/dataapproval/add")
                ->withErrors(['Pilih Departemen'])
                ->withInput();
        }else if($request->id_jabatan == null){
            return redirect("/dataapproval/add")
                ->withErrors(['Pilih Jabatan'])
                ->withInput();
        }else if($request->id_cabang_approvaller == null){
            return redirect("/dataapproval/add")
                ->withErrors(['Pilih Cabang Approvaller'])
                ->withInput();
        }else if($request->id_departemen_approvaller == null){
            return redirect("/dataapproval/add")
                ->withErrors(['Pilih Departemen Approvaller'])
                ->withInput();
        }else if($request->id_approvaller == null){
            return redirect("/dataapproval/add")
                ->withErrors(['Pilih Jabatan Approvaller'])
                ->withInput();
        }

        $rules = array(
                      
                      'id_jabatan' => 'required'                     );

        $messages = ['id_jabatan.required' => 'Dataapproval harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/dataapproval/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qDataapproval  = new DataapprovalModel;
            # ---------------
            
            $qDataapproval->createData($request);
            # ---------------
            session()->flash("success_message", "Dataapproval has been saved");
            # ---------------
            return redirect("/dataapproval/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Dataapproval";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/dataapproval/update";
        /* ----------
         Dataapproval
        ----------------------- */
        $qMaster              = new MasterModel;
        $qDataapproval        = new DataapprovalModel;
        /* ----------
         Source
        ----------------------- */
        $qDataapproval        = $qDataapproval->getProfile($id)->first();
        $qApproveds               = $qMaster->getSelectApprovaller();
        $qJabatan                 = $qMaster->getSelectJabatan();
        $qcabang                  = $qMaster->getJenisCabang();
        //hilangkan duplikasi
        foreach($qcabang as $key => $value){
            $cabang[$value->id] = $value;
        }
        $qcabang = $cabang;
        $qDepartemen                = $qMaster->getJenisDepartemenPusat();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_dataapproval", "label"=>"Jabatan ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]     = form_select_disable(array("name"=>"id_cabang", "label"=>"Pusat/Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qcabang, "value"=>$qDataapproval->id_cabang));
        $data["fields"][]     = form_hidden(array("name"=>"id_cabang", "label"=>"Pusat/Cabang","value"=>$qDataapproval->id_cabang));
        $data["fields"][]     = form_select_disable(array("name"=>"id_departemen", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qDataapproval->id_departemen));
        $data["fields"][]     = form_hidden(array("name"=>"id_departemen", "label"=>"Departemen","value"=>$qDataapproval->id_departemen));
        $data["fields"][]     = form_select_disable(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qDataapproval->id_jabatan));
        $data["fields"][]     = form_hidden(array("name"=>"id_jabatan", "label"=>"Jabatan","value"=>$qDataapproval->id_jabatan));
        $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"Approvaller"));
        $data["fields"][]     = form_select(array("name"=>"id_cabang_approvaller", "label"=>"Pusat/Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qcabang));
        $data["fields"][]     = form_select2(array("name"=>"id_departemen_approvaller", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen));
        $data["fields"][]     = form_select2(array("name"=>"id_approvaller", "label"=>"Disetujui oleh", "mandatory"=>"yes", "source"=>$qApproveds));
        $data["fields"][]     = form_text(array("name"=>"approvallevel", "label"=>"Level Approval", "mandatory"=>"yes"));
      
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.formselect", $data);
    }

    public function update(Request $request)
    {   
        if($request->id_cabang_approvaller == null || $request->id_cabang_approvaller == "0"){
            return redirect("/dataapproval/edit/".$request->id_dataapproval)
                ->withErrors(['Pilih Cabang Approvaller'])
                ->withInput();
        }else if($request->id_departemen_approvaller == null || $request->id_departemen_approvaller == "0"){
            return redirect("/dataapproval/edit/".$request->id_dataapproval)
                ->withErrors(['Pilih Departemen Approvaller'])
                ->withInput();
        }else if($request->id_approvaller == null || $request->id_departemen_approvaller == "0"){
            return redirect("/dataapproval/edit/".$request->id_dataapproval)
                ->withErrors(['Pilih Jabatan Approvaller'])
                ->withInput();
        }
        // $rules = array(
        //             'id_jabatan' => 'required|'               
        // );

        // $messages = [
        //             'id_jabatan.required' => 'Dataapproval harus diisi',

        // ];

        // $validator = Validator::make($request->all(), $rules, $messages);

        // if ($validator->fails()) {
        //     return redirect("/dataapproval/edit/" . $request->input("id_dataapproval"))
        //         ->withErrors($validator)
        //         ->withInput();
        // } else {
            $qDataapproval      = new DataapprovalModel;
            # ---------------
            $response = $qDataapproval->updateData($request);
            if($response['status'] == "failed"){
                # --------------
                    return redirect("/dataapproval/index")->withErrors([$response['response']])
                            ->withInput();
            }else{
            # ---------------
            session()->flash("success_message", "Dataapproval has been updated");
            # ---------------
            return redirect("/dataapproval/index");
            }
        // }
    }

    public function delete($id) {
        $data["title"]         = "Delete Dataapproval";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/dataapproval/remove";
        /* ----------
         Source
        ----------------------- */
        $qMaster              = new MasterModel;
        $qDataapproval     = new DataapprovalModel;
        $qDataapproval                 = $qDataapproval->getProfile($id)->first();
        $qApproveds               = $qMaster->getSelectApprovaller();
        $qJabatan                 = $qMaster->getSelectJabatan();
        $qcabang                  = $qMaster->getJenisCabang();
        //hilangkan duplikasi
        foreach($qcabang as $key => $value){
            $cabang[$value->id] = $value;
        }
        $qcabang = $cabang;
        $qDepartemen                = $qMaster->getJenisDepartemenPusat();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_dataapproval", "label"=>"Dataapproval ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]     = form_select_disable(array("name"=>"id_cabang", "label"=>"Pusat/Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qcabang, "value"=>$qDataapproval->id_cabang));
        $data["fields"][]     = form_select_disable(array("name"=>"id_departemen", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qDataapproval->id_departemen));
        $data["fields"][]     = form_select_disable(array("name"=>"id_jabatan", "label"=>"Jabatan", "mandatory"=>"yes", "source"=>$qJabatan,"value"=>$qDataapproval->id_jabatan));
        $data["fields"][]      = form_text3(array("name"=>"", "label"=>"", "mandatory"=>"yes", "value"=>"Approvaller"));
        $data["fields"][]     = form_select_disable(array("name"=>"id_cabang_approvaller", "label"=>"Pusat/Cabang", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qcabang, "value"=>$qDataapproval->id_cabang_approval));
        $data["fields"][]     = form_select_disable(array("name"=>"id_departemen_approvaller", "label"=>"Departemen", "withnull"=>"withnull", "mandatory"=>"yes", "source"=>$qDepartemen,"value"=>$qDataapproval->id_departemen_approval));
        $data["fields"][]     = form_select_disable(array("name"=>"id_approvaller", "label"=>"Disetujui oleh", "mandatory"=>"yes", "source"=>$qApproveds,"value"=>$qDataapproval->id_approvaller));
        $data["fields"][]     = form_text(array("name"=>"approvallevel", "label"=>"Level Approval", "readonly"=>"readonly", "mandatory"=>"yes","value"=>$qDataapproval->approvallevel));
       
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        // if($request->input("id") != 1) {
            $qDataapproval     = new DataapprovalModel;
            # ---------------
            $qDataapproval->removeData($request);
            # ---------------
            session()->flash("success_message", "Dataapproval has been removed");
        // } else {
        //     session()->flash("error_message", "Dataapproval cannot be removed");
        // }
      # ---------------
        return redirect("/dataapproval/index"); 
    }

    public function departemenCabang($param){
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qApproveds               = $qMaster->getSelectApprovaller();
        $qDepartemen                 = $qMaster->getSelectDepartemenById($param);
        $qcabang                 = $qMaster->getJenisCabang();
        //hilangkan duplicate data cabang
        foreach($qDepartemen as $key => $value){
            $qDepartemenn[$value->id] = $value;
        }
        $qDepartemen = $qDepartemenn;
        // $qDepartemen = array_unique($qDepartemen);
        // dd($qDepartemen);

        //GET THE ACCOUNT BASED ON TYPE
        // $csc_state = csc_states::where('country_id','=',$param)->get();
        //CREATE AN ARRAY 
        $options = array();      
        foreach ($qDepartemen as $arrayForEach) {
                  $options += array($arrayForEach->id => $arrayForEach->name);                
              }
        
        return response()->json($options);
        // return Response::json($options);
    }

    public function jabatan($id_cabang, $param){
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qApproveds               = $qMaster->getSelectApprovaller();
        $qDepartemen                 = $qMaster->getSelectDepartemen();
        if($param != "all"){
        $qJabatan                 = $qMaster->getSelectJabatanById($id_cabang, $param);
        }else{
        $qJabatan                 = $qMaster->getSelectJabatanByAll($id_cabang);
        }
        //GET THE ACCOUNT BASED ON TYPE
        // $csc_state = csc_states::where('country_id','=',$param)->get();
        //CREATE AN ARRAY 
        $options = array();      
        foreach ($qJabatan as $arrayForEach) {
                  $options += array($arrayForEach->id => $arrayForEach->name);                
              }
        
        return response()->json($options);
        // return Response::json($options);
    }

    public function approveler($id_cabang, $id_departemen, $param){
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qApproveds               = $qMaster->getSelectApprovallerById($id_cabang, $id_departemen, $param);
        $qDepartemen                 = $qMaster->getSelectDepartemen();
        $qcabang                 = $qMaster->getJenisCabang();
        //GET THE ACCOUNT BASED ON TYPE
        // $csc_state = csc_states::where('country_id','=',$param)->get();
        //CREATE AN ARRAY 
        $options = array();      
        foreach ($qApproveds as $arrayForEach) {
                  $options += array($arrayForEach->id => $arrayForEach->name);                
              }
        
        return response()->json($options);
        // return Response::json($options);
    }
}
