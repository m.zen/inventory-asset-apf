<?php

namespace App\Http\Controllers\Absensi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\CutiMassalModel;

class CutiMassalController extends Controller
{
    public function __construct(Request $request)
     {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = "Periode absen";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/cutimassal/proses";
        /* ----------
         Source
        ----------------------- */

          // $data["tabs"]          = array(
          //                        array("label"=>"Periode Absen", "url"=>"/periodeabsen/index/", "active"=>""), 
          //                        array("label"=>"Cuti Massal", "url"=>"/cutimassal/index/", "active"=>"active"), 
          //                       );
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]    = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text (array("name"=>"Tgl Cuti Massal", "label"=>"", "mandatory"=>"yes"));
        $data["fields"][]      = form_text (array("name"=>"Bulan ke", "label"=>"", "mandatory"=>"yes"));
        $data["fields"][]      = form_text (array("name"=>"Tahun", "label"=>"", "mandatory"=>"yes"));
                  


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        
    }
   
    public function proses(Request $request)
    {
        $rules = array(
                    'Tgl_Cuti_Massal' => 'required|',
                    'Bulan_ke' => 'required|',
                    'Tahun' => 'required|',
                    
                                          
        );

        $messages = [
                    'Tgl_Cuti_Massal.required' => 'Tanggal cuti massal harus diisi',
                    'Bulan_ke.required' => 'Bulan ke harus diisi',
                    'Tahun.required' => 'Tahun harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/periodeabsen/index/" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qProsesgaji      = new CutiMassalModel;
            # ---------------
            $qProsesgaji->prosesData($request);
            # ---------------
            session()->flash("success_message", "Proses Absensi has been updated");
            # ---------------
           return redirect("/cutimassal/index/");
        }
     }
}
