<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\LaporanCutiModel;
use App\Model\Master\MasterModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use App\Model\Personalia\KaryawanModel;

class LaporanCutiController extends Controller
{
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/laporancuti/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qLaporanCutiModel      = new LaporanCutiModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_grupabsen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"NIK"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Nama"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Tanggal Masuk"
                                                ,"name"=>"tgl_masuk"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Tanggal Keluar"
                                                ,"name"=>"tgl_keluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Sisa Cuti"
                                                ,"name"=>"sisacuti"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),


        							);
        # ---------------
         if($request->has('text_search')) {
            session(["SES_SEARCH_DATACUTI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATACUTI");

        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATACUTI");
        }
        # ---------------
        $data["select"]        = $qLaporanCutiModel->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qLaporanCutiModel->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function import() {
        $data["title"]        = "Import";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/laporancuti/importsave";
        /* ----------
         Updatesisacuti
        ----------------------- */
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_upload(array("name"=>"file_excel", "label"=>"File Excel", "mandatory"=>"yes"));
      
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }
  
    public function importsave(Request $request)
      { 
          $rules = array(
                      'file_excel' => 'required|'                 
          );
  
          $messages = [
                      'file_excel' => 'Pilih file',
  
          ];
  
          $validator = Validator::make($request->all(), $rules, $messages);
  
          if ($validator->fails()) {
                return redirect("/laporancuti/import")
                ->withErrors($validator)
                ->withInput();
          } else {
            $qKaryawan  = new LaporanCutiModel;
            
            $qKaryawan->importcsv($request);
            # ---------------
            session()->flash("success_message", "update has been updated");
            # ---------------
            return redirect("/laporancuti/index");
          }
      }

      public function export()
      {   
          $query  = DB::table("p_karyawan as a")
                              ->select("a.*","b.nama_cabang","c.nama_departemen","d.nama_jabatan")
                              ->leftjoin("m_cabang as b","b.id_cabang","=","a.id_cabang")
                              ->leftjoin("m_departemen as c","c.id_departemen","=","a.id_departemen")
                              ->leftjoin("m_jabatan as d","d.id_jabatan","=","a.id_jabatan")
                              ->where("a.aktif",1)
                              ->orderBy("a.id_cabang","ASC")
                              ->orderBy("a.id_departemen","ASC")->get();
  
          
  
        $styleArray = [
              'font' => [
                  'bold' => true,
              ],
              'alignment' => [
                  'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ],
              'borders' => [
                  'top' => [
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                  ],
              ],
              'fill' => [
                  'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                  'rotation' => 90,
                  'startColor' => [
                      'argb' => 'FFA0A0A0',
                  ],
                  'endColor' => [
                      'argb' => 'FFFFFFFF',
                  ],
              ],
          ];
  
          $spreadsheet = new Spreadsheet();
          $sheet = $spreadsheet->getActiveSheet();
          $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
          $baris = 2;
          /*-------------------------
          KOLOM DATA
          ---------------------------*/
          
          $sheet->setCellValue('A1', 'No.');
          $sheet->setCellValue('B1', 'Employee ID');
          $sheet->setCellValue('C1', 'Employee Name');
          $sheet->setCellValue('D1', 'Time Off Name');
          $sheet->setCellValue('E1', 'Total');
        $qKaryawan            = new KaryawanModel;
        foreach($query as $row) 
          {
        
                    $sheet->setCellValue('A'.$baris,$baris-1);
                    $sheet->setCellValue('B'.$baris,$row->nik);
                    $sheet->setCellValue('C'.$baris,$row->nama_karyawan);
                    $sheet->setCellValue('D'.$baris,'Cuti Tahunan');
                    $sheet->setCellValue('E'.$baris,$row->sisacuti);
         
                   $baris=$baris+1;
          }
  
          foreach(range('B','E') as $columnID)
           {
               $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
               
                    
           }
           $spreadsheet->getActiveSheet()->getColumnDimension('AA')
                    ->setAutoSize(true);
  
          
          $writer = new Xlsx($spreadsheet);
          // $writer->save('data karyawanlengkap.xlsx');
          $judul    = "balance cuti";  
          $name_file = $judul.'.xlsx';
        // $path = storage_path('Laporan\\'.$name_file);
        $path = public_path().'/app/'.$name_file;
        $contents = is_dir($path);
        // $headers = array('Content-Type' => File::mimeType($path));
        // dd($path.'/'.$name_file,$contents);
        $writer->save($path);
        $filename   = str_replace("@", "/", $path);
          # ---------------
          header("Cache-Control: public");
          header("Content-Description: File Transfer");
          header("Content-Disposition: attachment; filename=".$name_file);
          header("Content-Type: application/xlsx");
          header("Content-Transfer-Encoding: binary");
          # ---------------
          require "$filename";
          # ---------------
          exit;
  
     
  
      }
}