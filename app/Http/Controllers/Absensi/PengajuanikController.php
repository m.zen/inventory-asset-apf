<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use DB;
use View;
// use Excel;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DataabsensiModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\PengajuanikModel;
use DateTime;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use File;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;
class pengajuanikController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

  public function __construct(Request $request) {
        # ---------------
    $uri                      = getUrl() . "/index";
        # ---------------
    $qMenu                    = new MenuModel;
    $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
    $this->PROT_Parent        = $rs[0]->parent_name;
    $this->PROT_ModuleName    = $rs[0]->name;
    $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
    View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
}

public function index(Request $request, $page=null)
{
    $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
    $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
    $data["form_act"]       = "/pengajuanik/proses";
    $qMaster                = new MasterModel;

    $qCabang                = $qMaster->getSelectCabang();
    $qKaryawan                = $qMaster->getSelectKaryawan();
    
    $data["fields"][]       = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
    $data["fields"][]      = form_datepicker(array("name"=>"tgl_ijin_khusus", "label"=>"Tanggal Ijin Khusus", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
    $data["fields"][]       = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "withnull"=>"yes","mandatory"=>"yes", "source"=>$qCabang));
    $data["fields"][]     = form_selectby(array("name"=>"id_karyawan", "label"=>"Nama-Nama Karyawan", "mandatory"=>"yes", "source"=>[]));
    # ---------------
    $data["buttons"][]      = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
    $data["buttons"][]      = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
    # ---------------
    return view("default.form_jadwal", $data);
    }
    public function departemenCabang($param){
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $options = [];
        $qDepartemen = DB::table('p_karyawan')->where([['id_cabang', $param],['aktif',1]])->get();
        //hilangkan duplicate data cabang
        if(count($qDepartemen)>0){
        foreach($qDepartemen as $key => $value){
            $qDepartemenn[$value->id_karyawan] = $value;
        }
        $qDepartemen = $qDepartemenn;
        
        // $qDepartemen = array_unique($qDepartemen);
        // dd($qDepartemen);

        //GET THE ACCOUNT BASED ON TYPE
        // $csc_state = csc_states::where('country_id','=',$param)->get();
        //CREATE AN ARRAY 
        $options = array();      
        foreach ($qDepartemen as $arrayForEach) {
                  $options += array($arrayForEach->id_karyawan => $arrayForEach->nama_karyawan);                
              }
        }
        return response()->json($options);
        // return Response::json($options);
    }

    public function proses(Request $request)
    {   
        $rules = array(
                    'id_karyawan' => 'required|',
                    'id_cabang' => 'required|',
                    'tgl_ijin_khusus' => 'required|',
                    
                                          
        );

        $messages = [
                    'id_karyawan.required' => 'Nama karyawan harus dipilih',
                    'id_cabang.required' => 'Cabang ke harus dipilih',
                    'tgl_ijin_khusus.required' => 'Tanggal Ijin Khusus harus diisi',

        ];

        if($request->id_cabang == 0){
            return redirect("/pengajuanik/index/" )
                ->withErrors(['Cabang ke harus dipilih'])
                ->withInput();
        }
        if(count($request->id_karyawan) == 1 && $request->id_karyawan[0] == 0){
            return redirect("/pengajuanik/index/" )
                ->withErrors(['Nama karyawan harus dipilih'])
                ->withInput();
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pengajuanik/index/" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qProsesgaji      = new PengajuanikModel;
            # ---------------
            $qProsesgaji->prosesData($request);
            # ---------------
            session()->flash("success_message", "Proses has been updated");
            # ---------------
           return redirect("/pengajuanik/index/");
        }
     }

    
}
