<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\GrupabsenModel;
use App\Model\Master\MasterModel;
class GrupabsenController extends Controller
{
     protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/grupabsen/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qGrupabsen             = new GrupabsenModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_grupabsen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"Nama Group Absen"
                                                ,"name"=>"nama_grupabsen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Jam Masuk(Senin-Jumat)"
                                                ,"name"=>"jam_masuk"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Jam Keluar(Senin-Jumat)"
                                                ,"name"=>"jam_keluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jam Masuk(Sabtu)"
                                                ,"name"=>"jam_masuk2"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jam Keluar(Sabtu)"
                                                ,"name"=>"jam_keluar2"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),

				 array("label"=>"Jam Masuk(Minggu)"
                                                ,"name"=>"jam_masuk3"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jam Keluar(Minggu)"
                                                ,"name"=>"jam_keluar3"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),

        							);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_GRUPABSEN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_GRUPABSEN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_GRUPABSEN");
        }
        # ---------------
        $data["select"]        = $qGrupabsen->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qGrupabsen->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add() {
        $data["title"]         = "Add Grupabsen";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/grupabsen/save";
        /* ----------
         Grupabsen
        ----------------------- */
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
       // $qStatus			   = getSelectStatusGrupabsen();
        /* ----------
         Tabs
        ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
        ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
	    $data["fields"][]      = form_text(array("name"=>"nama_grupabsen", "label"=>"Nama Grup Absen", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"jam_masuk", "label"=>"Jam Masuk Senin-Jumat(hh:mm) Senin-Jumat", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"jam_keluar", "label"=>"Jam Keluar Senin-Jumat (hh:mm) Senin-Jumat", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"jam_masuk2", "label"=>"Jam Masuk Sabtu (hh:mm) Sabtu", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"jam_keluar2", "label"=>"Jam Keluar Sabtu (hh:mm) Sabtu", "mandatory"=>"yes"));
 	$data["fields"][]      = form_text(array("name"=>"jam_masuk3", "label"=>"Jam Masuk Minggu (hh:mm) Minggu", "mandatory"=>"yes"));
        $data["fields"][]      = form_text(array("name"=>"jam_keluar3", "label"=>"Jam Keluar Minggu (hh:mm) Minggu", "mandatory"=>"yes"));
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request) {
        $rules = array(
                      
                      'nama_grupabsen' => 'required'                     );

        $messages = ['nama_grupabsen.required' => 'Grupabsen harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/Grupabsen/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrupabsen  = new GrupabsenModel;
            # ---------------
            $qGrupabsen->createData($request);
            # ---------------
            session()->flash("success_message", "Grupabsen has been saved");
            # ---------------
            return redirect("/grupabsen/index");
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Grupabsen";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/grupabsen/update";
        /* ----------
         Grupabsen
        ----------------------- */
        $qMaster              = new MasterModel;
        $qGrupabsen             = new GrupabsenModel;
        /* ----------
         Source
        ----------------------- */
        $qGrupabsen           = $qGrupabsen->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_grupabsen", "label"=>"Grupabsen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nama_grupabsen", "label"=>"Grupabsen",  "mandatory"=>"yes", "value"=>$qGrupabsen->nama_grupabsen));
      	$data["fields"][]      = form_text(array("name"=>"jam_masuk", "label"=>"Jam Masuk Senin-Jumat (hh:mm)", "mandatory"=>"yes", "value"=>$qGrupabsen->jam_masuk));
        $data["fields"][]      = form_text(array("name"=>"jam_keluar", "label"=>"Jam Keluar Senin-Jumat (hh:mm)", "mandatory"=>"yes", "value"=>$qGrupabsen->jam_keluar));
        $data["fields"][]      = form_text(array("name"=>"jam_masuk2", "label"=>"Jam Masuk Sabtu (hh:mm)", "mandatory"=>"yes", "value"=>$qGrupabsen->jam_masuk2));
        $data["fields"][]      = form_text(array("name"=>"jam_keluar2", "label"=>"Jam Keluar Sabtu (hh:mm)", "mandatory"=>"yes", "value"=>$qGrupabsen->jam_keluar2));
        $data["fields"][]      = form_text(array("name"=>"jam_masuk3", "label"=>"Jam Masuk Minggu (hh:mm)", "mandatory"=>"yes", "value"=>$qGrupabsen->jam_masuk3));
        $data["fields"][]      = form_text(array("name"=>"jam_keluar3", "label"=>"Jam Keluar Minggu (hh:mm)", "mandatory"=>"yes", "value"=>$qGrupabsen->jam_keluar3));
                 
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'nama_grupabsen' => 'required|'               
        );

        $messages = [
                    'nama_grupabsen.required' => 'Grupabsen harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/grupabsen/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrupabsen      = new GrupabsenModel;
            # ---------------
            $qGrupabsen->updateData($request);
            # ---------------
            session()->flash("success_message", "Grupabsen has been updated");
            # ---------------
            return redirect("/grupabsen/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Grupabsen";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/grupabsen/remove";
        /* ----------
         Source
        ----------------------- */
        $qGrupabsen     = new GrupabsenModel;
         $qGrupabsen                 = $qGrupabsen->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_grupabsen", "label"=>"Grupabsen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_grupabsen", "label"=>"Grupabsen", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qGrupabsen->nama_grupabsen));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qGrupabsen     = new GrupabsenModel;
            # ---------------
            $qGrupabsen->removeData($request);
            # ---------------
            session()->flash("success_message", "Grupabsen has been removed");
        } else {
            session()->flash("error_message", "Grupabsen cannot be removed");
        }
      # ---------------
        return redirect("/grupabsen/index"); 
    }
}
