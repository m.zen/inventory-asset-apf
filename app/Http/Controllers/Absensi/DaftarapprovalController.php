<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Validator;
use Auth;
use DB;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DaftarapprovalModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\PengajuantmModel;
use Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

class DaftarapprovalController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
    $uri                      = getUrl() . "/index";
        # ---------------
    $qMenu                    = new MenuModel;
    $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
    $this->PROT_Parent        = $rs[0]->parent_name;
    $this->PROT_ModuleName    = $rs[0]->name;
    $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
    View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

public function index(Request $request, $page=null)
{
    $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
    $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
    $data["form_act"]       = "/daftarapproval/index";
    $data["active_page"]    = (empty($page)) ? 1 : $page;
    $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
         ----------------------- */
         $qMenu                  = new MenuModel;
         $qDaftarapproval        = new DaftarapprovalModel;
        # ---------------
         $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
         ----------------------- */
         
        # ---------------
        //  if($request->has('text_search')) {
        //     session(["SES_SEARCH_DATAABSENSI" => $request->input("text_search")]);
        //     # ---------------
        //     $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        // } else {
        //     $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        // }

        if($request->has('text_search')) {
            session(["SES_SEARCH_PENGAJUAN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PENGAJUAN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_PENGAJUAN");
        }

        $parameter_system = DB::table('m_profilesystem')->select('tgl_absenawal','tgl_absenakhir')->first();
        $tgl_absenawal = date('Y-m-d', strtotime($parameter_system->tgl_absenawal));
        $tgl_absenakhir = date('Y-m-d', strtotime($parameter_system->tgl_absenakhir));
        $status_approval = ['3'=>'Semua', '1'=>'Approve','2'=>'Pending','0'=>'Reject'];

        if($request->has('text_status')) {
            if($request->input("text_status") == '3'){
                session(["SES_SEARCH_STATUS" => '3']);
            }else {
                session(["SES_SEARCH_STATUS" => $request->input("text_status")]);
            }
            # ---------------
            $data["status_approval"]   = $request->session()->get("SES_SEARCH_STATUS");
            $data["option"]   = $status_approval;
        } else {
	    if($request->session()->get("SES_SEARCH_STATUS")==null){
                session(["SES_SEARCH_STATUS" => '3']);
            }
            $data["status_approval"]   = $request->session()->get("SES_SEARCH_STATUS");
            $data["option"]   = $status_approval;
        }
        if($request->has('tgl_awal_pengajuan') && $request->has('tgl_akhir_pengajuan')) {
            if($request->tgl_awal_pengajuan == null){
                session(["SES_SEARCH_PENGAJUAN_FROMDATE" => $tgl_absenawal]);
                session(["SES_SEARCH_PENGAJUAN_TODATE" => $tgl_absenakhir]);
            }else{
                session(["SES_SEARCH_PENGAJUAN_FROMDATE" => $request->input('tgl_awal_pengajuan')]);
                session(["SES_SEARCH_PENGAJUAN_TODATE" => $request->input('tgl_akhir_pengajuan')]);
            }
            # ---------------
            $data["tgl_awal_pengajuan"]   = $request->session()->get("SES_SEARCH_PENGAJUAN_FROMDATE");
            $data["tgl_akhir_pengajuan"]   = $request->session()->get("SES_SEARCH_PENGAJUAN_TODATE");

        } 
        else {
            if($request->session()->get("SES_SEARCH_PENGAJUAN_FROMDATE")==null){
                session(["SES_SEARCH_PENGAJUAN_FROMDATE" => $tgl_absenawal]);
            }
            if($request->session()->get("SES_SEARCH_PENGAJUAN_TODATE")==null){
                session(["SES_SEARCH_PENGAJUAN_TODATE" => $tgl_absenakhir]);
            }
            $data["tgl_awal_pengajuan"]   = $request->session()->get("SES_SEARCH_PENGAJUAN_FROMDATE");
            $data["tgl_akhir_pengajuan"]   = $request->session()->get("SES_SEARCH_PENGAJUAN_TODATE");
        }
        # ---------------
        $data["select"]        = $qDaftarapproval->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDaftarapproval->getList($request->input("text_search"));
        if(count($data["select"])>0){
            foreach ($data["select"] as $key => $value) {
                # code...
                // dd($value);
                // $tm = DB::table('tm_pengajuanabsen')->select("statusapproval")->where([["id_tm",$value->id_tm]])->get();
                // dd($tm);
                // if(count($tm)>0 != null){
                //     if($tm->statusapproval == null || $tm->statusapproval ==''){
                //         $status = ' Pending';
                //     }else if($tm->statusapproval == 0){
                //         $status = ' Reject';
                //     }else if($tm->statusapproval == 1){
                //         $status = ' Approved';
                //     }else if($tm->statusapproval == 2){
                //         $status = ' Return';
                //     }
                //     $data["select"][$key]->status = $status;
                // }
                
                // else{
                //     if($value->statusapproval == null || $value->statusapproval ==''){
                //         $status = ' Pending';
                //     }else if($value->statusapproval == 0){
                //         $status = ' Reject';
                //     }else if($value->statusapproval == 1){
                //         $status = ' Approved';
                //     }else if($value->statusapproval == 2){
                //         $status = ' Return';
                //     }
                //     $data["select"][$key]->status = $status;
                // }
                $cekpengajuan = DB::table('tm_pengajuanabsen')->select('id_tm','order','statusapproval','tgl_approved')->where('id_tm',$value->id_tm)->orderBy('order', 'DESC')->get();
                if(count($cekpengajuan)>1){
                    // $grup_nik_tgl = $this->group_by('statusapproval',$cekpengajuan);
                    // dd($grup_nik_tgl);
                    $jumlahstatus_1 = 0;
                    $jumlahapproved = 0;
                    $jumlahreject = 0;
                    $jumlahpending = 0;
                    $jumlahreturn = 0;
                    foreach ($cekpengajuan as $key2 => $value2) {
                        # code...
                        if($value2->statusapproval == null || $value2->statusapproval ==''){
                            $jumlahpending = $jumlahpending+1;
                        }else if($value2->statusapproval == 0){
                            $jumlahreject = $jumlahreject+1;
                        }else if($value2->statusapproval == 1){
                            $jumlahstatus_1 = $jumlahstatus_1+1;
                        }else if($value2->statusapproval == 2){
                            $jumlahreturn = $jumlahreturn+1;
                        }
                    }
                    if($jumlahstatus_1 == count($cekpengajuan)){
                        $status = '1 dari '. count($cekpengajuan).' - Approved';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 1 && $jumlahreject == 0){
                        $status = '1 dari '. count($cekpengajuan).' - Approved, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 1 && $jumlahreject == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Reject, Pending';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 0 && $jumlahreject == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Reject';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 0 && $jumlahreject == 2){
                            $status = '1 dari '. count($cekpengajuan).' - Reject';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 2 && $jumlahreject == 0){
                            $status = '1 dari '. count($cekpengajuan).' - Pending, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 1 && $jumlahreject == 0 && $jumlahreturn == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Return, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 0 && $jumlahreject == 0 && $jumlahreturn == 2){
                        $status = '1 dari '. count($cekpengajuan).' - Return, Return';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 0 && $jumlahreject == 0 && $jumlahreturn == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Approved, Return';
                    }else{
                        $status = ' ';
                    }
                }else if(count($cekpengajuan) == 1){
                    if($cekpengajuan[0]->statusapproval == null || $cekpengajuan[0]->statusapproval ==''){
                        $status = ' Pending';
                        // $status = '1 dari '. count($cekpengajuan).' Pending';
                    }else if($cekpengajuan[0]->statusapproval == 0){
                        $status = ' Reject';
                    }else if($cekpengajuan[0]->statusapproval == 1){
                        $status = ' Approved';
                    }else if($cekpengajuan[0]->statusapproval == 2){
                        $status = ' Return';
                    }else{
                        $status = ' ';
                    }
                }
                else{
                        $status = '';
                }
                // $data["select"][$key] = $value;
                $data["select"][$key]->status = $status;
            }
        }

        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        $data["table_header"]   = 
         array(array("label"=>"ID"
            ,"name"=>"id_pengajuanabsen"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"checkbox"
            ,"item-class"=>""
            ,"width"=>"5%"
            ,"add-style"=>""),
         array("label"=>"Nik"
            ,"name"=>"nik"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"20%"
            ,"add-style"=>""),
         array("label"=>"Nama Karyawan"
            ,"name"=>"nama_karyawan"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"20%"
            ,"add-style"=>""),
        array("label"=>"Cabang Approvaller"
            ,"name"=>"nama_cabang"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
        array("label"=>"Departemen Approvaller"
            ,"name"=>"nama_departemen"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Nama Pengajuan"
            ,"name"=>"nama_jenisabsen"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"20%"
            ,"add-style"=>""),
         array("label"=>"Tanggal Awal"
            ,"name"=>"tgl_awal"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Tanggal Akhir"
            ,"name"=>"tgl_akhir"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Status"
            ,"name"=>"status"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
        
        

     );
        $grup_id = Auth::user()->group_id;
        
          return view("default.datapengajuan", $data);
        
    
    }

    public function index_pending(Request $request, $page=null)
{
    $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
    $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
    $data["form_act"]       = "/daftarapproval/index_pending";
    $data["active_page"]    = (empty($page)) ? 1 : $page;
    $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
         ----------------------- */
         $qMenu                  = new MenuModel;
         $qDaftarapproval        = new DaftarapprovalModel;
        # ---------------
         $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
         ----------------------- */
         
        # ---------------
         if($request->has('text_search')) {
            session(["SES_SEARCH_DATAABSENSI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        }
        # ---------------
        $data["select"]        = $qDaftarapproval->getListPending($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDaftarapproval->getListPending($request->input("text_search"));
        if(count($data["select"])>0){
            foreach ($data["select"] as $key => $value) {
                # code...
                $cekpengajuan = DB::table('tm_pengajuanabsen')->select('id_tm','order','statusapproval','tgl_approved')->where('id_tm',$value->id_tm)->orderBy('order', 'DESC')->get();
                if(count($cekpengajuan)>1){
                    // $grup_nik_tgl = $this->group_by('statusapproval',$cekpengajuan);
                    // dd($grup_nik_tgl);
                    $jumlahstatus_1 = 0;
                    $jumlahapproved = 0;
                    $jumlahreject = 0;
                    $jumlahpending = 0;
                    $jumlahreturn = 0;
                    foreach ($cekpengajuan as $key2 => $value2) {
                        # code...
                        if($value2->statusapproval == null || $value2->statusapproval ==''){
                            $jumlahpending = $jumlahpending+1;
                        }else if($value2->statusapproval == 0){
                            $jumlahreject = $jumlahreject+1;
                        }else if($value2->statusapproval == 1){
                            $jumlahstatus_1 = $jumlahstatus_1+1;
                        }else if($value2->statusapproval == 2){
                            $jumlahreturn = $jumlahreturn+1;
                        }
                    }
                    if($jumlahstatus_1 == count($cekpengajuan)){
                        $status = '1 dari '. count($cekpengajuan).' - Approved';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 1 && $jumlahreject == 0){
                        $status = '1 dari '. count($cekpengajuan).' - Approved, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 1 && $jumlahreject == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Reject, Pending';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 0 && $jumlahreject == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Reject';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 0 && $jumlahreject == 2){
                            $status = '1 dari '. count($cekpengajuan).' - Reject';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 2 && $jumlahreject == 0){
                            $status = '1 dari '. count($cekpengajuan).' - Pending, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 1 && $jumlahreject == 0 && $jumlahreturn == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Return, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 0 && $jumlahreject == 0 && $jumlahreturn == 2){
                        $status = '1 dari '. count($cekpengajuan).' - Return, Return';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 0 && $jumlahreject == 0 && $jumlahreturn == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Approved, Return';
                    }else{
                        $status = ' ';
                    }
                }else if(count($cekpengajuan) == 1){
                    if($cekpengajuan[0]->statusapproval == null || $cekpengajuan[0]->statusapproval ==''){
                        $status = ' Pending';
                        // $status = '1 dari '. count($cekpengajuan).' Pending';
                    }else if($cekpengajuan[0]->statusapproval == 0){
                        $status = ' Reject';
                    }else if($cekpengajuan[0]->statusapproval == 1){
                        $status = ' Approved';
                    }else if($cekpengajuan[0]->statusapproval == 2){
                        $status = ' Return';
                    }else{
                        $status = ' ';
                    }
                }
                else{
                        $status = '';
                }
                // $data["select"][$key] = $value;
                $data["select"][$key]->status = $status;
            }
        }

        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        $data["table_header"]   = 
         array(array("label"=>"ID"
            ,"name"=>"id_pengajuanabsen"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"checkbox"
            ,"item-class"=>""
            ,"width"=>"5%"
            ,"add-style"=>""),
         array("label"=>"Nik"
            ,"name"=>"nik"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"20%"
            ,"add-style"=>""),
         array("label"=>"Nama Karyawan"
            ,"name"=>"nama_karyawan"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"20%"
            ,"add-style"=>""),
        array("label"=>"Cabang Approvaller"
            ,"name"=>"nama_cabang"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
        array("label"=>"Departemen Approvaller"
            ,"name"=>"nama_departemen"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Nama Pengajuan"
            ,"name"=>"nama_jenisabsen"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"20%"
            ,"add-style"=>""),
         array("label"=>"Tanggal Awal"
            ,"name"=>"tgl_awal"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Tanggal Akhir"
            ,"name"=>"tgl_akhir"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Status"
            ,"name"=>"status"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
        
        

     );
        return view("default.list", $data);
    
    }

    public function approve($id) {
        $data["title"]        = "Approve";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/daftarapproval/approve/update";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster              = new MasterModel;
         $qDaftarapproval     = new DaftarapprovalModel;
        /* ----------
         Source
         ----------------------- */
        $qGroups               = $qMaster->getSelectGroup();
        $qJenisPengajuan       = $qMaster->getJenisPengajuan();
        $qNamaAnggota          = $qMaster->getNamaAnggotaDepartemen(Auth::user()->nik);
        $listApprove           = getListApprove();
        $cek                   = DB::table("tm_pengajuanabsen as a")
                                ->select('a.id_tm','c.*')
                                ->leftjoin("tm_timeoff as b", "b.id_tm","a.id_tm")
                                ->leftjoin("p_karyawan as c", "c.id_karyawan","b.id_karyawan")
                                ->where('id_pengajuanabsen',$id)->first();
        $cekjumlahtm = DB::table("tm_pengajuanabsen as a")->select('a.id_tm')->where('id_tm',$cek->id_tm)->get();
        if(count($cekjumlahtm) > 1){
            $paths = "http://localhost/hrapf/storage/app/foto_timeoff/";
            $p_karyawan = DB::table("p_karyawan")->where([['nik', Auth::user()->nik]])->first();
            $p_karyawan_timeoff = DB::table("p_karyawan")->where([['id_karyawan', $cek->id_karyawan]])->first();
            $levelapproval = DB::table("tm_dataapproval")
                        ->where('id_cabang',$cek->id_cabang)
                        ->where('id_departemen',$cek->id_departemen)
                        ->where('id_jabatan',$cek->id_jabatan)
                        ->where('id_cabang_approval',$p_karyawan->id_cabang)
                        ->where('id_departemen_approval',$p_karyawan->id_departemen)
                        ->where('id_approvaller',$p_karyawan->id_jabatan)
                        ->first();
            // dd($levelapproval, $cek);
            // $levelapproval         = $qDaftarapproval->getDataApprovalLevel(Auth::user()->nik);
            $qDaftarapproval       = $qDaftarapproval->getProfileApproval($cek->id_tm, $levelapproval->approvallevel)->first();
            // dd($p_karyawan,$cek);

            $qlastDaftarapproval  = DB::table("tm_pengajuanabsen as a")
                                ->select("a.*","c.nik","c.nama_karyawan","d.*","b.*")
                                ->leftjoin("tm_timeoff as b","b.id_tm","=","a.id_tm")
                                ->leftjoin("p_karyawan as c","c.id_karyawan","=","b.id_karyawan")
                                ->leftjoin("tm_jenisabsen as d","d.id_jenisabsen","=","b.id_status_karyawan")
                                ->where([["a.id_tm", $cek->id_tm],["a.order", 1]])
                                ->first();
            // $pathurl               = url('').env('APPS_PATH_VIEW_DOC')."/";
            $vurl                  = url('')."/app/foto_timeoff/".$qDaftarapproval->foto;
            $pathImage             = url('')."/app/img/icon/remove.png";
            $vurlDelete            = url('')."/pengajuantm/deletefile/".$id."/".$qDaftarapproval->foto;
            if($levelapproval->approvallevel == '2' && ($qlastDaftarapproval->statusapproval ==0 || $qlastDaftarapproval->statusapproval ==null || $qlastDaftarapproval->statusapproval ==2)){
                return redirect("/daftarapproval/index")
                ->withErrors(['Belum Bisa di Approve'])
                ->withInput();
            }else{
                /* ----------
                 Fields
                 ----------------------- */
            $data["fields"][]      = form_hidden(array("name"=>"id_pengajuanabsen", "label"=>"Dataabsensi ID", "readonly"=>"readonly", "value"=>$qDaftarapproval->id_pengajuanabsen));
            $data["fields"][]      = form_hidden(array("name"=>"id_tm", "label"=>"Order", "readonly"=>"readonly", "value"=>$qDaftarapproval->id_tm));
            $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Order", "readonly"=>"readonly", "value"=>$qDaftarapproval->id_karyawan));
            $data["fields"][]      = form_hidden(array("name"=>"order", "label"=>"Order", "readonly"=>"readonly", "value"=>$levelapproval->approvallevel));
            $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
            $data["fields"][]     = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly",  "value"=>$qDaftarapproval->nama_karyawan));
            $data["fields"][]     = form_text(array("name"=>"id_status_karyawan", "label"=>"Nama Pengajuan", "readonly"=>"readonly","value"=>$qDaftarapproval->nama_jenisabsen));
            $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Tanggal Awal Pengajuan", "readonly"=>"readonly","value"=>date("d/m/Y", strtotime($qDaftarapproval->tgl_awal)), "first_selected"=>"yes"));
            $data["fields"][]      = form_text(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir Pengajuan", "readonly"=>"readonly","value"=>date("d/m/Y", strtotime($qDaftarapproval->tgl_akhir)), "first_selected"=>"yes"));
            $data["fields"][]      = form_text(array("name"=>"komentar", "label"=>"Keterangan", "readonly"=>"readonly", "value"=>$qDaftarapproval->komentar));
            if($qDaftarapproval->inisial =='CT'){
                $data["fields"][]      = form_text(array("name"=>"sisacuti", "label"=>"Sisa Cuti", "readonly"=>"readonly", "value"=>$qDaftarapproval->sisacuti));
            }
            $data["fields"][]      = form_select(array("name"=>"statusapproval", "label"=>"Status Approval", "source"=>$listApprove, "mandatory"=>"yes"));
            $data["fields"][]      = form_text(array("name"=>"komentarapp", "label"=>"Komentar","value"=>$qDaftarapproval->komentarapp));
           
                 // $data["fields"][]      = form_upload(array("name"=>"foto", "label"=>"Foto"));
                 // $data["fields"][]      = form_select(array("name"=>"status_absensi", "label"=>"Status Hadir", "mandatory"=>"yes","value"=>$qDatapengajuantm->status_absensi, "source"=>$qJenisPengajuan));
            // $data["fields"][]      = form_text(array("label"=>"Foto"));
            if(empty($qDaftarapproval->foto)) {
                  $data["fields"][]     = "<div class=\"form-group\">
                                          <label class=\"col-md-3 control-label\"></label>
                                          <div class=\"col-md-9\">
                                                    <span class=\"btn btn-sm btn-danger\">Dokumen Tidak Ada</span>
                                          </div></div>";
            } else {
                  $data["fields"][]     = "<div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\"></label>
                                            <div class=\"col-md-9\">
                                                      <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a>
                                            </div></div>";
            }
                                                      // <a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>

                # ---------------
            $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Save"));
            $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
                # ---------------
                 return view("default.form", $data);
            }
        }else{
            $paths = "http://localhost/hrapf/storage/app/foto_timeoff/";
            $p_karyawan = DB::table("p_karyawan")->where([['nik', Auth::user()->nik]])->first();
            $levelapproval = DB::table("tm_dataapproval")
                        ->where('id_cabang_approval',$p_karyawan->id_cabang)
                        ->where('id_departemen_approval',$p_karyawan->id_departemen)
                        ->where('id_approvaller',$p_karyawan->id_jabatan)
                        ->first();
			if(is_null($levelapproval)){
                return redirect("/daftarapproval/index")
                ->withErrors(['Tidak terdaftar sebagai approvaller'])
                ->withInput();
            }
            $qDaftarapproval       = $qDaftarapproval->getProfileApproval($cek->id_tm, $levelapproval->approvallevel)->first();
            // dd($levelapproval, $qDaftarapproval);
            
            $vurl                  = url('')."/app/foto_timeoff/".$qDaftarapproval->foto;
            $pathImage             = url('')."/app/img/icon/remove.png";
            $vurlDelete            = url('')."/inquery/deletedokumen/".$id."/".$qDaftarapproval->foto;
            
            $data["fields"][]      = form_hidden(array("name"=>"id_pengajuanabsen", "label"=>"Dataabsensi ID", "readonly"=>"readonly", "value"=>$qDaftarapproval->id_pengajuanabsen));
            $data["fields"][]      = form_hidden(array("name"=>"id_tm", "label"=>"Order", "readonly"=>"readonly", "value"=>$qDaftarapproval->id_tm));
            $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Order", "readonly"=>"readonly", "value"=>$qDaftarapproval->id_karyawan));
            $data["fields"][]      = form_hidden(array("name"=>"order", "label"=>"Order", "readonly"=>"readonly", "value"=>$qDaftarapproval->order));
            $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
            $data["fields"][]     = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly",  "value"=>$qDaftarapproval->nama_karyawan));
            $data["fields"][]     = form_text(array("name"=>"id_status_karyawan", "label"=>"Nama Pengajuan", "readonly"=>"readonly","value"=>$qDaftarapproval->nama_jenisabsen));
            $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Tanggal Awal Pengajuan", "readonly"=>"readonly","value"=>date("d/m/Y", strtotime($qDaftarapproval->tgl_awal)), "first_selected"=>"yes"));
            $data["fields"][]      = form_text(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir Pengajuan", "readonly"=>"readonly","value"=>date("d/m/Y", strtotime($qDaftarapproval->tgl_akhir)), "first_selected"=>"yes"));
            $data["fields"][]      = form_text(array("name"=>"komentar", "label"=>"Keterangan", "readonly"=>"readonly", "value"=>$qDaftarapproval->komentar));
            if($qDaftarapproval->inisial =='CT'){
            $data["fields"][]      = form_text(array("name"=>"sisacuti", "label"=>"Sisa Cuti", "readonly"=>"readonly", "value"=>$qDaftarapproval->sisacuti));
            }
            $data["fields"][]      = form_select(array("name"=>"statusapproval", "label"=>"Status Approval", "source"=>$listApprove, "mandatory"=>"yes"));
                 // $data["fields"][]      = form_upload(array("name"=>"foto", "label"=>"Foto"));
            $data["fields"][]      = form_text(array("name"=>"komentarapp", "label"=>"Komentar","value"=>$qDaftarapproval->komentarapp));
          
                 // $data["fields"][]      = form_select(array("name"=>"status_absensi", "label"=>"Status Hadir", "mandatory"=>"yes","value"=>$qDatapengajuantm->status_absensi, "source"=>$qJenisPengajuan));
            // $data["fields"][]      = form_text(array("label"=>"Foto"));
            if(empty($qDaftarapproval->foto)) {
                  $data["fields"][]     = "<div class=\"form-group\">
                                          <label class=\"col-md-3 control-label\"></label>
                                          <div class=\"col-md-9\">
                                                    <span class=\"btn btn-sm btn-danger\">Dokumen Tidak Ada</span>
                                          </div></div>";
            } else {
                  $data["fields"][]     = "<div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\"></label>
                                            <div class=\"col-md-9\">
                                                      <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a>
                                            </div></div>";
            }
                                                      // <a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>

                # ---------------
            $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Save"));
            $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
                # ---------------
                 return view("default.form", $data);
        }
     }

     
     public function approveupdate(Request $request)
     {
        $rules = array(
            'statusapproval' => 'required|'               
        );

        $messages = [
            'statusapproval.required' => 'Belum Di Pilih',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/daftarapproval/approve/" . $request->input("id_pengajuanabsen"))
            ->withErrors($validator)
            ->withInput();
        } else {
            $qDaftarapproval      = new DaftarapprovalModel;
            # ---------------
            $qDaftarapproval->updateData($request);
            # ---------------
            session()->flash("success_message", "Dataabsensi has been updated");
            # ---------------
            return redirect("/daftarapproval/index");
        }
    }

    public function cetakpending(){
        $qDaftarapproval      = new DaftarapprovalModel;
        # ---------------
        $qdatapending         = $qDaftarapproval->getdatapending2();
        $jumlahheader = 19;
        // call name coloumn excel use function this
        // $this->columnLetter($jumlahheader);
        $nama_nama_header = ["","No.","NIK","NAMA","PERIODE AWAL","PERIODE AKHIR","JENIS PENGAJUAN"];
        foreach($qdatapending as $key => $value){
            $tm_dataapproval = DB::table("tm_dataapproval")
                            ->where([
                                    ["id_cabang", $value->id_cabang],
                                    ["id_departemen", $value->id_departemen],
                                    ["id_jabatan", $value->id_jabatan],
                                    ["approvallevel", $value->order],
                                    // ["aktif",1]
                                    ])
                            ->first();
	    if(!is_null($tm_dataapproval)){
            $p_karyawan = DB::table('p_karyawan')
                            ->where([
                                ["id_cabang", $tm_dataapproval->id_cabang_approval],
                                ["id_departemen", $tm_dataapproval->id_departemen_approval],
                                ["id_jabatan", $tm_dataapproval->id_approvaller],
                                ["aktif",1]
                                    ])
                            ->first();
            $gruoppending[$p_karyawan->id_karyawan][] = $value;
	    }
        }
        $jumlahbarisapprovaller = count($gruoppending);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $baris = 4;
        $rowColoumnApprovaller = 1;
        $rowColoumnHeader = 2;
        $rowColoumn = 3;
        $sheet->setCellValue('A1', 'STATUS PENDING');
        
        foreach($gruoppending as $index => $valuepending){
            $no =1;
            $p_karyawan = DB::table("p_karyawan")->select('nama_karyawan')->where([["id_karyawan", $index]])->get()->toArray();
            if(count($p_karyawan)>0){
            $nama_karyawan = array_column($p_karyawan,'nama_karyawan');
            $nama =implode(',',$nama_karyawan);
            $sheet->setCellValue($this->columnLetter(1).$rowColoumnHeader, 'Disetujui Oleh : '.$nama);
            $sheet->setCellValue($this->columnLetter(2).$rowColoumnHeader, "No.");
            $sheet->setCellValue($this->columnLetter(3).$rowColoumnHeader, "NIK");
            $sheet->setCellValue($this->columnLetter(4).$rowColoumnHeader, "NAMA");
            $sheet->setCellValue($this->columnLetter(5).$rowColoumnHeader, "PERIODE AWAL");
            $sheet->setCellValue($this->columnLetter(6).$rowColoumnHeader, "PERIODE AKHIR");
            $sheet->setCellValue($this->columnLetter(7).$rowColoumnHeader, "JENIS PENGAJUAN");
            foreach($valuepending as $indexpending => $value){
                $sheet->setCellValue($this->columnLetter(2).$rowColoumn, $no++);
                $sheet->setCellValue($this->columnLetter(3).$rowColoumn, $value->nik);
                $sheet->setCellValue($this->columnLetter(4).$rowColoumn, $value->nama_karyawan);
                $sheet->setCellValue($this->columnLetter(5).$rowColoumn, $value->tgl_awal);
                $sheet->setCellValue($this->columnLetter(6).$rowColoumn, $value->tgl_akhir);
                $sheet->setCellValue($this->columnLetter(7).$rowColoumn, $value->nama_jenisabsen);
                $rowColoumnHeader = $rowColoumnHeader+1;
                $rowColoumn = $rowColoumn+1;
            }
            $rowColoumnHeader = $rowColoumnHeader+1;
            $rowColoumn = $rowColoumn+1;
        }
    }
        foreach(range('A','Z') as $columnID)
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
            
            
        }
        $writer = new Xlsx($spreadsheet);
        // $writer->save('data karyawanlengkap.xlsx');
        $judul    = "STATUS PENDING";
        $name_file = $judul.'.xlsx';
      	header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $name_file .'"'); 
        header('Cache-Control: max-age=0');
        
        $writer->save('php://output');
    }

    public function columnLetter($c){
        
        $c = intval($c);
        if ($c <= 0) return '';
        
        $letter = '';
        
        while($c != 0){
            $p = ($c - 1) % 26;
            $c = intval(($c - $p) / 26);
            $letter = chr(65 + $p) . $letter;
        }
        
        return $letter;
        
    }
}

