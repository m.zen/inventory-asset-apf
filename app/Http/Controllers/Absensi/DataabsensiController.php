<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DataabsensiModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\TemporaryAbsenModel;
use DateTime;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
// use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use File;
use Excel;
use Fpdf;
use PDF;
class DataabsensiController extends Controller
{
  protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

  public function __construct(Request $request) {
        # ---------------
    $uri                      = getUrl() . "/index";
        # ---------------
    $qMenu                    = new MenuModel;
    $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
    $this->PROT_Parent        = $rs[0]->parent_name;
    $this->PROT_ModuleName    = $rs[0]->name;
    $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
    View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
}

public function index(Request $request, $page=null)
{
    $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
    $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
    $data["form_act"]       = "/absensi/index";
    $data["active_page"]    = (empty($page)) ? 1 : $page;
    $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
         ----------------------- */
         $qMenu                  = new MenuModel;
         $qDataabsensi           = new DataabsensiModel;
        # ---------------
         $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
         ----------------------- */
         $data["table_header"]   = array(array("label"=>"ID"
            ,"name"=>"id_dataabsensi"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"checkbox"
            ,"item-class"=>""
            ,"width"=>"5%"
            ,"add-style"=>""),
         array("label"=>"N I K"
            ,"name"=>"nik"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"15%"
            ,"add-style"=>""),
         array("label"=>"Nama Karyawan"
            ,"name"=>"nama_karyawan"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Cabang"
            ,"name"=>"nama_cabang"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Tgl Absen"
            ,"name"=>"tgl_absensi"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"10%"
            ,"add-style"=>""),
         array("label"=>"Shift"
            ,"name"=>"shift"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"10%"
            ,"add-style"=>""),
         array("label"=>"Jam Masuk"
            ,"name"=>"jam_masuk"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"10%"
            ,"add-style"=>""),
         array("label"=>"Jam Keluar"
            ,"name"=>"jam_keluar"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"10%"
            ,"add-style"=>""),
         array("label"=>"Status"
            ,"name"=>"status_absensi"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
        array("label"=>"Foto"
            ,"name"=>"link"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"view"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Keterangan"
            ,"name"=>"keterangan"
            ,"align"=>"center"
            ,"item-align"=>"right"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>"")



     );
        # ---------------
         if($request->has('text_search')) {
            session(["SES_SEARCH_DATAABSENSI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");

        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        }

        $parameter_system = DB::table('m_profilesystem')->select('tgl_absenawal','tgl_absenakhir')->first();
        $tgl_absenawal = date('Y-m-d', strtotime($parameter_system->tgl_absenawal));
        $tgl_absenakhir = date('Y-m-d', strtotime($parameter_system->tgl_absenakhir));
        $interval1 = $this->getDatesFromRange($tgl_absenawal, $tgl_absenakhir);
        

        if($request->has('tgl_awal') && $request->has('tgl_akhir')) {
            if($request->tgl_awal == null){
                session(["SES_SEARCH_DATAABSENSI_FROMDATE" => $tgl_absenawal]);
                session(["SES_SEARCH_DATAABSENSI_TODATE" => $tgl_absenakhir]);
            }else{
                session(["SES_SEARCH_DATAABSENSI_FROMDATE" => $request->input('tgl_awal')]);
                session(["SES_SEARCH_DATAABSENSI_TODATE" => $request->input('tgl_akhir')]);
            }
            # ---------------
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_DATAABSENSI_FROMDATE");
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_DATAABSENSI_TODATE");

        } 
        else {
            if($request->session()->get("SES_SEARCH_DATAABSENSI_FROMDATE") == null){
                session(["SES_SEARCH_DATAABSENSI_FROMDATE" => $tgl_absenawal]);
                session(["SES_SEARCH_DATAABSENSI_TODATE" => $tgl_absenakhir]);
            }else{
                
                session(["SES_SEARCH_DATAABSENSI_FROMDATE" =>  $request->session()->get("SES_SEARCH_DATAABSENSI_FROMDATE") ]);
                session(["SES_SEARCH_DATAABSENSI_TODATE" => $request->session()->get("SES_SEARCH_DATAABSENSI_TODATE") ]);
            }
            $data["tgl_awal"]   = $request->session()->get("SES_SEARCH_DATAABSENSI_FROMDATE");
            $data["tgl_akhir"]   = $request->session()->get("SES_SEARCH_DATAABSENSI_TODATE");
        }

        # ---------------
        $data["select"]        = $qDataabsensi->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDataabsensi->getList($request->input("text_search"));
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }

        if(count($data["select"])>0){
            foreach ($data["select"] as $key => $value) {
                # code...
                $hari = new DateTime($value->tgl_absensi);
                $kode_hari = $hari->format('w');
                if($kode_hari  == 6){
                    $data["select"][$key]->shift = "Sabtu";
                }else if($kode_hari == 0){
                    $data["select"][$key]->shift = "Minggu";
                }else{
                    $data["select"][$key]->shift = "Normal";
                }

                if(in_array($value->tgl_absensi, $tgl_libur)){
                    $data["select"][$key]->shift = "National Day";
                }

                $qTimeoff = DB::table('tm_timeoff')
                            ->where('id_karyawan', $value->id_karyawan)
                            // ->where('tgl_awal', $value->tgl_absensi)
                            // ->orwhere('id_karyawan', $value->id_karyawan)
                            // ->where('tgl_akhir', $value->tgl_absensi)
                            ->where('tgl_awal', '<=', $value->tgl_absensi)
                            ->where('tgl_akhir', '>=', $value->tgl_absensi)
                            ->first();

                $data["select"][$key]->link = '';
                if($qTimeoff != null){
                    // dd($qTimeoff);
                    $tgl_awal = date('Y-m-d', strtotime($qTimeoff->tgl_awal));
                    $tgl_akhir = date('Y-m-d', strtotime($qTimeoff->tgl_akhir));
                    $interval2 = $this->getDatesFromRange($tgl_awal, $tgl_akhir);
                    if(in_array($value->tgl_absensi, $interval2)){
                    $vurl= '';
                        if($qTimeoff->foto != null){
                        $vurl = url('')."/app/foto_timeoff/".$qTimeoff->foto;
                        }
                        $data["select"][$key]->link = $vurl;
                        // $data["select"][$key]->keterangan = $qTimeoff->komentar;

                        if($value->status_absensi == "Waiting"){
                           $jenisabsen = DB::table('tm_jenisabsen')->select('inisial')->where('id_jenisabsen', $qTimeoff->id_status_karyawan)->first();
                           $data["select"][$key]->status_absensi = $value->status_absensi."-".$jenisabsen->inisial;
                        }
                    }
                }
            }
        }
        // $total_potong_makan = 0;
        // $total_potong_gaji = 0;
        // $total_terlambat = 0;
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        $grupid = Auth::user()->group_id;
        if($grupid != 5 ){
            return view("default.dataabsensi", $data);
        }else{
            return view("default.dataabsensi2", $data);
        }

    }

    public function getDatesFromRange2($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
    }

    public function add() {
        $data["title"]         = "Add Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/save";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $qStatusAbsensi        = getStatusHadir2();

       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan","withnull"=>"yes", "mandatory"=>"yes", "source"=>$qKaryawan));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_absensi", "label"=>"Tanggal Absensi", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
         $data["fields"][]      = form_time(array("name"=>"jam_masuk", "label"=>"Jam Masuk (hh:mm)"));
         $data["fields"][]      = form_time(array("name"=>"jam_keluar", "label"=>"Jam Keluar (hh:mm)"));
         $data["fields"][]      = form_select(array("name"=>"status_absensi", "label"=>"Status Hadir" ,"withnull"=>"yes", "mandatory"=>"yes", "source"=>$qStatusAbsensi));
         // $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Keterangan", "mandatory"=>"yes"));

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
     }



     public function save(Request $request) {
        if($request->id_karyawan == 0){
            return redirect("/absensi/add")
            ->withErrors(['Pilih Nama Karyawan'])
            ->withInput();
        }else if($request->status_absensi == '0'){
            return redirect("/absensi/add")
            ->withErrors(['Pilih Status Absensi'])
            ->withInput();
        }

        $tgl_absen = setYMD($request->tgl_absensi,'/');
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }
        if(in_array($tgl_absen, $tgl_libur)){
             return redirect("/absensi/add")
            ->withErrors(['Tanggal yg dipilih hari libur nasional'])
            ->withInput();
        }

        $hari = new DateTime($tgl_absen);
        $nohari = $hari->format('w');
        if($nohari == 0){
            return redirect("/absensi/add")
            ->withErrors(['Tanggal yg dipilih hari libur'])
            ->withInput();
        }
        
            $qDataabsensi  = new DataabsensiModel;
            # ---------------
            $qDataabsensi->createData($request);
            # ---------------
            session()->flash("success_message", "Dataabsensi has been saved");
            # ---------------
            return redirect("/absensi/index");
    }

    public function edit($id) {
        $data["title"]        = "Edit Dataabsensi";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/absensi/update";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster              = new MasterModel;
         $qDataabsensi             = new DataabsensiModel;
        /* ----------
         Source
         ----------------------- */
         $qDataabsensi             = $qDataabsensi->getProfile($id)->first();
         $qGroups              = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $qStatusAbsensi        = getStatusHadir2();
         $status = (explode('/',$qDataabsensi->status_absensi));
         if(in_array('H', $status)){
            $qDataabsensi->status_absensi = 'H';
         }
        /* ----------
         Fields
         ----------------------- */
         $data["fields"][]      = form_hidden(array("name"=>"id_dataabsensi", "label"=>"Dataabsensi ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"Karyawan ID", "readonly"=>"readonly", "value"=>$qDataabsensi->id_karyawan));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
         $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "readonly"=>"readonly", "value"=>$qDataabsensi->nama_karyawan));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_absensi", "label"=>"Tanggal Absensi", "mandatory"=>"yes","value"=>date('d/m/Y',strtotime($qDataabsensi->tgl_absensi)), "first_selected"=>"yes"));
         $data["fields"][]      = form_time(array("name"=>"jam_masuk", "label"=>"Jam Masuk (hh:mm)","value"=>date('H:i',strtotime($qDataabsensi->jam_masuk))));
         $data["fields"][]      = form_time(array("name"=>"jam_keluar", "label"=>"Jam Keluar (hh:mm)","value"=>date('H:i',strtotime($qDataabsensi->jam_keluar))));
         $data["fields"][]      = form_select(array("name"=>"status_absensi", "label"=>"Status Hadir","withnull"=>"yes", "mandatory"=>"yes","value"=>$qDataabsensi->status_absensi, "source"=>$qStatusAbsensi));
         // $data["fields"][]      = form_text(array("name"=>"keterangan", "label"=>"Keterangan", "mandatory"=>"yes"));


        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
     }

     public function update(Request $request)
     {
        if($request->id_karyawan == 0){
            return redirect("/absensi/edit/".$request->id_dataabsensi)
            ->withErrors(['Pilih Nama Karyawan'])
            ->withInput();
        }
        if($request->status_absensi == '0'){
            return redirect("/absensi/edit/".$request->id_dataabsensi)
            ->withErrors(['Pilih Status Absensi'])
            ->withInput();
        }

        $tgl_absen = setYMD($request->tgl_absensi,'/');
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }
        if(in_array($tgl_absen, $tgl_libur)){
             return redirect("/absensi/edit/".$request->id_dataabsensi)
            ->withErrors(['Tanggal yg dipilih hari libur nasional'])
            ->withInput();
        }

        $hari = new DateTime($tgl_absen);
        $nohari = $hari->format('w');
        if($nohari == 0){
            return redirect("/absensi/edit/".$request->id_dataabsensi)
            ->withErrors(['Tanggal yg dipilih hari libur'])
            ->withInput();
        }
            $qDataabsensi      = new DataabsensiModel;
            # ---------------
            $qDataabsensi->updateData($request);
            # ---------------
            session()->flash("success_message", "Dataabsensi has been updated");
            # ---------------
            return redirect("/absensi/index");
    }

    public function delete($id) {
        $data["title"]         = "Delete Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/remove";
        /* ----------
         Source
         ----------------------- */
         $qDataabsensi     = new DataabsensiModel;
         $qDataabsensi                 = $qDataabsensi->getProfile($id)->first();
         $qStatusAbsensi        = getStatusHadir();
        /* ----------
         Fields
         ----------------------- */
         $data["fields"][]      = form_hidden(array("name"=>"id_dataabsensi", "label"=>"Dataabsensi ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama Karyawan", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qDataabsensi->nama_karyawan));
         $data["fields"][]      = form_text(array("name"=>"tgl_absensi", "label"=>"Tanggal Absensi", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qDataabsensi->tgl_absensi));
         $data["fields"][]      = form_text(array("name"=>"status_absensi", "label"=>"Status Absensi", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qDataabsensi->status_absensi));

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
     }

     public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qDataabsensi     = new DataabsensiModel;
            # ---------------
            $qDataabsensi->removeData($request);
            # ---------------
            session()->flash("success_message", "Dataabsensi has been removed");
        } else {
            session()->flash("error_message", "Dataabsensi cannot be removed");
        }
      # ---------------
        return redirect("/absensi/index"); 
    }

    public function import() {
        $data["title"]         = "Import Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/import/save";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $qStatusAbsensi        = getStatusHadir();
         $getJenisUpload        = getJenisUpload();
       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
         $data["fields"][]      = form_radio(array("name"=>"jenis_file","id"=>"Hj", "label"=>"Jenis File", "mandatory"=>"yes", "source"=>$getJenisUpload, "value"=>$getJenisUpload));
         $data["fields"][]      = form_upload(array("name"=>"file_txt", "label"=>"File Upload", "mandatory"=>"yes"));

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         // dd($data);
         return view("default.form_download", $data);
     }

     public function export() {
        $data["title"]         = "Export Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/export/save";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $qStatusAbsensi        = getStatusHadir();

       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
         $data["fields"][]      = form_upload(array("name"=>"file_txt", "label"=>"File", "mandatory"=>"yes"));

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
     }

     public function array_group_by(array $arr, callable $key_selector) {
      $result = array();
      foreach ($arr as $i) {
        $key = call_user_func($key_selector, $i);
        $result[$key][] = $i;

    }  
    return $result;
}

public function importsave(Request $request) {
    $rules = array(

      'jenis_file' => 'required|'                     );

    $messages = ['jenis_file.required' => 'Pilih Jenis File'];

    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
        return redirect("/absensi/import")
        ->withErrors($validator)
        ->withInput();
    } else {    
        if($request->jenis_file == 'excel'){
            $rows = Excel::load($request->file('file_txt'))->get();
            $qData  = new TemporaryAbsenModel;
            $qimport = $qData->importcsv($rows);
            if($qimport['status'] == "failed"){
            # --------------
                return redirect("/absensi/import")->withErrors([array_unique($qimport['response'])])
                        ->withInput();
            }else{
                session()->flash("success_message", "Dataabsensi has been update");
                # ---------------
                return redirect("/absensi/index");
            }
        }else{

        $default_file   = $request->file_txt;
        $file_content   = file_get_contents($request->file_txt->getRealPath());
            // $file_name      = "TUGAS_" . date("dmY") . "_" . rand(100,200) . "." . $default_file->getClientOriginalExtension();
            // $file_url       = "/collection/temp/";
            // $mysqlfile      = "uploads/collection/temp/". $file_name;
                # -------------------
        $file_name      = "TUGAS_" . date("dmY") . "_".rand(100,200).".". $default_file->getClientOriginalExtension();
        $file_url       = "/collection/temp/";
                // $mysqlfile      = "uploads/collection/temp/" . $file_name;
                # -------------------
        $request->file('file_txt')->storeAs($file_url.$file_name, $default_file->getClientOriginalName());
        
        $pathh = fopen(storage_path().'/app/'.$file_url.$file_name.'/'.$default_file->getClientOriginalName(), "r");
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        // foreach ($hari_libur as $key => $value) {
        //     # code...
        //     $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        // }
        //   while(($data = fgets($pathh, 10000)) !== FALSE){
        //   if($data != "\r\n"){
        //   $data = explode('\t', $data);
        //   // dd($data[0]);
        //     $hari = new DateTime($data[1]);
        //     if(!in_array($hari->format('Y-m-d'), $tgl_libur)){
        //         $data[1] = $hari->format('w');
        //         $data[7] = $hari->format('Y-m-d');
        //         $data[8] = $hari->format('H:i:s');
        //         $c[] = $line;
        //     }
        //   }
        // }

        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }
        while (($data = fgetcsv($pathh, 1000, "\t")) !== FALSE) {
            // if($data != null ){
            // // if($line != "\r\n"){
            // $hari = new DateTime($data[1]);
            // if(!in_array($hari->format('Y-m-d'), $tgl_libur)){
            //     $data[1] = $hari->format('w');
            //     $data[7] = $hari->format('Y-m-d');
            //     $data[8] = $hari->format('H:i:s');
                $d[] = $data;
            // }
            // }
        }
        fclose($pathh);

        foreach ($d as $key => $value) {
                # code...
                if($value[0] != null){
                    // $kalimatbaru = str_replace(' ', '', $kalimat);
                        if($value[1]){
                            $hari = new DateTime(str_replace(' ', '', $value[1]));
                        }
    
                    // if(!in_array($hari->format('Y-m-d'), $tgl_libur)){
                        if($value[0]){
                        $value[0] = str_replace(' ', '', $value[0]);
                        }
                        if($value[1]){
                            $value[1] = new DateTime(str_replace(' ', '', $value[1]));
                        }
                        if($value[2]){
                        $value[2] = str_replace(' ', '', $value[2]);
                        }
                        if($value[3]){
                        $value[3] = str_replace(' ', '', $value[3]);
                        }
                        if($value[4]){
                        $value[4] = preg_replace('/\s+/', ' ', $value[4]);
                        }
                        $value[1] = $hari->format('w');
                        $value[7] = $hari->format('Y-m-d');
                        $value[8] = $hari->format('H:i:s');
                        $c[] = $value;
                    // }
                }
            }
    
            $result = array();
            foreach ($c as $key => $value) {
              # code...
              $result[$value[0]][] = $value;
          }
    
        foreach ($result as $key2 => $value2) {
              # code...
              foreach ($value2 as $key3 => $value3) {
                  # code...
                    if($value3[5] == 'I'){
                        if(date('Hi', strtotime($value3[8])) > 1330){
                            $result2[$key2][$value3[7]]["O"][] = $value3;
                        }else{
                            $result2[$key2][$value3[7]]["I"][] = $value3;
                        }
                    }else{
                        if(date('Hi', strtotime($value3[8])) <= 1330){
                            $result2[$key2][$value3[7]]["I"][] = $value3;
                        }else{
                            $result2[$key2][$value3[7]]["O"][] = $value3;
                        }
                    }
              }
            }
    
            foreach ($result2 as $key4 => $value4) {
                # code...
                foreach ($value4 as $key5 => $value5) {
                    # code...
                    foreach ($value5 as $key6 => $value6) {
                        # code...
                        foreach ($value6 as $key7 => $value7) {
                            # code...
                            $jumlah = count($value6)-1;
                            if($key6 == "I" && $key7 == 0){
                                // $result3[$key4][$key5][$key6][] = $value7;
                                $endvalue[] = $key6.','.$key4.','.$key5.','.$value7[1].','.$value7[8].','.$value7[4];
                            }else if($key6 == "O" && $key7 == $jumlah){
                                // $result3[$key4][$key5][$key6][] = $value7;
                                $endvalue[] = $key6.','.$key4.','.$key5.','.$value7[1].','.$value7[8].','.$value7[4];
                            }
                        }
                    }
                }
            }
        $grup_nik = $this->array_group_by($c, function($i){  return $i[0]; });
        $grup_tgl = $this->array_group_by($c, function($i){  return $i[7]; });
        $listgrupNik = array_keys($grup_nik);
        $listgrupTgl = array_keys($grup_tgl);
        foreach ($grup_nik as $key => $value) {
            # code...
            $grup_nik_tgl[$key] = $this->array_group_by($value, function($i){  return $i[7]; });
            $grup_nama[$key] = array_keys($this->array_group_by($value, function($i){  return $i[4]; }));
        }
        for ($i=0; $i <count($listgrupNik) ; $i++) { 
            # code...
            for ($j=0; $j <count($listgrupTgl) ; $j++) { 
                # code...
                //
                $tgl = $listgrupTgl[$j];
                    if(in_array($tgl, array_keys($grup_nik_tgl[$listgrupNik[$i]]))){
                        $format_no1 = new DateTime($listgrupTgl[$j]);
                        $no_hari1 = $format_no1->format('w');
                        $listgrup[] = 'I,'.$listgrupNik[$i].','.$listgrupTgl[$j].','.$no_hari1.',00:00:00'.','.$grup_nama[$listgrupNik[$i]][0];
                    }
            }
        }
        $qData  = new TemporaryAbsenModel;
        $qimport = $qData->createData(array_merge($listgrup,$endvalue));
    
        if($qimport['status'] == "failed"){
        # --------------
        return redirect("/absensi/import")->withErrors(array_unique($qimport['response']))
                    ->withInput();
        }else{
        session()->flash("success-message",$qimport['response']);
        return redirect("/absensi/index");
        }
        
        
        }
    }
    }

    public function formatpdff($dataDokumen,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi){
    foreach ($dataDokumen as $key => $value) {
        $pemiliknik[$value["nik"]][] = $value["nama_karyawan"]."&".$value["tgl_absensi"]."&".$value["kethari"]."&".$value["schedule_in"]."&".$value["schedule_out"]."&".$value["status_absensi"]."&".$value["jam_masuk"]."&".$value["jam_keluar"]."&".$value["status_terlambat"]."&".$value["keterangan"];
    }
    $rowHeader = 5;
    $borderColumn = 1;
    foreach ($pemiliknik as $key2 => $value2) {
        # code...
        $total = 0;
        $potong_makan = 0;
        $potong_gaji = 0;
        $potong_hari_makan = 0;
        // $total_potong_makan = 0;
        $uang_makan = 0;
        $uang_gaji = 0;
        $ket_uang_makan = '';
        $level_karyawan = DB::table("p_karyawan as a")
                ->select("a.id_karyawan","a.id_jabatan","b.id_jabatan as m_id_jabatan","b.id_level as m_id_level","c.id_level","c.uang_makan","a.id_cabang")
                ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                ->leftjoin("m_level as c","c.id_level","=","b.id_level")
                ->where("nik", $key2)
                ->first();
		$batas_terlambat = 3;
        if($level_karyawan != null){
            $uang_makan = $level_karyawan->uang_makan;
			if($level_karyawan->id_cabang == 3 || $level_karyawan->id_cabang == 4){
				$batas_terlambat = 3;
			}
            // $uang_gaji = $level_karyawan->uang_makan;
        }
        $no = 1;
        foreach ($value2 as $key3 => $value3) {
            # code...
            $pecaharray = explode('&',$value3);
            $pecaharray2 = explode('/',$pecaharray[0]);

            $ketarray = str_split($pecaharray[1]);
            $jumlahkaratter = count($ketarray);
            $jumlahbaris = $jumlahkaratter/25;
            $pembulatan = floor($jumlahbaris);
            if($pembulatan == 0){
                $pembulatan =1;
            }else{
                $pembulatan = $pembulatan;
            }
            $rowHeader = $rowHeader*$pembulatan;
            if($pecaharray[5] == 'H/LI' || $pecaharray[5] == 'H/LI/EO'){
                // if(in_array("LI", $pecaharray2)){
                // if(($key2 == 0 && $pecaharray[1] == 1) || ($key2 == 1 && $pecaharray[1] == 1)){
                $total = $total+1;
                if($total > $batas_terlambat){
                    $ket_uang_makan = '';
                }else{
                    $ket_uang_makan = 'Rp. '.$uang_makan;
                    if($uang_makan == 0){
                        $ket_uang_makan = '';
                    }
                }
            }else if($pecaharray[5] == 'CT' || $pecaharray[5] == 'I' || $pecaharray[5] == 'S' || $pecaharray[5] == 'A' || $pecaharray[5] == 'H/EO/NCO' || $pecaharray[5] == 'H/LI/NCO' || $pecaharray[5] == 'H/NCI' || $pecaharray[5] == 'H/NCO' || $pecaharray[5] == 'IDC'|| $pecaharray[5] == ''|| $pecaharray[5] == 'Waiting'){
              //   $inisial =['DL','CT','I','S','A','H','H/EO/NCI','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC'];
              // $val_stat_makan = [0,1,1,1,1,0,1,1,0,1,1,1,1,0,1];
              // $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
                    $ket_uang_makan = ' ';
            }else{
                $ket_uang_makan = 'Rp. '.$uang_makan;
                if($uang_makan == 0){
                $ket_uang_makan = '';
                }
            }

            if($pecaharray[2]=='National Day' || $pecaharray[2]=='Dayoff'){
                $ket_uang_makan = '';
            }
	
            $data["periode"] = (Object)[
                            "tanggal_awal"=> date('d-m-Y',strtotime($tgl_periode_awal)),
                            "tanggal_akhir"=>date('d-m-Y',strtotime($tgl_periode_akhir))
                            ];
            $data["qdata"][$pecaharray[0]][] = [
                                    "no"=>$no++,
                                    "nik"=>$key2,
                                    "nama"=>$pecaharray[0],
                                    "tanggal"=>$pecaharray[1],
                                    "shift"=>$pecaharray[2],
                                    "schedule_in"=>$pecaharray[3],
                                    "schedule_out"=>$pecaharray[4],
                                    "status"=>$pecaharray[5],
                                    "jam_masuk"=>$pecaharray[6],
                                    "jam_keluar"=>$pecaharray[7],
                                    "ket_uang_makan"=>$ket_uang_makan,
                                    "keterangan"=>$pecaharray[9],
                                    ];
            }
        }
            // Send data to the view using loadView function of PDF facade
            $pdf = PDF::loadView('laporan.laporanuangmakan', $data);
            return $pdf->download('laporanuangmakan.pdf');
            // return $pdf->stream('my.pdf',array('Attachment'=>1));
            
}
    public function formatpdf($dataDokumen,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi){
    foreach ($dataDokumen as $key => $value) {
		$pemiliknik[$value["nik"]][] = $value["nama_karyawan"]."&".$value["tgl_absensi"]."&".$value["kethari"]."&".$value["schedule_in"]."&".$value["schedule_out"]."&".$value["status_absensi"]."&".$value["jam_masuk"]."&".$value["jam_keluar"]."&".$value["status_terlambat"]."&".$value["keterangan"];
    }
    $rowHeader = 5;
    $borderColumn = 1;
    
    Fpdf::AddPage('L');
    Fpdf::SetLineWidth(0.3);
    Fpdf::SetFont("Arial", "B", 11);
    Fpdf::Ln(7);
    Fpdf::Cell(190, 5,'Perhitungan Uang Makan', 0, 0, 'C');
    Fpdf::Ln(10);
    

    foreach ($pemiliknik as $key2 => $value2) {
        # code...
        $total = 0;
        $potong_makan = 0;
        $potong_gaji = 0;
        $potong_hari_makan = 0;
        // $total_potong_makan = 0;
        $uang_makan = 0;
        $uang_gaji = 0;
        $ket_uang_makan = '';
        $no = 1;
        $totaluangmakan = 0;
        $tothari = 0;
        
        $level_karyawan = DB::table("p_karyawan as a")
                ->select("a.id_karyawan","a.id_jabatan","b.id_jabatan as m_id_jabatan","b.id_level as m_id_level","c.id_level","a.tunj_makan as uang_makan","a.id_cabang")
                ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                ->leftjoin("m_level as c","c.id_level","=","b.id_level")
                ->where("nik", $key2)
                ->first();
        $p_karyawan_ =  DB::table("p_karyawan as a")
                ->where("nik", $key2)
                ->first();
        $batas_terlambat = 3;
        if($level_karyawan != null){
            $uang_makan = $level_karyawan->uang_makan;
            if($level_karyawan->id_cabang == 3 || $level_karyawan->id_cabang == 4){
                $batas_terlambat = 3;
            }
            // $umr = $level_karyawan->umr;
            // $uang_gaji = $level_karyawan->uang_makan;
        }
        $uang_makan = isset($p_karyawan_->tunj_makan) ? $p_karyawan_->tunj_makan: 0;
    # ----------------
        Fpdf::SetFont('Arial', '', 7);
        Fpdf::Cell(15, 5, 'Tanggal : '.date('d-m-Y',strtotime($tgl_periode_awal)).' s/d '.date('d-m-Y',strtotime($tgl_periode_akhir)), 0, 0, 'L');
        Fpdf::Ln(5);
        Fpdf::Cell(4, $rowHeader, 'No.',$borderColumn, 0, 'C');
        Fpdf::Cell(17, $rowHeader, 'Employee Id.',$borderColumn, 0, 'C');
        Fpdf::Cell(43, $rowHeader, 'Full Name', $borderColumn, 0, 'C');
        Fpdf::Cell(15, $rowHeader, 'Date*', $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, 'Shift', $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, 'Schedule In', $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, 'Schedule Out', $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, 'Attendance Code', $borderColumn, 0, 'C');
        Fpdf::Cell(13, $rowHeader, 'Check In', $borderColumn, 0, 'C');
        Fpdf::Cell(13, $rowHeader, 'Check Out', $borderColumn, 0, 'C');
        Fpdf::Cell(16, $rowHeader, 'Uang Makan', $borderColumn, 0, 'C');
        Fpdf::Cell(75, $rowHeader, 'Keterangan', $borderColumn, 0, 'C');
        Fpdf::Ln();
        Fpdf::SetFont('Arial', '', 6);
        foreach ($value2 as $key3 => $value3) {
            # code...
            $pecaharray = explode('&',$value3);
            $pecaharray2 = explode('/',$pecaharray[0]);
            // if($pecaharray[1] == 1){
            // $potong_makan = $potong_makan+$uang_makan;
            // $potong_hari_makan = $potong_hari_makan+1;
            // }
            // if($pecaharray[2] == 1){
            //     $potong_gaji = $potong_gaji+1;
            // }
            // $total_potong_makan[$key] = $potong_makan;
            // $total_potong_gaji[$key] = $potong_gaji;
            // $total_hari_potong_makan[$key] = $potong_hari_makan;
            if($pecaharray[5] == 'H/LI' || $pecaharray[5] == 'H/LI/EO'){
                // if(in_array("LI", $pecaharray2)){
                // if(($key2 == 0 && $pecaharray[1] == 1) || ($key2 == 1 && $pecaharray[1] == 1)){
                $total = $total+1;
                if($total > $batas_terlambat){
                    $ket_uang_makan = '';
                    $uang = 0;
                }else{
                    $ket_uang_makan = 'Rp. '.$uang_makan;
                    $uang = $uang_makan;
                    if($uang_makan == 0){
                        $ket_uang_makan = '';
                        $uang = 0;
                    }
                }
				if($pecaharray[5] == 'H/LI/EO'){
                        $jam_masuk = $pecaharray[6];
                        $jam_keluar = $pecaharray[7];
                        if($total > $batas_terlambat){
                            $ket_uang_makan = '';
                            $uang = 0;
                        }else{
                            $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $jamkerja = $total_menit_telat;
                            if($jamkerja > 240){
                                $ket_uang_makan = 'Rp. '.$uang_makan;
                                $uang = $uang_makan;
                            }else{
                                $ket_uang_makan = ' ';
                                $uang = 0;
                            }
                        }
                }
            }else if($pecaharray[5] == 'CT' || $pecaharray[5] == 'S'|| $pecaharray[5] == 'ST' || $pecaharray[5] == 'A' || $pecaharray[5] == 'H/EO/NCO' || $pecaharray[5] == 'H/LI/NCO' || $pecaharray[5] == 'H/NCI/EO' || $pecaharray[5] == 'H/NCI' || $pecaharray[5] == 'H/NCO' || $pecaharray[5] == 'IDC'|| $pecaharray[5] == ''|| $pecaharray[5] == 'Waiting' || $pecaharray[5] =='IK'|| $pecaharray[5] =='OD'){
              //   $inisial =['DL','CT','I','S','A','H','H/EO/NCI','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC'];
              // $val_stat_makan = [0,1,1,1,1,0,1,1,0,1,1,1,1,0,1];
              // $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
                    $ket_uang_makan = ' ';
                    $uang = 0;
            }else if($pecaharray[5] == 'KR'){
              if($pecaharray[1] > '2020-05-01'){
                $ket_uang_makan = ' ';
                $uang = 0;
              }else{
                $ket_uang_makan = 'Rp. '.$uang_makan;
                $uang = $uang_makan;
              }
            }else if($pecaharray[5] == 'I'){
                    $jam_masuk = $pecaharray[6];
                    $jam_keluar = $pecaharray[7];

                    // dd($jam_masuk != null || $jam_masuk != "00:00:00" && $jam_keluar != "00:00:00" || $jam_keluar != null,$jam_masuk, $jam_keluar);
                    if($jam_masuk == null || $jam_keluar == null){
                        $ket_uang_makan = '';
                        $uang = 0;
                    }else if($jam_masuk == '' || $jam_keluar == ''){
                        $ket_uang_makan = '';
                        $uang = 0;
                    }else if($jam_masuk != "00:00:00" && $jam_keluar != "00:00:00"){
                        $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $jamkerja = $total_menit_telat;
						
                        if($jam_keluar != "" && $jamkerja > 240){
                            $ket_uang_makan = 'Rp. '.$uang_makan;
                            $uang = $uang_makan;
                        }
                        $ket_uang_makan = 'Rp. '.$uang_makan;
                        $uang = $uang_makan;    
                    }else{
                            $ket_uang_makan = ' ';
                            $uang = 0;
                        }
            }else{
                $ket_uang_makan = 'Rp. '.$uang_makan;
                $uang = $uang_makan;
                if($uang_makan == 0){
                $ket_uang_makan = '';
                $uang = 0;
            }
            }

            // if($pecaharray[2]=='National Day' ){
            //     $ket_uang_makan = '';
            //     $uang = 0;
            // }
	
	if($pecaharray[5] == 'H/LI' || $pecaharray[5] == 'H/LI/EO'){
            Fpdf::SetFillColor(176, 196, 222);
            }else{
            Fpdf::SetFillColor(255, 255, 255);
        }

        if($uang > 0){
            $tothari +=1;
        }
        $totaluangmakan = $totaluangmakan+$uang;
        Fpdf::Cell(4, $rowHeader, $no++,$borderColumn, 0, 'C');
        Fpdf::Cell(17, $rowHeader, (string)$key2,$borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(43, $rowHeader, $pecaharray[0], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(15, $rowHeader, $pecaharray[1], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(20, $rowHeader, $pecaharray[2], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(20, $rowHeader, $pecaharray[3], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(20, $rowHeader, $pecaharray[4], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(20, $rowHeader, $pecaharray[5], $borderColumn, 0, 'C', true);
        Fpdf::Cell(13, $rowHeader, $pecaharray[6], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(13, $rowHeader, $pecaharray[7], $borderColumn, 0, 'C', TRUE);
        Fpdf::Cell(16, $rowHeader, $ket_uang_makan, $borderColumn, 0, 'R', TRUE);
        Fpdf::Cell(75, $rowHeader, $pecaharray[9], $borderColumn, 0, 'L', TRUE);
        Fpdf::Ln();
            }
        Fpdf::Cell(139, $rowHeader, 'Total', $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, $tothari .' (hari)', $borderColumn, 0, 'C');
        Fpdf::Cell(13, $rowHeader, '', $borderColumn, 0, 'C');
        Fpdf::Cell(13, $rowHeader, '', $borderColumn, 0, 'C');
        Fpdf::Cell(16, $rowHeader, 'Rp. '.$totaluangmakan, $borderColumn, 0, 'R');
        Fpdf::Cell(75, $rowHeader, '', $borderColumn, 1, 'R');
        Fpdf::Ln();
        }

        
        Fpdf::Ln(5);

        Fpdf::Output('I','Summary Hitung Uang Makan'.date('d-m-Y',strtotime($tgl_periode_awal)).' s/d '.date('d-m-Y',strtotime($tgl_periode_akhir)).'.pdf'); 
        exit;

}

    public function exportsave(Request $request) {
    $rules = array(

      'file_txt' => 'required'                     );

    $messages = ['file_txt.required' => 'File excel harus diisi'];

    $validator = Validator::make($request->all(), $rules, $messages);
    if ($validator->fails()) {
        return redirect("/absensi/add")
        ->withErrors($validator)
        ->withInput();
    } else {
        // $path = $request->file('file_txt')->getRealPath();
        // $name = $request->file('file_txt')->getClientOriginalName();
        // $folder_to = public_path().'/uploads/'.$name;
        // $data = PHPExcel_IOFactory::load($folder_to);
        $rows = Excel::load($request->file('file_txt'))->get();
            foreach ($rows as $row => $value) {
                // echo $value . "<br>";
                foreach ($value as $c => $val) {
                    $dataarray[$value['employee_id']][$c] = $val;
                }
            }
        $qData  = new TemporaryAbsenModel;
        $qimport = $qData->importcsv($dataarray);
        if($qimport['status'] == "failed"){
        # --------------
            return redirect("/absensi/import")->withErrors(array_unique($qimport['response']))
                    ->withInput();
        }else{
            session()->flash("success_message", "Dataabsensi has been update");
            # ---------------
            return redirect("/absensi/export");
        }
    }
    }

    
public function laporan()
    {
         # code...
        $data["title"]         = "Laporan Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/laporan/export";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $collection            = [ (object)
                                    [
                                    'id' => '-',
                                    'name' => 'Semua'
                                    ]
                                ];
         $qKaryawan             = array_merge($collection,$qKaryawan);
         $qStatusAbsensi        = getStatusHadir();
         $qJenisLaporan         = getJenisLaporan();
         $qJenisCabang          = array_merge($collection,$qMaster->getJenisCabang());
         $qJenisDepartemen      = array_merge($collection,$qMaster->getSelectDepartemen());
         $parameter_system      = DB::table('m_profilesystem')->select('tgl_absenawal','tgl_absenakhir')->first();
         $tgl_absenawal         = date('d/m/Y', strtotime($parameter_system->tgl_absenawal));
         $tgl_absenakhir        = date('d/m/Y', strtotime($parameter_system->tgl_absenakhir));
       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_periode_awal", "label"=>"Tanggal Periode Awal", "mandatory"=>"yes","value"=>$tgl_absenawal, "first_selected"=>"yes"));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_periode_akhir", "label"=>"Tanggal Periode Akhir", "mandatory"=>"yes","value"=>$tgl_absenakhir, "first_selected"=>"yes"));
         $data["fields"][]      = form_select(array("name"=>"id_cabang", "label"=>"Nama Cabang", "mandatory"=>"yes", "source"=>$qJenisCabang));
         $data["fields"][]      = form_select(array("name"=>"id_departemen", "label"=>"Nama Departemen", "mandatory"=>"yes", "source"=>$qJenisDepartemen));
         $data["fields"][]      = form_select(array("name"=>"id_karyawan", "label"=>"Nama Karyawan", "mandatory"=>"yes", "source"=>$qKaryawan));
         $data["fields"][]      = form_select(array("name"=>"status_absensi", "label"=>"Status Kehadiran", "mandatory"=>"yes", "source"=>$qStatusAbsensi));
         $data["fields"][]      = form_radio(array("name"=>"jenis_laporan", "label"=>"Jenis", "mandatory"=>"yes", "source"=>$qJenisLaporan, "value"=>$qJenisLaporan));

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;laporan&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
    }
public function laporann(Request $request) {
        $rules = array(

          'tgl_periode_awal' => 'required' ,
          'tgl_periode_akhir' => 'required' ,
          'status_absensi' => 'required',
          'jenis_laporan' => 'required'                     );

        $messages = [
                    'tgl_periode_awal.required' => 'Pilih Periode Tanggal Masuk',
                    'tgl_periode_akhir.required' => 'Pilih Periode Tanggal Keluar',
                    'status_absensi.required' => 'Pilih Status Absensi',
                    'jenis_laporan.required' => 'Pilih Jenis Laporan',
                    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        // dd($request->all());
        if ($validator->fails()) {
            return redirect("/absensi/laporan")
            ->withErrors($validator)
            ->withInput();
        } else {
            $id_departemen = $request->id_departemen;
            $id_cabang = $request->id_cabang;
            $id_karyawan = $request->id_karyawan;
            $jenis_laporan = $request->jenis_laporan;
            $status_absensi = $request->status_absensi;
            $tgl_periode_awal = setYMD($request->tgl_periode_awal,'/');
            $tgl_periode_akhir = setYMD($request->tgl_periode_akhir,'/');
            if($status_absensi =='Semua' && $id_karyawan == '-' && $id_cabang != '-' && $id_departemen != '-'){
                // return redirect("/absensi/laporan")
                // ->withErrors(['Pilih Salah Satu Nama Karyawan'])
                // ->withInput();
                $whereCondition = [
                                    ['p_karyawan.id_cabang',$id_cabang],
                                    ['p_karyawan.id_departemen',$id_departemen],
                                  ];
            }else if($status_absensi =='Semua' && $id_karyawan != '-' && $id_cabang != '-' && $id_departemen != '-'){
                $whereCondition = [
                                    ['tm_dataabsensi.id_karyawan',$id_karyawan],
                                    ['p_karyawan.id_cabang',$id_cabang],
                                    ['p_karyawan.id_departemen',$id_departemen],
                                  ];
            }else if($status_absensi =='Semua' && $id_karyawan != '-' && $id_cabang != '-' && $id_departemen == '-'){
                $whereCondition = [
                                    ['tm_dataabsensi.id_karyawan',$id_karyawan],
                                    ['p_karyawan.id_cabang',$id_cabang],
                                    // ['p_karyawan.id_departemen',$id_departemen],
                                  ];
            }else if($status_absensi =='Semua' && $id_karyawan == '-' && $id_cabang != '-' && $id_departemen == '-'){
                $whereCondition = [
                                    ['p_karyawan.id_cabang',$id_cabang],
                                  ];
            }else if($status_absensi =='Semua' && $id_karyawan == '-' && $id_cabang == '-' && $id_departemen != '-'){
                $whereCondition = [
                                    ['p_karyawan.id_departemen',$id_departemen],
                                  ];
            }else if($status_absensi !='Semua' && $id_karyawan != '-' && $id_cabang != '-' && $id_departemen != '-'){
                if($status_absensi == 'H'){
                    $whereCondition = [
                        ['tm_dataabsensi.status_absensi', "LIKE", "%".$status_absensi. "%"],
                        ['tm_dataabsensi.id_karyawan',$id_karyawan],
                        ['p_karyawan.id_cabang',$id_cabang],
                        ['p_karyawan.id_departemen',$id_departemen],
                      ];   
                }else{
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                  ];
                }
                
            }else if($status_absensi !='Semua' && $id_karyawan != '-' && $id_cabang == '-' && $id_departemen == '-'){
                if($status_absensi == 'H'){
                    $whereCondition = [
                        ['tm_dataabsensi.status_absensi', "LIKE", "%".$status_absensi. "%"],
                        ['tm_dataabsensi.id_karyawan',$id_karyawan],
                      ];   
                }else{
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                    ['tm_dataabsensi.id_karyawan',$id_karyawan],
                                  ];
                }
                
            }else if($status_absensi !='Semua' && $id_karyawan != '-' && $id_cabang != '-' && $id_departemen == '-'){
                if($status_absensi == 'H'){
                    $whereCondition = [
                        ['tm_dataabsensi.status_absensi', "LIKE", "%".$status_absensi. "%"],
                        ['tm_dataabsensi.id_karyawan',$id_karyawan],
                        ['p_karyawan.id_cabang',$id_cabang],
                      ];   
                }else{
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                    ['tm_dataabsensi.id_karyawan',$id_karyawan],
                                    ['p_karyawan.id_cabang',$id_cabang],
                                  ];
                }
                
            }else if($status_absensi !='Semua' && $id_karyawan == '-' && $id_cabang == '-' && $id_departemen == '-'){
                if($status_absensi == 'H'){
                    $whereCondition = [
                        ['tm_dataabsensi.status_absensi', "LIKE", "%".$status_absensi. "%"],
                      ];   
                }else{
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                  ];
                }
            }else if($status_absensi !='Semua' && $id_karyawan == '-' && $id_cabang != '-' && $id_departemen != '-'){
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                    ['p_karyawan.id_cabang',$id_cabang],
                                    ['p_karyawan.id_departemen',$id_departemen],
                                  ];
            }else if($status_absensi !='Semua' && $id_karyawan == '-' && $id_cabang != '-' && $id_departemen == '-'){
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                    ['p_karyawan.id_cabang',$id_cabang],
                                  ];
            }else if($status_absensi !='Semua' && $id_karyawan == '-' && $id_cabang == '-' && $id_departemen == '-'){
                $whereCondition = [
                                    ['tm_dataabsensi.status_absensi',$status_absensi],
                                    ['p_karyawan.id_cabang',$id_cabang],
                                  ];
            }else if($status_absensi =='Semua' && $id_karyawan != '-' && $id_cabang == '-' && $id_departemen == '-'){
                $whereCondition = [
                                    ['tm_dataabsensi.id_karyawan',$id_karyawan],
                                  ];
            }else{
                $whereCondition = [
                                  ];
            }
            $dataDokumen = DB::table('tm_dataabsensi')
                        ->select("tm_dataabsensi.*")
                        ->leftjoin("p_karyawan","p_karyawan.id_karyawan","=","tm_dataabsensi.id_karyawan")
                        ->whereBetween('tgl_absensi', array($tgl_periode_awal, $tgl_periode_akhir))
                        ->where($whereCondition)
                        ->where("p_karyawan.aktif", 1)
                        // ->where("p_karyawan.tgl_keluar", null)
                        ->orderBy("p_karyawan.id_cabang", "ASC")
                        ->orderBy("tm_dataabsensi.id_karyawan", "ASC")
                            ->orderBy("tm_dataabsensi.tgl_absensi", "ASC")
                            ->orderBy("tm_dataabsensi.id_dataabsensi", "ASC")
                            // ->orderBy('tm_dataabsensi.tgl_absensi','asc')
                        ->get();
            
        $jenisabsen = DB::table('tm_jenisabsen')->select('inisial')->get();
        foreach ($jenisabsen as $keyjenisabsen => $valuejenisabsen) {
            # code...
            $stsabsensi[] = $valuejenisabsen->inisial;
        }

        $data =[];
        $nama_departemen = null;
        $nama_karyawan = null;
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }
        foreach ($dataDokumen as $key => $value) {
        $p_karyawan = DB::table('p_karyawan')->where([['id_karyawan', $value->id_karyawan]])->first();
        if($p_karyawan != null){
        $nama_karyawan = $p_karyawan->nama_karyawan;
        $m_departemen = DB::table('m_departemen')->where('id_departemen', $p_karyawan->id_departemen)->first();
        $nama_departemen = $m_departemen->nama_departemen;
        $m_cabangg = DB::table('m_cabang')->where('id_cabang', $p_karyawan->id_cabang)->first();
        $nilaihari = new DateTime($value->tgl_absensi);
        $no_nilaihari = $nilaihari->format('w');
        if($no_nilaihari == 0){
            $ket = "Dayoff";
        }else if($no_nilaihari == 5){
            $ket = "Jum'at";
        }else if($no_nilaihari == 6){
            $ket = "Sabtu";
        }else{
            $ket = "Normal";
        }
        
        if(in_array($value->tgl_absensi, $tgl_libur)){
            $ket = "National Day";
        }

        if (in_array($value->status_absensi, $stsabsensi)) {
        // if($value->status_absensi == $valuejenisabsen->inisial){
            $inisial = $value->status_absensi;
        }else{
            $inisial = '';
        }
        // if($value->status_absensi == 'H'){
        //     $inisial = "";
        // }else{
        //     $inisial = $value->status_absensi;
        // }

        if($m_cabangg != null){
        $dokumen = [
          // $asuransi,
          'nama_karyawan' => $nama_karyawan,
          'nama_departemen' => $nama_departemen,
          'id_divisi' => $m_cabangg->nama_cabang,
          'kethari' => $ket,
          'inisial' => $inisial,
        ];    
        }else{
            $dokumen = [
              // $asuransi,
              'nama_karyawan' => $nama_karyawan,
              'nama_departemen' => $nama_departemen,
              'id_divisi' => 'cc',
              'kethari' => $ket,
              'inisial' => $inisial,
            ];
        }
        
        foreach ($value as $key2 => $value2) {
          $y[$key2] = $value2;
        }
        $data []= array_merge($y,$dokumen);
        }
        }
            if($jenis_laporan ==  'i'){
                $this->repairdata();
                return redirect("/absensi/laporan");
            }else if($jenis_laporan == 'j'){
                $this->repairdataabsen();
            }else if($jenis_laporan == 'csv'){
                if($data == null){
                    return redirect("/absensi/laporan")
                    ->withErrors(['Report Absensi Untuk Periode Tanggal Tersebut Tidak Ada'])
                    ->withInput();
                }
                $laporancsv = $this->laporancsv($data,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi);
                return redirect("/absensi/laporan");
            }else if($jenis_laporan == 'pdf'){
                if($data == null){
                    return redirect("/absensi/laporan")
                    ->withErrors(['Report Absensi Untuk Periode Tanggal Tersebut Tidak Ada'])
                    ->withInput();
                }
                $laporancsv = $this->laporanpdf($data,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi);
                return redirect("/absensi/laporan");
            }else if($jenis_laporan == 'formatcsv'){
                if($data == null){
                    return redirect("/absensi/laporan")
                    ->withErrors(['Format Absensi Untuk Periode Tanggal Tersebut Tidak Ada'])
                    ->withInput();
                }
                $laporancsv = $this->formatcsv($data,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi);
                return redirect("/absensi/laporan");
            }else if($jenis_laporan == 'generate'){
                if($data == null){
                    return redirect("/absensi/laporan")
                    ->withErrors(['Format Absensi Untuk Periode Tanggal Tersebut Tidak Ada'])
                    ->withInput();
                }
                $laporancsv = $this->generate($data,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi);
                return redirect("/absensi/laporan");
            }else{
                if($data == null){
                    return redirect("/absensi/laporan")
                    ->withErrors(['Perhitungan Uang Makan Untuk Periode Tanggal Tersebut Tidak Ada'])
                    ->withInput();
                }    
                //$laporancsv = $this->formatpdf($data,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi);
                //return redirect("/absensi/laporan");
               $laporancsv = $this->formatpdf($data,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi);
               return $laporancsv;
            }
        }
    }
public function laporanpdf($dataDokumen,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi)
{
    foreach ($dataDokumen as $key => $value) {
        $pemiliknik[$value["nik"]][] = $value["nama_karyawan"]."&".$value["tgl_absensi"]."&".$value["kethari"]."&".$value["schedule_in"]."&".$value["schedule_out"]."&".$value["status_absensi"]."&".$value["jam_masuk"]."&".$value["jam_keluar"]."&".$value["status_terlambat"]."&".$value["ket_masuk"]."&".$value["ket_keluar"];
    }

    foreach ($pemiliknik as $key2 => $value2) {
    # code...
        $total = 0;
        $potong_makan = 0;
        $potong_gaji = 0;
        $potong_hari_makan = 0;
        // $total_potong_makan = 0;
        $uang_makan = 0;
        $uang_gaji = 0;
        $ket_absensi = '';
        $level_karyawan = DB::table("p_karyawan as a")
                ->select("a.id_karyawan","a.id_jabatan","b.id_jabatan as m_id_jabatan","b.id_level as m_id_level","c.id_level","c.uang_makan","a.nama_karyawan")
                ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                ->leftjoin("m_level as c","c.id_level","=","b.id_level")
                ->where("nik", $key2)
                ->first();
        if($level_karyawan != null){
            $uang_makan = $level_karyawan->uang_makan;
            // $uang_gaji = $level_karyawan->uang_makan;
        }
        foreach($value2 as $index => $valuerow){
            $list = explode('&',$valuerow);
            $nama_karyawan = $list[0];
        }
        $rowHeader = 5;
        $borderColumn = 1;
        Fpdf::AddPage();
        # ----------------
        Fpdf::SetLineWidth(0.3);
        Fpdf::SetFont("Arial", "B", 11);
        Fpdf::Ln(7);
        Fpdf::Cell(190, 5,'', 0, 0, 'C');
        Fpdf::Ln(10);
        Fpdf::SetFont('Arial', '', 7);
        Fpdf::Cell(15, 5, 'Nik : '.$key2.' Nama : '.$nama_karyawan, 0, 0, 'L');
        // Fpdf::Cell(15, 5, 'Tanggal : '.date('d-m-Y',strtotime($tgl_periode_awal)).' s/d '.date('d-m-Y',strtotime($tgl_periode_akhir)), 0, 0, 'L');
        Fpdf::Ln(5);
        Fpdf::Cell(5, 10, 'No.',$borderColumn, 0, 'C');
        Fpdf::Cell(20, 10, 'Tanggal', $borderColumn, 0, 'C');
        Fpdf::Cell(20, 10, 'Masuk', $borderColumn, 0, 'C');
        Fpdf::Cell(20, 10, 'Keluar', $borderColumn, 0, 'C');
        Fpdf::Cell(120, 5, 'Keterangan', $borderColumn, 1, 'C');
        // Fpdf::Cell(10, 0, '', 1, 0, 'C');
        Fpdf::Cell(65, 0, '', 0, 0, 'C');
        Fpdf::Cell(55, 5, 'Masuk', $borderColumn, 0, 'C');
        Fpdf::Cell(55, 5, 'Keluar', $borderColumn, 0, 'C');
        Fpdf::Cell(10, 5, '', $borderColumn, 0, 'C');
        Fpdf::Ln();
        Fpdf::SetFont('Arial', '', 6);
        
        $no = 1;
        $i = 0;
        $jml_ket_A = 0 ;
        $jml_ket_I = 0 ;
        $jml_ket_H = 0 ;
        $jml_ket_S = 0 ;
        $jml_ket_LI = 0 ;
        $jml_ket_EO = 0 ;
        $jml_ket_NCI = 0 ;
        $jml_ket_NCO = 0 ;
        $jml_ket_DL = 0 ;
        $jml_ket_IDC = 0 ;
        $jumlah_li_eo = 0 ;
        foreach ($value2 as $key3 => $value3) {
            # code...
            $pecaharray = explode('&',$value3);
            $ketarray = explode('/', $pecaharray[5]);
            $status_aktif_li = 0;
            $status_aktif_eo = 0;
            foreach ($ketarray as $indexket => $valueket) {
                # code...
            if($valueket == 'A'){
                $jml_ket_A = $jml_ket_A+1;
            }else if($valueket == 'H'){
                $jml_ket_H = $jml_ket_H+1;
            }else if($valueket == 'S'){
                $jml_ket_S = $jml_ket_S+1;
            }else if($valueket == 'LI'){
                $jml_ket_LI = $jml_ket_LI+1;
                $status_aktif_li = 1;
            }else if($valueket == 'EO'){
                $jml_ket_EO = $jml_ket_EO+1;
            }else if($valueket == 'NCI'){
                $jml_ket_NCI = $jml_ket_NCI+1;
            }else if($valueket == 'NCO'){
                $jml_ket_NCO = $jml_ket_NCO+1;
            }else if($valueket == 'DL'){
                $jml_ket_DL = $jml_ket_DL+1;
            }else if($valueket == 'IDC'){
                $jml_ket_IDC = $jml_ket_IDC+1;
            }
            }
            // if($pecaharray[1] == 1){
            // $potong_makan = $potong_makan+$uang_makan;
            // $potong_hari_makan = $potong_hari_makan+1;
            // }
            // if($pecaharray[2] == 1){
            //     $potong_gaji = $potong_gaji+1;
            // }
            // $total_potong_makan[$key] = $potong_makan;
            // $total_potong_gaji[$key] = $potong_gaji;
            // $total_hari_potong_makan[$key] = $potong_hari_makan;
            // if($pecaharray[5] == 'H/LI' || $pecaharray[5] == 'H/LI/EO'){
            //     // if(in_array("LI", $pecaharray2)){
            //     // if(($key2 == 0 && $pecaharray[1] == 1) || ($key2 == 1 && $pecaharray[1] == 1)){
            //     $total = $total+1;
            //     if($total > 2){
            //         $ket_absensi = '';
            //     }else{
            //         $ket_absensi = 'Rp. '.$uang_makan;
            //         if($uang_makan == 0){
            //             $ket_absensi = '';
            //         }
            //     }
            // }else if( $pecaharray[5] == 'A' || $pecaharray[5] == 'IDC'|| $pecaharray[5] == ''|| $pecaharray[5] == 'Waiting'){
            //   //   $inisial =['DL','CT','I','S','A','H','H/EO/NCI','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC'];
            //   // $val_stat_makan = [0,1,1,1,1,0,1,1,0,1,1,1,1,0,1];
            //   // $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
            //         $ket_absensi = ' ';
            // }else{
            //     $ket_absensi = 'Rp. '.$uang_makan;
            //     if($uang_makan == 0){
            //     $ket_absensi = '';
            //     }
            // }

            // if($pecaharray[2]=='National Day' || $pecaharray[2]=='Dayoff'){
            //     $ket_absensi = '';
            // }

            $kode_absensi = ['H/LI', 'H/LI/EO','H/EO', 'H/NCO','H/NCI/EO', 'H/LI/NCO','A','H/NCI'];
            $keterangan_absensi =  ['TERLAMBAT', 'TERLAMBAT DAN CEPAT PULANG','CEPAT PULANG', 'TIDAK CEK MASUK','TIDAK CEK PULANG',
                             'TIDAK CEK MASUK DAN CEPAT PULANG','TERLAMBAT DAN TIDAK CEK KELUAR', 'Alpha'
                            ];
             foreach($kode_absensi as $indexkey => $valuekey){
                if(in_array($pecaharray[5], $kode_absensi)){
                    $ket_absensi = $kode_absensi[$indexkey];
                }else{
                    $ket_absensi = '';
                }
             }
        Fpdf::Cell(5, $rowHeader, $no++,$borderColumn, 0, 'C');
        // Fpdf::Cell(17, $rowHeader, (string)$key2,$borderColumn, 0, 'C');
        // Fpdf::Cell(43, $rowHeader, $pecaharray[0], $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, date('d-m-Y', strtotime($pecaharray[1])), $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader,  $pecaharray[6], $borderColumn, 0, 'C');
        Fpdf::Cell(20, $rowHeader, $pecaharray[7], $borderColumn, 0, 'C');
        // Fpdf::Cell(20, $rowHeader, $pecaharray[2], $borderColumn, 0, 'C');
        // Fpdf::Cell(15, $rowHeader, (int)$pecaharray[6], $borderColumn, 0, 'C');
        // Fpdf::Cell(15, $rowHeader, $pecaharray[7], $borderColumn, 0, 'C');
        // Fpdf::Cell(20, $rowHeader, $pecaharray[5], $borderColumn, 0, 'C');
        Fpdf::Cell(55, $rowHeader, $pecaharray[9], $borderColumn, 0, 'R');
        Fpdf::Cell(55, $rowHeader, $pecaharray[10], $borderColumn, 0, 'R');
        Fpdf::Cell(10, $rowHeader, $pecaharray[5], $borderColumn, 0, 'C');
        Fpdf::Ln();
        }
        Fpdf::Ln(5);
        Fpdf::Cell(25, 5, 'Jumlah Kehadiran', $borderColumn, 0, 'C');
        Fpdf::Cell(25, 5, $jml_ket_H, $borderColumn, 0, 'C');
        Fpdf::Ln();
        Fpdf::Cell(25, 5, 'Jumlah Sakit', $borderColumn, 0, 'C');
        Fpdf::Cell(25, 5, $jml_ket_S, $borderColumn, 0, 'C');
        Fpdf::Ln();
        Fpdf::Cell(25, 5, 'Jumlah Alpa', $borderColumn, 0, 'C');
        Fpdf::Cell(25, 5, $jml_ket_A, $borderColumn, 0, 'C');
        Fpdf::Ln(60);
        }

        Fpdf::Output('I','Absensi'.date('d-m-Y',strtotime($tgl_periode_awal)).' s/d '.date('d-m-Y',strtotime($tgl_periode_akhir)).'.pdf'); 
        exit;
}
 function columnLetter($c){

    $c = intval($c);
    if ($c <= 0) return '';

    $letter = '';
             
    while($c != 0){
       $p = ($c - 1) % 26;
       $c = intval(($c - $p) / 26);
       $letter = chr(65 + $p) . $letter;
    }
    
    return $letter;
        
}

public function repairdata(){
    $tm_timeoff = DB::table("tm_timeoff as a")
                            ->select("a.*","b.id_karyawan","b.nama_karyawan","b.nik","c.id_departemen","c.id_grupabsen","d.*")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","b.id_departemen")
                            ->leftjoin("tm_grupabsen as d","d.id_grupabsen","=","c.id_grupabsen")
                            // ->whereBetween('a.tgl_awal', [$profile_system->tgl_absenawal, $profile_system->tgl_absenakhir])
                            ->where('a.tgl_awal', '<=', '2019-07-15')
                            ->get();

        $inisial =['DL','CT','I','S','A','H','H/NCI/EO','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC','IK'];
        $val_stat_makan = [0,1,1,1,1,0,1,1,0,1,1,1,1,0,1,1];
        $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0];
        $status_potong_gaji = 0;
        $status_potong_makan = 0;
        
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
            foreach ($hari_libur as $key => $value) {
                # code...
                $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }
        foreach ($tm_timeoff as $keytm_timeoff => $valuetm_timeoff) {
            $cekpengajuan = DB::table('tm_pengajuanabsen as a')->where('id_tm', $valuetm_timeoff->id_tm)->first();
            
            if($cekpengajuan == null ){
                $p_karyawan = DB::table('p_karyawan as a')
                ->where('id_karyawan', $valuetm_timeoff->id_karyawan)
                ->first();

            $cek2 = DB::table('p_karyawan')
                        ->join('tm_dataapproval','p_karyawan.id_jabatan','tm_dataapproval.id_jabatan')
                        ->select('p_karyawan.id_karyawan','tm_dataapproval.id_cabang_approval', 'tm_dataapproval.id_departemen_approval','tm_dataapproval.id_approvaller','tm_dataapproval.approvallevel','tm_dataapproval.id_jabatan',
                        'p_karyawan.id_cabang','p_karyawan.id_departemen','p_karyawan.id_jabatan')
                        ->where([['id_karyawan', $valuetm_timeoff->id_karyawan],['tm_dataapproval.id_cabang', $p_karyawan->id_cabang],['tm_dataapproval.id_departemen', $p_karyawan->id_departemen]])
                        ->get();
                    // dd($cek2);
                    if(!empty($cek2)){
                        foreach ($cek2 as $keycek => $valuecek) {
                        # code...
                            $approvaller = DB::table('p_karyawan')->select('id_karyawan')
                            ->where([
                                        ['id_cabang', $valuecek->id_cabang_approval],
                                        ['id_departemen', $valuecek->id_departemen_approval],
                                        ['id_jabatan', $valuecek->id_approvaller],
                                        ['aktif', 1]
                                    ])
                            ->first();
                            if($approvaller != null){
                                DB::table('tm_pengajuanabsen')->insert([
                                                    'id_tm'=> $valuetm_timeoff->id_tm,
                                                    'id_karyawan'=> $approvaller->id_karyawan,
                                                    'order'=> $valuecek->approvallevel,
                                                ]);
                            }
                        }    
                    }
            }   
        }

        $status_potong_gaji = 0;
        $status_potong_makan = 0;
        foreach ($tm_timeoff as $key => $value) {
            $rangedate = $this->getDatesFromRange2($value->tgl_awal,$value->tgl_akhir);
            $p_karyawan_baru = DB::table('p_karyawan')->where('id_karyawan', $value->id_karyawan)->select('sisacuti','tgl_batas_cuti')->first();
            foreach ($rangedate as $key2 => $value2) {
                # code...
            $hari = new DateTime($value2);
            $no_hari = $hari->format('w');
            $ceklibur = in_array($value2, $tgl_libur);

            $tm_dataabsensi = DB::table('tm_dataabsensi')->where([['id_karyawan', $value->id_karyawan],['tgl_absensi',$value2]])->first();
            if($tm_dataabsensi != null){
                if($no_hari != 0 && $ceklibur == false){
                        if($no_hari == 6){
                            $schedule_in = $value->jam_masuk2;
                            $schedule_out = $value->jam_keluar2;
                        }else{
                            $schedule_in = $value->jam_masuk;
                            $schedule_out = $value->jam_keluar;
                        }

                            $jammasuk = $tm_dataabsensi->jam_masuk;
                            $jamkeluar = $tm_dataabsensi->jam_keluar;

                            $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen',$value->id_status_karyawan)->first();
                            $nama_jenisabsen = $jenis_absen->inisial;

                        if($jammasuk != null && $jamkeluar == null){
                // $status_absensi = 'H/NCI';
                    if($jammasuk > $schedule_in ){
                        $status_absensi = 'H/LI/NCO';
                        $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if($jammasuk <= $schedule_in ){
                        $status_absensi = 'H/NCO';
                    }
                }else if($jammasuk == null && $jamkeluar != null){
                    // $status_absensi = 'H/NCI';
                    if($jamkeluar < $schedule_out ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($jamkeluar >= $schedule_out ){
                        $status_absensi = 'H/NCI';
                    }
                }else if($jammasuk != null && $jamkeluar != null){
                    if(($jammasuk != '00:00:00' || $jammasuk != '00:00') && ($jamkeluar == '00:00:00' || $jamkeluar == '00:00')){
                        if($jammasuk < $schedule_in ){
                            $status_absensi = 'H/NCO';
                            $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }else if($jammasuk > $schedule_in ){
                            $status_absensi = 'H/LI/NCO';
                        }

                    }else if(($jammasuk == '00:00:00' || $jammasuk == '00:00') && ($jamkeluar != '00:00:00' || $jamkeluar != '00:00')){
                        if($jamkeluar < $schedule_out ){
                            $status_absensi = 'H/NCI/EO';
                        }else if($jamkeluar >= $schedule_out ){
                            $status_absensi = 'H/NCI';
                        }
                    }else{
                        // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                        if($jammasuk <= $schedule_in && $jamkeluar < $schedule_out){
                            $status_absensi = 'H/EO';
                            $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }elseif($jammasuk <= $schedule_in && $jamkeluar >= $schedule_out){
                            $status_absensi = 'H';
                            $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }elseif($jammasuk > $schedule_in && $jamkeluar < $schedule_out){
                            $status_absensi = 'H/LI/EO';
                        }elseif($jammasuk > $schedule_in && $jamkeluar >= $schedule_out){
                            $status_absensi = 'H/LI';
                        }
                    }


                }else{
                    $status_absensi = $nama_jenisabsen;
                }
                
                if($value->status == 'Approve'){
                            foreach ($inisial as $keyinisial => $valueinisial) {
                                # code...
                                if($nama_jenisabsen == 'CT'){
                                    //jika tidak berulang
                                    if(!in_array($tm_dataabsensi->status_absensi, ['CT', 'IDC'])){
                                        $nilai_cuti = $p_karyawan_baru->sisacuti;
                                        $sisacuti = $nilai_cuti-1;
                                        DB::table('p_karyawan')
                                        ->where('id_karyawan', $value->id_karyawan)
                                        ->update([
                                                "sisacuti" => $sisacuti,
                                                ]);

                                        if($sisacuti < 0 ){
                                            DB::table('tm_dataabsensi')->where('id_dataabsensi', $tm_dataabsensi->id_dataabsensi)
                                                ->update([
                                                    'keterangan' => $value->komentar,
                                                    'schedule_in' => $schedule_in,
                                                    'schedule_out' => $schedule_out,
                                                    'status_absensi' => 'IDC',
                                                    'status_potong_makan' => 1,
                                                    'status_potong_gaji' => 1,
                                                  ]);
                                        }
                                    }
                                }else{
                                    if($nama_jenisabsen == $valueinisial){
                                        $status_potong_gaji = $val_stat_gaji[$keyinisial];
                                        $status_potong_makan = $val_stat_makan[$keyinisial];
                                        }
                                    
                                        DB::table('tm_dataabsensi')->where('id_dataabsensi', $tm_dataabsensi->id_dataabsensi)
                                        ->update([
                                            'keterangan' => $value->komentar,
                                            'schedule_in' => $schedule_in,
                                            'schedule_out' => $schedule_out,
                                            'status_absensi' => $nama_jenisabsen,
                                            'status_potong_makan' => $status_potong_makan,
                                            'status_potong_gaji' => $status_potong_gaji,
                                          ]);
                                    
                                }
                              }
                    
                    $tm_pengajuanabsen = DB::table('tm_pengajuanabsen')->where('id_tm', $value->id_tm)->first();
                    if($tm_pengajuanabsen != null){
                            DB::table('tm_pengajuanabsen')->where('id_tm', $tm_pengajuanabsen->id_tm)
                                                ->update([
                                                    'statusapproval' => 1,
                                                  ]);
                    }

                    }else if($value->status == 'Reject'){
                    DB::table('tm_dataabsensi')->where('id_dataabsensi', $tm_dataabsensi->id_dataabsensi)
                                                ->update([
                                                    'keterangan' => $value->komentar,
                                                    'status_absensi' => 'A',
                                                    'status_potong_makan' => 1,
                                                    'status_potong_gaji' => 0,
                                                  ]);
                        $tm_pengajuanabsen = DB::table('tm_pengajuanabsen')->where('id_tm', $value->id_tm)->first();
                        if($tm_pengajuanabsen != null){
                                DB::table('tm_pengajuanabsen')->where('id_tm', $tm_pengajuanabsen->id_tm)
                                                    ->update([
                                                        'statusapproval' => 0,
                                                      ]);
                        }
                    }else if($value->status == 'Waiting'){
                        DB::table('tm_dataabsensi')->where('id_dataabsensi', $tm_dataabsensi->id_dataabsensi)
                                                    ->update([
                                                        'keterangan' => $value->komentar,
                                                        'status_absensi' => 'Waiting',
                                                        'status_potong_makan' => 1,
                                                        'status_potong_gaji' => 0,
                                                      ]);
                    }else if($value->status == 'Pending'){
                        DB::table('tm_dataabsensi')->where('id_dataabsensi', $tm_dataabsensi->id_dataabsensi)
                                                    ->update([
                                                    'keterangan' => $value->komentar,
                                                    'status_absensi' => 'Pending',
                                                        'status_potong_makan' => 1,
                                                        'status_potong_gaji' => 0,
                                                      ]);
                    }else{
                        DB::table('tm_dataabsensi')->where('id_dataabsensi', $tm_dataabsensi->id_dataabsensi)
                                                ->update([
                                                    'keterangan' => $value->komentar,
                                                    'status_absensi' => 'Waiting',
                                                  ]);
                    }


                }
            }else{
                    // $pengajuan = DB::table('tm_pengajuanabsen')->select('id_tm')->where('id_tm',$value->id_tm)->first();
                $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen',$value->id_status_karyawan)->first();
                $status_absensi = $jenis_absen->inisial;

                if($no_hari != 0 && $ceklibur == false){
                        if($no_hari == 6){
                            $schedule_in = $value->jam_masuk2;
                            $schedule_out = $value->jam_keluar2;
                        }else{
                            $schedule_in = $value->jam_masuk;
                            $schedule_out = $value->jam_keluar;
                        }

                    // $status_potong_gaji = 1;
                    // $status_potong_makan = 0;
                    $id_karyawan = $value->id_karyawan;
                    $nik = $value->nik;
                    $userid = $value->user_id;
                    
                    if($value->status == 'Approve'){
                            foreach ($inisial as $keyinisial => $valueinisial) {
                                # code...
                                if($status_absensi == $valueinisial){
                                  $status_absensi = $status_absensi;
                                  $status_potong_gaji = $val_stat_gaji[$keyinisial];
                                  $status_potong_makan = $val_stat_makan[$keyinisial];
                                }
                              }
                        $tm_pengajuanabsen = DB::table('tm_pengajuanabsen')->where('id_tm', $value->id_tm)->first();
                        if($tm_pengajuanabsen != null){
                                DB::table('tm_pengajuanabsen')->where('id_tm', $tm_pengajuanabsen->id_tm)
                                                    ->update([
                                                        'statusapproval' => 1,
                                                      ]);
                        }

                    }else if($value->status == 'Reject'){
                    
                        $status_absensi = 'A';
                        $status_potong_makan = 1;
                        $status_potong_gaji = 0;

                        $tm_pengajuanabsen = DB::table('tm_pengajuanabsen')->where('id_tm', $value->id_tm)->first();
                        if($tm_pengajuanabsen != null){
                                DB::table('tm_pengajuanabsen')->where('id_tm', $tm_pengajuanabsen->id_tm)
                                                    ->update([
                                                        'keterangan' => $value->komentar,
                                                        'statusapproval' => 0,
                                                      ]);
                        }
                    }else if($value->status == 'Waiting' || $value->status == 'Pending'){
                    
                        $status_absensi = $value->status;
                        $status_potong_makan = 1;
                        $status_potong_gaji = 0;
                    }else{
                        $status_absensi = 'Waiting';
                        
                    }

                    DB::table('tm_dataabsensi')->insert([
                                                    'keterangan' => $value->komentar,
                                                    'id_karyawan'=> $id_karyawan,
                                                    'nik'=> $nik,
                                                    'tgl_absensi'=> $value2,
                                                    'schedule_in' => $schedule_in,
                                                    'schedule_out' => $schedule_out,
                                                    'status_absensi' => $status_absensi,
                                                    'status_potong_makan' => 1,
                                                    'status_potong_gaji' => 0,
                                                    'user_id' => $userid,
                                                  ]);

                }
            }
        }
        }
        // return redirect("/absensi/laporan");
}

public function repairdataabsen(){
    $tm_dataabsensi = DB::table("tm_dataabsensi as a")
                            ->select("a.*","a.jam_masuk as jammasuk","a.jam_keluar as jamkeluar","b.id_karyawan","b.nama_karyawan","b.nik","c.id_departemen","c.id_grupabsen","d.*")
                            ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
                            ->leftjoin("m_departemen as c","c.id_departemen","=","b.id_departemen")
                            ->leftjoin("tm_grupabsen as d","d.id_grupabsen","=","c.id_grupabsen")
                            ->get();

        $inisial =['DL','CT','I','S','A','H','H/NCI/EO','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC','IK'];
        $val_stat_makan = [0,1,1,1,1,0,1,1,0,1,1,1,1,0,1,1];
        $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0];
        $status_potong_gaji = 0;
        $status_potong_makan = 0;
        
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
            foreach ($hari_libur as $key => $value) {
                # code...
                $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }

        foreach ($tm_dataabsensi as $key => $value) {
            # code...
            // dd($value);
            $hari = new DateTime($value->tgl_absensi);
            $no_hari = $hari->format('w');
            $ceklibur = in_array($value->tgl_absensi, $tgl_libur);
            
            if($no_hari != 0 && $ceklibur == false){
                if($no_hari == 6){
                    $schedule_in = $value->jam_masuk2;
                    $schedule_out = $value->jam_keluar2;
                }else{
                    $schedule_in = $value->jam_masuk;
                    $schedule_out = $value->jam_keluar;
                }

                $jammasuk = $value->jammasuk;
                $jamkeluar = $value->jamkeluar;
                 // $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen',$value->id_status_karyawan)->first();
                $nama_jenisabsen = $value->status_absensi;   
                
                if($jammasuk != null && $jamkeluar == null){
                // $status_absensi = 'H/NCI';
                    if($jammasuk > $schedule_in ){
                        $status_absensi = 'H/LI/NCO';
                        $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if($jammasuk <= $schedule_in ){
                        $status_absensi = 'H/NCO';
                    }
                }else if($jammasuk == null && $jamkeluar != null){
                    // $status_absensi = 'H/NCI';
                    if($jamkeluar < $schedule_out ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($jamkeluar > $schedule_out ){
                        $status_absensi = 'H/NCI';
                    }
                }else if($jammasuk != null && $jamkeluar != null){
                    if(($jammasuk != '00:00:00' || $jammasuk != '00:00') && ($jamkeluar == '00:00:00' || $jamkeluar == '00:00')){
                        if($jammasuk < $schedule_in ){
                            $status_absensi = 'H/NCO';
                            $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }else if($jammasuk > $schedule_in ){
                            $status_absensi = 'H/LI/NCO';
                        }

                    }else if(($jammasuk == '00:00:00' || $jammasuk == '00:00') && ($jamkeluar != '00:00:00' || $jamkeluar != '00:00')){
                        if($jamkeluar < $schedule_out ){
                            $status_absensi = 'H/NCI/EO';
                        }else if($jamkeluar >= $schedule_out ){
                            $status_absensi = 'H/NCI';
                        }
                    }else{
                        // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                        if($jammasuk <= $schedule_in && $jamkeluar < $schedule_out){
                            $status_absensi = 'H/EO';
                            $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }elseif($jammasuk <= $schedule_in && $jamkeluar >= $schedule_out){
                            $status_absensi = 'H';
                            $selisih  = date_diff(date_create($jammasuk), date_create($schedule_in));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $menit_terlambat = $total_menit_telat;
                        }elseif($jammasuk > $schedule_in && $jamkeluar < $schedule_out){
                            $status_absensi = 'H/LI/EO';
                        }elseif($jammasuk > $schedule_in && $jamkeluar >= $schedule_out){
                            $status_absensi = 'H/LI';
                        }
                    }
                }else{
                    $status_absensi = $nama_jenisabsen;
                }

                foreach ($inisial as $keyinisial => $valueinisial) {
                # code...
                    if($nama_jenisabsen == $valueinisial){
                        $status_potong_gaji = $val_stat_gaji[$keyinisial];
                        $status_potong_makan = $val_stat_makan[$keyinisial];
                    }
                }
                
                DB::table('tm_dataabsensi')->where('id_dataabsensi', $value->id_dataabsensi)
                                                ->update([
                                                    'schedule_in' => $schedule_in,
                                                    'schedule_out' => $schedule_out,
                                                    'status_absensi' => $status_absensi,
                                                    'status_potong_makan' => $status_potong_makan,
                                                    'status_potong_gaji' => $status_potong_gaji,
                                                  ]);


            }
        }
        // return redirect("/absensi/laporan");
}

public function status_kehadiran(){
    if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                    $status_absensi = 'H';
            }else if($jam_masuk != null && $jam_keluar == null){
                // $status_absensi = 'H/NCI';
                if($jam_masuk > $schedule_in ){
                    $status_absensi = 'H/LI/NCO';
                    $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                    $nilai_jam = $selisih->h;
                    $nilai_menit = $selisih->i;
                    $total_menit_telat = $nilai_jam*60+$nilai_menit;
                    $menit_terlambat = $total_menit_telat;
                }else if($jam_masuk <= $schedule_in ){
                    $status_absensi = 'H/NCO';
                }
            }else if($jam_masuk == null && $jam_keluar != null){
                // $status_absensi = 'H/NCI';
                if($jam_keluar < $schedule_out ){
                    $status_absensi = 'H/NCI/EO';
                }else if($jam_keluar > $schedule_out ){
                    $status_absensi = 'H/NCI';
                }
            }else if($jam_masuk != null && $jam_keluar != null){
                if(($jam_masuk != '00:00:00' || $jam_masuk != '00:00') && ($jam_keluar == '00:00:00' || $jam_keluar == '00:00')){
                    if($jam_masuk < $schedule_in ){
                        $status_absensi = 'H/NCO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }else if($jam_masuk > $schedule_in ){
                        $status_absensi = 'H/LI/NCO';
                    }

                }else if(($jam_masuk == '00:00:00' || $jam_masuk == '00:00') && ($jam_keluar != '00:00:00' || $jam_keluar != '00:00')){
                    if($jam_keluar < $schedule_out ){
                        $status_absensi = 'H/NCI/EO';
                    }else if($jam_keluar >= $schedule_out ){
                        $status_absensi = 'H/NCI';
                    }
                }else{
                    // dd($jam_masuk <= $schedule_in,$jam_masuk ,$schedule_in);
                    if($jam_masuk <= $schedule_in && $jam_keluar < $schedule_out){
                        $status_absensi = 'H/EO';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }elseif($jam_masuk <= $schedule_in && $jam_keluar >= $schedule_out){
                        $status_absensi = 'H';
                        $selisih  = date_diff(date_create($jam_masuk), date_create($schedule_in));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $menit_terlambat = $total_menit_telat;
                    }elseif($jam_masuk > $schedule_in && $jam_keluar < $schedule_out){
                        $status_absensi = 'H/LI/EO';
                    }elseif($jam_masuk > $schedule_in && $jam_keluar >= $schedule_out){
                        $status_absensi = 'H/LI';
                    }
                }
            }

            return $status_absensi;
}
    public function laporancsv($dataDokumen,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi)
{
    # code...
    // $hurufaplhabet = $this->columnLetter(1);
    $result = array();
    $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
    foreach ($hari_libur as $key => $value) {
        # code...
        $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
    }
    foreach ($dataDokumen as $key => $value) {
    $i=0;
        foreach ($value as $key2 => $valuee) {
            $result[$key][$i] = $valuee;
      $i++;
      }
    }
    $grupid = $this->array_group_by($result, function($i){  return $i[2]; });
    $gruptgll = $this->array_group_by($result, function($i){  return $i[3]; });
    $daftartanggall = $this->getDatesFromRange($tgl_periode_awal, $tgl_periode_akhir);
    $no = 1;
    $jumlahuangmakan = 0;
    foreach ($grupid as $key => $value) {
        $gruptgl[$key] = $this->array_group_by($value, function($i){  return $i[3]; });
                $i = 0;
                $jml_ket_A = 0 ;
                $jml_ket_I = 0 ;
                $jml_ket_H = 0 ;
                $jml_ket_S = 0 ;
                $jml_ket_CT = 0 ;
                $jml_ket_LI = 0 ;
                $jml_ket_EO = 0 ;
                $jml_ket_NCI = 0 ;
                $jml_ket_NCO = 0 ;
                $jml_ket_DL = 0 ;
                $jml_ket_IDC = 0 ;
                $jumlah_li_eo = 0 ;
                        
                        foreach ($daftartanggall as $key2 => $value2) {
                    // $tgl3[] = $key2;
                            // $departement = $gruptgl[$key][$value2][0][11];
                        if(in_array($value2, array_keys($gruptgl[$key]))){
                            $tgl4 = $value2;
                            $nilaihari = new DateTime($value2);
                            $no_nilaihari = $nilaihari->format('w');
                            if($no_nilaihari == 0){
                            $ket4 = '';
                            }else{
                            $ket4 = $gruptgl[$key][$value2][0][8];
                            }

                            if(in_array($value2, $tgl_libur)){
                              $ket4='';  
                            }
                            // $needle = 'H';
                            // if(stristr($ket4, $needle)===FALSE){
                            //     dd('false');
                            // }else{
                            //     dd(stristr($ket4, $needle));
                            // }
                            // preg_replace(
                            $jeniskehadiran = array('H','LI','NCI','EO','NCO','A','I','DL','IDC','S','CT');
                            $ketarray = explode('/', $ket4);
                            $status_aktif_li = 0;
                            $status_aktif_eo = 0;
                            foreach ($ketarray as $indexket => $valueket) {
                                # code...
                            if($valueket == 'A'){
                                $jml_ket_A = $jml_ket_A+1;
                            }else if($valueket == 'H'){
                                $jml_ket_H = $jml_ket_H+1;
                            }else if($valueket == 'S'){
                                $jml_ket_S = $jml_ket_S+1;
                            }else if($valueket == 'LI'){
                                $jml_ket_LI = $jml_ket_LI+1;
                                $status_aktif_li = 1;
                            }else if($valueket == 'EO'){
                                $jml_ket_EO = $jml_ket_EO+1;
                            }else if($valueket == 'NCI'){
                                $jml_ket_NCI = $jml_ket_NCI+1;
                            }else if($valueket == 'NCO'){
                                $jml_ket_NCO = $jml_ket_NCO+1;
                            }else if($valueket == 'DL'){
                                $jml_ket_DL = $jml_ket_DL+1;
                            }else if($valueket == 'IDC'){
                                $jml_ket_IDC = $jml_ket_IDC+1;
                            }else if($valueket == 'CT'){
                                $jml_ket_CT = $jml_ket_CT+1;
                            }
                        }
                        if($status_aktif_li ==1 && $status_aktif_eo == 1){
                            $jml_ket_I = $jml_ket_I+1;
                        }else if($status_aktif_li ==1 && $status_aktif_eo == 0){
                            $jml_ket_I = $jml_ket_I+1;
                        }else if($status_aktif_li ==0 && $status_aktif_eo == 1){
                            $jml_ket_I = $jml_ket_I+1;
                        }
                            
                        }else{
                            $tgl4 = $value2;
                            $nilaihari = new DateTime($value2);
                            $no_nilaihari = $nilaihari->format('w');
                            $ket4 = "";
                            if($no_nilaihari == 0){
                            $ket4 = "/freeday";
                            }
                        }
                        // $cekdepar[] = $departement;
                        $tgl3[] =$tgl4;
                        $ket3[] =$ket4;
                        $jumlah_alpha =$jml_ket_A;
                        $jumlah_izin =$jml_ket_I;
                        $jumlah_sakit =$jml_ket_S;
                        $jumlah_cuti =$jml_ket_CT;
                        $jumlah_li =$jml_ket_LI;
                        $jumlah_eo =$jml_ket_EO;
                        $jumlah_dl =$jml_ket_DL;
                        $jumlah_idc =$jml_ket_IDC;
                        $jumlah_ct =$jml_ket_CT;
                        $jumlah_nci =$jml_ket_NCI;
                        $jumlah_nco =$jml_ket_NCO;
                        $jumlah_hadir =$jml_ket_H;
                        $jumlah_li_eo =$jumlah_li+$jumlah_eo;
                        // if(isset($gruptgl[$key][$value2][0][17])){
                        //     $jumlahuangmakan += $gruptgl[$key][$value2][0][17];
                        // }   
                        $i++;
                    }
                    // dd(array_filter(array_unique($ket6)));
        $tgl_ab[] = implode(',', $tgl3);
        $ket_ab[] = implode('&', $ket3);
	$value[0][20] = str_replace('&', 'dan', $value[0][20]);

        if($status_absensi == 'Semua'){
        $list_row[] = $no.'&'.$key.'&'.$value[0][20].'&'.$value[0][21].'&'.$value[0][22].'&'.implode('&', $ket3).'&'.$jumlah_hadir.'&'.$jumlah_alpha.'&'.$jumlah_dl.'&'.$jumlah_izin.'&'.$jumlah_sakit.'&'.$jumlah_ct.'&'.$jumlah_idc.'&'.''.'&'.''.'&'.$jumlah_li.'&'.$jumlah_eo.'&'.$jumlah_nci.'&'.$jumlah_nco;
        }else{
        $list_row[] = $no.'&'.$key.'&'.$value[0][20].'&'.$value[0][21].'&'.$value[0][22].'&'.implode('&', $ket3).'&'.$jumlah_hadir.'&'.$jumlah_alpha.'&'.$jumlah_li.'&'.$jumlah_eo.'&'.$jumlah_nci.'&'.$jumlah_nco;
        }
        // $nik[] = $key;
        $tgl3 = null;
        $ket3 = null;
        $no++;
    }
if(count($dataDokumen) > 0){
   
  // dd(count($dataDokumen));            
  $spreadsheet = new Spreadsheet();
  $sheet = $spreadsheet->getActiveSheet();
  // $spreadsheet->getActiveSheet()->mergeCells('A1:'.$namevariabel[$jumlahbaris-1].'1');
  
  foreach ($dataDokumen as $key2 => $valued) {
    $values[$key2] = [];
    foreach ($valued as $key => $value) {
      $values[$key2][] = $value;
    }
  }

  foreach ($list_row as $key => $value) {
      # code...
    $value_list = explode('&', $value);
    $value_row[] = $value_list;
  }
  $onFieldTitle = [
                    ['ATTENDANCE REPORT' ],
                    ['PT.'],
                    [],
                    [],
                    [],
                    [],
                    // ['Employee ID', 'Name', 'Departement', 'Branch', 'Umur', 'Tenor', 'Tanggal Awal', 'Tanggal AKhir', 'Plafon', 'Rate', 'Premi', 'Status Bayar'],
                  ];
  $endValues = array_merge($onFieldTitle, $value_row);
  $jumlahkolom = count($endValues);
  // $spreadsheet->getActiveSheet()->mergeCells('A1:AV1')->createTextRun('PhpSpreadsheet:');
  $sheet->fromArray($endValues);
  // dd($sheet, $endValues);
  $cell_st =[
             'font' =>['bold' => true],
             'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
             'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN]]
            ];

  $cell_st2 =[
                'font' => [
                    // 'bold' => true,
                ],
                // 'alignment' => [
                //     'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
                // ],
                'borders' => [
                    'allBorders' => [
                  'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                  'color' => ['argb' => '00000000'],
                  ],
                ],
                'fill' => [
                    // 'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    // 'rotation' => 90,
                    // 'startColor' => [
                    //     'argb' => 'FFA0A0A0',
                    // ],
                    // 'endColor' => [
                    //     'argb' => 'FFFFFFFF',
                    // ],
                ],
            ];

            
$cell_st3 =[
             'font' =>['bold' => true],
             'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
             'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]],
             'fill' => [
    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
    'rotation' => 90,
    'startColor' => [
        'argb' => 'ccffeb',
    ],
    'endColor' => [
        'argb' => 'FFFFFFFF',
    ],
],
            ];

$cell_st4 =[
             'font' =>['bold' => true],
             'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
             'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]],
             'fill' => [
    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
    'rotation' => 90,
    'startColor' => [
        'argb' => 'DCDCDC',
    ],
    'endColor' => [
        'argb' => 'FFFFFFFF',
    ],
],
            ];
  // ; getRight(); getTop(); getBottom()
  
  // $sheet->mergeCells('A4:A6');
  // $sheet->mergeCells('B4:B6');
  // $sheet->mergeCells('C4:C6');
  // $sheet->mergeCells('D4:D6');
  // $sheet->mergeCells('E4:E6');
  // $sheet->mergeCells('F4:AI4');
  // $sheet->mergeCells('AJ4:AJ6');
  // $sheet->mergeCells('AK4:AK6');
  // $sheet->mergeCells('AL4:AL6');
  // $sheet->mergeCells('AM4:AM6');
  // $sheet->mergeCells('AN4:AN6');
  // $sheet->mergeCells('AO4:AO6');
  // $sheet->mergeCells('AP4:AP6');
  // $sheet->mergeCells('F5:T5');
  // $sheet->mergeCells('U5:AI5');

  $value_alpabet = ['A4','B4','C4','D4','E4'];
  $value_alpabett = ['A4','B4','C4','D4','E4','F4','AJ4','AK4','AL4','AM4','AN4','AO4','AP4'];
  $value_field = ['No.','Employee ID', 'Name', 'Departement', 'Branch','Date'];
  if($status_absensi == 'Semua'){
    $value_fieldd = ['Total Present', 'Total Absence','Total Dinas Luar', 'Total IJIN (MASUK TELAT/PULANG CEPAT)','Total Sakit Dengan Surat Dokter','Total Cuti Tahunan','Total IJIN DI LUAR CUTI','Full Basic Sallary','Prorate','Total Late In','Total Early Out', 'Total No Check In', 'Total No Check Out'];
    $judul = 'Report Attendance';
    }else{
  $value_fieldd = ['Total Present', 'Total Absence', 'Total Late In', 'Total Early Out', 'Total No Check In', 'Total No Check Out'];
    if($status_absensi == 'H'){
        $namaabsen = 'Hadir';
    }else if($status_absensi == 'I'){
        $namaabsen = 'Izin';
    }else if($status_absensi == 'A'){
        $namaabsen = 'Alpha';
    }else if($status_absensi == 'S'){
        $namaabsen = 'Sakit';
    }else if($status_absensi == 'DL'){
        $namaabsen = 'Dinas Luar';
    }else if($status_absensi == 'CT'){
        $namaabsen = 'Cuti';
    }else if($status_absensi == 'IDC'){
        $namaabsen = 'Izin Di Luar Cuti';
    }else if($status_absensi == 'NCO'){
        $namaabsen = 'No Check Out';
    }else if($status_absensi == 'NCI'){
        $namaabsen = 'No Check In';
    }else if($status_absensi == 'EO'){
        $namaabsen = 'Early Out';
    }else if($status_absensi == 'LI'){
        $namaabsen = 'Late In';
    }
    $judul = 'Report '.$namaabsen;

    }
  for($i = 0; $i<count($value_alpabet); $i++) {
      # code...
    // $hurufaplhabet = $this->columnLetter($i+1);
    $sheet->setCellValue($value_alpabet[$i],$value_field[$i]);
  }

  $r = $this->getDatesFromRange($tgl_periode_awal, $tgl_periode_akhir);
  foreach ($r as $key => $value) {
      # code...
  $get_nilai_tangal[] = substr($value,8,2);
  $no_bulan = substr($value,5,2);
  $konv_nm_bln[] = getMonthName($no_bulan);
  }
  $jumlahbaris = count($value_field)+count($get_nilai_tangal)+count($value_fieldd);
  //  $j = 0;
  // for ($i='A'; $i !== 'DN'; $i++) { 
  //   if($j <= $jumlahbaris){
  //   $namevariabel[] = $i;
  //   $j++;
  //   }
  // }
  $value_alpabet_month = ['F5','U5'];
  $nm_bulan = $konv_nm_bln;
  $jumlahkolombulan = array_count_values($nm_bulan);
  $noindexkolombulan = array_unique($nm_bulan);
  // for($i = 0; $i<count($value_alpabet_month); $i++) {
  //     # code...
  //   $sheet->setCellValue($value_alpabet_month[$i],$nm_bulan[$i]);
  // }
  $jumlahbaris2 = $jumlahbaris+1;
  for ($i=1; $i <$jumlahbaris2 ; $i++) { 
      # code...
    $hurufaplhabet = $this->columnLetter($i);
    $sheet->getColumnDimension($hurufaplhabet)->setAutoSize(true);
  }
  // foreach ($jumlahbaris2 as $width) {
  //   $hurufaplhabet = $this->columnLetter($j);
  //   $sheet->getColumnDimension($hurufaplhabet)->setAutoSize(true);
  // }
  $jumlahpertama = count($value_field);
  $jumlahkedua = count($get_nilai_tangal);
  $jumlahketiga = count($value_field)+count($get_nilai_tangal);
  $jumlahkeempat = count($value_field)+count($get_nilai_tangal)+count($value_fieldd);
  for ($i=0; $i < $jumlahpertama ; $i++) { 
    
    if($i == 5){
    $hurufaplhabet = $this->columnLetter(count($value_field)+count($get_nilai_tangal)-1);
$sheet->mergeCells('F4:'.$hurufaplhabet.'4')->setCellValue('F4',$value_field[$i])->getStyle('F4:'.$hurufaplhabet.'4')->applyFromArray($cell_st3);
    }
    else{
    $hurufaplhabet2 = $this->columnLetter($i+1);
    $sheet->mergeCells($hurufaplhabet2.'4:'.$hurufaplhabet2.'6')->setCellValue($hurufaplhabet2.'4',$value_field[$i])->getStyle($hurufaplhabet2.'4:'.$hurufaplhabet2.'6')->applyFromArray($cell_st3);    
    }
  }
  $b=0;
for ($jumlahpertama; $jumlahpertama < $jumlahketiga  ; $jumlahpertama++) { 
    $hurufaplhabet = $this->columnLetter($jumlahpertama);
    $sheet->setCellValue($hurufaplhabet.'6',$get_nilai_tangal[$b])->getStyle($hurufaplhabet.'6',$get_nilai_tangal[$b])->applyFromArray($cell_st3);
    $b++;

}
$gh = count($value_field)-1;
foreach ($noindexkolombulan as $key => $value) {
    # code...
    // $yui[] = $namevariabel[$key+count($value_field)].'5:'.$namevariabel[$jumlahkolombulan[$value]+count($value_field)-1].'5';
    $hurufaplhabet = $this->columnLetter($key+count($value_field));
    $gh = $gh+$jumlahkolombulan[$value];
    $hurufaplhabet2 = $this->columnLetter($gh);
    $sheet
    ->setCellValue($hurufaplhabet.'5',$value)
    ->mergeCells($hurufaplhabet.'5:'.$hurufaplhabet2.'5')->getStyle($hurufaplhabet.'5:'.$hurufaplhabet2.'5')->applyFromArray($cell_st3);
    // $sheet->setCellValue($namevariabel[$key+5+1].'5',$nm_bulan[$b]);
    // $v[] = $namevariabel[$gh];
    // $vv[] = $namevariabel[$key+count($value_field)-1];
}
    // dd($vv, $v);
// dd(count($value_field), $jumlahkedua, $namevariabel);
$c = 0;
for ($jumlahketiga; $jumlahketiga < $jumlahkeempat  ; $jumlahketiga++) { 
    $hurufaplhabet = $this->columnLetter($jumlahketiga);
    $sheet->mergeCells($hurufaplhabet.'4:'.$hurufaplhabet.'6')->setCellValue($hurufaplhabet.'4',$value_fieldd[$c])->getStyle($hurufaplhabet.'4:'.$hurufaplhabet.'6')->applyFromArray($cell_st3);
    $c++;
}

$indexm = 7;
for ($m=6; $m <count($endValues) ; $m++) { 
    # code...
    for ($n=0; $n <count($endValues[$m]) ; $n++) { 
        # code...
    if(stristr($endValues[$m][$n], '/freeday')===FALSE){
    // preg_replace(,);
        $hurufaplhabet = $this->columnLetter($n+1);
        $sheet->setCellValue($hurufaplhabet.$indexm, $endValues[$m][$n])->getStyle($hurufaplhabet.$indexm.':'.$hurufaplhabet.$indexm)->applyFromArray($cell_st2);
    }else{
        $hurufaplhabet = $this->columnLetter($n+1);
        $endValues[$m][$n] = str_replace('/freeday','', $endValues[$m][$n]);
        $sheet->setCellValue($hurufaplhabet.$indexm, $endValues[$m][$n])->getStyle($hurufaplhabet.$indexm.':'.$hurufaplhabet.$indexm)->applyFromArray($cell_st4);
    }
    }
$indexm++;
}
//       $sheet->setCellValue('A1', 'Firstname:');
// $sheet->setCellValue('A2', 'Lastname:');
// $sheet->setCellValue('B1', 'Maarten');
// $sheet->setCellValue('B2', 'Balliauw');
  // // $sheet->mergeCells('A1:'.$namevariabel[$jumlahbaris-1].'1');
  // $sheet->mergeCells('AJ4:AJ6');
// dd('F4:'.$namevariabel[count($value_field)+count($get_nilai_tangal)-1].'4')->getStyle('F4:'.$namevariabel[count($value_field)+count($get_nilai_tangal)-1].'4');
  $hurufaplhabet = $this->columnLetter($jumlahbaris-1);
  $hurufaplhabet2 = $this->columnLetter($jumlahbaris);
  $sheet->mergeCells('A1:'.$hurufaplhabet.'1')->getStyle('A1:'.$hurufaplhabet2.'1')->applyFromArray($cell_st);
  $sheet->mergeCells('A2:'.$hurufaplhabet.'2')->getStyle('A1:'.$hurufaplhabet2.'2')->applyFromArray($cell_st);
  $sheet->mergeCells('A3:'.$hurufaplhabet.'3')->getStyle('A3:'.$hurufaplhabet2.'3');
  $sheet->getStyle('A1:'.$hurufaplhabet.$jumlahkolom)->applyFromArray($cell_st2);
  $sheet->setTitle($judul);

  $writer = new Xlsx($spreadsheet);
  $name_file = $judul.'.xlsx';
  // $path = storage_path('Laporan\\'.$name_file);
  $file_url       = "collection/temp";
  $path = storage_path().'/app/'.$file_url.'/'.$name_file;
  $contents = is_dir($path);
  // $headers = array('Content-Type' => File::mimeType($path));
  // dd($path.'/'.$name_file,$contents);
  $writer->save($path);
  $filename   = str_replace("@", "/", $path);
    # ---------------
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=".$name_file);
    header("Content-Type: application/xlsx");
    header("Content-Transfer-Encoding: binary");
    # ---------------
    require "$filename";
    # ---------------
    exit;

  // return response()->download(storage_path().'/app/'.$file_url.'/'.$name_file, $name_file, $headers);

}
}
    

    public function formatcsv($dataDokumen,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi)
    {
        # code...
        $result = array();
        foreach ($dataDokumen as $key => $value) {
        $i=0;
            foreach ($value as $key2 => $valuee) {
                $result[$key][$i] = $valuee;
          $i++;
          }
        }
        $grupid = $this->array_group_by($result, function($i){  return $i[2]; });
        $gruptgll = $this->array_group_by($result, function($i){  return $i[3]; });
        $daftartanggall = $this->getDatesFromRange($tgl_periode_awal, $tgl_periode_akhir);
        $no = 1;
        foreach ($grupid as $key => $value) {
            // if($status_absensi == 'Semua'){
            // $list_row[] = $no.'&'.$key.'&'.$value[$key];
            // }else{
            // $list_row[] = $no.'&'.$key;
            // }
            
            // $no++;

            foreach ($value as $key2 => $value2) {
                # code...
                if($status_absensi == 'Semua'){
                    $list_row[] = $no.'&'.$value2[2].'&'.$value2[19].'&'.$value2[3].'&'.$value2[22].'&'.$value2[4].'&'.$value2[5].'&'.$value2[8].'&'.$value2[11].'&'.$value2[6].'&'.$value2[7].'&'.''.'&'.''.'&'.''.'&'.''.'&'.''.'&'.''.'&'.$value2[21];
                }else{
                    $list_row[] = $no.'&'.$value2[2].'&'.$value2[19].'&'.$value2[3].'&'.$value2[22].'&'.$value2[4].'&'.$value2[5].'&'.$value2[8].'&'.$value2[11].'&'.$value2[6].'&'.$value2[7].'&'.''.'&'.''.'&'.''.'&'.''.'&'.''.'&'.''.'&'.$value2[21];
                }
                $no++;
            }
        }
    if(count($dataDokumen) > 0){
       
      // dd(count($dataDokumen));            
      $spreadsheet = new Spreadsheet();
      $sheet = $spreadsheet->getActiveSheet();
      // $spreadsheet->getActiveSheet()->mergeCells('A1:'.$namevariabel[$jumlahbaris-1].'1');
      
      foreach ($dataDokumen as $key2 => $valued) {
        $values[$key2] = [];
        foreach ($valued as $key => $value) {
          $values[$key2][] = $value;
        }
      }

      foreach ($list_row as $key => $value) {
          # code...
        $value_list = explode('&', $value);
        $value_row[] = $value_list;
      }
      $jumlahkolom = count($value_row);
      $value_field[] = ['No','Employee ID','Full Name','Date*','Shift','Schedule In','Schedule Out', 'Attendance Code', 'Information', 'Check In','Check Out','Overtime Check In', 'Overtime Check Out', 'Overtime Before', 'Overtime After', 'Overtime Break Before', 'Overtime Break After', 'Time Off Code', 'Time Off Halfday','Time Off Schedule In', 'Time Off Schedule Out'];
      if($status_absensi == 'Semua'){
        $judul = 'Format Upload Attendance';
        }else{
      $value_fieldd = ['Total Present', 'Total Absence', 'Total Late In', 'Total Early Out', 'Total No Check In', 'Total No Check Out'];
        if($status_absensi == 'H'){
            $namaabsen = 'Hadir';
        }else if($status_absensi == 'I'){
            $namaabsen = 'Izin';
        }else if($status_absensi == 'A'){
            $namaabsen = 'Alpha';
        }else if($status_absensi == 'S'){
            $namaabsen = 'Sakit';
        }else if($status_absensi == 'DL'){
            $namaabsen = 'Dinas Luar';
        }else if($status_absensi == 'CT'){
            $namaabsen = 'Cuti';
        }else if($status_absensi == 'IDC'){
            $namaabsen = 'Izin Di Luar Cuti';
        }else if($status_absensi == 'NCO'){
            $namaabsen = 'No Check Out';
        }else if($status_absensi == 'NCI'){
            $namaabsen = 'No Check In';
        }else if($status_absensi == 'EO'){
            $namaabsen = 'Early Out';
        }else if($status_absensi == 'LI'){
            $namaabsen = 'Late In';
        }else if($status_absensi == 'IK'){
            $namaabsen = 'Izin Khusus';
        }else{
            $namaabsen = '';
        }
        $judul = 'Format Upload '.$namaabsen;

        }
      $r = $this->getDatesFromRange($tgl_periode_awal, $tgl_periode_akhir);
      $header = $value_field;
      $endValues = array_merge($header, $value_row);
      $sheet->fromArray($endValues);
      // $nameAlphabet = ['A','B'];
      // $start = 2;
      // foreach ($value_row as $key => $value) {
      //     # code...
      //   foreach ($value as $key2 => $value2) {
      //       # code...
      //       $sheet->setCellValue($nameAlphabet[$key2].$start,$value2);
      //   }
      //   $start++;
      // }
      
      // $jumlahbaris = count($r)+count($nameAlphabet);
      // //  $j = 0;
      // // for ($i='A'; $i !== 'DN'; $i++) { 
      // //   if($j <= $jumlahbaris){
      // //   $namevariabel[] = $i;
      // //   $j++;
      // //   }
      // // }
      for ($i=1; $i <= count($header[0]); $i++) { 
          # code...
        $hurufaplhabet = $this->columnLetter($i);
        $sheet->getColumnDimension($hurufaplhabet)->setAutoSize(true);
      }

   
      $sheet->setTitle($judul);

      $writer = new Xlsx($spreadsheet);
      $name_file = $judul.'.xlsx';
      // $path = storage_path('Laporan\\'.$name_file);
      //$file_url       = "collection/temp";
      //$path = storage_path().'/app/'.$file_url.'/'.$name_file;
      //$contents = is_dir($path);
      // $headers = array('Content-Type' => File::mimeType($path));
      // dd($path.'/'.$name_file,$contents);
      //$writer->save($path);
      //$filename   = str_replace("@", "/", $path);
        # ---------------
      //  header("Cache-Control: public");
       // header("Content-Description: File Transfer");
       // header("Content-Disposition: attachment; filename=".$name_file);
       // header("Content-Type: application/xlsx");
       // header("Content-Transfer-Encoding: binary");
        # ---------------
       // require "$filename";
        # ---------------
       // exit;

      // return response()->download(storage_path().'/app/'.$file_url.'/'.$name_file, $name_file, $headers);
	//$judul    = "KryKeluarExport";  
        //$name_file = $judul.'.xlsx';
      	header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $name_file .'"'); 
        header('Cache-Control: max-age=0');
        
        $writer->save('php://output');
    }
}


public function getDatesFromRange($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
}

    public function convert() {
        $data["title"]         = "Add Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/convert/save";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $qStatusAbsensi        = getStatusHadir();

       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
         $data["fields"][]      = form_text(array("name"=>"url", "label"=>"Url", "mandatory"=>"yes"));
        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
     }

	public function generate($dataDokumen,$tgl_periode_awal, $tgl_periode_akhir, $status_absensi){
		
    foreach ($dataDokumen as $key => $value) {
		$pemiliknik[$value["nik"]][] = $value["nama_karyawan"]."&".$value["tgl_absensi"]."&".$value["kethari"]."&".$value["schedule_in"]."&".$value["schedule_out"]."&".$value["status_absensi"]."&".$value["jam_masuk"]."&".$value["jam_keluar"]."&".$value["status_terlambat"]."&".$value["id_dataabsensi"];
	}
    $rowHeader = 5;
    $borderColumn = 1;

    foreach ($pemiliknik as $key2 => $value2) {
        # code...
        $total = 0;
        $potong_makan = 0;
        $potong_gaji = 0;
        $potong_hari_makan = 0;
        // $total_potong_makan = 0;
        $uang_makan = 0;
        $uang_gaji = 0;
        $ket_uang_makan = '';
        $no = 1;
        $totaluangmakan = 0;
        
        $level_karyawan = DB::table("p_karyawan as a")
                ->select("a.id_karyawan","a.id_jabatan","b.id_jabatan as m_id_jabatan","b.id_level as m_id_level","c.id_level","a.tunj_makan as uang_makan","a.id_cabang")
                ->leftjoin("m_jabatan as b","b.id_jabatan","=","a.id_jabatan")
                ->leftjoin("m_level as c","c.id_level","=","b.id_level")
                ->where("nik", $key2)
                ->first();
        $p_karyawan_ =  DB::table("p_karyawan as a")
                ->where("nik", $key2)
                ->first();
        $batas_terlambat = 3;
        if($level_karyawan != null){
            $uang_makan = $level_karyawan->uang_makan;
            if($level_karyawan->id_cabang == 3 || $level_karyawan->id_cabang == 4){
                $batas_terlambat = 3;
            }
            // $umr = $level_karyawan->umr;
            // $uang_gaji = $level_karyawan->uang_makan;
        }
        $uang_makan = isset($p_karyawan_->tunj_makan) ? $p_karyawan_->tunj_makan: 0;

        foreach ($value2 as $key3 => $value3) {
            # code...
            $pecaharray = explode('&',$value3);
            $pecaharray2 = explode('/',$pecaharray[0]);
            // if($pecaharray[1] == 1){
            // $potong_makan = $potong_makan+$uang_makan;
            // $potong_hari_makan = $potong_hari_makan+1;
            // }
            // if($pecaharray[2] == 1){
            //     $potong_gaji = $potong_gaji+1;
            // }
            // $total_potong_makan[$key] = $potong_makan;
            // $total_potong_gaji[$key] = $potong_gaji;
            // $total_hari_potong_makan[$key] = $potong_hari_makan;
            if($pecaharray[5] == 'H/LI' || $pecaharray[5] == 'H/LI/EO'){
                // if(in_array("LI", $pecaharray2)){
                // if(($key2 == 0 && $pecaharray[1] == 1) || ($key2 == 1 && $pecaharray[1] == 1)){
                $total = $total+1;
                if($total > $batas_terlambat){
                    $ket_uang_makan = 0;
                }else{
                    $ket_uang_makan =  $uang_makan;
                    if($uang_makan == 0){
                        $ket_uang_makan = 0;
                    }
                }
				if($pecaharray[5] == 'H/LI/EO'){
                        $jam_masuk = $pecaharray[6];
                        $jam_keluar = $pecaharray[7];
                        if($total > 2){
                            $ket_uang_makan = 0;
                        }else{
                            $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                            $nilai_jam = $selisih->h;
                            $nilai_menit = $selisih->i;
                            $total_menit_telat = $nilai_jam*60+$nilai_menit;
                            $jamkerja = $total_menit_telat;
                            if($jamkerja > 240){
                                $ket_uang_makan = $uang_makan;
                            }else{
                                $ket_uang_makan = 0;
                            }
                        }
                }
            }else if($pecaharray[5] == 'CT' || $pecaharray[5] == 'S' || $pecaharray[5] == 'ST' || $pecaharray[5] == 'A' || $pecaharray[5] == 'H/EO/NCO' || $pecaharray[5] == 'H/LI/NCO' || $pecaharray[5] == 'H/NCI/EO'|| $pecaharray[5] == 'H/NCI' || $pecaharray[5] == 'H/NCO' || $pecaharray[5] == 'IDC'|| $pecaharray[5] == ''|| $pecaharray[5] == 'Waiting' || $pecaharray[5] =='IK'|| $pecaharray[5] =='OD'){
              //   $inisial =['DL','CT','I','S','A','H','H/EO/NCI','H/NCI','H/EO','H/LI/EO','H/LI','H/NCO','H/LI/NCO','Waiting','IDC'];
              // $val_stat_makan = [0,1,1,1,1,0,1,1,0,1,1,1,1,0,1];
              // $val_stat_gaji =  [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1];
                    $ket_uang_makan = 0;
            }else if($pecaharray[5] == 'KR'){
              if($pecaharray[1] > '2020-05-01'){
                $ket_uang_makan = 0;
              }else{
                $ket_uang_makan = $uang_makan;
              }
            }else if($pecaharray[5] == 'I'){
                    $jam_masuk = $pecaharray[6];
                    $jam_keluar = $pecaharray[7];
					
					// dd($jam_masuk != null || $jam_masuk != "00:00:00" && $jam_keluar != "00:00:00" || $jam_keluar != null,$jam_masuk, $jam_keluar);
                    if($jam_masuk == null && $jam_keluar == null){
                        $ket_uang_makan = 0;
                    }else if($jam_masuk == '' || $jam_keluar == ''){
                        $ket_uang_makan = 0;
                    }else if($jam_masuk != "00:00:00" && $jam_keluar != "00:00:00"){
                        $selisih  = date_diff(date_create($jam_masuk), date_create($jam_keluar));
                        $nilai_jam = $selisih->h;
                        $nilai_menit = $selisih->i;
                        $total_menit_telat = $nilai_jam*60+$nilai_menit;
                        $jamkerja = $total_menit_telat;
						
                        if($jam_masuk == null || $jam_masuk == ''){
                        $ket_uang_makan = 0;
                        }else if($jam_keluar != "" && $jamkerja > 240){
                        $ket_uang_makan = $uang_makan;
                          if($uang_makan > 0){
                            DB::table('tm_dataabsensi')->where('id_dataabsensi', $pecaharray[9])->update(["status_potong_makan"=> 0]);
                          }
                        }
                    }else{
                            $ket_uang_makan = 0;
                    }
					
                    
            }else{
                $ket_uang_makan = $uang_makan;
                if($uang_makan == 0){
                $ket_uang_makan = 0;
            }
            }

            // if($pecaharray[2]=='National Day' ){
            //     $ket_uang_makan = 0;
            // }
			
			
            DB::table('tm_dataabsensi')->where('id_dataabsensi', $pecaharray[9])->update(["uang_makan"=> $ket_uang_makan]);
		
        }
    }
	   DB::table('tm_dataabsensi')->where('status_absensi', 'IDC')->update(["status_potong_gaji"=> 1]);
    DB::table('tm_dataabsensi')->where('status_absensi', '!=','IDC')->update(["status_potong_gaji"=> 0]);
    DB::table('tm_dataabsensi')->where('status_absensi', 'KR')->update(["status_potong_makan"=> 0]);
    DB::table('tm_dataabsensi')->where('status_absensi', 'H')->update(["status_potong_makan"=> 0,"status_potong_gaji"=> 0]);
}
	
     public function convertxmlto(Request $request) {
        $rules = array(

          'url' => 'required'                     );

        $messages = ['url.required' => 'link url harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/absensi/xml/convert")
            ->withErrors($validator)
            ->withInput();
        } else {
            $options = [
                'trace' => true,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $credentials = [
                'username' => 'username',
                'password' => 'password'
            ];

            $header = new SoapHeader($NAMESPACE, 'AuthentificationInfo', $credentials);

            $client = new SoapClient($WSDL, $options); // null for non-wsdl mode

            $client->__setSoapHeaders($header);

            $params = [
                // Your parameters
            ];

            $result = $client->GetResult($params);
            // 'GetResult' being the name of the soap method

            if (is_soap_fault($result)) {
                error_log("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})");
            }
            // dd($result);
        }
    }

    public function chat() {
        $data["title"]         = "Add Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/absensi/chat/send";
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qKaryawan             = $qMaster->getSelectKaryawan();
         $qStatusAbsensi        = getStatusHadir();

       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
         $data["fields"][]      = form_text(array("name"=>"text-chat", "label"=>"Jam Keluar (hh:mm)", "mandatory"=>"yes"));
         
        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         
         return view("default.chat", $data);
     }
     
 }
