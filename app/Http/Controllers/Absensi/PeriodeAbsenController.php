<?php

namespace App\Http\Controllers\Absensi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use Fpdf;
use App\User;
use App\Model\MenuModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\PeriodeAbsenModel;

class PeriodeAbsenController extends Controller
{
    public function __construct(Request $request)
     {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index()
    {
        $data["title"]        = "Proses Absen";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/periodeabsen/update";
        /* ----------
         Prosesgaji
        ----------------------- */
        $qMaster              = new MasterModel;
        $qPeriodeabsen        = new PeriodeAbsenModel;
        /* ----------
         Source
        ----------------------- */

          $data["tabs"]          = array(
                                 array("label"=>"Proses Absen", "url"=>"/periodeabsen/index/", "active"=>"active"), 
                                );
                                 
        //$idProsesgaji             = explode("&", $id); 
        $qPeriodeabsen           = $qPeriodeabsen->getProfile()->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]    = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      =  form_datepicker(array("name"=>"Tgl Absen awal", "label"=>"", "mandatory"=>"","value"=> displayDMY($qPeriodeabsen->tgl_absenawal,"/")));
        $data["fields"][]      =  form_datepicker(array("name"=>"Tgl Absen akhir", "label"=>"", "mandatory"=>"","value"=> displayDMY($qPeriodeabsen->tgl_absenakhir,"/")));
        // $data["fields"][]      = form_text(array("name"=>"Tgl Absen akhir", "label"=>"", "value"=>$qPeriodeabsen->tgl_absenakhir));
                  


     # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
        
    }

    

   
    public function update(Request $request)
    {
        
        $rules = array(
                    'Tgl_Absen_awal' => 'required|',
                    'Tgl_Absen_akhir' => 'required|',
                    
                                          
        );

        $messages = [
                    'Tgl_Absen_awal.required' => 'Tanggal awal harus diisi',
                    'Tgl_Absen_akhir.required' => 'Tanggal awal harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/periodeabsen/index/" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qProsesgaji      = new PeriodeAbsenModel;
            # ---------------
            $qProsesgaji->updateData($request);
            # ---------------
            session()->flash("success_message", "Proses Absensi has been updated");
            # ---------------
           return redirect("/periodeabsen/index/");
        }
     }
}
