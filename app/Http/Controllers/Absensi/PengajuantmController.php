<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use DB;
use View;
// use Excel;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DataabsensiModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\PengajuantmModel;
use DateTime;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use File;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;
class pengajuantmController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

  public function __construct(Request $request) {
        # ---------------
    $uri                      = getUrl() . "/index";
        # ---------------
    $qMenu                    = new MenuModel;
    $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
    $this->PROT_Parent        = $rs[0]->parent_name;
    $this->PROT_ModuleName    = $rs[0]->name;
    $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
    View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
}

public function index(Request $request, $page=null)
{
    $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
    $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
    $data["form_act"]       = "/pengajuantm/index";
    $data["active_page"]    = (empty($page)) ? 1 : $page;
    $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
         ----------------------- */
         $qMenu                  = new MenuModel;
         $qDatapengajuantm       = new PengajuantmModel;
         $p_karyawan             = $qDatapengajuantm->getIdKaryawan(Auth::user()->nik);
         $sisacuti              = $p_karyawan->sisacuti;
        # ---------------
         $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
         ----------------------- nama pengajuan,tgl awal, tgl akhir, status*/
         $data["table_header"]   = array(array("label"=>"ID"
            ,"name"=>"id_tm"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"checkbox"
            ,"item-class"=>""
            ,"width"=>"5%"
            ,"add-style"=>""),
         array("label"=>"Nama Pengajuan"
            ,"name"=>"nama_jenisabsen"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Tanggal Awal"
            ,"name"=>"tgl_awal"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Tanggal Akhir"
            ,"name"=>"tgl_akhir"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
         array("label"=>"Status"
            ,"name"=>"statusapproval"
            ,"align"=>"center"
            ,"item-align"=>"left"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),
        array("label"=>"Komentar"
            ,"name"=>"komentarapp"
            ,"align"=>"center"
            ,"item-align"=>"center"
            ,"item-format"=>"normal"
            ,"item-class"=>""
            ,"width"=>"25%"
            ,"add-style"=>""),

         // array("label"=>"Status Approval 2"
         //    ,"name"=>"approval2"
         //    ,"align"=>"center"
         //    ,"item-align"=>"center"
         //    ,"item-format"=>"normal"
         //    ,"item-class"=>""
         //    ,"width"=>"25%"
         //    ,"add-style"=>"")


     );
        # ---------------
         if($request->has('text_search')) {
            session(["SES_SEARCH_DATAABSENSI" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DATAABSENSI");
        }
        # ---------------
        $data["select"]        = $qDatapengajuantm->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDatapengajuantm->getList($request->input("text_search"));
        if(count($data["select"])>0){
            foreach ($data["select"] as $key => $value) {
                # code...
                $cekpengajuan = DB::table('tm_pengajuanabsen')->select('id_tm','order','statusapproval','tgl_approved')->where('id_tm',$value->id_tm)->orderBy('order', 'DESC')->get();
                if(count($cekpengajuan)>1){
                    // $grup_nik_tgl = $this->group_by('statusapproval',$cekpengajuan);
                    // dd($grup_nik_tgl);
                    $jumlahstatus_1 = 0;
                    $jumlahapproved = 0;
                    $jumlahreject = 0;
                    $jumlahpending = 0;
                    $jumlahreturn = 0;
                    foreach ($cekpengajuan as $key2 => $value2) {
                        # code...
                        if($value2->statusapproval == null || $value2->statusapproval ==''){
                            $jumlahpending = $jumlahpending+1;
                        }else if($value2->statusapproval == 0){
                            $jumlahreject = $jumlahreject+1;
                        }else if($value2->statusapproval == 1){
                            $jumlahstatus_1 = $jumlahstatus_1+1;
                        }else if($value2->statusapproval == 2){
                            $jumlahreturn = $jumlahreturn+1;
                        }
                    }
                    
                    if($jumlahstatus_1 == count($cekpengajuan)){
                        $status = '1 dari '. count($cekpengajuan).' - Approved';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 1 && $jumlahreject == 0){
                        $status = '1 dari '. count($cekpengajuan).' - Approved, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 1 && $jumlahreject == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Reject, Pending';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 0 && $jumlahreject == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Reject';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 0 && $jumlahreject == 2){
                            $status = '1 dari '. count($cekpengajuan).' - Reject';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 2 && $jumlahreject == 0){
                            $status = '1 dari '. count($cekpengajuan).' - Pending, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 1 && $jumlahreject == 0 && $jumlahreturn == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Return, Pending';
                    }else if($jumlahstatus_1 == 0 && $jumlahpending == 0 && $jumlahreject == 0 && $jumlahreturn == 2){
                        $status = '1 dari '. count($cekpengajuan).' - Return, Return';
                    }else if($jumlahstatus_1 == 1 && $jumlahpending == 0 && $jumlahreject == 0 && $jumlahreturn == 1){
                        $status = '1 dari '. count($cekpengajuan).' - Approved, Return';
                    }else{
                        $status = ' ';
                    }
                }else if(count($cekpengajuan) == 1){
                    if($cekpengajuan[0]->statusapproval == null || $cekpengajuan[0]->statusapproval ==''){
                        $status = ' Pending';
                        // $status = '1 dari '. count($cekpengajuan).' Pending';
                    }else if($cekpengajuan[0]->statusapproval == 0){
                        $status = ' Reject';
                    }else if($cekpengajuan[0]->statusapproval == 1){
                        $status = ' Approved';
                    }else if($cekpengajuan[0]->statusapproval == 2){
                        $status = ' Return';
                    }else{
                        $status = ' ';
                    }
                }else{
                        $status = ' ';
                }
                $data["select"][$key] = $value;
                $data["select"][$key]->statusapproval = $status;
            }
        }

        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);

        $p_karyawan_baru = DB::table('p_karyawan')->where('nik', Auth::user()->nik)->select('sisacuti','tgl_batas_cuti','id_karyawan')->first();
        $tgl_batas_cuti_akhir = $p_karyawan_baru->tgl_batas_cuti;
        $tgl_batas_cuti_akhir_plus_1 = date('Y-m-d', strtotime($tgl_batas_cuti_akhir. ' + 1 years'));
        $tgl_sekarang = date('Y-m-d');
        // $tgl_masuk = $p_karyawan->tgl_masuk;
        if($tgl_sekarang > $tgl_batas_cuti_akhir){
        $nilai_cuti = $p_karyawan_baru->sisacuti;
        if($nilai_cuti < 0){
        $nilai_cuti = 12 + $nilai_cuti;
        }else{
        $nilai_cuti = 12;
        }
        $sisacuti = $nilai_cuti;
        DB::table('p_karyawan')->where('id_karyawan', $p_karyawan_baru->id_karyawan)
        ->update([
        "tgl_batas_cuti" => $tgl_batas_cuti_akhir_plus_1,
        "sisacuti" => $sisacuti,
        ]);
        }
        $data["sisacuti"] = $sisacuti;
        # ---------------
        return view("default.pengajuantm", $data);
    }
    

    public function add() {
        $data["title"]         = "Add Pengajuan Tidak Masuk Kantor";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/pengajuantm/save";
        $data["form_value"]     = 0;
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster               = new MasterModel;
        /* ----------
         Source
         ----------------------- */
         $qGroups               = $qMaster->getSelectGroup();
         $qJenisPengajuan       = $qMaster->getJenisPengajuan();
         $qNamaAnggota        = $qMaster->getNamaAnggotaDepartemen(Auth::user()->nik);

       // $qStatus             = getSelectStatusDataabsensi();
        /* ----------
         Tabs
         ----------------------- */
        // $data["tabs"]          = array(array("label"=>"Data Nasabah", "url"=>"/url/datakendaraa/1", "active"=>"active")
        //                               ,array("label"=>"Data Surat Menyurat", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Keluarga", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Usaha Pasangan", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data BPKB", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data STNK", "url"=>"/url", "active"=>"")
        //                               ,array("label"=>"Data Kredit", "url"=>"/url", "active"=>""));
        /* ----------
         Fields
         ----------------------- */
       // $data["fields"][]      = form_text(array("name"=>"", "label"=>"Kode", "mandatory"=>"yes", "first_selected"=>"yes"));
         $data["fields"][]      = form_select(array("name"=>"id_status_karyawan", "label"=>"Nama Pengajuan", "withnull"=>"yes", "mandatory"=>"yes", "source"=>$qJenisPengajuan));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Mulai Pengajuan", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir Pengajuan", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
         // $data["fields"][]      = form_select(array("name"=>"id_delegasi", "label"=>"To Delegasi", "withnull"=>"yes", "source"=>$qNamaAnggota));
         $data["fields"][]      = form_text(array("name"=>"komentar", "label"=>"Keterangan", "mandatory"=>"yes"));
         $data["fields"][]      = form_upload2(array("name"=>"foto", "label"=>"Foto"));
		 $data["fields"][]      = form_upload3(array("name"=>"foto2", "label"=>"Foto"));
		 

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
		 $data["foto_value"]     = 0;

        # ---------------
         // $query  = DB::table("tm_timeoff as a")
         //                    ->select("a.*","b.id_karyawan","b.nama_karyawan","c.*")
         //                    ->leftjoin("p_karyawan as b","b.id_karyawan","=","a.id_karyawan")
         //                    ->leftjoin("tm_dataapproval as c","c.id_jabatan","=","b.id_jabatan")
         //                    ->get();
         return view("default.form_upload", $data);
     }

    public function getDatesFromRange2($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
    }

    public function save(Request $request) {
	if($request->id_status_karyawan == 0){
            return redirect("/pengajuantm/add")->withErrors(['Pilih Nama Pengajuan'])
                ->withInput();  



        }
        $rules = array(
            'foto' => 'mimes:jpeg,jpg,png',
            'tgl_awal' => 'required',
            'tgl_akhir' => 'required'
        );

        $messages = [
                    'foto.required' => 'Format Foto Tidak Sesuai',
                    'tgl_awal.required' => 'Tanggal Awal Harus diisi',
                    'tgl_akhir.required' => 'Tanggal Akhir Harus diisi',
                    // 'foto:mimes' => 'Type File Harus jpeg,jpg,png',
                    ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect("/pengajuantm/add")
            ->withErrors($validator)
            ->withInput();
        }

	$profile_system = DB::table('m_profilesystem')->first();
        $tgl_absenawal = $profile_system->tgl_absenawal;
        $tgl_absenakhir = $profile_system->tgl_absenakhir;

        $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen', $request->id_status_karyawan)->first();
        $tm_kerja_dari_rumah = DB::table('tm_kerja_dari_rumah')->first(); 
        if($jenis_absen->inisial == 'I' || $jenis_absen->inisial == 'IK'){
            $qkaryawan = DB::table("p_karyawan")->select('id_karyawan')->where("nik", Auth::user()->nik)->first();
            $cek_tm = DB::table('tm_timeoff')
                    ->select('tgl_awal', 'tgl_akhir','status')
                    ->where('id_karyawan', $qkaryawan->id_karyawan)
                    ->whereIn('id_status_karyawan', [1,5,7])
                    ->whereBetween('tgl_awal', array($tgl_absenawal, $tgl_absenakhir))
                    ->get()->toArray();

            $count_pengajuan = 0;
            if($cek_tm > 0){
                foreach($cek_tm as $k_tm => $v_tm){
                    if($v_tm->status == null || $v_tm->status == 'Approve' || $v_tm->status == 'Pending'){
                        $rangedate = $this->getDatesFromRange2($v_tm->tgl_awal,$v_tm->tgl_akhir);
                        $count_pengajuan = $count_pengajuan + count($rangedate);
                    }
                }
            }
            if($count_pengajuan >= $jenis_absen->batas_sebulan){
                return redirect("/pengajuantm/add")
                    ->withErrors(['Total jumlah pengajuan Ijin(Masuk Telat/Tidak Absen Masuk), Ijin(Pulang Cepat/Tidak Absen Pulang) atau Ijin Tidak Masuk dalam sebulan maksimal '.$jenis_absen->batas_sebulan.' hari'])
                    ->withInput();
            }
        }

        if($request->id_status_karyawan == 0){
            return redirect("/pengajuantm/add")->withErrors(['Pilih Nama Pengajuan'])
                ->withInput();  
        }
        
        $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'),setYMD($request->tgl_akhir,'/'));
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }   

        if(count($rangedate) == 2){
            $jumlahharilibur = 0;
            foreach ($rangedate as $key3 => $value3) {
                # code...
                $hari = new DateTime($value3);
                $nohari = $hari->format('w');
                if($nohari == 0){   
                    $jumlahharilibur = $jumlahharilibur+1;
                }else if(in_array($value3, $tgl_libur)){
                    $jumlahharilibur = $jumlahharilibur+1;
                    
                }
            }
            if($jumlahharilibur == 2){
            //  return redirect("/pengajuantm/add")
            //         ->withErrors(['Tanggal yg dipilih hari libur minggu dan nasional'])
            //         ->withInput();
            }
        }

		 $tglmerah = 0;
         date_default_timezone_set('Asia/Jakarta');
         $datenow = date('Y-m-d');
         $rangedate_ = $rangedate;
         foreach ($rangedate_ as $key3 => $value3) {
             # code...
             $hari = new DateTime($value3);
             $nohari = $hari->format('w');
             if($nohari == 0){   
                 $tglmerah = $tglmerah+1;
             }else if(in_array($value3, $tgl_libur)){
                 $tglmerah = $tglmerah+1;
             }
         }

         if($jenis_absen->inisial != 'H'){                                                               
             $filterafterdayoff = [];
             $date1=date_create($datenow);
             foreach($rangedate_ as $keyrangedate => $vrangedate){
                 $filterafterdayoff[] = $vrangedate; 
                 $date2=date_create($vrangedate);
                 $diff=date_diff($date1,$date2);
                 $selisih =  $diff->format("%R%a");

                 //if( $selisih < -7 ){
                 //    return redirect("/pengajuantm/add")
                 //        ->withErrors(['Pengajuan time off paling lambat H+7'])
                 //        ->withInput();
                 //}
             }
         }
		
        $total = 0;
        if(count($rangedate) == 1){
            foreach ($rangedate as $key2 => $value2) {
                # code...
                $hari = new DateTime($value2);
                $nohari = $hari->format('w');
                if($nohari == 0){
                    // return redirect("/pengajuantm/add")
                    // ->withErrors(['Tanggal yg dipilih hari libur'])
                    // ->withInput();
                }else if(in_array($value2, $tgl_libur)){
                    //  return redirect("/pengajuantm/add")
                    // ->withErrors(['Tanggal yg dipilih hari libur nasional'])
                    // ->withInput();
                }else{
                    $total = $total+1;
                }
            }
        }else{
            foreach ($rangedate as $key2 => $value2) {
                # code...
                $hari = new DateTime($value2);
                $nohari = $hari->format('w');
                if($nohari == 0){
                    
                }else if(in_array($value2, $tgl_libur)){
                     
                }else{
                    $total = $total+1;
                }
            }
            
        }
        $batas = 0;
        $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen', $request->id_status_karyawan)->first();
        $batas = $jenis_absen->batas_max;
        
        if($total > $batas){
            return redirect("/pengajuantm/add")
                    ->withErrors(['Jumlah hari dalam satu kali pengajuan '.$jenis_absen->nama_jenisabsen.' maksimal '.$batas.' hari'])
                    ->withInput();
        }
		
		if($jenis_absen->inisial == 'KR'){
            if( ((setYMD($request->tgl_awal,'/') >= $tm_kerja_dari_rumah->tanggal_awal) && (setYMD($request->tgl_akhir,'/') <= $tm_kerja_dari_rumah->tanggal_akhir)) ){

            }
            //else{
            //    return redirect("/pengajuantm/add")
            //        ->withErrors(['Hanya berlaku dari '.date('d-m-Y', strtotime($tm_kerja_dari_rumah->tanggal_awal)).' s/d '.date('d-m-Y', strtotime($tm_kerja_dari_rumah->tanggal_akhir))  ])
            //        ->withInput();
            //}
        }
		
        $rules = array(
            'foto' => 'mimes:jpeg,jpg,png',
        );

        $messages = [
                    'foto.required' => 'Format Foto Tidak Sesuai',
                    // 'foto:mimes' => 'Type File Harus jpeg,jpg,png',
                    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pengajuantm/add")
            ->withErrors($validator)
            ->withInput();
        } else {
            $qDataabsensi  = new PengajuantmModel;
            # ---------------
            $qresponse = $qDataabsensi->createData($request);
            # ---------------
            if($qresponse['status'] == 'failed'){
            return redirect("/pengajuantm/add")->withErrors(array_unique($qresponse['response']))
                ->withInput();
            }else{
            session()->flash("success_message", "Dataabsensi has been saved");
            # ---------------
            return redirect("/pengajuantm/add");
            }
        }
    }

    public function edit($id) {
        $data["title"]        = "Edit Pengajuan Tidak Masuk Kantor";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/pengajuantm/update";
        $data["form_value"]     = 1;
        /* ----------
         Dataabsensi
         ----------------------- */
         $qMaster              = new MasterModel;
         $qDatapengajuantm             = new PengajuantmModel;
        /* ----------
         Source
         ----------------------- */
        $qDatapengajuantm             = $qDatapengajuantm->getProfile($id)->first();
        $qGroups               = $qMaster->getSelectGroup();
        $qJenisPengajuan       = $qMaster->getJenisPengajuan();
        $qNamaAnggota        = $qMaster->getNamaAnggotaDepartemen(Auth::user()->nik);
        // $paths = "http://localhost/mitrasdm/storage/app/foto_timeoff/";
	if ($qDatapengajuantm->status == "Approve" || $qDatapengajuantm->status == "Reject") {
        
            return redirect("/pengajuantm/index")
            ->withErrors(['Pengajuan Sudah diterima dan tidak bisa diedit'])
            ->withInput();
        }else {
            
        /* ----------
         Fields
         ----------------------- */
         $data["fields"][]      = form_hidden(array("name"=>"id_tm", "label"=>"Dataabsensi ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
         $data["fields"][]      = form_select(array("name"=>"id_status_karyawan", "label"=>"Nama Pengajuan", "mandatory"=>"yes", "source"=>$qJenisPengajuan, "value"=>$qDatapengajuantm->id_status_karyawan));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Awal Pengajuan", "mandatory"=>"yes","value"=>date("d/m/Y", strtotime($qDatapengajuantm->tgl_awal)), "first_selected"=>"yes"));
         $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir Pengajuan", "mandatory"=>"yes","value"=>date("d/m/Y", strtotime($qDatapengajuantm->tgl_akhir)), "first_selected"=>"yes"));
         // $data["fields"][]      = form_select(array("name"=>"id_delegasi", "label"=>"To Delegasi", "source"=>$qNamaAnggota, "mandatory"=>"yes", "value"=>$qDatapengajuantm->id_delegasi));
         $data["fields"][]      = form_text(array("name"=>"komentar", "label"=>"Keterangan", "mandatory"=>"yes",  "value"=>$qDatapengajuantm->komentar));
         $data["fields"][]      = form_upload2(array("name"=>"foto", "label"=>"Foto"));
		 $data["fields"][]      = form_upload3(array("name"=>"foto2", "label"=>"Foto"));

         // $data["fields"][]      = form_select(array("name"=>"status_absensi", "label"=>"Status Hadir", "mandatory"=>"yes","value"=>$qDatapengajuantm->status_absensi, "source"=>$qJenisPengajuan));
         
            if(empty($qDatapengajuantm->foto)) {
        
                  $data["fields"][]     = "<div class=\"form-group\">
                                          <label class=\"col-md-3 control-label\"></label>
                                          <div class=\"col-md-9\">
                                                    <span class=\"btn btn-sm btn-danger\">Foto-Foto Tidak Ada</span>
                                          </div></div>";
            } else {
        $vurl                  = url('')."/app/foto_timeoff/".$qDatapengajuantm->foto;
        $pathImage             = url('')."/app/img/icon/remove.png";
        $vurlDelete            = url('')."/pengajuantm/deletefile/".$id."/".$qDatapengajuantm->foto;
                  $data["fields"][]     = "<div class=\"form-group\">
                                            <label class=\"col-md-3 control-label\"></label>
                                            <div class=\"col-md-9\">
                                                      <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a><a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>
                                            </div></div>";
            }
                                                      // <a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>

        # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
		 $data["foto_value"]     = empty($qDatapengajuantm->foto) ? 0:1;
        # ---------------
         return view("default.form_upload", $data);
        }
     }

     public function update(Request $request)
     {  
	if($request->id_status_karyawan == 0){
            return redirect("/pengajuantm/edit/" . $request->input("id_tm"))->withErrors(['Pilih Nama Pengajuan'])
                ->withInput();  
        }
        
		$rules = array(
            'foto' => 'mimes:jpeg,jpg,png',
            'tgl_awal' => 'required',
            'tgl_akhir' => 'required'
        );

        $messages = [
                    'foto.required' => 'Format Foto Tidak Sesuai',
                    'tgl_awal.required' => 'Tanggal Awal Harus diisi',
                    'tgl_akhir.required' => 'Tanggal Akhir Harus diisi',
                    // 'foto:mimes' => 'Type File Harus jpeg,jpg,png',
                    ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
            ->withErrors($validator)
            ->withInput();
        }

	$profile_system = DB::table('m_profilesystem')->first();
        $tgl_absenawal = $profile_system->tgl_absenawal;
        $tgl_absenakhir = $profile_system->tgl_absenakhir;

        $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen', $request->id_status_karyawan)->first();
         $tm_kerja_dari_rumah = DB::table('tm_kerja_dari_rumah')->first();
        if($jenis_absen->inisial == 'I' || $jenis_absen->inisial == 'IK'){
            $qkaryawan = DB::table("p_karyawan")->select('id_karyawan')->where("nik", Auth::user()->nik)->first();
            $cek_tm = DB::table('tm_timeoff')
                    ->select('tgl_awal', 'tgl_akhir','status')
                    ->where('id_karyawan', $qkaryawan->id_karyawan)
                    ->whereIn('id_status_karyawan', [1,5,7])
                    ->whereBetween('tgl_awal', array($tgl_absenawal, $tgl_absenakhir))
                    ->get()->toArray();

            $count_pengajuan = 0;
            if($cek_tm > 0){
                foreach($cek_tm as $k_tm => $v_tm){
                    if($v_tm->status == null || $v_tm->status == 'Approve' || $v_tm->status == 'Pending'){
                        $rangedate = $this->getDatesFromRange2($v_tm->tgl_awal,$v_tm->tgl_akhir);
                        $count_pengajuan = $count_pengajuan + count($rangedate);
                    }
                }
            }
            if($count_pengajuan >= $jenis_absen->batas_sebulan){
                return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
                    ->withErrors(['Total jumlah pengajuan Ijin(Masuk Telat/Tidak Absen Masuk), Ijin(Pulang Cepat/Tidak Absen Pulang) atau Ijin Tidak Masuk dalam sebulan maksimal '.$jenis_absen->batas_sebulan.' hari'])
                    ->withInput();
            }
        }

        if($request->id_status_karyawan == 0){
            return redirect("/pengajuantm/edit/" . $request->input("id_tm"))->withErrors(['Pilih Nama Pengajuan'])
                ->withInput();  
        }
        
        $rangedate = $this->getDatesFromRange2(setYMD($request->tgl_awal,'/'),setYMD($request->tgl_akhir,'/'));
        $hari_libur = DB::table('m_harilibur')->where([['nama_harilibur','!=', null],['nama_harilibur','!=','']])->select('tgl_libur')->get();
        foreach ($hari_libur as $key => $value) {
            # code...
            $tgl_libur[] = date('Y-m-d', strtotime($value->tgl_libur));
        }   

        if(count($rangedate) == 2){
            $jumlahharilibur = 0;
            foreach ($rangedate as $key3 => $value3) {
                # code...
                $hari = new DateTime($value3);
                $nohari = $hari->format('w');
                if($nohari == 0){   
                    $jumlahharilibur = $jumlahharilibur+1;
                }else if(in_array($value3, $tgl_libur)){
                    $jumlahharilibur = $jumlahharilibur+1;
                    
                }
            }
            if($jumlahharilibur == 2){
            //  return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
            //         ->withErrors(['Tanggal yg dipilih hari libur minggu dan nasional'])
            //         ->withInput();
            }
        }

		 $tglmerah = 0;
         date_default_timezone_set('Asia/Jakarta');
         $datenow = date('Y-m-d');
         $rangedate_ = $rangedate;
         foreach ($rangedate_ as $key3 => $value3) {
             # code...
             $hari = new DateTime($value3);
             $nohari = $hari->format('w');
             if($nohari == 0){   
                 $tglmerah = $tglmerah+1;
             }else if(in_array($value3, $tgl_libur)){
                 $tglmerah = $tglmerah+1;
             }
         }

         if($jenis_absen->inisial != 'H'){
         $filterafterdayoff = [];
         $date1=date_create($datenow);
         foreach($rangedate_ as $keyrangedate => $vrangedate){
             $filterafterdayoff[] = $vrangedate; 
             $date2=date_create($vrangedate);
             $diff=date_diff($date1,$date2);
             $selisih =  $diff->format("%R%a");

             //if( $selisih < -7 ){
             //    return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
             //        ->withErrors(['Pengajuan time off paling lambat H+7'])
             //        ->withInput();
             //}
         }
        }
		
        $total = 0;
        if(count($rangedate) == 1){
            foreach ($rangedate as $key2 => $value2) {
                # code...
                $hari = new DateTime($value2);
                $nohari = $hari->format('w');
                if($nohari == 0){
                    // return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
                    // ->withErrors(['Tanggal yg dipilih hari libur'])
                    // ->withInput();
                }else if(in_array($value2, $tgl_libur)){
                    //  return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
                    // ->withErrors(['Tanggal yg dipilih hari libur nasional'])
                    // ->withInput();
                }else{
                    $total = $total+1;
                }
            }
        }else{
            foreach ($rangedate as $key2 => $value2) {
                # code...
                $hari = new DateTime($value2);
                $nohari = $hari->format('w');
                if($nohari == 0){
                    
                }else if(in_array($value2, $tgl_libur)){
                     
                }else{
                    $total = $total+1;
                }
            }
            
        }
        $batas = 0;
        $jenis_absen = DB::table('tm_jenisabsen')->where('id_jenisabsen', $request->id_status_karyawan)->first();
        $batas = $jenis_absen->batas_max;
        
        if($total > $batas){
            return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
            ->withErrors(['Jumlah hari dalam satu kali pengajuan '.$jenis_absen->nama_jenisabsen.' maksimal '.$batas.' hari' ])
            ->withInput();
        }
		
		if($jenis_absen->inisial == 'KR'){
            if( ((setYMD($request->tgl_awal,'/') >= $tm_kerja_dari_rumah->tanggal_awal) && (setYMD($request->tgl_akhir,'/') <= $tm_kerja_dari_rumah->tanggal_akhir)) ){

            }
	    //else{
            //    return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
            //        ->withErrors(['Hanya berlaku dari '.date('d-m-Y', strtotime($tm_kerja_dari_rumah->tanggal_awal)).' s/d '.date('d-m-Y', strtotime($tm_kerja_dari_rumah->tanggal_akhir))  ])
            //        ->withInput();
            //}
        }
        $rules = array(

            'foto' => 'mimes:jpeg,jpg,png|',
        );
        $messages = [
                    // 'foto.required' => 'Format Foto Tidak Sesuai',
                    'foto:mimes' => 'Type File Harus jpeg,jpg,png',
                    ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/pengajuantm/edit/" . $request->input("id_tm"))
            ->withErrors($validator)
            ->withInput();
        } else {
            // $qDatapengajuantm      = new PengajuantmModel;
            // # ---------------
            // $qDatapengajuantm->updateData($request);
            // # ---------------
            // session()->flash("success_message", "Dataabsensi has been updated");
            // # ---------------
            // return redirect("/pengajuantm/edit/" . $request->input("id_tm"));

            $qDataabsensi  = new PengajuantmModel;
            # ---------------
            $qresponse = $qDataabsensi->updateData($request);
            # ---------------
            if($qresponse['status'] == 'failed'){
            return redirect("/pengajuantm/edit/". $request->input("id_tm"))->withErrors(array_unique($qresponse['response']))
                ->withInput();
            }else{
            session()->flash("success_message", "Dataabsensi has been updated");
            # ---------------
            return redirect("/pengajuantm/edit/". $request->input("id_tm"));
            }

        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Dataabsensi";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/pengajuantm/remove";
        /* ----------
         Source
         ----------------------- */
         $qMaster     = new MasterModel;
         $qDatapengajuantm     = new PengajuantmModel;
         $qDatapengajuantm       = $qDatapengajuantm->getProfile($id)->first();
        /* ----------
         Fields
         ----------------------- */
         if ($qDatapengajuantm->status == "Approve" || $qDatapengajuantm->status == "Reject") {
            return redirect("/pengajuantm/index")
            ->withErrors(['Pengajuan Sudah diterima dan tidak bisa dihapus'])
            ->withInput();
        }else {
        $data["fields"][]      = form_hidden(array("name"=>"id_tm", "label"=>"Dataabsensi ID", "readonly"=>"readonly", "value"=>$id));
         $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
         $data["fields"][]      = form_text(array("name"=>"nama_jenisabsen", "label"=>"Nama Pengajuan", "readonly"=>"readonly", "value"=>$qDatapengajuantm->nama_jenisabsen));
         // $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Nama Pengajuan", "readonly"=>"readonly", "value"=>date("d/m/Y", strtotime($qDatapengajuantm->tgl_awal)), "first_selected"=>"yes"));
         $data["fields"][]      = form_text(array("name"=>"tgl_awal", "label"=>"Tanggal Awal Pengajuan", "readonly"=>"readonly","value"=>date("d/m/Y", strtotime($qDatapengajuantm->tgl_awal)), "first_selected"=>"yes"));
         $data["fields"][]      = form_text(array("name"=>"tgl_akhir", "label"=>"Tanggal Akhir Pengajuan", "readonly"=>"readonly","value"=>date("d/m/Y", strtotime($qDatapengajuantm->tgl_akhir)), "first_selected"=>"yes"));
         # ---------------
         $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
         $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
         return view("default.form", $data);
        }
     }

     public function remove(Request $request) {
        // if($request->input("id_tm") != 1) {
            $qDatapengajuantm     = new PengajuantmModel;
            # ---------------
            $qDatapengajuantm->removeData($request);
            # ---------------
            session()->flash("success_message", "Dataabsensi has been removed");
        // } else {
        //     session()->flash("error_message", "Dataabsensi cannot be removed");
        // }
      # ---------------
        return redirect("/pengajuantm/index"); 
    }

    public function deletefile($id, $namafile) {
        $tm_timeoff = DB::table('tm_timeoff')->where('id_tm',$id)->first();
        $file_path=public_path() .'/app/foto_timeoff/'. $tm_timeoff->foto;
        File::delete($file_path);
        DB::table('tm_timeoff')->where('id_tm',$id)->update(['foto'=> null]);
        session()->flash('success_message', 'Foto berhasil dihapus.');
      # ---------------
        return redirect("/pengajuantm/edit/".$id); 
    }

    public function getDatesFromRange($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('d-m-Y', strtotime(current($dates).' +1 day'));
    }
    return $dates;
}
}
