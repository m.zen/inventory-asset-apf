<?php
//use Storage;
if(!function_exists("uploadHelper"))
 {

}




if(!function_exists("form_button_submit")) {
    function form_button_submit($var) {
        return  "<button type=\"submit\" class=\"btn btn-sm btn-primary\" name=\"".$var["name"]."\" id=\"".$var["name"]."\">".$var["label"]."</button>";
    }
}

if(!function_exists("form_button_cancel")) {
    function form_button_cancel($var) {
          return    "<button type=\"button\" class=\"btn btn-sm btn-default\" name=\"".$var["name"]."\" id=\"".$var["name"]."\" onclick=\"history.go(-1)\">".$var["label"]."</button>";
    }
}

if(!function_exists("form_button_cancel2")) {
    function form_button_cancel2($var) {
          return    "<button type=\"button\" class=\"btn btn-sm btn-default\" name=\"".$var["name"]."\" id=\"".$var["name"]."\" onclick=\"location.href = '".$var["action"]."'\">".$var["label"]."</button>";
    }
}

if(!function_exists("form_button_window")) {
    function form_button_window($var) {
        return  "<button type=\"button\" class=\"btn btn-sm btn-primary\" name=\"".$var["name"]."\" id=\"".$var["name"]."\"  onClick=\"LWindow();\">".$var["label"]."</button>";
    }
}

if(!function_exists("form_action_button")) {
    function form_action_button($var) {
        return  "<button type=\"button\" class=\"btn btn-sm btn-default\" name=\"".$var["name"]."\" id=\"".$var["name"]."\" onclick=\"window.location='".url($var["url"])."'\">".$var["label"]."</button>";
    }
}

if(!function_exists("form_button_dismiss_modal")) {
    function form_button_dismiss_modal($var) {
          return    "<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\" >".$var["label"]."</button>";
    }
}

if(!function_exists("form_text")) {
      function form_text($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
                    </div>
                </div>";
      }
}

if(!function_exists("form_textarea")) {
      function form_textarea($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
            $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $rows             = (empty($var["rows"])) ? "rows='5'" : "rows=" . $var["rows"];
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>            
                    <div class=\"col-md-9\">
                        <textarea class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\"".$jsaction." ".$readonly." ".$focus_obj." ".$rows.">".$value."</textarea>
                    </div>
                </div>";
      }
}

if(!function_exists("form_textarea2")) {
      function form_textarea2($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
            $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $rows             = "rows='5'" ;
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        // return "<div class=\"form-group\">
        //             <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
        //             <div class=\"col-md-9\">
        //                 <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
        //             </div>
        //         </div>";

        return "<div class=\"form-group\">
                             
                    <div class=\"col-md-12\">
                        <textarea class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\"".$jsaction." ".$readonly." ".$focus_obj." ".$rows.">".$value."</textarea>
                    </div>
                </div>";
      }
}

if(!function_exists("form_date")) {
    function form_date($var) {
        $label        = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class        = (empty($var["class"])) ? "" : $var["class"];
        $first        = (empty($var["first_selected"])) ? "" : "first-selected";
        $align        = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder  = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title        = (empty($var["title"])) ? "" : $var["title"];
        $size         = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly     = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction     = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory    = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add    = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value        = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css    = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                 $focus_obj   = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                 $focus_obj   = "";
            } else {
                 $focus_obj   = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
            $focus_obj    = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"text\" class=\"form-control masked-input-date ".$class." ".$first." ".$class_add."\" ".$readonly." placeholder=\"dd/mm/yyyy\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"  />
                    </div>
                </div>";
    }
}

if(!function_exists("form_datepicker")) {
    function form_datepicker($var) {
        $label        = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class        = (empty($var["class"])) ? "" : $var["class"];
        $first        = (empty($var["first_selected"])) ? "" : "first-selected";
        $align        = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder  = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title        = (empty($var["title"])) ? "" : $var["title"];
        $size         = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly     = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction     = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory    = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add    = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value        = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css    = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                 $focus_obj   = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                 $focus_obj   = "";
            } else {
                 $focus_obj   = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
            $focus_obj    = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <div class=\"input-group date datepicker-format\" id=\"datepicker-disabled-past\" data-date-format=\"dd/mm/yyyy\">
                            <input type=\"text\" class=\"form-control\" placeholder=\"Select Date\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"  />
                            <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
                        </div>
                    </div>
                </div>";
    }
}

if(!function_exists("form_currency")) {
    function form_currency($var) {
        $label          = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class          = (empty($var["class"])) ? "" : $var["class"];
        $first          = (empty($var["first_selected"])) ? "" : "first-selected";
        $align          = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title          = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value          = ((old($var["name"]) != "") || (empty($var["value"]))) ? $var["value"] : $var["value"];
        $style_icon     = (empty($var["readonly"])) ? "" : "  style=\"background-color:#EEEEEE\"";
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                $focus_obj      = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                $focus_obj      = "";
            } else {
                $focus_obj      = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
            $focus_obj      = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" placeholder=\"".$placeholder."\" value=\"".$value."\" title=\"".$title."\" ".$jsaction." ".$readonly." onBlur=\"this.value=formatCurrency(this.value);\" ".$focus_obj.">
                    </div>
                </div>";
    }
}



if(!function_exists("form_number")) {
    function form_number($var) {
        $label          = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class          = (empty($var["class"])) ? "" : $var["class"];
        $first          = (empty($var["first_selected"])) ? "" : "first-selected";
        $align          = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title          = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value          = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $style_icon     = (empty($var["readonly"])) ? "" : "  style=\"background-color:#EEEEEE\"";
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                $focus_obj      = "'enter'";
            } elseif ($var["focus_field"] == "no") {
                $focus_obj      = "no";
            } else {
                $focus_obj      = "document.myform." . $var["focus_field"];
            }
        } else {
            $focus_obj      = "no";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" placeholder=\"".$placeholder."\" value=\"".$value."\" title=\"".$title."\" ".$jsaction." ".$readonly." onkeypress=\"return isNumberKey(this, event, $focus_obj);\">
                    </div>
                </div>";
    }
}


if(!function_exists("form_number")) {
    function form_number($var) {
        $label          = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class          = (empty($var["class"])) ? "" : $var["class"];
        $first          = (empty($var["first_selected"])) ? "" : "first-selected";
        $align          = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title          = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value          = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $style_icon     = (empty($var["readonly"])) ? "" : "  style=\"background-color:#EEEEEE\"";
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                $focus_obj      = "'enter'";
            } elseif ($var["focus_field"] == "no") {
                $focus_obj      = "no";
            } else {
                $focus_obj      = "document.myform." . $var["focus_field"];
            }
        } else {
            $focus_obj      = "no";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" placeholder=\"".$placeholder."\" value=\"".$value."\" title=\"".$title."\" ".$jsaction." ".$readonly." onkeypress=\"return isNumberKey(this, event, $focus_obj);\">
                    </div>
                </div>";
    }
}

if(!function_exists("form_password")) {
      function form_password($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"password\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
                    </div>
                </div>";
      }
}
if(!function_exists("form_text3")) {
      function form_text3($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
            $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        return "<div class=\"form-group\">
                   
                    <div class=\"col-md-12\">
                        <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."  style=\"background-color:Black;color: white;\" />
                    </div>
                </div>";
      }
}

if(!function_exists("form_select")) {
      function form_select($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtoupper($label)) : ucwords(strtoupper($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $withnull       = (empty($var["withnull"])) ? "" : $var["withnull"];

        if(!empty($var["value"])) {
            $value      = (old($var["name"]) != "") ? old($var["name"]) : $var["value"];
        } else {
            $value      = "";
        }

        $object     = "<div class=\"form-group\">
                                        <label class=\"control-label col-md-3\">".$label.$mandatory."</label>
                                        <div class=\"col-md-9\">
                                            <select class=\"form-control selectpicker input-sm $class\" data-size=\"5\" data-live-search=\"true\" data-style=\"btn-white\" name=\"".$var["name"]."\" id=\"".$var["name"]."\">";
                                if(!empty($var["withnull"])) {
                                    $object     .= "<label>";
                                    $object     .= "<option value=\"0\" selected=\"selected\">-Pilih-</option>";
                                    $object     .= "</label>";
                                }

                                foreach($var["source"] as $rows) {
                                    $rows = (object)$rows;

                                    if($rows->id) {
                                        $selected   = ($rows->id == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows->id."\" $selected>".$rows->name."</option>";
                                        $object     .= "</label>";
                                    } else {
                                        $selected   = ($rows["id"] == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows["id"]."\" $selected>".$rows["name"]."</option>";
                                        $object     .= "</label>";
                                    }
                                }

                                $object     .= "</select>
                                        </div>
                            </div>";

        return $object;
      }
}

if(!function_exists("form_upload")) {
    function form_upload($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $first            = (empty($var["first_selected"])) ? "" : "first-selected";
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                   $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                   $focus_obj       = "";
            } else {
                   $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
              $focus_obj        = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"file\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
                    </div>
                </div>";
    }
}

if(!function_exists("form_checklist")) {
    function form_checklist($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $checked           = (($var["checked"]=="1")) ? "checked" : "" ;
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $first            = (empty($var["first_selected"])) ? "" : "first-selected";
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                   $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                   $focus_obj       = "";
            } else {
                   $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
              $focus_obj        = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-1\">
                        <input type=\"checkbox\" class=\"form-control ".$class." ".$first." "."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj." ".$focus_obj." ".$checked."/>
                    </div>
                     <div class=\"col-md-1\" style=\"align:'left'';\" ></div> 
                </div>";
    }
}

if(!function_exists("form_popup")) {
      function form_popup($var) {
            list($height, $width) = explode("|", $var["size"]);
            # ----------------- 
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];
            $href           = url("/") . "/" . $var["href"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <div class=\"input-group\">
                            <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
                            <span class=\"input-group-addon\">
                                <span><a href=\"javascript:void(0)\" onClick=\"window.open('$href','targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=$width,height=$height')\">...</a></span>
                            </span>
                        </div>
                    </div>
                </div>";
      }
}

if(!function_exists("form_link")) {
    function form_link($var) {
        $label          = (empty($var["label"])) ? $var["name"] : $var["label"];
        $url            = url("/uploads/") . $var["url"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <span class=\"btn btn-sm btn-warning m-r-5\" onClick=\"window.open('$url', '_blank')\">".$label."</span>
                    </div>
                </div>";
    }
}

if(!function_exists("form_hidden")) {
    function form_hidden($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $value            = (empty($var["value"])) ? "" : $var["value"];
            $title            = (empty($var["title"])) ? "" : $var["title"];
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];

            return  "<input type=\"hidden\" class=\"form-control ".$class."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\">";
    }
}

if(!function_exists("form_radio")) {
    function form_radio($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "form-control" : $var["class"];
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtoupper($label)) : ucwords(strtoupper($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $value            = (old($var["name"]) != "") ? old($var["name"]) : $var["value"];

            $object     = "<div class=\"form-group\">
                            <label for=\"".$var["name"]."\" class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                                <div class=\"col-md-9\">";
                                      $i = 0;

                                foreach($var["source"] as $rows) {
                                    $rows = (object)$rows;

                                    if($rows->id) {
                                        $checked   = ($rows->id == $value) ? "checked" : "";

                                        $object     .= "<label class=\"radio-inline\">
                                                               <input type=\"radio\" style=\"margin-top:0px;\" class=\"styled\" name=\"".$var["name"]."\" id=\"".$rows->name."_$i\" value=\"".$rows->id."\" $checked>".$rows->name."&nbsp;&nbsp;&nbsp;
                                                        </label>";

                                        // $object     .= "<label>";
                                        // $object     .= "<option value=\"".$rows->id."\" $selected>".$rows->name."</option>";
                                        // $object     .= "</label>";

                                    } else {
                                        $checked   = ($rows->id == $value) ? "checked" : "";

                                        $object     .= "<label class=\"radio-inline\">
                                                               <input type=\"radio\" style=\"margin-top:0px;\" class=\"styled\" name=\"".$var["name"]."\" id=\"".$rows->name."_$i\" value=\"".$rows->id."\" $checked>".$rows->name."&nbsp;&nbsp;&nbsp;
                                                        </label>";
                                        // $checked   = ($rows["id"] == $value) ? "checked" : "";

                                        // $object     .= "<label class=\"radio-inline\">
                                        //                        <input type=\"radio\" style=\"margin-top:0px;\" class=\"styled\" name=\"".$var["name"]."\" id=\"".$rows["name"]."_$i\" value=\"".$rows["id"]."\" $checked>".$rows["name"]."&nbsp;&nbsp;&nbsp;
                                        //                 </label>";
                                    }





                       //               $checked    = ($rows["id"] == $value) ? "checked" : "";
                       //               $object     .= "<label class=\"radio-inline\">
                         //                                            <input type=\"radio\" style=\"margin-top:0px;\" class=\"styled\" name=\"".$var["name"]."\" id=\"".$var["name"]."_$i\" value=\"".$rows["id"]."\" $checked>".$rows["name"]."&nbsp;&nbsp;&nbsp;
                         //                                     </label>";

                                                          // $i++;
                                }

                                  $object   .= "</div>
                        </div>";

            return $object;
    }
}

if(!function_exists("formsmall_text")) {
    function formsmall_text($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $first            = (empty($var["first_selected"])) ? "" : "first-selected";
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder      = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly         = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction         = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory        = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add        = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css        = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                   $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                   $focus_obj       = "";
            } else {
                   $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
              $focus_obj        = "";
        }

        return "<div class=\"form-group col-md-6\">
                    <label>".$label."</label>".$mandatory."
                    <input type=\"text\" class=\"form-control ".$first." ".$class_add."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\" placeholder=\"".$placeholder."\" ".$readonly." ".$focus_obj." />
                </div>";

    }
}

if(!function_exists("formsmall_currency")) {
    function formsmall_currency($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $first            = (empty($var["first_selected"])) ? "" : "first-selected";
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder      = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly         = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction         = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory        = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add        = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css        = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                   $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                   $focus_obj       = "";
            } else {
                   $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
              $focus_obj        = "";
        }

         return "<div class=\"form-group col-md-6\">
                    <label class=\"col-md-4 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-8\">
                        <input type=\"text\" class=\"form-control ".$class." ".$first." ".$class_add."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" placeholder=\"".$placeholder."\" value=\"".$value."\" title=\"".$title."\" ".$jsaction." ".$readonly." onBlur=\"this.value=formatCurrency(this.value);\" ".$focus_obj.">
                    </div>
                </div>";

//        return "<div class=\"form-group col-md-6\">
  //                  <label>".$label."</label>".$mandatory."
    //                <input type=\"text\" class=\"form-control ".$first." ".$class_add."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\" placeholder=\"".$placeholder."\" ".$readonly." ".$focus_obj."  onBlur=\"this.value=formatCurrency(this.value);\" />
      //          </div>";

    }
}

if(!function_exists("formsmall_select")) {
      function formsmall_select($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtoupper($label)) : ucwords(strtoupper($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $withnull       = (empty($var["withnull"])) ? "" : $var["withnull"];

        if(!empty($var["value"])) {
            $value      = (old($var["name"]) != "") ? old($var["name"]) : $var["value"];
        } else {
            $value      = "";
        }


        $object     = "<div class=\"form-group col-md-4\">
                            <label>".$label."</label>".$mandatory."
                                <select class=\"form-control selectpicker input-sm\" data-size=\"5\" data-live-search=\"true\" data-style=\"btn-white\" name=\"".$var["name"]."\" id=\"".$var["name"]."\">";
                                if(!empty($var["withnull"])) {
                                    $object     .= "<label>";
                                    $object     .= "<option value=\"0\" selected=\"selected\">-Pilih-</option>";
                                    $object     .= "</label>";
                                }

                                foreach($var["source"] as $rows) {
                                    $rows = (object)$rows;

                                    if($rows->id) {
                                        $selected   = ($rows->id == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows->id."\" $selected>".$rows->name."</option>";
                                        $object     .= "</label>";
                                    } else {
                                        $selected   = ($rows["id"] == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows["id"]."\" $selected>".$rows["name"]."</option>";
                                        $object     .= "</label>";
                                    }
                                }

                                $object     .= "</select>
                        </div>";

        return $object;
    }
}

if(!function_exists("formsmall_empty")) {
    function formsmall_empty() {
        return "<div class=\"form-group col-md-4\">
                    <label>&nbsp;</label>
                    <input type=\"hidden\" />
                </div>";
    }
}

if(!function_exists("getActionButton")) {
    function getActionButton($name, $url, $var) {
            $var    = explode("|", $var);

            if($var[0] == "direct") {
                $action     = array("btn-none-direct", $var[1]);
                # ---------------
                return  "<a href=\"".URL::to('/').$url."\" class=\"btn btn-sm btn-primary m-r-5 $action[0]\" title=\"".URL::to('/').$url."\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</a>";
            } elseif($var[0] == "single") {
                $action     = array("btn-single-direct", $var[1]);
                $urls       = URL::to('/').$url;
                # ---------------
                return  "<a href=\"javascript:;\" class=\"btn btn-sm btn-success m-r-5 $action[0]\" title=\"".URL::to('/').$url."\" onmousedown=\"rightclick('$urls');\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</a>";
            } elseif($var[0] == "popup") {
                $action     = array("btn-single-popup", $var[1]);
                # ---------------
                return  "<a href=\"javascript:;\" class=\"btn btn-sm btn-success m-r-5 $action[0]\" title=\"".URL::to('/').$url."\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</a>";
            } elseif($var[0] == "modal") {
                $action     = array("btn-modal", $var[1]);
                # ---------------
                return "<button type=\"button\" class=\"btn btn-sm btn-primary m-r-5\" data-toggle=\"modal\" data-target=\"#formModal\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</button>";
                //return  "<a href=\"javascript:;\" class=\"btn btn-sm btn-success m-r-5 $action[0]\" title=\"".URL::to('/').$url."\" data-toggle=\"modal\" data-target=\"#formModal\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</a>";
            } elseif($var[0] == "single-modal") {
                $action     = array("btn-single-modal", $var[1]);
                # ---------------
                return "<button type=\"button\" class=\"btn btn-sm btn-success m-r-5\" data-toggle=\"modal\" data-target=\"#formModal\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</button>";
                //return  "<a href=\"javascript:;\" class=\"btn btn-sm btn-success m-r-5 $action[0]\" title=\"".URL::to('/').$url."\" data-toggle=\"modal\" data-target=\"#formModal\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</a>";
            } elseif($var[0] == "excel") {
                $action     = array("btn-excel", $var[1]);
                # ---------------
                return  "<a href=\"javascript:;\" target=\"_blank\" class=\"btn btn-sm btn-primary m-r-5 $action[0]\" title=\"".URL::to('/').$url."\"><i class=\"$action[1]\"></i>&nbsp;&nbsp; $name</a>";
            } else {

            }
    }
}

if(!function_exists("getPagging")) {
    function getPagging($active_page, $record, $url) {
            $total_page = ceil($record / Auth::user()->perpage);
            $pref = (($active_page - 1) == 0) ? 1 : $active_page - 1;
        $next = $active_page + 1;
        # ----------------
        if($active_page == $total_page) {
            $next   = $total_page;
        } elseif ($active_page <= 1) {
            $pref   = 1;
        } else {
            $next   = $active_page + 1;
        }
        # ----------------
            return  "<ul class=\"pagination pull-right\" style=\"margin-top:-5px;\">
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."\">&laquo;</a></li>
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$pref."\">&lsaquo;</a></li>
                        <li class=\"paginate_button \"><a href=\"javascript:void(0)\">Page ".$active_page." of ".$total_page."</a></li>
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$next."\">&rsaquo;</a></li>
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$total_page."\">&raquo;</a></li>
                     </ul>";
    }
}

if(!function_exists("getPaggingCustom")) {
    function getPaggingCustom($active_page, $record, $url, $rows) {
            $total_page = ceil($record / $rows);
            $pref = (($active_page - 1) == 0) ? 1 : $active_page - 1;
        $next = $active_page + 1;
        # ----------------
        if($active_page == $total_page) {
            $next   = $total_page;
        } elseif ($active_page <= 1) {
            $pref   = 1;
        } else {
            $next   = $active_page + 1;
        }
        # ----------------
            return  "<ul class=\"pagination pull-right\" style=\"margin-top:-5px;\">
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."\">&laquo;</a></li>
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$pref."\">&lsaquo;</a></li>
                        <li class=\"paginate_button \"><a href=\"javascript:void(0)\">Page ".$active_page." of ".$total_page."</a></li>
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$next."\">&rsaquo;</a></li>
                        <li class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$total_page."\">&raquo;</a></li>
                     </ul>";
    }
}

if(!function_exists("getSimplePagging")) {
    function getSimplePagging($active_page, $record, $url, $perpage) {
            $total_page = ceil($record / $perpage);
            $pref = (($active_page - 1) == 0) ? 1 : $active_page - 1;
        $next = $active_page + 1;
        # ----------------
        if($active_page == $total_page) {
            $next   = $total_page;
        } elseif ($active_page <= 1) {
            $pref   = 1;
        } else {
            $next   = $active_page + 1;
        }
        # ----------------
            return  "<span class=\"paginate_button \"><a href=\"".URL::to('/').$url."\">&laquo;</a></span>
                    <span class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$pref."\">&lsaquo;</a></span>
                    <span class=\"paginate_button \"><a href=\"javascript:void(0)\">Page ".$active_page." of ".$total_page."</a></span>
                    <span class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$next."\">&rsaquo;</a></span>
                    <span class=\"paginate_button \"><a href=\"".URL::to('/').$url."/".$total_page."\">&raquo;</a></span>";
    }
}

if(!function_exists("form_checklist2")) {
    function form_checklist2($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $checked           = (($var["checked"]=="2")) ? "checked" : "" ;
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $first            = (empty($var["first_selected"])) ? "" : "first-selected";
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $disabled       = (empty($var["disabled"])) ? "" : "disabled=".$var["disabled"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                   $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                   $focus_obj       = "";
            } else {
                   $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
              $focus_obj        = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-1\">
                        <input type=\"checkbox\" class=\"form-control ".$class." ".$first." "."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$disabled." ".$focus_obj." ".$focus_obj." ".$checked."/>
                    </div>
                     <div class=\"col-md-1\" style=\"align:'left'';\" ></div> 
                </div>";
    }
}

if(!function_exists("form_upload")) {
    function form_upload($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $value            = (empty($var["value"])) ? "" : $var["value"];
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];

        return  "<input type=\"file\" name=\"file\" id=\"file\" class=\"inputfile\" />";
        return  "<input type=\"hidden\" class=\"form-control ".$class."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\">";
    }
}



if(!function_exists("form_simpleselect")) {
    function form_simpleselect($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtoupper($label)) : ucwords(strtoupper($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $withnull       = (empty($var["withnull"])) ? "" : $var["withnull"];

        if(!empty($var["value"])) {
            $value      = (old($var["name"]) != "") ? old($var["name"]) : $var["value"];
        } else {
            $value      = "";
        }

        $object     = "<select class=\"form-control selectpicker input-sm\" data-size=\"5\" data-live-search=\"true\" data-style=\"btn-white\" name=\"".$var["name"]."\" id=\"".$var["name"]."\">";
                                if(!empty($var["withnull"])) {
                                    $object     .= "<label>";
                                    $object     .= "<option value=\"0\" selected=\"selected\">-Pilih-</option>";
                                    $object     .= "</label>";
                                }

                                foreach($var["source"] as $rows) {
                                    $rows = (object)$rows;

                                    if($rows->id) {
                                        $selected   = ($rows->id == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows->id."\" $selected>".$rows->name."</option>";
                                        $object     .= "</label>";
                                    } else {
                                        $selected   = ($rows["id"] == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows["id"]."\" $selected>".$rows["name"]."</option>";
                                        $object     .= "</label>";
                                    }
                                }

                                $object     .= "</select>";

        return $object;
    }
}

if(!function_exists("form_search_text")) {
    function form_search_text($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];


    return "<div class=\"form-group m-r-10\">
                <input type=\"text\" class=\"form-control\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" placeholder=\"".$placeholder."\" value=\"". $value . "\" />
            </div>";
    }
}

if(!function_exists("form_search_select")) {
    function form_search_select($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $withnull         = (empty($var["withnull"])) ? "" : $var["withnull"];

        if(!empty($var["value"])) {
            $value      = (old($var["name"]) != "") ? old($var["name"]) : $var["value"];
        } else {
            $value      = "";
        }

        $object     = "<div class=\"form-group m-r-10\">
                                            <select class=\"form-control selectpicker input-sm\" data-size=\"5\" data-live-search=\"true\" data-style=\"btn-white\" name=\"".$var["name"]."\" id=\"".$var["name"]."\">";
                                if(!empty($var["withnull"])) {
                                    $object     .= "<label>";
                                    $object     .= "<option value=\"0\" selected=\"selected\">- $label -</option>";
                                    $object     .= "</label>";
                                }

                                foreach($var["source"] as $rows) {
                                    $rows = (object)$rows;

                                    if($rows->id) {
                                        $selected   = ($rows->id == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows->id."\" $selected>".$rows->name."</option>";
                                        $object     .= "</label>";
                                    } else {
                                        $selected   = ($rows["id"] == $value) ? "selected" : "";
                                        $object     .= "<label>";
                                        $object     .= "<option value=\"".$rows["id"]."\" $selected>".$rows["name"]."</option>";
                                        $object     .= "</label>";
                                    }
                                }

                                $object     .= "</select>
                            </div>";

        return $object;
      }
}

if(!function_exists("form_search_datepicker")) {
    function form_search_datepicker($var) {
        $label        = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class        = (empty($var["class"])) ? "" : $var["class"];
        $first        = (empty($var["first_selected"])) ? "" : "first-selected";
        $align        = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder  = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title        = (empty($var["title"])) ? "" : $var["title"];
        $size         = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly     = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction     = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory    = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add    = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value        = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css    = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                 $focus_obj   = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                 $focus_obj   = "";
            } else {
                 $focus_obj   = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
            $focus_obj    = "";
        }

        return "<div class=\"form-group m-r-10\">
                    <div class=\"input-group date datepicker-format\" id=\"datepicker-disabled-past\" data-date-format=\"dd/mm/yyyy\">
                        <input type=\"text\" class=\"form-control\" placeholder=\"$placeholder\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"  />
                        <span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>
                    </div>
                </div>";
    }
}
if(!function_exists("form_time")) {
      function form_time($var) {
            $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
            $class            = (empty($var["class"])) ? "" : $var["class"];
            $first            = (empty($var["first_selected"])) ? "" : "first-selected";
            $align            = (empty($var["align"])) ? "left" : $var["align"];
            $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
            $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
            $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
            $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
            $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
            $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
            $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
            $group_css      = "style_form_input_" . $var["name"];

            if($readonly != "readonly") {
                if(empty($var["focus_field"])) {
                       $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
                } elseif ($var["focus_field"] == "no") {
                       $focus_obj       = "";
                } else {
                       $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
                }
            } else {
                  $focus_obj        = "";
            }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"time\" class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
                    </div>
                </div>";
      }
}
if(!function_exists("form_upload2")) {
    function form_upload2($var) {
        $label            = (empty($var["label"])) ? $var["name"] : $var["label"];
        $class            = (empty($var["class"])) ? "" : $var["class"];
        $first            = (empty($var["first_selected"])) ? "" : "first-selected";
        $align            = (empty($var["align"])) ? "left" : $var["align"];
        $placeholder    = (empty($var["placeholder"])) ? ucwords(strtolower($label)) : ucwords(strtolower($var["placeholder"]));
        $title            = (empty($var["title"])) ? "" : $var["title"];
        $size             = (empty($var["size"])) ? "30px" : $var["size"]."px";
        $readonly       = (empty($var["readonly"])) ? "" : $var["readonly"];
        $jsaction       = (empty($var["jsaction"])) ? "" : $var["jsaction"];
        $mandatory      = (empty($var["mandatory"])) ? "" : " <span style=\"color:#FF0000\">*</span>";
        $class_add      = (empty($var["mandatory"])) ? "" : " mandatory-input";
        $value            = ((old($var["name"]) != "") || (empty($var["value"]))) ? old($var["name"]) : $var["value"];
        $group_css      = "style_form_input_" . $var["name"];

        if($readonly != "readonly") {
            if(empty($var["focus_field"])) {
                   $focus_obj       = " onKeypress=\"return handleEnter(this, event)\"";
            } elseif ($var["focus_field"] == "no") {
                   $focus_obj       = "";
            } else {
                   $focus_obj       = " onKeypress=\"return focusObject(document.myform.".$var["focus_field"].", event)\"";
            }
        } else {
              $focus_obj        = "";
        }

        return "<div class=\"form-group\">
                    <label class=\"col-md-3 control-label\" id=\"Foto\">".$label.$mandatory."</label>
                    <div class=\"col-md-9\">
                        <input type=\"file\" accept=\"image/*\" capture=\"camera\"class=\"form-control ".$class." ".$first." ".$class_add."\" placeholder=\"".$placeholder."\" id=\"".$var["name"]."\" name=\"".$var["name"]."\" value=\"".$value."\"".$jsaction." ".$readonly." ".$focus_obj."/>
                    </div>
                </div>";
    }
}