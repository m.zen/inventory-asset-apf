<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use DB;
use View;
// use Excel;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DataabsensiModel;
use App\Model\Master\MasterModel;
use App\Model\Absensi\JadwalabsenModel;
use DateTime;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use File;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;

class JadwalabsenController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }

    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/jadwalabsen/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qGrupabsen             = new JadwalabsenModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_jadwal"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Nama"
                                    ,"name"=>"nama_karyawan"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"25%"
                                                ,"add-style"=>""),
                                    array("label"=>"Nik"
                                    ,"name"=>"nik"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"25%"
                                                ,"add-style"=>""),
                                    array("label"=>"Tanggal"
                                    ,"name"=>"tanggal"
                                        ,"align"=>"center"
                                        ,"item-align"=>"left"
                                            ,"item-format"=>"normal"
                                            ,"item-class"=>""
                                                ,"width"=>"25%"
                                                ,"add-style"=>""),
                                    array("label"=>"Nama Group Absen"
                                                ,"name"=>"nama_grupabsen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Jam Masuk(Senin-Jumat)"
                                                ,"name"=>"jam_masuk"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
        							array("label"=>"Jam Keluar(Senin-Jumat)"
                                                ,"name"=>"jam_keluar"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jam Masuk(Sabtu)"
                                                ,"name"=>"jam_masuk2"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jam Keluar(Sabtu)"
                                                ,"name"=>"jam_keluar2"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
			      array("label"=>"Jam Masuk(Minggu)"
                                                ,"name"=>"jam_masuk3"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Jam Keluar(Minggu)"
                                                ,"name"=>"jam_keluar3"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),


        							);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_JADWALABSEN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JADWALABSEN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_JADWALABSEN");
        }
        # ---------------
        $data["select"]        = $qGrupabsen->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qGrupabsen->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

public function add(Request $request, $page=null)
{
    $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
    $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
    $data["form_act"]       = "/jadwalabsen/proses";
    $qMaster                = new MasterModel;

    $qCabang                = $qMaster->getSelectCabang();
    $qKaryawan                = $qMaster->getSelectKaryawan();
    $qgrupabsen     = DB::table('tm_grupabsen')->select('id_grupabsen as id', 'nama_grupabsen as name')->get();
    
    $data["fields"][]       = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
    $data["fields"][]      = form_datepicker(array("name"=>"tgl_awal", "label"=>"Tanggal Periode Awal", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
    $data["fields"][]      = form_datepicker(array("name"=>"tgl_akhir", "label"=>"Tanggal Periode Akhir", "mandatory"=>"yes","value"=>date("d/m/Y"), "first_selected"=>"yes"));
    $data["fields"][]       = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "withnull"=>"yes","mandatory"=>"yes", "source"=>$qCabang));
    $data["fields"][]     = form_selectby(array("name"=>"id_karyawan", "label"=>"Nama-Nama Karyawan", "mandatory"=>"yes", "source"=>[]));
    $data["fields"][]       = form_select(array("name"=>"id_grupabsen", "label"=>"Grup Absen", "withnull"=>"yes","mandatory"=>"yes", "source"=>$qgrupabsen));
    
    # ---------------
    $data["buttons"][]      = form_button_submit(array("name"=>"button_save", "label"=>"&nbsp;&nbsp;Save&nbsp;&nbsp;"));
    $data["buttons"][]      = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
    # ---------------
    return view("default.form_jadwal", $data);
}

    public function departemenCabang($param){
        $qMaster               = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $options = [];
        $qDepartemen = DB::table('p_karyawan')->where([['id_cabang', $param],['aktif',1]])->get();
        //hilangkan duplicate data cabang
        if(count($qDepartemen)>0){
        foreach($qDepartemen as $key => $value){
            $qDepartemenn[$value->id_karyawan] = $value;
        }
        $qDepartemen = $qDepartemenn;
        
        // $qDepartemen = array_unique($qDepartemen);
        // dd($qDepartemen);

        //GET THE ACCOUNT BASED ON TYPE
        // $csc_state = csc_states::where('country_id','=',$param)->get();
        //CREATE AN ARRAY 
        $options = array();      
        foreach ($qDepartemen as $arrayForEach) {
                  $options += array($arrayForEach->id_karyawan => $arrayForEach->nama_karyawan);                
              }
        }
        return response()->json($options);
        // return Response::json($options);
    }

    public function proses(Request $request)
    {   
        $rules = array(
                    'id_karyawan' => 'required|',
                    'id_cabang' => 'required|',
                    'tgl_awal' => 'required|',
                    'tgl_akhir' => 'required|',
                    
                                          
        );

        $messages = [
                    'id_karyawan.required' => 'Nama karyawan harus dipilih',
                    'id_cabang.required' => 'Cabang ke harus dipilih',
                    'tgl_awal.required' => 'Tanggal periode awal harus diisi',
                    'tgl_akhir.required' => 'Tanggal periode akhir harus diisi',

        ];

        if($request->id_cabang == 0){
            return redirect("/jadwalabsen/add" )
                ->withErrors(['Cabang harus dipilih'])
                ->withInput();
        }
        if($request->id_grupabsen == 0){
            return redirect("/jadwalabsen/add" )
                ->withErrors(['Grup Absen harus dipilih'])
                ->withInput();
        }
        if($request->id_karyawan == null){
            return redirect("/jadwalabsen/add" )
                ->withErrors(['Nama karyawan harus dipilih'])
                ->withInput();
        }
        if(count($request->id_karyawan) == 1 && $request->id_karyawan[0] == 0){
            return redirect("/jadwalabsen/add" )
                ->withErrors(['Nama karyawan harus dipilih'])
                ->withInput();
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/jadwalabsen/add" )
                ->withErrors($validator)
                ->withInput();
        } else 
        {
            $qProsesgaji      = new JadwalabsenModel;
            # ---------------
            $qProsesgaji->prosesData($request);
            # ---------------
            session()->flash("success_message", "Proses has been updated");
            # ---------------
           return redirect("/jadwalabsen/index");
        }
     }

     public function edit($id) {
        $data["title"]        = "Edit Jadwal Absen";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/jadwalabsen/update";
        /* ----------
         Grupabsen
        ----------------------- */
        $qMaster              = new MasterModel;
        $qGrupabsen             = new JadwalabsenModel;
        /* ----------
         Source
        ----------------------- */
        $qJadwalabsen           = $qGrupabsen->getProfile($id)->first();
        $qCabang                = $qMaster->getSelectCabang();
        $qgrupabsen     = DB::table('tm_grupabsen')->select('id_grupabsen as id', 'nama_grupabsen as name')->get();
        $qkaryawan     = DB::table('p_karyawan')->select('id_karyawan as id', 'nama_karyawan as name')->where('nik', $qJadwalabsen->nik)->get();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_jadwal", "label"=>"Grupabsen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]     = form_select(array("name"=>"id_karyawan", "label"=>"Nama-Nama Karyawan", "mandatory"=>"yes", "source"=>$qkaryawan, "value"=>$id));
        $data["fields"][]      = form_datepicker(array("name"=>"tanggal", "label"=>"Tanggal", "mandatory"=>"yes","value"=>date('d/m/Y',strtotime($qJadwalabsen->tanggal)), "first_selected"=>"yes"));
        $data["fields"][]       = form_select(array("name"=>"id_cabang", "label"=>"Cabang", "mandatory"=>"yes", "source"=>$qCabang, "value"=>$qJadwalabsen->id_cabang));
        $data["fields"][]       = form_select(array("name"=>"id_grupabsen", "label"=>"Grup Absen", "mandatory"=>"yes", "source"=>$qgrupabsen, "value"=>$qJadwalabsen->id_grupabsen));
                
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'tanggal' => 'required|'      ,         
                    'id_grupabsen' => 'required|'               
        );

        $messages = [
                    'tanggal.required' => 'Tanggal harus diisi',
                    'id_grupabsen.required' => 'Grup absen harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/jadwalabsen/edit/" . $request->input("id_jadwal"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qGrupabsen      = new JadwalabsenModel;
            # ---------------
            $qGrupabsen->updateData($request);
            # ---------------
            session()->flash("success_message", "Grup absen has been updated");
            # ---------------
            return redirect("/jadwalabsen/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Jadwal Absen";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/jadwalabsen/remove";
        /* ----------
         Source
        ----------------------- */
        $qGrupabsen     = new JadwalabsenModel;
        $qJadwalabsen                 = $qGrupabsen->getProfile($id)->first();
        $qkaryawan     = DB::table('p_karyawan')->select('id_karyawan as id', 'nama_karyawan as name')->where('nik', $qJadwalabsen->nik)->get();
        $qgrupabsen     = DB::table('tm_grupabsen')->select('id_grupabsen as id', 'nama_grupabsen as name')->get();
         
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_jadwal", "label"=>"Grupabsen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]     = form_select(array("name"=>"id_karyawan", "label"=>"Nama-Nama Karyawan", "mandatory"=>"yes", "source"=>$qkaryawan, "value"=>$id));
        $data["fields"][]       = form_select(array("name"=>"id_grupabsen", "label"=>"Grup Absen", "mandatory"=>"yes", "source"=>$qgrupabsen, "value"=>$qJadwalabsen->id_grupabsen));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
            $qGrupabsen     = new JadwalabsenModel;
            # ---------------
            $qGrupabsen->removeData($request);
            # ---------------
            session()->flash("success_message", "Grupabsen has been removed");
        
      # ---------------
        return redirect("/jadwalabsen/index"); 
    }
}
