<?php

namespace App\Http\Controllers\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use App\User;
use App\Model\MenuModel;
use App\Model\Absensi\DaftarabsenModel;
use App\Model\Master\MasterModel;
class DaftarabsenController extends Controller
{
         protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request) {
        # ---------------
        $uri                      = getUrl() . "/index";
        # ---------------
        $qMenu                    = new MenuModel;
        $rs                       = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent        = $rs[0]->parent_name;
        $this->PROT_ModuleName    = $rs[0]->name;
        $this->PROT_ModuleId      = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent"=>$this->PROT_Parent, "SHR_Module"=>$this->PROT_ModuleName));
    }


    public function index(Request $request, $page=null)
    {
        $data["title"]          = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"]         = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]       = "/daftarabsen/index";
        $data["active_page"]    = (empty($page)) ? 1 : $page;
        $data["offset"]         = (empty($data["active_page"])) ? 0 : ($data["active_page"]-1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu                  = new MenuModel;
        $qDaftarabsen                  = new DaftarabsenModel;
        # ---------------
        $data["action"]         = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"]   = array(array("label"=>"ID"
                                                ,"name"=>"id_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"checkbox"
                                                        ,"item-class"=>""
                                                          ,"width"=>"5%"
                                                            ,"add-style"=>""),
        							array("label"=>"N I K"
                                                ,"name"=>"nik"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"center"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"10%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Karyawan"
                                                ,"name"=>"nama_karyawan"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"Cabang"
                                                ,"name"=>"nama_cabang"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"left"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"25%"
                                                            ,"add-style"=>""),
                                    array("label"=>"No Absen"
                                                ,"name"=>"noabsen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                      array("label"=>"Grup Absen"
                                                ,"name"=>"nama_grupabsen"
                                                  ,"align"=>"center"
                                                    ,"item-align"=>"right"
                                                      ,"item-format"=>"normal"
                                                        ,"item-class"=>""
                                                          ,"width"=>"15%"
                                                           ,"add-style"=>""),
                                       
										);
        # ---------------
        if($request->has('text_search')) {
            session(["SES_SEARCH_DAFTARABSEN" => $request->input("text_search")]);
            # ---------------
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DAFTARABSEN");
        } else {
            $data["text_search"]   = $request->session()->get("SES_SEARCH_DAFTARABSEN");
        }
        # ---------------
        $data["select"]        = $qDaftarabsen->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"]         = $qDaftarabsen->getList($request->input("text_search"));
        # ---------------
        $data["record"]        = count($data["query"]);
        $data["pagging"]       = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    

    public function edit($id) {
        $data["title"]        = "Detail Gaji";
        $data["parent"]       = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]     = "/daftarabsen/update";
        /* ----------
         Daftarabsen
        ----------------------- */
        $qMaster              = new MasterModel;
        $qDaftarabsen           = new DaftarabsenModel;

        /* ----------
         Source
        ----------------------- */
        $qDaftarabsen        = $qDaftarabsen->getProfile($id)->first();
        $qGroups              = $qMaster->getSelectGroup();
        $qGrupabsen			  = $qMaster->getSelectGrupabsen();
        			

        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_karyawan", "label"=>"ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"PUT"));
        $data["fields"][]      = form_text(array("name"=>"nik", "label"=>"NIK", "readonly"=>"readonly", "mandatory"=>"", "value"=>$qDaftarabsen->nik));
        $data["fields"][]      = form_text(array("name"=>"nama_karyawan", "label"=>"Nama","readonly"=>"readonly",  "mandatory"=>"", "value"=>$qDaftarabsen->nama_karyawan));
  		$data["fields"][]      = form_text3(array("name"=>"", "label"=>"","readonly"=>"readonly",  "mandatory"=>"", "value"=>"Komponen Gaji"));
        
        $data["fields"][]      = form_text(array("name"=>"noabsen", "label"=>"No Absen",  "mandatory"=>"", "value"=>$qDaftarabsen->noabsen));
        $data["fields"][]      = form_select(array("name"=>"id_grupabsen", "label"=>"Grup Absen", "mandatory"=>"yes", "source"=>$qGrupabsen, "value"=>$qDaftarabsen->id_grupabsen));
        
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Update"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
                    'id_grupabsen' => 'required|'                 
        );

        $messages = [
                    'id_grupabsen' => 'Daftarabsen harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/daftarabsen/edit/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qDaftarabsen      = new DaftarabsenModel;
            # ---------------
            $qDaftarabsen->updateData($request);
            # ---------------
            session()->flash("success_message", "Daftarabsen has been updated");
            # ---------------
            return redirect("/daftarabsen/index");
        }
    }

    public function delete($id) {
        $data["title"]         = "Delete Daftarabsen";
        $data["parent"]        = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]      = "/daftarabsen/remove";
        /* ----------
         Source
        ----------------------- */
        $qDaftarabsen     = new DaftarabsenModel;
         $qDaftarabsen                 = $qDaftarabsen->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][]      = form_hidden(array("name"=>"id_daftarabsen", "label"=>"Daftarabsen ID", "readonly"=>"readonly", "value"=>$id));
        $data["fields"][]      = form_hidden(array("name"=>"_method", "label"=>"Method", "readonly"=>"readonly", "value"=>"DELETE"));
        $data["fields"][]      = form_text(array("name"=>"nama_daftarabsen", "label"=>"Daftarabsen", "readonly"=>"readonly", "mandatory"=>"yes", "value"=>$qDaftarabsen->nama_daftarabsen));
        
        # ---------------
        $data["buttons"][]     = form_button_submit(array("name"=>"button_save", "label"=>"Delete"));
        $data["buttons"][]     = form_button_cancel(array("name"=>"button_cancel", "label"=>"Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request) {
        if($request->input("id") != 1) {
            $qDaftarabsen     = new DaftarabsenModel;
            # ---------------
            $qDaftarabsen->removeData($request);
            # ---------------
            session()->flash("success_message", "Daftarabsen has been removed");
        } else {
            session()->flash("error_message", "Daftarabsen cannot be removed");
        }
      # ---------------
        return redirect("/daftarabsen/index"); 
    }
}
