<?php

namespace App\Http\Controllers\Transaksi;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use File;
use App\User;
use App\Model\MenuModel;
use App\Model\Transaksi\TransakhapusModel;
use App\Model\Master\MasterModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransaksihapusController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request)
    {
        # ---------------
        $uri = getUrl() . "/index";
        # ---------------
        $qMenu = new MenuModel;
        $rs = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent = $rs[0]->parent_name;
        $this->PROT_ModuleName = $rs[0]->name;
        $this->PROT_ModuleId = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent" => $this->PROT_Parent, "SHR_Module" => $this->PROT_ModuleName));
    }

    public function index(Request $request, $page = null)
    {
        $data["title"] = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_disposal/index";
        $data["active_page"] = (empty($page)) ? 1 : $page;
        $data["offset"] = (empty($data["active_page"])) ? 0 : ($data["active_page"] - 1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu = new MenuModel;
        $qSales = new TransakhapusModel;
        $qMaster = new MasterModel;
        # ---------------
        $data["action"] = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        $data["filtered_info"] = array();
        $qStatus = getStatusproses();
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"] = array(
            array(
                "label" => "ID", "name" => "id_asset", "align" => "center", "item-align" => "center", "item-format" => "checkbox", "item-class" => "", "width" => "5%", "add-style" => ""
            ),
            array(
                "label" => "No Asset", "name" => "no_asset", "align" => "center", "item-align" => "left", "item-format" => "normal", "item-class" => "", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Nama Barang", "name" => "nama_barang", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Tgl Hapus", "name" => "tgl_hapus", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Keterangan", "name" => "keterangan", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Approval", "name" => "status_approval", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),



        );
        # ---------------
        if ($request->has('text_search')) {
            session(["SES_SEARCH_DISPOSAL" => $request->input("text_search")]);
            # ---------------
            $data["text_search"] = $request->session()->get("SES_SEARCH_DISPOSAL");
        } else {
            $data["text_search"] = $request->session()->get("SES_SEARCH_DISPOSAL");
        }
        # ---------------
        $data["select"] = $qSales->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"] = $qSales->getList($request->input("text_search"));
        # ---------------
        $data["record"] = count($data["query"]);
        $data["pagging"] = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add()
    {
        $data["title"] = "Add Asset B";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_disposal/save";
        /* ----------
         Asset
        ----------------------- */
        $qMaster = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;
      
        /* ----------
        
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qAsset = array_merge($collection, $qMaster->getSelectAssetDetail());
        $qDisposal = array_merge($collection, $qMaster->getSelectDisposal());
      
        $data["fields"][] = form_select(array("name" => "id_asset", "label" => "Asset", "mandatory" => "yes", "source" => $qAsset));
        $data["fields"][] = form_datepicker(array("name" => "tgl_hapus", "label" => "Tgl Disposal", "mandatory" => "yes", "value" => date("d/m/Y"), "first_selected" => ""));
        $data["fields"][] = form_select(array("name" => "id_disposal", "label" => "Alasan Disposal", "mandatory" => "yes", "source" => $qDisposal));
     
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => ""));
     
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function save(Request $request)
    {
        $rules = array(

            'id_asset' => 'required'
        );

        $messages = ['id_asset.required' => 'Asset harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_disposal/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qAsset = new TransakhapusModel;
            # ---------------
            $qAsset->createData($request);
            # ---------------
            session()->flash("success_message", "Asset disposal has been saved");
            # ---------------
            return redirect("/trans_disposal/index");
        }
    }

    public function edit($id)
    {
        $data["title"] = "Edit Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_disposal/update";
        
  /* ----------
         Asset
        ----------------------- */
        $qMaster = new MasterModel;
        /* ----------
         Source
        ----------------------- */

        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;


        $qTransHapus = new TransakhapusModel;
        /* ----------
         Source
        ----------------------- */
        $qTransHapus = $qTransHapus->getProfile($id)->first();
       
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qAsset = array_merge($collection, $qMaster->getSelectAsset());
        $qDisposal = array_merge($collection, $qMaster->getSelectDisposal());
    
       // dd($qTransHapus);
        $data["fields"][] = form_hidden(array("name" => "id_disposal_asset", "label" => "id_disposal_asset", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));
        $data["fields"][] = form_select(array("name" => "id_asset", "label" => "Asset", "mandatory" => "yes", "source" => $qAsset, "value" => $qTransHapus->id_asset));
         $data["fields"][] = form_datepicker(array("name" => "tgl_hapus", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" =>displayDMY($qTransHapus->tgl_hapus,"/")));
        $data["fields"][] = form_select(array("name" => "id_disposal", "label" => "Alasan Disposal.", "mandatory" => "yes", "source" => $qDisposal, "value" => $qTransHapus->id_disposal));
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => "", "value" => $qTransHapus->keterangan));

   
      


        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Update"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update(Request $request)
    {
       // dd($request);
        $rules = array(
            'nama_asset' => 'id_asset|'
        );

        $messages = [
            'nama_asset.required' => 'Asset harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_disposal/edit/" . $request->input("id_asset"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qTransHapus = new TransakhapusModel;
            # ---------------
            $qTransHapus->updateData($request);
            # ---------------
            session()->flash("success_message", "Asset has been updated");
            # ---------------
            return redirect("/trans_disposal/index");
        }
    }

    public function delete($id)
    {
        $data["title"] = "Delete Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_disposal/remove";
        /* ----------
         Source
        ----------------------- */
        $qAsset = new AssetModel;
        $qAsset = $qAsset->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $data["fields"][] = form_hidden(array("name" => "id_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "DELETE"));
        $data["fields"][] = form_text(array("name" => "nama_asset", "label" => "Asset", "readonly" => "readonly", "mandatory" => "yes", "value" => $qAsset->nama_asset));
        
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Delete"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request)
    {
        if ($request->input("id") != 1) {
            $qAsset = new AssetModel;
            # ---------------
            $qAsset->removeData($request);
            # ---------------
            session()->flash("success_message", "Asset has been removed");
        } else {
            session()->flash("error_message", "Asset cannot be removed");
        }
      # ---------------
        return redirect("/trans_disposal/index");
    }
  
}
