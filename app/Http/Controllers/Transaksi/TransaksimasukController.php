<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use File;
use App\User;
use App\Model\MenuModel;
use App\Model\Transaksi\TransaksimasukModel;
use App\Model\Master\MasterModel;

class TransaksimasukController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request)
    {
        # ---------------
        $uri = getUrl() . "/index";
        # ---------------
        $qMenu = new MenuModel;
        $rs = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent = $rs[0]->parent_name;
        $this->PROT_ModuleName = $rs[0]->name;
        $this->PROT_ModuleId = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent" => $this->PROT_Parent, "SHR_Module" => $this->PROT_ModuleName));
    }

    public function index(Request $request, $page = null)
    {
        $data["title"] = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_masuk/index";
        $data["active_page"] = (empty($page)) ? 1 : $page;
        $data["offset"] = (empty($data["active_page"])) ? 0 : ($data["active_page"] - 1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu = new MenuModel;
        $qSales = new TransaksimasukModel;
        $qMaster = new MasterModel;
        # ---------------
        $data["action"] = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        $data["filtered_info"] = array();
        $qStatus = getStatusproses();
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"] = array(
            array(
                "label" => "ID", "name" => "id", "align" => "center", "item-align" => "center", "item-format" => "checkbox", "item-class" => "", "width" => "5%", "add-style" => ""
            ),
            array(
                "label" => "No Transaksi", "name" => "no_transaksi", "align" => "center", "item-align" => "left", "item-format" => "normal", "item-class" => "", "width" => "25%", "add-style" => ""
            ),
            // array(
            //     "label" => "Nama Barang", "name" => "nama_barang", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "20%", "add-style" => ""
            // ),
            // array(
            //     "label" => "Tgl Perolehan", "name" => "tgl_perolehan", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "20%", "add-style" => ""
            // ),
            // array(
            //     "label" => "Harga", "name" => "harga_perolehan", "align" => "center", "item-align" => "left", "item-format" => "number", "width" => "10%", "add-style" => ""
            // ),
            // array(
            //     "label" => "Keterangan", "name" => "keterangan", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            // ),
            array(
                "label" => "Status Approval", "name" => "status", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "10%", "add-style" => ""
            ),



        );
        # ---------------
        if ($request->has('text_search')) {
            session(["SES_SEARCH_ASSET" => $request->input("text_search")]);
            # ---------------
            $data["text_search"] = $request->session()->get("SES_SEARCH_ASSET");
        } else {
            $data["text_search"] = $request->session()->get("SES_SEARCH_ASSET");
        }
        # ---------------
        $data["select"] = $qSales->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"] = $qSales->getList($request->input("text_search"));
        # ---------------
        $data["record"] = count($data["query"]);
        $data["pagging"] = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add()
    {
        $data["title"] = "Add Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_masuk/save";
        /* ----------
         Asset
        ----------------------- */
       
        /* ----------
         Source
        ----------------------- */
        $qMaster = new MasterModel;
        $qGroups = $qMaster->getSelectGroup();
       
      
        /* ----------
        
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];

        $qBarang = array_merge($collection, $qMaster->getSelectBarang());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());

        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());
        $qStatusasset = array_merge($collection, getSelectStatusAsset());
        $qStatuskondisi = array_merge($collection, getSelectKondisiperolehan());
        $qJref = array_merge($collection, getSelectJenisRef());
        $qReferensi = array_merge($collection, $qMaster->getSelectReferensi());

        $data["default_row"]   = 30;
        $data["kondisi_pembelian"]  = $qStatuskondisi;
        $data["status_asset"]       = $qStatusasset;
        $data["id_barang"]          = $qBarang;
        $data["id_cabang"]          = $qCabang;
        $data["id_departement"]     = $qDepartemen;
        $data["id_lokasi"]          = $qLokasi;

        // $data["fields"][] = form_select(array("name" => "id_barang", "label" => "Barang", "mandatory" => "yes", "source" => $qBarang));
        // $data["fields"][] = form_text(array("name" => "no_asset", "label" => "No Asset", "mandatory" => "yes"));
        $data["fields"][] = form_text(array("name" => "no_transaksi", "label" => "No Transaksi"));
        $data["fields"][] = form_datepicker(array("name" => "tgl_perolehan", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" => date("d/m/Y"), "first_selected" => ""));
        // $data["fields"][] = form_select(array("name" => "kondisi_pembelian", "label" => "Barang Baru/Bekas", "mandatory" => "yes", "source" => $qStatuskondisi));
        // $data["fields"][] = form_select(array("name" => "status_asset", "label" => "Cara Kepemilikan", "mandatory" => "yes", "source" => $qStatusasset));
        // $data["fields"][] = form_select(array("name" => "id_cabang", "label" => "Cabang", "mandatory" => "yes", "source" => $qCabang));
        // $data["fields"][] = form_select(array("name" => "id_departement", "label" => "Departemen", "mandatory" => "yes", "source" => $qDepartemen));

        // $data["fields"][] = form_select(array("name" => "id_lokasi", "label" => "Lokasi/PIC", "mandatory" => "yes", "source" => $qLokasi));
        // $data["fields"][] = form_currency(array("name" => "harga_perolehan", "label" => "Harga", "mandatory" => "", "value" => number_format(0)), 0);
        // $data["fields"][] = form_text(array("name" => "serial_no", "label" => "Serial No", "mandatory" => ""));
        $data["fields"][] = form_select(array("name" => "id_referensi", "label" => "Dokumen Ref.", "mandatory" => "yes", "source" => $qReferensi));

        $vurl                   = url('')."/app/foto_timeoff/";
        $pathImage              = url('')."/app/img/icon/remove.png";
        $vurlDelete             = url('')."/pengajuantm/deletefile/";
        $data["fields"][]       = "<div class=\"form-group\" id=\"fotoDokumen\">
                                        <label class=\"col-md-3 control-label\"></label>
                                        <div class=\"col-md-9\">
                                            <a href=\" $vurl \" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\" id=\"linkFotoDokumen\">Tampilkan Dok</a>
                                        </div>
                                    </div>";

        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form-add", $data);
    }

    public function save(Request $request)
    {
        $rules = array(
            // 'id_barang' => 'required'
        );

        $messages = [
            // 'id_barang.required' => 'Barang harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_masuk/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qAsset = new TransaksiMasukModel;
            # ---------------
            $qAsset->createData($request);
            # ---------------
            session()->flash("success_message", "Asset Baru has been saved");
            # ---------------
            return redirect("/trans_masuk/index");
        }
    }

    public function getdocref($id) {
        $qDocRef  = DB::table('m_referensi')->where('id_referensi', $id)->first();
        return json_encode($qDocRef);
    }

    public function edit($id)
    {
        $data["title"] = "Edit Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_masuk/update";
        
        /* ----------
         Asset
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        $qMaster            = new MasterModel;

        $qGroups            = $qMaster->getSelectGroup();

        $qTransMasukAsset   = new TransaksimasukModel;
        /* ----------
         Source
        ----------------------- */
        $qTransMasuk        = $qTransMasukAsset->getAssetHeader($id)->first();
        $qTransMasukAssets  = $qTransMasukAsset->getAssets($id);
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qBarang = array_merge($collection, $qMaster->getSelectBarang());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qStatusasset = array_merge($collection, getSelectStatusAsset());
        $qStatuskondisi = array_merge($collection, getSelectKondisiperolehan());
        $qReferensi = array_merge($collection, $qMaster->getSelectReferensi());
        $qJref = array_merge($collection, getSelectJenisRef());
        $filepath = public_path() . '/app/photo_asset/' . $qTransMasuk->no_ref;

        $data["kondisi_pembelian"]  = $qStatuskondisi;
        $data["status_asset"]       = $qStatusasset;
        $data["id_barang"]          = $qBarang;
        $data["id_cabang"]          = $qCabang;
        $data["id_departement"]     = $qDepartemen;
        $data["id_lokasi"]          = $qLokasi;
        $data["assets"]             = $qTransMasukAssets;

        $data["fields"][] = form_hidden(array("name" => "id_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));
        $data["fields"][] = form_text(array("name" => "no_transaksi", "label" => "No Transaksi", "value" => $qTransMasuk->no_transaksi));
        $data["fields"][] = form_datepicker(array("name" => "tgl_perolehan", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" => displayDMY($qTransMasuk->tanggal_perolehan, "/")));
        $data["fields"][] = form_select(array("name" => "id_referensi", "label" => "Dokumen Ref.", "mandatory" => "yes", "source" => $qReferensi, "value" => $qTransMasuk->no_ref));
        
        // $data["fields"][] = form_select(array("name" => "id_barang", "label" => "Barang", "mandatory" => "yes", "source" => $qBarang, "value" => $qTransMasuk->id_barang));
        // $data["fields"][] = form_text(array("name" => "no_asset", "label" => "No Asset", "mandatory" => "yes", "value" => $qTransMasuk->no_asset));
        // $data["fields"][] = form_datepicker(array("name" => "tgl_perolehan", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" => displayDMY($qTransMasuk->tgl_perolehan, "/")));
        // $data["fields"][] = form_select(array("name" => "kondisi_pembelian", "label" => "Barang Baru/Bekas", "mandatory" => "yes", "source" => $qStatuskondisi, "value" => $qTransMasuk->kondisi_pembelian));
        // $data["fields"][] = form_select(array("name" => "status_asset", "label" => "Cara Kepemilikan", "mandatory" => "yes", "source" => $qStatusasset, "value" => $qTransMasuk->status_asset));
        // $data["fields"][] = form_select(array("name" => "id_cabang", "label" => "Cabang", "mandatory" => "yes", "source" => $qCabang, "value" => $qTransMasuk->id_cabang));
        // $data["fields"][] = form_select(array("name" => "id_departement", "label" => "Departemen", "mandatory" => "yes", "source" => $qDepartemen, "value" => $qTransMasuk->id_departemen));

        // $data["fields"][] = form_select(array("name" => "id_lokasi", "label" => "Lokasi/PIC", "mandatory" => "yes", "source" => $qLokasi, "value" => $qTransMasuk->id_lokasi));
        // $data["fields"][] = form_currency(array("name" => "harga_perolehan", "label" => "Harga", "mandatory" => "", "value" => number_format(0)), 0);
        // $data["fields"][] = form_text(array("name" => "serial_no", "label" => "Serial No", "mandatory" => "", "value" => $qTransMasuk->serial_no));
        // $data["fields"][] = form_select(array("name" => "id_referensi", "label" => "Dokumen Ref.", "mandatory" => "yes", "source" => $qReferensi, "value" => $qTransMasuk->id_referensi));
        // if (empty($qTransMasuk->gambar_asset)) {
        //     $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Asset", "mandatory" => "yes", "value" => $filepath));

        // } else {
        //     $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Asset", "mandatory" => "", "value" => $filepath));

        // }
        // $data["fields"][] = form_hidden(array("name" => "nama_gambar", "label" => "Nama Gambar", "mandatory" => "", "value" => $qTransMasuk->gambar_asset));
    
        // //$data["fields"][] = form_upload3(array("name" => "foto2", "label" => "Foto"));
        // if (!empty($qTransMasuk->gambar_asset)) {

        //     $vurl = url('') . "/app/photo_asset/" . $qTransMasuk->gambar_asset;
        //     $pathImage = url('') . "/app/img/icon/remove.png";
        //     $vurlDelete = url('') . "/trans_masuk/deletefile/" . $id . "/" . $qTransMasuk->gambar_asset;
        //     $data["fields"][] = "<div class=\"form-group\">
        //                               <label class=\"col-md-3 control-label\"></label>
        //                               <div class=\"col-md-9\">
        //                                         <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a><a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>
        //                               </div></div>";
        // }
        // $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => "", "value" => $qTransMasuk->keterangan));
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Update"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form-edit", $data);
    }

    public function editV1($id)
    {
        $data["title"] = "Edit Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_masuk/update";
        
  /* ----------
         Asset
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        $qMaster = new MasterModel;

        $qGroups = $qMaster->getSelectGroup();


        $qTransMasuk = new TransaksimasukModel;
        /* ----------
         Source
        ----------------------- */
        $qTransMasuk = $qTransMasuk->getProfile($id)->first();
       
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qBarang = array_merge($collection, $qMaster->getSelectBarang());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qStatusasset = array_merge($collection, getSelectStatusAsset());
        $qStatuskondisi = array_merge($collection, getSelectKondisiperolehan());
        $qReferensi = array_merge($collection, $qMaster->getSelectReferensi());
        $qJref = array_merge($collection, getSelectJenisRef());
        $filepath = public_path() . '/app/photo_asset/' . $qTransMasuk->gambar_asset;
 
       // dd($qTransMasuk);
        $data["fields"][] = form_hidden(array("name" => "id_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));
        $data["fields"][] = form_select(array("name" => "id_barang", "label" => "Barang", "mandatory" => "yes", "source" => $qBarang, "value" => $qTransMasuk->id_barang));
        $data["fields"][] = form_text(array("name" => "no_asset", "label" => "No Asset", "mandatory" => "yes", "value" => $qTransMasuk->no_asset));
        $data["fields"][] = form_datepicker(array("name" => "tgl_perolehan", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" => displayDMY($qTransMasuk->tgl_perolehan, "/")));
        $data["fields"][] = form_select(array("name" => "kondisi_pembelian", "label" => "Barang Baru/Bekas", "mandatory" => "yes", "source" => $qStatuskondisi, "value" => $qTransMasuk->kondisi_pembelian));
        $data["fields"][] = form_select(array("name" => "status_asset", "label" => "Cara Kepemilikan", "mandatory" => "yes", "source" => $qStatusasset, "value" => $qTransMasuk->status_asset));
        $data["fields"][] = form_select(array("name" => "id_cabang", "label" => "Cabang", "mandatory" => "yes", "source" => $qCabang, "value" => $qTransMasuk->id_cabang));
        $data["fields"][] = form_select(array("name" => "id_departement", "label" => "Departemen", "mandatory" => "yes", "source" => $qDepartemen, "value" => $qTransMasuk->id_departemen));

        $data["fields"][] = form_select(array("name" => "id_lokasi", "label" => "Lokasi/PIC", "mandatory" => "yes", "source" => $qLokasi, "value" => $qTransMasuk->id_lokasi));
        $data["fields"][] = form_currency(array("name" => "harga_perolehan", "label" => "Harga", "mandatory" => "", "value" => number_format(0)), 0);
        $data["fields"][] = form_text(array("name" => "serial_no", "label" => "Serial No", "mandatory" => "", "value" => $qTransMasuk->serial_no));
        $data["fields"][] = form_select(array("name" => "id_referensi", "label" => "Dokumen Ref.", "mandatory" => "yes", "source" => $qReferensi, "value" => $qTransMasuk->id_referensi));
        if (empty($qTransMasuk->gambar_asset)) {
            $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Asset", "mandatory" => "yes", "value" => $filepath));

        } else {
            $data["fields"][] = form_upload(array("name" => "foto", "label" => "Photo Asset", "mandatory" => "", "value" => $filepath));

        }
        $data["fields"][] = form_hidden(array("name" => "nama_gambar", "label" => "Nama Gambar", "mandatory" => "", "value" => $qTransMasuk->gambar_asset));
    
        //$data["fields"][] = form_upload3(array("name" => "foto2", "label" => "Foto"));
        if (!empty($qTransMasuk->gambar_asset)) {

            $vurl = url('') . "/app/photo_asset/" . $qTransMasuk->gambar_asset;
            $pathImage = url('') . "/app/img/icon/remove.png";
            $vurlDelete = url('') . "/trans_masuk/deletefile/" . $id . "/" . $qTransMasuk->gambar_asset;
            $data["fields"][] = "<div class=\"form-group\">
                                      <label class=\"col-md-3 control-label\"></label>
                                      <div class=\"col-md-9\">
                                                <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a><a href=\" $vurlDelete \"><img src=\" $pathImage  \" width=\"15px\" /></a>
                                      </div></div>";
        }
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => "", "value" => $qTransMasuk->keterangan));

   
      


        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Update"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form-edit", $data);
    }

    public function update(Request $request)
    {
        $rules = array(
            // 'nama_asset' => 'id_asset|'
        );

        $messages = [
            // 'nama_asset.required' => 'Asset harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_masuk/edit/" . $request->input("id_asset"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qTransMasuk = new TransaksimasukModel;
            # ---------------
            $qTransMasuk->updateData($request);
            # ---------------
            session()->flash("success_message", "Asset has been updated");
            # ---------------
            return redirect("/trans_masuk/index");
        }
    }

    public function delete($id)
    {
        $data["title"] = "Delete Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_masuk/remove";
        
        /*
        Asset
        ----------------------- */
        $qMaster = new MasterModel;
        /* ----------
         Source
        ----------------------- */

        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;

        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;

        $qTransMasuk = new TransaksimasukModel;
        /* ----------
         Source
        ----------------------- */
        $qTransMasuk = $qTransMasuk->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qBarang = array_merge($collection, $qMaster->getSelectBarang());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qStatusasset = array_merge($collection, getSelectStatusAsset());
        $qStatuskondisi = array_merge($collection, getSelectKondisiperolehan());
        $qReferensi = array_merge($collection, $qMaster->getSelectReferensi());
        $data["fields"][] = form_hidden(array("name" => "id_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "DELETE"));
        $data["fields"][] = form_select(array("name" => "id_barang", "label" => "Barang", "mandatory" => "yes", "source" => $qBarang, "value" => $qTransMasuk->id_barang));
        $data["fields"][] = form_text(array("name" => "no_asset", "label" => "No Asset", "mandatory" => "yes", "value" => $qTransMasuk->no_asset));
        $data["fields"][] = form_datepicker(array("name" => "tgl_perolehan", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" => displayDMY($qTransMasuk->tgl_perolehan, "/")));
     
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Delete"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request)
    {
        if ($request->input("id") != 1) {

            $qTransMasuk = new TransaksimasukModel;
            # ---------------
            $qTransMasuk->removeData($request);
            # ---------------
            session()->flash("success_message", "Asset has been removed");
        } else {
            session()->flash("error_message", "Asset cannot be removed");
        }
      # ---------------
        return redirect("/trans_masuk/index");
    }
    public function deletefile($id, $namafile)
    {
        $tm_timeoff = DB::table('m_asset')->where('id_asset', $id)->first();
        $file_path = public_path() . '/app/photo_asset/' . $tm_timeoff->gambar_asset;
        File::delete($file_path);
        DB::table('m_asset')->where('id_asset', $id)->update(['gambar_asset' => null]);
        session()->flash('success_message', 'Foto berhasil dihapus.');
      # ---------------
        return redirect("/trans_masuk/edit/" . $id);
    }
    public function approve($id)
    {
        $data["title"] = "Approval Promosi/Mutasi/Demosi";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_masuk/prosesapprove";

       /* ----------
         Asset
        ----------------------- */
        /* ----------
         Source
        ----------------------- */
        $qMaster = new MasterModel;

        $qGroups = $qMaster->getSelectGroup();

        $qApprove = getStatusproses();

        $qTransMasuk = new TransaksimasukModel;
        /* ----------
         Source
        ----------------------- */
        $qTransMasuk = $qTransMasuk->getProfile($id)->first();
       
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qBarang = array_merge($collection, $qMaster->getSelectBarang());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qStatusasset = array_merge($collection, getSelectStatusAsset());
        $qStatuskondisi = array_merge($collection, getSelectKondisiperolehan());
        $qReferensi = array_merge($collection, $qMaster->getSelectReferensi());
        $qJref = array_merge($collection, getSelectJenisRef());
        $filepath = public_path() . '/app/photo_asset/' . $qTransMasuk->gambar_asset;
 
       // dd($qTransMasuk);
        $data["fields"][] = form_hidden(array("name" => "id_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));
        $data["fields"][] = form_select(array("name" => "id_barang", "label" => "Barang", "mandatory" => "yes", "source" => $qBarang, "value" => $qTransMasuk->id_barang));
        $data["fields"][] = form_text(array("name" => "no_asset", "label" => "No Asset", "mandatory" => "yes", "value" => $qTransMasuk->no_asset));
        $data["fields"][] = form_datepicker(array("name" => "tgl_perolehan", "label" => "Tgl Perolehan", "mandatory" => "yes", "value" => displayDMY($qTransMasuk->tgl_perolehan, "/")));
        $data["fields"][] = form_select(array("name" => "kondisi_pembelian", "label" => "Barang Baru/Bekas", "mandatory" => "yes", "source" => $qStatuskondisi, "value" => $qTransMasuk->kondisi_pembelian));
        $data["fields"][] = form_select(array("name" => "status_asset", "label" => "Cara Kepemilikan", "mandatory" => "yes", "source" => $qStatusasset, "value" => $qTransMasuk->status_asset));
        $data["fields"][] = form_select(array("name" => "id_cabang", "label" => "Cabang", "mandatory" => "yes", "source" => $qCabang, "value" => $qTransMasuk->id_cabang));
        $data["fields"][] = form_select(array("name" => "id_departement", "label" => "Departemen", "mandatory" => "yes", "source" => $qDepartemen, "value" => $qTransMasuk->id_departemen));

        $data["fields"][] = form_select(array("name" => "id_lokasi", "label" => "Lokasi/PIC", "mandatory" => "yes", "source" => $qLokasi, "value" => $qTransMasuk->id_lokasi));
        $data["fields"][] = form_currency(array("name" => "harga_perolehan", "label" => "Harga", "mandatory" => "", "value" => number_format(0)), 0);
        $data["fields"][] = form_text(array("name" => "serial_no", "label" => "Serial No", "mandatory" => "", "value" => $qTransMasuk->serial_no));
        $data["fields"][] = form_select(array("name" => "id_referensi", "label" => "Dokumen Ref.", "mandatory" => "yes", "source" => $qReferensi, "value" => $qTransMasuk->id_referensi));
        $data["fields"][] = form_hidden(array("name" => "nama_gambar", "label" => "Nama Gambar", "mandatory" => "", "value" => $qTransMasuk->gambar_asset));
    
        //$data["fields"][] = form_upload3(array("name" => "foto2", "label" => "Foto"));
        if (!empty($qTransMasuk->gambar_asset)) {

            $vurl = url('') . "/app/photo_asset/" . $qTransMasuk->gambar_asset;
            $pathImage = url('') . "/app/img/icon/remove.png";
            $vurlDelete = url('') . "/trans_masuk/deletefile/" . $id . "/" . $qTransMasuk->gambar_asset;
            $data["fields"][] = "<div class=\"form-group\">
                                      <label class=\"col-md-3 control-label\"></label>
                                      <div class=\"col-md-9\">
                                                <a href=\" $vurl \" target=\"_blank\" target=\"_blank\" class=\"btn btn-sm btn-warning m-r-5\">Tampilkan Foto</a>
                                      </div></div>";
        }
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => "", "value" => $qTransMasuk->keterangan));


        $data["fields"][] = form_select(array("name" => "status_approve", "label" => "Approval", "mandatory" => "yes", "source" => $qApprove, "value" => ""));

        $data["fields"][] = form_text(array("name" => "Komentar", "label" => "Komentar", "value" => ""));
         


     # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }
    
    public function prosesapprove(Request $request)
    {

  // dd($request);
        $rules = array(
            'nama_asset' => 'id_asset|'
        );

        $messages = [
            'nama_asset.required' => 'Asset harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_masuk/edit/" . $request->input("id_asset"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qTransMasuk = new TransaksimasukModel;
    # ---------------
            $qTransMasuk->ApproveData($request);
    # ---------------
            session()->flash("success_message", "Asset has been approved");
    # ---------------
            return redirect("/trans_masuk/index");
        }

    }
}
