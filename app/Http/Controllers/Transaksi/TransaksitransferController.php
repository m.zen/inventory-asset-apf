<?php

namespace App\Http\Controllers\Transaksi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use Validator;
use Hash;
use File;
use App\User;
use App\Model\MenuModel;
use App\Model\Transaksi\TransaktransferModel;
use App\Model\Transaksi\TransaksimasukModel;
use App\Model\Master\MasterModel;

class TransaksitransferController extends Controller
{
    protected $PROT_SideMenu, $PROT_Parent, $PROT_ModuleId, $PROT_ModuleName;

    public function __construct(Request $request)
    {
        # ---------------
        $uri = getUrl() . "/index";
        # ---------------
        $qMenu = new MenuModel;
        $rs = $qMenu->getParentMenu($uri);
        # ---------------
        $this->PROT_Parent = $rs[0]->parent_name;
        $this->PROT_ModuleName = $rs[0]->name;
        $this->PROT_ModuleId = $rs[0]->id;
        # ---------------
        View::share(array("SHR_Parent" => $this->PROT_Parent, "SHR_Module" => $this->PROT_ModuleName));
    }

    public function index(Request $request, $page = null)
    {
        $data["title"] = ucwords(strtolower($this->PROT_ModuleName));
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_transfer/index";
        $data["active_page"] = (empty($page)) ? 1 : $page;
        $data["offset"] = (empty($data["active_page"])) ? 0 : ($data["active_page"] - 1) * Auth::user()->perpage;
        /* ----------
         Action
        ----------------------- */
        $qMenu = new MenuModel;
        $qSales = new TransaktransferModel;
        $qMaster = new MasterModel;
        # ---------------
        $data["action"] = $qMenu->getActionMenu(Auth::user()->group_id, $this->PROT_ModuleId);
        $data["filtered_info"] = array();
        $qStatus = getStatusproses();
        /* ----------
         Table header
        ----------------------- */
        $data["table_header"] = array(
            array(
                "label" => "ID", "name" => "id_transfer_asset", "align" => "center", "item-align" => "center", "item-format" => "checkbox", "item-class" => "", "width" => "5%", "add-style" => ""
            ),
            array(
                "label" => "Nama Barang", "name" => "nama_barang", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Tgl Transfer", "name" => "tgl_transfer", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Cabang Asal", "name" => "daricabang", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Cabang Tujuan", "name" => "kecabang", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),
            array(
                "label" => "Approval", "name" => "status_proses", "align" => "center", "item-align" => "left", "item-format" => "normal", "width" => "25%", "add-style" => ""
            ),




        );
        # ---------------
        if ($request->has('text_search')) {
            session(["SES_SEARCH_ASSET_TRANSFER" => $request->input("text_search")]);
            # ---------------
            $data["text_search"] = $request->session()->get("SES_SEARCH_ASSET_TRANSFER");
        } else {
            $data["text_search"] = $request->session()->get("SES_SEARCH_ASSET_TRANSFER");
        }
        # ---------------
        $data["select"] = $qSales->getList($request->input("text_search"), $data["offset"], Auth::user()->perpage);
        $data["query"] = $qSales->getList($request->input("text_search"));
        # ---------------
        $data["record"] = count($data["query"]);
        $data["pagging"] = getPagging($data["active_page"], $data["record"], $data["form_act"]);
        # ---------------
        return view("default.list", $data);
    }

    public function add()
    {
        $data["title"] = "Add Transfer Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_transfer/save";
        $data["url_select"] = "/trans_transfer/get_profile/";
        /* ----------
         Asset
        ----------------------- */
        $qMaster = new MasterModel;
        /* ----------
         Source
        ----------------------- */
        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;
      
        /* ----------
        
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qAsset = array_merge($collection, $qMaster->getSelectAssetDetail());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());




        $data["fields"][] = form_select(array("name" => "id_asset", "label" => "Asset", "mandatory" => "yes", "source" => $qAsset));
        $data["fields"][] = form_hidden(array("name" => "dari_cabang", "label" => "Cabang Asal", "readonly" => "readonly"));
        $data["fields"][] = form_text(array("name" => "nama_cabang", "label" => "Cabang Asal", "readonly" => "readonly"));
        $data["fields"][] = form_hidden(array("name" => "dari_departemen", "label" => "Departemen Asal", "readonly" => "readonly"));
        $data["fields"][] = form_text(array("name" => "nama_departemen", "label" => "Departemen Asal", "readonly" => "readonly"));
        $data["fields"][] = form_hidden(array("name" => "dari_lokasi", "label" => "PIC/Lokasi Asal", "readonly" => "readonly"));
        $data["fields"][] = form_text(array("name" => "nama_lokasi", "label" => "PIC/Lokasi Asal", "readonly" => "readonly"));
        $data["fields"][] = form_select(array("name" => "ke_cabang", "label" => "Cabang Tujuan", "mandatory" => "yes", "source" => $qCabang));
        $data["fields"][] = form_select(array("name" => "ke_departemen", "label" => "Cabang Tujuan", "mandatory" => "yes", "source" => $qCabang));

        $data["fields"][] = form_select(array("name" => "ke_lokasi", "label" => "Lokasi/PIC Tujuan", "mandatory" => "yes", "source" => $qLokasi));
        $data["fields"][] = form_datepicker(array("name" => "tgl_transfer", "label" => "Tgl Transfer", "mandatory" => "yes", "value" => date("d/m/Y"), "first_selected" => ""));
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => ""));
        
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------

        return view("default.formtransfer", $data);
    }

    public function save(Request $request)
    {
        $rules = array(

            'id_asset' => 'required'
        );

        $messages = ['id_asset.required' => 'Barang harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_transfer/add")
                ->withErrors($validator)
                ->withInput();
        } else {
            $qAsset = new TransaktransferModel;
            # ---------------
            $qAsset->createData($request);
            # ---------------
            session()->flash("success_message", "Asset Baru has been saved");
            # ---------------
            return redirect("/trans_transfer/index");
        }
    }

    public function edit($id)
    {
        $data["title"]      = "Edit Transfer Asset";
        $data["parent"]     = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"]   = "/trans_transfer/update";
        $data["url_select"] = "/trans_transfer/get_profile/";

        $qMaster = new MasterModel;
        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;


        $qTransTransfer = new TransaktransferModel;
        /* ----------
         Source
        ----------------------- */
        $qTransTransfer = $qTransTransfer->getProfile($id)->first();
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qAsset         = array_merge($collection, $qMaster->getSelectAssetDetail());
        $qCabang        = array_merge($collection, $qMaster->getSelectCabang());
        $qDepartemen    = array_merge($collection, $qMaster->getSelectDepartemen());
        $qLokasi        = array_merge($collection, $qMaster->getSelectLokasi());
       // dd($qTransTransfer);
        $data["fields"][] = form_hidden(array("name" => "id_transfer_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));

        $data["fields"][] = form_select(array("name" => "id_asset", "label" => "Asset", "mandatory" => "yes", "source" => $qAsset, "value" => $qTransTransfer->id_asset));
        $data["fields"][] = form_hidden(array("name" => "dari_cabang", "label" => "Cabang Asal", "readonly" => "readonly", "value" => $qTransTransfer->dari_cabang));
        $data["fields"][] = form_hidden(array("name" => "dari_departemen", "label" => "Departemen Asal", "readonly" => "readonly", "value" => $qTransTransfer->dari_departemen));
        $data["fields"][] = form_hidden(array("name" => "dari_lokasi", "label" => "PIC/Lokasi Asal", "readonly" => "readonly", "value" => $qTransTransfer->dari_lokasi));
        $data["fields"][] = form_text(array("name" => "nama_cabang", "label" => "Cabang Asal", "readonly" => "readonly", "value" => $qTransTransfer->nama_cabang));
        $data["fields"][] = form_text(array("name" => "nama_departemen", "label" => "Departemen Asal", "readonly" => "readonly", "value" => $qTransTransfer->nama_departemen));
        $data["fields"][] = form_text(array("name" => "nama_lokasi", "label" => "PIC/Lokasi Asal", "readonly" => "readonly", "value" => $qTransTransfer->nama_lokasi));
        $data["fields"][] = form_select(array("name" => "ke_cabang", "label" => "Cabang Tujuan", "mandatory" => "yes", "source" => $qCabang, "value" => $qTransTransfer->ke_cabang));
        $data["fields"][] = form_select(array("name" => "ke_departemen", "label" => "Cabang Tujuan", "mandatory" => "yes", "source" => $qDepartemen, "value" => $qTransTransfer->ke_departemen));
        $data["fields"][] = form_select(array("name" => "ke_lokasi", "label" => "Lokasi/PIC Tujuan", "mandatory" => "yes", "source" => $qLokasi, "value" => $qTransTransfer->ke_lokasi));
        $data["fields"][] = form_datepicker(array("name" => "tgl_transfer", "label" => "Tgl Transfer", "mandatory" => "yes", "value" => date("d/m/Y"), "first_selected" => ""));
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => ""));
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Update"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.formtransfer", $data);
    }

    public function update(Request $request)
    {
       // dd($request);
        $rules = array(
            'nama_asset' => 'id_asset|'
        );

        $messages = [
            'nama_asset.required' => 'Asset harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/trans_transfer/edit/" . $request->input("id_asset"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qTransTransfer = new TransaktransferModel;
            # ---------------
            $qTransTransfer->updateData($request);
            # ---------------
            session()->flash("success_message", "Asset has been updated");
            # ---------------
            return redirect("/trans_transfer/index");
        }
    }

    public function delete($id)
    {
        $data["title"] = "Delete Transfer Asset";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_transfer/remove";

        $qMaster = new MasterModel;
       /* ----------
        Source
       ----------------------- */

        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;


        $qTransTransfer = new TransaktransferModel;
       /* ----------
        Source
       ----------------------- */
        $qTransTransfer = $qTransTransfer->getProfile($id)->first();
        /* ----------
         Source
        ----------------------- */

        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;


        $qTransTransfer = new TransaktransferModel;
        /* ----------
         Source
        ----------------------- */
        $qTransTransfer = $qTransTransfer->getProfile($id)->first();

        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qAsset = array_merge($collection, $qMaster->getSelectAsset());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());


        $data["fields"][] = form_hidden(array("name" => "id_transfer_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "DELETE"));

        $data["fields"][] = form_select(array("name" => "id_asset", "label" => "Asset", "mandatory" => "yes", "source" => $qAsset, "value" => $qTransTransfer->id_asset));
 
        # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "Delete"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function remove(Request $request)
    {
        if ($request->input("id") != 1) {
           // $qAsset = new AssetModel;
            $qTransTransfer = new TransaktransferModel;
            # ---------------
            $qTransTransfer->removeData($request);
            # ---------------
            session()->flash("success_message", "Asset has been removed");
        } else {
            session()->flash("error_message", "Asset cannot be removed");
        }
      # ---------------
        return redirect("/trans_transfer/index");
    }
    public function deletefile($id, $namafile)
    {
        $tm_timeoff = DB::table('m_asset')->where('id_asset', $id)->first();
        $file_path = public_path() . '/app/photo_asset/' . $tm_timeoff->gambar_asset;
        File::delete($file_path);
        DB::table('m_asset')->where('id_asset', $id)->update(['gambar_asset' => null]);
        session()->flash('success_message', 'Foto berhasil dihapus.');
      # ---------------
        return redirect("/trans_transfer/edit/" . $id);
    }
    public function get_profile($id)
    {

        $qTransMasuk = new TransaksimasukModel;
        /* ----------
         Source
        ----------------------- */
        $qTransMasuk = $qTransMasuk->getProfile($id)->first();
       
        
        // Return dalam bentuk json untuk diproses oleh jquery
        return json_encode($qTransMasuk);

    }

    public function approve($id)
    {
        $data["title"] = "Approval Promosi/Mutasi/Demosi";
        $data["parent"] = ucwords(strtolower($this->PROT_Parent));
        $data["form_act"] = "/trans_transfer/update_approve";

        $qMaster = new MasterModel;
        $qGroups = $qMaster->getSelectGroup();
        $qMaster = new MasterModel;
        $qApprove =  getStatusproses();
        


        $qTransTransfer = new TransaktransferModel;
        /* ----------
         Source
        ----------------------- */
        $qTransTransfer = $qTransTransfer->getProfile($id)->first();
      
       
        /* ----------
         Fields
        ----------------------- */
        $collection = [(object)[
            'id' => '-',
            'name' => '--Pilih--'
        ]];
        $qAsset = array_merge($collection, $qMaster->getSelectAsset());
        $qCabang = array_merge($collection, $qMaster->getSelectCabang());
        $qDepartemen = array_merge($collection, $qMaster->getSelectDepartemen());
        $qLokasi = array_merge($collection, $qMaster->getSelectLokasi());
    
       // dd($qTransTransfer);
        $data["fields"][] = form_hidden(array("name" => "id_transfer_asset", "label" => "Asset ID", "readonly" => "readonly", "value" => $id));
        $data["fields"][] = form_hidden(array("name" => "_method", "label" => "Method", "readonly" => "readonly", "value" => "PUT"));

        $data["fields"][] = form_select(array("name" => "id_asset", "label" => "Asset", "mandatory" => "yes", "source" => $qAsset, "value" => $qTransTransfer->id_asset));
        $data["fields"][] = form_text(array("name" => "dari_cabang", "label" => "Cabang Asal", "readonly" => "readonly", "value" => $qTransTransfer->nama_cabang));
        $data["fields"][] = form_text(array("name" => "dari_departemen", "label" => "Departemen Asal", "readonly" => "readonly", "value" => $qTransTransfer->nama_departemen));
        $data["fields"][] = form_text(array("name" => "dari_lokasi", "label" => "PIC/Lokasi Asal", "readonly" => "readonly", "value" => $qTransTransfer->nama_lokasi));
        $data["fields"][] = form_text(array("name" => "cabang_tujuan", "label" => "Cabang Tujuan", "readonly" => "readonly", "value" => $qTransTransfer->cabang_tujuan));
        $data["fields"][] = form_text(array("name" => "departemen_tujuan", "label" => "Departemen Tujuan", "readonly" => "readonly", "value" => (!empty($qTransTransfer->departemen_tujuan)) ? $qTransTransfer->departemen_tujuan : ''));
        $data["fields"][] = form_text(array("name" => "lokasi_tujuan", "label" => "PIC/Lokasi Tujuan", "readonly" => "readonly", "value" => (!empty($qTransTransfer->lokasi_tujuan)) ? $qTransTransfer->lokasi_tujuan : '' ));
       
        $data["fields"][] = form_datepicker(array("name" => "tgl_transfer", "label" => "Tgl Transfer", "mandatory" => "yes", "value" => date("d/m/Y"), "first_selected" => ""));
        $data["fields"][] = form_text(array("name" => "keterangan", "label" => "Keterangan", "mandatory" => ""));
      $data["fields"][] = form_select(array("name" => "status_proses", "label" => "Approval", "mandatory" => "yes", "source" => $qApprove, "value" => ""));

        $data["fields"][] = form_text(array("name" => "Komentar", "label" => "Komentar", "value" => ""));
         


     # ---------------
        $data["buttons"][] = form_button_submit(array("name" => "button_save", "label" => "&nbsp;&nbsp;Save&nbsp;&nbsp;"));
        $data["buttons"][] = form_button_cancel(array("name" => "button_cancel", "label" => "Cancel"));
        # ---------------
        return view("default.form", $data);
    }

    public function update_approve(Request $request)
    {


        $rules = array(
            'no_surat' => 'required|',

            'no_surat' => 'required|'
        );

        $messages = [
            'no_surat.required' => 'No Surat harus diisi',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect("/karir/approve/" . $request->input("id"))
                ->withErrors($validator)
                ->withInput();
        } else {
            $qKarir = new KarirModel;
            # ---------------
            $qKarir->updateData_approve($request);
            # ---------------
            session()->flash("success_message", "Karir has been updated");
            # ---------------
            return redirect("/karir/index");
        }
    }
}
