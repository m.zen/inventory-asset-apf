<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Api\ApiProfileModel;

class ApiProfileController extends Controller
{
     public function ProfileKaryawan(Request $request)
    {
    	 $qProfile  	= new ApiProfileModel;
    	 $nik           = $request->nik;
         $qProfile 		= $qProfile->getProfilekry($nik)->first();
       
        $response 	= [
        	"message" => "Success",
        	"profilekry" => $qProfile,
        	
        ];
        return response()->json($response, 200);

    }
}
