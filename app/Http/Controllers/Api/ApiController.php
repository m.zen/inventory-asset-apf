<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Api\ApiModel;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Support\Facades\Auth;
use App\Model\Master\UserModel;
use App\Model\Absensi\DataasensiModel;

use Validator;
use App\User;
use Auth;




class ApiController extends Controller
{
    //
    use AuthenticatesUsers;
    public function login(Request $request){

    // $mUser       = new ApiModel;
    $credentials = [
    			'nik' => $request->nik,
    			'password' => $request->password
    		];

    
    if(Auth::attempt($credentials)) 
    {
		    $user 			= Auth::user();
		    $qApiModel 		= new ApiModel();
		    $qUser			= $qApiModel->getUser($request->nik)->first();
		    $qPerideoAbsen 	= $qApiModel->getPeriodeAbsen()->first();

            return response()->json(['success' =>"Sukses",'user'=>$qUser,'periodeabsen'=>$qPerideoAbsen], 200);
        }
        else{
            return response()->json(['error'=>'User atau password salah'], 401);
        }
}
 
}
