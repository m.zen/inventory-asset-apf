<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use App\Model\Api\ApiAbsensiModel;
use App\Model\Master\MasterModel;

class Apiabsensicontroller extends Controller
{
    public function Listabsensi(Request $request)
    {
    	 $mAbsensi  	= new ApiAbsensiModel;
    	 $nik           = $request->nik;
    	 $tgl_awal		= $request->tgl_awal;
    	 $tgl_akhir		= $request->tgl_akhir;	

         $qAbsensi 		= $mAbsensi->getApiabsensi($nik,$tgl_awal,$tgl_akhir		);
          $qsumAbsensi 		= $mAbsensi->getApisum_absensi($nik,$tgl_awal,$tgl_akhir);
         

        $response 	= [
        	//"message" => "Success",
        	"dataabsensi" => $qAbsensi,
        	//"sumdataabsensi" => $qsumAbsensi
        ];

        return response()->json($response, 200);


    }
}
