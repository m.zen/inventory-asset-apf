<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use View;
use Auth;
use App\Model\Api\ApiTimeOffModel;
use App\Model\Master\MasterModel;

class ApiTimeOffController extends Controller
{
    public function ListTimeOff(Request $request)
    {
    	 $qTimeOff  	= new ApiTimeOffModel;
    	 $nik           = $request->nik;
    	 $tgl_awal		= $request->tgl_awal;
    	 $tgl_akhir		= $request->tgl_akhir;	

       $qTimeOff 		= $qTimeOff->getTimeOff($nik,$tgl_awal,$tgl_akhir		);
       
        $response 	= [
        	"message" => "Success",
        	"datatimeoff" => $qTimeOff,
        ];

        return response()->json($response, 200);


    }

    public function JenisTimeOff()
    {
        $qTimeOff   = new ApiTimeOffModel;
        

       $qjenisTo        = $qTimeOff->getJenisTimeoff();
       
        $response   = [
            //"message" => "Success",
            "jenistimeoff" =>$qjenisTo,
        ];

        return response()->json($response, 200);
    }
    public function Simpan_Timeoff(Request $request)
    {
        $qTimeOff   = new ApiTimeOffModel;

        if($request->id_status_karyawan == 0){
           
        $response   = [
            "status" =>"Faild",
            "message" =>"Pilih Nama Pengajuan",
           
        ];
        return response()->json($response, 400);


        }
        $qTimeOff   = $qTimeOff->createData($request);
        

    }
}
