<?php
if(!function_exists("getPembulatan")) {
	function getPembulatan($nominal, $pembulatan) {
		$nominal 	= (((int)($nominal / $pembulatan)) * $pembulatan) + $pembulatan;

		return $nominal;
	}
}

if(!function_exists("getNominalDenda")) {
	function getNominalDenda($kewajiban, $hari, $mildenda, $mindenda) {
		if($hari < $mindenda) {
			$denda 	= 0;
		} else {
			$denda 	= $kewajiban * $hari * $mildenda;
		}

		return $denda;
	}
}

if(!function_exists("getSelectKelamin")) {
	function getSelectKelamin() {
		$qSelect		= array(array("id"=>"L", "name"=>"Laki-laki"), array("id"=>"P", "name"=>"Perempuan"));

		return $qSelect;
	}
}
if(!function_exists("getSelectHubungan")) {
	function getSelectHubungan() {
		$qSelect		= array(array("id"=>"Anak", "name"=>"Anak"), array("id"=>"Istri", "name"=>"Istri"),array("id"=>"S", "name"=>"Suami"));

		return $qSelect;
	}
}
if(!function_exists("getListApprove")) {
  	function getListApprove() {
        $data = array(array("id"=>"APPROVE", "name"=>"APPROVE")
        			 ,array("id"=>"REJECT", "name"=>"REJECT")
                 );

        return $data;
    }
}


if(!function_exists("getLabelFlag")) {
	function getLabelFlag($var) {
		switch(strtoupper($var)) {
			case "ACTIVE" :
				$flag 	= "<span class=\"label label-primary\">ACTIVE</span>";
				break;

			case "INACTIVE" :
				$flag 	= "<span class=\"label label-warning\">INACTIVE</span>";
				break;

			case "ANGSURAN" :
				$flag 	= "<span class=\"label label-success\">ANGSURAN</span>";
				break;

			case "PELUNASAN" :
				$flag 	= "<span class=\"label label-danger\">PERMOHONAN DP</span>";
				break;

			case "W-DENDA" :
				$flag 	= "<span class=\"label bg-primary\">W-DENDA</span>";
				break;

			default :
				$flag 	= "<span class=\"label bg-grey\">$var</span>";
		}
		# ---------------
		return $flag;
	}
}

if(!function_exists("getSelectStatusUser")) {
  	function getSelectStatusUser() {
		$qSelect		= array(array("id"=>"1", "name"=>"Active"), array("id"=>"0", "name"=>"Inactive"));

		return $qSelect;
  	}
}






if(!function_exists("getSelectBulan")) {
  	function getSelectBulan() {
        $data = array(array("id"=>1, "name"=>"Januari")
                     ,array("id"=>2, "name"=>"Februari")
                     ,array("id"=>3, "name"=>"Maret")
                     ,array("id"=>4, "name"=>"April")
                     ,array("id"=>5, "name"=>"Mei")
                     ,array("id"=>6, "name"=>"Juni")
                     ,array("id"=>7, "name"=>"Juli")
                     ,array("id"=>8, "name"=>"Agustus")
                     ,array("id"=>9, "name"=>"September")
                     ,array("id"=>10, "name"=>"Oktober")
                     ,array("id"=>11, "name"=>"November")
                     ,array("id"=>12, "name"=>"Desember")	
                     );

        return $data;
    }
}


if(!function_exists("getWarganegara")) {
  	function getWarganegara($var) {
  		switch($var) {
			case "I":
				$keterangan 	= "WNI";
				break;
			case "A":
				$keterangan 	= "WNA";
				break;
		}
		return $keterangan;
    }
}

if(!function_exists("getKondisiKendaraan")) {
  	function getKondisiKendaraan($var) {
  		switch($var) {
			case "N":
				$keterangan 	= "Baru";
				break;
			case "S":
				$keterangan 	= "Bekas";
				break;
		}
		return $keterangan;
    }
}


if(!function_exists("getBulanRomawi")) {
  	function getBulanRomawi($var) {
  		
  		switch($var) {
			case 1:
				$keterangan 	= "I";
				break;
			case 2:
				$keterangan 	= "II";
				break;
			case 3:
				$keterangan 	= "III";
				break;
			
			case "4":
				$keterangan 	= "IV";
				break;
			case 5:
				$keterangan 	= "V";
				break;
			case 6:
				$keterangan 	= "VI";
				break;
			case 7:
				$keterangan 	= "VII";
				break;
			case 8:
				$keterangan 	= "VIII";
				break;
			case 9:
				$keterangan 	= "IX";
				break;
			case 10:
				$keterangan 	= "X";
				break;
			case 11:
				$keterangan 	= "XI";
				break;
			case 12:
				$keterangan 	= "XII";
				break;
			
			
		}
		return $keterangan;
    }
}

if(!function_exists("getJenisLaporan")) {
  	function getJenisLaporan() {
        //$data = array(array("id"=>"csv", "name"=>"Report Absensi")
        //			 ,array("id"=>"formatcsv", "name"=>"Format Absensi")
        //			 ,array("id"=>"formatpdf", "name"=>"Perhitungan Uang Makan")
        //			 ,array("id"=>"i", "name"=>"repair")
        //			 );
	
				 $data = array(array("id"=>"csv", "name"=>"Report Absensi")
        			 ,array("id"=>"formatcsv", "name"=>"Format Absensi")
        			 ,array("id"=>"formatpdf", "name"=>"Perhitungan Uang Makan")
        			 ,array("id"=>"generate", "name"=>"generate")
        			 ,array("id"=>"i", "name"=>"repair")
        			 );
				
        return $data;
    }
}
if(!function_exists("getFilelaporan")) {
  	function getFilelaporan() {
        $data = array(array("id"=>"pdf", "name"=>"PDF")
        			 ,array("id"=>"xls", "name"=>"Excel")
        			
        			 );

        return $data;
    }
}
if(!function_exists("getLaporangaji")) {
  	function getLaporangaji() {
        $data = array(array("id"=>"1", "name"=>"Laporan Detail Gaji")
        			 ,array("id"=>"2", "name"=>"Laporan Summary Gaji")
        			 ,array("id"=>"3", "name"=>"Laporan Kiriman Data Ke  Bank")
        			 );

        return $data;
    }
}


if(!function_exists("getSlipbulan")) {
  	function getSlipbulan() {
        $data = array(array("id"=>"1", "name"=>"Januari")
        			 ,array("id"=>"2", "name"=>"Februari")
        			 ,array("id"=>"3", "name"=>"Maret")
        			 ,array("id"=>"4", "name"=>"April")
        			 ,array("id"=>"5", "name"=>"Mei")
        			 ,array("id"=>"6", "name"=>"Juni")
        			 ,array("id"=>"7", "name"=>"Juli")
        			 ,array("id"=>"8", "name"=>"Agustus")
        			 ,array("id"=>"9", "name"=>"September")
        			 ,array("id"=>"10", "name"=>"Oktober")
        			 ,array("id"=>"11", "name"=>"November")
        			 ,array("id"=>"12", "name"=>"Desember")
        			 );

        return $data;
    }
}

if(!function_exists("getJenisUpload")) {
  	function getJenisUpload() {
        $data = array(
        		      // array("id"=>"txt", "name"=>"file txt")
                //      ,array("id"=>"excel", "name"=>"file excel")
                     array("id"=>"dat", "name"=>"file dat")
                 );

        return $data;
    }
}

if(!function_exists("integerToRoman")) {
	function integerToRoman($integer) {
	// Convert the integer into an integer (just to make sure)
	$integer = intval($integer);
	$result = '';
	
	// Create a lookup array that contains all of the Roman numerals.
	$lookup = array('M' => 1000,
	'CM' => 900,
	'D' => 500,
	'CD' => 400,
	'C' => 100,
	'XC' => 90,
	'L' => 50,
	'XL' => 40,
	'X' => 10,
	'IX' => 9,
	'V' => 5,
	'IV' => 4,
	'I' => 1);
	
	foreach($lookup as $roman => $value){
	// Determine the number of matches
	$matches = intval($integer/$value);
	
	// Add the same number of characters to the string
	$result .= str_repeat($roman,$matches);
	
	// Set the integer to be the remainder of the integer and the value
	$integer = $integer % $value;
 	}
 
 	// The Roman numeral should be built, return it
 	return $result;
	}
}

if(!function_exists("penyebut")) {
	function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
	}
}

  if(!function_exists("getSelectStatusAsset")) {
	function getSelectStatusAsset() {
	  $data = array(array("id"=>"Beli", "name"=>"Beli")
				   ,array("id"=>"Sewa", "name"=>"Sewa")
				   ,array("id"=>"Transfer", "name"=>"Transfer Group")
				   );

	  return $data;
  }
}
if(!function_exists("getSelectStatusFixAsset")) {
	function getSelectStatusFixAsset() {
	  $data = array(array("id"=>"Fix Asset", "name"=>"Fix Asset")
				   ,array("id"=>"Non Fix Asset", "name"=>"No Fix Asset")
			
				   );

	  return $data;
  }
}
if(!function_exists("getSelectKondisiperolehan")) {
	function getSelectKondisiperolehan() {
	  $data = array(array("id"=>"Baru", "name"=>"Baru")
				   ,array("id"=>"Bekas", "name"=>"Bekas")
			
				   );

	  return $data;
  }
}
if(!function_exists("getSelectJenisRef")) {
	function getSelectJenisRef() {
	  $data = array(array("id"=>"MPD", "name"=>"Memo Pengajuan Dana")
				   ,array("id"=>"TTD", "name"=>"Tanda Terima")
			
				   );

	  return $data;
  }
}



if(!function_exists("getStatusproses")) {
	function getStatusproses() {
	  $data = array(array("id"=>"Proses", "name"=>"Proses")
				   ,array("id"=>"Batal", "name"=>"Batal")
				   ,array("id"=>"Approve", "name"=>"Approve")
				   );

	  return $data;
  }
}