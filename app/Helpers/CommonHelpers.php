<?php
if(!function_exists("getUrl")) {
	function getUrl() {
		$url  	= "/" . \Request::segment(1);

		return $url;
	}
}

if(!function_exists("setString")) {
	function setString($var) {
		$var 	= strtoupper(str_replace('"', "", str_replace("'", "", $var)));

		return $var;
	}
}

if(!function_exists("setDate")) {
	function setDate($var) {
		if(($var == "") || ($var == "0000-00-00") || ($var == "____-__-__")) {
			$var 	= null;
		} else {
			$var	= $var;
		}

		return $var;
	}
}

if(!function_exists("isNumber")) {
	function isNumber($var) {
		if(preg_match('/[^0-9]+$/', $var)) {
			return false;
		} else {
			return true;
		}
	}
}

if(!function_exists("setRupiah")) {
    function setRupiah($var) {
        $var   =  number_format($var,0,',','.');

        return $var;
    }
}
if(!function_exists("setYMD")) {
	function setYMD($var, $chr) {
		list($day, $month, $year) = explode($chr, $var);
		# ---------------
		$var = $year . "-" . $month . "-" . $day;
		# ---------------
		return $var;
	}
}

if(!function_exists("setDMY")) {
	function setDMY($var, $chr) {
		list($year, $month, $day) = explode($chr, $var);
		# ---------------
		$var = $day . "-" . $month . "-" . $year;
		# ---------------
		return $var;
	}
}

if(!function_exists("displayDMY")) {
	function displayDMY($var) {
		if(!empty($var)) {
			list($year, $month, $day) = explode("-", $var);
			# ---------------
			$var = $day . "/" . $month . "/" . $year;
			# ---------------
			return $var;
		} else {
			return "";
		}
	}
}

if(!function_exists("setNoComma")) {
	function setNoComma($var) {
		$var = str_replace(",", "", $var);
		# ---------------
		return $var;
	}
}

if(!function_exists("setComma")) {
	function setComma($var) {
		$var = number_format($var, 0);
		# ---------------
		return $var;
	}
}

if(!function_exists("getNextMonth")) {
	function getNextMonth($var) {
		if ($var <12)
		{
			$var=$var+1;
		}
		else
		{
			$var=1;
		}
	
		return $var;
	}
}

if(!function_exists("getNextYear")) {
	function getNextYear($var,$tahun) {
		if ($var <12)
		{
			$tahun=$tahun;
		}
		else
		{
			$tahun=$tahun+1;
		}
	
		return $tahun;
	}
}

if(!function_exists("setTitik")) {
	function setTitik($var) {
		$var = number_format($var,0,",",".");
		# ---------------
		return $var;
	}
}
if(!function_exists("getMonthName")) {
	function getMonthName($var, $lang=null) {		
		switch($var) {
			case 1:
				$en = "January";
				$id = "JANUARI";
				break;
			case 2:
				$en = "February";
				$id = "FEBRUARI";
				break;
			case 3:
				$en = "March";
				$id = "MARET";
				break;
			case 4:
				$en = "April";
				$id = "APRIL";
				break;
			case 5:
				$en = "May";
				$id = "MEI";
				break;
			case 6:
				$en = "June";
				$id = "JUNI";
				break;
			case 7:
				$en = "July";
				$id = "JULI";
				break;
			case 8:
				$en = "August";
				$id = "AGUSTUS";
				break;
			case 9:
				$en = "September";
				$id = "SEPTEMBER";
				break;
			case 10:
				$en = "October";
				$id = "OKTOBER";
				break;
			case 11:
				$en = "November";
				$id = "NOVEMBER";
				break;
			case 12:
				$en = "December";
				$id = "DESEMBER";
				break;
		}
		# ---------------
		if($lang == "en") {
			return $en;
		} else {
			return $id;
		}
	}
}