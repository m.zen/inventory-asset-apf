$(".btn-none-direct").click(function() {
  	var url 				= $(this).attr("title");
  	// --------------
  	window.location.href 	= url;
});

$(".btn-single-direct").click(function () {
  	var searchIDs = [];
  	var atLeastOneIsChecked = $(".checkbox_id:checked").length;

  	if(atLeastOneIsChecked == 0) {
    		$("#alert-message").text("No item selected (0)");
    		$("#alert-box").slideDown(100).delay(1000).slideUp(500);
  	} else if(atLeastOneIsChecked == 1) {
    		$("#myform input:checkbox:checked").map(function(){
    			   searchIDs.push($(this).val());
    		});

    		var url = $(this).attr("title") + "/" + searchIDs;

    		window.location.href 	= url;
  	} else {
    		$("#alert-message").text("Please select only one item");
    		$("#alert-box").slideDown(100).delay(1000).slideUp(500);
  	}
});

$(".btn-single-prompt").click(function () {
	var searchIDs 			= [];
	var atLeastOneIsChecked = $(".checkbox_id:checked").length;

	if(atLeastOneIsChecked == 0) {
		$("#alert-message").text("No item selected (0)");
		$("#alert-box").slideDown(100).delay(1000).slideUp(500);
	} else if(atLeastOneIsChecked == 1) {
		$("#myform input:checkbox:checked").map(function(){
			searchIDs.push($(this).val());
		});

		var url_comb = $(this).attr("title").split("|");

		var url = url_comb[0] + "/" + searchIDs;

		swal({
            title: url_comb[1],
            //text: url_comb[1],
            type: "input",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: url_comb[1]
        },
        function(inputValue){
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Anda perlu menulis sesuatu!");
                return false
            }

			window.location.href 	= url;
        });
	} else {
		$("#alert-message").text("Please select only one item");
		$("#alert-box").slideDown(100).delay(1000).slideUp(500);
	}
});
