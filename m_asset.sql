/*
SQLyog Community v13.1.9 (64 bit)
MySQL - 8.0.31-0ubuntu0.22.04.1 : Database - db_inventory
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_inventory` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_inventory`;

/*Table structure for table `m_asset` */

DROP TABLE IF EXISTS `m_asset`;

CREATE TABLE `m_asset` (
  `id_asset` int NOT NULL AUTO_INCREMENT,
  `id_asset_header` int DEFAULT NULL,
  `id_barang` int DEFAULT NULL,
  `no_asset` varchar(20) DEFAULT NULL,
  `tgl_perolehan` date DEFAULT NULL,
  `harga_perolehan` decimal(10,0) DEFAULT NULL,
  `status_asset` varchar(10) DEFAULT NULL,
  `id_lokasi` int DEFAULT NULL,
  `id_cabang` int DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  `status_approve` varchar(10) DEFAULT NULL,
  `kondisi_pembelian` varchar(20) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `serial_no` varchar(20) DEFAULT NULL,
  `id_referensi` int DEFAULT NULL,
  `id_departemen` int DEFAULT NULL,
  `gambar_asset` varchar(100) DEFAULT NULL,
  `aktif` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_asset`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `m_asset` */

insert  into `m_asset`(`id_asset`,`id_asset_header`,`id_barang`,`no_asset`,`tgl_perolehan`,`harga_perolehan`,`status_asset`,`id_lokasi`,`id_cabang`,`keterangan`,`status_approve`,`kondisi_pembelian`,`user_id`,`create_at`,`update_at`,`serial_no`,`id_referensi`,`id_departemen`,`gambar_asset`,`aktif`) values 
(1,NULL,1,'IT-0003','2022-10-03',15000000,'Beli',1,2,'-','Proses','Baru',21,'2022-10-03 04:18:06','2022-10-03 04:18:06','0',1,1,'Aset-1 2022-10-03.png','0'),
(2,2,1,NULL,NULL,NULL,'-',NULL,NULL,NULL,NULL,'Baru',27,'2022-10-27 10:10:48','2022-10-27 10:10:48',NULL,NULL,NULL,NULL,NULL),
(3,2,1,NULL,NULL,NULL,'Beli',NULL,NULL,NULL,NULL,'-',27,'2022-10-27 10:10:48','2022-10-27 10:10:48',NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
